package ec.gob.msp.rdacaa.validation.webservice.process;

import java.io.File;
import java.io.IOException;
import java.util.UUID;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import ec.gob.msp.alfresco.operations.AlfrescoOperations;
import ec.gob.msp.rdacaa.test.ValidationTestContext;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = ValidationTestContext.class)
public class AlfrescoControllerTest {

	private static final Logger logger = LoggerFactory.getLogger(AlfrescoControllerTest.class);

	@Value("${alfrescoUrl}")
	private String alfrescoUrl;

	@Value("${userAlfresco}")
	private String userAlfresco;

	@Value("${passwordAlfresco}")
	private String passwordAlfresco;

	@Value("${carpetaInputAlfresco}")
	private String carpetaInputAlfresco;

	@Value("${carpetaOutputAlfresco}")
	private String carpetaOutputAlfresco;

	@Autowired
	private AlfrescoController alfrescoController;

	@Test
	public void whenDownloadFileByUUID_thenVerifyDownload() throws IOException {
		String TEST_FILE_NAME = "530e66f2-c290-4b26-b93c-43f8e972a63a";
		String fileUUID = AlfrescoOperations.findFileUUID(alfrescoUrl, carpetaInputAlfresco, TEST_FILE_NAME,
				userAlfresco, passwordAlfresco);
		logger.info("fileUUID descargado {}", fileUUID);

		String downloadedFilePath = alfrescoController.downloadFileByUUID(fileUUID);
		File downloadedFile = new File(downloadedFilePath);
		
		boolean exists = (null != downloadedFile && downloadedFile.exists());
		Assert.assertTrue("Se espera que el archivo se descargue en la ruta ingresada", exists);
	}

	@Test
	public void whenUploadValidatedFile_thenVerifyUpload() throws IOException {
		ClassLoader classLoader = getClass().getClassLoader();

		File fileArchivoASubir = new File(
				classLoader.getResource("validateTestDataPrivateKeyCiphered-VALIDATED.rdacaa").getFile());

		String pathDirArchivoASubir = fileArchivoASubir.getAbsolutePath();
		
		File archvivoASubir = new File(pathDirArchivoASubir);

		logger.info(pathDirArchivoASubir);

		UUID fileNameUuid = UUID.randomUUID();
		
		logger.info("File name subido {}", fileNameUuid.toString());
		
		String fileUUID = alfrescoController.uploadValidatedFile(archvivoASubir, fileNameUuid.toString());
		logger.info("fileUUID subido {}", fileUUID);

		String fileUUIDBuscado = AlfrescoOperations.findFileUUID(alfrescoUrl, carpetaOutputAlfresco, fileNameUuid.toString(),
				userAlfresco, passwordAlfresco);
		
		logger.info("fileUUID buscado {}", fileUUIDBuscado);
		
		boolean exists = (fileUUIDBuscado.equals(fileUUID));
		Assert.assertTrue("Se espera que el archivo se encuentre en la ruta remota", exists);
	}
}

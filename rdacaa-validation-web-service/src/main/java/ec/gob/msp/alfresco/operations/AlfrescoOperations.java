package ec.gob.msp.alfresco.operations;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.Charset;

import org.apache.commons.codec.binary.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AlfrescoOperations {

	private static final String CMIS_OBJECT_ID = "cmis:objectId";
	private static final String SLASH = "/";
	private static final String CONTENT_TYPE = "Content-Type";
	private static final String AUTHORIZATION = "Authorization";

	private AlfrescoOperations() {
	}

	private static final Logger logger = LoggerFactory.getLogger(AlfrescoOperations.class);

	public static String list(String alfrescoUrl, String libraryName, String user, String password) {

		StringBuilder responseBody = new StringBuilder("");

		try {
			String url = alfrescoUrl + "/api/-default-/public/cmis/versions/1.1/browser/root/" + libraryName
					+ "?cmisselector=children&succinct=true";

			HttpURLConnection connection = (HttpURLConnection) new URL(url).openConnection();
			connection.setRequestProperty(AUTHORIZATION, getAuthorizationHeader(user, password));

			connection.setRequestMethod("GET");

			int responseCode = connection.getResponseCode();
			if (responseCode == 200) {
				String result = processResponse(connection.getInputStream());

				int inicial = result.indexOf("cmis:contentStreamFileName");

				while (inicial > 0) {
					responseBody.append(",").append(result.substring(inicial + 29, result.indexOf('"', inicial + 29)));
					inicial = result.indexOf("cmis:contentStreamFileName", inicial + 30);
				}

			} else {
				logger.error("No se encontró el directorio");
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
			responseBody.setLength(0);
			responseBody = responseBody.append(e.getMessage());
		}
		return responseBody.toString();
	}

	public static String createFolder(String alfrescoUrl, String libraryName, String newFolderName, String user,
			String password) {

		String newFolderUUID = findFolderUUID(alfrescoUrl, libraryName + newFolderName, user, password);

		String responseBody = null;

		if (newFolderUUID.length() == 0) {
			try {

				String nameFolder = "";
				String parent = "";
				if (newFolderName.lastIndexOf(SLASH) > 0) {
					nameFolder = newFolderName.substring(newFolderName.lastIndexOf(SLASH) + 1);

					parent = newFolderName.substring(0, newFolderName.lastIndexOf(SLASH));
				} else {
					nameFolder = newFolderName;

					parent = "";
				}

				String folderUUID = findFolderUUID(alfrescoUrl, libraryName + parent, user, password);

				String url = alfrescoUrl + "/s/api/node/folder/workspace/SpacesStore/" + folderUUID;

				HttpURLConnection connection = (HttpURLConnection) new URL(url).openConnection();
				connection.setRequestProperty(AUTHORIZATION, getAuthorizationHeader(user, password));
				connection.setRequestProperty(CONTENT_TYPE, "application/json; charset=UTF-8");

				connection.setDoOutput(true);
				connection.setRequestMethod("POST");

				OutputStream output = connection.getOutputStream();
				output.write(("{\"name\": \"" + nameFolder + "\",\"title\": \"" + nameFolder + "\",\"description\": \""
						+ nameFolder + "\",\"type\": \"cm:folder\"}").getBytes());

				int responseCode = connection.getResponseCode();
				if (responseCode == 200) {
					responseBody = "true";
				} else {
					logger.error("No se pudo crear la carpeta");
					logger.error("{} : {}", connection.getResponseCode(), connection.getResponseMessage());
					responseBody = "false";
				}
			} catch (Exception e) {
				logger.error(e.getMessage());
				responseBody = e.getMessage();
			}
		} else {
			responseBody = "true";
		}

		return responseBody;
	}

	public static String saveFile(String alfrescoUrl, String libraryName, String fileName, File file, String user,
			String password) {

		String responseBody = "";
		try (InputStream is = new FileInputStream(file)) {

			String folderUuid = findFolderUUID(alfrescoUrl, libraryName, user, password);

			String crlf = "\r\n";

			String boundary = Long.toHexString(System.currentTimeMillis());

			String url = alfrescoUrl + "/s/api/upload";

			HttpURLConnection connection = (HttpURLConnection) new URL(url).openConnection();
			connection.setRequestProperty(AUTHORIZATION, getAuthorizationHeader(user, password));
			connection.setRequestProperty(CONTENT_TYPE, "multipart/form-data; boundary=" + boundary);

			connection.setDoOutput(true);
			connection.setRequestMethod("POST");

			OutputStream output = connection.getOutputStream();
			PrintWriter writer = new PrintWriter(new OutputStreamWriter(output, Charset.forName("UTF-8").newEncoder()),
					true);

			// Send normal param.
			writer.append("--" + boundary).append(crlf);
			writer.append("Content-Disposition: form-data; name=\"filename\"").append(crlf);
			writer.append(crlf).append(fileName).append(crlf).flush();

			writer.append("--" + boundary).append(crlf);
			writer.append("Content-Disposition: form-data; name=\"destination\"").append(crlf);
			writer.append(crlf).append("workspace://SpacesStore/" + folderUuid).append(crlf).flush();

			writer.append("--" + boundary).append(crlf);
			writer.append("Content-Disposition: form-data; name=\"createdirectory\"").append(crlf);
			writer.append(crlf).append("true").append(crlf).flush();

			writer.append("--" + boundary).append(crlf);
			writer.append("Content-Disposition: form-data; name=\"overwrite\"").append(crlf);
			writer.append(crlf).append("true").append(crlf).flush();

			// Send binary file.
			writer.append("--" + boundary).append(crlf);
			writer.append("Content-Disposition: form-data; name=\"filedata\"; filename=\"" + file.getName() + "\"")
					.append(crlf);
			writer.append("Content-Type: " + URLConnection.guessContentTypeFromName(file.getName())).append(crlf);
			writer.append("Content-Transfer-Encoding: binary").append(crlf);
			writer.append(crlf).flush();

			byte[] buffer = new byte[4096];
			int length;
			while ((length = is.read(buffer)) > 0) {
				output.write(buffer, 0, length);
			}

			output.flush(); // Important before continuing with writer!
			writer.append(crlf).flush(); // CRLF is important! It indicates end
											// of boundary.

			// End of multipart/form-data.
			writer.append("--" + boundary + "--").append(crlf).flush();

			int responseCode = connection.getResponseCode();
			if (responseCode == 200) {
				String result = processResponse(connection.getInputStream());
				
				logger.debug("Result save file {}",result);
				
				int inicial = result.indexOf("SpacesStore") + 12;

				responseBody = result.substring(inicial, result.indexOf('"', inicial));

			} else {
				logger.error("No se pudo crear el archivo");
				logger.error("{} : {}", connection.getResponseCode(), connection.getResponseMessage());
				throw new AlfrescoSavingException(
						connection.getResponseCode() + ": " + connection.getResponseMessage());
			}

		} catch (Exception e) {
			logger.error(e.getMessage());
			throw new AlfrescoSavingException("No se pudo crear el archivo", e);
		}
		return responseBody;
	}

	public static byte[] getFile(String alfrescoUrl, String uuid, String user, String password) {
		try {
			String url = alfrescoUrl + "/api/-default-/public/cmis/versions/1.1/atom/content?id=" + uuid;
			HttpURLConnection connection = (HttpURLConnection) new URL(url).openConnection();
			connection.setRequestProperty(AUTHORIZATION, getAuthorizationHeader(user, password));

			connection.setRequestMethod("GET");

			int responseCode = connection.getResponseCode();
			if (responseCode == 200) {
				InputStream response = connection.getInputStream();

				byte[] buffer = new byte[4096];
				int n;

				ByteArrayOutputStream output = new ByteArrayOutputStream();
				while ((n = response.read(buffer)) != -1) {
					output.write(buffer, 0, n);
				}
				output.close();
				return output.toByteArray();
			} else {
				return new byte[0];
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw new AlfrescoDownloadingException("Error descargando el archivo de Alfresco", e);
		}
	}

	public static byte[] getFile(String alfrescoUrl, String libraryName, String fileName, String user,
			String password) {

		try {
			String uuid = findFileUUID(alfrescoUrl, libraryName, fileName, user, password);
			return getFile(alfrescoUrl, uuid, user, password);
		} catch (Exception e) {
			logger.error(e.getMessage());
			return new byte[0];
		}

	}

	public static String findFolderUUID(String alfrescoUrl, String libraryName, String username, String password) {

		String responseBody = "";
		try {
			String nameFolder = "";
			if (libraryName.lastIndexOf(SLASH) > 0) {
				nameFolder = libraryName.substring(libraryName.lastIndexOf(SLASH) + 1);
			} else {
				nameFolder = libraryName;
			}

			String url = alfrescoUrl
					+ "/api/-default-/public/cmis/versions/1.1/browser/?cmisselector=query&succinct=true&q="
					+ "SELECT+cmis:path,+cmis:objectId+FROM+cmis:folder+WHERE+cmis:name='" + nameFolder + "'";

			HttpURLConnection connection = (HttpURLConnection) new URL(url).openConnection();
			connection.setRequestProperty(AUTHORIZATION, getAuthorizationHeader(username, password));
			connection.setRequestProperty(CONTENT_TYPE, "application/json; charset=UTF-8");

			connection.setDoOutput(true);
			connection.setRequestMethod("GET");
			int responseCode = connection.getResponseCode();
			if (responseCode == 200) {
				InputStream response = connection.getInputStream();

				String result = processResponse(response);
				String replace = libraryName.replace(SLASH, "\\/");
				int path = result.indexOf("cmis:path\":\"\\/" + replace + "\"");

				if (path > 0) {
					int inicial = result.indexOf(CMIS_OBJECT_ID, path) + 16;
					responseBody = result.substring(inicial, result.indexOf('"', inicial + 16));
				} else {
					responseBody = "";
				}

			} else {
				logger.info("No se encontró la carpeta: {}", libraryName);
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return responseBody;
	}

	public static String findFileUUID(String alfrescoUrl, String libraryName, String fileName, String username,
			String password) {

		String responseBody = "";

		try {

			String nameFile = "";
			String parentFolder = "";
			if (fileName.lastIndexOf(SLASH) > 0) {
				nameFile = fileName.substring(fileName.lastIndexOf(SLASH) + 1);

				parentFolder = fileName.substring(0, fileName.lastIndexOf(SLASH));
			} else {
				nameFile = fileName;

				parentFolder = "";
			}

			String url = alfrescoUrl + "/api/-default-/public/cmis/versions/1.1/browser/root/" + libraryName + SLASH
					+ parentFolder + "?cmisselector=children&succinct=true";
			
			logger.debug("URL {}",url);

			HttpURLConnection connection = (HttpURLConnection) new URL(url).openConnection();
			connection.setRequestProperty(AUTHORIZATION, getAuthorizationHeader(username, password));

			connection.setRequestMethod("GET");

			int responseCode = connection.getResponseCode();
			if (responseCode == 200) {
				String result = processResponse(connection.getInputStream());
				
				logger.debug("Result find file {}",result);
				
				int inicial = result.indexOf(nameFile);

				responseBody = result.substring(result.indexOf(CMIS_OBJECT_ID, inicial) + 16,
						result.indexOf(';', result.indexOf(CMIS_OBJECT_ID, inicial + 17)));

			} else {
				logger.error("No se encontro el archivo: {} en el directorio: {}", fileName, libraryName);
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return responseBody;
	}

	private static String getAuthorizationHeader(String user, String password) {
		String authString = user + ":" + password;
		byte[] authEncBytes = Base64.encodeBase64(authString.getBytes());
		return "Basic " + new String(authEncBytes);
	}

	private static String processResponse(InputStream response) {

		try {
			InputStreamReader isr = new InputStreamReader(response);

			int numCharsRead;
			char[] charArray = new char[1024];
			StringBuilder sb = new StringBuilder();
			while ((numCharsRead = isr.read(charArray)) > 0) {
				sb.append(charArray, 0, numCharsRead);
			}
			return sb.toString();

		} catch (Exception e) {
			logger.error(e.getMessage());
			return "";
		}
	}
}
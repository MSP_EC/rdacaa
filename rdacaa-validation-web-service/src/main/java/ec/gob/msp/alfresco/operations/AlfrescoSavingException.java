package ec.gob.msp.alfresco.operations;

public class AlfrescoSavingException extends RuntimeException {

	private static final long serialVersionUID = -8255099845618064203L;

	public AlfrescoSavingException() {
		super();
	}

	public AlfrescoSavingException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public AlfrescoSavingException(String message, Throwable cause) {
		super(message, cause);
	}

	public AlfrescoSavingException(String message) {
		super(message);
	}

	public AlfrescoSavingException(Throwable cause) {
		super(cause);
	}

	
}

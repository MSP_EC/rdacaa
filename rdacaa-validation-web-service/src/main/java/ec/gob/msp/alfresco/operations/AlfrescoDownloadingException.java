package ec.gob.msp.alfresco.operations;

public class AlfrescoDownloadingException extends RuntimeException {

	private static final long serialVersionUID = 6620320451785819198L;

	public AlfrescoDownloadingException() {
		super();
	}

	public AlfrescoDownloadingException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public AlfrescoDownloadingException(String message, Throwable cause) {
		super(message, cause);
	}

	public AlfrescoDownloadingException(String message) {
		super(message);
	}

	public AlfrescoDownloadingException(Throwable cause) {
		super(cause);
	}
}

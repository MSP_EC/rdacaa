package ec.gob.msp.rdacaa.validation.webservice.process;

import java.io.File;
import java.io.IOException;
import java.security.GeneralSecurityException;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.google.crypto.tink.config.TinkConfig;

import ec.gob.msp.rdacaa.business.validation.database.common.DecipheringWithPrivateKeyDataFunction;
import ec.gob.msp.rdacaa.business.validation.database.common.MapperFunction;
import ec.gob.msp.rdacaa.business.validation.database.common.PreparingResultsFunction;
import ec.gob.msp.rdacaa.business.validation.database.common.ValidatorFunction;
import ec.gob.msp.rdacaa.utilitario.exportdatabase.CipheredDataParams;
import ec.gob.msp.rdacaa.utilitario.exportdatabase.SQLiteUtil;

@Component
public class ValidateRdacaaFile {

	private static final Logger logger = LoggerFactory.getLogger(ValidateRdacaaFile.class);

	@Autowired
	private MapperFunction mapperFunction;

	@Autowired
	private ValidatorFunction validatorFunction;

	@Autowired
	private PreparingResultsFunction preparingResultsFunction;

	private DecipheringWithPrivateKeyDataFunction decipheringWithPrivateKeyDataFunction = new DecipheringWithPrivateKeyDataFunction();

	@PostConstruct
	public void init() throws GeneralSecurityException {
		// Initialize the config.
		TinkConfig.register();
	}

	public File validateCipheredRdacaaFile(String pathDirArchivoAValidar, String privatekeyPath) {

		try {
			logger.debug(pathDirArchivoAValidar);
			logger.debug(privatekeyPath);

			CipheredDataParams cipheredDataParams = new CipheredDataParams(null, false, true, privatekeyPath);
			File validatedFile = SQLiteUtil.exportAfterValidationMeshCipheredFile(pathDirArchivoAValidar,
					decipheringWithPrivateKeyDataFunction, mapperFunction, validatorFunction, preparingResultsFunction,
					cipheredDataParams);

			boolean exists = (validatedFile != null && validatedFile.exists());

			if (exists) {
				return validatedFile;
			}else {
				throw new ValidateCipheredRdacaaFileException("Error archivo resultante no existe");
			}
		} catch (IOException e) {
			logger.error("Error creando base temporal", e);
			throw new ValidateCipheredRdacaaFileException("Error creando base temporal", e);
		} catch (Exception e) {
			logger.error("Ejecutando la validación", e);
			throw new ValidateCipheredRdacaaFileException("Ejecutando la validación", e);
		}

	}
}

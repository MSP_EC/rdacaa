package ec.gob.msp.rdacaa.validation.webservice.process;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import ec.gob.msp.alfresco.operations.AlfrescoFileNotFoundException;
import ec.gob.msp.alfresco.operations.AlfrescoOperations;

@Component
public class AlfrescoController {

	private static final Logger logger = LoggerFactory.getLogger(AlfrescoController.class);

	@Value("${alfrescoUrl}")
	private String alfrescoUrl;

	@Value("${userAlfresco}")
	private String userAlfresco;

	@Value("${passwordAlfresco}")
	private String passwordAlfresco;

	@Value("${carpetaInputAlfresco}")
	private String carpetaInputAlfresco;

	@Value("${carpetaOutputAlfresco}")
	private String carpetaOutputAlfresco;

	public String downloadFileByUUID(String uuid) throws IOException {
		byte[] fileBuffer = AlfrescoOperations.getFile(alfrescoUrl, uuid, userAlfresco, passwordAlfresco);
		if (fileBuffer.length == 0) {
			logger.warn("Archivo con uuid {} no encontrado", uuid);
			throw new AlfrescoFileNotFoundException("Archivo no encontrado en Alfresco");
		}
		String tempFilePath = Files.createTempFile(uuid + "---", ".rdacaa").toString();
		Path path = Paths.get(tempFilePath);
		Files.write(path, fileBuffer);
		return tempFilePath;
	}

	public String uploadValidatedFile(File validatedFile, String fileName) {
		return AlfrescoOperations.saveFile(alfrescoUrl, carpetaOutputAlfresco, fileName, validatedFile, userAlfresco,
				passwordAlfresco);
	}
}

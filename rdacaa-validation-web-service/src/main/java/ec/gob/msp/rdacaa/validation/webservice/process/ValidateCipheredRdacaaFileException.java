package ec.gob.msp.rdacaa.validation.webservice.process;

public class ValidateCipheredRdacaaFileException extends RuntimeException {

	private static final long serialVersionUID = -4313788713580731695L;

	public ValidateCipheredRdacaaFileException() {
		super();
	}

	public ValidateCipheredRdacaaFileException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public ValidateCipheredRdacaaFileException(String message, Throwable cause) {
		super(message, cause);
	}

	public ValidateCipheredRdacaaFileException(String message) {
		super(message);
	}

	public ValidateCipheredRdacaaFileException(Throwable cause) {
		super(cause);
	}

}

package ec.gob.msp.rdacaa.validation.webservice.process;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import javax.annotation.PostConstruct;
import javax.xml.bind.JAXBElement;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.TypeFactory;

import ec.gob.msp.alfresco.operations.AlfrescoDownloadingException;
import ec.gob.msp.alfresco.operations.AlfrescoFileNotFoundException;
import ec.gob.msp.alfresco.operations.AlfrescoSavingException;
import ec.gob.msp.rdacaa.utilitario.exportdatabase.SQLiteUtil;
import ec.gob.msp.rdacaa.validation.webservice.pojo.RespuestaValidacionArchivoRdacaa;
import ec.gob.msp.rdacaa.validation.webservice.pojo.RespuestaValidacionArchivosRdacaaTotal;
import ec.gob.msp.rdacaa.validation.webservice.pojo.ValidarArchivosRdacaaRequest;
import ec.gob.msp.rdacaa.validation.webservice.pojo.ValidarArchivosRdacaaResponse;

@Component
public class RdacaaValidationWebServiceProcess {

	private static final String ERROR_ELIMINANDO_TEMP_FILE_PATH = "Error eliminando tempFilePath";

	private static final String ERROR_ELIMINANDO_VALIDATED_RDACAA_FILE = "Error eliminando validatedRdacaaFile";

	private static final Logger logger = LoggerFactory.getLogger(RdacaaValidationWebServiceProcess.class);

	private ec.gob.msp.rdacaa.validation.webservice.pojo.ObjectFactory objectFactory = new ec.gob.msp.rdacaa.validation.webservice.pojo.ObjectFactory();

	private ScheduledExecutorService scheduledExecutorService;

	@Autowired
	private AlfrescoController alfrescoController;

	@Autowired
	private ValidateRdacaaFile validateRdacaaFile;

	@Value("${privatekey}")
	private String privatekeyPath;

	@Value("${alfrescoUrl}")
	private String alfrescoUrl;

	private static final String RESPONSE_OK = "000";
	private static final String RESPONSE_OK_MESSAGE = "Archivos enviados a procesar correctamente";
	private static final String RESPONSE_OK_EN_PROCESO = "001";
	private static final String RESPONSE_OK_MESSAGE_EN_PROCESO = "Sistema en proceso de validación";
	private static final String RESPONSE_ERROR_EN_PROCESO = "002";
	private static final String RESPONSE_ERROR_MESSAGE_EN_PROCESO = "Sistema en proceso de validación, no pueden agregarse mas archivos hasta terminar el actual proceso";
	private static final String RESPONSE_PROCESO_FINALIZADO = "003";
	private static final String RESPONSE_PROCESO_FINALIZADO_MESSAGE = "Procesamiento finalizado";

	private static final String RDACAA_FILE_NO_ERROR = "004";
	private static final String RDACAA_FILE_NO_ERROR_MESSAGE = "Archivo sin errores subido correctamente";
	private static final String RDACAA_FILE_ERROR = "005";
	private static final String RDACAA_FILE_ERROR_MESSAGE = "Archivo con errores subido correctamente";

	private static final String ERROR_DOWNLOADING_FILE = "-001";
	private static final String ERROR_DOWNLOADING_FILE_MESSAGE = "Error descargando archivo de alfresco en la URL: ";
	private static final String ERROR_FILE_NOT_FOUND = "-002";
	private static final String ERROR_FILE_NOT_FOUND_MESSAGE = "Error archivo no encontrado en alfresco en la URL: ";
	private static final String ERROR_VALIDATING_RDACAA_FILE = "-003";
	private static final String ERROR_VALIDATING_RDACAA_FILE_MESSAGE = "Error validando el archivo ";
	private static final String ERROR_UPLOADING_FILE = "-004";
	private static final String ERROR_UPLOADING_FILE_MESSAGE = "Error subiendo archivo de alfresco en la URL: ";
	private static final String ERROR_INPUT_FILES_LIST = "-005";
	private static final String ERROR_INPUT_FILES_LIST_MESSAGE = "Error transformado la lista de archivos, json inválido";

	private RespuestaValidacionArchivosRdacaaTotal responseTotal;
	private List<RespuestaValidacionArchivoRdacaa> responseFiles;
	private List<String> uuidsAValidar;
	private List<Future<?>> listaTareasEnEjecucion;

	@PostConstruct
	private void init() {
		this.responseTotal = new RespuestaValidacionArchivosRdacaaTotal();
		this.responseFiles = responseTotal.getResponseFiles();
		this.uuidsAValidar = new ArrayList<>();
		this.listaTareasEnEjecucion = new ArrayList<>();

		scheduledExecutorService = Executors.newScheduledThreadPool(1);
		Runnable runnableTask = () -> logger.info("Iniciando ejecutor");

		Future<?> future = scheduledExecutorService.schedule(runnableTask, 100, TimeUnit.MILLISECONDS);
		listaTareasEnEjecucion.add(future);
		scheduledExecutorService.shutdown();
	}

	private boolean isProcessValidationDone() {
		return listaTareasEnEjecucion.stream().allMatch(Future::isDone);
	}

	public ValidarArchivosRdacaaResponse runRdacaaValidationWebServiceProcess(ValidarArchivosRdacaaRequest request) {

		List<String> uuidsAValidarNew;

		try {
			uuidsAValidarNew = getUUIDsAValidar(request);
		} catch (IOException e) {
			responseTotal.setCodigoMensaje(ERROR_INPUT_FILES_LIST);
			responseTotal.setMensaje(ERROR_INPUT_FILES_LIST_MESSAGE);
			responseTotal.setArchivosPendientes(null);
			responseTotal.setArchivosProcesados(null);
			ValidarArchivosRdacaaResponse validarArchivosRdacaaResponse = new ValidarArchivosRdacaaResponse();
			validarArchivosRdacaaResponse.setResponse(responseTotal);
			return validarArchivosRdacaaResponse;
		}

		if (isProcessValidationDone() && !uuidsAValidarNew.isEmpty()) {

			listaTareasEnEjecucion.clear();
			scheduledExecutorService.shutdown();

			responseFiles.clear();

			scheduledExecutorService = Executors.newScheduledThreadPool(2);

			uuidsAValidar = uuidsAValidarNew;

			for (String uuidAValidar : uuidsAValidar) {
				Runnable runnableTask = () -> {
					long tiempoInicio = System.currentTimeMillis();
					RespuestaValidacionArchivoRdacaa oneFile = processOneFile(uuidAValidar);
					long tiempoFin = System.currentTimeMillis();
					long tiempoTotal = tiempoFin - tiempoInicio;
					logger.info("Tiempo Total archivo {}", tiempoTotal);
					synchronized (responseFiles) {
						responseFiles.add(oneFile);
					}
				};
				Future<?> future = scheduledExecutorService.submit(runnableTask);
				listaTareasEnEjecucion.add(future);
			}

			responseTotal.setCodigoMensaje(RESPONSE_OK);
			responseTotal.setMensaje(RESPONSE_OK_MESSAGE);
			responseTotal
					.setArchivosPendientes(objectFactory.createRespuestaValidacionArchivosRdacaaTotalArchivosPendientes(
							String.valueOf(uuidsAValidar.size())));
			responseTotal.setArchivosProcesados(
					objectFactory.createRespuestaValidacionArchivosRdacaaTotalArchivosProcesados("0"));

		} else {

			if (isProcessValidationDone()) {
				listaTareasEnEjecucion.clear();
				scheduledExecutorService.shutdown();
			}
			
			int archivosPendientes = uuidsAValidar.size() - responseFiles.size();
			int archivosProcesados = responseFiles.size();

			if (archivosPendientes == 0 && uuidsAValidarNew.isEmpty()) {
				responseTotal.setCodigoMensaje(RESPONSE_PROCESO_FINALIZADO);
				responseTotal.setMensaje(RESPONSE_PROCESO_FINALIZADO_MESSAGE);
			} else {
				if (uuidsAValidarNew.isEmpty()) {
					responseTotal.setCodigoMensaje(RESPONSE_OK_EN_PROCESO);
					responseTotal.setMensaje(RESPONSE_OK_MESSAGE_EN_PROCESO);
				} else {
					responseTotal.setCodigoMensaje(RESPONSE_ERROR_EN_PROCESO);
					responseTotal.setMensaje(RESPONSE_ERROR_MESSAGE_EN_PROCESO);
				}
			}

			responseTotal
					.setArchivosPendientes(objectFactory.createRespuestaValidacionArchivosRdacaaTotalArchivosPendientes(
							String.valueOf(archivosPendientes)));
			responseTotal
					.setArchivosProcesados(objectFactory.createRespuestaValidacionArchivosRdacaaTotalArchivosProcesados(
							String.valueOf(archivosProcesados)));

		}

		ValidarArchivosRdacaaResponse validarArchivosRdacaaResponse = new ValidarArchivosRdacaaResponse();
		validarArchivosRdacaaResponse.setResponse(responseTotal);
		return validarArchivosRdacaaResponse;

	}

	private RespuestaValidacionArchivoRdacaa processOneFile(String uuidAValidar) {
		String tempFilePath = "";
		try {
			tempFilePath = alfrescoController.downloadFileByUUID(uuidAValidar);
		} catch (IOException | AlfrescoDownloadingException e) {
			logger.error("", e);
			return createResponseObject(ERROR_DOWNLOADING_FILE, ERROR_DOWNLOADING_FILE_MESSAGE + alfrescoUrl,
					uuidAValidar, "", "");
		} catch (AlfrescoFileNotFoundException e) {
			return createResponseObject(ERROR_FILE_NOT_FOUND, ERROR_FILE_NOT_FOUND_MESSAGE + alfrescoUrl, uuidAValidar, "", "");
		}

		if (!tempFilePath.isEmpty()) {
			File validatedRdacaaFile = null;
			try {
				logger.info("Iniciando validación {}", uuidAValidar);
				validatedRdacaaFile = validateRdacaaFile.validateCipheredRdacaaFile(tempFilePath, privatekeyPath);
				logger.info("Finalizando validación {}", uuidAValidar);
				deleteIfExists(tempFilePath, ERROR_ELIMINANDO_TEMP_FILE_PATH);
			} catch (Exception e) {
				deleteIfExists(tempFilePath, ERROR_ELIMINANDO_TEMP_FILE_PATH);
				if (validatedRdacaaFile != null) {
					deleteIfExists(validatedRdacaaFile.getAbsolutePath(), ERROR_ELIMINANDO_VALIDATED_RDACAA_FILE);
				}
				logger.error("", e);
				return createResponseObject(ERROR_VALIDATING_RDACAA_FILE, ERROR_VALIDATING_RDACAA_FILE_MESSAGE,
						uuidAValidar, "", "");
			}

			if (validatedRdacaaFile != null) {

				UUID fileNameUuid = UUID.randomUUID();

				try {
					boolean isValidatedFileWithoutErrors = SQLiteUtil
							.isValidatedFileWithoutErrors(validatedRdacaaFile.getAbsolutePath());
					if (isValidatedFileWithoutErrors) {
						String uuidSubido = alfrescoController.uploadValidatedFile(validatedRdacaaFile,
								fileNameUuid.toString());
						deleteIfExists(validatedRdacaaFile.getAbsolutePath(), ERROR_ELIMINANDO_VALIDATED_RDACAA_FILE);
						return createResponseObject(RDACAA_FILE_NO_ERROR, RDACAA_FILE_NO_ERROR_MESSAGE, uuidAValidar,
								uuidSubido, fileNameUuid.toString());
					}
				} catch (FileNotFoundException er) {
					logger.error("Error validando si hay errores en el archivo " + validatedRdacaaFile.getName(), er);
				} catch (AlfrescoSavingException err) {
					deleteIfExists(validatedRdacaaFile.getAbsolutePath(), ERROR_ELIMINANDO_VALIDATED_RDACAA_FILE);
					return createResponseObject(ERROR_UPLOADING_FILE, ERROR_UPLOADING_FILE_MESSAGE + alfrescoUrl,
							uuidAValidar, "", "");
				}

				try {
					String uuidSubido = alfrescoController.uploadValidatedFile(validatedRdacaaFile,
							fileNameUuid.toString());
					deleteIfExists(validatedRdacaaFile.getAbsolutePath(), ERROR_ELIMINANDO_VALIDATED_RDACAA_FILE);
					return createResponseObject(RDACAA_FILE_ERROR, RDACAA_FILE_ERROR_MESSAGE, uuidAValidar, uuidSubido,
							fileNameUuid.toString());
				} catch (Exception e) {
					deleteIfExists(validatedRdacaaFile.getAbsolutePath(), ERROR_ELIMINANDO_VALIDATED_RDACAA_FILE);
					return createResponseObject(ERROR_UPLOADING_FILE, ERROR_UPLOADING_FILE_MESSAGE + alfrescoUrl,
							uuidAValidar, "", "");
				}
			}
		}

		return new RespuestaValidacionArchivoRdacaa();
	}

	private void deleteIfExists(String fileToDelete, String msg) {
		try {
			Files.deleteIfExists(Paths.get(fileToDelete));
		} catch (IOException e) {
			logger.error(msg, e);
		}
	}

	private List<String> getUUIDsAValidar(ValidarArchivosRdacaaRequest request) throws IOException {
		if (request.getUUIDsArchivoAValidar().trim().isEmpty()) {
			return new ArrayList<>();
		}

		ObjectMapper objectMapper = new ObjectMapper();
		TypeFactory typeFactory = objectMapper.getTypeFactory();
		return objectMapper.readValue(request.getUUIDsArchivoAValidar().trim(),
				typeFactory.constructCollectionType(List.class, String.class));
	}

	private RespuestaValidacionArchivoRdacaa createResponseObject(String codigoMensaje, String mensaje,
			String uuidAValidar, String uuidValidado, String nombreArchivoValidado) {
		RespuestaValidacionArchivoRdacaa response = new RespuestaValidacionArchivoRdacaa();
		response.setCodigoMensajeArchivo(codigoMensaje);
		response.setMensajeArchivo(mensaje);
		JAXBElement<String> uuidArchivoAValidar = objectFactory
				.createRespuestaValidacionArchivoRdacaaUUIDArchivoAValidar(uuidAValidar);
		JAXBElement<String> uuidArchivoValidado = objectFactory
				.createRespuestaValidacionArchivoRdacaaUUIDArchivoValidado(uuidValidado);
		JAXBElement<String> nomrchivoValidado = objectFactory
				.createRespuestaValidacionArchivoRdacaaNombreArchivoValidado(nombreArchivoValidado);
		response.setUUIDArchivoAValidar(uuidArchivoAValidar);
		response.setUUIDArchivoValidado(uuidArchivoValidado);
		response.setNombreArchivoValidado(nomrchivoValidado);

		return response;
	}
}

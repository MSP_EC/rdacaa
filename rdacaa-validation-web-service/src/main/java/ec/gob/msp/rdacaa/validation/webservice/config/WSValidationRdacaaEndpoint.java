package ec.gob.msp.rdacaa.validation.webservice.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import ec.gob.msp.rdacaa.validation.webservice.pojo.ValidarArchivosRdacaaRequest;
import ec.gob.msp.rdacaa.validation.webservice.pojo.ValidarArchivosRdacaaResponse;
import ec.gob.msp.rdacaa.validation.webservice.process.RdacaaValidationWebServiceProcess;

@Endpoint
public class WSValidationRdacaaEndpoint {

	private static final String NAMESPACE_URI = "http://msp.gob.ec/validarArchivoRdacaa";
	
	private static final Logger logger = LoggerFactory.getLogger(WSValidationRdacaaEndpoint.class);

	@Autowired
	private RdacaaValidationWebServiceProcess rdacaaValidationWebServiceProcess;

	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "ValidarArchivosRdacaaRequest")
	@ResponsePayload
	public ValidarArchivosRdacaaResponse validateRdacaaFile(@RequestPayload ValidarArchivosRdacaaRequest request) {
		logger.info("UUIDs archivos a validar {}", request.getUUIDsArchivoAValidar());
		return rdacaaValidationWebServiceProcess.runRdacaaValidationWebServiceProcess(request);
	}
}

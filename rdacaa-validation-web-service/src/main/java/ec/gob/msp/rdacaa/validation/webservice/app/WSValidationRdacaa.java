package ec.gob.msp.rdacaa.validation.webservice.app;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRegistration.Dynamic;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.autoconfigure.http.HttpMessageConvertersAutoConfiguration;
import org.springframework.boot.autoconfigure.jackson.JacksonAutoConfiguration;
import org.springframework.boot.autoconfigure.web.servlet.WebMvcAutoConfiguration;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.web.context.request.RequestContextListener;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.springframework.web.filter.DelegatingFilterProxy;
import org.springframework.ws.transport.http.MessageDispatcherServlet;

@SpringBootApplication(exclude = { WebMvcAutoConfiguration.class, JacksonAutoConfiguration.class,
		HttpMessageConvertersAutoConfiguration.class })
@EntityScan("ec.gob.msp.rdacaa.business.entity")
@EnableJpaRepositories(basePackages = "ec.gob.msp.rdacaa.business.repository")
@ComponentScan(basePackages = { "ec.gob.msp", "ec.gob.msp.rdacaa.system.database", "ec.gob.msp.rdacaa.business.service",
		"ec.gob.msp.rdacaa.business.validation" })
@PropertySource(value= {"file:/opt/rdacaa/ws-config-variables.properties"}, encoding="UTF-8")
public class WSValidationRdacaa extends SpringBootServletInitializer {

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return configureApplication(application);
	}

	public static void main(String[] args) {
		configureApplication(new SpringApplicationBuilder()).run(args);
	}

	private static SpringApplicationBuilder configureApplication(SpringApplicationBuilder application) {
		return application.sources(WSValidationRdacaa.class);
	}

	@Override
	public void onStartup(ServletContext servletContext) throws ServletException {

		AnnotationConfigWebApplicationContext ctx = new AnnotationConfigWebApplicationContext();
		ctx.register(WSValidationRdacaa.class);
		ctx.setServletContext(servletContext);
		MessageDispatcherServlet servlet = new MessageDispatcherServlet();
		servlet.setApplicationContext(ctx);
		
		servlet.setTransformWsdlLocations(true);
		
		Dynamic dynamic = servletContext.addServlet("dispatcher", servlet);
		// Servlet listener that exposes the request to the current thread
		servletContext.addListener(new RequestContextListener());

		dynamic.addMapping("/");
		dynamic.addMapping("/soapws/*");
		dynamic.setLoadOnStartup(1);
		
		final DelegatingFilterProxy springSecurityFilterChain = new DelegatingFilterProxy("springSecurityFilterChain");
		final javax.servlet.FilterRegistration.Dynamic addedFilter = servletContext.addFilter("springSecurityFilterChain", springSecurityFilterChain);
		addedFilter.addMappingForUrlPatterns(null, false, "/*");
	}
}

package ec.gob.msp.rdacaa.utilitario.medico.test;

import org.junit.Assert;
import org.junit.Test;

import ec.gob.msp.rdacaa.utilitario.medico.UtilitarioMedico;

public class UtilitarioMedicoTest {

	@Test
	public void whenZEntreTresYMenosTres_thenZ() {
		Double talla = 120.0d;
		Double l = 1.0d;
		Double m = 112.3895d;
		Double s = 0.04195d;
		
		Double z = UtilitarioMedico.calcularScoreZGivenValues(talla, l, m, s);
		Assert.assertTrue("Se espera un Z=1.61 pero es ", z.equals(1.61d));
	}
	
	@Test
	public void whenZMayorTres_thenZ() {
		Double talla = 127.0d;
		Double l = 1.0d;
		Double m = 112.3895d;
		Double s = 0.04195d;
		
		Double z = UtilitarioMedico.calcularScoreZGivenValues(talla, l, m, s);
		Assert.assertTrue("Se espera un Z=3.1 pero es ", z.equals(3.1d));
	}
	
	@Test
	public void whenZMenorTres_thenZ() {
		Double talla = 97.0d;
		Double l = 1.0d;
		Double m = 112.3895d;
		Double s = 0.04195d;
		
		Double z = UtilitarioMedico.calcularScoreZGivenValues(talla, l, m, s);
		Assert.assertTrue("Se espera un Z=-3.26 pero es ", z.equals(-3.26d));
	}
	
	
	@Test
	public void whenZPesoParaTalla_thenZ() {
		Double talla = 80.7d;
		Double l = 1.0d;
		Double m = 84.6403d;
		Double s = 0.03358d;
		
		Double z = UtilitarioMedico.calcularScoreZGivenValues(talla, l, m, s);
		Assert.assertTrue("Se espera un Z=-1.39 pero es " + z, z.equals(-1.39d));
	}
}

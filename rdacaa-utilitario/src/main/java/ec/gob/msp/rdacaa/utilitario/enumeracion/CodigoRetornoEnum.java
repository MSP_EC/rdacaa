package ec.gob.msp.rdacaa.utilitario.enumeracion;


public enum CodigoRetornoEnum {
    CORRECTO("1", "OK"),
    ERROR("2", "EXCEPCI\u00d3N"),
    USUARIO_INCORRECTO("3", "USUARIO Y/O CLAVE NO EXISTE"),
    INFORMACION_NO_EXISTE("4", "INFORMACI\u00d3N SOLICITADA NO EXISTE"),
    EXCEPCION_CONTROLADA("5", "EXCEPCI\u00d3N CONTROLADA");
    
    private String codigo;
    private String mensaje;

    private CodigoRetornoEnum(String codigo, String mensaje) {
        this.mensaje = mensaje;
        this.codigo = codigo;
    }

    public static final String validarRespuesta(String codigo, String mensajeEnviar) {
        String respuesta = "C\u00f3digo no v\u00e1lido";
        switch (codigo) {
            case "1": {
                respuesta = null;
                break;
            }
            case "2": {
                respuesta = ComunEnum.MENSAJE_ERROR_FALLO_OPERACION.getDescripcion();
                break;
            }
            case "4": {
                respuesta = mensajeEnviar;
                break;
            }
            case "5": {
                respuesta = mensajeEnviar;
                break;
            }
            default: {
                respuesta = "El servicio no esta disponible";
            }
        }
        return respuesta;
    }

    public String getCodigo() {
        return this.codigo;
    }

    public String getMensaje() {
        return this.mensaje;
    }
}
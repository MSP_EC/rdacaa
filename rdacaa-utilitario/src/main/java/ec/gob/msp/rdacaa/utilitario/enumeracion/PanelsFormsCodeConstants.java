/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.gob.msp.rdacaa.utilitario.enumeracion;

/**
 *
 * @author jazael.faubla
 */
public class PanelsFormsCodeConstants {
    
    private PanelsFormsCodeConstants() { }
    
    public static final String FORMAFRONTEND_PANEL_ADMISION = "PNL-ADM";
    public static final String FORMAFRONTEND_PANEL_GP = "PNL-GP";
    public static final String FORMAFRONTEND_PANEL_DANT = "PNL-DANT";
    public static final String FORMAFRONTEND_PANEL_OBST = "PNL-OBST";
    public static final String FORMAFRONTEND_PANEL_VIH = "PNL-VIH";
    public static final String FORMAFRONTEND_PANEL_VACU = "PNL-VACU";
    public static final String FORMAFRONTEND_PANEL_SIVAN = "PNL-SIVAN";
    public static final String FORMAFRONTEND_PANEL_EXLAB = "PNL-EXLAB";
    public static final String FORMAFRONTEND_PANEL_DIAG = "PNL-DIAG";
    public static final String FORMAFRONTEND_PANEL_PROC = "PNL-PROC";
    public static final String FORMAFRONTEND_PANEL_PRESC = "PNL-PRESC";
    public static final String FORMAFRONTEND_PANEL_ODONT = "PNL-ODONT";
    public static final String FORMAFRONTEND_PANEL_REFINT = "PNL-REFINT";
    public static final String FORMAFRONTEND_PANEL_VIOLENCIA = "PNL-VIOLE";
    public static final String FORMAFRONTEND_PANEL_GV = "PNL-VULNE";
}

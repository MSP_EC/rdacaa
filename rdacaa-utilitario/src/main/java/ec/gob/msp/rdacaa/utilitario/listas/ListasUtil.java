/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.gob.msp.rdacaa.utilitario.listas;

import java.io.PrintStream;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import ec.gob.msp.rdacaa.utilitario.file.Log;

public class ListasUtil {
    private ListasUtil() {
    }

    public static final String convertirListaACadena(Collection lista) {
        String cadena = "";
        for (Object obj : lista) {
            cadena = cadena + obj;
        }
        return cadena;
    }

    public static final Collection randomizarColeccion(Collection lista) {
        Collections.shuffle((List)lista);
        return lista;
    }

    public static final Collection convertirCadenaLista(String cadena) {
        ArrayList<String> lista = null;
        if (!cadena.isEmpty()) {
            lista = new ArrayList<String>();
            char[] arr$ = cadena.toCharArray();
            int len$ = arr$.length;
            for (int i$ = 0; i$ < len$; ++i$) {
                Character a = Character.valueOf(arr$[i$]);
                lista.add(a.toString());
            }
        }
        return lista;
    }

    public static final void ordenarLista(List lista, final String propiedad, boolean ascendente) {
        Collections.sort(lista, new Comparator(){

            public int compare(Object obj1, Object obj2) {
                Class clase = obj1.getClass();
                String getter = "get" + Character.toUpperCase(propiedad.charAt(0)) + propiedad.substring(1);
                try {
                    Method getPropiedad = clase.getMethod(getter, new Class[0]);
                    Object propiedad1 = getPropiedad.invoke(obj1, new Object[0]);
                    Object propiedad2 = getPropiedad.invoke(obj2, new Object[0]);
                    if (propiedad1 instanceof Comparable && propiedad2 instanceof Comparable) {
                        Comparable prop1 = (Comparable)propiedad1;
                        Comparable prop2 = (Comparable)propiedad2;
                        return prop1.compareTo(prop2);
                    }
                    if (propiedad1.equals(propiedad2)) {
                        return 0;
                    }
                    return 1;
                }
                catch (Exception e) {
                    Log.error(this.getClass().getName(), "no se ordena", e);
                    return 0;
                }
            }
        });
        if (!ascendente) {
            Collections.reverse(lista);
        }
    }

    public static String generarCadenaINNativeQuery(List<String> condiciones) {
        String resultado = "";
        for (String s : condiciones) {
            resultado = resultado.concat("'".concat(s).concat("'").concat(","));
        }
        resultado = resultado.substring(0, resultado.length() - 1);
        resultado = "(".concat(resultado).concat(")");
        return resultado;
    }

    public static int[] generarAleatorio(int maximo) {
        int MAX = maximo;
        int i = 0;
        int j = 0;
        int k = 0;
        int[] rand = new int[MAX];
        while (i < MAX) {
            rand[i] = i++;
        }
        for (i = 0; i < MAX; ++i) {
            j = (int)(Math.random() * (double)MAX);
            k = rand[i];
            rand[i] = rand[j];
            rand[j] = k;
        }
        return rand;
    }

    public static void main(String[] args) {
        System.out.println(Arrays.toString(ListasUtil.generarAleatorio(5)));
    }

    public static boolean listaDiferenteNullVacia(List lista) {
        return lista != null && !lista.isEmpty();
    }

}
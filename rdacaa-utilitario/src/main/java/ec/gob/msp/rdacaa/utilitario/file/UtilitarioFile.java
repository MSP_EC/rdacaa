/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.gob.msp.rdacaa.utilitario.file;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.filechooser.FileNameExtensionFilter;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.ErrorHandler;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;
import org.xml.sax.XMLReader;

/**
 *
 * @author dmurillo
 */
public class UtilitarioFile {

	private UtilitarioFile() {
	}

	public static boolean isXML(final File file) {

		byte[] buffer = new byte[(int) file.length()];
		try (FileInputStream fis = new FileInputStream(file)) {

			while (fis.read(buffer) > 0) {
				// Se carga en el buffer
			}

			SAXParserFactory factory = SAXParserFactory.newInstance();
			factory.setValidating(false);
			factory.setNamespaceAware(true);

			SAXParser parser = factory.newSAXParser();
			XMLReader reader = parser.getXMLReader();
			reader.setErrorHandler(new ErrorHandler() {

				@Override
				public void warning(SAXParseException e) throws SAXException {
					log(e);
				}

				@Override
				public void error(SAXParseException e) throws SAXException {
					log(e);
				}

				@Override
				public void fatalError(SAXParseException e) throws SAXException {
					log(e);
				}

				private void log(final Exception e) {
					Log.error("UtilitarioFile", "El documento no es un XML: ", e);
				}
			});
			reader.parse(new InputSource(new ByteArrayInputStream(buffer)));

		} catch (IOException | ParserConfigurationException | SAXException ex) {
			Logger.getLogger(UtilitarioFile.class.getName()).log(Level.SEVERE, null, ex);
			return false;
		}

		return true;
	}
        
}

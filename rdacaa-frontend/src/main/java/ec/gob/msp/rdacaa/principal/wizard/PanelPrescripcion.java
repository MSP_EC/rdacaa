/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.gob.msp.rdacaa.principal.wizard;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.github.cjwizard.WizardSettings;

import ec.gob.msp.rdacaa.amed.controller.PrescripcionController;
import ec.gob.msp.rdacaa.amed.forma.Prescripcion;
import ec.gob.msp.rdacaa.principal.controller.CommonPanelController;

/**
 *
 * @author eduardo
 */
@Component
public class PanelPrescripcion  extends com.github.cjwizard.WizardPage{
    
    private final Prescripcion prescripcion;
    private final PrescripcionController prescripcionController;
    private final CommonPanelController commonPanelController;
    
    @Autowired
    public PanelPrescripcion(Prescripcion prescripcion,PrescripcionController prescripcionController,CommonPanelController commonPanelController) {
        super("Registro de Prescripcion", "Atencion Médica");
        this.prescripcion = prescripcion;
        this.prescripcionController = prescripcionController;
        this.commonPanelController = commonPanelController;
    }
    
    @PostConstruct
    private void init() {
        prescripcion.setVisible(true);
        add(prescripcion);
    }
    
    @Override
    public boolean onNext(WizardSettings settings) {
        return prescripcionController.validateForm();
    }

    @Override
    public void updateSettings(WizardSettings settings) {
        commonPanelController.refreshPanels();
    }
    
}

package ec.gob.msp.rdacaa.reporte.controller;

import ec.gob.msp.rdacaa.principal.forma.EscritorioPrincipal;
import ec.gob.msp.rdacaa.reporte.forma.Atenciones;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.swing.JDesktopPane;
import javax.swing.JInternalFrame;
import javax.swing.JMenuItem;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Controller;

import ec.gob.msp.rdacaa.reporte.forma.ProduccionFechaEspecialidad;
import ec.gob.msp.rdacaa.reporte.forma.ReporteCaptacionVacunas;
import ec.gob.msp.rdacaa.reporte.forma.RegistroVacunas;
import ec.gob.msp.rdacaa.reporte.forma.RegistroVacunasDosis;
import ec.gob.msp.rdacaa.reporte.forma.ReporteNutricion;
import ec.gob.msp.rdacaa.reporte.forma.ReporteVacunas;
import ec.gob.msp.rdacaa.reporte.forma.Reportes;

@Controller
@Lazy(value = false)
public class ReportesController {

	private Reportes reportesFrame;
	private ProduccionFechaEspecialidad reporteProdAtencionesPanel;
	private RegistroVacunas registroVacunas;
	private Atenciones atenciones;
	private JDesktopPane reportesDesktopFrame;
	private ReporteCaptacionVacunas reporteCaptacionVacunas;
	private ReporteNutricion reporteNutricion;
	private RegistroVacunasDosis registroVacunasDosis;
	private ReporteVacunas reporteVacunas;

	@Autowired
	public ReportesController(Reportes reportesFrame, 
			ProduccionFechaEspecialidad produccionFechaEspecialidad,
			RegistroVacunas registroVacunas, Atenciones atenciones, 
			ReporteCaptacionVacunas reporteCaptacionVacunas,
			ReporteNutricion reporteNutricion,
			RegistroVacunasDosis registroVacunasDosis,
			ReporteVacunas reporteVacunas
			) {
		this.reportesFrame = reportesFrame;
		this.reporteProdAtencionesPanel = produccionFechaEspecialidad;
		this.registroVacunas = registroVacunas;
		this.atenciones = atenciones;
		this.reporteCaptacionVacunas = reporteCaptacionVacunas;
		this.reporteNutricion = reporteNutricion;
		this.registroVacunasDosis = registroVacunasDosis;
		this.reporteVacunas = reporteVacunas;
		this.reportesDesktopFrame = reportesFrame.getReportesDesktopFrame();
	}

	@PostConstruct
	private void init() {
		JMenuItem reporteProdAtenciones = reportesFrame.getReporteProdAtenciones();
		reportesDesktopFrame.add(reporteProdAtencionesPanel);
		reporteProdAtenciones.addActionListener(l -> showInternalFrame(reporteProdAtencionesPanel));
		JMenuItem reporteRegistroVacunas = reportesFrame.getReporteVacunas();
		reportesDesktopFrame.add(registroVacunas);
		reporteRegistroVacunas.addActionListener(l -> showInternalFrame(registroVacunas));
		JMenuItem reporteAtenciones = reportesFrame.getReporteAtenciones();
		reportesDesktopFrame.add(atenciones);
		reporteAtenciones.addActionListener(l -> showInternalFrame(atenciones));
		JMenuItem reporteCaptacionVac = reportesFrame.getReporteCaptacionVacunas();
		reportesDesktopFrame.add(reporteCaptacionVacunas);
		reporteCaptacionVac.addActionListener(l -> showInternalFrame(reporteCaptacionVacunas));
		JMenuItem reporteN = reportesFrame.getReporteNutricion();
		reportesDesktopFrame.add(reporteNutricion);
		reporteN.addActionListener(l -> showInternalFrame(reporteNutricion));
		JMenuItem reporteVacunasDosis = reportesFrame.getReporteVacunasDosis();
		reportesDesktopFrame.add(registroVacunasDosis);
		reporteVacunasDosis.addActionListener(l -> showInternalFrame(registroVacunasDosis));
		JMenuItem produccionVacunas = reportesFrame.getReporteVacunasTodo();
		reportesDesktopFrame.add(reporteVacunas);
		produccionVacunas.addActionListener(l -> showInternalFrame(reporteVacunas));
		JMenuItem mnuCerrar = reportesFrame.getMnuCerrar();
		mnuCerrar.addActionListener(l -> cerrar());
		mnuCerrar.addMouseListener(new MouseListener() {

			@Override
			public void mouseReleased(MouseEvent e) {
				// No se usa

			}

			@Override
			public void mousePressed(MouseEvent e) {
				// No se usa

			}

			@Override
			public void mouseExited(MouseEvent e) {
				// No se usa

			}

			@Override
			public void mouseEntered(MouseEvent e) {
				// No se usa

			}

			@Override
			public void mouseClicked(MouseEvent e) {
				cerrar();

			}
		});
	}

	public void showInternalFrame(JInternalFrame forma) {
		try {
			forma.setVisible(true);
                        forma.show();
			forma.setMaximum(true);
		} catch (Exception e) {
			Logger.getLogger(ReportesController.class.getName()).log(Level.SEVERE, null, e);
		}

	}

	public void cerrar() {
		reportesFrame.dispose();
		EscritorioPrincipal.dsp_contenedorRDACAA.remove(reportesFrame);
	}
}

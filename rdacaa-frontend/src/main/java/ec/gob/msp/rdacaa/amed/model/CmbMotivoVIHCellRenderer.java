package ec.gob.msp.rdacaa.amed.model;

import java.awt.Component;

import javax.swing.DefaultListCellRenderer;
import javax.swing.JList;

import ec.gob.msp.rdacaa.business.entity.Detallecatalogo;

public class CmbMotivoVIHCellRenderer extends DefaultListCellRenderer {

	private static final long serialVersionUID = -7690753132643460094L;
	@Override
	public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected,
			boolean cellHasFocus) {
		if (value instanceof Detallecatalogo) {
			Detallecatalogo detallecatalogo = (Detallecatalogo) value;
			value = detallecatalogo.getDescripcion();
		}
		return super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
	}
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.gob.msp.rdacaa.principal.forma;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.github.cjwizard.StackWizardSettings;
import com.github.cjwizard.WizardContainer;
import com.github.cjwizard.WizardListener;
import com.github.cjwizard.WizardPage;
import com.github.cjwizard.WizardSettings;
import com.github.cjwizard.pagetemplates.TitledPageTemplate;
import ec.gob.msp.rdacaa.amed.controller.AtencionMedicaController;
import ec.gob.msp.rdacaa.business.entity.Accesoformaespecialidad;

import ec.gob.msp.rdacaa.frontend.sesion.VariableSesion;
import ec.gob.msp.rdacaa.principal.controller.CommonPanelController;
import ec.gob.msp.rdacaa.principal.wizard.PanelAdmision;
import ec.gob.msp.rdacaa.principal.wizard.PanelDatosAntropometricos;
import ec.gob.msp.rdacaa.principal.wizard.PanelDatosSIVAN;
import ec.gob.msp.rdacaa.principal.wizard.PanelDiagnosticos;
import ec.gob.msp.rdacaa.principal.wizard.PanelGruposPrioritarios;
import ec.gob.msp.rdacaa.principal.wizard.PanelGruposVulnerables;
import ec.gob.msp.rdacaa.principal.wizard.PanelInformacionObstetrica;
import ec.gob.msp.rdacaa.principal.wizard.PanelOdontologia;
import ec.gob.msp.rdacaa.principal.wizard.PanelOrdenMedica;
import ec.gob.msp.rdacaa.principal.wizard.PanelPrescripcion;
import ec.gob.msp.rdacaa.principal.wizard.PanelProcedimientos;
import ec.gob.msp.rdacaa.principal.wizard.PanelReferencia;
import ec.gob.msp.rdacaa.principal.wizard.PanelVIH;
import ec.gob.msp.rdacaa.principal.wizard.PanelVacunas;
import ec.gob.msp.rdacaa.principal.wizard.PanelViolencia;
import ec.gob.msp.rdacaa.principal.wizard.WizardFactory;
import ec.gob.msp.rdacaa.seguridades.AutenticacionService;
import ec.gob.msp.rdacaa.utilitario.enumeracion.PanelsFormsCodeConstants;
import ec.gob.msp.rdacaa.utilitario.forma.UtilitarioForma;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.ArrayList;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;

/**
 *
 * @author miguel.faubla
 */
@Component
public class ContainerWizard extends javax.swing.JInternalFrame {

    private final Logger logs = LoggerFactory.getLogger(ContainerWizard.class);
    private final WizardFactory factory;
    private WizardContainer wc;
    private final VariableSesion variableSesion;
    private final AtencionMedicaController atencionMedicaController;
    private final CommonPanelController commonPanelController;
    private final AutenticacionService autenticacionService;
    private List<Accesoformaespecialidad> listPanelesFormaEsp = new ArrayList<>();
    List<String> optionListCodForm = new ArrayList<>();

    /**
     * Creates new form ContainerWizard
     *
     * @param factory
     * @param atencionMedicaController
     * @param commonPanelController
     * @param autenticacionService
     */
    @Autowired
    public ContainerWizard(
        WizardFactory factory, 
        AtencionMedicaController atencionMedicaController,
        CommonPanelController commonPanelController,
        AutenticacionService autenticacionService
    ) {
        initComponents();
        this.factory = factory;
        this.variableSesion = VariableSesion.getInstanciaVariablesSession();
        this.atencionMedicaController = atencionMedicaController;
        this.commonPanelController = commonPanelController;
        this.autenticacionService = autenticacionService;
        
//        loadWizardContainer();
    }

    public final void loadWizardContainer() {
    	this.getContentPane().removeAll();
        wc = new WizardContainer(factory, new TitledPageTemplate(), new StackWizardSettings());
        wc.addWizardListener(new WizardListener() {
            @Override
            public void onPageChanged(WizardPage newPage, List<WizardPage> path) {
                logs.debug("settings: {}", wc.getSettings());
                ContainerWizard.this.setTitle(newPage.getDescription());
                renderButtonsByEspecialidad(newPage);
                ContainerWizard.this.commonPanelController.initDatesVetoPolicy(newPage);
                ContainerWizard.this.commonPanelController.initCambioDPA(newPage);
            }

            @Override
            public void onFinished(List<WizardPage> path, WizardSettings settings) {
                logs.debug("settings: {}", wc.getSettings());
                int eleccion = JOptionPane.showConfirmDialog(null, "¿Estimado usuario esta seguro que desea finalizar la atención médica?", "RDACAA 2.0", JOptionPane.YES_NO_OPTION);
                if (eleccion == 0) {
                    boolean proceso;
                    try {
                        proceso = atencionMedicaController.finalizarAtencionMedica();
                        if (proceso) {
                            UtilitarioForma.muestraMensaje("", ContainerWizard.this, "INFO", "La atención médica se registró de manera exitosa");
                            commonPanelController.cleanInputsAllPanels();
                            variableSesion.setInstanciaNull();
                        } else {
                            UtilitarioForma.muestraMensaje("", ContainerWizard.this, "ERROR", "Hubo un error al registrar la atención médica");
                        }
                        EscritorioPrincipal.disableControlsAttentionFlow(true);
                        ContainerWizard.this.backToFirstPage(wc);
                        ContainerWizard.this.dispose();
                        EscritorioPrincipal.dsp_contenedorRDACAA.remove(ContainerWizard.this);
                    } catch (GeneralSecurityException | IOException e) {
                        logs.debug("Error: {}", e.getMessage());
                    }
                    
                }
            }

            @Override
            public void onCanceled(List<WizardPage> path, WizardSettings settings) {
                logs.debug("settings: {}", wc.getSettings());               
                commonPanelController.cleanInputsAllPanels();
                List<String> codeFormsPanels = variableSesion.getCodeFormsPanels();
                variableSesion.setInstanciaNull();
                variableSesion.setCodeFormsPanels(codeFormsPanels);
                EscritorioPrincipal.disableControlsAttentionFlow(true);
                ContainerWizard.this.backToFirstPage(wc);
                ContainerWizard.this.dispose();
                EscritorioPrincipal.dsp_contenedorRDACAA.remove(ContainerWizard.this);
            }
        });

        this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        JScrollPane scrPane = new JScrollPane(wc);
        scrPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
        scrPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);   
        this.getContentPane().add(scrPane);
        this.pack();
    }

    private void backToFirstPage(WizardContainer wc) {
        while (!(wc.getPath().size() - 1 == 0)) {
            wc.prev();
        }
    }
    
    public void disableFlowWizard(String typeUser) {
    	EscritorioPrincipal.btn_registroadmision.setVisible(false);
    	EscritorioPrincipal.btn_gruposprioritarios.setVisible(false);
    	EscritorioPrincipal.btn_gruposvulnerables.setVisible(false);
    	EscritorioPrincipal.btn_datosantropometricos.setVisible(false);
    	EscritorioPrincipal.btn_infoobstetrica.setVisible(false);
    	EscritorioPrincipal.btn_vihsida.setVisible(false);
    	EscritorioPrincipal.btn_diagnosticos.setVisible(false);
    	EscritorioPrincipal.btn_sivan.setVisible(false);
    	EscritorioPrincipal.btn_vacunacion.setVisible(false);
    	EscritorioPrincipal.btn_exameneslaboratorio.setVisible(false);
    	EscritorioPrincipal.btn_procedimientos.setVisible(false);
    	EscritorioPrincipal.btn_odontologia.setVisible(false);
    	EscritorioPrincipal.btn_prescripcion.setVisible(false);
    	EscritorioPrincipal.btn_referencia.setVisible(false);
    	EscritorioPrincipal.btn_datosviolencia.setVisible(false);
    	EscritorioPrincipal.btn_reportes.setVisible(false);
    	EscritorioPrincipal.btn_cierremes.setVisible(false);
    	EscritorioPrincipal.btn_validararchivo.setVisible(false);
		EscritorioPrincipal.btn_carga_pacientes.setVisible(false);
    	if(typeUser.equals("Invitado")) {
    		EscritorioPrincipal.btn_validararchivo.setVisible(true);
    	}
    	if(typeUser.equals("Cargapaciente")) {
    		EscritorioPrincipal.btn_carga_pacientes.setVisible(true);
    	}
    	if(typeUser.equals("Session")) {
    		EscritorioPrincipal.btn_carga_pacientes.setVisible(true);
    		EscritorioPrincipal.btn_reportes.setVisible(true);
        	EscritorioPrincipal.btn_cierremes.setVisible(true);
    	}
    }
    
    public void renderButtonsByInvitado() {
    	disableFlowWizard("Invitado");
    }
    
    public void renderButtonsByImportPacientes() {
    	disableFlowWizard("Cargapaciente");
    }
    
    public void renderButtonsByEspecialidad(WizardPage newPage) {
        if (EscritorioPrincipal.isEscritorioPrincipalInitialized()) {
        	listPanelesFormaEsp = new ArrayList<>();
        	optionListCodForm = new ArrayList<>();
            if(variableSesion.getCtEspecialidad() != null) {
                if(listPanelesFormaEsp.isEmpty()) {
                    listPanelesFormaEsp = autenticacionService.getPanelesFormaEspecialidad(variableSesion.getCtEspecialidad().getId());
                    if(!listPanelesFormaEsp.isEmpty()) {
                    	disableFlowWizard("Session");
                        EscritorioPrincipal.visibleControls();
                        listPanelesFormaEsp.forEach((Accesoformaespecialidad acceso) -> {
                            switch(acceso.getCodigoformafrontendId().getCodigo()) {
                                //Boton Acceso Admision
                                case PanelsFormsCodeConstants.FORMAFRONTEND_PANEL_ADMISION:
                                    EscritorioPrincipal.btn_registroadmision.setVisible(true);
                                break;
                                //Boton Acceso Grupos Prioritarios
                                case PanelsFormsCodeConstants.FORMAFRONTEND_PANEL_GP:
                                    EscritorioPrincipal.btn_gruposprioritarios.setVisible(true);
                                break;
                                //Boton Acceso Grupos Vulnerables
                                case PanelsFormsCodeConstants.FORMAFRONTEND_PANEL_GV:
                                    EscritorioPrincipal.btn_gruposvulnerables.setVisible(true);
                                break;
                                //Boton Acceso Datos Antropometricos
                                case PanelsFormsCodeConstants.FORMAFRONTEND_PANEL_DANT:
                                    EscritorioPrincipal.btn_datosantropometricos.setVisible(true);
                                break;
                                //Boton Acceso Datos Obstetricos
                                case PanelsFormsCodeConstants.FORMAFRONTEND_PANEL_OBST:
                                    EscritorioPrincipal.btn_infoobstetrica.setVisible(true);
                                break;
                                //Boton Acceso Datos VIH
                                case PanelsFormsCodeConstants.FORMAFRONTEND_PANEL_VIH:
                                    EscritorioPrincipal.btn_vihsida.setVisible(true);
                                break;
                                //Boton Acceso Datos Diagnostico
                                case PanelsFormsCodeConstants.FORMAFRONTEND_PANEL_DIAG:
                                    EscritorioPrincipal.btn_diagnosticos.setVisible(true);
                                break;
                                //Boton Acceso Datos SIVAN
                                case PanelsFormsCodeConstants.FORMAFRONTEND_PANEL_SIVAN:
                                    EscritorioPrincipal.btn_sivan.setVisible(true);
                                break;
                                //Boton Acceso Datos Vacunas
                                case PanelsFormsCodeConstants.FORMAFRONTEND_PANEL_VACU:
                                    EscritorioPrincipal.btn_vacunacion.setVisible(true);
                                break;
                                //Boton Acceso Datos Examenes Laboratorio
                                case PanelsFormsCodeConstants.FORMAFRONTEND_PANEL_EXLAB:
                                    EscritorioPrincipal.btn_exameneslaboratorio.setVisible(true);
                                break;
                                //Boton Acceso Datos Procedimientos
                                case PanelsFormsCodeConstants.FORMAFRONTEND_PANEL_PROC:
                                    EscritorioPrincipal.btn_procedimientos.setVisible(true);
                                break;
                                //Boton Acceso Datos Odontologia
                                case PanelsFormsCodeConstants.FORMAFRONTEND_PANEL_ODONT:
                                    EscritorioPrincipal.btn_odontologia.setVisible(true);
                                break;
                                //Boton Acceso Datos Prescripcion
                                case PanelsFormsCodeConstants.FORMAFRONTEND_PANEL_PRESC:
                                    EscritorioPrincipal.btn_prescripcion.setVisible(true);
                                break;
                                //Boton Acceso Datos Referencia
                                case PanelsFormsCodeConstants.FORMAFRONTEND_PANEL_REFINT:
                                    EscritorioPrincipal.btn_referencia.setVisible(true);
                                break;
                                //Boton Acceso Violencia
                                case PanelsFormsCodeConstants.FORMAFRONTEND_PANEL_VIOLENCIA:
                                    EscritorioPrincipal.btn_datosviolencia.setVisible(true);
                                break;
                                
                            }
                        });
                    }
                }
            }
            
            pageButtonDisable(newPage);
            
            if(!listPanelesFormaEsp.isEmpty()) {
                if(optionListCodForm.isEmpty()) {
                    listPanelesFormaEsp.forEach(objFormEsp -> {
                        optionListCodForm.add(objFormEsp.getCodigoformafrontendId().getCodigo());
                    });
                    variableSesion.setCodeFormsPanels(optionListCodForm);
                } 
            }
        }
    }

    public void pageButtonDisable(WizardPage newPage) {
        EscritorioPrincipal.disableControls(); 
        if (newPage instanceof PanelAdmision) {
            EscritorioPrincipal.btn_registroadmision.setEnabled(true);
        } else if (newPage instanceof PanelGruposPrioritarios) {
            EscritorioPrincipal.disableControlsCierre();
            EscritorioPrincipal.btn_gruposprioritarios.setEnabled(true);
        } else if (newPage instanceof PanelDatosAntropometricos) {
            EscritorioPrincipal.btn_datosantropometricos.setEnabled(true);
        } else if (newPage instanceof PanelGruposVulnerables) {
            EscritorioPrincipal.btn_gruposvulnerables.setEnabled(true);
        } else if (newPage instanceof PanelInformacionObstetrica) {
            EscritorioPrincipal.btn_infoobstetrica.setEnabled(true);
        } else if (newPage instanceof PanelVIH) {
            EscritorioPrincipal.btn_vihsida.setEnabled(true);
        } else if (newPage instanceof PanelDiagnosticos) {
            EscritorioPrincipal.btn_diagnosticos.setEnabled(true);
        } else if (newPage instanceof PanelDatosSIVAN) {
            EscritorioPrincipal.btn_sivan.setEnabled(true);
        } else if (newPage instanceof PanelVacunas) {
            EscritorioPrincipal.btn_vacunacion.setEnabled(true);
        } else if (newPage instanceof PanelOrdenMedica) {
            EscritorioPrincipal.btn_exameneslaboratorio.setEnabled(true);
        } else if (newPage instanceof PanelProcedimientos) {
            EscritorioPrincipal.btn_procedimientos.setEnabled(true);
        } else if (newPage instanceof PanelOdontologia) {
            EscritorioPrincipal.btn_odontologia.setEnabled(true);
        } else if (newPage instanceof PanelPrescripcion) {
            EscritorioPrincipal.btn_prescripcion.setEnabled(true);
        } else if (newPage instanceof PanelReferencia) {
            EscritorioPrincipal.btn_referencia.setEnabled(true);
        } else if (newPage instanceof PanelViolencia) {
            EscritorioPrincipal.btn_datosviolencia.setEnabled(true);
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    private void initComponents() {
        setResizable(true);
        setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        setDoubleBuffered(true);
    }
}

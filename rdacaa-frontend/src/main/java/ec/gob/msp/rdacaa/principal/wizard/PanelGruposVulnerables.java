/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.gob.msp.rdacaa.principal.wizard;

import com.github.cjwizard.WizardSettings;
import ec.gob.msp.rdacaa.amed.controller.GrupoVulnerableController;
import ec.gob.msp.rdacaa.amed.forma.GrupoVulnerable;
import ec.gob.msp.rdacaa.principal.controller.CommonPanelController;
import javax.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author jazael.faubla
 */
@Component
public class PanelGruposVulnerables extends com.github.cjwizard.WizardPage {

    private static final long serialVersionUID = -7446533763699088387L;
    
    private final GrupoVulnerable grupoVulnerable;
    private final CommonPanelController commonPanelController;
    private final GrupoVulnerableController grupoVulnerableController;
    
    @Autowired
    public PanelGruposVulnerables(GrupoVulnerable grupoVulnerable, CommonPanelController commonPanelController,
            GrupoVulnerableController grupoVulnerableController) {
        super("Registro de Grupo Vulnerables", "Atención Médica");
        this.grupoVulnerable = grupoVulnerable;
        this.grupoVulnerableController = grupoVulnerableController;
        this.commonPanelController = commonPanelController;
    }
    
    @PostConstruct
    private void init() {
        grupoVulnerable.setVisible(true);
        add(grupoVulnerable);
    }

//    @Override
//    public boolean onNext(WizardSettings settings) {
//        return grupoVulnerableController.validateForm();
//    }
    
    @Override
    public void updateSettings(WizardSettings settings) {
        commonPanelController.refreshPanels();
    }
    
}

package ec.gob.msp.rdacaa.amed.model;

import ec.gob.msp.rdacaa.business.entity.Cierreatenciones;
import ec.gob.msp.rdacaa.utilitario.forma.ModeloTablaGenerico;
import ec.gob.msp.rdacaa.utilitario.forma.UtilitarioForma;

public class CierreAtencionTablaModel extends ModeloTablaGenerico<Cierreatenciones> {

	public CierreAtencionTablaModel(String[] columnas) {
		super(columnas);
	}

	private static final long serialVersionUID = -6583859144518636066L;

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		Cierreatenciones obj = getListaDatos().get(rowIndex);
		switch (columnIndex) {
		case 0:
			return obj;
		case 1:
			return obj.getAnio();
		case 2:
			return UtilitarioForma.devolverDescripcionMes(obj.getMes());
		case 3:
			return UtilitarioForma.devolverDescripcionEstado(obj.getEstadocierre());
		case 4:
			return obj.getEstadocierre() == 0 ? "Exportar archivo .rdacaa" : "";
		default:
			return "";
		}
	}

	@Override
	public boolean isCellEditable(int row, int col) {
		return col == 4;
	}

	@Override
	public void setValueAt(Object value, int row, int col) {
		fireTableCellUpdated(row, col);
	}

}

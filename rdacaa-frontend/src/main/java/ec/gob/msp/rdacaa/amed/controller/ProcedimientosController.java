/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.gob.msp.rdacaa.amed.controller;

import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.imageio.ImageIO;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JTable;

import org.jdesktop.swingx.autocomplete.AutoCompleteDecorator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import ec.gob.msp.rdacaa.amed.forma.Procedimientos;
import ec.gob.msp.rdacaa.amed.model.AmedDetalleServicioTarifarioModel;
import ec.gob.msp.rdacaa.amed.model.AmedDetalleserviciotarifarioToStringCoverter;
import ec.gob.msp.rdacaa.amed.model.CmbDetalleserviciotarifarioCellRenderer;
import ec.gob.msp.rdacaa.amed.model.ProcedimientoActividadTablaModel;
import ec.gob.msp.rdacaa.business.entity.Detalleserviciotarifario;
import ec.gob.msp.rdacaa.business.entity.Procedimientoatencion;
import ec.gob.msp.rdacaa.business.service.DetalleserviciotarifarioService;
import ec.gob.msp.rdacaa.control.swing.personalizado.CajaTextoIess;
import ec.gob.msp.rdacaa.frontend.sesion.VariableSesion;
import ec.gob.msp.rdacaa.utilitario.forma.ButtonColumn;
import ec.gob.msp.rdacaa.utilitario.forma.FormatoTabla;
import ec.gob.msp.rdacaa.utilitario.forma.UtilitarioForma;

/**
 *
 * @author eduardo
 */
@Controller
public class ProcedimientosController {

    private final Procedimientos procedimientosFrame;
    private final DetalleserviciotarifarioService detalleserviciotarifarioService;
    private final AmedDetalleServicioTarifarioModel amedDetalleServicioTarifarioModel ;
    private JComboBox<Detalleserviciotarifario> cmbProcedimientos;
    private CajaTextoIess ctxCantidad;
    private JButton btnAgregarP;
    private JTable tbProcedimientos;
    private ProcedimientoActividadTablaModel procedimientoActividadTablaModel;
    private final List<Procedimientoatencion> listProcedimientos = new ArrayList<>();
    private final Object[] controles = new Object[2];
    private final VariableSesion variableSesion = VariableSesion.getInstanciaVariablesSession();
    private Detalleserviciotarifario detalleserviciotarifarioNull;
    private Procedimientoatencion procedimientoNull;
    private JLabel alertaRegistroTarifario;

    @Autowired
    public ProcedimientosController(Procedimientos procedimientosFrame,
                                    DetalleserviciotarifarioService detalleserviciotarifarioService) {
        this.procedimientosFrame = procedimientosFrame;
        this.detalleserviciotarifarioService = detalleserviciotarifarioService;
        this.amedDetalleServicioTarifarioModel = new AmedDetalleServicioTarifarioModel();
    }
    
    @PostConstruct
    private void init() {
        loadDefaultCombos();
        this.cmbProcedimientos = procedimientosFrame.getCmbProcedimientos();
        this.alertaRegistroTarifario = procedimientosFrame.getLblProcedimientosAlerta();
        this.btnAgregarP = procedimientosFrame.getBtnAgregarP();
        this.cmbProcedimientos.setRenderer(new CmbDetalleserviciotarifarioCellRenderer());
        AutoCompleteDecorator.decorate(cmbProcedimientos, new AmedDetalleserviciotarifarioToStringCoverter());
        loadProcedimientos();
        this.tbProcedimientos = procedimientosFrame.getTbProcedimientos();
        controles[0] = procedimientosFrame.getCmbProcedimientos();
        controles[1] = procedimientosFrame.getCtxCantidad();
        this.ctxCantidad = procedimientosFrame.getCtxCantidad();
        cargarTabla();
        btnAgregarP.addMouseListener (new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
                if(validateForm()) {
                    getProcedimientos();
                    cleanInput(null);
                }
            }
            @Override
            public void mousePressed(MouseEvent e) {
            	//No se usa
            }
            @Override
            public void mouseReleased(MouseEvent e) {
            	//No se usa            	
            }
            @Override
            public void mouseEntered(MouseEvent e) {
            	//No se usa            	
            }
            @Override
            public void mouseExited(MouseEvent e) {
            	//No se usa            	
            }
        });
        ctxCantidad.addKeyListener(new java.awt.event.KeyAdapter() {
            @Override
            public void keyTyped(java.awt.event.KeyEvent evt) {
                UtilitarioForma.soloNumeros(evt);
                UtilitarioForma.validarLongitudCampo(evt, 2);
            }
        });
        createAlertRegistroProcedimientos();
        
    }
    
    private void loadProcedimientos() {
        List<Detalleserviciotarifario> listaProcedimientos = detalleserviciotarifarioService.findAll();
        amedDetalleServicioTarifarioModel.clear();
        amedDetalleServicioTarifarioModel.addElements(listaProcedimientos);
        cmbProcedimientos.setModel(amedDetalleServicioTarifarioModel);
        cmbProcedimientos.setSelectedItem(detalleserviciotarifarioNull);
    }
    
    private void cargarTabla() {
        tbProcedimientos.setDefaultRenderer(Object.class, new FormatoTabla());
        String[] columns = {"Id","Procedimientos", "Actividades","Eliminar"};
        procedimientoActividadTablaModel = new ProcedimientoActividadTablaModel(columns);
        procedimientoActividadTablaModel.addFila(listProcedimientos.isEmpty() ? new ArrayList() : listProcedimientos);
        tbProcedimientos.setModel(procedimientoActividadTablaModel);
        if(!listProcedimientos.isEmpty()) {
            loadButtonDelete(tbProcedimientos, 3);
        }
        procedimientoActividadTablaModel.ocultaColumnaCodigo(tbProcedimientos);
        procedimientoActividadTablaModel.definirAnchoColumnas(tbProcedimientos ,new int[] { 300,35,25 }); 
    }
    
    public boolean validateForm(){
        boolean validarForma = UtilitarioForma.validarCamposRequeridos(controles);      
        return validarForma;
    }
    
    public void getProcedimientos() {
        Procedimientoatencion procedimientoatencion = new Procedimientoatencion();
        procedimientoatencion.setDetalleserviciotarifarioId(amedDetalleServicioTarifarioModel.getSelectedItem());
        if (ctxCantidad.getText().equals("")){
        procedimientoatencion.setNumactividades(0);
        }else{
            procedimientoatencion.setNumactividades(Integer.parseInt(ctxCantidad.getText()));
        }
        
        if(listProcedimientos.isEmpty()) {
            listProcedimientos.add(procedimientoatencion);
        } else {
            if(listProcedimientos.size() == 5) {
                        UtilitarioForma.muestraMensaje("", procedimientosFrame, "ERROR", "Estimado usuario le recordamos que puede ingresar un máximo de 5 Procedimientos, por favor verificar.");
                } else {
                        Optional<Procedimientoatencion> procedimiento = listProcedimientos.stream().filter(procd -> procd.getDetalleserviciotarifarioId().getId().equals(procedimientoatencion.getDetalleserviciotarifarioId().getId())).findFirst();
                        if(procedimiento.isPresent()) {
                            UtilitarioForma.muestraMensaje("", procedimientosFrame, "ERROR", "Estimado usuario el procedimiento ya ha sido agregado, por favor verificar.");
                         } else {
                            listProcedimientos.add(procedimientoatencion);
                        }
            }
        }
        variableSesion.setProcedimientoatencion(listProcedimientos);                 
        cargarTabla();
    }
   
    
    private void loadButtonDelete(JTable table, int column) {
        try {
            ButtonColumn buttonColumn = new ButtonColumn(table, createActionExport(), column);
            Image img = ImageIO.read(getClass().getResource("/principal/grupoprioritario.jpg"));
            buttonColumn.getTableCellRendererComponent(table, new ImageIcon(img), false,true, column, column);
        } catch (IOException ex) {
            Logger.getLogger(DiagnosticoCIEController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private Action createActionExport() {
        return new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                listProcedimientos.remove(tbProcedimientos.getSelectedRow());
                cargarTabla();
            }
        };
    }
    
    public void loadDefaultCombos(){
        detalleserviciotarifarioNull = new Detalleserviciotarifario(){
            private static final long serialVersionUID = 1L;
            boolean isNoSelectable = true;
        };
        detalleserviciotarifarioNull.setId(0);
        detalleserviciotarifarioNull.setDescripcion("Seleccione");
    }
     
    public void cleanInput(String panel) {
        if(panel != null && panel.equals("commons")) {
            listProcedimientos.clear();
            cargarTabla();
        }
        Object[] inputs = {cmbProcedimientos, ctxCantidad};
        UtilitarioForma.limpiarCampos(procedimientosFrame, inputs);
        cmbProcedimientos.setSelectedItem(detalleserviciotarifarioNull);
        ctxCantidad.setText(null);
    }
    
    
        public void createAlertRegistroProcedimientos() {
              String alerta=  "<html>"
                + "<body>"
                + "&nbsp; <span style=\"color: #0635A9;\">**Procedimientos para producción</span> <br/>"
                + "<body>"
                + "<html>";
              
              alertaRegistroTarifario.setText(alerta);
    }
}

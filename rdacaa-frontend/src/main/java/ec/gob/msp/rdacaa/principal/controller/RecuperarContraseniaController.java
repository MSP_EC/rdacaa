/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.gob.msp.rdacaa.principal.controller;

import ec.gob.msp.rdacaa.business.service.UsuarioService;
import ec.gob.msp.rdacaa.principal.forma.RecuperarContrasenia;
import ec.gob.msp.rdacaa.utilitario.forma.UtilitarioForma;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import javax.annotation.PostConstruct;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

/**
 *
 * @author eduardo
 */
@Controller
public class RecuperarContraseniaController {
    private JButton btnGenerarArchivo;
    private JButton btmCancelar;
    private JTextField txt_usuario;
    private RecuperarContrasenia recuperarContraseniaFrame;
    @Autowired
    private UsuarioService usuarioService;
    
@Autowired
    public RecuperarContraseniaController(RecuperarContrasenia recuperarContraseniaFrame) {
        this.recuperarContraseniaFrame = recuperarContraseniaFrame;
    }
    
@PostConstruct
    private void init(){
    this.txt_usuario = recuperarContraseniaFrame.getTxt_usuario();
    txt_usuario.setText("");
    this.btmCancelar = recuperarContraseniaFrame.getBtmCancelar();
    this.btnGenerarArchivo = recuperarContraseniaFrame.getBtmGenerarArchivo();
    btnGenerarArchivo.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
                recuperarContrasenia();
            }

            @Override
            public void mousePressed(MouseEvent e) {
            	//no se usa
            }

            @Override
            public void mouseReleased(MouseEvent e) {
            	//no se usa
            }

            @Override
            public void mouseEntered(MouseEvent e) {
            	//no se usa
            }

            @Override
            public void mouseExited(MouseEvent e) {
            	//no se usa
            }
        });
    
        txt_usuario.addKeyListener(new java.awt.event.KeyAdapter() {
            @Override
            public void keyTyped(java.awt.event.KeyEvent evt) {
                UtilitarioForma.validarLongitudCampo(evt, 10);
                UtilitarioForma.soloNumeros(evt);
            }
        });
    
        btmCancelar.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
                cerrarPantalla();
            }

            @Override
            public void mousePressed(MouseEvent e) {
            	//no se usa
            }

            @Override
            public void mouseReleased(MouseEvent e) {
            	//no se usa
            }

            @Override
            public void mouseEntered(MouseEvent e) {
            	//no se usa
            }

            @Override
            public void mouseExited(MouseEvent e) {
            	//no se usa
            }
        });
    }
    
    private void recuperarContrasenia(){
        
        
        
        JFileChooser fc=new JFileChooser();
        int seleccion=fc.showSaveDialog(recuperarContraseniaFrame);
            //Si el usuario, pincha en aceptar
            if(seleccion==JFileChooser.APPROVE_OPTION){
                //Seleccionamos el fichero
                File fichero=fc.getSelectedFile();
                try(FileWriter fw=new FileWriter(fichero)){
                    if (usuarioService.findOneByLogin(txt_usuario.getText()) != null ){
                        String contrasenia = usuarioService.findOneIdentificacionClave(txt_usuario.getText()).getPublickey();
                        if (contrasenia != null){
                            //Escribimos el texto en el fichero
                            fw.write(contrasenia);
                            UtilitarioForma.muestraMensaje("", recuperarContraseniaFrame, "INFO", "El archivo fue generado satifactoriamente");
                            txt_usuario.setText("");
                            SwingUtilities.getWindowAncestor(recuperarContraseniaFrame).dispose();
                        }else{
                            UtilitarioForma.muestraMensaje("", recuperarContraseniaFrame, "INFO", "El usuario aun no inicia sesion por primera vez. Contraseña vacía");
                        }
                    }else
                        {
                          UtilitarioForma.muestraMensaje("", recuperarContraseniaFrame, "INFO", "No se encuentra usuario "+ txt_usuario.getText());
                        }   
                fw.close();
                } catch (IOException e1) {
                    e1.printStackTrace();
                }

            }
    }
    
        public void cerrarPantalla(){
            SwingUtilities.getWindowAncestor(recuperarContraseniaFrame).dispose();
        }
}

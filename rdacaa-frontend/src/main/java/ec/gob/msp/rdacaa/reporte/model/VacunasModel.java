package ec.gob.msp.rdacaa.reporte.model;

import java.util.List;

import ec.gob.msp.rdacaa.business.entity.Atencionmedica;
import ec.gob.msp.rdacaa.business.entity.Persona;
import ec.gob.msp.rdacaa.business.entity.Registrovacunacion;
import lombok.Getter;
import lombok.Setter;

public class VacunasModel {
	@Getter
	@Setter
	private Persona persona;
	@Getter
	@Setter
	private Atencionmedica atencionMedica;
	@Getter
	@Setter
	private List<Registrovacunacion> registros;
}

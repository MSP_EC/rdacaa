package ec.gob.msp.rdacaa.reporte.controller;

import java.awt.Dimension;
import java.awt.Font;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.text.DecimalFormat;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JTable;
import javax.swing.event.InternalFrameAdapter;
import javax.swing.event.InternalFrameEvent;

import org.apache.commons.lang3.StringUtils;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.labels.PieSectionLabelGenerator;
import org.jfree.chart.labels.StandardPieSectionLabelGenerator;
import org.jfree.chart.plot.PiePlot;
import org.jfree.data.general.DefaultPieDataset;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Controller;

import com.github.lgooddatepicker.components.DatePicker;

import ec.gob.msp.rdacaa.business.entity.Atencionmedica;
import ec.gob.msp.rdacaa.business.entity.Persona;
import ec.gob.msp.rdacaa.business.service.AtencionmedicaService;
import ec.gob.msp.rdacaa.business.service.EntidadService;
import ec.gob.msp.rdacaa.business.service.IntraextramuralService;
import ec.gob.msp.rdacaa.business.service.PersonaService;
import ec.gob.msp.rdacaa.business.service.ReporteProduccionService;
import ec.gob.msp.rdacaa.business.service.VariablesDatosAntropometricosService;
import ec.gob.msp.rdacaa.business.service.VariablesPrescripcionService;
import ec.gob.msp.rdacaa.business.service.VariablesSivanCuestionarioService;
import ec.gob.msp.rdacaa.frontend.sesion.VariableSesion;
import ec.gob.msp.rdacaa.reporte.forma.ReporteNutricion;
import ec.gob.msp.rdacaa.reporte.model.NutricionReporte;
import ec.gob.msp.rdacaa.reporte.model.NutricionReporteModel;
import ec.gob.msp.rdacaa.reporte.model.TablaInterna;
import ec.gob.msp.rdacaa.reporte.model.TablaInternaModel;
import ec.gob.msp.rdacaa.reporte.utilitario.ReportesUtil;
import ec.gob.msp.rdacaa.seguridades.util.SeguridadUtil;
import ec.gob.msp.rdacaa.utilitario.enumeracion.ComunEnum;
import ec.gob.msp.rdacaa.utilitario.fecha.FechasUtil;
import ec.gob.msp.rdacaa.utilitario.forma.FormatoTabla;

@Controller
@Lazy(value = false)
public class ReporteNutricionController {

	private static final Logger logger = LoggerFactory.getLogger(ReporteNutricionController.class);
	private ReporteProduccionService reporteProduccionService;
	private AtencionmedicaService atencionmedicaService;
	private PersonaService personaService;
	private JButton btnBuscar;
	private JButton btnDescargar;
	private JTable tblReporte;
	private DatePicker dttDesde;
	private DatePicker dttHasta;
	private List<NutricionReporte> controles;
	private List<TablaInterna> controlesIntena;
	private ReporteNutricion reporteNutricionFrame;
	private JComboBox<String> tipoReporte;
	private List<Atencionmedica> atenciones;
	private static final String SALTO = "\n";
	private EntidadService entidadService; 
	private VariablesDatosAntropometricosService variablesDatosAntropometricosService;
	private VariablesSivanCuestionarioService variablesSivanCuestionarioService;
	private VariablesPrescripcionService variablesPrescripcionService;
        private IntraextramuralService intraextramuralService;
	private TablaInternaModel modeloTablaInterna;
	private NutricionReporteModel modeloTabla;
	private static final Integer EDADREPORTE = 50029;
	
	
	@Autowired
	public ReporteNutricionController(ReporteNutricion reporteNutricionFrame,
			                          ReporteProduccionService reporteProduccionService,
	                                  AtencionmedicaService atencionmedicaService,
	                                  PersonaService personaService,
	                                  EntidadService entidadService,
	                                  VariablesDatosAntropometricosService variablesDatosAntropometricosService,
	                                  VariablesSivanCuestionarioService variablesSivanCuestionarioService,
	                                  VariablesPrescripcionService variablesPrescripcionService,
                                          IntraextramuralService intraextramuralService
		                              ){
		this.reporteNutricionFrame = reporteNutricionFrame;
		this.reporteProduccionService = reporteProduccionService;
		this.atencionmedicaService = atencionmedicaService;
		this.personaService = personaService;
		this.entidadService = entidadService;
		this.variablesDatosAntropometricosService = variablesDatosAntropometricosService;
		this.variablesSivanCuestionarioService = variablesSivanCuestionarioService;
		this.variablesPrescripcionService = variablesPrescripcionService;
                this.intraextramuralService = intraextramuralService;
	}
	
	@PostConstruct
	private void init() {
		this.dttDesde = reporteNutricionFrame.getDttDesde();
		this.dttHasta = reporteNutricionFrame.getDttHasta();
		this.btnBuscar = reporteNutricionFrame.getBtnBuscar();
		this.btnDescargar = reporteNutricionFrame.getBtnDescargar();
		this.tblReporte = reporteNutricionFrame.getTblReporte();
		this.tipoReporte = reporteNutricionFrame.getCmbTipo();

		btnBuscar.addActionListener(e -> consultar());
		btnDescargar.addActionListener(e -> exportarXLS());
		cargarTabla();
		reporteNutricionFrame.addInternalFrameListener( new  InternalFrameAdapter() {
			@Override
			public void internalFrameClosing(InternalFrameEvent e) {
				super.internalFrameClosing(e);
				cerrar();
			}
		});
	}
	
	private void consultar() {
		if (dttDesde.getDate() != null && dttHasta.getDate() != null) {
			ejecutarConsulta();
		}
	}
	
	private void cargarTabla() {

		tblReporte.setDefaultRenderer(Object.class, new FormatoTabla());
		String[] columnNames = { "", "Codigo", "Establecimiento", "Profesional", "Especialidad", "Cédula", "Nombres Paciente",
				"Apellidos Paciente", "Fecha Nacimiento Paciente", "Sexo", "País de Nacimiento Paciente", "Provincia Residencia Paciente",
				"Cantón Residencia Paciente", "Parroquia Residencia Paciente", "Zona Urbano Rural Establecimiento",
				"Barrio", "Calle Principal","Calle Secundaria","Fecha Toma de datos", "Edad años", "Edad meses", "Edad días" ,
				"Peso (Kg)","Longitud-Estatura (cm)","Hemoglobina (g/dL)","Talla para la edad Z","Peso para la edad Z",
				"IMC para la edad Z","Indicador: Anemia Categorias","Indicador: Talla/Edad Categorias","Indicador: Peso/Edad Categorias",
				"Indicador: IMC/edad Categorias","Recibió hierro, multivitaminas y minerales en polvo?","Recibió vitamina A",
				"Recibió LECHE MATERNA? (0 a 5 meses)"," Comió algún ALIMENTO SÓLIDO O SEMISÓLIDO? (6 a 8 meses)","Lugar toma de datos"
				};
		modeloTabla = new NutricionReporteModel(columnNames);
		controles = controles == null ? new ArrayList<>() : controles;
		modeloTabla.addFila(controles);
		tblReporte.setModel(modeloTabla);
		tblReporte.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		modeloTabla.ocultaColumnaCodigo(tblReporte);
		modeloTabla.definirAnchoColumnas(tblReporte,definirTamano());
	}
	
	private void cargarTablaInterna() {
		tblReporte.setDefaultRenderer(Object.class, new FormatoTabla());
		String[] columnNames = { "", "Indicador", "# de atenciones", "%"};
		modeloTablaInterna = new TablaInternaModel(columnNames);
		controlesIntena = controlesIntena == null ? new ArrayList<>() : controlesIntena;
		modeloTablaInterna.addFila(controlesIntena);
		tblReporte.setModel(modeloTablaInterna);
		tblReporte.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		modeloTablaInterna.ocultaColumnaCodigo(tblReporte);
	}
	
	private void exportarXLS() {
		String autor = VariableSesion.getInstanciaVariablesSession().getUsuario().getPersonaId().getPrimernombre()
				+ " ";
		autor += VariableSesion.getInstanciaVariablesSession().getUsuario().getPersonaId().getPrimerapellido() + " ";
		StringBuilder fechas = new StringBuilder();
		fechas.append("DESDE: ").append(dttDesde.getDateStringOrEmptyString()).append(" HASTA: ").append(dttHasta.getDateStringOrEmptyString());
		
                String tituloReporte = tipoReporte.getSelectedItem().toString();
                if(tipoReporte.getSelectedItem().equals("Detalle")){
                    tituloReporte = "Reporte Nutricional General";
                }else{
                    tituloReporte = "Reporte Nutricional " + tituloReporte;
                }
                
		ReportesUtil.exportarXlsReportes(tblReporte, "CaptacionVacunas", autor, "xls", tituloReporte , fechas.toString() );
	}
	
	private void ejecutarConsulta() {
		
		String desde = dttDesde.getDateStringOrEmptyString();
		String hasta = dttHasta.getDateStringOrEmptyString();
		NutricionReporte nutricionReporte;
		controles = new ArrayList<>();
		atenciones = atencionmedicaService.findAllAtencionesByDates(
				VariableSesion.getInstanciaVariablesSession().getProfesional().getId(),
				Date.from(dttDesde.getDate().atStartOfDay(ZoneId.systemDefault()).toInstant()),
				Date.from(dttHasta.getDate().atStartOfDay(ZoneId.systemDefault()).toInstant()),
				VariableSesion.getInstanciaVariablesSession().getCtEspecialidad().getId());
		
		String co = null;
		Persona paciente = new Persona();
		List<Atencionmedica> atencionesNinos = new ArrayList<>();
		if (!atenciones.isEmpty()) {
			for (Atencionmedica atencionmedica : atenciones) {
				try {
					co = SeguridadUtil.descifrarString(atencionmedica.getKsCident(),
							VariableSesion.getInstanciaVariablesSession().getUtilitario(),
							VariableSesion.getInstanciaVariablesSession().getUsuario().getLogin());
					paciente = personaService.findOneByPersonaUUID(co);
				} catch (GeneralSecurityException e) {
					logger.error("Error GeneralSecurityException {} ", e.getMessage());
				} catch (IOException e) {
					logger.error("Error IOException {} ", e.getMessage());
				}
				int anios = FechasUtil.getDiffDatesDesdeHasta(paciente.getFechanacimiento(),atencionmedica.getFechaatencion(),0);
				int meses = FechasUtil.getDiffDatesDesdeHasta(paciente.getFechanacimiento(),atencionmedica.getFechaatencion(),1);
				int dias = FechasUtil.getDiffDatesDesdeHasta(paciente.getFechanacimiento(),atencionmedica.getFechaatencion(),2);
				
				if(concatenarEdad(anios, meses, dias) <= EDADREPORTE){
					atencionesNinos.add(atencionmedica);
				}	
			}
			atenciones = atencionesNinos;
		}
		if (!atenciones.isEmpty()) {
			if(tipoReporte.getSelectedItem().equals("Detalle")){
				controles = new ArrayList<>();
				for (Atencionmedica atencion : atenciones) {
					nutricionReporte = new NutricionReporte();
					nutricionReporte.setEntidad(entidadService.findOneByEntidadId(atencion.getEntidadId()));
					nutricionReporte.setProfesional(atencion.getUsuariocreacionId());
					nutricionReporte.setAtencionmedica(atencion);
					try {
						co = SeguridadUtil.descifrarString(atencion.getKsCident(),
								VariableSesion.getInstanciaVariablesSession().getUtilitario(),
								VariableSesion.getInstanciaVariablesSession().getUsuario().getLogin());
						nutricionReporte.setPaciente(personaService.findOneByPersonaUUID(co));
					} catch (GeneralSecurityException e) {
						logger.error("Error GeneralSecurityException {} ", e.getMessage());
					} catch (IOException e) {
						logger.error("Error IOException {} ", e.getMessage());
					}
					nutricionReporte.setDa(variablesDatosAntropometricosService.findOneByAtencionId(atencion.getId()));
					nutricionReporte.setSc(variablesSivanCuestionarioService.findOneByAtencionId(atencion.getId()));
					nutricionReporte.setVp(variablesPrescripcionService.findOneByAtencionId(atencion.getId()));
                                        nutricionReporte.setLugarAtencion(intraextramuralService.findIntraextramuralByAtencionId(atencion.getId()));
					controles.add(nutricionReporte);
				}
				cargarTabla();
			}else{
				List<String> filtro = new ArrayList<>();
				for (Atencionmedica atencionmedica : atenciones) {
					filtro.add(atencionmedica.getId().toString());
				}
				
				Object[] datos = null;
				try {
					if(tipoReporte.getSelectedItem().equals("Anemia")){
						datos = reporteProduccionService.findNuticionAnemiaByFechasAndEstablecimientoId(desde, hasta, VariableSesion.getInstanciaVariablesSession().getEstablecimientoSalud().getId(), VariableSesion.getInstanciaVariablesSession().getCtEspecialidad().getId(), filtro);
					}else if(tipoReporte.getSelectedItem().equals("Talla Edad")){
						datos = reporteProduccionService.findNuticionTallaEdadByFechasAndEstablecimientoId(desde, hasta, VariableSesion.getInstanciaVariablesSession().getEstablecimientoSalud().getId(), VariableSesion.getInstanciaVariablesSession().getCtEspecialidad().getId(), filtro);
					}else if(tipoReporte.getSelectedItem().equals("Peso Talla")){
						datos = reporteProduccionService.findNuticionPesoTallaByFechasAndEstablecimientoId(desde, hasta, VariableSesion.getInstanciaVariablesSession().getEstablecimientoSalud().getId(), VariableSesion.getInstanciaVariablesSession().getCtEspecialidad().getId(), filtro);
					}else if(tipoReporte.getSelectedItem().equals("Peso edad")){
						datos = reporteProduccionService.findNuticionPesoEdadByFechasAndEstablecimientoId(desde, hasta, VariableSesion.getInstanciaVariablesSession().getEstablecimientoSalud().getId(), VariableSesion.getInstanciaVariablesSession().getCtEspecialidad().getId(), filtro);
					}else if(tipoReporte.getSelectedItem().equals("IMC Edad")){
						datos = reporteProduccionService.findNuticionIMCEdadByFechasAndEstablecimientoId(desde, hasta, VariableSesion.getInstanciaVariablesSession().getEstablecimientoSalud().getId(), VariableSesion.getInstanciaVariablesSession().getCtEspecialidad().getId(), filtro);
					}
				} catch (Exception e) {
					datos = null;
				}
				controlesIntena = new ArrayList<>();
				
				if(datos != null){
					int totalAnemia = 0;
					TablaInterna ti;
					for (int i = 0; i < datos.length; i++) {
						Object[] fila = (Object[]) datos[i];
						ti = new TablaInterna();
						ti.setNombre(fila[1].toString());
						ti.setCantidad(Integer.valueOf(fila[0].toString()));
						ti.setPorcentaje( Double.valueOf(fila[0].toString()) * Double.valueOf(100) / Double.valueOf(atenciones.size()));
						controlesIntena.add(ti);
						totalAnemia = totalAnemia + Integer.valueOf(fila[0].toString());
					}
					if(Integer.valueOf((atenciones.size() - totalAnemia)) != 0){
						controlesIntena.add(new TablaInterna("", Integer.valueOf((atenciones.size() - totalAnemia)), (Double.valueOf((atenciones.size() - totalAnemia)) * Double.valueOf(100) / Double.valueOf(atenciones.size())) ));
					}
					controlesIntena.add(new TablaInterna("TOTAL ", (atenciones.size()), 100.0));
				}
				generateGraph(datos);
				cargarTablaInterna();
			}
		}

	}
	private int[] definirTamano(){
		int[] medida = new int[37];
		for (int i = 0; i < medida.length; i++) {
			medida[i] = 300;
		}
		return medida;
	}
	
	private void generateGraph(Object[] datos){
		StringBuilder cabecera = new StringBuilder();
		cabecera.append("MINISTERIO DE SALUD PÚBLICA");
		cabecera.append(SALTO);
		cabecera.append("REPORTE: "+tipoReporte.getSelectedItem().toString());
		cabecera.append(SALTO);
		cabecera.append("FECHA DE GENERACIÓN: "+ FechasUtil.formateadorfechaAString(ComunEnum.PATRON_FECHA6.getDescripcion(), new Date()));
		cabecera.append(SALTO);
		cabecera.append(VariableSesion.getInstanciaVariablesSession().getEstablecimientoSalud().getNombreoficial());
		cabecera.append(SALTO);
		cabecera.append("GENERADO POR: "+VariableSesion.getInstanciaVariablesSession().getProfesional().getPrimernombre()+" "
		               +VariableSesion.getInstanciaVariablesSession().getProfesional().getPrimerapellido() );
		cabecera.append(SALTO);
		cabecera.append("PERIODO");
		cabecera.append(SALTO);
		cabecera.append("DESDE: "+dttDesde.getDateStringOrEmptyString()+" HASTA: "+dttHasta.getDateStringOrEmptyString());
		
		
		DefaultPieDataset dataset = new DefaultPieDataset();
		
		if (datos != null && datos.length > 0 ) {
			double totalAnemia = 0;
			for (int i = 0; i < datos.length; i++) {
				Object[] fila = (Object[]) datos[i];
//				dataset.setValue(fila[1].toString(), (Double.valueOf(fila[0].toString()) * 100) / atenciones.size());
                                dataset.setValue(fila[1].toString(), Double.valueOf(fila[0].toString()));
				totalAnemia = totalAnemia + Integer.valueOf(fila[0].toString());
			}
			
//			dataset.setValue("Sin datos", ((atenciones.size() - totalAnemia) * 100) / atenciones.size());
                        dataset.setValue("Sin datos", (atenciones.size() - totalAnemia));
                        
                        PieSectionLabelGenerator gen = new StandardPieSectionLabelGenerator("{0}: {1} ({2})", new DecimalFormat("0"), new DecimalFormat("0%"));
			
			JFreeChart someChart = ChartFactory.createPieChart3D(
					cabecera.toString(), 
					dataset, 
					true, 
					true, 
					false);
			Font customFont = new Font("SansSerif", Font.PLAIN, 12);
            customFont = customFont.deriveFont(12f);
            someChart.getTitle().setFont(customFont);
            
                        PiePlot plot = (PiePlot) someChart.getPlot();
                        plot.setSimpleLabels(true);
                        plot.setLabelGenerator(gen);

		        JFrame f = new JFrame(tipoReporte.getSelectedItem().toString());
		        f.setSize(600, 400);
		        f.add(new ChartPanel(someChart) {
					private static final long serialVersionUID = 582370765272457924L;

					@Override
		            public Dimension getPreferredSize() {
		                return new Dimension(600, 400);
		            }
		        });
		        f.pack();
		        f.setLocationRelativeTo(null);
		        f.setVisible(true);	
		}

	}
	private void cerrar(){
		
		dttDesde.setText("");
		dttHasta.setText("");
		
		if(modeloTablaInterna != null){
			modeloTablaInterna.clear();
		}
		if(modeloTabla != null){
			modeloTabla.clear();
		}
		
	}
	
	private  Integer concatenarEdad(Integer edadAnios, Integer edadMeses, Integer edadDias) {
    	String edadString = StringUtils.leftPad(edadAnios.toString(), 3, "0") + StringUtils.leftPad(edadMeses.toString(), 2, "0") + StringUtils.leftPad(edadDias.toString(), 2, "0");
    	return Integer.parseInt(edadString);
	}
}

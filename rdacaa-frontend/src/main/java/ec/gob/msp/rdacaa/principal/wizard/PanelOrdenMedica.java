/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.gob.msp.rdacaa.principal.wizard;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.github.cjwizard.WizardSettings;

import ec.gob.msp.rdacaa.amed.controller.OrdenMedicaController;
import ec.gob.msp.rdacaa.amed.forma.OrdenMedica;
import ec.gob.msp.rdacaa.principal.controller.CommonPanelController;

/**
 *
 * @author eduardo
 */
@Component
public class PanelOrdenMedica extends com.github.cjwizard.WizardPage {

	private static final long serialVersionUID = -7209042336870332106L;
	private OrdenMedica ordenMedica;
	private OrdenMedicaController ordenMedicaController;
	private CommonPanelController commonPanelController;

	@Autowired
	public PanelOrdenMedica(OrdenMedica ordenMedica, OrdenMedicaController ordenMedicaController,
			CommonPanelController commonPanelController) {
		super("Registro de Examenes - Ordenes Médicas ", "Atencion Médica");
		this.ordenMedica = ordenMedica;
		this.ordenMedicaController = ordenMedicaController;
		this.commonPanelController = commonPanelController;
	}

	@PostConstruct
	private void init() {
		ordenMedica.setVisible(true);
		add(ordenMedica);
	}

	@Override
	public boolean onNext(WizardSettings settings) {
		return ordenMedicaController.validateForm();
	}

	@Override
	public void updateSettings(WizardSettings settings) {
		commonPanelController.refreshPanels();
	}
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.gob.msp.rdacaa.principal.wizard;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.github.cjwizard.WizardSettings;

import ec.gob.msp.rdacaa.amed.controller.VIHController;
import ec.gob.msp.rdacaa.amed.forma.VIH;
import ec.gob.msp.rdacaa.principal.controller.CommonPanelController;

/**
 *
 * @author jazael.faubla
 */
@Component
public class PanelVIH extends com.github.cjwizard.WizardPage {
    
	private static final long serialVersionUID = 1L;
	private final VIH vih;
    private final VIHController vihController;
    private final CommonPanelController commonPanelController;
    
    @Autowired
    public PanelVIH(VIH vih, VIHController vihController, CommonPanelController commonPanelController) {
        super("Registro de VIH", "Atención Médica");
        this.vih = vih;
        this.vihController = vihController;
        this.commonPanelController = commonPanelController;
    }
    
    @PostConstruct
    private void init() {
        vih.setVisible(true);
        add(vih);
    }
    
    @Override
    public boolean onNext(WizardSettings settings) {  
        return vihController.validateForm();    
    }
    
    @Override
    public void updateSettings(WizardSettings settings) {
        commonPanelController.refreshPanels();
    }
    
}

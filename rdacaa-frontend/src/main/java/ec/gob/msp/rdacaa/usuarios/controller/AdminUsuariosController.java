/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.gob.msp.rdacaa.usuarios.controller;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.time.Instant;
import java.time.ZoneId;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.github.lgooddatepicker.components.DatePicker;

import ec.gob.msp.rdacaa.admision.model.CmbDetalleCatalogoCellRenderer;
import ec.gob.msp.rdacaa.business.entity.Detallecatalogo;
import ec.gob.msp.rdacaa.business.entity.Persona;
import ec.gob.msp.rdacaa.business.service.DetallecatalogoService;
import ec.gob.msp.rdacaa.business.service.PersonaService;
import ec.gob.msp.rdacaa.frontend.sesion.VariableSesion;
import ec.gob.msp.rdacaa.principal.forma.EscritorioPrincipal;
import ec.gob.msp.rdacaa.usuarios.forma.AdminUsuarios;
import ec.gob.msp.rdacaa.usuarios.model.UsuariosCmbFormacionProfesionalModel;
import ec.gob.msp.rdacaa.utilitario.forma.UtilitarioForma;

/**
 *
 * @author eduardo
 */
@Controller
public class AdminUsuariosController {
    
    private AdminUsuarios adminUsuariosFrame;
    
    private DetallecatalogoService detallecatalogoService;
    private UsuariosCmbFormacionProfesionalModel usuariosCmbFormacionProfesionalModel;
    private JComboBox<Detallecatalogo> cmbFormacionProfesional;
    private PersonaService personaService;
    private Persona persona;
    private DatePicker dttFechaNacimiento;
    private JTextField ctxNumeroIdentificacion;
    private JTextField ctxEstado;
    private JTextField ctxNumeroRegistro;
    private JTextField ctxPrimerApellido;
    private JTextField ctxPrimerNombre;
    private JTextField ctxSegundoApellido;
    private JTextField ctxSegundoNombre;
    private JTextField ctxSexo;
    private JTextField ctxTipoIdentificacion;
    private JTextField ctxTipoPersona;
    private JButton btnGuardar;
    private JButton btnCancelar;
    private Object[] controlesValidar = new Object[17];
    
    
    @Autowired
    public AdminUsuariosController(AdminUsuarios adminUsuariosFrame, 
                                    DetallecatalogoService detallecatalogoService,
                                    PersonaService personaService) {
        this.adminUsuariosFrame = adminUsuariosFrame;
        this.detallecatalogoService = detallecatalogoService;
        this.usuariosCmbFormacionProfesionalModel = new UsuariosCmbFormacionProfesionalModel();
        this.personaService = personaService;
    }
    
    @PostConstruct
    private void init() {
         this.btnGuardar = adminUsuariosFrame.btnGuardar;
         this.btnCancelar = adminUsuariosFrame.btnCancelar;
        this.ctxNumeroIdentificacion = adminUsuariosFrame.getCtxNumeroIdentificacion();
        this.ctxEstado = adminUsuariosFrame.getCtxEstado();
        this.ctxNumeroRegistro = adminUsuariosFrame.getCtxNumeroRegistro();
        this.ctxPrimerApellido = adminUsuariosFrame.getCtxPrimerApellido();
        this.ctxPrimerNombre = adminUsuariosFrame.getCtxPrimerNombre();
        this.ctxSegundoApellido = adminUsuariosFrame.getCtxSegundoApellido();
        this.ctxSegundoNombre = adminUsuariosFrame.getCtxSegundoNombre();
        this.ctxSexo = adminUsuariosFrame.getCtxSexo();
        this.ctxTipoIdentificacion = adminUsuariosFrame.getCtxTipoIdentificacion();
        this.ctxTipoPersona = adminUsuariosFrame.getCtxTipoPersona();
        this.cmbFormacionProfesional = adminUsuariosFrame.getCmb_FormacionProfesional();
        this.dttFechaNacimiento = adminUsuariosFrame.getDttFechaNacimiento();
        loadFormacionProfesional();

        btnGuardar.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
               validateForm();
            }

            @Override
            public void mousePressed(MouseEvent e) {
            	//No usado
            }

            @Override
            public void mouseReleased(MouseEvent e) {
            	//No usado
            }

            @Override
            public void mouseEntered(MouseEvent e) {
            	//No usado
            }

            @Override
            public void mouseExited(MouseEvent e) {
            	//No usado
            }
        });
        
        btnCancelar.addMouseListener(new MouseListener() {
             @Override
             public void mouseClicked(MouseEvent e) {
                 adminUsuariosFrame.dispose();
                 EscritorioPrincipal.dsp_contenedorRDACAA.remove(adminUsuariosFrame);
             }

             @Override
             public void mousePressed(MouseEvent e) {
            	//No usado
             }

             @Override
             public void mouseReleased(MouseEvent e) {
            	//No usado
             }

             @Override
             public void mouseEntered(MouseEvent e) {
            	//No usado
             }

             @Override
             public void mouseExited(MouseEvent e) {
            	//No usado
             }
         });
    }
    
    
    private void loadFormacionProfesional(){
        List<Detallecatalogo> formacionProfesional = detallecatalogoService.findAllFormacionProfesional();
        usuariosCmbFormacionProfesionalModel.clear();
        usuariosCmbFormacionProfesionalModel.addElements(formacionProfesional);
        cmbFormacionProfesional.setRenderer(new CmbDetalleCatalogoCellRenderer());
        cmbFormacionProfesional.setModel(usuariosCmbFormacionProfesionalModel);
        cmbFormacionProfesional.setSelectedItem(null);
        
    }
     public void cargaDatosUsuario(){

        if (VariableSesion.getInstanciaVariablesSession().getProfesional() != null ){
         persona = VariableSesion.getInstanciaVariablesSession().getProfesional();
        
            if(persona.getEstado() != null){
                ctxSexo.setEnabled(false);
                if (persona.getEstado().toString().equals("0")){
                
                    ctxEstado.setText("Activo");
                    
                }else 
                    ctxEstado.setText("Inactivo");
            }
            if(persona.getFechanacimiento() != null ){
                dttFechaNacimiento.setEnabled(false);
                dttFechaNacimiento.setDate(Instant.ofEpochMilli(persona.getFechanacimiento().getTime()).atZone(ZoneId.systemDefault()).toLocalDate());
            }
            if(persona.getNumeroarchivo() != null){
                ctxNumeroRegistro.setEnabled(false);
                ctxNumeroRegistro.setText(persona.getNumeroarchivo());
            }
            if(persona.getPrimernombre() != null){
                ctxPrimerNombre.setEnabled(false);
                ctxPrimerNombre.setText(persona.getPrimernombre());
            }
            if(persona.getPrimerapellido() != null){
                ctxPrimerApellido.setEnabled(false);
                ctxPrimerApellido.setText(persona.getPrimerapellido());
            }
            if(persona.getSegundoapellido() != null){
                ctxSegundoApellido.setEnabled(false);
                ctxSegundoApellido.setText(persona.getSegundoapellido());
            }
            if(persona.getSegundonombre() != null){
                ctxSegundoNombre.setEnabled(false);
                ctxSegundoNombre.setText(persona.getSegundonombre());
            }
            if(persona.getCtsexoId() != null){
                ctxSexo.setEnabled(false);
                ctxSexo.setText(persona.getCtsexoId().getDescripcion());
            }
            if(persona.getCttipoidentificacionId() != null){
                ctxTipoIdentificacion.setEnabled(false);
                ctxTipoIdentificacion.setText(persona.getCttipoidentificacionId().getDescripcion());
            }
            if(persona.getCttipopersonaId() != null){
                ctxTipoPersona.setEnabled(false);
                ctxTipoPersona.setText(persona.getCttipopersonaId().getDescripcion());
            }
            if(persona.getNumeroidentificacion() != null){
                ctxNumeroIdentificacion.setEnabled(false);
                ctxNumeroIdentificacion.setText(persona.getNumeroidentificacion());
            }
            if(persona.getCtformacionprofId() != null){
                cmbFormacionProfesional.setSelectedItem(detallecatalogoService.findOneByDetalleId(persona.getCtformacionprofId().getId()));
            }
            
            VariableSesion.getInstanciaVariablesSession().setProfesional(persona);
        
        }
    }
     
     
     public boolean validateForm(){
        boolean validarForma;
        validarForma = UtilitarioForma.validarCamposRequeridos(controlesValidar);
        if(validarForma && VariableSesion.getInstanciaVariablesSession().getProfesional() != null){
            persona = VariableSesion.getInstanciaVariablesSession().getProfesional();
            persona.setCtformacionprofId(usuariosCmbFormacionProfesionalModel.getSelectedItem());
            personaService.saveAndFlush(persona);
              JOptionPane.showMessageDialog(null, "¡Registro actualizado exitosamente!","Actualizacion Infomacion Usuario", JOptionPane.INFORMATION_MESSAGE);
        }
       return validarForma;
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.gob.msp.rdacaa.amed.controller;

import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.imageio.ImageIO;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JTable;

import org.jdesktop.swingx.autocomplete.AutoCompleteDecorator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import ec.gob.msp.rdacaa.admision.model.CmbDetalleCatalogoCellRenderer;
import ec.gob.msp.rdacaa.admision.model.DetalleCatalogoToStringConverter;
import ec.gob.msp.rdacaa.amed.forma.GrupoVulnerable;
import ec.gob.msp.rdacaa.amed.model.AmedCmbGrupoVulnerableModel;
import ec.gob.msp.rdacaa.amed.model.GrupoVulnerableTablaModel;
import ec.gob.msp.rdacaa.business.entity.Atenciongvulnerable;
import ec.gob.msp.rdacaa.business.entity.Detallecatalogo;
import ec.gob.msp.rdacaa.business.service.DetallecatalogoService;
import ec.gob.msp.rdacaa.business.service.GvulnerablevalidacionService;
import ec.gob.msp.rdacaa.frontend.sesion.VariableSesion;
import ec.gob.msp.rdacaa.utilitario.forma.ButtonColumn;
import ec.gob.msp.rdacaa.utilitario.forma.FormatoTabla;
import ec.gob.msp.rdacaa.utilitario.forma.UtilitarioForma;

/**
 *
 * @author jazael.faubla
 */
@Controller
public class GrupoVulnerableController {

    private final GrupoVulnerable grupoVulnerableFrame;
    private final DetallecatalogoService detallecatalogoService;
    private GvulnerablevalidacionService gvulnerablevalidacionService; 
    private final AmedCmbGrupoVulnerableModel amedCmbGrupoVulnerableModel;
    private JComboBox<Detallecatalogo> cbmGruposVulnerables;
    private JButton btnAgregarGV;
    private JTable tbGruposVulnerables;
    private final List<Atenciongvulnerable> listGruposVulnerables = new ArrayList<>();
    private final VariableSesion variableSesion = VariableSesion.getInstanciaVariablesSession();
    private Detallecatalogo detallecatalogoNull;
    private GrupoVulnerableTablaModel grupoVulnerableTablaModel;
    private static final String SUBSISTEMA = "No Aplica";
    
    @Autowired
    public GrupoVulnerableController (GrupoVulnerable grupoVulnerableFrame,DetallecatalogoService detallecatalogoService, GvulnerablevalidacionService gvulnerablevalidacionService){
        this.grupoVulnerableFrame = grupoVulnerableFrame;
        this.detallecatalogoService = detallecatalogoService;
        this.amedCmbGrupoVulnerableModel = new AmedCmbGrupoVulnerableModel();
        this.gvulnerablevalidacionService = gvulnerablevalidacionService;
    }
    
    @PostConstruct
    private void init() {

        loadDefaultCombos();
        this.cbmGruposVulnerables = grupoVulnerableFrame.getCbmGrupoVulnerable();
        this.btnAgregarGV = grupoVulnerableFrame.getBtn_Agregar();
        this.tbGruposVulnerables = grupoVulnerableFrame.getTbGruposVulnerables();
        AutoCompleteDecorator.decorate(cbmGruposVulnerables, new DetalleCatalogoToStringConverter());
        loadGruposVulnerables();
        cargarTabla();
        this.btnAgregarGV.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
                if (validateForm()){
                    if (amedCmbGrupoVulnerableModel.getSelectedItem() != null && amedCmbGrupoVulnerableModel.getSelectedItem().getDescripcion().equals(SUBSISTEMA)){
                        listGruposVulnerables.clear();
                        cargarTabla();                        
                        getGruposVulnerables();
                        //cleanInput(null);
                        cargarTabla();
                        cbmGruposVulnerables.setSelectedItem(detallecatalogoNull);
                        btnAgregarGV.setEnabled(false);
                        cbmGruposVulnerables.setEnabled(false);
                    }else{
                            getGruposVulnerables();
                            cargarTabla();
                             btnAgregarGV.setEnabled(true);
                             cbmGruposVulnerables.setEnabled(true);
                    } 
                }
            }

            @Override
            public void mousePressed(MouseEvent e) {
               //nada
            }

            @Override
            public void mouseReleased(MouseEvent e) {
               //nada
            }

            @Override
            public void mouseEntered(MouseEvent e) {
               //nada
            }

            @Override
            public void mouseExited(MouseEvent e) {
                //nada
            }
        });
    }
    public void loadGruposVulnerables() {
        int embarazada = 0;
        List<Detallecatalogo> listaGruposVulnerables = null;
        if (VariableSesion.getInstanciaVariablesSession().getIsEmbarazada() != null){
            if (VariableSesion.getInstanciaVariablesSession().getIsEmbarazada()){
                embarazada=1;
            } else embarazada=0;
        }
        if(cbmGruposVulnerables.getSelectedItem() == null || cbmGruposVulnerables.getSelectedItem().equals(detallecatalogoNull)){
            if(VariableSesion.getInstanciaVariablesSession().getPersona() != null){
                listaGruposVulnerables = gvulnerablevalidacionService.findGruposVulnerablesFiltrados(VariableSesion.getInstanciaVariablesSession().getAniosPersona(),
                                                                                                    VariableSesion.getInstanciaVariablesSession().getMesesPersona(),
                                                                                                    VariableSesion.getInstanciaVariablesSession().getDiasPersona(),
                                                                                                    VariableSesion.getInstanciaVariablesSession().getPersona().getCtsexoId().getId(),
                                                                                                    embarazada);
            }else
                listaGruposVulnerables = detallecatalogoService.findAllGruposVulnerables();
         
            amedCmbGrupoVulnerableModel.clear();
            amedCmbGrupoVulnerableModel.addElements(listaGruposVulnerables);
            cbmGruposVulnerables.setRenderer(new CmbDetalleCatalogoCellRenderer());
            cbmGruposVulnerables.setModel(amedCmbGrupoVulnerableModel);
            cbmGruposVulnerables.setSelectedItem(detallecatalogoNull);
        }
        
    }
    
    public void getGruposVulnerables(){
        Atenciongvulnerable atenciongvulnerable = new Atenciongvulnerable();
        atenciongvulnerable.setCtgvulnerableId(amedCmbGrupoVulnerableModel.getSelectedItem());
                if(listGruposVulnerables.isEmpty()) {
                    listGruposVulnerables.add(atenciongvulnerable);
                } else {
                    if(listGruposVulnerables.size() == 5) {
                        UtilitarioForma.muestraMensaje("", grupoVulnerableFrame, "ERROR", "Estimado usuario le recordamos que puede ingresar un máximo de 5 códigos, por favor verificar.");
                    }else {
                        Optional<Atenciongvulnerable> gruposVulnerables = listGruposVulnerables.stream().filter(gvulnerables -> gvulnerables.getCtgvulnerableId().getId().equals(atenciongvulnerable.getCtgvulnerableId().getId())).findFirst();
                        if(gruposVulnerables.isPresent()) {
                            UtilitarioForma.muestraMensaje("", grupoVulnerableFrame, "ERROR", "Estimado usuario el grupo Vulnerable ya ha sido agregado, por favor verificar.");
                        } else {
                            listGruposVulnerables.add(atenciongvulnerable);
                        }
                    }
                 }
            
        variableSesion.setAtenciongvulnerable(listGruposVulnerables);
    }
    
    private void cargarTabla() {
        tbGruposVulnerables.setDefaultRenderer(Object.class, new FormatoTabla());
        String[] columns = {"Id","Grupos Vulnerables","Eliminar"};
        grupoVulnerableTablaModel = new GrupoVulnerableTablaModel(columns);
        grupoVulnerableTablaModel.addFila(listGruposVulnerables.isEmpty() ? new ArrayList() : listGruposVulnerables);
        tbGruposVulnerables.setModel(grupoVulnerableTablaModel);
        if(!listGruposVulnerables.isEmpty()) {
            loadButtonDelete(tbGruposVulnerables, 2);
        }else {
            btnAgregarGV.setEnabled(true);
            cbmGruposVulnerables.setEnabled(true);
        }
            
        grupoVulnerableTablaModel.ocultaColumnaCodigo(tbGruposVulnerables);
        grupoVulnerableTablaModel.definirAnchoColumnas(tbGruposVulnerables ,new int[] { 130,25 }); 
        
    }
    
    private void loadButtonDelete(JTable table, int column) {
        try {
            ButtonColumn buttonColumn = new ButtonColumn(table, createActionExport(), column);
            Image img = ImageIO.read(getClass().getResource("/principal/grupoprioritario.jpg"));
            buttonColumn.getTableCellRendererComponent(table, new ImageIcon(img), false,true, column, column);
        } catch (IOException ex) {
            Logger.getLogger(GrupoVulnerableController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private Action createActionExport() {
        return new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                listGruposVulnerables.remove(tbGruposVulnerables.getSelectedRow());
                cargarTabla();
            }
        };
    }
    
    public void loadDefaultCombos(){
        detallecatalogoNull = new Detallecatalogo(){
            private static final long serialVersionUID = 1L;
            boolean isNoSelectable = true;
        };
        detallecatalogoNull.setId(0);
        detallecatalogoNull.setDescripcion("Seleccione");
    }
    
    public void cleanInput(String panel) {
        if(panel != null && panel.equals("commons")) {
            listGruposVulnerables.clear();
            cargarTabla();
        }
        Object[] inputs = {cbmGruposVulnerables};
        UtilitarioForma.limpiarCampos(grupoVulnerableFrame, inputs);
        cbmGruposVulnerables.setSelectedItem(detallecatalogoNull);
    }
    
    public boolean validateForm() {

        boolean validarForma = UtilitarioForma.validarCamposRequeridos(cbmGruposVulnerables);      
        return validarForma;
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.gob.msp.rdacaa.amed.controller;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import ec.gob.msp.rdacaa.business.entity.Atencionmedica;
import ec.gob.msp.rdacaa.business.entity.Medicionpaciente;
import ec.gob.msp.rdacaa.business.service.ParroquiaService;
import ec.gob.msp.rdacaa.business.service.RegistroAtencionService;
import ec.gob.msp.rdacaa.frontend.sesion.VariableSesion;
import ec.gob.msp.rdacaa.seguridades.util.SeguridadUtil;

/**
 *
 * @author miguel.faubla
 */
@Controller
public class AtencionMedicaController {

	private final Logger logger = LoggerFactory.getLogger(AtencionMedicaController.class);
    private final VariableSesion variableSesion = VariableSesion.getInstanciaVariablesSession();
    private final RegistroAtencionService registroAtencionControler;
    private ParroquiaService parroquiaService;
    
    @Autowired
    public AtencionMedicaController(RegistroAtencionService registroAtencionControler, ParroquiaService parroquiaService) {
        this.registroAtencionControler = registroAtencionControler;
        this.parroquiaService = parroquiaService;
    }
    
    public boolean finalizarAtencionMedica() throws GeneralSecurityException, IOException {
        Atencionmedica atencionmedica = new Atencionmedica();
        atencionmedica.setEntidadId(variableSesion.getEstablecimientoSalud().getId());
        atencionmedica.setUuid(UUID.randomUUID().toString());
        atencionmedica.setCttipobonoId(variableSesion.getPersona().getCttipobonoId());
        atencionmedica.setCttiposegurosaludId(variableSesion.getPersona().getCttiposegurosaludId());
        atencionmedica.setFechaatencion(variableSesion.getFechaAtencion());
        atencionmedica.setCtespecialidadmedicaId(variableSesion.getAtencionMedica().getCtespecialidadmedicaId());
        atencionmedica.setUsuariocreacionId(variableSesion.getAtencionMedica().getUsuariocreacionId());
        atencionmedica.setKsCident(SeguridadUtil.cifrarString(variableSesion.getPersona().getUuidpersona(), VariableSesion.getInstanciaVariablesSession().getUtilitario(), VariableSesion.getInstanciaVariablesSession().getUsuario().getLogin()));
        atencionmedica.setKuCident(SeguridadUtil.cifrarPublicoString(variableSesion.getPersona().getUuidpersona(), VariableSesion.getInstanciaVariablesSession().getKeysetPublico()));
        atencionmedica.setKuKeyid(String.valueOf(VariableSesion.getInstanciaVariablesSession().getKeysetPublico().getKeysetInfo().getPrimaryKeyId()));
        if(variableSesion.getEstablecimientoSalud().getUbicacionParroquiaId() != null){
        	atencionmedica.setUbicacionParroquiaId(variableSesion.getEstablecimientoSalud().getUbicacionParroquiaId());
        }else{
        	atencionmedica.setUbicacionParroquiaId(parroquiaService.findOneByParroquiaId(variableSesion.getEstablecimientoSalud().getParroquiaId()));
        }
        
        boolean finalizado;
        try {
            registroAtencionControler
                .executeRegistroAtencion(
                    variableSesion.getPersona(), 
                    variableSesion.getIntraExtraMural(), 
                    variableSesion.getGruposPrioritarios(), 
                    atencionmedica, 
                    variablesAntropometricasToList(),
                    variableSesion.getProfesional(), 
                    variableSesion.getUsuario(), 
                    variableSesion.getAtencionobstetrica(), 
                    variableSesion.getVihsida(),
                    variableSesion.getEstablecimientoSalud(),
                    variableSesion.getCtEspecialidad(), 
                    variableSesion.getDiagnosticoatencion(),
                    variableSesion.getAtencionsivan(), 
                    variableSesion.getRegistrovacunacion(), 
                    variableSesion.getExamenlaboratorio(),
                    variableSesion.getOdontologia(), //odontologia
                    variableSesion.getReferencia(), //referencia
                    variableSesion.getInterconsulta(),//interconsulta
                    variableSesion.getAtencionprescripcion(), //prescripcion 
                    variableSesion.getProcedimientoatencion(),
                    variableSesion.getAtencionviolencia(),//violencia
                    variableSesion.getAtenciongvulnerable(),//vulnerables
                    variableSesion.getEstrategiapaciente()//Estrategia
                );  
            finalizado = true;
        } catch (Exception e) {
        	logger.error("Error guardando el registro" , e);
            finalizado = false;
        }
  
        return finalizado;
    }
    
	private List<Medicionpaciente> variablesAntropometricasToList() {
		List<Medicionpaciente> listaVariablesAntropometricas = new ArrayList<>();
		if (variableSesion.getMedicionPeso() != null) {
			listaVariablesAntropometricas.add(variableSesion.getMedicionPeso());
		}
		if (variableSesion.getMedicionTalla() != null) {
			listaVariablesAntropometricas.add(variableSesion.getMedicionTalla());
		}
		if (variableSesion.getMedicionTallaCorregida() != null) {
			listaVariablesAntropometricas.add(variableSesion.getMedicionTallaCorregida());
		}
		if (variableSesion.getMedicionPerimetroCefalico() != null) {
			listaVariablesAntropometricas.add(variableSesion.getMedicionPerimetroCefalico());
		}
		if (variableSesion.getMedicionIMC() != null) {
			if (variableSesion.getClasificacionIMC() != null) {
				variableSesion.getMedicionIMC().setSignosvitalesalertasId(variableSesion.getClasificacionIMC());
			}
			listaVariablesAntropometricas.add(variableSesion.getMedicionIMC());
		}
		if (variableSesion.getValorHBRiesgo() != null) {
			listaVariablesAntropometricas.add(variableSesion.getValorHBRiesgo());
		}
		if (variableSesion.getValorHBRiesgoCorregido() != null) {
			if (variableSesion.getIndicadorAnemiaHBRiesgoCorregido() != null) {
				variableSesion.getValorHBRiesgoCorregido()
						.setSignosvitalesalertasId(variableSesion.getIndicadorAnemiaHBRiesgoCorregido());
			}
			listaVariablesAntropometricas.add(variableSesion.getValorHBRiesgoCorregido());
		}
		if (variableSesion.getScoreZTallaParaEdad() != null) {
			if (variableSesion.getCategoriaTallaParaEdad() != null) {
				variableSesion.getScoreZTallaParaEdad()
						.setSignosvitalesalertasId(variableSesion.getCategoriaTallaParaEdad());
			}
			listaVariablesAntropometricas.add(variableSesion.getScoreZTallaParaEdad());
		}
		if (variableSesion.getScoreZPesoParaEdad() != null) {
			if (variableSesion.getCategoriaPesoParaEdad() != null) {
				variableSesion.getScoreZPesoParaEdad()
						.setSignosvitalesalertasId(variableSesion.getCategoriaPesoParaEdad());
			}
			listaVariablesAntropometricas.add(variableSesion.getScoreZPesoParaEdad());
		}
		if (variableSesion.getScoreZPesoParaLongitudTalla() != null) {
			if (variableSesion.getCategoriaPesoParaLongitudTalla() != null) {
				variableSesion.getScoreZPesoParaLongitudTalla()
						.setSignosvitalesalertasId(variableSesion.getCategoriaPesoParaLongitudTalla());
			}
			listaVariablesAntropometricas.add(variableSesion.getScoreZPesoParaLongitudTalla());
		}
		if (variableSesion.getScoreZIMCParaEdad() != null) {
			if (variableSesion.getCategoriaIMCParaEdad() != null) {
				variableSesion.getScoreZIMCParaEdad()
						.setSignosvitalesalertasId(variableSesion.getCategoriaIMCParaEdad());
			}
			listaVariablesAntropometricas.add(variableSesion.getScoreZIMCParaEdad());
		}
		if (variableSesion.getScoreZPerimetroCefalicoParaEdad() != null) {
			if (variableSesion.getCategoriaPerimetroCefalicoParaEdad() != null) {
				variableSesion.getScoreZPerimetroCefalicoParaEdad()
						.setSignosvitalesalertasId(variableSesion.getCategoriaPerimetroCefalicoParaEdad());
			}
			listaVariablesAntropometricas.add(variableSesion.getScoreZPerimetroCefalicoParaEdad());
		}

		return listaVariablesAntropometricas;
	}
    
}

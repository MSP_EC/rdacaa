/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.gob.msp.rdacaa.amed.model;

import ec.gob.msp.rdacaa.business.entity.Detalleserviciotarifario;
import ec.gob.msp.rdacaa.control.swing.model.CustomObjectToStringConverter;

/**
 *
 * @author dmurillo
 */
public class AmedDetalleserviciotarifarioToStringCoverter extends CustomObjectToStringConverter {

	@Override
	public String getPreferredStringForItem(Object o) {
		if (o == null) {
			return null;
		} else if (o instanceof Detalleserviciotarifario) {
			Detalleserviciotarifario detalleserviciotarifario = (Detalleserviciotarifario) o;
			if (detalleserviciotarifario.getId().equals(0)) {
				return detalleserviciotarifario.getDescripcion();
			}
			return detalleserviciotarifario.getCodigo() + " - " + detalleserviciotarifario.getDescripcion();
		} else {
			return o.toString();
		}
	}

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.gob.msp.rdacaa.principal.wizard;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.github.cjwizard.WizardSettings;

import ec.gob.msp.rdacaa.amed.controller.InformacionObstetricaController;
import ec.gob.msp.rdacaa.amed.forma.InfoObstetrica;
import ec.gob.msp.rdacaa.frontend.sesion.VariableSesion;
import ec.gob.msp.rdacaa.principal.controller.CommonPanelController;

/**
 *
 * @author miguel.faubla
 */
@Component
public class PanelInformacionObstetrica extends com.github.cjwizard.WizardPage {

    private final InfoObstetrica infoObstetrica;
    private final InformacionObstetricaController informacionObstetricaController;
    private final CommonPanelController commonPanelController;

    @Autowired
    public PanelInformacionObstetrica(InfoObstetrica infoObstetrica, InformacionObstetricaController informacionObstetricaController, CommonPanelController commonPanelController) {
        super("Información Obstetrica", "Atención Médica");
        this.infoObstetrica = infoObstetrica;
        this.informacionObstetricaController = informacionObstetricaController;
        this.commonPanelController = commonPanelController;
    }

    @PostConstruct
    private void init() {
        infoObstetrica.setVisible(true);
        add(infoObstetrica);
    }
    
    @Override
    public boolean onNext(WizardSettings settings) {
        return informacionObstetricaController.validateForm();
    }
    
    @Override
    public void updateSettings(WizardSettings settings) {
        commonPanelController.refreshPanels();      
    }
    
}

package ec.gob.msp.rdacaa.reporte.model;

import ec.gob.msp.rdacaa.business.entity.ReporteProduccion;
import ec.gob.msp.rdacaa.utilitario.forma.ModeloTablaGenerico;

public class ProduccionFechaEspecialidadTablaModel extends ModeloTablaGenerico<ReporteProduccion> {

	private static final long serialVersionUID = 7707274263698299422L;

	public ProduccionFechaEspecialidadTablaModel(String[] columnas) {
		super(columnas);

	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		ReporteProduccion obj = getListaDatos().get(rowIndex);
		switch (columnIndex) {
		case 0:
			return "";
		case 1:
			return obj.getCodigoestablecimiento();
		case 2:
			return obj.getNombreestablecimiento();
		case 3:
			return obj.getNombreprofesional();
		case 4:
			return obj.getEspecialidaprofesional();
		case 5:
			return obj.getPrevencionprimera();
		case 6:
			return obj.getPrevencionsubsecuente();
		case 7:
			return obj.getMorbilidadprimera();
		case 8:
			return obj.getMorbilidadsubsecuente();
		case 9:
			return obj.getVacunas();
		case 10:
			return obj.getId();
		default:
			return "";
		}
	}

}

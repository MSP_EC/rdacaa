package ec.gob.msp.rdacaa.admision.model;

import java.awt.Component;

import javax.swing.DefaultListCellRenderer;
import javax.swing.JList;

import ec.gob.msp.rdacaa.business.entity.Provincia;

public class CmbProvinciaCellRenderer extends DefaultListCellRenderer {

    private static final long serialVersionUID = -8541367145847548845L;
	

    @Override
	public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected,
			boolean cellHasFocus) {
		if (value instanceof Provincia) {
			Provincia provincia = (Provincia) value;
			value = provincia.getDescripcion();
		}
		return super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
	}
}
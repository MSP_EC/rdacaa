/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.gob.msp.rdacaa.principal.wizard;


import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.github.cjwizard.WizardSettings;

import ec.gob.msp.rdacaa.amed.controller.DiagnosticoCIEController;
import ec.gob.msp.rdacaa.amed.forma.DiagnosticoCIE;
import ec.gob.msp.rdacaa.principal.controller.CommonPanelController;

/**
 *
 * @author eduardo
 */
@Component
public class PanelDiagnosticos extends com.github.cjwizard.WizardPage{
    

    private final DiagnosticoCIE diagnosticoCIE;
    private final DiagnosticoCIEController diagnosticoCIEController;
    private final CommonPanelController commonPanelController;

    @Autowired
    public PanelDiagnosticos(DiagnosticoCIE diagnosticoCIE,
                            DiagnosticoCIEController diagnosticoCIEController,
                            CommonPanelController commonPanelController) {
        super("Registro de Diagnosticos", "Atención Médica");
        this.diagnosticoCIE = diagnosticoCIE;
        this.diagnosticoCIEController = diagnosticoCIEController;
        this.commonPanelController = commonPanelController;
    }

    @PostConstruct
    private void init() {
        diagnosticoCIE.setVisible(true);
        add(diagnosticoCIE);
    }
    
    @Override
    public boolean onNext(WizardSettings settings) {
        return diagnosticoCIEController.validateForm();
    }
    
    @Override
    public void updateSettings(WizardSettings settings) {
        commonPanelController.refreshPanels();
    }
    
}

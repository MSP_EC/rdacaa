package ec.gob.msp.rdacaa.reporte.model;

import lombok.Getter;
import lombok.Setter;

public class VacunasDosisReporte {
	@Getter
	@Setter
	private String descripcion;
	@Getter
	@Setter
	private int dosis1;
	@Getter
	@Setter
	private int dosis2;
	@Getter
	@Setter
	private int dosis3;
	@Getter
	@Setter
	private int dosis4;
	@Getter
	@Setter
	private int dosis5;
	@Getter
	@Setter
	private int dosis6;
}

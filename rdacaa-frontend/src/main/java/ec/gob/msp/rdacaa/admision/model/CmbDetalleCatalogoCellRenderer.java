package ec.gob.msp.rdacaa.admision.model;

import ec.gob.msp.rdacaa.business.entity.Detallecatalogo;
import java.awt.Component;

import javax.swing.DefaultListCellRenderer;
import javax.swing.JList;

public class CmbDetalleCatalogoCellRenderer extends DefaultListCellRenderer {

    private static final long serialVersionUID = -1911821062776433300L;

    @Override
    public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected,
            boolean cellHasFocus) {
        if (value instanceof Detallecatalogo) {
            Detallecatalogo detallecatalogo = (Detallecatalogo) value;
            value = detallecatalogo.getDescripcion();
        }
        return super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
    }
}

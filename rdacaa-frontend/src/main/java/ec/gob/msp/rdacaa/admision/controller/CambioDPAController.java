package ec.gob.msp.rdacaa.admision.controller;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.SwingUtilities;

import org.jdesktop.swingx.autocomplete.AutoCompleteDecorator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import ec.gob.msp.rdacaa.admision.forma.CambioDPA;
import ec.gob.msp.rdacaa.admision.model.AdmisionCmbCantonModel;
import ec.gob.msp.rdacaa.admision.model.AdmisionCmbParroquiaModel;
import ec.gob.msp.rdacaa.admision.model.AdmisionCmbProvinciaModel;
import ec.gob.msp.rdacaa.admision.model.CantonToStringConverter;
import ec.gob.msp.rdacaa.admision.model.CmbCantonCellRenderer;
import ec.gob.msp.rdacaa.admision.model.CmbParroquiaCellRenderer;
import ec.gob.msp.rdacaa.admision.model.CmbProvinciaCellRenderer;
import ec.gob.msp.rdacaa.admision.model.ParroquiaToStringConverter;
import ec.gob.msp.rdacaa.admision.model.ProvinciaToStringConverter;
import ec.gob.msp.rdacaa.business.entity.Canton;
import ec.gob.msp.rdacaa.business.entity.Entidad;
import ec.gob.msp.rdacaa.business.entity.Parroquia;
import ec.gob.msp.rdacaa.business.entity.Provincia;
import ec.gob.msp.rdacaa.business.service.CantonService;
import ec.gob.msp.rdacaa.business.service.EntidadService;
import ec.gob.msp.rdacaa.business.service.ParroquiaService;
import ec.gob.msp.rdacaa.business.service.ProvinciaService;
import ec.gob.msp.rdacaa.frontend.sesion.VariableSesion;
import ec.gob.msp.rdacaa.principal.controller.AccesoPrincipalController;
import ec.gob.msp.rdacaa.principal.forma.EscritorioPrincipal;
import ec.gob.msp.rdacaa.utilitario.forma.UtilitarioForma;

@Controller
public class CambioDPAController {
	private CambioDPA cambioDPAFrame;
	private JComboBox<Provincia> cmbProvincia;
    private JComboBox<Canton> cmbCanton;
    private JComboBox<Parroquia> cmbParroquia;
    private Provincia provinciaNull;
    private Canton cantonNull;
    private Parroquia parroquiaNull;
    private static final String COMBONULO = "Seleccione";
    private ProvinciaService provinciaService;
    private AdmisionCmbProvinciaModel admisionCmbProvinciaModel;
    private CantonService cantonService;
    private AdmisionCmbCantonModel admisionCmbCantonModel;
    private ParroquiaService parroquiaService;
    private AdmisionCmbParroquiaModel admisionCmbParroquiaModel;
    private EntidadService entidadService;
    private JButton btnAceptar;
    private JButton btmCancelar;
    private Object[] controlesValidar = new Object[3];
    private AccesoPrincipalController accesoPrincipalController;
	
	
	@Autowired
    public CambioDPAController(CambioDPA cambioDPAFrame,
    						   ProvinciaService provinciaService,
    						   AdmisionCmbProvinciaModel admisionCmbProvinciaModel,
    						   CantonService cantonService,
    						   AdmisionCmbCantonModel admisionCmbCantonModel,
    						   ParroquiaService parroquiaService,
    						   AdmisionCmbParroquiaModel admisionCmbParroquiaModel,
    						   EntidadService entidadService,
    						   AccesoPrincipalController accesoPrincipalController
    ){
       this.cambioDPAFrame = cambioDPAFrame;
       this.provinciaService = provinciaService;
       this.admisionCmbProvinciaModel = admisionCmbProvinciaModel;
       this.cantonService = cantonService;
       this.admisionCmbCantonModel = admisionCmbCantonModel;
       this.parroquiaService = parroquiaService;
       this.admisionCmbParroquiaModel = admisionCmbParroquiaModel;
       this.entidadService = entidadService;
       this.accesoPrincipalController = accesoPrincipalController;
	}
	
	@PostConstruct
    private void init() {
		this.cmbProvincia = cambioDPAFrame.getCmbProvincia();
        this.cmbCanton = cambioDPAFrame.getCmbCanton();
        this.cmbParroquia = cambioDPAFrame.getCmbParroquia();
        this.btnAceptar = cambioDPAFrame.getBtnAceptar();
        btnAceptar.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
                save();
            }

            @Override
            public void mousePressed(MouseEvent e) {
            	//no se usa
            }

            @Override
            public void mouseReleased(MouseEvent e) {
            	//no se usa
            }

            @Override
            public void mouseEntered(MouseEvent e) {
            	//no se usa
            }

            @Override
            public void mouseExited(MouseEvent e) {
            	//no se usa
            }
        });
        
        addAutocompleteToCombos();
        loadDefaultCombos();
        cmbProvincia.addActionListener(e -> loadCantones());
        cmbCanton.addActionListener(e -> loadParroquias());
        loadProvincias();
        loadCatalogosIndividuales();
        
        this.btmCancelar = cambioDPAFrame.getBtm_cancelar();
        btmCancelar.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
                SwingUtilities.getWindowAncestor(cambioDPAFrame).dispose();
                EscritorioPrincipal.dsp_contenedorRDACAA.remove(cambioDPAFrame);
            }

            @Override
            public void mousePressed(MouseEvent e) {
            	//no se usa
            }

            @Override
            public void mouseReleased(MouseEvent e) {
            	//no se usa
            }

            @Override
            public void mouseEntered(MouseEvent e) {
            	//no se usa
            }

            @Override
            public void mouseExited(MouseEvent e) {
            	//no se usa
            }
        });
	}
	
	private void loadCatalogosIndividuales() {
		cmbCanton.setRenderer(new CmbCantonCellRenderer());
		loadCantones();
        cmbParroquia.setRenderer(new CmbParroquiaCellRenderer());
        loadParroquias();
	}
	
	private void loadDefaultCombos(){
        
        provinciaNull = new Provincia(){
            private static final long serialVersionUID = 1L;
            boolean isNoSelectable = true;
        };
        provinciaNull.setId(0);
        provinciaNull.setDescripcion(COMBONULO);
        
        cantonNull = new Canton(){
            private static final long serialVersionUID = 1L;
            boolean isNoSelectable = true;
        };
        cantonNull.setId(0);
        cantonNull.setDescripcion(COMBONULO);
        
        parroquiaNull = new Parroquia(){
            private static final long serialVersionUID = 1L;
            boolean isNoSelectable = true;
        };
        parroquiaNull.setId(0);
        parroquiaNull.setDescripcion(COMBONULO);
    }
	private void addAutocompleteToCombos() {
		AutoCompleteDecorator.decorate(cmbProvincia, new ProvinciaToStringConverter());
        AutoCompleteDecorator.decorate(cmbCanton, new CantonToStringConverter());
        AutoCompleteDecorator.decorate(cmbParroquia, new ParroquiaToStringConverter());
	}
	private void loadProvincias() {
        List<Provincia> provincias = provinciaService.findAllOrderedByDescripcion();
        admisionCmbProvinciaModel.clear();
        admisionCmbProvinciaModel.addElements(provincias);
        cmbProvincia.setRenderer(new CmbProvinciaCellRenderer());
        cmbProvincia.setModel(admisionCmbProvinciaModel);
        cmbProvincia.setSelectedItem(provinciaNull);
    }
	private void loadCantones() {
        if (admisionCmbProvinciaModel.getSelectedItem() != null) {
            List<Canton> cantones = cantonService.findAllByProvinciaId(admisionCmbProvinciaModel.getSelectedItem().getId());
            admisionCmbCantonModel.clear();
            admisionCmbCantonModel.addElements(cantones);
            cmbCanton.setModel(admisionCmbCantonModel);
            cmbCanton.setSelectedItem(cantonNull);
        } else {
            admisionCmbCantonModel.clear();
        }

    }
    private void loadParroquias() {
        if (admisionCmbProvinciaModel.getSelectedItem() != null && admisionCmbCantonModel.getSelectedItem() != null) {
            List<Parroquia> parroquias = parroquiaService.findAllByProvinciaId(admisionCmbCantonModel.getSelectedItem().getId());
            admisionCmbParroquiaModel.clear();
            admisionCmbParroquiaModel.addElements(parroquias);
            cmbParroquia.setModel(admisionCmbParroquiaModel);
            cmbParroquia.setSelectedItem(parroquiaNull);
        } else {
            admisionCmbParroquiaModel.clear();
        }

    }
    private void cleanInput(){
    	UtilitarioForma.limpiarCampos(cambioDPAFrame, controlesValidar);
        setNullCombobox();

    }
    private void setNullCombobox(){
    	cmbProvincia.setSelectedItem(provinciaNull);
        cmbCanton.setSelectedItem(cantonNull);
        cmbParroquia.setSelectedItem(parroquiaNull);
    }
    
    private void save() {
    	if(VariableSesion.getInstanciaVariablesSession().getEstablecimientoSalud() != null &&
    			admisionCmbParroquiaModel.getSelectedItem() != null &&
    			admisionCmbParroquiaModel.getSelectedItem().getId() != 0
    	   ){
    		boolean validarForma;
    		validarForma = UtilitarioForma.validarCamposRequeridos(controlesValidar);
    		if(validarForma){
    			Entidad entidad = VariableSesion.getInstanciaVariablesSession().getEstablecimientoSalud();
    			entidad.setUbicacionParroquiaId(admisionCmbParroquiaModel.getSelectedItem());
    			entidad = entidadService.save(entidad);
            	VariableSesion.getInstanciaVariablesSession().setEstablecimientoSalud(entidad);
            	cleanInput();
            	UtilitarioForma.muestraMensaje("", cambioDPAFrame, "INFO", "Proceso realizado correctamente");
            	accesoPrincipalController.addCabeceraInfoUsuario(VariableSesion.getInstanciaVariablesSession());
            	SwingUtilities.getWindowAncestor(cambioDPAFrame).dispose();
            	EscritorioPrincipal.dsp_contenedorRDACAA.remove(cambioDPAFrame);
    		}
    	}
    	
	}
    
}

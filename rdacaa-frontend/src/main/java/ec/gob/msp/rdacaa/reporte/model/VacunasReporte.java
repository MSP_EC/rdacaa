package ec.gob.msp.rdacaa.reporte.model;

import lombok.Getter;
import lombok.Setter;

public class VacunasReporte {
	@Getter
	@Setter
	private String descripcion;
	@Getter
	@Setter
	private int num028;
	@Getter
	@Setter
	private int num111;
	@Getter
	@Setter
	private int num14;
	@Getter
	@Setter
	private int num59;
	@Getter
	@Setter
	private int num1014;
	@Getter
	@Setter
	private int num1519;
	@Getter
	@Setter
	private int num2039;
	@Getter
	@Setter
	private int num4064;
	@Getter
	@Setter
	private int num65;
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.gob.msp.rdacaa.principal.wizard;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.github.cjwizard.WizardSettings;

import ec.gob.msp.rdacaa.amed.controller.DatosSIVANController;
import ec.gob.msp.rdacaa.amed.forma.DatosSIVAN;
import ec.gob.msp.rdacaa.principal.controller.CommonPanelController;

/**
 *
 * @author eduardo
 */
@Component
public class PanelDatosSIVAN extends com.github.cjwizard.WizardPage{

    private static final long serialVersionUID = 807061797141528745L;
    
    private final DatosSIVAN datosSIVAN;
    private final DatosSIVANController datosSIVANController;
    private final CommonPanelController commonPanelController;
    
    @Autowired
    public PanelDatosSIVAN(DatosSIVAN datosSIVAN,DatosSIVANController datosSIVANController,CommonPanelController commonPanelController) {
        super("Registro de Datos Nutricionales", "Atencion Médica");
        this.datosSIVAN = datosSIVAN;
        this.datosSIVANController = datosSIVANController;
        this.commonPanelController = commonPanelController;
    }
    
    @PostConstruct
    private void init() {
        datosSIVAN.setVisible(true);
        add(datosSIVAN);
    }
    
    @Override
    public boolean onNext(WizardSettings settings) {  
        return datosSIVANController.validateForm();    
    }

    @Override
    public void updateSettings(WizardSettings settings) {
        commonPanelController.refreshPanels();
    }
}

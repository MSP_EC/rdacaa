/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.gob.msp.rdacaa.principal.controller;

import ec.gob.msp.rdacaa.principal.forma.TerminosDeUso;
import javax.annotation.PostConstruct;
import javax.swing.JLabel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

/**
 *
 * @author eduardo
 */
@Controller
public class TerminosDeUsoController {
    
    @Autowired
    private TerminosDeUso terminosDeUsoFrame;
     private JLabel txt_Art9;
     private JLabel txt_Art10;
     private JLabel txt_Art61;
     private JLabel txt_Art25;
    
     
     @PostConstruct
    private void init(){
        
        this.txt_Art9 = terminosDeUsoFrame.getTxtArt9();
        this.txt_Art10 = terminosDeUsoFrame.getTxtArt10();
        this.txt_Art61 = terminosDeUsoFrame.getTxtArt61();
        this.txt_Art25 = terminosDeUsoFrame.getTxtArt25();
        cargaTexto();
        
        
    }
     
     public void cargaTexto(){
    
    String art9 = "<html>"
                + "<br>Art. 9.- El personal operativo y administrativo de los establecimientos del Sistema Nacional de Salud que tenga acceso a información de<br>"
                + "<br>deberá guardar reserva de manera indefinida respecto los/las usuarios/as durante el ejercicio de sus funcionales,<br>"
                + "<br>información contenida en la historia clínica, de dicha información y no podrá divulgar la<br>"
                + "<br>ni aquella constante en todo documento donde reposen datos confidenciales de los/las usuarios/as.\"<br>"
                + "<br><br>"
                + "</html>" ;
        txt_Art9.setText(art9);
        txt_Art9.setVisible(true);
        
        String art10 = "<html>"
                + "<br>Art. 10.- Los documentos que contengan información confidencial se mantendrán abiertos(tanto en formato físico como digital) únicamente<br>"
                + "<br>que correspondan, como parte de un estudio epidemiológico, mientras se estén utilizando en la prestación de servicios al<br>"
                + "<br>una auditoría de calidad de la atención en salud u otro debidamente justificados y que se enmarquen en los casos establecidos en el<br>"
                + "<br>presente reglamento.\"<br>"
                + "<br><br>"
                + "</html>" ;
        txt_Art10.setText(art10);
        
        String art61 = "<html>"
                + "<br><br>"
                + "<br>Art. 61.- Las instituciones publicas y privadas, los profesionales de salud y la población en general, reportaran en forma oportuna la existencia de casos<br>"
                + "<br> sospechosos, probables, compatibles y confirmados de enfermedades declaradas por la autoridad sanitaria nacional como de notificación<br>"
                + "<br>obligatoria y aquellas de reporte internacional. Las instituciones y profesionales de salud, garantizaran la confidencialidad de la información entregada y recibida.<br>"
                + "<br><br>"
                + "</html>" ;
        txt_Art61.setText(art61);
        
        String art25 = "<html>"
                + "<br>Art. 25.- Las personas que, de cualquier modo, intervengan en la ejecución de investigación que realicen las entidades sujetas<br>"
                + "<br>distinta de la que haya sido autorizada. De contravenir al Sistema Estadístico Nacional, no podrán requerir información<br>"
                + "<br> a esta prohibición, se les impondrá las sanciones establecidas en la Ley de Servicio Civil y Carrera Administrativa\"<br>"
                + "<br><br>"
                + "</html>" ;
                
        txt_Art25.setText(art25);
        
        terminosDeUsoFrame.repaint();
    }
}

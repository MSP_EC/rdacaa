/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.gob.msp.rdacaa.principal.wizard;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.github.cjwizard.WizardSettings;

import ec.gob.msp.rdacaa.amed.controller.GrupoPrioritarioController;
import ec.gob.msp.rdacaa.amed.forma.GrupoPrioritario;
import ec.gob.msp.rdacaa.principal.controller.CommonPanelController;

/**
 *
 * @author msp
 */
@Component
public class PanelGruposPrioritarios extends com.github.cjwizard.WizardPage {

    private final CommonPanelController commonPanelController;
    private final GrupoPrioritario grupoPrioritario;
    private final GrupoPrioritarioController grupoPrioritarioController;

    @Autowired
    public PanelGruposPrioritarios(GrupoPrioritario grupoPrioritario,
            GrupoPrioritarioController grupoPrioritarioController,
            CommonPanelController commonPanelController) {
        super("Grupos Prioritarios", "Atención Médica");
        this.grupoPrioritario = grupoPrioritario;
        this.grupoPrioritarioController = grupoPrioritarioController;
        this.commonPanelController = commonPanelController;
    }

    @PostConstruct
    private void init() {
        grupoPrioritario.setVisible(true);
        add(grupoPrioritario);
    }

    @Override
    public boolean onNext(WizardSettings settings) {
        return grupoPrioritarioController.validateForm();
    }

    @Override
    public void updateSettings(WizardSettings settings) {
        commonPanelController.refreshPanels();
    }

}

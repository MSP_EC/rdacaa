/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.gob.msp.rdacaa.principal.wizard;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.github.cjwizard.WizardSettings;

import ec.gob.msp.rdacaa.amed.controller.OdontologiaController;
import ec.gob.msp.rdacaa.amed.forma.OdontologiaPanel;
import ec.gob.msp.rdacaa.principal.controller.CommonPanelController;

/**
 *
 * @author eduardo
 */
@Component
public class PanelOdontologia extends com.github.cjwizard.WizardPage{
    
    private OdontologiaPanel odontologia;
    private OdontologiaController odontologiaController;
    private CommonPanelController commonPanelController;
    
    @Autowired
    public PanelOdontologia(OdontologiaPanel odontologia,OdontologiaController odontologiaController,CommonPanelController commonPanelController) {
        super("Registro de Odontologia", "Atencion Médica");
        this.odontologia = odontologia;
        this.odontologiaController = odontologiaController;
        this.commonPanelController = commonPanelController;
    }
    
    @PostConstruct
    private void init() {
        odontologia.setVisible(true);
        add(odontologia);
    }

    @Override
    public boolean onNext(WizardSettings settings) {
        return odontologiaController.validateForm();
    }
    
    @Override
    public void updateSettings(WizardSettings settings) {
        commonPanelController.refreshPanels();
    }
    
}

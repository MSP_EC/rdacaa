/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.gob.msp.rdacaa.principal.wizard;

import com.github.cjwizard.WizardSettings;
import ec.gob.msp.rdacaa.amed.controller.ViolenciaController;
import ec.gob.msp.rdacaa.amed.forma.ViolenciaPanel;
import ec.gob.msp.rdacaa.principal.controller.CommonPanelController;
import javax.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author jazael.faubla
 */
@Component
public class PanelViolencia extends com.github.cjwizard.WizardPage{
    
    private final CommonPanelController commonPanelController;
    private final ViolenciaPanel violenciaPanel;
    private final ViolenciaController violenciaController;
    
    @Autowired
    public PanelViolencia(ViolenciaPanel violenciaPanel, CommonPanelController commonPanelController, ViolenciaController violenciaController) {
        super("Registro de Violencia de Género", "Atención Médica");
        this.commonPanelController = commonPanelController;
        this.violenciaPanel = violenciaPanel;
        this.violenciaController = violenciaController;
    }
    
    @PostConstruct
    private void init() {
        violenciaPanel.setVisible(true);
        add(violenciaPanel);
    }
    
    @Override
    public boolean onNext(WizardSettings settings) {
        return violenciaController.validateForm();
    }

    @Override
    public void updateSettings(WizardSettings settings) {
        commonPanelController.refreshPanels();
    }
    
}

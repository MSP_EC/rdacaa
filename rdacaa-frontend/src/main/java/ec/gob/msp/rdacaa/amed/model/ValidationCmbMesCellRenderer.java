package ec.gob.msp.rdacaa.amed.model;

import java.awt.Component;
import java.time.Month;
import java.time.format.TextStyle;
import java.util.Locale;

import javax.swing.DefaultListCellRenderer;
import javax.swing.JList;

public class ValidationCmbMesCellRenderer extends DefaultListCellRenderer {

	private static final long serialVersionUID = 5083719691582272580L;
	private static final Locale SPANISH = new Locale("es", "ES");

	@Override
	public Component getListCellRendererComponent(JList<? extends Object> list, Object value, int index,
			boolean isSelected, boolean cellHasFocus) {
		if (value instanceof Month) {
			Month mes = (Month) value;
			value = mes.getDisplayName(TextStyle.FULL, SPANISH);
		}
		return super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
	}

}

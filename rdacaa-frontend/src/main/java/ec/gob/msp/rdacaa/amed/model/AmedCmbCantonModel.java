/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.gob.msp.rdacaa.amed.model;

import ec.gob.msp.rdacaa.business.entity.Canton;
import ec.gob.msp.rdacaa.control.swing.model.DefaultComboBoxModel;
import org.springframework.stereotype.Component;

/**
 *
 * @author dmurillo
 */
@Component
public class AmedCmbCantonModel extends DefaultComboBoxModel<Canton>{
    
    private static final long serialVersionUID = 673297060335093192L;
    
}

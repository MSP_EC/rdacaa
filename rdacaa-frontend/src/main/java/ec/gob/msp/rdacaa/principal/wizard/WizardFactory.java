/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.gob.msp.rdacaa.principal.wizard;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.github.cjwizard.APageFactory;
import com.github.cjwizard.WizardPage;
import com.github.cjwizard.WizardSettings;

import ec.gob.msp.rdacaa.frontend.sesion.VariableSesion;
import ec.gob.msp.rdacaa.utilitario.enumeracion.ComunEnum;
import ec.gob.msp.rdacaa.utilitario.enumeracion.PanelsFormsCodeConstants;
import org.apache.commons.lang3.StringUtils;

/**
 *
 * @author miguel.faubla
 */
@Component
public class WizardFactory extends APageFactory {

    private final Logger logs = LoggerFactory.getLogger(WizardFactory.class);
    private final PanelAdmision panelAdmision;
    private final PanelGruposPrioritarios panelGruposPrioritarios;
    private final PanelGruposVulnerables panelGruposVulnerables;
    private final PanelViolencia panelViolencia;
    private final PanelDatosAntropometricos panelDatosAntropometricos;
    private final PanelInformacionObstetrica panelInformacionObstetrica;
    private final PanelVIH panelVIH;
    private final PanelDiagnosticos panelDiagnosticos;
    private final PanelVacunas panelVacunas;
    private final VariableSesion variableSesion = VariableSesion.getInstanciaVariablesSession();
    private final PanelOdontologia panelOdontologia;
    private final PanelReferencia panelReferencia;
    private final PanelPrescripcion panelPrescripcion;
    private final PanelProcedimientos panelProcedimientos;
    private final PanelOrdenMedica panelOrdenMedica;
    private final PanelDatosSIVAN panelDatosSIVAN;
    private WizardPage pageUltimateWizard = null;
    private boolean loadingPageWizard;

    @Autowired
    public WizardFactory(
            PanelAdmision panelAdmision,
            PanelGruposPrioritarios panelGruposPrioritarios,
            PanelGruposVulnerables panelGruposVulnerables,
            PanelViolencia panelViolencia,
            PanelDatosAntropometricos panelDatosAntropometricos,
            PanelInformacionObstetrica panelInformacionObstetrica,
            PanelVIH panelVIH,
            PanelDiagnosticos panelDiagnosticos,
            PanelVacunas panelVacunas,
            PanelOdontologia panelOdontologia,
            PanelReferencia panelReferencia,
            PanelPrescripcion panelPrescripcion,
            PanelProcedimientos panelProcedimientos,
            PanelOrdenMedica panelOrdenMedica,
            PanelDatosSIVAN panelDatosSIVAN
    ) {
        this.loadingPageWizard = false;
        this.panelAdmision = panelAdmision;
        this.panelGruposPrioritarios = panelGruposPrioritarios;
        this.panelGruposVulnerables = panelGruposVulnerables;
        this.panelViolencia = panelViolencia;
        this.panelDatosAntropometricos = panelDatosAntropometricos;
        this.panelInformacionObstetrica = panelInformacionObstetrica;
        this.panelVIH = panelVIH;
        this.panelDiagnosticos = panelDiagnosticos;
        this.panelVacunas = panelVacunas;
        this.panelOdontologia = panelOdontologia;
        this.panelReferencia = panelReferencia;
        this.panelPrescripcion = panelPrescripcion;
        this.panelProcedimientos = panelProcedimientos;
        this.panelOrdenMedica = panelOrdenMedica;
        this.panelDatosSIVAN = panelDatosSIVAN;
    }

    @Override
    public WizardPage createPage(List<WizardPage> path, WizardSettings settings) {
        logs.debug("Creando pagina {}", path.size());
        WizardPage page = buildPage(path.size(), path, settings);
        logs.debug("Retornando pagina: {}", page);
        return page;
    }

    private WizardPage buildPage(int pageCount, List<WizardPage> newPage, final WizardSettings settings) {
    	loadingPageWizard = false;
        List<WizardPage> listDinamica = new ArrayList<>();
        listDinamica.add(panelAdmision);
        Integer edadAniosMesDias = 0;
        if (variableSesion.getCodeFormsPanels() != null && !variableSesion.getCodeFormsPanels().isEmpty()) {
        	edadAniosMesDias = Integer.parseInt(concatenarEdad(variableSesion.getAniosPersona(), variableSesion.getMesesPersona(), variableSesion.getDiasPersona()));
            // 1 Registro paciente
            if (variableSesion.getCodeFormsPanels().contains(PanelsFormsCodeConstants.FORMAFRONTEND_PANEL_ADMISION)) {
                if (!newPage.contains(panelAdmision)) {
                    listDinamica.add(panelAdmision);
                }
            }
            // 2 Grupos Prioritarios
            if (variableSesion.getCodeFormsPanels().contains(PanelsFormsCodeConstants.FORMAFRONTEND_PANEL_GP)) {
                listDinamica.add(panelGruposPrioritarios);
            }
            // 4 Datos Antropometricos
            if (variableSesion.getCodeFormsPanels().contains(PanelsFormsCodeConstants.FORMAFRONTEND_PANEL_DANT)) {
                listDinamica.add(panelDatosAntropometricos);
            }
            // 5 Informacion Obstetrica
            if (variableSesion.getCodeFormsPanels().contains(PanelsFormsCodeConstants.FORMAFRONTEND_PANEL_OBST)) {
                if (variableSesion.getIsMujer() != null && variableSesion.getIsMujer()) {
                    if (edadAniosMesDias >= 80000 && edadAniosMesDias <= 590000) {
                        if (variableSesion.getIsEmbarazada() != null && variableSesion.getIsEmbarazada()) {
                            listDinamica.add(panelInformacionObstetrica);
                        }
                    }
                }
            }
            // 7 Vacunacion
            if (variableSesion.getCodeFormsPanels().contains(PanelsFormsCodeConstants.FORMAFRONTEND_PANEL_VACU)) {
                if ((variableSesion.getIsHombre() != null && variableSesion.getIsHombre()) 
                		|| (variableSesion.getIsMujer() != null && variableSesion.getIsMujer())
                		|| (variableSesion.getIsInterSexual() != null && variableSesion.getIsInterSexual())) {
                        listDinamica.add(panelVacunas);
                }
            }
            // 8 Datos Nutricionales
            if (variableSesion.getCodeFormsPanels().contains(PanelsFormsCodeConstants.FORMAFRONTEND_PANEL_SIVAN)) {
            	if ((variableSesion.getIsHombre() != null && variableSesion.getIsHombre()) 
            			&& (variableSesion.getAniosPersona() != null && edadAniosMesDias < 900)) {
                        listDinamica.add(panelDatosSIVAN);
                }
                if (variableSesion.getIsMujer() != null && variableSesion.getIsMujer() 
                		&& (variableSesion.getAniosPersona() != null && edadAniosMesDias < 560000)) {
                        listDinamica.add(panelDatosSIVAN);
                }
            }
            // 9 Orden Medica
            if (variableSesion.getCodeFormsPanels().contains(PanelsFormsCodeConstants.FORMAFRONTEND_PANEL_EXLAB)) {
                if (variableSesion.getIsMujer() != null && variableSesion.getIsMujer()) {
                    if ((variableSesion.getAniosPersona() != null && edadAniosMesDias >= 90000) && (variableSesion.getAniosPersona() != null && edadAniosMesDias <= 560000)) {
                        if (variableSesion.getIsEmbarazada() != null && variableSesion.getIsEmbarazada()
                        		&& variableSesion.getSemanasGestacion() != null && variableSesion.getSemanasGestacion() >= 1 && variableSesion.getSemanasGestacion() <= 42) {
                            listDinamica.add(panelOrdenMedica);
                        }
                    }
                }
            }
            //10 Diagnosticos
            if (variableSesion.getCodeFormsPanels().contains(PanelsFormsCodeConstants.FORMAFRONTEND_PANEL_DIAG)) {
            	if (variableSesion.getCtEspecialidad() != null && 
            		(!variableSesion.getCtEspecialidad().getId().equals(Integer.parseInt(ComunEnum.IDENTIFICADOR_ENFERMERIA.getDescripcion())) &&
            		!variableSesion.getCtEspecialidad().getId().equals(Integer.parseInt(ComunEnum.IDENTIFICADOR_ENFERMERIA_RURAL.getDescripcion())) &&
            		!variableSesion.getCtEspecialidad().getId().equals(Integer.parseInt(ComunEnum.IDENTIFICADOR_AUXILIAR_ENFERMERIA.getDescripcion())))) {
            		listDinamica.add(panelDiagnosticos);
            	} else {
            		if(variableSesion.getAniosPersona() != null && edadAniosMesDias >= 90000) {
            			listDinamica.add(panelDiagnosticos);
            		}
            	}
            }
            // 3 Violencia de Genero
            if (variableSesion.getCodeFormsPanels().contains(PanelsFormsCodeConstants.FORMAFRONTEND_PANEL_VIOLENCIA)) {
                listDinamica.add(panelViolencia);
            }
            // 6 VIH
            if (variableSesion.getCodeFormsPanels().contains(PanelsFormsCodeConstants.FORMAFRONTEND_PANEL_VIH)) {
                listDinamica.add(panelVIH);
            }
            // 11 Diagnostico
            if (variableSesion.getCodeFormsPanels().contains(PanelsFormsCodeConstants.FORMAFRONTEND_PANEL_PROC)) {
                listDinamica.add(panelProcedimientos);
            }
            //12 Odontologia
            if (variableSesion.getCodeFormsPanels().contains(PanelsFormsCodeConstants.FORMAFRONTEND_PANEL_ODONT)) {
                listDinamica.add(panelOdontologia);
            }
            //13 Prescripcion
            if (variableSesion.getCodeFormsPanels().contains(PanelsFormsCodeConstants.FORMAFRONTEND_PANEL_PRESC)) {
                if(variableSesion.getAniosPersona() != null && edadAniosMesDias < 50000) {
                    listDinamica.add(panelPrescripcion);
                } else if (variableSesion.getAniosPersona() != null && variableSesion.getAniosPersona() > 8) {
                    if ((variableSesion.getIsMujer() != null && variableSesion.getIsMujer()) && (variableSesion.getIsEmbarazada() != null && variableSesion.getIsEmbarazada())) {
                        listDinamica.add(panelPrescripcion);
                    }
                }
            }
            // 14 Grupos Vulnerables
            if (variableSesion.getCodeFormsPanels().contains(PanelsFormsCodeConstants.FORMAFRONTEND_PANEL_GV)) {
                listDinamica.add(panelGruposVulnerables);
            }
            // 15 referencia
            if (variableSesion.getCodeFormsPanels().contains(PanelsFormsCodeConstants.FORMAFRONTEND_PANEL_REFINT)) {
                listDinamica.add(panelReferencia);
            }
        }

        //Obtenermos el ultimo elemento de la lista dinamica para poder saber cual es el ultimo panel
        //y renderizar de manera dinamica el boton finalizar
        if (variableSesion.getCodeFormsPanels() == null) {
            //Le asignamos por default el panel de admision
            pageUltimateWizard = getPageWizard(PanelsFormsCodeConstants.FORMAFRONTEND_PANEL_ADMISION);
        } else {
            //Buscamos el ultimo elemento de lista de paneles y especialidades
            if (pageUltimateWizard instanceof PanelAdmision && !loadingPageWizard) {
                //Habilitamos a true si entra por aqui
                loadingPageWizard = true;
                //Obtenermos la ultima pagina del Wizard
                pageUltimateWizard = getPageWizard((variableSesion.getCodeFormsPanels().get(variableSesion.getCodeFormsPanels().size() - 1)));
            }
        }

        listDinamica.iterator().forEachRemaining((WizardPage action) -> {
            try {
                if (pageUltimateWizard != null) {
                	logs.debug(pageUltimateWizard.getDescription());
                    if (action.getController().getPath().contains(pageUltimateWizard)) {
                        action.getController().setFinishEnabled(true);
                        action.getController().setNextEnabled(false);
                    }
                }
            } catch (Exception e) {
                e.getMessage();
            }
        });

        return (listDinamica.size() > pageCount) ? listDinamica.get(pageCount) : listDinamica.get(pageCount - 1);
    }

    private String concatenarEdad(Integer edadAnios, Integer edadMeses, Integer edadDias) {
        return StringUtils.leftPad(edadAnios.toString(), 3, "0") + StringUtils.leftPad(edadMeses.toString(), 2, "0")
                + StringUtils.leftPad(edadDias.toString(), 2, "0");
    }

    @Override
    public boolean isTransient(List<WizardPage> path, WizardSettings settings) {
        return true;
    }

    public WizardPage getPageWizard(String codePanel) {
        WizardPage currentPage = null;
        switch (codePanel) {
            case PanelsFormsCodeConstants.FORMAFRONTEND_PANEL_ADMISION:
                currentPage = panelAdmision;
                break;
            case PanelsFormsCodeConstants.FORMAFRONTEND_PANEL_GP:
                currentPage = panelGruposPrioritarios;
                break;
            case PanelsFormsCodeConstants.FORMAFRONTEND_PANEL_GV:
                currentPage = panelGruposVulnerables;
                break;
            case PanelsFormsCodeConstants.FORMAFRONTEND_PANEL_VIOLENCIA:
                currentPage = panelViolencia;
                break;
            case PanelsFormsCodeConstants.FORMAFRONTEND_PANEL_DANT:
                currentPage = panelDatosAntropometricos;
                break;
            case PanelsFormsCodeConstants.FORMAFRONTEND_PANEL_OBST:
                currentPage = panelInformacionObstetrica;
                break;
            case PanelsFormsCodeConstants.FORMAFRONTEND_PANEL_VIH:
                currentPage = panelVIH;
                break;
            case PanelsFormsCodeConstants.FORMAFRONTEND_PANEL_DIAG:
                currentPage = panelDiagnosticos;
                break;
            case PanelsFormsCodeConstants.FORMAFRONTEND_PANEL_SIVAN:
                currentPage = panelDatosSIVAN;
                break;
            case PanelsFormsCodeConstants.FORMAFRONTEND_PANEL_VACU:
                currentPage = panelVacunas;
                break;
            case PanelsFormsCodeConstants.FORMAFRONTEND_PANEL_EXLAB:
                currentPage = panelOrdenMedica;
                break;
            case PanelsFormsCodeConstants.FORMAFRONTEND_PANEL_PROC:
                currentPage = panelProcedimientos;
                break;
            case PanelsFormsCodeConstants.FORMAFRONTEND_PANEL_ODONT:
                currentPage = panelOdontologia;
                break;
            case PanelsFormsCodeConstants.FORMAFRONTEND_PANEL_PRESC:
                currentPage = panelPrescripcion;
                break;
            case PanelsFormsCodeConstants.FORMAFRONTEND_PANEL_REFINT:
                currentPage = panelReferencia;
                break;
        }

        return currentPage;
    }

}

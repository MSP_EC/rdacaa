/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.gob.msp.rdacaa.amed.controller;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.swing.JComboBox;
import javax.swing.JTextField;

import org.jdesktop.swingx.autocomplete.AutoCompleteDecorator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import ec.gob.msp.rdacaa.admision.model.CmbDetalleCatalogoCellRenderer;
import ec.gob.msp.rdacaa.admision.model.DetalleCatalogoToStringConverter;
import ec.gob.msp.rdacaa.amed.forma.ViolenciaPanel;
import ec.gob.msp.rdacaa.amed.model.AmedIdentificaAgresorModel;
import ec.gob.msp.rdacaa.amed.model.AmedLesionesOcurridasModel;
import ec.gob.msp.rdacaa.amed.model.AmedParentescoAgresorModel;
import ec.gob.msp.rdacaa.business.entity.Atencionviolencia;
import ec.gob.msp.rdacaa.business.entity.Detallecatalogo;
import ec.gob.msp.rdacaa.business.service.DetallecatalogoService;
import ec.gob.msp.rdacaa.business.validation.NumeroFormViolenciaValidator;
import ec.gob.msp.rdacaa.business.validation.common.ValidationResult;
import ec.gob.msp.rdacaa.frontend.sesion.VariableSesion;
import ec.gob.msp.rdacaa.utilitario.forma.UtilitarioForma;

/**
 *
 * @author jazael.faubla
 */
@Controller
public class ViolenciaController {

    private final ViolenciaPanel violenciaPanelFrame;
    private final DetallecatalogoService detallecatalogoService;
    private JTextField txtserie;
    private JComboBox<Detallecatalogo> cmbLesionesOcurridas;
    private JComboBox<Detallecatalogo> cmbParentescoAgresor;
    private JComboBox<Detallecatalogo> cmbIdentificaAgresor;
    private final AmedParentescoAgresorModel amedParentescoAgresorModel;
    private final AmedLesionesOcurridasModel amedLesionesOcurridasModel;
    private final AmedIdentificaAgresorModel amedIdentificaAgresorModel;
    private final VariableSesion variableSesion = VariableSesion.getInstanciaVariablesSession();
    private Detallecatalogo detallecatalogoNull;
    List<Detallecatalogo> lesionesOcurridas = new ArrayList<>();
    List<Detallecatalogo> parentescoAgresor = new ArrayList<>();
    List<Detallecatalogo> identificaAgresor = new ArrayList<>();
    private static final String IDENTIFICA_AGRESOR = "Si";
    private final Object[] controlesValidar = new Object[3];
    private NumeroFormViolenciaValidator numeroFormViolenciaValidator;
    private List<ValidationResult> listaErrores;
    
    @Autowired
    public ViolenciaController(ViolenciaPanel violenciaPanelFrame,DetallecatalogoService detallecatalogoService) {
        this.violenciaPanelFrame = violenciaPanelFrame;
        this.amedLesionesOcurridasModel = new AmedLesionesOcurridasModel();
        this.amedParentescoAgresorModel = new AmedParentescoAgresorModel();
        this.amedIdentificaAgresorModel = new AmedIdentificaAgresorModel();
        this.detallecatalogoService = detallecatalogoService;
    }
    
    @PostConstruct
    private void init() {
        
        loadDefaultCombos();
        this.cmbLesionesOcurridas = violenciaPanelFrame.getCmb_lesiones();
        this.cmbParentescoAgresor = violenciaPanelFrame.getCmb_perentesco();
        this.txtserie = violenciaPanelFrame.getTxtserie();
        this.cmbIdentificaAgresor = violenciaPanelFrame.getCmb_identificaagresor();
        addAutocompleteToCombos();                      
        loadLesionesOcurridas();
        loadIdentificaAgresor();
        loadParentescoAgresor();
        
        UtilitarioForma.habilitarControles(false, cmbParentescoAgresor);
        UtilitarioForma.habilitarControles(false, cmbIdentificaAgresor);
        UtilitarioForma.habilitarControles(false, cmbLesionesOcurridas);
        
        this.txtserie = violenciaPanelFrame.getTxtserie();
        txtserie.addKeyListener(new java.awt.event.KeyAdapter() {
        	@Override
            public void keyTyped(java.awt.event.KeyEvent evt) {
                 //System.out.println(evt.getKeyChar());
                UtilitarioForma.validarLongitudCampo(evt, 6);
                UtilitarioForma.soloNumeros(evt);
                 char codigo = evt.getKeyChar();
                if(!(codigo < '0' || codigo > '9') || (codigo == '\b') ){
                     String caracterIngresado = String.valueOf(evt.getKeyChar());
                    JTextField txtserie = (JTextField) evt.getComponent();
                   // System.out.println(txtserie.getText().length());
                    validateNumeroFormVilencia(!caracterIngresado.isEmpty() , txtserie.getText().length());
                }
            }
        });
        
        cmbIdentificaAgresor.addActionListener(dt -> deshabilitarParentesco());
    }
    
     private void validateNumeroFormVilencia(boolean habilitar, int longitudTexto) {
    	if (habilitar) {
                UtilitarioForma.habilitarControles(true, cmbLesionesOcurridas);
                UtilitarioForma.habilitarControles(true, cmbIdentificaAgresor);
    	}
        if (longitudTexto == 0){
            UtilitarioForma.habilitarControles(false, cmbLesionesOcurridas);
            UtilitarioForma.habilitarControles(false, cmbIdentificaAgresor);
            cmbLesionesOcurridas.setSelectedItem(detallecatalogoNull);
            cmbIdentificaAgresor.setSelectedItem(detallecatalogoNull);
        }
             
    }
    
    public void loadDefaultCombos() {
        detallecatalogoNull = new Detallecatalogo(){
            private static final long serialVersionUID = 1L;
            boolean isNoSelectable = true;
        };
        
        detallecatalogoNull.setId(0);
        detallecatalogoNull.setDescripcion("Seleccione");
    }
    
    public void loadLesionesOcurridas() {
        lesionesOcurridas = detallecatalogoService.findAllLesionesOcurridas();
        amedLesionesOcurridasModel.clear();
        amedLesionesOcurridasModel.addElements(lesionesOcurridas);
        cmbLesionesOcurridas.setRenderer(new CmbDetalleCatalogoCellRenderer());
        cmbLesionesOcurridas.setModel(amedLesionesOcurridasModel);
        cmbLesionesOcurridas.setSelectedItem(detallecatalogoNull);
    }
    
    public void loadIdentificaAgresor() {
        identificaAgresor = detallecatalogoService.findAllIdentificaAgresor();
        amedIdentificaAgresorModel.clear();
        amedIdentificaAgresorModel.addElements(identificaAgresor);
        cmbIdentificaAgresor.setRenderer(new CmbDetalleCatalogoCellRenderer());
        cmbIdentificaAgresor.setModel(amedIdentificaAgresorModel);
        cmbIdentificaAgresor.setSelectedItem(detallecatalogoNull);
    }
    
    
    public void deshabilitarParentesco(){
     if (amedIdentificaAgresorModel.getSelectedItem() != null && amedIdentificaAgresorModel.getSelectedItem().getDescripcion().equals(IDENTIFICA_AGRESOR)){
                UtilitarioForma.habilitarControles(true,cmbParentescoAgresor );
          }else {
         UtilitarioForma.habilitarControles(false,cmbParentescoAgresor );
            cmbParentescoAgresor.setSelectedItem(detallecatalogoNull);
     }
    }
    public void loadParentescoAgresor() {
                parentescoAgresor = detallecatalogoService.findAllParentescoAgresor();
                amedParentescoAgresorModel.clear();
                amedParentescoAgresorModel.addElements(parentescoAgresor);
                cmbParentescoAgresor.setRenderer(new CmbDetalleCatalogoCellRenderer());
                cmbParentescoAgresor.setModel(amedParentescoAgresorModel);
                cmbParentescoAgresor.setSelectedItem(detallecatalogoNull);
    }
    
    private void addAutocompleteToCombos() {
    	AutoCompleteDecorator.decorate(cmbLesionesOcurridas, new DetalleCatalogoToStringConverter());
        AutoCompleteDecorator.decorate(cmbParentescoAgresor, new DetalleCatalogoToStringConverter());
        AutoCompleteDecorator.decorate(cmbIdentificaAgresor, new DetalleCatalogoToStringConverter());
        
    }
    
    public void cleanInput() {
        
            cmbLesionesOcurridas.setSelectedItem(detallecatalogoNull);
            cmbParentescoAgresor.setSelectedItem(detallecatalogoNull);
            cmbIdentificaAgresor.setSelectedItem(detallecatalogoNull);
            cmbLesionesOcurridas.setEnabled(false);
            cmbIdentificaAgresor.setEnabled(false); 
            txtserie.setText("");
    }
    
    public void getViolencia() {
        Atencionviolencia atencionviolencia = new Atencionviolencia();
            if (txtserie.getText().equals("")){
                atencionviolencia.setNumserieformulario("0");
            } else  atencionviolencia.setNumserieformulario(txtserie.getText());

            atencionviolencia.setCtlesionesocurridasId(amedLesionesOcurridasModel.getSelectedItem());
            atencionviolencia.setCtidentificaagresorId(amedIdentificaAgresorModel.getSelectedItem());
            if ( cmbParentescoAgresor.getSelectedItem() != null && cmbParentescoAgresor.getSelectedObjects()[0].equals(detallecatalogoNull)){
                atencionviolencia.setCtparentescoId(null);
            }else atencionviolencia.setCtparentescoId(amedParentescoAgresorModel.getSelectedItem());
            VariableSesion.getInstanciaVariablesSession().setAtencionviolencia(atencionviolencia);        
    }
   
    
    public boolean validateForm() {
        if (txtserie.getText() != null && !txtserie.getText().isEmpty()){
           getViolencia();
        }
        return true;
    }
}

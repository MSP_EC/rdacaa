/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.gob.msp.rdacaa.principal.wizard;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.github.cjwizard.WizardSettings;

import ec.gob.msp.rdacaa.amed.controller.VacunasController;
import ec.gob.msp.rdacaa.amed.forma.Vacunas;
import ec.gob.msp.rdacaa.principal.controller.CommonPanelController;

/**
 *
 * @author eduardo
 */
@Component
public class PanelVacunas extends com.github.cjwizard.WizardPage{
    
    private final Vacunas vacunas;
    private final VacunasController vacunasController;
    private final CommonPanelController commonPanelController;

    @Autowired
    public PanelVacunas(Vacunas vacunas, VacunasController vacunasController, CommonPanelController commonPanelController) {
        super("Registro de Vacunas", "Atención Médica");
        this.vacunas = vacunas;
        this.vacunasController = vacunasController;
        this.commonPanelController = commonPanelController;
    }
    
    @PostConstruct
    private void init() {
        vacunas.setVisible(true);
        add(vacunas);
    }
    
    @Override
    public void updateSettings(WizardSettings settings) {
        commonPanelController.refreshPanels();
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.gob.msp.rdacaa.principal.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import ec.gob.msp.rdacaa.frontend.sesion.VariableSesion;
import ec.gob.msp.rdacaa.principal.forma.EscritorioPrincipal;

@Controller
public class EscritorioPrincipalController {
    
    private final EscritorioPrincipal frame;
//    private CierreatencionesService cierreatencionesService;
    
    @Autowired
    public EscritorioPrincipalController(EscritorioPrincipal frame
//    		,CierreatencionesService cierreatencionesService
                                         ) {
        this.frame = frame;
//        this.cierreatencionesService = cierreatencionesService;
    }
    
    public void setVs(VariableSesion vs) {
        frame.setVs(vs);
    }
    
    public void prepareAndOpenFrame() {
        frame.setVisible(true);
    }
    
//    public void validateCierre(){
//        if (VariableSesion.getInstanciaVariablesSession().getUsuario() == null) {
//            frame.disableControls();
//        } else {
//            Cierreatenciones actual = cierreatencionesService.findOneByUsuarioId(VariableSesion.getInstanciaVariablesSession().getUsuario().getId());
//            if (actual != null) {
//                if (FechasUtil.validarEmisionCierre(Integer.valueOf(actual.getAnio()), actual.getMes())) {
//                    frame.disableControls();
//                } else {
//                    frame.disableControlsCierre();
//                    frame.disableControls();
//                }
//            }
//        }
//    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.gob.msp.rdacaa.amed.model;

import ec.gob.msp.rdacaa.business.entity.Atenciongvulnerable;
import ec.gob.msp.rdacaa.utilitario.forma.ModeloTablaGenerico;

/**
 *
 * @author eduardo
 */
public class GrupoVulnerableTablaModel extends ModeloTablaGenerico<Atenciongvulnerable>{
    
    public GrupoVulnerableTablaModel(String[] columnas) {
        super(columnas);
    }
    
    private static final long serialVersionUID = -2176409506747171621L;
    
    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Atenciongvulnerable obj =  getListaDatos().get(rowIndex);
        switch (columnIndex){
            case 0:
                return obj;
            case 1:   
                return obj.getCtgvulnerableId().getDescripcion();    
            case 2:
                return "Eliminar";
            default:
                return "";
        }
    }
    
    @Override
    public boolean isCellEditable(int row, int col) {
        return col == 2;
    }

    @Override
    public void setValueAt(Object value, int row, int col) {
        fireTableCellUpdated(row, col);
    }
}

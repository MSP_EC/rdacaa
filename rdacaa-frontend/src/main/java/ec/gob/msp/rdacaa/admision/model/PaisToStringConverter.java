/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.gob.msp.rdacaa.admision.model;

import ec.gob.msp.rdacaa.business.entity.Pais;
import ec.gob.msp.rdacaa.control.swing.model.CustomObjectToStringConverter;

/**
 *
 * @author dmurillo
 */
public class PaisToStringConverter extends CustomObjectToStringConverter {

    @Override
    public String getPreferredStringForItem(Object o) {
        if (o == null) {
            return null;
        } else if (o instanceof Pais) {
            return ((Pais) o).getNacionalidad();
        } else {
            return o.toString();
        }
    }
}

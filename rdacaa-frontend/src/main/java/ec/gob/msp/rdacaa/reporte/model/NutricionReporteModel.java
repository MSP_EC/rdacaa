package ec.gob.msp.rdacaa.reporte.model;

import ec.gob.msp.rdacaa.utilitario.fecha.FechasUtil;
import ec.gob.msp.rdacaa.utilitario.forma.ModeloTablaGenerico;

public class NutricionReporteModel extends ModeloTablaGenerico<NutricionReporte> {

	private static final long serialVersionUID = 2929270639906285220L;
	private static final String ESPACIO = " ";

	public NutricionReporteModel(String[] columnas) {
		super(columnas);
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		NutricionReporte obj = getListaDatos().get(rowIndex);
                String tipoParroquia;
                if(obj.getPaciente().getParroquiaId().getCttipoId() != null){
                    tipoParroquia = obj.getPaciente().getParroquiaId().getCttipoId().getDescripcion();
                }else{
                    tipoParroquia = "";
                }
                
                String lugarAtencion;
                if(obj.getLugarAtencion() != null){
                    lugarAtencion = obj.getLugarAtencion().getCtlugaratencionId().getDescripcion();
                }else{
                    lugarAtencion = "";
                }
                
		switch (columnIndex) {
		case 0:
			return "";
		case 1:
			return obj.getEntidad().getId();
		case 2:
			return obj.getEntidad().getNombreoficial();
		case 3:
			StringBuilder nombrePro = new StringBuilder();
			nombrePro.append(obj.getProfesional().getPrimerapellido());
			nombrePro.append(ESPACIO);
			nombrePro.append(obj.getProfesional().getSegundoapellido() == null ? "" :  obj.getProfesional().getSegundoapellido());
			nombrePro.append(ESPACIO);
			nombrePro.append(obj.getProfesional().getPrimernombre());
			nombrePro.append(ESPACIO);
			nombrePro.append(obj.getProfesional().getSegundonombre() == null ? "" : obj.getProfesional().getSegundonombre());
			return nombrePro;
		case 4:
			return obj.getAtencionmedica().getCtespecialidadmedicaId().getDescripcion();
		case 5:
			return obj.getPaciente().getNumerohistoriaclinica();
		case 6:
			StringBuilder nombre = new StringBuilder();
			nombre.append(obj.getPaciente().getPrimernombre());
			nombre.append(ESPACIO);
			nombre.append(obj.getPaciente().getSegundonombre() == null ? "" : obj.getPaciente().getSegundonombre());
			return nombre;
		case 7:StringBuilder apellido = new StringBuilder();
			apellido.append(obj.getPaciente().getPrimerapellido());
			apellido.append(ESPACIO);
			apellido.append(obj.getPaciente().getSegundoapellido() == null ? "" : obj.getPaciente().getSegundoapellido());
			return apellido;
		case 8:
			return obj.getPaciente().getFechanacimiento();
		case 9:
			return obj.getPaciente().getCtsexoId().getDescripcion();
		case 10:
			return obj.getPaciente().getPaisId().getDescripcion();
		case 11:
			return obj.getPaciente().getParroquiaId().getCantonId().getProvinciaId().getDescripcion();
		case 12:
			return obj.getPaciente().getParroquiaId().getCantonId().getDescripcion();
		case 13:
			return obj.getPaciente().getParroquiaId().getDescripcion();
		case 14:
			return tipoParroquia;
		case 15:
			return "";
		case 16:
			return obj.getPaciente().getDirecciondomicilio();
		case 17:
			return "";
		case 18:
			return obj.getAtencionmedica().getFechaatencion();
		case 19:
			StringBuilder edadAnios = new StringBuilder();
			edadAnios.append(FechasUtil.getDiffDatesDesdeHasta(obj.getPaciente().getFechanacimiento(), obj.getAtencionmedica().getFechaatencion(),0));
			return edadAnios;
		case 20:
			StringBuilder edadMeses = new StringBuilder();
			edadMeses.append(FechasUtil.getDiffDatesDesdeHasta(obj.getPaciente().getFechanacimiento(), obj.getAtencionmedica().getFechaatencion(),1));
			return edadMeses;
		case 21:
			StringBuilder edadDias = new StringBuilder();
			edadDias.append(FechasUtil.getDiffDatesDesdeHasta(obj.getPaciente().getFechanacimiento(), obj.getAtencionmedica().getFechaatencion(),2));
			return edadDias;
		case 22:
			return obj.getDa().getDatAntPeso();
		case 23:
			return obj.getDa().getDatAntTalla();
		case 24:
			return obj.getDa().getDatAntHbCorregido();
		case 25:
			return obj.getDa().getDatAntPuntajeZTe();
		case 26:
			return obj.getDa().getDatAntPuntajeZPe();
		case 27:
			return obj.getDa().getDatAntPuntajeZImcE();
		case 28:
			return obj.getDa().getCodIndAnemiaDesc();
		case 29:
			return obj.getDa().getDatAntCategoriaTeDesc();
		case 30:
			return obj.getDa().getDatAntCategoriaPeDesc();
		case 31:
			return obj.getDa().getDatAntCategoriaImcEDesc();
		case 32:
			return obj.getVp().getPreSupHieMultivitaminasDesc();
		case 33:
			return obj.getVp().getPreSupVitaminaADesc();
		case 34:
			return obj.getSc().getSiv24hLecheDesc();
		case 35:
			return obj.getSc().getSiv24hAlimentosDesc();
		case 36:
			return lugarAtencion;
		default:
			return "";
		}
	}

}

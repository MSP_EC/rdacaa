/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.gob.msp.rdacaa.amed.model;

import java.awt.Component;

import javax.swing.DefaultListCellRenderer;
import javax.swing.JList;

import ec.gob.msp.rdacaa.business.entity.Detalleserviciotarifario;

/**
 *
 * @author dmurillo
 */
public class CmbDetalleserviciotarifarioCellRenderer extends DefaultListCellRenderer {

	private static final long serialVersionUID = 3919050479663324210L;

	@Override
	public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected,
			boolean cellHasFocus) {
		if (value instanceof Detalleserviciotarifario) {
			Detalleserviciotarifario detalleserviciotarifario = (Detalleserviciotarifario) value;
			if (detalleserviciotarifario.getId().equals(0)) {
				value = detalleserviciotarifario.getDescripcion();
			} else {
				value = detalleserviciotarifario.getCodigo() + " - " + detalleserviciotarifario.getDescripcion();
			}
		}
		return super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
	}

}

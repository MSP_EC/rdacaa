/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.gob.msp.rdacaa.amed.controller;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.swing.ButtonGroup;
import javax.swing.JComboBox;
import javax.swing.JRadioButton;

import org.jdesktop.swingx.autocomplete.AutoCompleteDecorator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import ec.gob.msp.rdacaa.admision.model.CmbDetalleCatalogoCellRenderer;
import ec.gob.msp.rdacaa.admision.model.DetalleCatalogoToStringConverter;
import ec.gob.msp.rdacaa.amed.forma.OrdenMedica;
import ec.gob.msp.rdacaa.amed.model.AmedCmbResultadoExamenBactModel;
import ec.gob.msp.rdacaa.business.entity.Detallecatalogo;
import ec.gob.msp.rdacaa.business.entity.Examenlaboratorio;
import ec.gob.msp.rdacaa.business.service.DetallecatalogoService;
import ec.gob.msp.rdacaa.frontend.sesion.VariableSesion;

/**
 *
 * @author eduardo
 */
@Controller
public class OrdenMedicaController {

	private final OrdenMedica ordenMedicaFrame;
	private final DetallecatalogoService detallecatalogoService;
	private final AmedCmbResultadoExamenBactModel amedCmbResultadoExamenBactModel;
	private JComboBox<Detallecatalogo> cbmResultadoBact;
	private final VariableSesion variableSesion = VariableSesion.getInstanciaVariablesSession();
	private JRadioButton rbnoTreponemicaMas;
	private JRadioButton rbnoTreponemicaMenos;
	private JRadioButton rbnoTreponemicaSD;

	private JRadioButton rbTreponemicaMas;
	private JRadioButton rbTreponemicaMenos;
	private JRadioButton rbTreponemicaSD;

	private JRadioButton rbSifilisMas;
	private JRadioButton rbSifilisMenos;
	private JRadioButton rbSifilisNC;

	private JRadioButton rbSifilisParejaSi;
	private JRadioButton rbSifilisParejaNo;
	private JRadioButton rbSifilisParejaNohizo;

	private ButtonGroup bgNoTreponemica;
	private ButtonGroup bgTreponemica;
	private ButtonGroup bgSifilis;
	private ButtonGroup bgSifilisPareja;
	private Detallecatalogo detallecatalogoNull;

	@Autowired
	public OrdenMedicaController(OrdenMedica ordenMedicaFrame, DetallecatalogoService detallecatalogoService) {
		this.ordenMedicaFrame = ordenMedicaFrame;
		this.detallecatalogoService = detallecatalogoService;
		this.amedCmbResultadoExamenBactModel = new AmedCmbResultadoExamenBactModel();
	}

	@PostConstruct
	private void init() {
		loadDefaultCombos();
		this.cbmResultadoBact = ordenMedicaFrame.getCbmResultadoBact();
		AutoCompleteDecorator.decorate(cbmResultadoBact, new DetalleCatalogoToStringConverter());
		this.rbnoTreponemicaMas = ordenMedicaFrame.getRbnoTreponemicaMas();
		this.rbnoTreponemicaMenos = ordenMedicaFrame.getRbnoTreponemicaMenos();
		this.rbnoTreponemicaSD = ordenMedicaFrame.getRbnoTreponemicaSD();

		this.rbTreponemicaMas = ordenMedicaFrame.getRbTreponemicaMas();
		this.rbTreponemicaMenos = ordenMedicaFrame.getRbTreponemicaMenos();
		this.rbTreponemicaSD = ordenMedicaFrame.getRbTreponemicaSD();

		this.rbSifilisMas = ordenMedicaFrame.getRbSifilisMas();
		this.rbSifilisMenos = ordenMedicaFrame.getRbSifilisMenos();
		this.rbSifilisNC = ordenMedicaFrame.getRbSifilisNC();

		this.rbSifilisParejaSi = ordenMedicaFrame.getRbSifilisParejaSi();
		this.rbSifilisParejaNo = ordenMedicaFrame.getRbSifilisParejaNo();
		this.rbSifilisParejaNohizo = ordenMedicaFrame.getRbSifilisParejaNohizo();

		this.bgNoTreponemica = ordenMedicaFrame.getBgNoTreponemica();
		this.bgTreponemica = ordenMedicaFrame.getBgTreponemica();
		this.bgSifilis = ordenMedicaFrame.getBgSifilis();
		this.bgSifilisPareja = ordenMedicaFrame.getBgSifilisPareja();
		
		loadResultadoExamen();
	}

	private void loadResultadoExamen() {
		List<Detallecatalogo> listaResultadoExamen = detallecatalogoService.findAllResultadoExamen();
		amedCmbResultadoExamenBactModel.clear();
		amedCmbResultadoExamenBactModel.addElements(listaResultadoExamen);
		cbmResultadoBact.setRenderer(new CmbDetalleCatalogoCellRenderer());
		cbmResultadoBact.setModel(amedCmbResultadoExamenBactModel);
		cbmResultadoBact.setSelectedItem(detallecatalogoNull);
	}

	private Examenlaboratorio getExamenLaboratorio() {
		Examenlaboratorio examenlaboratorio = new Examenlaboratorio();
		if (rbnoTreponemicaMas.isSelected()) {
			examenlaboratorio.setNotreponemica(1);
		} else if (rbnoTreponemicaMenos.isSelected()) {
			examenlaboratorio.setNotreponemica(2);
		} else if (rbnoTreponemicaSD.isSelected()) {
			examenlaboratorio.setNotreponemica(3);
		} else {
			examenlaboratorio.setNotreponemica(null);
		}

		if (rbTreponemicaMas.isSelected()) {
			examenlaboratorio.setTreponemica(1);
		} else if (rbTreponemicaMenos.isSelected()) {
			examenlaboratorio.setTreponemica(2);
		} else if (rbTreponemicaSD.isSelected()) {
			examenlaboratorio.setTreponemica(3);
		} else {
			examenlaboratorio.setTreponemica(null);
		}

		if (rbSifilisMas.isSelected()) {
			examenlaboratorio.setTratamiento(1);
		} else if (rbSifilisMenos.isSelected()) {
			examenlaboratorio.setTratamiento(2);
		} else if (rbSifilisNC.isSelected()) {
			examenlaboratorio.setTratamiento(3);
		} else {
			examenlaboratorio.setTratamiento(null);
		}

		if (rbSifilisParejaSi.isSelected()) {
			examenlaboratorio.setTratamientoprja(1);
		} else if (rbSifilisParejaNo.isSelected()) {
			examenlaboratorio.setTratamientoprja(2);
		} else if (rbSifilisParejaNohizo.isSelected()) {
			examenlaboratorio.setTratamientoprja(3);
		} else {
			examenlaboratorio.setTratamientoprja(null);
		}

		if (!amedCmbResultadoExamenBactModel.getSelectedItem().equals(detallecatalogoNull)) {
			examenlaboratorio.setCtresultadoexmnId(amedCmbResultadoExamenBactModel.getSelectedItem());
		}

		if (examenlaboratorio.getNotreponemica() == null && examenlaboratorio.getTreponemica() == null
				&& examenlaboratorio.getTratamiento() == null && examenlaboratorio.getTratamientoprja() == null
				&& amedCmbResultadoExamenBactModel.getSelectedItem().equals(detallecatalogoNull)) {
			examenlaboratorio = null;
		}

		return examenlaboratorio;
	}

	public void loadDefaultCombos() {
		detallecatalogoNull = new Detallecatalogo() {
			private static final long serialVersionUID = 1L;
			boolean isNoSelectable = true;
		};
		detallecatalogoNull.setId(0);
		detallecatalogoNull.setDescripcion("Seleccione");
	}

	public void cleanInput() {
		bgNoTreponemica.clearSelection();
		bgTreponemica.clearSelection();
		bgSifilis.clearSelection();
		bgSifilisPareja.clearSelection();
		cbmResultadoBact.setSelectedItem(detallecatalogoNull);
	}

	public boolean validateForm() {

		variableSesion.setExamenlaboratorio(getExamenLaboratorio());

		return true;
	}

}

package ec.gob.msp.rdacaa.admision.model;

import java.awt.Component;

import javax.swing.DefaultListCellRenderer;
import javax.swing.JList;

import ec.gob.msp.rdacaa.business.entity.Pais;


public class CmbNacionalidadCellRenderer extends DefaultListCellRenderer {
	
	private static final long serialVersionUID = -1911821062776433300L;
	@Override
	public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected,
			boolean cellHasFocus) {
		if (value instanceof Pais) {
			Pais pais = (Pais) value;
			value = pais.getNacionalidad();
		}
		return super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
	}
}
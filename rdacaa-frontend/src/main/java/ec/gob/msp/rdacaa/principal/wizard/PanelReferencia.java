/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.gob.msp.rdacaa.principal.wizard;

import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.github.cjwizard.WizardPage;
import com.github.cjwizard.WizardSettings;

import ec.gob.msp.rdacaa.amed.controller.ReferenciaController;
import ec.gob.msp.rdacaa.amed.forma.Referencia;
import ec.gob.msp.rdacaa.principal.controller.CommonPanelController;

/**
 *
 * @author eduardo
 */
@Component
public class PanelReferencia extends com.github.cjwizard.WizardPage {

	private static final long serialVersionUID = 8839184259561209902L;
	private Referencia referencia;
	private ReferenciaController referenciaController;
	private CommonPanelController commonPanelController;

	@Autowired
	public PanelReferencia(Referencia referencia, ReferenciaController referenciaController,
			CommonPanelController commonPanelController) {
		super("Registro referencia", "Atención Médica");
		this.referencia = referencia;
		this.referenciaController = referenciaController;
		this.commonPanelController = commonPanelController;
	}

	@PostConstruct
	private void init() {
		referencia.setVisible(true);
		add(referencia);
	}

	@Override
	public boolean onNext(WizardSettings settings) {
		return referenciaController.validateForm();
	}

	@Override
	public void updateSettings(WizardSettings settings) {
		commonPanelController.refreshPanels();
	}

	/*
	 * @Override public void rendering(List<WizardPage> path, WizardSettings
	 * settings) { super.rendering(path, settings); setFinishEnabled(true);
	 * setNextEnabled(false); }
	 */
}

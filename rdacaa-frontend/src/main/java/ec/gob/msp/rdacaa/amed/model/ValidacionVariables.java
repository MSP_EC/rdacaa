package ec.gob.msp.rdacaa.amed.model;

public enum ValidacionVariables {
	
	CARGA_INICIAL_NO_VALIDACION, ARCHIVO_NULO, ARCHIVO_CON_ERRORES, ARCHIVO_SIN_ERRORES
	
}
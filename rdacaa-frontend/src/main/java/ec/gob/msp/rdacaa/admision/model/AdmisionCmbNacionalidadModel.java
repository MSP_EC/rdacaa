/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.gob.msp.rdacaa.admision.model;


import ec.gob.msp.rdacaa.control.swing.model.DefaultComboBoxModel;
import org.springframework.stereotype.Component;
import ec.gob.msp.rdacaa.business.entity.Pais;

/**
 *
 * @author msp
 */
@Component
public class AdmisionCmbNacionalidadModel extends DefaultComboBoxModel<Pais> {
	
    private static final long serialVersionUID = 5409151842146816713L;
}

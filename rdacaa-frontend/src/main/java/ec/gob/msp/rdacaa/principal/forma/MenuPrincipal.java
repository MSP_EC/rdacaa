/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.gob.msp.rdacaa.principal.forma;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.beans.PropertyVetoException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.swing.JDesktopPane;
import javax.swing.JFrame;
import javax.swing.JInternalFrame;

import org.springframework.stereotype.Component;

import ec.gob.msp.rdacaa.admision.forma.Admision;
import ec.gob.msp.rdacaa.frontend.sesion.VariableSesion;
import lombok.Getter;
import lombok.Setter;

/**
 *
 * @author dmurillo
 */
@Component
public class MenuPrincipal extends JFrame {

	private static final long serialVersionUID = -3300706782358540554L;
	@Getter
	@Setter
	private VariableSesion vs;

	@Getter
	@Setter
	private Admision admision;

	@PostConstruct
	private void init() {
//		this.admision = admision;
		initComponents();
		setExtendedState(MAXIMIZED_BOTH);
		configurarPaneles();
	}

	/**
	 * Creates new form MenuPrincipal
	 */
//	@Autowired
//	public MenuPrincipal(Admision admision) {
//		this.admision = admision;
//	}

	private void configurarPaneles() {
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();

//		agregarPanel(admision, pnlContenedor, "ADMISION", screenSize.width, screenSize.height);
	}

	public void agregarPanel(JInternalFrame iFrame, JDesktopPane jpanel, String titulo, int iAncho, int iAlto) {
		try {
			iFrame.setSize(iAncho, iAlto);
			jpanel.add(iFrame);
			iFrame.setVisible(true);
			iFrame.setSelected(true);
			iFrame.setTitle(titulo);
		} catch (PropertyVetoException ex) {
			Logger.getLogger(MenuPrincipal.class.getName()).log(Level.SEVERE, null, ex);
		}
	}

	@SuppressWarnings("unchecked")
	// <editor-fold defaultstate="collapsed"
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        pnlContenedor = new javax.swing.JDesktopPane();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        pnlContenedor.setPreferredSize(new java.awt.Dimension(1111, 831));

        javax.swing.GroupLayout pnlContenedorLayout = new javax.swing.GroupLayout(pnlContenedor);
        pnlContenedor.setLayout(pnlContenedorLayout);
        pnlContenedorLayout.setHorizontalGroup(
            pnlContenedorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 1111, Short.MAX_VALUE)
        );
        pnlContenedorLayout.setVerticalGroup(
            pnlContenedorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 767, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(16, 16, 16)
                .addComponent(pnlContenedor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(70, 70, 70)
                .addComponent(pnlContenedor, javax.swing.GroupLayout.PREFERRED_SIZE, 767, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(29, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JDesktopPane pnlContenedor;
    // End of variables declaration//GEN-END:variables
}

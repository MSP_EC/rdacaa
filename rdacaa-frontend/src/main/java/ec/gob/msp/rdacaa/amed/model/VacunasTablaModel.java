package ec.gob.msp.rdacaa.amed.model;

import ec.gob.msp.rdacaa.business.entity.Registrovacunacion;
import ec.gob.msp.rdacaa.utilitario.forma.ModeloTablaGenerico;
import java.text.SimpleDateFormat;

public class VacunasTablaModel extends ModeloTablaGenerico<Registrovacunacion> {

    public VacunasTablaModel(String[] columnas) {
        super(columnas);
    }

    private static final long serialVersionUID = -8259310025068677691L;

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Registrovacunacion obj = (Registrovacunacion) getListaDatos().get(rowIndex);
        
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        String dateString = format.format( obj.getFecharegistro() );
        
        switch (columnIndex) {
            case 0:
                return obj;
            case 1:
                return obj.getEsquemavacunacionId().getVacunaId().getNombrevacuna();
            case 2:
                return obj.getEsquemavacunacionId().getDosis();
            case 3:
                return obj.getLote();
            case 4:
                return obj.getCtgruporiesgoId()== null ? "" :obj.getCtgruporiesgoId().getDescripcion();
            case 5:
                return dateString;
            case 6:
                return "Eliminar";
            default:
                return ""; 
        }
    }
    
    @Override
    public boolean isCellEditable(int row, int col) {
        return col == 6;
    }

    @Override
    public void setValueAt(Object value, int row, int col) {
        fireTableCellUpdated(row, col);
    }

}

package ec.gob.msp.rdacaa.reporte.model;

import ec.gob.msp.rdacaa.utilitario.enumeracion.ComunEnum;
import ec.gob.msp.rdacaa.utilitario.fecha.FechasUtil;
import ec.gob.msp.rdacaa.utilitario.forma.ModeloTablaGenerico;

public class ReporteVacunasTablaModel extends ModeloTablaGenerico<ReporteVacunasModel> {

	public ReporteVacunasTablaModel(String[] columnas) {
		super(columnas);
	}

	private static final long serialVersionUID = 3711367031462145149L;
	private static final String ESPACIO = " ";
	private static final String NOAPLICA = "NO APLICA";

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		ReporteVacunasModel obj = getListaDatos().get(rowIndex);
		switch (columnIndex) {
		case 0:
			return "";
		case 1:
			return rowIndex + 1;
		case 2:
			return obj.getAtencionMedica().getId();
		case 3:
			if(obj.getIntraextramural() != null){
				return obj.getIntraextramural().getCtlugaratencionId().getDescripcion();
			}
			return NOAPLICA;
		case 4:
			return obj.getEntidad().getId();
		case 5:
			return obj.getEntidad().getNombreoficial();
		case 6:
			return obj.getProfesional().getNumeroidentificacion();
		case 7:
			StringBuilder dato = new StringBuilder();
			dato.append(obj.getProfesional().getPrimernombre());
			dato.append(ESPACIO);
			dato.append(obj.getProfesional().getSegundonombre() == null ? "" :  obj.getProfesional().getSegundonombre());
			dato.append(ESPACIO);
			dato.append(obj.getProfesional().getPrimerapellido());
			dato.append(ESPACIO);
			dato.append(obj.getProfesional().getSegundoapellido() == null ? "" : obj.getProfesional().getSegundoapellido());
			return dato;
		case 8:
			if(obj.getProfesional().getCtsexoId() != null){
				return obj.getProfesional().getCtsexoId().getDescripcion();
			}
			return NOAPLICA;
		case 9:
			return FechasUtil.formateadorfechaAString(ComunEnum.PATRON_FECHA6.getDescripcion(), obj.getProfesional().getFechanacimiento());
		case 10:
			if(obj.getProfesional().getParroquiaId() != null){
				return obj.getProfesional().getParroquiaId().getCantonId().getProvinciaId().getDescripcion();
			}
			return NOAPLICA;
		case 11:
			if(obj.getProfesional().getParroquiaId() != null){
				return obj.getProfesional().getParroquiaId().getCantonId().getDescripcion();
			}
			return NOAPLICA;
		case 12:
			if(obj.getProfesional().getParroquiaId() != null){
				return obj.getProfesional().getParroquiaId().getDescripcion();
			}
			return NOAPLICA;
		case 13:
			return obj.getAtencionMedica().getCtespecialidadmedicaId().getDescripcion();
		case 14:
			dato = new StringBuilder();
			dato.append(obj.getPersona().getPrimernombre());
			dato.append(ESPACIO);
			dato.append(obj.getPersona().getSegundonombre() == null ? "" :  obj.getPersona().getSegundonombre());
			dato.append(ESPACIO);
			dato.append(obj.getPersona().getPrimerapellido());
			dato.append(ESPACIO);
			dato.append(obj.getPersona().getSegundoapellido() == null ? "" : obj.getPersona().getSegundoapellido());
			return dato;
		case 15:
			return obj.getPersona().getNumeroidentificacion();
		case 16:
			return obj.getPersona().getCtsexoId().getDescripcion();
		case 17:
			return obj.getPersona().getCtorientacionsexualId().getDescripcion();
		case 18:
			if(obj.getPersona().getCtidentidadgeneroId() != null){
				return obj.getPersona().getCtidentidadgeneroId().getDescripcion();
			}
			return NOAPLICA;
		case 19:
			return FechasUtil.formateadorfechaAString(ComunEnum.PATRON_FECHA6.getDescripcion(), obj.getPersona().getFechanacimiento());
		case 20:
			StringBuilder edadAnios = new StringBuilder();
			edadAnios.append(FechasUtil.getDiffDatesDesdeHasta(obj.getPersona().getFechanacimiento(), obj.getAtencionMedica().getFechaatencion(),0));
			return edadAnios;
		case 21:
			StringBuilder edadMeses = new StringBuilder();
			edadMeses.append(FechasUtil.getDiffDatesDesdeHasta(obj.getPersona().getFechanacimiento(), obj.getAtencionMedica().getFechaatencion(),1));
			return edadMeses;
		case 22:
			StringBuilder edadDias = new StringBuilder();
			edadDias.append(FechasUtil.getDiffDatesDesdeHasta(obj.getPersona().getFechanacimiento(), obj.getAtencionMedica().getFechaatencion(),2));
			return edadDias;
		case 23:
			return obj.getPersona().getPaisId().getDescripcion();
		case 24:
			return obj.getPersona().getCttiposegurosaludId().getDescripcion();
		case 25:
			if(obj.getVariablesGruposPrioritarios().getCodGprPrioritarioDesc1() != null){
				return obj.getVariablesGruposPrioritarios().getCodGprPrioritarioDesc1();
			}
			return NOAPLICA;
		case 26:
			if(obj.getVariablesGruposPrioritarios().getCodGprPrioritarioDesc2() != null){
				return obj.getVariablesGruposPrioritarios().getCodGprPrioritarioDesc2();
			}
			return NOAPLICA;
		case 27:
			if(obj.getVariablesGruposPrioritarios().getCodGprPrioritarioDesc3() != null){
				return obj.getVariablesGruposPrioritarios().getCodGprPrioritarioDesc3();
			}
			return NOAPLICA;
		case 28:
			if(obj.getVariablesGruposPrioritarios().getCodGprPrioritarioDesc4() != null){
				return obj.getVariablesGruposPrioritarios().getCodGprPrioritarioDesc4();
			}
			return NOAPLICA;
		case 29:
			if(obj.getVariablesGruposPrioritarios().getCodGprPrioritarioDesc5() != null){
				return obj.getVariablesGruposPrioritarios().getCodGprPrioritarioDesc5();
			}
			return NOAPLICA;
		case 30:
			if(obj.getVariablesVacunas().getCodVacunaDesc1() != null){
				return obj.getVariablesVacunas().getCodVacunaDesc1();
			}
			return NOAPLICA;
		case 31:
			if(obj.getVariablesVacunas().getCodvacgprriesgo1() != null){
				dato = new StringBuilder();
				dato.append(obj.getVariablesVacunas().getCodvacgprriesgo1() == null ? "" :  obj.getVariablesVacunas().getCodvacgprriesgo1());
				dato.append(ESPACIO);
				dato.append(obj.getVariablesVacunas().getCodvacgprriesgo1desc() == null ? "" :  obj.getVariablesVacunas().getCodvacgprriesgo1desc());
				return dato;
			}
			return NOAPLICA;
		case 32:
			if(obj.getVariablesVacunas().getCodVacNroDosis1() != null){
				return obj.getVariablesVacunas().getCodVacNroDosis1();
			}
			return NOAPLICA;
		case 33:
			if(obj.getVariablesVacunas().getVacLote1() != null){
				return obj.getVariablesVacunas().getVacLote1();
			}
			return NOAPLICA;
		case 34:
			if(obj.getVariablesVacunas().getCodVacEsquemaDesc1() != null){
				return obj.getVariablesVacunas().getCodVacEsquemaDesc1();
			}
			return NOAPLICA;
		case 35:
			if(obj.getVariablesVacunas().getVacFecha1() != null){
				return obj.getVariablesVacunas().getVacFecha1();
			}
			return NOAPLICA;
		case 36:
			if(obj.getVariablesVacunas().getCodVacunaDesc2() != null){
				return obj.getVariablesVacunas().getCodVacunaDesc2();
			}
			return NOAPLICA;
		case 37:
			if(obj.getVariablesVacunas().getCodvacgprriesgo2() != null){
				dato = new StringBuilder();
				dato.append(obj.getVariablesVacunas().getCodvacgprriesgo2() == null ? "" :  obj.getVariablesVacunas().getCodvacgprriesgo2());
				dato.append(ESPACIO);
				dato.append(obj.getVariablesVacunas().getCodvacgprriesgo2desc() == null ? "" :  obj.getVariablesVacunas().getCodvacgprriesgo2desc());
				return dato;
			}
			return NOAPLICA;
		case 38:
			if(obj.getVariablesVacunas().getCodVacNroDosis2() != null){
				return obj.getVariablesVacunas().getCodVacNroDosis2();
			}
			return NOAPLICA;
		case 39:
			if(obj.getVariablesVacunas().getVacLote2() != null){
				return obj.getVariablesVacunas().getVacLote2();
			}
			return NOAPLICA;
		case 40:
			if(obj.getVariablesVacunas().getCodVacEsquemaDesc2() != null){
				return obj.getVariablesVacunas().getCodVacEsquemaDesc2();
			}
			return NOAPLICA;
		case 41:
			if(obj.getVariablesVacunas().getVacFecha2() != null){
				return obj.getVariablesVacunas().getVacFecha2();
			}
			return NOAPLICA;
		case 42:
			if(obj.getVariablesVacunas().getCodVacunaDesc3() != null){
				return obj.getVariablesVacunas().getCodVacunaDesc3();
			}
			return NOAPLICA;
		case 43:
			if(obj.getVariablesVacunas().getCodvacgprriesgo3() != null){
				dato = new StringBuilder();
				dato.append(obj.getVariablesVacunas().getCodvacgprriesgo3() == null ? "" :  obj.getVariablesVacunas().getCodvacgprriesgo3());
				dato.append(ESPACIO);
				dato.append(obj.getVariablesVacunas().getCodvacgprriesgo3desc() == null ? "" :  obj.getVariablesVacunas().getCodvacgprriesgo3desc());
				return dato;
			}
			return NOAPLICA;
		case 44:
			if(obj.getVariablesVacunas().getCodVacNroDosis3() != null){
				return obj.getVariablesVacunas().getCodVacNroDosis3();
			}
			return NOAPLICA;
		case 45:
			if(obj.getVariablesVacunas().getVacLote3() != null){
				return obj.getVariablesVacunas().getVacLote3();
			}
			return NOAPLICA;
		case 46:
			if(obj.getVariablesVacunas().getCodVacEsquemaDesc3() != null){
				return obj.getVariablesVacunas().getCodVacEsquemaDesc3();
			}
			return NOAPLICA;
		case 47:
			if(obj.getVariablesVacunas().getVacFecha3() != null){
				return obj.getVariablesVacunas().getVacFecha3();
			}
			return NOAPLICA;
		case 48:
			if(obj.getVariablesVacunas().getCodVacunaDesc4() != null){
				return obj.getVariablesVacunas().getCodVacunaDesc4();
			}
			return NOAPLICA;
		case 49:
			if(obj.getVariablesVacunas().getCodvacgprriesgo4() != null){
				dato = new StringBuilder();
				dato.append(obj.getVariablesVacunas().getCodvacgprriesgo4() == null ? "" :  obj.getVariablesVacunas().getCodvacgprriesgo4());
				dato.append(ESPACIO);
				dato.append(obj.getVariablesVacunas().getCodvacgprriesgo4desc() == null ? "" :  obj.getVariablesVacunas().getCodvacgprriesgo4desc());
				return dato;
			}
			return NOAPLICA;
		case 50:
			if(obj.getVariablesVacunas().getCodVacNroDosis4() != null){
				return obj.getVariablesVacunas().getCodVacNroDosis4();
			}
			return NOAPLICA;
		case 51:
			if(obj.getVariablesVacunas().getVacLote4() != null){
				return obj.getVariablesVacunas().getVacLote4();
			}
			return NOAPLICA;
		case 52:
			if(obj.getVariablesVacunas().getCodVacEsquemaDesc4() != null){
				return obj.getVariablesVacunas().getCodVacEsquemaDesc4();
			}
			return NOAPLICA;
		case 53:
			if(obj.getVariablesVacunas().getVacFecha4() != null){
				return obj.getVariablesVacunas().getVacFecha4();
			}
			return NOAPLICA;
		case 54:
			if(obj.getVariablesVacunas().getCodVacunaDesc5() != null){
				return obj.getVariablesVacunas().getCodVacunaDesc5();
			}
			return NOAPLICA;
		case 55:
			if(obj.getVariablesVacunas().getCodvacgprriesgo5() != null){
				dato = new StringBuilder();
				dato.append(obj.getVariablesVacunas().getCodvacgprriesgo5() == null ? "" :  obj.getVariablesVacunas().getCodvacgprriesgo5());
				dato.append(ESPACIO);
				dato.append(obj.getVariablesVacunas().getCodvacgprriesgo5desc() == null ? "" :  obj.getVariablesVacunas().getCodvacgprriesgo5desc());
				return dato;
			}
			return NOAPLICA;
		case 56:
			if(obj.getVariablesVacunas().getCodVacNroDosis5() != null){
				return obj.getVariablesVacunas().getCodVacNroDosis5();
			}
			return NOAPLICA;
		case 57:
			if(obj.getVariablesVacunas().getVacLote5() != null){
				return obj.getVariablesVacunas().getVacLote5();
			}
			return NOAPLICA;
		case 58:
			if(obj.getVariablesVacunas().getCodVacEsquemaDesc5() != null){
				return obj.getVariablesVacunas().getCodVacEsquemaDesc5();
			}
			return NOAPLICA;
		case 59:
			if(obj.getVariablesVacunas().getVacFecha5() != null){
				return obj.getVariablesVacunas().getVacFecha5();
			}
			return NOAPLICA;
		default:
			return "";
		}
	}

}

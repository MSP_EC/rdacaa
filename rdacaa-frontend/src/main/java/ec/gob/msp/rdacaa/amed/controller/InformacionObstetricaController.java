/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.gob.msp.rdacaa.amed.controller;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.swing.ButtonGroup;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JRadioButton;

import org.jdesktop.swingx.autocomplete.AutoCompleteDecorator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.github.lgooddatepicker.components.DatePicker;

import ec.gob.msp.rdacaa.admision.model.CmbDetalleCatalogoCellRenderer;
import ec.gob.msp.rdacaa.admision.model.DetalleCatalogoToStringConverter;
import ec.gob.msp.rdacaa.amed.forma.InfoObstetrica;
import ec.gob.msp.rdacaa.amed.model.AmedCmbCatRiesgoObstetricoModel;
import ec.gob.msp.rdacaa.amed.model.AmedCmbMetSemanaGestacionModel;
import ec.gob.msp.rdacaa.business.entity.Atencionobstetrica;
import ec.gob.msp.rdacaa.business.entity.Detallecatalogo;
import ec.gob.msp.rdacaa.business.service.DetallecatalogoService;
import ec.gob.msp.rdacaa.control.swing.personalizado.CajaTextoIess;
import ec.gob.msp.rdacaa.frontend.sesion.VariableSesion;
import ec.gob.msp.rdacaa.utilitario.forma.UtilitarioForma;
import ec.gob.msp.rdacaa.utilitario.medico.UtilitarioMedico;

/**
 *
 * @author miguel.faubla
 */
@Controller
public class InformacionObstetricaController {

    private final InfoObstetrica infoObstetricaFrame;
    private final AmedCmbCatRiesgoObstetricoModel amedCmbCatRiesgoObstetricoModel;
    private final AmedCmbMetSemanaGestacionModel amedCmbMetSemanaGestacionModel;
    private final DetallecatalogoService detallecatalogoService;
    private final VariableSesion variableSesion = VariableSesion.getInstanciaVariablesSession();
    private JComboBox<Detallecatalogo> cmbCatRiesgObst;
    private JComboBox<Detallecatalogo> cmbMetodSemanaGestacion;
    private DatePicker dttFechaMenstruacion;
    private final Object[] controlesValidar = new Object[6];
    private JRadioButton rbPlanPartoSI;
    private JRadioButton rbPlanPartoNO;
    private JRadioButton rbPlanTranspSI;
    private JRadioButton rbPlanTranspNO;
    private JRadioButton rbEmbarazoPlanSI;
    private JRadioButton rbEmbarazoPlanNO;
    private ButtonGroup grupoEmbarazoPlanificado, grupoPlanTransporte, grupoPlandeParto;
    private CajaTextoIess  txtsemanasgestacioninfo;
    private CajaTextoIess  txtAlerta;
    private JLabel lbAlerta;
    private static final String RIESGO_BAJO = "Bajo Riesgo";
    private static final String RIESGO_ALTO = "Alto Riesgo";
    private static final int CATALOGO_MET_SEM_GEST_CLIN = 2579;
    private static final int CATALOGO_MET_SEM_GEST_ECO = 2578;
    private Detallecatalogo detallecatalogoNull;
    
    @Autowired
    public InformacionObstetricaController(
        InfoObstetrica infoObstetricaPrescripcionFrame,
        DetallecatalogoService detallecatalogoService
    ) 
    {
        this.infoObstetricaFrame = infoObstetricaPrescripcionFrame;
        this.amedCmbCatRiesgoObstetricoModel = new AmedCmbCatRiesgoObstetricoModel();
        this.amedCmbMetSemanaGestacionModel = new AmedCmbMetSemanaGestacionModel();
        this.detallecatalogoService = detallecatalogoService;
    }
    
    @PostConstruct
    private void init() {
        
        loadDefaultCombos();
        this.cmbCatRiesgObst = infoObstetricaFrame.getCmbCatRiesgoObstetrico();
        this.cmbMetodSemanaGestacion = infoObstetricaFrame.getCmbDetSemGestacion();
        AutoCompleteDecorator.decorate(cmbCatRiesgObst, new DetalleCatalogoToStringConverter());
        AutoCompleteDecorator.decorate(cmbMetodSemanaGestacion, new DetalleCatalogoToStringConverter());
        loadCatRiesgoObstetrico();
        loadMetodosSemanasGestacion();
        
        this.dttFechaMenstruacion = infoObstetricaFrame.getDttFechaMenstruacion();
        dttFechaMenstruacion.addDateChangeListener(dt -> setterSemanasGestacion());
        
        this.rbPlanPartoSI = infoObstetricaFrame.getRbPlanPartoSI();
        this.rbPlanPartoNO = infoObstetricaFrame.getRbPlanPartoNO();
        this.rbEmbarazoPlanSI = infoObstetricaFrame.getRbEmbarazoPlanSI();
        this.rbEmbarazoPlanNO = infoObstetricaFrame.getRbEmbarazoPlanNO();
        this.rbPlanTranspSI = infoObstetricaFrame.getRbPlanTranspSI();
        this.rbPlanTranspNO = infoObstetricaFrame.getRbPlanTranspNO();
        this.grupoEmbarazoPlanificado = infoObstetricaFrame.getGrupoEmbarazoPlanificado();
        this.grupoPlanTransporte = infoObstetricaFrame.getGrupoPlanTransporte();
        this.grupoPlandeParto = infoObstetricaFrame.getGrupoPlandeParto();
        
        this.txtsemanasgestacioninfo = infoObstetricaFrame.getTxtsemanasgestacioninfo();
        this.txtAlerta = infoObstetricaFrame.getTxtAlerta();
        this.lbAlerta = infoObstetricaFrame.getLbAlerta();
        txtsemanasgestacioninfo.setEnabled(false);
        
        txtsemanasgestacioninfo.addKeyListener(new java.awt.event.KeyAdapter() {
            @Override
            public void keyTyped(java.awt.event.KeyEvent evt) {
                UtilitarioForma.soloNumerosDecimales(evt,2);
                UtilitarioForma.validarLongitudCampo(evt, 10);
                
            }
        });
        txtsemanasgestacioninfo.addFocusListener(new FocusListener()  {

            @Override
            public void focusGained(FocusEvent e) {
                //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }

            @Override
            public void focusLost(FocusEvent e) {
                UtilitarioForma.validarValorMinimoMaximo (e,1,42);
            }
        });
        
        
        
        controlesValidar[0] = new Object();
        controlesValidar[1] = infoObstetricaFrame.getCmbDetSemGestacion();
        controlesValidar[2] = infoObstetricaFrame.getTxtsemanasgestacioninfo();
        controlesValidar[3] = infoObstetricaFrame.getGrupoPlandeParto();      
        controlesValidar[4] = infoObstetricaFrame.getGrupoPlanTransporte();      
        controlesValidar[5] = infoObstetricaFrame.getGrupoEmbarazoPlanificado();
        
        cmbCatRiesgObst.addActionListener(e -> validateRiesgoObstetrico());
        cmbMetodSemanaGestacion.addActionListener(e -> validateMetodSemanaGestacion(amedCmbMetSemanaGestacionModel.getSelectedItem().getId()));
    }
    
    public void getInformacionObstetrica() {
        Atencionobstetrica atencionobstetrica = new Atencionobstetrica();
        if(infoObstetricaFrame.getDttFechaMenstruacion().isEnabled()) {
            if(!infoObstetricaFrame.getDttFechaMenstruacion().getText().equals("")) {
                atencionobstetrica.setFum(Date.from(infoObstetricaFrame.getDttFechaMenstruacion().getDate().atStartOfDay(ZoneId.systemDefault()).toInstant()));   
            }
        }
        atencionobstetrica.setCtmetodosemgestacionId(amedCmbMetSemanaGestacionModel.getSelectedItem());
        atencionobstetrica.setCtriesgoobstetricoId(amedCmbCatRiesgoObstetricoModel.getSelectedItem());
        Integer planPartoInt = null;
        if(rbPlanPartoSI.isSelected()) {
        	planPartoInt = 1;
        }else if (rbPlanPartoNO.isSelected()) {
        	planPartoInt = 0;
        }
        atencionobstetrica.setPlandeparto(planPartoInt);
        Integer planTransporteInt = null;
        if(rbPlanTranspSI.isSelected()) {
        	planTransporteInt = 1;
        }else if(rbPlanTranspNO.isSelected()) {
        	planTransporteInt = 0;
        }
        atencionobstetrica.setPlantransporte(planTransporteInt);
        Integer planEmbarazoPlanificadoInt = null;
        if(rbEmbarazoPlanSI.isSelected()) {
        	planEmbarazoPlanificadoInt = 1;
        }else if(rbEmbarazoPlanNO.isSelected()) {
        	planEmbarazoPlanificadoInt = 0;
        }
        atencionobstetrica.setEmbarazoplanificado(planEmbarazoPlanificadoInt);
        atencionobstetrica.setSemgestacion((double)Double.parseDouble(txtsemanasgestacioninfo.getText()));
        VariableSesion.getInstanciaVariablesSession().setAtencionobstetrica(atencionobstetrica);
        VariableSesion.getInstanciaVariablesSession().setSemanasGestacion(Double.parseDouble(txtsemanasgestacioninfo.getText()));
    }
    
    public void loadCatRiesgoObstetrico() {
        List<Detallecatalogo> catRiesgoObstetrico = detallecatalogoService.findAllCatRiesgoObstetrico();
        amedCmbCatRiesgoObstetricoModel.clear();
        amedCmbCatRiesgoObstetricoModel.addElements(catRiesgoObstetrico);
        cmbCatRiesgObst.setRenderer(new CmbDetalleCatalogoCellRenderer());
        cmbCatRiesgObst.setModel(amedCmbCatRiesgoObstetricoModel);
        
        if (variableSesion.getGruposPrioritarios() !=  null && variableSesion.getIsEmbarazada()){
            Detallecatalogo dc = new Detallecatalogo();
            for (Detallecatalogo detallecatalogo : catRiesgoObstetrico) {
                if(detallecatalogo.getDescripcion().equals(RIESGO_BAJO)){
                    dc = detallecatalogo;
                    break;
                }
            }
            cmbCatRiesgObst.setSelectedItem(dc);
        }else{
           cmbCatRiesgObst.setSelectedItem(detallecatalogoNull);     
        }
    }
    
    public void updateDatePickerSettingsFUM(){
        if(variableSesion.getFechaAtencion() != null){
            LocalDate fechaAtencion = LocalDateTime.ofInstant(variableSesion.getFechaAtencion().toInstant(), ZoneId.systemDefault()).toLocalDate();
            dttFechaMenstruacion.getSettings().setDateRangeLimits(fechaAtencion.minusWeeks(42L).minusDays(3), fechaAtencion);
        }else{
            LocalDate today = LocalDate.now();
            dttFechaMenstruacion.getSettings().setDateRangeLimits(today.minusWeeks(42L).minusDays(3), today);
        }
        if(cmbMetodSemanaGestacion.getSelectedItem().equals(detallecatalogoNull)){
            dttFechaMenstruacion.setEnabled(false);
            dttFechaMenstruacion.setDate(null);
        }
    }
    
    private void loadMetodosSemanasGestacion() {
        List<Detallecatalogo> metodosSemanasGestacion = detallecatalogoService.findAllMetodosSemanasGestacion();
        amedCmbMetSemanaGestacionModel.clear();
        amedCmbMetSemanaGestacionModel.addElements(metodosSemanasGestacion);
        cmbMetodSemanaGestacion.setRenderer(new CmbDetalleCatalogoCellRenderer());
        cmbMetodSemanaGestacion.setModel(amedCmbMetSemanaGestacionModel);
        cmbMetodSemanaGestacion.setSelectedItem(detallecatalogoNull);
    }
    
    private void validateRiesgoObstetrico() {
        if (amedCmbCatRiesgoObstetricoModel.getSelectedItem() != null){
            switch (amedCmbCatRiesgoObstetricoModel.getSelectedItem().getDescripcion()) {
                case RIESGO_BAJO:
                    cmbCatRiesgObst.setBackground(Color.GREEN);
                    txtAlerta.setBackground(Color.GREEN);
                    lbAlerta.setForeground(Color.GREEN);
                    break;
                case RIESGO_ALTO:
                    cmbCatRiesgObst.setBackground(Color.YELLOW);
                    txtAlerta.setBackground(Color.YELLOW);
                    lbAlerta.setForeground(Color.YELLOW);
                    break;
                default:
                    cmbCatRiesgObst.setBackground(Color.RED);
                    txtAlerta.setBackground(Color.RED);
                    lbAlerta.setForeground(Color.RED);
                    break;
            }
        }
    }
    
    private void validateMetodSemanaGestacion(Integer detalleCatalogId) {
        if(detalleCatalogId != null) {
            switch(detalleCatalogId) {
                case CATALOGO_MET_SEM_GEST_CLIN:
                    dttFechaMenstruacion.setEnabled(false);
                    dttFechaMenstruacion.setDate(null);
                    dttFechaMenstruacion.setBackground(Color.LIGHT_GRAY);
                    txtsemanasgestacioninfo.setEnabled(true);
                    break;
                case CATALOGO_MET_SEM_GEST_ECO:
                    dttFechaMenstruacion.setEnabled(false);
                    dttFechaMenstruacion.setDate(null);
                    dttFechaMenstruacion.setBackground(Color.LIGHT_GRAY);
                    txtsemanasgestacioninfo.setEnabled(true);
                    break;
                default:
                    txtsemanasgestacioninfo.setEnabled(false);
                    dttFechaMenstruacion.setEnabled(true);
                    dttFechaMenstruacion.setBackground(Color.WHITE);
                    txtsemanasgestacioninfo.setText("");
                    txtsemanasgestacioninfo.setForeground(Color.BLUE);
                    txtsemanasgestacioninfo.setFont(new Font("Serif", Font.BOLD, 15));
                    break;
            }
        }
    }
    
    public boolean validateForm(){
        boolean validarForma = UtilitarioForma.validarCamposRequeridos(controlesValidar);
        if(validarForma){
            getInformacionObstetrica();
        }
       return validarForma;
    }
    
    public void setterSemanasGestacion(){  
        try {
            if(infoObstetricaFrame.getDttFechaMenstruacion().isEnabled()){
                Double semanagestacionactual = UtilitarioMedico.calcularSemanasEmbarazo(Date.from(infoObstetricaFrame.getDttFechaMenstruacion().getDate().atStartOfDay(ZoneId.systemDefault()).toInstant()),
                        variableSesion.getFechaAtencion()); 
                infoObstetricaFrame.getTxtsemanasgestacioninfo().setText(String.valueOf(UtilitarioForma.round(semanagestacionactual, 2)));
            }
        } catch (Exception e) {
        }
    }
    public void loadDefaultCombos(){
        detallecatalogoNull = new Detallecatalogo(){
            private static final long serialVersionUID = 1L;
            boolean isNoSelectable = true;
        };
        detallecatalogoNull.setId(0);
        detallecatalogoNull.setDescripcion("Seleccione");
    }
    
    public void cleanInput(){
        cmbCatRiesgObst.setSelectedItem(detallecatalogoNull);
        cmbMetodSemanaGestacion.setSelectedItem(detallecatalogoNull);
        txtsemanasgestacioninfo.setText("");
        grupoEmbarazoPlanificado.clearSelection();
        grupoPlanTransporte.clearSelection();
        grupoPlandeParto.clearSelection();
    }
}

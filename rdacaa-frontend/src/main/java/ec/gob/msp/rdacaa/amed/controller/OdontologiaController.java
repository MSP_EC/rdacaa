/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.gob.msp.rdacaa.amed.controller;

import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;

import javax.annotation.PostConstruct;
import javax.swing.JLabel;
import javax.swing.JTextField;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import ec.gob.msp.rdacaa.amed.forma.OdontologiaPanel;
import ec.gob.msp.rdacaa.business.entity.Odontologia;
import ec.gob.msp.rdacaa.frontend.sesion.VariableSesion;
import ec.gob.msp.rdacaa.utilitario.forma.UtilitarioForma;

/**
 *
 * @author eduardo
 */
@Controller
public class OdontologiaController {
    
    private final OdontologiaPanel odontologiaFrame;
    private JTextField ctxCariados;
    private JTextField ctxExtraidos;
    private JTextField ctxObturados;
    private JTextField ctxPerdidos;
    private static final String CEO = "12";
    private static final String CPO = "11";
    private static final int NUMPIEZASDENTALES = 32;
    private final VariableSesion variableSesion = VariableSesion.getInstanciaVariablesSession();
    private JLabel valorTotalDientes;
    private int sumaPiezas=0;

    @Autowired
    public OdontologiaController(OdontologiaPanel odontologiaFrame) {
        this.odontologiaFrame = odontologiaFrame;
    }
    
    @PostConstruct
    private void init() {
        this.ctxCariados = odontologiaFrame.getCtxCariados();
        this.ctxExtraidos = odontologiaFrame.getCtxExtraidos();
        this.ctxObturados = odontologiaFrame.getCtxObturados();
        this.ctxPerdidos = odontologiaFrame.getCtxPerdidos();
        this.valorTotalDientes = odontologiaFrame.getLbl_Valor_Total_Dientes();
        
        ctxCariados.addKeyListener(new java.awt.event.KeyAdapter() {
            @Override
            public void keyTyped(java.awt.event.KeyEvent evt) {
                UtilitarioForma.soloNumeros(evt);
                UtilitarioForma.validarLongitudCampo(evt, 2);
            }
        });
        ctxExtraidos.addKeyListener(new java.awt.event.KeyAdapter() {
            @Override
            public void keyTyped(java.awt.event.KeyEvent evt) {
                UtilitarioForma.soloNumeros(evt);
                UtilitarioForma.validarLongitudCampo(evt, 2);
            }
        });
        ctxObturados.addKeyListener(new java.awt.event.KeyAdapter() {
            @Override
            public void keyTyped(java.awt.event.KeyEvent evt) {
                UtilitarioForma.soloNumeros(evt);
                UtilitarioForma.validarLongitudCampo(evt, 2);
            }
        });
        ctxPerdidos.addKeyListener(new java.awt.event.KeyAdapter() {
            @Override
            public void keyTyped(java.awt.event.KeyEvent evt) {
                UtilitarioForma.soloNumeros(evt);
                UtilitarioForma.validarLongitudCampo(evt, 2);
            }
        });
        
        
        ctxCariados.addFocusListener(new FocusListener() {
            @Override
            public void focusGained(FocusEvent e) {
            	validateCampoVacio();
            }

            @Override
            public void focusLost(FocusEvent e) {
                validateCampoVacio();
                UtilitarioForma.validarValorMinimoMaximo (e,0,32);
                sumaPiezasDentales();
            }
        });
        ctxExtraidos.addFocusListener(new FocusListener() {
            @Override
            public void focusGained(FocusEvent e) {
            	//No es usado
            }

            @Override
            public void focusLost(FocusEvent e) {
                validateCampoVacio();
                UtilitarioForma.validarValorMinimoMaximo (e,0,32);
                sumaPiezasDentales();
            }
        });
        ctxObturados.addFocusListener(new FocusListener() {
            @Override
            public void focusGained(FocusEvent e) {
            	//No es usado
            }

            @Override
            public void focusLost(FocusEvent e) {
                validateCampoVacio();
                UtilitarioForma.validarValorMinimoMaximo (e,0,32);
                sumaPiezasDentales();
            }
        });
        ctxPerdidos.addFocusListener(new FocusListener() {
            @Override
            public void focusGained(FocusEvent e) {
            	//No es usado
            }

            @Override
            public void focusLost(FocusEvent e) {
                validateCampoVacio();
                UtilitarioForma.validarValorMinimoMaximo (e,0,32);
                sumaPiezasDentales();
            }
        });
    }
    
    public boolean validateForm() {
        boolean validarForma;
        validarForma = validateDentalPiezas();
        if(validarForma) {
            getOdontologia();
        }
        
        return validarForma;
    }
    
    public void getOdontologia() {
        
        Odontologia odontologia = new Odontologia();
        if(ctxCariados.getText() != null && !ctxCariados.getText().isEmpty() && ctxCariados.getText() != "0") {
            odontologia.setDientecaido(Integer.parseInt(ctxCariados.getText()));
        }
        if(ctxExtraidos.getText() != null && !ctxExtraidos.getText().isEmpty() && ctxExtraidos.getText() != "0") {
            odontologia.setDienteextraido(Integer.parseInt(ctxExtraidos.getText()));
        }
        if(ctxObturados.getText() != null && !ctxObturados.getText().isEmpty() && ctxObturados.getText() != "0") {
            odontologia.setDienteobturado(Integer.parseInt(ctxObturados.getText()));
        }
        if(ctxPerdidos.getText() != null && !ctxPerdidos.getText().isEmpty() && ctxPerdidos.getText() != "0") {
            odontologia.setDienteperdido(Integer.parseInt(ctxPerdidos.getText()));
        }
        
        variableSesion.setOdontologia(odontologia);
    }
    
    private Boolean validateDentalPiezas() {
       Boolean validar = false;
      validateCampoVacio();
       int total = Integer.valueOf(ctxCariados.getText()) + Integer.valueOf(ctxExtraidos.getText()) + Integer.valueOf(ctxObturados.getText()) + Integer.valueOf(ctxPerdidos.getText()); 
       
           if(total <= NUMPIEZASDENTALES) {
                validar = true;
           } else{

                UtilitarioForma.muestraMensaje("Validacion", odontologiaFrame, "ERROR", "El total de dientes no debe sobrepasar el límite de 32 por favor verifique");

           }
       
       return validar;
    }

    public void validateCampoVacio(){
        if (ctxCariados.getText().equals("")){
            ctxCariados.setText("0");
        }
        if (ctxExtraidos.getText().equals("")){
            ctxExtraidos.setText("0");
        }
        if (ctxObturados.getText().equals("")){
            ctxObturados.setText("0");
        }
        if (ctxPerdidos.getText().equals("")){
            ctxPerdidos.setText("0");
        }
        
    }
    
    public void sumaPiezasDentales(){
        sumaPiezas = Integer.parseInt(ctxObturados.getText())+
                Integer.parseInt(ctxPerdidos.getText())+
                Integer.parseInt(ctxExtraidos.getText())+
                Integer.parseInt(ctxCariados.getText());
        valorTotalDientes.setText(""+sumaPiezas);
    }
    public void cleanInput() {
        ctxCariados.setText("0");
        ctxExtraidos.setText("0");
        ctxObturados.setText("0");
        ctxPerdidos.setText("0");
        valorTotalDientes.setText("0");
    }
}

package ec.gob.msp.rdacaa.amed.model;

import ec.gob.msp.rdacaa.business.entity.Diagnosticoatencion;
import ec.gob.msp.rdacaa.utilitario.forma.ModeloTablaGenerico;

public class DiagnosticoAtencionTablaModel extends ModeloTablaGenerico<Diagnosticoatencion> {

    public DiagnosticoAtencionTablaModel(String[] columnas) {
        super(columnas);
    }

    private static final long serialVersionUID = -8259310025068677691L;

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Diagnosticoatencion obj =  getListaDatos().get(rowIndex);
        switch (columnIndex) {
            case 0:
                return obj;
            case 1:
            	return obj.getCieId().getCodigo();
            case 2:
                return obj.getCieId().getNombre();
            case 3:
                return obj.getCtmorbilidaddiagnosticoId().getDescripcion();
            case 4:     
                return obj.getCtprevenciondiagnosticoId().getDescripcion();
            case 5:
                return (obj.getCtcondiciondiagnositcoId() != null) ? obj.getCtcondiciondiagnositcoId().getDescripcion() : "";
            case 6:
                return obj.getNumerocertificado();
            case 7:
                return "Eliminar";
            default:
                return "";                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    
        }
    }
    
    @Override
    public boolean isCellEditable(int row, int col) {
        return col == 7;
    }

    @Override
    public void setValueAt(Object value, int row, int col) {
        fireTableCellUpdated(row, col);
    }

}

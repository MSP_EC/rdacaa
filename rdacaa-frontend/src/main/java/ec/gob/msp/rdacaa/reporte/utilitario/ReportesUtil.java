/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.gob.msp.rdacaa.reporte.utilitario;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.filechooser.FileNameExtensionFilter;

import ec.gob.msp.rdacaa.frontend.sesion.VariableSesion;

/**
 *
 * @author dmurillo
 */
public class ReportesUtil {
	
    private static final String TITULOMENSAJE = "Advertencia";
    private ReportesUtil(){
    	
    }
    public static void exportarXlsReportes(JTable tbl, 
    		                               String nombreHoja, 
    		                               String nombreProfesional, 
    		                               String tipo,
    		                               String tituloReporte,
    		                               String tituloFechas){
            if (tbl.getRowCount()==0) {
                JOptionPane.showMessageDialog (null, "No hay datos en la tabla para exportar.",TITULOMENSAJE,
                    JOptionPane.INFORMATION_MESSAGE);
                return;
            }
	        JFileChooser chooser=new JFileChooser();
	        FileNameExtensionFilter filter;
	        if(tipo.equals("xls")){
	        	filter=new FileNameExtensionFilter("Archivos de excel","xls");
	        }else{
	        	filter=new FileNameExtensionFilter("Archivos de pdf","pdf");
	        }
	        chooser.setFileFilter(filter);
	        chooser.setDialogTitle("Guardar archivo");
	        chooser.setMultiSelectionEnabled(false);
	        chooser.setAcceptAllFileFilterUsed(false);
	        if (chooser.showSaveDialog(null)==JFileChooser.APPROVE_OPTION){
	            List<JTable> tb=new ArrayList<>();
	            List<String>nom=new ArrayList<>();
	            tb.add(tbl);
	            nom.add(nombreHoja);
	            String file;
	            if(tipo.equals("xls")){
	            	file=chooser.getSelectedFile().toString().concat(".xls");
	            }else{
	            	file=chooser.getSelectedFile().toString().concat(".pdf");
	            }
	            String autor = nombreProfesional;
         	    Date fecha = new Date();	

                    try (InputStream is = ReportesUtil.class.getResource("/principal/logomspReporte.png").openStream();) {
                        File f = readPngFileFromResources(is, "logomspReporte");
                        Exporter e = new Exporter();
                        e.setFile(new File(file));
                        e.setTabla(tb);
                        e.setNomFiles(nom);
                        e.setElaborado(autor);
                        e.setFecha(fecha);
                        e.setLogo(f);
                        e.setUsuario(VariableSesion.getInstanciaVariablesSession().getUsuario());
                        e.setEntidad(VariableSesion.getInstanciaVariablesSession().getEstablecimientoSalud());
                        e.setEspecialidad(VariableSesion.getInstanciaVariablesSession().getCtEspecialidad());
                        e.setTituloReporte(tituloReporte);
                        e.setFechas(tituloFechas);
                        
                        if(tipo.equals("xls")){
                        	if (e.export()) {
                                JOptionPane.showMessageDialog(null, "Los datos fueron exportados a excel.", TITULOMENSAJE,
                                        JOptionPane.INFORMATION_MESSAGE);
                            }
                        }else{
                        	if (e.exportPdf()) {
                                JOptionPane.showMessageDialog(null, "Los datos fueron exportados a PDF.", TITULOMENSAJE,
                                        JOptionPane.INFORMATION_MESSAGE);
                            }
                        }
                        

                            
                        f.deleteOnExit();
                    } catch (Exception ex) {
                        JOptionPane.showMessageDialog(null, "Hubo un error" + ex.getMessage(), "Error ", JOptionPane.ERROR_MESSAGE);
                    }
	        }
        }
        
        private static File readPngFileFromResources(InputStream is, String filename) throws IOException {
        byte[] buffer = new byte[is.available()];
        File f = File.createTempFile(filename, ".png");
        try (OutputStream outStream = new FileOutputStream(f)) {
            if (is.read(buffer) != 0) {
                outStream.write(buffer);
            }
        }

        return f;
    } 
    
}

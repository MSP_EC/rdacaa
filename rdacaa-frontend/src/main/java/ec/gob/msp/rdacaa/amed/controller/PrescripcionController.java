/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.gob.msp.rdacaa.amed.controller;

import javax.annotation.PostConstruct;
import javax.swing.ButtonGroup;
import javax.swing.JRadioButton;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import ec.gob.msp.rdacaa.amed.forma.Prescripcion;
import ec.gob.msp.rdacaa.business.entity.Atencionprescripcion;
import ec.gob.msp.rdacaa.frontend.sesion.VariableSesion;
import ec.gob.msp.rdacaa.utilitario.forma.UtilitarioForma;

/**
 *
 * @author eduardo
 */
@Controller
public class PrescripcionController {
    
    private final Prescripcion prescripcionFrame;
    private JRadioButton rbhierromineralSI;
    private JRadioButton rbhierromineralNO;
    private JRadioButton rbvitaminacapsulaSI;
    private JRadioButton rbvitaminacapsulaNO;
    private JRadioButton rbhierrojarabeSI;
    private JRadioButton rbhierrojarabeNO;
    private JRadioButton rbhierroacidofolicoSI;
    private JRadioButton rbhierroacidofolicoNO;
    private ButtonGroup bgHierromineral, bgVitaminacapsula, bgHierrojarabe, bgHierroacidofolico;
    private final VariableSesion variableSesion = VariableSesion.getInstanciaVariablesSession();
    
    @Autowired
    public PrescripcionController(Prescripcion prescripcionFrame) {
        this.prescripcionFrame = prescripcionFrame;
    }
    
    @PostConstruct
    private void init() {
        rbhierromineralSI = prescripcionFrame.getRbhierromineralSI();
        rbhierromineralNO = prescripcionFrame.getRbhierromineralNO();
        rbvitaminacapsulaSI = prescripcionFrame.getRbvitaminacapsulaSI();
        rbvitaminacapsulaNO = prescripcionFrame.getRbvitaminacapsulaNO();
        rbhierrojarabeSI = prescripcionFrame.getRbhierrojarabeSI();
        rbhierrojarabeNO = prescripcionFrame.getRbhierrojarabeNO();
        rbhierroacidofolicoSI = prescripcionFrame.getRbhierroacidofolicoSI();
        rbhierroacidofolicoNO = prescripcionFrame.getRbhierroacidofolicoNO();
        
        bgHierromineral = prescripcionFrame.getBgHierromineral();
        bgVitaminacapsula = prescripcionFrame.getBgVitaminacapsula();
        bgHierrojarabe = prescripcionFrame.getBgHierrojarabe();
        bgHierroacidofolico = prescripcionFrame.getBgHierroacidofolico();
    }
    
    public void getPrescripcion() {
        Atencionprescripcion atencionprescripcion = new Atencionprescripcion();
        if(rbhierromineralSI.isEnabled() && rbhierromineralNO.isEnabled()) {
            if(bgHierromineral.getSelection().equals(rbhierromineralSI.getModel())) {
                atencionprescripcion.setHierrominerales(1);
            } else if(bgHierromineral.getSelection().equals(rbhierromineralNO.getModel())) {
                atencionprescripcion.setHierrominerales(0);
            }
        }
        if(rbvitaminacapsulaSI.isEnabled() && rbvitaminacapsulaNO.isEnabled()) {
            if(bgVitaminacapsula.getSelection().equals(rbvitaminacapsulaSI.getModel())) {
                atencionprescripcion.setVitamina(1);
            } else if(bgVitaminacapsula.getSelection().equals(rbvitaminacapsulaNO.getModel())) {
                atencionprescripcion.setVitamina(0);
            }
        }
        if(rbhierrojarabeSI.isEnabled() && rbhierrojarabeNO.isEnabled()) {
            if(bgHierrojarabe.getSelection().equals(rbhierrojarabeSI.getModel())) {
                atencionprescripcion.setHierrojarabe(1);
            } else if(bgHierrojarabe.getSelection().equals(rbhierrojarabeNO.getModel())) {
                atencionprescripcion.setHierrojarabe(0);
            }
        }
        if(rbhierroacidofolicoSI.isEnabled() && rbhierroacidofolicoNO.isEnabled()) {
            if(bgHierroacidofolico.getSelection().equals(rbhierroacidofolicoSI.getModel())) {
                atencionprescripcion.setHierroacidofo(1);
            } else if(bgHierroacidofolico.getSelection().equals(rbhierroacidofolicoNO.getModel())) {
                atencionprescripcion.setHierroacidofo(0);
            }
        }
        
        variableSesion.setAtencionprescripcion(atencionprescripcion);  
    }
    
    public boolean validateForm() {
        Object[] controlesValidar = new Object[2];
        if(rbhierromineralSI.isEnabled() && rbhierromineralNO.isEnabled()) {
            controlesValidar[0] = prescripcionFrame.getBgHierromineral();
        }
        if(rbvitaminacapsulaSI.isEnabled() && rbvitaminacapsulaNO.isEnabled()) {
            controlesValidar[1] = prescripcionFrame.getBgVitaminacapsula();
        }
        if(rbhierrojarabeSI.isEnabled() && rbhierrojarabeNO.isEnabled()) {
            controlesValidar[0] = prescripcionFrame.getBgHierrojarabe();
        }
        if(rbhierroacidofolicoSI.isEnabled() && rbhierroacidofolicoNO.isEnabled()) {
            controlesValidar[0] = prescripcionFrame.getBgHierroacidofolico();
        }
        
        boolean validarForma = UtilitarioForma.validarCamposRequeridos(controlesValidar);
        if(validarForma) {
            getPrescripcion();
        }
        
        return validarForma;
    }

    public void cleanInput() {
        bgHierromineral.clearSelection();
        bgVitaminacapsula.clearSelection();
        bgHierrojarabe.clearSelection();
        bgHierroacidofolico.clearSelection();
    }
    
}

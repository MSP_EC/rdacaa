# Contribuyendo

Al contribuir a este repositorio, primero discuta el cambio que desea realizar a través del problema,
correo electrónico o cualquier otro método con los propietarios de este repositorio antes de realizar un cambio. 

Tenga en cuenta que tenemos un código de conducta, sígalo en todas sus interacciones con el proyecto.

## Proceso de solicitud de extracción

1. Asegúrese de eliminar las dependencias de instalación o compilación antes del final de la capa al hacer un 
   construir.
2. Actualice README.md con detalles de los cambios en la interfaz, esto incluye un nuevo entorno. 
   variables, puertos expuestos, ubicaciones útiles de archivos y parámetros de contenedor.
3. Aumente los números de versión en cualquier archivo de ejemplos y el archivo README.md a la nueva versión que esto
   Pull Request representaría. El esquema de versiones que utilizamos es [SemVer] (http://semver.org/).
4. Puede fusionar la solicitud de extracción una vez que tenga el cierre de sesión de otros dos desarrolladores, o si 
   no tiene permiso para hacerlo, puede solicitar al segundo revisor que lo combine por usted.

## Código de Conducta

### Nuestro compromiso

En aras de fomentar un ambiente abierto y acogedor, nosotros como
Los contribuyentes y mantenedores se comprometen a participar en nuestro proyecto y
nuestra comunidad es una experiencia libre de acoso para todos, independientemente de su edad, cuerpo
tamaño, discapacidad, etnia, identidad y expresión de género, nivel de experiencia,
nacionalidad, apariencia personal, raza, religión o identidad sexual y
orientación.

### Nuestras normas

Ejemplos de comportamiento que contribuyen a crear un ambiente positivo.
incluir:

* Uso de lenguaje acogedor e inclusivo
* Ser respetuoso de los diferentes puntos de vista y experiencias.
* Aceptando con gracia la crítica constructiva
* Centrarse en lo que es mejor para la comunidad
* Mostrar empatía hacia otros miembros de la comunidad.

Los ejemplos de comportamiento inaceptable de los participantes incluyen:

* El uso de lenguaje o imágenes sexualizadas y atención sexual no deseada
avances
* Comentarios curiosos, insultantes / despectivos y ataques personales o políticos.
* Acoso público o privado
* Publicar información privada de otros, como una física o electrónica
  dirección, sin permiso explícito
* Otra conducta que razonablemente podría considerarse inapropiada en un
  entorno profesional

### Nuestras responsabilidades

Los encargados del mantenimiento del proyecto son responsables de aclarar los estándares de aceptación
comportamiento y se espera que tomen medidas correctivas apropiadas y justas en
respuesta a cualquier instancia de comportamiento inaceptable.

Los encargados del proyecto tienen el derecho y la responsabilidad de eliminar, editar o
rechazar comentarios, confirmaciones, código, ediciones de wiki, problemas y otras contribuciones
que no están alineados con este Código de Conducta, o para prohibir temporalmente o
permanentemente cualquier contribuyente para otros comportamientos que considere inapropiados,
amenazante, ofensivo o dañino.

### Alcance

Este Código de conducta se aplica tanto dentro de los espacios del proyecto como en los espacios públicos.
cuando un individuo representa el proyecto o su comunidad. Ejemplos de
representar un proyecto o comunidad incluye el uso de un correo electrónico oficial del proyecto
dirección, publicación a través de una cuenta oficial de redes sociales, o actuar como un designado
representante en un evento en línea o fuera de línea. La representación de un proyecto puede ser
Definido y aclarado por los encargados del proyecto.

### Aplicación

Los casos de comportamiento abusivo, acosador o inaceptable pueden ser
informó contactando al equipo del proyecto en [INSERTAR DIRECCIÓN DE CORREO ELECTRÓNICO]. Todos
las quejas serán revisadas e investigadas y darán como resultado una respuesta que
se considera necesario y apropiado a las circunstancias. El equipo del proyecto es
obligado a mantener la confidencialidad con respecto al reportero de un incidente.
Se pueden publicar por separado más detalles de políticas de cumplimiento específicas.

Los encargados del mantenimiento del proyecto que no siguen o hacen cumplir el Código de Conducta en buena
la fe puede enfrentar repercusiones temporales o permanentes según lo determine otro
miembros de la dirección del proyecto.

### Atribución

Este Código de conducta está adaptado del [Colaborador del colaborador] [página de inicio], versión 1.4,
disponible en [http://contributor-covenant.org/version/1/4font>[version]

[página de inicio]: http://contributor-covenant.org
[versión]: http://contributor-covenant.org/version/1/4/


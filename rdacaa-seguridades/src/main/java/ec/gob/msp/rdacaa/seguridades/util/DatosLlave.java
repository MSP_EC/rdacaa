package ec.gob.msp.rdacaa.seguridades.util;

import lombok.Getter;
import lombok.Setter;

public class DatosLlave {
	
	@Getter
	@Setter
	private byte[] key;
	@Getter
	@Setter
	private byte[] salt;	

        public DatosLlave() {
            
        }
	
	public DatosLlave(byte[] key, byte[] salt) {
		this.key = key;
		this.salt = salt;
	}
	
	//TODO getKeyBase64
}

package ec.gob.msp.rdacaa.seguridades.util;

import lombok.Getter;
import lombok.Setter;

public class DatosCifrado {
	
	@Getter
	@Setter
	private byte[] cifrado;
	@Getter
	@Setter
	private byte[] nonce;
	
	public DatosCifrado(byte[] cifrado, byte[] nonce) {
		this.cifrado = cifrado;
		this.nonce = nonce;
	}
}

CREATE VIEW VARIABLES_ATENCION_MEDICA AS
SELECT
    ATENCIONMEDICA.ID                                                    AS ID_ATENCION,
    ATENCIONMEDICA.UUID                                                  AS UUID_ATENCION,
    ATENCIONMEDICA.ENTIDAD_ID                                            AS COD_EST_SALUD,
    ENTIDAD.NOMBREOFICIAL                                                AS COD_EST_SALUD_NOMBRE_OFICIAL,
    DATE(ATENCIONMEDICA.FECHAATENCION/1000,'unixepoch', 'localtime') AS FEC_ATENCION,
    PERSONA.IDPRAS			                                 AS COD_PROFESIONAL,
    PERSONA.NUMEROIDENTIFICACION                                         AS COD_PROFESIONAL_NUM_IDENT,
    ATENCIONMEDICA.CTESPECIALIDADMEDICA_ID AS COD_ESPECIALIDAD,
    CT_ESPECIALIDAD.DESCRIPCION            AS COD_ESPECIALIDAD_DESC,
    ATENCIONMEDICA.CTTIPOBONO_ID           AS COD_PER_TIPO_BONO,
    CT_TP_BONO.DESCRIPCION                 AS COD_PER_TIPO_BONO_DESC,
    ATENCIONMEDICA.CTTIPOSEGUROSALUD_ID    AS COD_PER_SEGURO,
    CT_TP_SEGURO.DESCRIPCION               AS COD_PER_SEGURO_DESC,
    ATENCIONMEDICA.KS_CIDENT,
    ATENCIONMEDICA.KU_CIDENT,
    ATENCIONMEDICA.KU_KEYID, 
    PARROQUIA.CODPARROQUIA                 AS COD_DPA_EST_PARROQUIA,
    PARROQUIA.DESCRIPCION                  AS COD_DPA_EST_PARROQUIA_DESC
FROM
    ATENCIONMEDICA
LEFT OUTER JOIN
    PERSONA
ON
    (
        ATENCIONMEDICA.USUARIOCREACION_ID = PERSONA.ID)
LEFT OUTER JOIN
    DETALLECATALOGO CT_ESPECIALIDAD
ON
    (
        ATENCIONMEDICA.CTESPECIALIDADMEDICA_ID = CT_ESPECIALIDAD.ID)
LEFT OUTER JOIN
    DETALLECATALOGO CT_TP_BONO
ON
    (
        ATENCIONMEDICA.CTTIPOBONO_ID = CT_TP_BONO.ID)
LEFT OUTER JOIN
    DETALLECATALOGO CT_TP_SEGURO
ON
    (
        ATENCIONMEDICA.CTTIPOSEGUROSALUD_ID = CT_TP_SEGURO.ID)
LEFT OUTER JOIN
    ENTIDAD
ON
    (
        ATENCIONMEDICA.ENTIDAD_ID = ENTIDAD.ID)
LEFT OUTER JOIN
    PARROQUIA
ON
    (
        ATENCIONMEDICA.UBICACION_PARROQUIA_ID = PARROQUIA.ID);


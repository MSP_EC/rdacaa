CREATE VIEW VARIABLEXESPECIALIDAD_ENFERMERIA_DETALLE AS
SELECT
    VARIABLEXESPECIALIDAD_ENFERMERIA.ID,
    VARIABLEXESPECIALIDAD_ENFERMERIA.VARIABLE_ID,
    VARIABLE.CODIGO,
    VARIABLE.NOMBRE,
    VARIABLEXESPECIALIDAD_ENFERMERIA.CTESPECIALIDAD_ID,
    DETALLECATALOGO.DESCRIPCION,
    VARIABLEXESPECIALIDAD_ENFERMERIA.ISMANDATORIO,
    VARIABLEXESPECIALIDAD_ENFERMERIA.ESTADO
FROM
    VARIABLEXESPECIALIDAD_ENFERMERIA
INNER JOIN
    VARIABLE
ON
    (
        VARIABLEXESPECIALIDAD_ENFERMERIA.VARIABLE_ID = VARIABLE.ID)
INNER JOIN
    DETALLECATALOGO
ON
    (
        VARIABLEXESPECIALIDAD_ENFERMERIA.CTESPECIALIDAD_ID = DETALLECATALOGO.ID);


##### Se cambia la versión de aplicativo por los tickets atendidos
 
> Fecha:  2019-08-12 con hash 8c7d0df Realizado por Jaime Eduardo García Zapata
##### atencion varios tickets MR para regularizar
 
> Fecha:  2019-08-12 con hash 155b809 Realizado por Jaime Eduardo García Zapata
##### Merge branch '501-ticket-2019072981000391-pras-rdacaa-actualizacion-tarifario-en-sistema-rdacaa-2-0' into 'dev'

Resolve "Ticket#2019072981000391 — PRAS: RDACAA actualizacion tarifario en sistema RDACAA 2.0"

See merge request MSP_EC/rdacaa!62 
> Fecha:  2019-08-12 con hash 3fd5e57 Realizado por Saulo Velasco
##### Se versiona todos los tickets pendientes
 
> Fecha:  2019-08-12 con hash 883a338 Realizado por Jaime Eduardo García Zapata
##### Merge branch '501-ticket-2019072981000391-pras-rdacaa-actualizacion-tarifario-en-sistema-rdacaa-2-0' into 'dev'

Se atiende ticket Ticket#2019072981000391 — PRAS: RDACAA actualizacion...

See merge request MSP_EC/rdacaa!58 
> Fecha:  2019-07-31 con hash caa635c Realizado por Saulo Velasco
##### Se atiende ticket Ticket#2019072981000391 — PRAS: RDACAA actualizacion tarifario en sistema RDACAA 2.0
 
> Fecha:  2019-07-31 con hash 732117e Realizado por Jaime Eduardo García Zapata
##### Merge branch '500-ticket-2019050881000209-pras-inconvenientes-en-sistema-rdacaa-2-0-escritorio' into 'dev'

Resolve "Ticket#2019050881000209 — PRAS: inconvenientes en sistema RDACAA 2.0 -escritorio" varios tickets

See merge request MSP_EC/rdacaa!56 
> Fecha:  2019-07-31 con hash d615700 Realizado por Saulo Velasco
##### Se atiende 500-ticket-2019050881000209-pras-inconvenientes-en-sistema-rdacaa-2-0-escritorio
 
> Fecha:  2019-07-31 con hash 2fd86ce Realizado por Jaime Eduardo García Zapata
##### Con este commit se solventa varios tickets del memorando enviado por wilson
 
> Fecha:  2019-07-03 con hash f7f3af0 Realizado por Jaime Eduardo García Zapata
##### Se corrige malla semanas de gestacion, debe redondear los decimales
 
> Fecha:  2019-06-24 con hash 668275e Realizado por Jaime Eduardo García Zapata
##### cambio en reporte de atenciones autoidentidad
 
> Fecha:  2019-06-20 con hash 5daf8d0 Realizado por bolivar.murillo
##### Se atiende tickets solicitados por hilda envidos por memo junio
 
> Fecha:  2019-06-18 con hash c262349 Realizado por Jaime Eduardo García Zapata
##### cambio en pantalla de diagnosticos
 
> Fecha:  2019-06-17 con hash 4e1bb49 Realizado por bolivar.murillo
##### cambio por issues hilda
 
> Fecha:  2019-06-13 con hash b7bcf0f Realizado por bolivar.murillo
##### cambio en reporte de atenciones
 
> Fecha:  2019-05-20 con hash db03d61 Realizado por bolivar.murillo
##### cambio en condicion de diagnostico reporte atenciones
 
> Fecha:  2019-05-14 con hash 7a57072 Realizado por bolivar.murillo
##### Merge branch '500-ticket-2019050881000209-pras-inconvenientes-en-sistema-rdacaa-2-0-escritorio' of https://gitlab.com/MSP_EC/rdacaa into 500-ticket-2019050881000209-pras-inconvenientes-en-sistema-rdacaa-2-0-escritorio
 
> Fecha:  2019-05-14 con hash 5ad83be Realizado por Eduardo Garcia
##### Se modifica reporte de atenciones, se quita etiqueta de INSTITUTCION
 
> Fecha:  2019-05-14 con hash 8cf5169 Realizado por Eduardo Garcia
##### cambio en reporte de atenciones
 
> Fecha:  2019-05-13 con hash 29922ca Realizado por bolivar.murillo
##### Merge branch '500-ticket-2019050881000209-pras-inconvenientes-en-sistema-rdacaa-2-0-escritorio' of https://gitlab.com/MSP_EC/rdacaa into 500-ticket-2019050881000209-pras-inconvenientes-en-sistema-rdacaa-2-0-escritorio
 
> Fecha:  2019-05-13 con hash 2144075 Realizado por bolivar.murillo
##### cambio en reporte de atenciones
 
> Fecha:  2019-05-13 con hash e218139 Realizado por bolivar.murillo
##### Se carga tabla entidad
 
> Fecha:  2019-05-13 con hash 13b5128 Realizado por Eduardo Garcia
##### se modifica reporte de atenciones y pantalla de vih
 
> Fecha:  2019-05-13 con hash 808b359 Realizado por Eduardo Garcia
##### Se modifica las vistas de CIE no estaban incluyendo dias y meses, tambien vih
 
> Fecha:  2019-05-10 con hash 83b1b48 Realizado por Eduardo Garcia
##### Merge branch '500-ticket-2019050881000209-pras-inconvenientes-en-sistema-rdacaa-2-0-escritorio' into 'dev'

"Ticket#2019050881000209 — PRAS: inconvenientes en sistema RDACAA 2.0 -escritorio"

See merge request MSP_EC/rdacaa!55 
> Fecha:  2019-05-08 con hash b457aaa Realizado por Eduardo García
##### Merge branch '499-ticket-2019050281001139-pras-persistencia-inconsistencia-dpa-en-sistema-rdacaa-2-0' into 500-ticket-2019050881000209-pras-inconvenientes-en-sistema-rdacaa-2-0-escritorio
 
> Fecha:  2019-05-08 con hash dd19347 Realizado por Eduardo Garcia
##### 499-ticket-2019050281001139-pras-persistencia-inconsistencia-dpa-en-sistema-rdacaa-2-0
 
> Fecha:  2019-05-08 con hash fea1cd1 Realizado por Eduardo Garcia
##### 500-ticket-2019050881000209-pras-inconvenientes-en-sistema-rdacaa-2-0-escritorio
 
> Fecha:  2019-05-08 con hash 9e0303a Realizado por Eduardo Garcia
##### Merge branch '500-ticket-2019050881000209-pras-inconvenientes-en-sistema-rdacaa-2-0-escritorio' into 'dev'

500-ticket-2019050881000209-pras-inconvenientes-en-sistema-rdacaa-2-0-escritorio

See merge request MSP_EC/rdacaa!54 
> Fecha:  2019-05-08 con hash 78c2008 Realizado por Eduardo García
##### Se rectifica la informacion catalogos en base a l pedido de la dnais
 
> Fecha:  2019-05-07 con hash 1f44b8c Realizado por Eduardo Garcia
##### Merge branch '498-ticket-2019040381001031-actualizaciones-en-sistema-rdacaa-2-0' into 'dev'

Se garga documento con pasos para el instalador

See merge request MSP_EC/rdacaa!50 
> Fecha:  2019-04-09 con hash e8c4f30 Realizado por Eduardo García
##### Se garga documento con pasos para el instalador
 
> Fecha:  2019-04-09 con hash b6ce7cf Realizado por Eduardo Garcia
##### Merge branch '498-ticket-2019040381001031-actualizaciones-en-sistema-rdacaa-2-0' into 'dev'

Resolve "Ticket#2019040381001031 — Actualizaciones en sistema RDACAA 2.0"

See merge request MSP_EC/rdacaa!48 
> Fecha:  2019-04-08 con hash 00be7c9 Realizado por Saulo Velasco
##### Ticket#2019040381001031 correccion de version en pom-s
 
> Fecha:  2019-04-08 con hash fe68f97 Realizado por Eduardo Garcia
##### Ticket#2019040381001031
 
> Fecha:  2019-04-08 con hash 6f2777f Realizado por Eduardo Garcia
##### Merge branch '498-ticket-2019040381001031-actualizaciones-en-sistema-rdacaa-2-0' of https://gitlab.com/MSP_EC/rdacaa into 498-ticket-2019040381001031-actualizaciones-en-sistema-rdacaa-2-0
 
> Fecha:  2019-04-08 con hash 5416957 Realizado por Eduardo Garcia
##### Se corrige la nacionalidad Shiwiar
 
> Fecha:  2019-03-11 con hash 1c2850e Realizado por Eduardo Garcia
##### Merge branch '497-ticket-2019030181000887-correccion-de-la-funcionalidad-del-sistema-rdacaa-2-0' of https://gitlab.com/MSP_EC/rdacaa into 497-ticket-2019030181000887-correccion-de-la-funcionalidad-del-sistema-rdacaa-2-0
 
> Fecha:  2019-03-11 con hash c75ea29 Realizado por Eduardo Garcia
##### Merge branch '497-ticket-2019030181000887-correccion-de-la-funcionalidad-del-sistema-rdacaa-2-0' into 'dev'

Resolve "Ticket#2019030181000887-Corrección de la funcionalidad del sistema RDACAA 2.0"

See merge request MSP_EC/rdacaa!43 
> Fecha:  2019-03-07 con hash 5232953 Realizado por David Murillo
##### Cambio version 2.0.2 servicio
 
> Fecha:  2019-03-06 con hash e35604b Realizado por bolivar.murillo
##### Cambio de verison 2.0.2
 
> Fecha:  2019-03-06 con hash 2c5c463 Realizado por bolivar.murillo
##### Merge branch '497-ticket-2019030181000887-correccion-de-la-funcionalidad-del-sistema-rdacaa-2-0' into 'dev'

Resolve "Ticket#2019030181000887-Corrección de la funcionalidad del sistema RDACAA 2.0"

See merge request MSP_EC/rdacaa!41 
> Fecha:  2019-03-06 con hash 1b0b693 Realizado por Saulo Velasco
##### Merge branch '490-reporte-de-detalle-de-vacunas-falta-el-lugar-de-atencion' into 497-ticket-2019030181000887-correccion-de-la-funcionalidad-del-sistema-rdacaa-2-0
 
> Fecha:  2019-03-06 con hash 2f12ef6 Realizado por bolivar.murillo
##### Cambio en dpa y en sesion
 
> Fecha:  2019-03-06 con hash 7ceccca Realizado por bolivar.murillo
##### Merge branch '491-reporte-de-detalle-de-vacunas-falta-el-lugar-de-atencion' of https://gitlab.com/MSP_EC/rdacaa into 491-reporte-de-detalle-de-vacunas-falta-el-lugar-de-atencion
 
> Fecha:  2019-02-27 con hash 82275cc Realizado por Eduardo Garcia
##### Merge branch '491-reporte-de-detalle-de-vacunas-falta-el-lugar-de-atencion' into 'dev'

Resolve "reporte de detalle de vacunas falta el lugar de atención"

See merge request MSP_EC/rdacaa!39 
> Fecha:  2019-02-27 con hash 11a0b35 Realizado por Saulo Velasco
##### Actualizando para la ejecución el nuevo gitlab-runner 
> Fecha:  2019-02-26 con hash 43e9c0d Realizado por Saulo Velasco
##### Cambio reporte vacunas
 
> Fecha:  2019-02-26 con hash 32f3bb3 Realizado por bolivar.murillo
##### Update .gitlab-ci.yml 
> Fecha:  2019-02-25 con hash 80015f6 Realizado por Saulo Velasco
##### Prueba nuevo runner 2 
> Fecha:  2019-02-25 con hash 01f809b Realizado por Saulo Velasco
##### Prueba nuevo runner 
> Fecha:  2019-02-25 con hash 280c7a1 Realizado por Saulo Velasco
##### Probando nuevo runner
 
> Fecha:  2019-02-25 con hash 5823620 Realizado por Saulo Ismael Velasco Rivera
##### Merge branch 'dev' into '476-nuevo-reporte-de-vacunas-atenciones-en-vacunas'

Dev

See merge request MSP_EC/rdacaa!38 
> Fecha:  2019-02-25 con hash 08af454 Realizado por Saulo Velasco
##### Update .gitlab-ci.yml 
> Fecha:  2019-02-21 con hash 5c53a28 Realizado por Saulo Velasco
##### Update .gitlab-ci.yml 
> Fecha:  2019-02-21 con hash cf73dd2 Realizado por Saulo Velasco
##### Borrando archivos previos de compilación 
> Fecha:  2019-02-21 con hash ad7f74b Realizado por Saulo Velasco
##### Merge branch '476-nuevo-reporte-de-vacunas-atenciones-en-vacunas' into 'dev'

Resolve "Nuevo Reporte de vacunas - atenciones en vacunas"

See merge request MSP_EC/rdacaa!36 
> Fecha:  2019-02-21 con hash 27f8c31 Realizado por Saulo Velasco
##### Cambio de vacunas
 
> Fecha:  2019-02-21 con hash 39c95b8 Realizado por bolivar.murillo
##### Se carga la nueva DPA porque la anterior alexandra tenia registros duplicados
 
> Fecha:  2019-02-20 con hash 1d1b73a Realizado por Eduardo Garcia
##### Contro ingreso
 
> Fecha:  2019-02-20 con hash 855a18a Realizado por bolivar.murillo
##### Se cambia la version de codigo fuente para pasar a produccion
 
> Fecha:  2019-02-19 con hash 3db9220 Realizado por Eduardo Garcia
##### Se respalda la BDD en el proyecto para tener un backup
 
> Fecha:  2019-02-19 con hash 83bcf8a Realizado por Eduardo Garcia
##### Cambio en reporte de atenciones
 
> Fecha:  2019-02-18 con hash 0409f62 Realizado por bolivar.murillo
##### Cambio reporte de nutricion
 
> Fecha:  2019-02-15 con hash 2a3b286 Realizado por bolivar.murillo
##### Cambio reportes scroll
 
> Fecha:  2019-02-14 con hash 8c75fc3 Realizado por bolivar.murillo
##### Cambio reporte Retenciones
 
> Fecha:  2019-02-14 con hash 7057f3e Realizado por bolivar.murillo
##### Corrige la pantalla inicial
 
> Fecha:  2019-02-14 con hash 4cdceb6 Realizado por Eduardo Garcia
##### Configurando autogeneración de location del servicio web de descifrado
de recuperación de contraseña 
> Fecha:  2019-02-13 con hash 615010c Realizado por Saulo Ismael Velasco Rivera
##### Solucinando recomendaciones de SpotBugs y agregando filtros por IP para
los servicios web.
También se agrega nueva clave de cifrado para recuperación de
contraseñas. 
> Fecha:  2019-02-13 con hash 5358eca Realizado por Saulo Ismael Velasco Rivera
##### Merge branch '476-nuevo-reporte-de-vacunas-atenciones-en-vacunas' of https://gitlab.com/MSP_EC/rdacaa into 476-nuevo-reporte-de-vacunas-atenciones-en-vacunas
 
> Fecha:  2019-02-13 con hash 1644579 Realizado por bolivar.murillo
##### Cambio reportes vacunas
 
> Fecha:  2019-02-13 con hash 644c9ab Realizado por bolivar.murillo
##### Se corrige cuadro de dialogo para la recuperacino de contreaseña
 
> Fecha:  2019-02-13 con hash d375b67 Realizado por Eduardo Garcia
##### Cambio persona servicio
 
> Fecha:  2019-02-12 con hash 96404ce Realizado por bolivar.murillo
##### Se añade boton en la pantalla ACERCADE
 
> Fecha:  2019-02-12 con hash 9f189e2 Realizado por Eduardo Garcia
##### persona servicio filtro tipo persona
 
> Fecha:  2019-02-11 con hash 6bab026 Realizado por bolivar.murillo
##### Se añade boton cerrar en la pantalla de DPA
 
> Fecha:  2019-02-11 con hash 27adb17 Realizado por Eduardo Garcia
##### Merge branch '476-nuevo-reporte-de-vacunas-atenciones-en-vacunas' of https://gitlab.com/MSP_EC/rdacaa into 476-nuevo-reporte-de-vacunas-atenciones-en-vacunas
 
> Fecha:  2019-02-11 con hash 8daa6ab Realizado por Eduardo Garcia
##### Se corrige la suma de piezas dentales
 
> Fecha:  2019-02-11 con hash 8130389 Realizado por Eduardo Garcia
##### cambio en controlador cambioDPA
 
> Fecha:  2019-02-11 con hash 03b551e Realizado por bolivar.murillo
##### Servicio web de recuperación de contraseña y quitando la herencia de
Serializable de las clases 
> Fecha:  2019-02-08 con hash d0c653a Realizado por Saulo Ismael Velasco Rivera
##### Actualizar DPA sin cerrar el aplicativo 
> Fecha:  2019-02-08 con hash f76e055 Realizado por Saulo Ismael Velasco Rivera
##### Merge branch '476-nuevo-reporte-de-vacunas-atenciones-en-vacunas' of https://gitlab.com/MSP_EC/rdacaa into 476-nuevo-reporte-de-vacunas-atenciones-en-vacunas
 
> Fecha:  2019-02-07 con hash e31bfa7 Realizado por Eduardo Garcia
##### Merge branch '476-nuevo-reporte-de-vacunas-atenciones-en-vacunas' of https://gitlab.com/MSP_EC/rdacaa into 476-nuevo-reporte-de-vacunas-atenciones-en-vacunas
 
> Fecha:  2019-02-07 con hash 92b1a38 Realizado por Eduardo Garcia
##### Se crea funcionalidad para exportar credenciales
 
> Fecha:  2019-02-07 con hash d75b121 Realizado por Eduardo Garcia
##### Reporte de produccion de vacunas
 
> Fecha:  2019-02-07 con hash b8f9d20 Realizado por bolivar.murillo
##### Merge branch '424-modulo-de-registro-de-unidades-moviles' into 'dev'

Corrección reporte atenciones

See merge request MSP_EC/rdacaa!33 
> Fecha:  2019-02-06 con hash ebf5157 Realizado por Saulo Velasco
##### Update .gitlab-ci.yml 
> Fecha:  2019-02-06 con hash 578f57d Realizado por palichis
##### Update .gitlab-ci.yml 
> Fecha:  2019-02-06 con hash a102943 Realizado por palichis
##### Update .gitlab-ci.yml 
> Fecha:  2019-02-06 con hash b87295e Realizado por palichis
##### Update .gitlab-ci.yml 
> Fecha:  2019-02-06 con hash 7cbb824 Realizado por palichis
##### Update .gitlab-ci.yml 
> Fecha:  2019-02-06 con hash 6798042 Realizado por palichis
##### Update .gitlab-ci.yml 
> Fecha:  2019-02-06 con hash 6206770 Realizado por palichis
##### Update .gitlab-ci.yml 
> Fecha:  2019-02-06 con hash c604656 Realizado por palichis
##### Update .gitlab-ci.yml 
> Fecha:  2019-02-06 con hash d4e4b17 Realizado por palichis
##### Update .gitlab-ci.yml 
> Fecha:  2019-02-06 con hash 08fb6cb Realizado por palichis
##### Update .gitlab-ci.yml 
> Fecha:  2019-02-06 con hash bf8abde Realizado por palichis
##### Update .gitlab-ci.yml 
> Fecha:  2019-02-06 con hash 1368349 Realizado por palichis
##### Corrección reporte atenciones 
> Fecha:  2019-02-06 con hash 4b5e15c Realizado por Saulo Ismael Velasco Rivera
##### Merge branch '424-modulo-de-registro-de-unidades-moviles' into 'dev'

Resolve "Modulo de Registro de Unidades Moviles"

See merge request MSP_EC/rdacaa!32 
> Fecha:  2019-02-06 con hash 60575bc Realizado por Saulo Velasco
##### Update .gitlab-ci.yml 
> Fecha:  2019-02-05 con hash 8b4473c Realizado por palichis
##### Merge branch 'dev' into '424-modulo-de-registro-de-unidades-moviles'

# Conflicts:
#   .gitlab-ci.yml 
> Fecha:  2019-02-05 con hash 932f36a Realizado por Saulo Velasco
##### Update .gitlab-ci.yml
eliminar tty 
> Fecha:  2019-02-05 con hash 5977547 Realizado por palichis
##### Update .gitlab-ci.yml
agregando variables de entorno  
> Fecha:  2019-02-05 con hash 3560d05 Realizado por palichis
##### Update .gitlab-ci.yml
ejecución directa find-sec-bugs 
> Fecha:  2019-02-05 con hash 9186f40 Realizado por palichis
##### Update .gitlab-ci.yml 
> Fecha:  2019-02-05 con hash f503778 Realizado por Saulo Velasco
##### Update .gitlab-ci.yml 
> Fecha:  2019-02-05 con hash f4d6e78 Realizado por Saulo Velasco
##### Update .gitlab-ci.yml 
> Fecha:  2019-02-05 con hash 084a7f3 Realizado por Saulo Velasco
##### Update .gitlab-ci.yml 
> Fecha:  2019-02-05 con hash 5ee4ff5 Realizado por Saulo Velasco
##### Update .gitlab-ci.yml 
> Fecha:  2019-02-04 con hash 84c039a Realizado por Saulo Velasco
##### Update .gitlab-ci.yml 
> Fecha:  2019-02-04 con hash a03dbad Realizado por Saulo Velasco
##### Update .gitlab-ci.yml 
> Fecha:  2019-02-04 con hash 3d7305e Realizado por Saulo Velasco
##### Update .gitlab-ci.yml 
> Fecha:  2019-02-04 con hash 147e171 Realizado por Saulo Velasco
##### Update .gitlab-ci.yml 
> Fecha:  2019-02-04 con hash b193439 Realizado por Saulo Velasco
##### Update .gitlab-ci.yml 
> Fecha:  2019-02-04 con hash e143452 Realizado por Saulo Velasco
##### Update .gitlab-ci.yml 
> Fecha:  2019-02-04 con hash 1b9baba Realizado por Saulo Velasco
##### Update .gitlab-ci.yml 
> Fecha:  2019-02-04 con hash f1b0041 Realizado por Saulo Velasco
##### Update .gitlab-ci.yml 
> Fecha:  2019-02-04 con hash 0189138 Realizado por Saulo Velasco
##### Update .gitlab-ci.yml 
> Fecha:  2019-02-04 con hash 6db5d9b Realizado por Saulo Velasco
##### Update .gitlab-ci.yml 
> Fecha:  2019-02-04 con hash 55e2c5f Realizado por Saulo Velasco
##### Update .gitlab-ci.yml 
> Fecha:  2019-02-04 con hash f9b9a20 Realizado por Saulo Velasco
##### Update .gitlab-ci.yml 
> Fecha:  2019-02-04 con hash 9111bef Realizado por Saulo Velasco
##### Update .gitlab-ci.yml 
> Fecha:  2019-02-04 con hash 4a2ba9a Realizado por Saulo Velasco
##### Update .gitlab-ci.yml 
> Fecha:  2019-02-04 con hash dbb3b2d Realizado por Saulo Velasco
##### Update .gitlab-ci.yml
agregar ruta m2 a docker
--volume /home/gitlab-runner/.m2:/root/.m2 
> Fecha:  2019-02-04 con hash 220ae77 Realizado por palichis
##### Update .gitlab-ci.yml 
> Fecha:  2019-02-04 con hash f9ec075 Realizado por Saulo Velasco
##### Merge branch '424-modulo-de-registro-de-unidades-moviles' of https://gitlab.com/MSP_EC/rdacaa into 424-modulo-de-registro-de-unidades-moviles
 
> Fecha:  2019-02-04 con hash 93f47a3 Realizado por Saulo Ismael Velasco Rivera
##### Actualizacion runner pasos SAST
 
> Fecha:  2019-02-04 con hash 5a12f59 Realizado por Saulo Ismael Velasco Rivera
##### Se corrige carga de pacientes
 
> Fecha:  2019-02-04 con hash 5e99812 Realizado por Eduardo Garcia
##### Cambio reporte Atenciones medica titulo
 
> Fecha:  2019-02-02 con hash 234586e Realizado por bolivar.murillo
##### Cambio reporte Atenciones
 
> Fecha:  2019-02-01 con hash 12d469b Realizado por bolivar.murillo
##### Cambio reporte Atenciones
 
> Fecha:  2019-02-01 con hash 6683f56 Realizado por bolivar.murillo
##### Merge branch '424-modulo-de-registro-de-unidades-moviles' of https://gitlab.com/MSP_EC/rdacaa into 424-modulo-de-registro-de-unidades-moviles
 
> Fecha:  2019-02-01 con hash 5ca18cb Realizado por bolivar.murillo
##### Cambio reporte Atenciones
 
> Fecha:  2019-02-01 con hash 531c99d Realizado por bolivar.murillo
##### Cerrar reportes 
> Fecha:  2019-02-01 con hash 34f1180 Realizado por Saulo Ismael Velasco Rivera
##### Cambio reporte captacion de vacunas
 
> Fecha:  2019-02-01 con hash cd1dfe6 Realizado por bolivar.murillo
##### Cambio reporte Atenciones
 
> Fecha:  2019-02-01 con hash eab805c Realizado por bolivar.murillo
##### Cambio reporte Atenciones
 
> Fecha:  2019-02-01 con hash 85d6ad6 Realizado por bolivar.murillo
##### Merge branch '424-modulo-de-registro-de-unidades-moviles' of https://gitlab.com/MSP_EC/rdacaa into 424-modulo-de-registro-de-unidades-moviles
 
> Fecha:  2019-02-01 con hash 2e22d20 Realizado por bolivar.murillo
##### Update .gitlab-ci.yml 
> Fecha:  2019-02-01 con hash 4084bc1 Realizado por Saulo Velasco
##### runner
 
> Fecha:  2019-02-01 con hash 62ff028 Realizado por Eduardo Garcia
##### Merge branch '424-modulo-de-registro-de-unidades-moviles' of https://gitlab.com/MSP_EC/rdacaa into 424-modulo-de-registro-de-unidades-moviles
 
> Fecha:  2019-02-01 con hash 6201f6b Realizado por Eduardo Garcia
##### Se carga runner
 
> Fecha:  2019-02-01 con hash 535858f Realizado por Eduardo Garcia
##### Cambio reporte Atenciones issue 471
 
> Fecha:  2019-02-01 con hash 1dde03f Realizado por bolivar.murillo
##### Reglas de odontologia 
> Fecha:  2019-01-31 con hash 1adc854 Realizado por Saulo Ismael Velasco Rivera
##### -
 
> Fecha:  2019-01-31 con hash 108bf7d Realizado por Eduardo Garcia
##### Cambio en reportes
 
> Fecha:  2019-01-31 con hash df9facc Realizado por bolivar.murillo
##### Se corrige rangos de dientes
 
> Fecha:  2019-01-31 con hash 31c6b14 Realizado por Eduardo Garcia
##### Se corrige conflictos en ODONTOLOGIACONTROLLER
 
> Fecha:  2019-01-31 con hash 93c041c Realizado por Eduardo Garcia
##### Se corrigen observaciones del issue 471
 
> Fecha:  2019-01-31 con hash 47dc64a Realizado por Eduardo Garcia
##### Merge branch '424-modulo-de-registro-de-unidades-moviles' of https://gitlab.com/MSP_EC/rdacaa into 424-modulo-de-registro-de-unidades-moviles
 
> Fecha:  2019-01-31 con hash 64e2cac Realizado por Saulo Ismael Velasco Rivera
##### Validación de VIH y correcciones ortográficas 
> Fecha:  2019-01-31 con hash 4506e25 Realizado por Saulo Ismael Velasco Rivera
##### Limpiando inputs de usuario y contraseña al momento de cerrar la session
 
> Fecha:  2019-01-31 con hash cd47a0c Realizado por miguel.faubla
##### Corrigiendo splash de RDCAA a RDACAA 
> Fecha:  2019-01-30 con hash 0a297dd Realizado por svelasco
##### Evitando que se actualice un registro anterior de persona cuando se
edita la cédula de una persona antes registrada 
> Fecha:  2019-01-30 con hash 47b3daa Realizado por svelasco
##### Arreglando layout de reportes 
> Fecha:  2019-01-30 con hash 983ca2b Realizado por svelasco
##### Se añade boton limpiar en pantalla REFERENCIA
 
> Fecha:  2019-01-30 con hash c90466a Realizado por Eduardo Garcia
##### Merge branch '424-modulo-de-registro-de-unidades-moviles' of https://gitlab.com/MSP_EC/rdacaa into 424-modulo-de-registro-de-unidades-moviles
 
> Fecha:  2019-01-30 con hash ceba6d8 Realizado por miguel.faubla
##### Proceso de cierre de session sastifactorio
 
> Fecha:  2019-01-30 con hash ea514bb Realizado por miguel.faubla
##### Cambio cifrado password
 
> Fecha:  2019-01-30 con hash f715814 Realizado por bolivar.murillo
##### Cambiando funcionalidad cerrar para que vaya a la pantalla de login 
> Fecha:  2019-01-30 con hash 0d7be9c Realizado por Saulo Ismael Velasco Rivera
##### Se corrige terminos y condicionaes para ENTER en campo contraseña
 
> Fecha:  2019-01-29 con hash 7c962db Realizado por Eduardo Garcia
##### Calculando semanas gestación 
> Fecha:  2019-01-29 con hash 62c68e9 Realizado por Saulo Ismael Velasco Rivera
##### Se aumenta campos en la carga de pacientes y se corrige terminos de uso para boton con enter
 
> Fecha:  2019-01-29 con hash a44aad9 Realizado por Eduardo Garcia
##### Cambio reporte de atenciones
 
> Fecha:  2019-01-29 con hash 49d45fa Realizado por bolivar.murillo
##### Titulo en reporte de vacunas por dosis
 
> Fecha:  2019-01-29 con hash 75e5953 Realizado por bolivar.murillo
##### Agregando generación del location del wsdl automatico 
> Fecha:  2019-01-28 con hash 041312c Realizado por Saulo Ismael Velasco Rivera
##### implementacion reporte de vacunas por dosis
 
> Fecha:  2019-01-28 con hash 3b04574 Realizado por bolivar.murillo
##### Merge branch '424-modulo-de-registro-de-unidades-moviles' of https://gitlab.com/MSP_EC/rdacaa into 424-modulo-de-registro-de-unidades-moviles
 
> Fecha:  2019-01-28 con hash 8fb2ae5 Realizado por Eduardo Garcia
##### Se corrigen observaciones del ISSUE 457
 
> Fecha:  2019-01-28 con hash 2b2a23a Realizado por Eduardo Garcia
##### cambio en reporte de nutricion
 
> Fecha:  2019-01-28 con hash 4b93a31 Realizado por bolivar.murillo
##### Agregando condición de reglas de fecha de atencion y vacunas mayores al
2018-12-31 y paralelización de malla de validación 
> Fecha:  2019-01-25 con hash c457aaf Realizado por Saulo Ismael Velasco Rivera
##### Modificación del servicio web para que se ejecute en background, 2
tareas de validación simultánea 
> Fecha:  2019-01-25 con hash 5de4db7 Realizado por Saulo Ismael Velasco Rivera
##### Se carga MENSAJE PARA PROCEDIMIENTOS
 
> Fecha:  2019-01-25 con hash a29cff0 Realizado por Eduardo Garcia
##### Merge branch '424-modulo-de-registro-de-unidades-moviles' of https://gitlab.com/MSP_EC/rdacaa into 424-modulo-de-registro-de-unidades-moviles
 
> Fecha:  2019-01-25 con hash 4c2238a Realizado por Eduardo Garcia
##### PROCEDIMIENTOS CONTROLLER
 
> Fecha:  2019-01-25 con hash be05792 Realizado por Eduardo Garcia
##### Cambio vacunasController
 
> Fecha:  2019-01-25 con hash 66d9b0a Realizado por bolivar.murillo
##### Merge branch '424-modulo-de-registro-de-unidades-moviles' of https://gitlab.com/MSP_EC/rdacaa into 424-modulo-de-registro-de-unidades-moviles
 
> Fecha:  2019-01-24 con hash 3120730 Realizado por Eduardo Garcia
##### Seteo de fecha de atencion y de fecha de vacunas al año 2019
 
> Fecha:  2019-01-24 con hash 472dbd1 Realizado por miguel.faubla
##### Merge branch '424-modulo-de-registro-de-unidades-moviles' of https://gitlab.com/MSP_EC/rdacaa into 424-modulo-de-registro-de-unidades-moviles
 
> Fecha:  2019-01-24 con hash a6f816e Realizado por Eduardo Garcia
##### Se rectifica reporte vacunas - OPV se elimino
 
> Fecha:  2019-01-24 con hash 84933bd Realizado por Eduardo Garcia
##### Seteo de fecha de atencion y de fecha de vacunas al año 2019
 
> Fecha:  2019-01-24 con hash ee1f891 Realizado por miguel.faubla
##### Se corrigen observaciones pre produccion 22012019
 
> Fecha:  2019-01-23 con hash 9130f97 Realizado por Eduardo Garcia
##### Se corrigen observaciones de presentacion 23012019
 
> Fecha:  2019-01-23 con hash dcd3f21 Realizado por Eduardo Garcia
##### Corección de reglas de exámenes de laboratorio y haciendo que el
servicio web retorne desencriptado los datos cuando pase la validación
sin problemas 
> Fecha:  2019-01-22 con hash b865743 Realizado por Saulo Ismael Velasco Rivera
##### Quitando código duplicado 
> Fecha:  2019-01-22 con hash 657898d Realizado por Saulo Ismael Velasco Rivera
##### Se corrige sonnar cube
 
> Fecha:  2019-01-22 con hash 31c6552 Realizado por Eduardo Garcia
##### Correción regla de validación de ácido fólico 
> Fecha:  2019-01-22 con hash 7868f40 Realizado por Saulo Ismael Velasco Rivera
##### Corrección reglas grupos de riesgo 
> Fecha:  2019-01-21 con hash c4a7ee8 Realizado por Saulo Ismael Velasco Rivera
##### Se actualiza a la version 10 del aplicativo
 
> Fecha:  2019-01-21 con hash a65913c Realizado por Eduardo Garcia
##### Abrir edad en años, meses y días 
> Fecha:  2019-01-21 con hash e7ff3fc Realizado por Saulo Ismael Velasco Rivera
##### Aumentando el tamaño del array de columnas 
> Fecha:  2019-01-21 con hash b78326b Realizado por Saulo Ismael Velasco Rivera
##### abriendo en 3 campos la edad del paciente en años, meses y días 
> Fecha:  2019-01-21 con hash 9f81b11 Realizado por Saulo Ismael Velasco Rivera
##### Corrección de reportes 
> Fecha:  2019-01-18 con hash ed7565f Realizado por Saulo Ismael Velasco Rivera
##### Correccion de reportes
 
> Fecha:  2019-01-18 con hash 3a6fe25 Realizado por Saulo Ismael Velasco Rivera
##### Cambiando texto de mensajes 
> Fecha:  2019-01-17 con hash 3ee133c Realizado por Saulo Ismael Velasco Rivera
##### Cambiando de primera a subsecuente 
> Fecha:  2019-01-17 con hash 0477723 Realizado por Saulo Ismael Velasco Rivera
##### Evitando que se guarde un cierre mensual para meses posteriores al
actual 
> Fecha:  2019-01-17 con hash f21fc5b Realizado por Saulo Ismael Velasco Rivera
##### Corrigiendo mensaje al guardar 
> Fecha:  2019-01-17 con hash 53d66b5 Realizado por Saulo Ismael Velasco Rivera
##### Merge branch '424-modulo-de-registro-de-unidades-moviles' of https://gitlab.com/MSP_EC/rdacaa into 424-modulo-de-registro-de-unidades-moviles
 
> Fecha:  2019-01-17 con hash 2f8c9f8 Realizado por Saulo Ismael Velasco Rivera
##### visualizando vacunas pra intersexual 
> Fecha:  2019-01-17 con hash d2a7f95 Realizado por Saulo Ismael Velasco Rivera
##### Se corrige reglas para codigo cie 10
 
> Fecha:  2019-01-17 con hash c9317c5 Realizado por Eduardo Garcia
##### Resporte vacunas y validación datos sivan 
> Fecha:  2019-01-16 con hash 6033b35 Realizado por Saulo Ismael Velasco Rivera
##### Quitando las vacunas agregadas para cambio de edad y sexo 
> Fecha:  2019-01-16 con hash bc31a64 Realizado por Saulo Ismael Velasco Rivera
##### OGANIZANDO IMPORT 
> Fecha:  2019-01-16 con hash 35ff364 Realizado por Saulo Ismael Velasco Rivera
##### Validacion de grupos de riesgo de vacunas por sexo 
> Fecha:  2019-01-16 con hash 843554d Realizado por Saulo Ismael Velasco Rivera
##### Extension .rdacaa 
> Fecha:  2019-01-15 con hash 0630072 Realizado por Saulo Ismael Velasco Rivera
##### Se modifica entidad GRIESGOXVACUNA
 
> Fecha:  2019-01-15 con hash 198cce9 Realizado por Eduardo Garcia
##### Merge branch '424-modulo-de-registro-de-unidades-moviles' of https://gitlab.com/MSP_EC/rdacaa into 424-modulo-de-registro-de-unidades-moviles
 
> Fecha:  2019-01-15 con hash 4222810 Realizado por Eduardo Garcia
##### Se corrigen observaciones HILDA
 
> Fecha:  2019-01-15 con hash 8a619bd Realizado por Eduardo Garcia
##### Reglas de grupos de riesgo y validacion de cedula 
> Fecha:  2019-01-15 con hash 551beef Realizado por Saulo Ismael Velasco Rivera
##### FUNCIONALIDAD MANUAL E INSTRUCTIVO -- VACUNAS GRIESGO
 
> Fecha:  2019-01-15 con hash 4892a82 Realizado por Eduardo Garcia
##### Reglas de Grupo de Riesgo para vacunas 
> Fecha:  2019-01-14 con hash 1e347d3 Realizado por Saulo Ismael Velasco Rivera
##### Agregando variables de grupos de riesgo por vacuna 
> Fecha:  2019-01-14 con hash 90a994f Realizado por Saulo Ismael Velasco Rivera
##### Metodo encontrar Entidad por Usuario 
> Fecha:  2019-01-14 con hash a29e54e Realizado por Saulo Ismael Velasco Rivera
##### Agregando código a los procedimientos 
> Fecha:  2019-01-14 con hash 4cb9e96 Realizado por Saulo Ismael Velasco Rivera
##### Lista de grupos de riesgo por vacuna 
> Fecha:  2019-01-14 con hash c30ce46 Realizado por Saulo Ismael Velasco Rivera
##### load riesgo vacunas 
> Fecha:  2019-01-14 con hash dc1385d Realizado por Saulo Ismael Velasco Rivera
##### Se corrige observaciones
 
> Fecha:  2019-01-12 con hash a74a490 Realizado por Eduardo Garcia
##### Generacion de nuevas tabla y servicios 
> Fecha:  2019-01-12 con hash c19e6ad Realizado por svelasco
##### Actualizacion cuando cambia edad y  sexo de grupos prioritario y
diagnosticos y correcion de observaciones 
> Fecha:  2019-01-11 con hash 7b69e67 Realizado por Saulo Ismael Velasco Rivera
##### Se corrige la validación para identidad de genero
 
> Fecha:  2019-01-11 con hash 527bd5e Realizado por Eduardo Garcia
##### Merge branch '424-modulo-de-registro-de-unidades-moviles' of https://gitlab.com/MSP_EC/rdacaa into 424-modulo-de-registro-de-unidades-moviles
 
> Fecha:  2019-01-11 con hash b503351 Realizado por Eduardo Garcia
##### Deshabilitando selección de fecha de atencion a 3 meses anteriores a la
fecha actual 
> Fecha:  2019-01-11 con hash 6f51092 Realizado por Saulo Ismael Velasco Rivera
##### Se corrige la carga por modificacion de datos para campos PARROQUIA Y PAIS - DNEAIS
 
> Fecha:  2019-01-10 con hash 4f155a8 Realizado por Eduardo Garcia
##### Merge branch '424-modulo-de-registro-de-unidades-moviles' of https://gitlab.com/MSP_EC/rdacaa into 424-modulo-de-registro-de-unidades-moviles
 
> Fecha:  2019-01-10 con hash 8044690 Realizado por Eduardo Garcia
##### Corrección de pruebas unitarias de reglas 
> Fecha:  2019-01-10 con hash 9ce0bd1 Realizado por Saulo Ismael Velasco Rivera
##### Merge branch '424-modulo-de-registro-de-unidades-moviles' of https://gitlab.com/MSP_EC/rdacaa into 424-modulo-de-registro-de-unidades-moviles
 
> Fecha:  2019-01-10 con hash 060ede5 Realizado por Eduardo Garcia
##### Se cambia funcionalidad CARGA PACIERNTES para pais por codigo y paroquia por codigo
 
> Fecha:  2019-01-10 con hash 5784900 Realizado por Eduardo Garcia
##### Frontend y reglas para atenciones médicas de enfermería 
> Fecha:  2019-01-10 con hash 6e9b480 Realizado por Saulo Ismael Velasco Rivera
##### Solución carga de diagnostico cuando cambia el sexo 
> Fecha:  2019-01-09 con hash c3a90b6 Realizado por Saulo Ismael Velasco Rivera
##### Merge branch '391-sistema-validaciones-malla-de-validacion-variables-individuales-3-de-3-parte-2' into 'dev'

Resolve "Sistema validaciones - malla de validación - Variables Individuales 3 de 3 PARTE 2"

See merge request MSP_EC/rdacaa!25 
> Fecha:  2019-01-09 con hash 71defc1 Realizado por Saulo Velasco
##### Se corrigen observaciones HILDA issue 427
 
> Fecha:  2019-01-09 con hash fb16d6e Realizado por Eduardo Garcia
##### Correcion SIVAN y cambios menores 
> Fecha:  2019-01-09 con hash e51476c Realizado por Saulo Ismael Velasco Rivera
##### Cambio ValidacionResult
 
> Fecha:  2019-01-08 con hash 179907b Realizado por bolivar.murillo
##### Se carga modificaciones en TEST de ODONTOLOGIA
 
> Fecha:  2019-01-08 con hash b553407 Realizado por Eduardo Garcia
##### Se modifica pantalla de TERMINOS Y CONDICIONES
 
> Fecha:  2019-01-08 con hash 457ad00 Realizado por Eduardo Garcia
##### Se modifica clases para odontologia - TEST
 
> Fecha:  2019-01-08 con hash 85b81ab Realizado por Eduardo Garcia
##### Merge branch '391-sistema-validaciones-malla-de-validacion-variables-individuales-3-de-3-parte-2' of https://gitlab.com/MSP_EC/rdacaa into 391-sistema-validaciones-malla-de-validacion-variables-individuales-3-de-3-parte-2
 
> Fecha:  2019-01-08 con hash 2e2615a Realizado por Eduardo Garcia
##### Se carga pantalla TERMINOS Y COBNDICIONES DE USO
 
> Fecha:  2019-01-08 con hash d62b185 Realizado por Eduardo Garcia
##### Cambio reporte atenciones
 
> Fecha:  2019-01-08 con hash 1d32e8d Realizado por bolivar.murillo
##### Corrección de reglas 
> Fecha:  2019-01-08 con hash 74fe6f8 Realizado por Saulo Ismael Velasco Rivera
##### Reglas para codigo parroquia ubicación 
> Fecha:  2019-01-04 con hash cd9cb7d Realizado por Saulo Ismael Velasco Rivera
##### Cambio pantalla Admision
 
> Fecha:  2019-01-04 con hash 806d2b0 Realizado por bolivar.murillo
##### Cambiando varibales de jvm para servicio web y creando nuevos campos y
tablas para ubicación de la atención 
> Fecha:  2019-01-04 con hash 2078001 Realizado por Saulo Ismael Velasco Rivera
##### Se añade mensaje para pacientes ya registrados em proceso de carga pacientes
 
> Fecha:  2019-01-04 con hash a49f253 Realizado por Eduardo Garcia
##### Merge branch '391-sistema-validaciones-malla-de-validacion-variables-individuales-3-de-3-parte-2' of https://gitlab.com/MSP_EC/rdacaa into 391-sistema-validaciones-malla-de-validacion-variables-individuales-3-de-3-parte-2
 
> Fecha:  2019-01-03 con hash ed9a969 Realizado por Eduardo Garcia
##### Agregando version a la URL del servicio web 
> Fecha:  2019-01-02 con hash 89a0a97 Realizado por Saulo Ismael Velasco Rivera
##### Control de fecha de aplicacion en pantalla de vacuna, tomando como referencia la fecha de atención
 
> Fecha:  2019-01-02 con hash 5b4add7 Realizado por miguel.faubla
##### Merge branch '391-sistema-validaciones-malla-de-validacion-variables-individuales-3-de-3-parte-2' of https://gitlab.com/MSP_EC/rdacaa into 391-sistema-validaciones-malla-de-validacion-variables-individuales-3-de-3-parte-2
 
> Fecha:  2019-01-02 con hash de90db6 Realizado por Eduardo Garcia
##### Se corrigen algnos puntos observados por calidad en la patalla de carga de pacientes
 
> Fecha:  2019-01-02 con hash e1537c8 Realizado por Eduardo Garcia
##### Arregalndo guardado de grupos prioritarios para nuevos usuarios 
> Fecha:  2018-12-28 con hash ff8cd05 Realizado por Saulo Ismael Velasco Rivera
##### Cambiando de orden de guardado de grupos prioritarios 
> Fecha:  2018-12-28 con hash 0865f18 Realizado por Saulo Ismael Velasco Rivera
##### Se corrige persistencia de grupos vulnerables y violencia
 
> Fecha:  2018-12-28 con hash 5b25551 Realizado por Eduardo Garcia
##### Solucion validacion subsitema contrareferencia y validación telefonos en
el frontend 
> Fecha:  2018-12-27 con hash 456a726 Realizado por Saulo Ismael Velasco Rivera
##### Merge branch '391-sistema-validaciones-malla-de-validacion-variables-individuales-3-de-3-parte-2' of https://gitlab.com/MSP_EC/rdacaa into 391-sistema-validaciones-malla-de-validacion-variables-individuales-3-de-3-parte-2
 
> Fecha:  2018-12-27 con hash 31ca556 Realizado por Eduardo Garcia
##### Se mejora el diseño y las observaciones echas por calidad en la pantalla de carga de pacientes
 
> Fecha:  2018-12-27 con hash c6ed15f Realizado por Eduardo Garcia
##### Corrigiendo constante parentesco agresor 
> Fecha:  2018-12-27 con hash e80a84e Realizado por Saulo Ismael Velasco Rivera
##### Cambio regla prueba vih
 
> Fecha:  2018-12-27 con hash 111beed Realizado por bolivar.murillo
##### Quitando impresiones a consola y agregando log debug 
> Fecha:  2018-12-27 con hash 713b27b Realizado por Saulo Ismael Velasco Rivera
##### Elimina comentarios de clase DETALLECATALOGOSERVICE - variables de
violencia 
> Fecha:  2018-12-27 con hash 3b5eeda Realizado por Eduardo Garcia
##### Quitando comentarios 
> Fecha:  2018-12-27 con hash a19cc0d Realizado por Saulo Ismael Velasco Rivera
##### Correción de validación de variables 
> Fecha:  2018-12-26 con hash bc14263 Realizado por Saulo Ismael Velasco Rivera
##### Merge branch '391-sistema-validaciones-malla-de-validacion-variables-individuales-3-de-3-parte-2' of https://gitlab.com/MSP_EC/rdacaa into 391-sistema-validaciones-malla-de-validacion-variables-individuales-3-de-3-parte-2
 
> Fecha:  2018-12-26 con hash 71e4ba4 Realizado por Saulo Ismael Velasco Rivera
##### Se corrige catalogos de violencia
 
> Fecha:  2018-12-26 con hash fa25c1a Realizado por Eduardo Garcia
##### Pantalla para carga de pacientes
 
> Fecha:  2018-12-21 con hash fe79a35 Realizado por Eduardo Garcia
##### Cambio en regla autoidentificacion etnica
 
> Fecha:  2018-12-21 con hash 7af8474 Realizado por bolivar.murillo
##### Merge branch '391-sistema-validaciones-malla-de-validacion-variables-individuales-3-de-3-parte-2' of https://gitlab.com/MSP_EC/rdacaa into 391-sistema-validaciones-malla-de-validacion-variables-individuales-3-de-3-parte-2
 
> Fecha:  2018-12-20 con hash 18f5a51 Realizado por Saulo Ismael Velasco Rivera
##### validacion procedimiento 
> Fecha:  2018-12-20 con hash 11eb5ee Realizado por Saulo Ismael Velasco Rivera
##### Cambio regla autoidentificacion etnica
 
> Fecha:  2018-12-20 con hash cc66823 Realizado por bolivar.murillo
##### Corección de rangos obligatorios para datos antropometricos 
> Fecha:  2018-12-20 con hash a87ce69 Realizado por Saulo Ismael Velasco Rivera
##### Cambio regla Orientacion sexual
 
> Fecha:  2018-12-20 con hash a27bf13 Realizado por bolivar.murillo
##### corrigiendo reglas de archivo e identidad de genero 
> Fecha:  2018-12-20 con hash f551098 Realizado por Saulo Ismael Velasco Rivera
##### Reporte de atenciones completo
 
> Fecha:  2018-12-20 con hash c4ac68b Realizado por bolivar.murillo
##### Reporte de atenciones completo
 
> Fecha:  2018-12-20 con hash aa142f7 Realizado por bolivar.murillo
##### corrigiendo variables 
> Fecha:  2018-12-20 con hash 3e664fd Realizado por Saulo Ismael Velasco Rivera
##### Reglas sivan, prescripcion, obstetricia 
> Fecha:  2018-12-20 con hash effe6fe Realizado por Saulo Ismael Velasco Rivera
##### Cambio reportes atenciones
 
> Fecha:  2018-12-18 con hash a3096c8 Realizado por bolivar.murillo
##### Merge branch '391-sistema-validaciones-malla-de-validacion-variables-individuales-3-de-3-parte-2' of https://gitlab.com/MSP_EC/rdacaa into 391-sistema-validaciones-malla-de-validacion-variables-individuales-3-de-3-parte-2
 
> Fecha:  2018-12-17 con hash a95eaae Realizado por Saulo Ismael Velasco Rivera
##### Vacunas y examenes de laboratorio 
> Fecha:  2018-12-17 con hash ad7f979 Realizado por Saulo Ismael Velasco Rivera
##### Cambios Admision
 
> Fecha:  2018-12-17 con hash ed1d4f7 Realizado por bolivar.murillo
##### Cambios Admision
 
> Fecha:  2018-12-17 con hash 730fe48 Realizado por bolivar.murillo
##### Merge branch '391-sistema-validaciones-malla-de-validacion-variables-individuales-3-de-3-parte-2' of https://gitlab.com/MSP_EC/rdacaa into 391-sistema-validaciones-malla-de-validacion-variables-individuales-3-de-3-parte-2
 
> Fecha:  2018-12-17 con hash 82823ea Realizado por Saulo Ismael Velasco Rivera
##### Validando edad y sexo para vacunas 
> Fecha:  2018-12-17 con hash 700d6eb Realizado por Saulo Ismael Velasco Rivera
##### Se carga pantalla para CARGA DE PACIENTES
 
> Fecha:  2018-12-17 con hash c5c6bb3 Realizado por Eduardo Garcia
##### Cambios Admision
 
> Fecha:  2018-12-17 con hash 3bab326 Realizado por bolivar.murillo
##### Merge branch '391-sistema-validaciones-malla-de-validacion-variables-individuales-3-de-3-parte-2' of https://gitlab.com/MSP_EC/rdacaa into 391-sistema-validaciones-malla-de-validacion-variables-individuales-3-de-3-parte-2
 
> Fecha:  2018-12-17 con hash b1136c8 Realizado por Eduardo Garcia
##### Validacion de vacunas 
> Fecha:  2018-12-14 con hash a6fa407 Realizado por Saulo Ismael Velasco Rivera
##### Merge branch '391-sistema-validaciones-malla-de-validacion-variables-individuales-3-de-3-parte-2' of https://gitlab.com/MSP_EC/rdacaa into 391-sistema-validaciones-malla-de-validacion-variables-individuales-3-de-3-parte-2
 
> Fecha:  2018-12-14 con hash d18abf4 Realizado por Eduardo Garcia
##### Se generan las pruebas para ODONTOLOGIA Y GRUPOS VULNERABLES
 
> Fecha:  2018-12-14 con hash 0246215 Realizado por Eduardo Garcia
##### Validando el lote de las vacunas 
> Fecha:  2018-12-14 con hash c69acab Realizado por Saulo Ismael Velasco Rivera
##### Validacion de fechas de vacunas 
> Fecha:  2018-12-14 con hash 6af06c7 Realizado por Saulo Ismael Velasco Rivera
##### reglas básicas 
> Fecha:  2018-12-13 con hash d0aea18 Realizado por Saulo Ismael Velasco Rivera
##### Merge branch '391-sistema-validaciones-malla-de-validacion-variables-individuales-3-de-3-parte-2' of https://gitlab.com/MSP_EC/rdacaa into 391-sistema-validaciones-malla-de-validacion-variables-individuales-3-de-3-parte-2
 
> Fecha:  2018-12-13 con hash 7166782 Realizado por Eduardo Garcia
##### Modifica reglas dientes cariados
 
> Fecha:  2018-12-13 con hash e82e110 Realizado por Eduardo Garcia
##### cambio fecha
 
> Fecha:  2018-12-13 con hash 5bb4351 Realizado por bolivar.murillo
##### Cambio formula reportes nutricion
 
> Fecha:  2018-12-13 con hash e626acb Realizado por bolivar.murillo
##### Merge branch '391-sistema-validaciones-malla-de-validacion-variables-individuales-3-de-3-parte-2' of https://gitlab.com/MSP_EC/rdacaa into 391-sistema-validaciones-malla-de-validacion-variables-individuales-3-de-3-parte-2
 
> Fecha:  2018-12-12 con hash 4ecfa2c Realizado por Eduardo Garcia
##### Resolviendo conflictos con calase de constantes
 
> Fecha:  2018-12-12 con hash 38db6db Realizado por Eduardo Garcia
##### Merge branch '391-sistema-validaciones-malla-de-validacion-variables-individuales-3-de-3-parte-2' of https://gitlab.com/MSP_EC/rdacaa into 391-sistema-validaciones-malla-de-validacion-variables-individuales-3-de-3-parte-2
 
> Fecha:  2018-12-12 con hash 84e7a43 Realizado por Saulo Ismael Velasco Rivera
##### Merge branch
'391-sistema-validaciones-malla-de-validacion-variables-individuales-3-de-3-parte-2'
of https://gitlab.com/MSP_EC/rdacaa into
391-sistema-validaciones-malla-de-validacion-variables-individuales-3-de-3-parte-2

# Conflicts:
#	rdacaa-validation/src/main/java/ec/gob/msp/rdacaa/business/validation/common/ValidationResultCatalogConstants.java
 
> Fecha:  2018-12-12 con hash 4a337d3 Realizado por Saulo Ismael Velasco Rivera
##### Arreglando ando pantalla de SIVAN
 
> Fecha:  2018-12-12 con hash e3a37f4 Realizado por miguel.faubla
##### Validaciones básicas 
> Fecha:  2018-12-12 con hash 8c02ed1 Realizado por Saulo Ismael Velasco Rivera
##### Prueba de test procedimiento
 
> Fecha:  2018-12-12 con hash c9ab54e Realizado por Eduardo Garcia
##### Merge branch '391-sistema-validaciones-malla-de-validacion-variables-individuales-3-de-3-parte-2' of https://gitlab.com/MSP_EC/rdacaa into 391-sistema-validaciones-malla-de-validacion-variables-individuales-3-de-3-parte-2
 
> Fecha:  2018-12-12 con hash c63c1a0 Realizado por bolivar.murillo
##### Reporte de Nutricion
 
> Fecha:  2018-12-12 con hash f542d9a Realizado por bolivar.murillo
##### se carga reglas para grupos vulnerables
 
> Fecha:  2018-12-12 con hash 11b3fa5 Realizado por Eduardo Garcia
##### reporteNutricion
 
> Fecha:  2018-12-12 con hash c7cbd32 Realizado por bolivar.murillo
##### Moificar constantes
 
> Fecha:  2018-12-12 con hash d669e54 Realizado por Eduardo Garcia
##### Rule group executors variables 
> Fecha:  2018-12-12 con hash c159243 Realizado por Saulo Ismael Velasco Rivera
##### Reglas para odontologia OBTURADOS, PERDIDOS, EXTRAIDOS
 
> Fecha:  2018-12-12 con hash 8b9f35c Realizado por Eduardo Garcia
##### Merge branch '391-sistema-validaciones-malla-de-validacion-variables-individuales-3-de-3-parte-2' of https://gitlab.com/MSP_EC/rdacaa into 391-sistema-validaciones-malla-de-validacion-variables-individuales-3-de-3-parte-2
 
> Fecha:  2018-12-12 con hash 3c64836 Realizado por bolivar.murillo
##### Rule Group Executor nuevas reglas 
> Fecha:  2018-12-12 con hash 3b14580 Realizado por Saulo Ismael Velasco Rivera
##### reporteNutricion
 
> Fecha:  2018-12-12 con hash e7e173a Realizado por bolivar.murillo
##### Reglas odontologia 
> Fecha:  2018-12-12 con hash b5cb066 Realizado por Saulo Ismael Velasco Rivera
##### Variables 
> Fecha:  2018-12-12 con hash 3b7c270 Realizado por Saulo Ismael Velasco Rivera
##### Merge branch '198-sistema-validaciones-malla-de-validacion-variables-individuales-3-de-3' into 'dev'

Resolve "Sistema validaciones - malla de validación - Variables Individuales 3 de 3 PARTE 1"

See merge request MSP_EC/rdacaa!22 
> Fecha:  2018-12-11 con hash b409570 Realizado por Saulo Velasco
##### Merge branch '198-sistema-validaciones-malla-de-validacion-variables-individuales-3-de-3' of https://gitlab.com/MSP_EC/rdacaa into 198-sistema-validaciones-malla-de-validacion-variables-individuales-3-de-3
 
> Fecha:  2018-12-11 con hash d9ce96c Realizado por bolivar.murillo
##### Merge branch '198-sistema-validaciones-malla-de-validacion-variables-individuales-3-de-3' into 'dev'

Resolve "Sistema validaciones - malla de validación - Variables Individuales 3 de 3 PARTE 1"

See merge request MSP_EC/rdacaa!21 
> Fecha:  2018-12-11 con hash b6df2c8 Realizado por Saulo Velasco
##### Se suben cambios pendientes locales
 
> Fecha:  2018-12-11 con hash 7ea08fb Realizado por Eduardo Garcia
##### Se corrigen BUGS presentados en SONARQUBE
 
> Fecha:  2018-12-11 con hash c95c9fd Realizado por Eduardo Garcia
##### Cierre de mes
 
> Fecha:  2018-12-11 con hash ac530ee Realizado por bolivar.murillo
##### Merge branch '198-sistema-validaciones-malla-de-validacion-variables-individuales-3-de-3' of https://gitlab.com/MSP_EC/rdacaa into 198-sistema-validaciones-malla-de-validacion-variables-individuales-3-de-3
 
> Fecha:  2018-12-11 con hash d9df353 Realizado por Eduardo Garcia
##### Se corrigen observaciones de territorio
 
> Fecha:  2018-12-11 con hash 74938b0 Realizado por Eduardo Garcia
##### Cambio cierre
 
> Fecha:  2018-12-10 con hash 0c7227d Realizado por bolivar.murillo
##### Merge branch '198-sistema-validaciones-malla-de-validacion-variables-individuales-3-de-3' of https://gitlab.com/MSP_EC/rdacaa into 198-sistema-validaciones-malla-de-validacion-variables-individuales-3-de-3
 
> Fecha:  2018-12-06 con hash 97db79b Realizado por bolivar.murillo
##### Reportes nutricion
 
> Fecha:  2018-12-06 con hash ab1c903 Realizado por bolivar.murillo
##### Corrigiendo orden de las pruebas 
> Fecha:  2018-12-06 con hash 20fd164 Realizado por Saulo Ismael Velasco Rivera
##### OK 
> Fecha:  2018-12-05 con hash 555a5e8 Realizado por Saulo Ismael Velasco Rivera
##### Nuevo atributo orden para ordenar la malla de validación 
> Fecha:  2018-12-05 con hash 0d64edb Realizado por Saulo Ismael Velasco Rivera
##### Formateando 
> Fecha:  2018-12-05 con hash 4238225 Realizado por Saulo Ismael Velasco Rivera
##### Merge branch
'198-sistema-validaciones-malla-de-validacion-variables-individuales-3-de-3'
of https://gitlab.com/MSP_EC/rdacaa into
198-sistema-validaciones-malla-de-validacion-variables-individuales-3-de-3

# Conflicts:
#	rdacaa-validation/src/main/java/ec/gob/msp/rdacaa/business/validation/common/ValidationResultCatalogConstants.java
 
> Fecha:  2018-12-05 con hash 9844120 Realizado por Saulo Ismael Velasco Rivera
##### Resolucion de conflicos
 
> Fecha:  2018-12-05 con hash 8acd949 Realizado por Saulo Ismael Velasco Rivera
##### se atiende al issue https://gitlab.com/MSP_EC/rdacaa/issues/388
 
> Fecha:  2018-12-05 con hash c1ca374 Realizado por Eduardo Garcia
##### Reglas de datos antropométricos
 
> Fecha:  2018-12-05 con hash bf04b59 Realizado por Saulo Ismael Velasco Rivera
##### Cambio reporte captacion
 
> Fecha:  2018-12-03 con hash 1b36fa5 Realizado por bolivar.murillo
##### Merge branch '198-sistema-validaciones-malla-de-validacion-variables-individuales-3-de-3' of https://gitlab.com/MSP_EC/rdacaa into 198-sistema-validaciones-malla-de-validacion-variables-individuales-3-de-3
 
> Fecha:  2018-12-03 con hash c253135 Realizado por bolivar.murillo
##### Cambio reporte
 
> Fecha:  2018-12-03 con hash e06d74d Realizado por bolivar.murillo
##### Se añaden reglas para PROCEDIMIENTOS
 
> Fecha:  2018-11-30 con hash a6cfc22 Realizado por Eduardo Garcia
##### Se añaden las reglas de procedimientos
 
> Fecha:  2018-11-29 con hash 4509a78 Realizado por Eduardo Garcia
##### Reglas para procedimientos-codigo
 
> Fecha:  2018-11-29 con hash 7e1cfb0 Realizado por Eduardo Garcia
##### Se carga logos para reportes
 
> Fecha:  2018-11-28 con hash 0f66e08 Realizado por Eduardo Garcia
##### Se modifica controlador de admision para ordenar catalogo IDENTIDAD DE GENERO
 
> Fecha:  2018-11-28 con hash 5e071a7 Realizado por Eduardo Garcia
##### Merge branch '198-sistema-validaciones-malla-de-validacion-variables-individuales-3-de-3' of https://gitlab.com/MSP_EC/rdacaa into 198-sistema-validaciones-malla-de-validacion-variables-individuales-3-de-3
 
> Fecha:  2018-11-27 con hash 7523638 Realizado por bolivar.murillo
##### reporteCaptacion
 
> Fecha:  2018-11-27 con hash 691894c Realizado por bolivar.murillo
##### Se generan las reglas para campo INTERCONSULTA
 
> Fecha:  2018-11-27 con hash 926a7dc Realizado por Eduardo Garcia
##### Se generan las raglas para LUGAR DE REFERENCIA
 
> Fecha:  2018-11-27 con hash fba4f4f Realizado por Eduardo Garcia
##### Se carga reglas de parroquia para referencia y contra referencia
 
> Fecha:  2018-11-26 con hash b387c71 Realizado por Eduardo Garcia
##### Reglas
 
> Fecha:  2018-11-26 con hash 3b32da7 Realizado por Eduardo Garcia
##### Merge branch '198-sistema-validaciones-malla-de-validacion-variables-individuales-3-de-3' of https://gitlab.com/MSP_EC/rdacaa into 198-sistema-validaciones-malla-de-validacion-variables-individuales-3-de-3
 
> Fecha:  2018-11-21 con hash 9b9b7ce Realizado por bolivar.murillo
##### Cambio valores autoidentificacion etnica
 
> Fecha:  2018-11-21 con hash 2bdcb95 Realizado por bolivar.murillo
##### Limpiando codigo comentado 
> Fecha:  2018-11-21 con hash 912f21e Realizado por Saulo Ismael Velasco Rivera
##### Merge remote-tracking branch 'origin/dev' into 198-sistema-validaciones-malla-de-validacion-variables-individuales-3-de-3
 
> Fecha:  2018-11-21 con hash fd8e4fc Realizado por Saulo Ismael Velasco Rivera
##### Merge branch '153-pras-carga-archivo-rdacaa-servicio-web-validaciones-rdacaa' into 'dev'

Resolve "PRAS - Carga Archivo .rdacaa - Servicio Web Validaciones RDACAA"

See merge request MSP_EC/rdacaa!18 
> Fecha:  2018-11-21 con hash e1ad4f9 Realizado por Saulo Velasco
##### Corrigiendo nombre de variable COD_VIOL_IDE_AGRESOR
 
> Fecha:  2018-11-21 con hash 9119174 Realizado por Saulo Ismael Velasco Rivera
##### Se modifica la version del SNAPSHOT a la numero 7
 
> Fecha:  2018-11-21 con hash abba30d Realizado por Eduardo Garcia
##### Merge branch '198-sistema-validaciones-malla-de-validacion-variables-individuales-3-de-3' of https://gitlab.com/MSP_EC/rdacaa into 198-sistema-validaciones-malla-de-validacion-variables-individuales-3-de-3
 
> Fecha:  2018-11-20 con hash d4a9907 Realizado por Saulo Ismael Velasco Rivera
##### Inicio de implementación de reglas para datos antropométricos 
> Fecha:  2018-11-20 con hash 789a170 Realizado por Saulo Ismael Velasco Rivera
##### fin malla vih
 
> Fecha:  2018-11-20 con hash a81ba7e Realizado por bolivar.murillo
##### Variable talla corregida 
> Fecha:  2018-11-20 con hash 2e78643 Realizado por Saulo Ismael Velasco Rivera
##### Merge branch '153-pras-carga-archivo-rdacaa-servicio-web-validaciones-rdacaa' into 'dev'

Resolve "PRAS - Carga Archivo .rdacaa - Servicio Web Validaciones RDACAA"

See merge request MSP_EC/rdacaa!16 
> Fecha:  2018-11-20 con hash 5944653 Realizado por Saulo Velasco
##### Merge branch '153-pras-carga-archivo-rdacaa-servicio-web-validaciones-rdacaa' of https://gitlab.com/MSP_EC/rdacaa into 153-pras-carga-archivo-rdacaa-servicio-web-validaciones-rdacaa
 
> Fecha:  2018-11-20 con hash 0a7fd4b Realizado por Eduardo Garcia
##### Se generan las reglas para PARENTESCOAGRESOR
 
> Fecha:  2018-11-20 con hash 6b248ba Realizado por Eduardo Garcia
##### Quitando coódigo comentado 
> Fecha:  2018-11-20 con hash 5ffcdb7 Realizado por Saulo Ismael Velasco Rivera
##### malla vih
 
> Fecha:  2018-11-20 con hash 52166c9 Realizado por bolivar.murillo
##### Diagnosticos 100% test
 
> Fecha:  2018-11-19 con hash e8aca7b Realizado por jazael.faubla
##### Bugs en la pantalla de vacunación corregidos
 
> Fecha:  2018-11-19 con hash 990f356 Realizado por jazael.faubla
##### Se rectifica las pruebas realizadas para IDENTIFICAAGRESOR
 
> Fecha:  2018-11-19 con hash e639915 Realizado por Eduardo Garcia
##### Merge branch '153-pras-carga-archivo-rdacaa-servicio-web-validaciones-rdacaa' of https://gitlab.com/MSP_EC/rdacaa into 153-pras-carga-archivo-rdacaa-servicio-web-validaciones-rdacaa
 
> Fecha:  2018-11-19 con hash 288047e Realizado por Eduardo Garcia
##### Se desarrollan las reglas para IDENTIFICAAGRESOR
 
> Fecha:  2018-11-19 con hash 915be9b Realizado por Eduardo Garcia
##### Bug en la lista de diagnostico, no refrescaba cuando cambias de sexo
 
> Fecha:  2018-11-16 con hash e58a5f9 Realizado por jazael.faubla
##### Se corrigen conflictos
 
> Fecha:  2018-11-16 con hash 45551b5 Realizado por Eduardo Garcia
##### Creando tabla temporal en memoria y barra de progreso 
> Fecha:  2018-11-16 con hash 261bddf Realizado por Saulo Ismael Velasco Rivera
##### Se cargan reglas de lesiones
 
> Fecha:  2018-11-16 con hash 48929c1 Realizado por Eduardo Garcia
##### Malla VIH Parte 1
 
> Fecha:  2018-11-16 con hash 42245ee Realizado por bolivar.murillo
##### Merge branch '153-pras-carga-archivo-rdacaa-servicio-web-validaciones-rdacaa' of https://gitlab.com/MSP_EC/rdacaa into 153-pras-carga-archivo-rdacaa-servicio-web-validaciones-rdacaa
 
> Fecha:  2018-11-16 con hash 6297f19 Realizado por Eduardo Garcia
##### Merge branch '153-pras-carga-archivo-rdacaa-servicio-web-validaciones-rdacaa' of https://gitlab.com/MSP_EC/rdacaa into 153-pras-carga-archivo-rdacaa-servicio-web-validaciones-rdacaa
 
> Fecha:  2018-11-16 con hash 856ed61 Realizado por bolivar.murillo
##### Bug en la lista de diagnostico, no refrescaba cuando cambias de sexo
 
> Fecha:  2018-11-16 con hash 44faa83 Realizado por jazael.faubla
##### Merge branch '153-pras-carga-archivo-rdacaa-servicio-web-validaciones-rdacaa' of https://gitlab.com/MSP_EC/rdacaa into 153-pras-carga-archivo-rdacaa-servicio-web-validaciones-rdacaa
 
> Fecha:  2018-11-16 con hash fda064f Realizado por bolivar.murillo
##### Malla
 
> Fecha:  2018-11-16 con hash 487ef68 Realizado por bolivar.murillo
##### Merge branch '153-pras-carga-archivo-rdacaa-servicio-web-validaciones-rdacaa' of https://gitlab.com/MSP_EC/rdacaa into 153-pras-carga-archivo-rdacaa-servicio-web-validaciones-rdacaa
 
> Fecha:  2018-11-16 con hash f7fca51 Realizado por Eduardo Garcia
##### Bug en el ocapacity al darle transparencia al Splash
 
> Fecha:  2018-11-16 con hash 4fe1ce4 Realizado por jazael.faubla
##### malla vih
 
> Fecha:  2018-11-16 con hash 9c9c95c Realizado por bolivar.murillo
##### Merge branch '153-pras-carga-archivo-rdacaa-servicio-web-validaciones-rdacaa' of https://gitlab.com/MSP_EC/rdacaa into 153-pras-carga-archivo-rdacaa-servicio-web-validaciones-rdacaa
 
> Fecha:  2018-11-16 con hash 02bef3a Realizado por Eduardo Garcia
##### Bug en el ocapacity al darle transparencia al Splash
 
> Fecha:  2018-11-15 con hash ccf9124 Realizado por jazael.faubla
##### Se cargan reglas para Identificacion de AGRESOR
 
> Fecha:  2018-11-15 con hash a974375 Realizado por Eduardo Garcia
##### Splash cargando componentes y test de diagnosticos
 
> Fecha:  2018-11-15 con hash 8d1cee7 Realizado por jazael.faubla
##### Validaciones finales 
> Fecha:  2018-11-15 con hash 945c722 Realizado por Saulo Ismael Velasco Rivera
##### Se carga las reglas para la variable IDENTIFICA AL AGRESOR
 
> Fecha:  2018-11-15 con hash f0a4b91 Realizado por Eduardo Garcia
##### Merge branch '153-pras-carga-archivo-rdacaa-servicio-web-validaciones-rdacaa' of https://gitlab.com/MSP_EC/rdacaa into 153-pras-carga-archivo-rdacaa-servicio-web-validaciones-rdacaa
 
> Fecha:  2018-11-15 con hash 562d14b Realizado por Eduardo Garcia
##### Cambio en VIH
 
> Fecha:  2018-11-14 con hash a31b580 Realizado por bolivar.murillo
##### Agregando condiciones adicionales 
> Fecha:  2018-11-14 con hash 12777d8 Realizado por Saulo Ismael Velasco Rivera
##### Merge branch '153-pras-carga-archivo-rdacaa-servicio-web-validaciones-rdacaa' of https://gitlab.com/MSP_EC/rdacaa into 153-pras-carga-archivo-rdacaa-servicio-web-validaciones-rdacaa
 
> Fecha:  2018-11-14 con hash 176531c Realizado por Saulo Ismael Velasco Rivera
##### Validaciones adicionales 
> Fecha:  2018-11-14 con hash ee8d446 Realizado por Saulo Ismael Velasco Rivera
##### Merge branch '153-pras-carga-archivo-rdacaa-servicio-web-validaciones-rdacaa' of https://gitlab.com/MSP_EC/rdacaa into 153-pras-carga-archivo-rdacaa-servicio-web-validaciones-rdacaa
 
> Fecha:  2018-11-14 con hash d937349 Realizado por Eduardo Garcia
##### Diagnosticos Reglas 75%
 
> Fecha:  2018-11-14 con hash dd3c540 Realizado por jazael.faubla
##### Se sube reglas para lesiones ocurridas
 
> Fecha:  2018-11-14 con hash f6ba2be Realizado por Eduardo Garcia
##### Merge branch '153-pras-carga-archivo-rdacaa-servicio-web-validaciones-rdacaa' of https://gitlab.com/MSP_EC/rdacaa into 153-pras-carga-archivo-rdacaa-servicio-web-validaciones-rdacaa
 
> Fecha:  2018-11-14 con hash 311c00c Realizado por Eduardo Garcia
##### Se añade nuevas reglas de lesiones
 
> Fecha:  2018-11-14 con hash b795379 Realizado por Eduardo Garcia
##### Notificaciones de errores para los CIE2, CIE3, CIE4, CIE5
 
> Fecha:  2018-11-14 con hash abea40b Realizado por jazael.faubla
##### Reportes correccion
 
> Fecha:  2018-11-14 con hash 18240d7 Realizado por bolivar.murillo
##### Merge branch '153-pras-carga-archivo-rdacaa-servicio-web-validaciones-rdacaa' of https://gitlab.com/MSP_EC/rdacaa into 153-pras-carga-archivo-rdacaa-servicio-web-validaciones-rdacaa
 
> Fecha:  2018-11-14 con hash 47cfed9 Realizado por Eduardo Garcia
##### Validacion de profesional y establecimiento 
> Fecha:  2018-11-13 con hash 1ab84b7 Realizado por svelasco
##### Merge branch '153-pras-carga-archivo-rdacaa-servicio-web-validaciones-rdacaa' of https://gitlab.com/MSP_EC/rdacaa into 153-pras-carga-archivo-rdacaa-servicio-web-validaciones-rdacaa
 
> Fecha:  2018-11-13 con hash 201d9ac Realizado por svelasco
##### Validacion de variables 
> Fecha:  2018-11-13 con hash 7ac2fd8 Realizado por svelasco
##### Se ordena los catalogos que tengan NOSABE/NORESPONDE por campo ORDENVISSUALIZACION
 
> Fecha:  2018-11-13 con hash 0b42c19 Realizado por Eduardo Garcia
##### Mostrar pantalla por especialidad y cuando sea menor a 9 años para enfermeria, enfermeria rural
 
> Fecha:  2018-11-13 con hash 7aceb9f Realizado por jazael.faubla
##### Verificar con perfil enfermería, auxiliar de enfermería y enfermería rural que los diagnósticos no sean obligatorios
 
> Fecha:  2018-11-13 con hash a2fcbe5 Realizado por jazael.faubla
##### Cambio grupos prioritarios catalogo Ninguno
 
> Fecha:  2018-11-13 con hash 604bbc1 Realizado por bolivar.murillo
##### Merge branch '153-pras-carga-archivo-rdacaa-servicio-web-validaciones-rdacaa' of https://gitlab.com/MSP_EC/rdacaa into 153-pras-carga-archivo-rdacaa-servicio-web-validaciones-rdacaa
 
> Fecha:  2018-11-13 con hash c45cfcb Realizado por jazael.faubla
##### Validación de archivos para proveedores externos 
> Fecha:  2018-11-13 con hash 097f179 Realizado por Saulo Ismael Velasco Rivera
##### Diagnostico 100%
 
> Fecha:  2018-11-13 con hash 16c6be4 Realizado por jazael.faubla
##### Merge branch '153-pras-carga-archivo-rdacaa-servicio-web-validaciones-rdacaa' of https://gitlab.com/MSP_EC/rdacaa into 153-pras-carga-archivo-rdacaa-servicio-web-validaciones-rdacaa
 
> Fecha:  2018-11-13 con hash 0a9dfee Realizado por jazael.faubla
##### Se modifica la pantalla de login
 
> Fecha:  2018-11-13 con hash caaade6 Realizado por Eduardo Garcia
##### Merge branch '153-pras-carga-archivo-rdacaa-servicio-web-validaciones-rdacaa' of https://gitlab.com/MSP_EC/rdacaa into 153-pras-carga-archivo-rdacaa-servicio-web-validaciones-rdacaa
 
> Fecha:  2018-11-13 con hash e0d463e Realizado por Eduardo Garcia
##### Se carga prueba para variables de violencia
 
> Fecha:  2018-11-13 con hash 806f815 Realizado por Eduardo Garcia
##### Merge branch '153-pras-carga-archivo-rdacaa-servicio-web-validaciones-rdacaa' of https://gitlab.com/MSP_EC/rdacaa into 153-pras-carga-archivo-rdacaa-servicio-web-validaciones-rdacaa
 
> Fecha:  2018-11-13 con hash c187e02 Realizado por jazael.faubla
##### Cambio en consulta de persona
 
> Fecha:  2018-11-13 con hash 6b7bd99 Realizado por bolivar.murillo
##### Merge branch '153-pras-carga-archivo-rdacaa-servicio-web-validaciones-rdacaa' of https://gitlab.com/MSP_EC/rdacaa into 153-pras-carga-archivo-rdacaa-servicio-web-validaciones-rdacaa
 
> Fecha:  2018-11-13 con hash 347ab97 Realizado por jazael.faubla
##### Malla validaciones representante
 
> Fecha:  2018-11-12 con hash e7d4006 Realizado por bolivar.murillo
##### Diagnosticos actualizando test
 
> Fecha:  2018-11-12 con hash 2e27d47 Realizado por jazael.faubla
##### Merge branch '153-pras-carga-archivo-rdacaa-servicio-web-validaciones-rdacaa' of https://gitlab.com/MSP_EC/rdacaa into 153-pras-carga-archivo-rdacaa-servicio-web-validaciones-rdacaa
 
> Fecha:  2018-11-12 con hash bf2332c Realizado por Saulo Ismael Velasco Rivera
##### Pantalla para validación de archivos de prestadores externos 
> Fecha:  2018-11-12 con hash f439554 Realizado por Saulo Ismael Velasco Rivera
##### se corrige clase GRUPEXECUTOR en archivo y numerodeserieformulario
 
> Fecha:  2018-11-12 con hash 43652c8 Realizado por Eduardo Garcia
##### Se corrige comflictos de merge
 
> Fecha:  2018-11-12 con hash 768cf58 Realizado por Eduardo Garcia
##### Se corrige problemas en clases TESt ARCHIVO NUMESERIEFORMULARIO
 
> Fecha:  2018-11-12 con hash f62f6ed Realizado por Eduardo Garcia
##### validacion malla se retira provincia y canton
 
> Fecha:  2018-11-12 con hash ab8442b Realizado por bolivar.murillo
##### Finalizando servicio web de validacion 
> Fecha:  2018-11-09 con hash 88a1bdb Realizado por Saulo Ismael Velasco Rivera
##### Malla Autoidentificacion Etnica
 
> Fecha:  2018-11-09 con hash 15645ff Realizado por bolivar.murillo
##### Malla provincia
 
> Fecha:  2018-11-08 con hash c26b575 Realizado por bolivar.murillo
##### Bajando y subiendo archivos al alfresco 
> Fecha:  2018-11-08 con hash 9b1e512 Realizado por Saulo Ismael Velasco Rivera
##### Filtro de diagnosticos por especialidad, edad y sexo
 
> Fecha:  2018-11-08 con hash 26ad0fd Realizado por jazael.faubla
##### Filtro de diagnosticos por especialidad, edad y sexo
 
> Fecha:  2018-11-08 con hash 6b8f2f9 Realizado por jazael.faubla
##### Se carga genera el compnente SPLASH
 
> Fecha:  2018-11-08 con hash 6b8301a Realizado por Eduardo Garcia
##### Quitando lombock como dependencia a empaquetar 
> Fecha:  2018-11-07 con hash 29a487b Realizado por svelasco
##### Cambio pantalla vih
 
> Fecha:  2018-11-07 con hash 765b31e Realizado por bolivar.murillo
##### Configuración servico web 
> Fecha:  2018-11-07 con hash 8c0659c Realizado por Saulo Ismael Velasco Rivera
##### Nuevas reglas 
> Fecha:  2018-11-07 con hash 7f1d50b Realizado por Saulo Ismael Velasco Rivera
##### Merge branch
'153-pras-carga-archivo-rdacaa-servicio-web-validaciones-rdacaa' of
https://gitlab.com/MSP_EC/rdacaa into
153-pras-carga-archivo-rdacaa-servicio-web-validaciones-rdacaa

# Conflicts:
#	rdacaa-validation-web-service/pom.xml
 
> Fecha:  2018-11-07 con hash d1feda5 Realizado por Saulo Ismael Velasco Rivera
##### Configuraciones del servicio web 
> Fecha:  2018-11-07 con hash 8ed536b Realizado por Saulo Ismael Velasco Rivera
##### web service
 
> Fecha:  2018-11-07 con hash 269c759 Realizado por bolivar.murillo
##### web service
 
> Fecha:  2018-11-07 con hash 70520e2 Realizado por bolivar.murillo
##### Correccion de observaciones de pilotaje
 
> Fecha:  2018-11-07 con hash 03b93be Realizado por Eduardo Garcia
##### tipo de seguro malla
 
> Fecha:  2018-11-07 con hash 3a3d5cd Realizado por bolivar.murillo
##### Merge branch '153-pras-carga-archivo-rdacaa-servicio-web-validaciones-rdacaa' of https://gitlab.com/MSP_EC/rdacaa into 153-pras-carga-archivo-rdacaa-servicio-web-validaciones-rdacaa
 
> Fecha:  2018-11-06 con hash c8a8cf5 Realizado por Eduardo Garcia
##### Se realiza cambios en la parte de admision - archivo
 
> Fecha:  2018-11-06 con hash e0ce549 Realizado por Eduardo Garcia
##### Proyecto de servicio web de validación 
> Fecha:  2018-11-06 con hash c6fcc90 Realizado por Saulo Ismael Velasco Rivera
##### Merge branch '197-sistema-validaciones-malla-de-validacion-variables-individuales-2-de-3' into 'dev'

Resolve "Sistema validaciones - malla de validación - Variables Individuales 2 de 3 PARTE I"

See merge request MSP_EC/rdacaa!15 
> Fecha:  2018-11-06 con hash 7916c83 Realizado por Saulo Velasco
##### Ejecución de malla de validación 
> Fecha:  2018-11-06 con hash 891c622 Realizado por Saulo Ismael Velasco Rivera
##### Merge branch '197-sistema-validaciones-malla-de-validacion-variables-individuales-2-de-3' of https://gitlab.com/MSP_EC/rdacaa into 197-sistema-validaciones-malla-de-validacion-variables-individuales-2-de-3
 
> Fecha:  2018-11-06 con hash a082e40 Realizado por bolivar.murillo
##### cambios en grupos prioritarios
 
> Fecha:  2018-11-06 con hash 3708265 Realizado por bolivar.murillo
##### Merge branch '197-sistema-validaciones-malla-de-validacion-variables-individuales-2-de-3' of https://gitlab.com/MSP_EC/rdacaa into 197-sistema-validaciones-malla-de-validacion-variables-individuales-2-de-3
 
> Fecha:  2018-11-06 con hash 1e704da Realizado por Eduardo Garcia
##### Se añade mensaje en pantalla diagnosticos
 
> Fecha:  2018-11-06 con hash 01add17 Realizado por Eduardo Garcia
##### Usuario invitado terminado y muestra de mensaje de advertencia cuando no hay perfiles en el usuario que se loguea
 
> Fecha:  2018-11-06 con hash cbb75b4 Realizado por jazael.faubla
##### Merge branch '197-sistema-validaciones-malla-de-validacion-variables-individuales-2-de-3' of https://gitlab.com/MSP_EC/rdacaa into 197-sistema-validaciones-malla-de-validacion-variables-individuales-2-de-3
 
> Fecha:  2018-11-06 con hash 51d0cd8 Realizado por jazael.faubla
##### Se carga el icono de arhivo valido
 
> Fecha:  2018-11-06 con hash 0a52e02 Realizado por Eduardo Garcia
##### Merge branch '197-sistema-validaciones-malla-de-validacion-variables-individuales-2-de-3' of https://gitlab.com/MSP_EC/rdacaa into 197-sistema-validaciones-malla-de-validacion-variables-individuales-2-de-3
 
> Fecha:  2018-11-06 con hash fff0ca8 Realizado por jazael.faubla
##### Trabajando con usuario invitado
 
> Fecha:  2018-11-06 con hash 24db633 Realizado por jazael.faubla
##### Se añade boton en pantalla principal para validar archivo
 
> Fecha:  2018-11-06 con hash 3f5e4f8 Realizado por Eduardo Garcia
##### Merge branch '197-sistema-validaciones-malla-de-validacion-variables-individuales-2-de-3' of https://gitlab.com/MSP_EC/rdacaa into 197-sistema-validaciones-malla-de-validacion-variables-individuales-2-de-3
 
> Fecha:  2018-11-06 con hash 14490a9 Realizado por bolivar.murillo
##### cambio boton vih
 
> Fecha:  2018-11-06 con hash 447b796 Realizado por bolivar.murillo
##### Se añade check INVITADO en pantalla login
 
> Fecha:  2018-11-06 con hash 75a87fa Realizado por Eduardo Garcia
##### cambio en pantalla de admiision
 
> Fecha:  2018-11-06 con hash 0ef422b Realizado por bolivar.murillo
##### Merge branch '197-sistema-validaciones-malla-de-validacion-variables-individuales-2-de-3' of https://gitlab.com/MSP_EC/rdacaa into 197-sistema-validaciones-malla-de-validacion-variables-individuales-2-de-3
 
> Fecha:  2018-11-05 con hash ea6bbdc Realizado por bolivar.murillo
##### Nacionalidad Mall
 
> Fecha:  2018-11-05 con hash 6f45aed Realizado por bolivar.murillo
##### Arreglando conflictos
 
> Fecha:  2018-11-05 con hash 8113f13 Realizado por jazael.faubla
##### Diagnosticos 80%
 
> Fecha:  2018-10-31 con hash b64c1fa Realizado por jazael.faubla
##### reglas malla Orientacion Sexual
 
> Fecha:  2018-10-31 con hash a5c94e4 Realizado por bolivar.murillo
##### validacion con llave públicq 
> Fecha:  2018-10-31 con hash c92ea52 Realizado por Saulo Ismael Velasco Rivera
##### Merge branch '197-sistema-validaciones-malla-de-validacion-variables-individuales-2-de-3' of https://gitlab.com/MSP_EC/rdacaa into 197-sistema-validaciones-malla-de-validacion-variables-individuales-2-de-3
 
> Fecha:  2018-10-30 con hash d33b848 Realizado por Saulo Ismael Velasco Rivera
##### Clases para cifrado y validacion de archivos sin cifrar 
> Fecha:  2018-10-30 con hash 1dbe74e Realizado por Saulo Ismael Velasco Rivera
##### Clases para cifrado y validacion de archivos sin cifrar 
> Fecha:  2018-10-30 con hash 0d9fb1f Realizado por Saulo Ismael Velasco Rivera
##### Se cambia la version del pom para que se cargue a gitlab
 
> Fecha:  2018-10-30 con hash a4649b5 Realizado por Eduardo Garcia
##### Merge branch '197-sistema-validaciones-malla-de-validacion-variables-individuales-2-de-3' of https://gitlab.com/MSP_EC/rdacaa into 197-sistema-validaciones-malla-de-validacion-variables-individuales-2-de-3
 
> Fecha:  2018-10-30 con hash da9f1fb Realizado por jazael.faubla
##### Variables de diagnosticos
 
> Fecha:  2018-10-30 con hash 0f1d753 Realizado por jazael.faubla
##### Se modifica el tolltip del la pantalla de PROCEDIMIENTOS
 
> Fecha:  2018-10-30 con hash 3f20804 Realizado por Eduardo Garcia
##### Se resuelve conflictos de versionamiento con ValidationResultCatalogConstants
 
> Fecha:  2018-10-30 con hash fe51a65 Realizado por Eduardo Garcia
##### Se generan las pruebas para el campo Numero Serie de variables de violencia
 
> Fecha:  2018-10-30 con hash 0134482 Realizado por Eduardo Garcia
##### Grupos Prioritarios Terminado con validacion si es mandatoria y si esta excluida o no
 
> Fecha:  2018-10-30 con hash 536b975 Realizado por jazael.faubla
##### Merge branch '197-sistema-validaciones-malla-de-validacion-variables-individuales-2-de-3' of https://gitlab.com/MSP_EC/rdacaa into 197-sistema-validaciones-malla-de-validacion-variables-individuales-2-de-3
 
> Fecha:  2018-10-30 con hash d7f6263 Realizado por bolivar.murillo
##### Reglas segundo nombre
 
> Fecha:  2018-10-30 con hash d92e588 Realizado por bolivar.murillo
##### Grupos Prioritarios Terminado con validacion si es mandatoria y si esta excluida o no
 
> Fecha:  2018-10-30 con hash 5eaed8f Realizado por jazael.faubla
##### Merge branch '197-sistema-validaciones-malla-de-validacion-variables-individuales-2-de-3' of https://gitlab.com/MSP_EC/rdacaa into 197-sistema-validaciones-malla-de-validacion-variables-individuales-2-de-3
 
> Fecha:  2018-10-30 con hash 3d7906a Realizado por jazael.faubla
##### Merge branch '197-sistema-validaciones-malla-de-validacion-variables-individuales-2-de-3' of https://gitlab.com/MSP_EC/rdacaa into 197-sistema-validaciones-malla-de-validacion-variables-individuales-2-de-3
 
> Fecha:  2018-10-30 con hash 8fa932d Realizado por bolivar.murillo
##### Exportando validaciones de persona y atenciones 
> Fecha:  2018-10-29 con hash f492cc7 Realizado por svelasco
##### Reemplazando por constante 
> Fecha:  2018-10-29 con hash 740ef84 Realizado por Saulo Ismael Velasco Rivera
##### Merge branch '197-sistema-validaciones-malla-de-validacion-variables-individuales-2-de-3' of https://gitlab.com/MSP_EC/rdacaa into 197-sistema-validaciones-malla-de-validacion-variables-individuales-2-de-3
 
> Fecha:  2018-10-29 con hash 42f835f Realizado por jazael.faubla
##### Exportación de validaciones 
> Fecha:  2018-10-29 con hash 3379bdf Realizado por Saulo Ismael Velasco Rivera
##### Agregando persona UUID 
> Fecha:  2018-10-29 con hash 9930b8f Realizado por Saulo Ismael Velasco Rivera
##### Tipo de Estrategia
 
> Fecha:  2018-10-29 con hash bb236e4 Realizado por jazael.faubla
##### Merge branch '197-sistema-validaciones-malla-de-validacion-variables-individuales-2-de-3' of https://gitlab.com/MSP_EC/rdacaa into 197-sistema-validaciones-malla-de-validacion-variables-individuales-2-de-3
 
> Fecha:  2018-10-29 con hash 80d2f26 Realizado por bolivar.murillo
##### Reglas identificacion
 
> Fecha:  2018-10-29 con hash 0d34063 Realizado por bolivar.murillo
##### Se corrige conflictos locales
 
> Fecha:  2018-10-29 con hash f5053ed Realizado por Eduardo Garcia
##### Tipo de Estrategia
 
> Fecha:  2018-10-29 con hash 596df7f Realizado por jazael.faubla
##### Merge branch '197-sistema-validaciones-malla-de-validacion-variables-individuales-2-de-3' of https://gitlab.com/MSP_EC/rdacaa into 197-sistema-validaciones-malla-de-validacion-variables-individuales-2-de-3
 
> Fecha:  2018-10-29 con hash 4c6c5d3 Realizado por Eduardo Garcia
##### Merge branch '197-sistema-validaciones-malla-de-validacion-variables-individuales-2-de-3' of https://gitlab.com/MSP_EC/rdacaa into 197-sistema-validaciones-malla-de-validacion-variables-individuales-2-de-3
 
> Fecha:  2018-10-29 con hash f6b4aec Realizado por jazael.faubla
##### Certificado medico
 
> Fecha:  2018-10-29 con hash ffb5b16 Realizado por jazael.faubla
##### Certificado medico
 
> Fecha:  2018-10-29 con hash d47ce92 Realizado por jazael.faubla
##### export
 
> Fecha:  2018-10-26 con hash 0c51c05 Realizado por Saulo Ismael Velasco Rivera
##### Se resuelve conflictos de ValidationResultCatalogoConstan
 
> Fecha:  2018-10-26 con hash 6d3f5c5 Realizado por Eduardo Garcia
##### Reglas de validacion para variables de violencia
 
> Fecha:  2018-10-26 con hash fff5b32 Realizado por Eduardo Garcia
##### Modificacion en reglas malla
 
> Fecha:  2018-10-26 con hash 43ccc82 Realizado por bolivar.murillo
##### Numero de certificado medico
 
> Fecha:  2018-10-26 con hash 08a4e56 Realizado por jazael.faubla
##### Merge branch '197-sistema-validaciones-malla-de-validacion-variables-individuales-2-de-3' of https://gitlab.com/MSP_EC/rdacaa into 197-sistema-validaciones-malla-de-validacion-variables-individuales-2-de-3
 
> Fecha:  2018-10-26 con hash fa0e1ec Realizado por jazael.faubla
##### Numero de certificado medico
 
> Fecha:  2018-10-26 con hash c820f88 Realizado por jazael.faubla
##### Reglas de validaciom tipo de identificacion
 
> Fecha:  2018-10-25 con hash 3b03587 Realizado por bolivar.murillo
##### variableHasErrors 
> Fecha:  2018-10-25 con hash 0b6fb1d Realizado por Saulo Ismael Velasco Rivera
##### Guardando en base de datos en batch resultados de validacion 
> Fecha:  2018-10-25 con hash f90e9c9 Realizado por Saulo Ismael Velasco Rivera
##### Variable condicion de diagnostico y tipo atencion diagnostico terminada
 
> Fecha:  2018-10-25 con hash 31bba0d Realizado por jazael.faubla
##### Malla de validación 
> Fecha:  2018-10-25 con hash 2b3d204 Realizado por Saulo Ismael Velasco Rivera
##### cambio en formato de fechas
 
> Fecha:  2018-10-25 con hash 89124ae Realizado por bolivar.murillo
##### Cambio de formato de fechas a YYYY-MM-DD en grupos prioritarios
 
> Fecha:  2018-10-25 con hash caf8faa Realizado por jazael.faubla
##### Merge branch '197-sistema-validaciones-malla-de-validacion-variables-individuales-2-de-3' of https://gitlab.com/MSP_EC/rdacaa into 197-sistema-validaciones-malla-de-validacion-variables-individuales-2-de-3
 
> Fecha:  2018-10-24 con hash 791156f Realizado por Saulo Ismael Velasco Rivera
##### Funcion validador 
> Fecha:  2018-10-24 con hash f8523cf Realizado por Saulo Ismael Velasco Rivera
##### cambio en formato de fechas
 
> Fecha:  2018-10-24 con hash 967e565 Realizado por bolivar.murillo
##### Cambios en reporte Atenciones
 
> Fecha:  2018-10-24 con hash 8dd3e06 Realizado por bolivar.murillo
##### Se carga los cambios pendientes
 
> Fecha:  2018-10-24 con hash 649e4f5 Realizado por Eduardo Garcia
##### Cambio en persona sexo
 
> Fecha:  2018-10-23 con hash 4282ec9 Realizado por bolivar.murillo
##### ValidatorFunction para ejecutar la malla de validacion 
> Fecha:  2018-10-23 con hash f97208d Realizado por Saulo Ismael Velasco Rivera
##### Reporte de Atenciones
 
> Fecha:  2018-10-23 con hash 480e9c9 Realizado por bolivar.murillo
##### Merge branch '197-sistema-validaciones-malla-de-validacion-variables-individuales-2-de-3' of https://gitlab.com/MSP_EC/rdacaa into 197-sistema-validaciones-malla-de-validacion-variables-individuales-2-de-3
 
> Fecha:  2018-10-23 con hash 5a7a2b6 Realizado por Eduardo Garcia
##### Exportando lista de filas a validar 
> Fecha:  2018-10-22 con hash d2485c3 Realizado por Saulo Ismael Velasco Rivera
##### Se corrige conflictos de codigo
 
> Fecha:  2018-10-22 con hash 7a99d48 Realizado por Eduardo Garcia
##### Funcionalidad para descifrar campos de persona para el usuario actual
del sistema antes de validar la información. 
> Fecha:  2018-10-22 con hash c5fbc12 Realizado por Saulo Ismael Velasco Rivera
##### Se corrige conflictos
 
> Fecha:  2018-10-22 con hash cde7ec6 Realizado por Eduardo Garcia
##### Variable codigo diagnostico 1 terminada
 
> Fecha:  2018-10-22 con hash 19b040c Realizado por jazael.faubla
##### Merge branch '197-sistema-validaciones-malla-de-validacion-variables-individuales-2-de-3' of https://gitlab.com/MSP_EC/rdacaa into 197-sistema-validaciones-malla-de-validacion-variables-individuales-2-de-3
 
> Fecha:  2018-10-22 con hash e0ae03e Realizado por Eduardo Garcia
##### reporte de atenciones a excel
 
> Fecha:  2018-10-22 con hash 313d7af Realizado por bolivar.murillo
##### Merge branch '197-sistema-validaciones-malla-de-validacion-variables-individuales-2-de-3' of https://gitlab.com/MSP_EC/rdacaa into 197-sistema-validaciones-malla-de-validacion-variables-individuales-2-de-3
 
> Fecha:  2018-10-22 con hash 8fdd54a Realizado por Eduardo Garcia
##### Merge branch '197-sistema-validaciones-malla-de-validacion-variables-individuales-2-de-3' of https://gitlab.com/MSP_EC/rdacaa into 197-sistema-validaciones-malla-de-validacion-variables-individuales-2-de-3
 
> Fecha:  2018-10-19 con hash be407c6 Realizado por bolivar.murillo
##### Reporte de atenciones medicaa
 
> Fecha:  2018-10-19 con hash 51f5a68 Realizado por bolivar.murillo
##### Merge branch '197-sistema-validaciones-malla-de-validacion-variables-individuales-2-de-3' of https://gitlab.com/MSP_EC/rdacaa into 197-sistema-validaciones-malla-de-validacion-variables-individuales-2-de-3
 
> Fecha:  2018-10-19 con hash ce8fb00 Realizado por Eduardo Garcia
##### Se modifica la clase validationQueryService - INTRAEXTRAMURAL
 
> Fecha:  2018-10-19 con hash eef2a25 Realizado por Eduardo Garcia
##### Validacion malla grupos prioritarios terminado
 
> Fecha:  2018-10-19 con hash 8dd5d6c Realizado por jazael.faubla
##### Grupo prioritario 1 realizando validacion de sexo y edad
 
> Fecha:  2018-10-19 con hash 9519816 Realizado por jazael.faubla
##### Cambio en reporte de vacunas
 
> Fecha:  2018-10-18 con hash e498ace Realizado por bolivar.murillo
##### 80% Grupos Priortarios Malla
 
> Fecha:  2018-10-18 con hash de384aa Realizado por jazael.faubla
##### Se modifica pantalla de login
 
> Fecha:  2018-10-18 con hash 63ca132 Realizado por Eduardo Garcia
##### Reporte de atenciones
 
> Fecha:  2018-10-17 con hash 8ae52d9 Realizado por bolivar.murillo
##### Merge branch '197-sistema-validaciones-malla-de-validacion-variables-individuales-2-de-3' of https://gitlab.com/MSP_EC/rdacaa into 197-sistema-validaciones-malla-de-validacion-variables-individuales-2-de-3
 
> Fecha:  2018-10-17 con hash 85d3444 Realizado por Eduardo Garcia
##### Cargando datos de archivo para malla de validacion 
> Fecha:  2018-10-17 con hash 97d34e4 Realizado por Saulo Ismael Velasco Rivera
##### Merge branch '197-sistema-validaciones-malla-de-validacion-variables-individuales-2-de-3' of https://gitlab.com/MSP_EC/rdacaa into 197-sistema-validaciones-malla-de-validacion-variables-individuales-2-de-3
 
> Fecha:  2018-10-17 con hash b2a3c7a Realizado por Eduardo Garcia
##### Se añade validacion para embarazada para grupos vulnerables
 
> Fecha:  2018-10-17 con hash d9033d4 Realizado por Eduardo Garcia
##### Merge branch '197-sistema-validaciones-malla-de-validacion-variables-individuales-2-de-3' of https://gitlab.com/MSP_EC/rdacaa into 197-sistema-validaciones-malla-de-validacion-variables-individuales-2-de-3
 
> Fecha:  2018-10-17 con hash e6409a0 Realizado por jazael.faubla
##### Grupos prioritarios malla de validacion
 
> Fecha:  2018-10-17 con hash 2f936e6 Realizado por jazael.faubla
##### Cambio reporte de produccion
 
> Fecha:  2018-10-17 con hash 52f8bdc Realizado por bolivar.murillo
##### Se organiza el orden de visualizacion de los botones de la pnatalla principal
 
> Fecha:  2018-10-17 con hash 45546ed Realizado por Eduardo Garcia
##### Se desarrollar funcinoalidad filtrado por edad iSUUE 169
 
> Fecha:  2018-10-17 con hash 8e6fbc0 Realizado por Eduardo Garcia
##### Mapeo validacion de grupos vulnerables 
> Fecha:  2018-10-17 con hash 0f1ba2b Realizado por Saulo Ismael Velasco Rivera
##### Merge branch '162-reporteria-rdacaa-atenciones-por-usuario' into 'dev'

Resolve "Reportería RDACAA - Atenciones por usuario"

See merge request MSP_EC/rdacaa!13 
> Fecha:  2018-10-16 con hash 73399cb Realizado por Saulo Velasco
##### Se ,pdifica la pantalla login
 
> Fecha:  2018-10-16 con hash d204bdd Realizado por Eduardo Garcia
##### Merge branch '162-reporteria-rdacaa-atenciones-por-usuario' of https://gitlab.com/MSP_EC/rdacaa into 162-reporteria-rdacaa-atenciones-por-usuario
 
> Fecha:  2018-10-16 con hash dea10c9 Realizado por bolivar.murillo
##### Cambio en vacunas seleccione
 
> Fecha:  2018-10-16 con hash 5815bd7 Realizado por bolivar.murillo
##### Subiendo a version 0.0.5 
> Fecha:  2018-10-16 con hash 5f651ea Realizado por Saulo Ismael Velasco Rivera
##### Reporte Atenciones modelo
 
> Fecha:  2018-10-16 con hash 0e57f31 Realizado por bolivar.murillo
##### Se corrigen errores de sonnar
 
> Fecha:  2018-10-16 con hash 5505a6e Realizado por Eduardo Garcia
##### Cargando referencias mediante autowired y cargando el orden desde la
base de datos 
> Fecha:  2018-10-16 con hash 7f634b6 Realizado por Saulo Ismael Velasco Rivera
##### Se modifica el diseño de la pantalla de login y se añade restriccion de 10 caracteres en caja de texto USUARIO
 
> Fecha:  2018-10-15 con hash 50a1f03 Realizado por Eduardo Garcia
##### Se corrige los conflictos con ACEESO.java
 
> Fecha:  2018-10-12 con hash bc9918e Realizado por Eduardo Garcia
##### Se modifica pantalla de inicio
 
> Fecha:  2018-10-12 con hash 46c63e1 Realizado por Eduardo Garcia
##### Formato excel de los reportes
 
> Fecha:  2018-10-12 con hash 4c738ff Realizado por bolivar.murillo
##### Merge branch '162-reporteria-rdacaa-atenciones-por-usuario' of https://gitlab.com/MSP_EC/rdacaa into 162-reporteria-rdacaa-atenciones-por-usuario
 
> Fecha:  2018-10-12 con hash d95b902 Realizado por Saulo Ismael Velasco Rivera
##### Modificando valores mínimos para talla 
> Fecha:  2018-10-12 con hash fa5f2b9 Realizado por Saulo Ismael Velasco Rivera
##### Color etiqueta mensaje vacunas
 
> Fecha:  2018-10-12 con hash e9a1681 Realizado por Eduardo Garcia
##### Validaciones de campos para edad 
> Fecha:  2018-10-12 con hash 79457a0 Realizado por Saulo Ismael Velasco Rivera
##### Formato de exportacion de reportes
 
> Fecha:  2018-10-12 con hash 1acffd2 Realizado por bolivar.murillo
##### observaciones pantalla de VACUNACION issue_238 terminado
 
> Fecha:  2018-10-12 con hash 85754f1 Realizado por jazael.faubla
##### Se cambia funcionalidad de pántalla DATOS SIVAN
 
> Fecha:  2018-10-11 con hash 30e78ee Realizado por Eduardo Garcia
##### Se resuelven casos con sivan
 
> Fecha:  2018-10-11 con hash 32f3541 Realizado por Eduardo Garcia
##### Cambio en exportacion de excel
 
> Fecha:  2018-10-11 con hash 9024c14 Realizado por bolivar.murillo
##### Cambios pantalla vacunación issue_238 (Tarea)
 
> Fecha:  2018-10-11 con hash 60e73ce Realizado por jazael.faubla
##### Guardando tipo bono en la atención 
> Fecha:  2018-10-11 con hash b3bc073 Realizado por Saulo Ismael Velasco Rivera
##### cambios en seguridades
 
> Fecha:  2018-10-11 con hash 01b9f63 Realizado por bolivar.murillo
##### cambios en seguridades
 
> Fecha:  2018-10-11 con hash 1fdd53d Realizado por bolivar.murillo
##### Variables de encriptacion y campos obligatorio variables antropometricas 
> Fecha:  2018-10-11 con hash 0112a8c Realizado por Saulo Ismael Velasco Rivera
##### Recomendaciones sonarqube 
> Fecha:  2018-10-11 con hash 7a34915 Realizado por Saulo Ismael Velasco Rivera
##### Perímetro cefálico deshabilitado para mayor de 5 años 
> Fecha:  2018-10-11 con hash f92a2c9 Realizado por Saulo Ismael Velasco Rivera
##### Cambio reportes registro de vacunas
 
> Fecha:  2018-10-11 con hash 30af89f Realizado por bolivar.murillo
##### Agregando esquema al combo de vacunas 
> Fecha:  2018-10-10 con hash 213ec9f Realizado por svelasco
##### Autogenerar primer cierre mensual 
> Fecha:  2018-10-10 con hash f12fbe5 Realizado por Saulo Ismael Velasco Rivera
##### Exportando archivo de variables rdacaa 
> Fecha:  2018-10-10 con hash 6fc9a51 Realizado por Saulo Ismael Velasco Rivera
##### Cambio ruta clave publica
 
> Fecha:  2018-10-10 con hash b8e38d3 Realizado por bolivar.murillo
##### Ruta actualizada 
> Fecha:  2018-10-10 con hash a1f5127 Realizado por Saulo Ismael Velasco Rivera
##### Variable de clave pública y exportación de archivos 
> Fecha:  2018-10-10 con hash 927c9f3 Realizado por Saulo Ismael Velasco Rivera
##### Cambio encriptacion a entidada persona
 
> Fecha:  2018-10-10 con hash 9894ca5 Realizado por bolivar.murillo
##### Merge branch '162-reporteria-rdacaa-atenciones-por-usuario' of https://gitlab.com/MSP_EC/rdacaa into 162-reporteria-rdacaa-atenciones-por-usuario
 
> Fecha:  2018-10-10 con hash 790646d Realizado por Saulo Ismael Velasco Rivera
##### Campos para cifrado 
> Fecha:  2018-10-10 con hash 1d96a7b Realizado por Saulo Ismael Velasco Rivera
##### Merge branch '162-reporteria-rdacaa-atenciones-por-usuario' of https://gitlab.com/MSP_EC/rdacaa into 162-reporteria-rdacaa-atenciones-por-usuario
 
> Fecha:  2018-10-09 con hash 625d820 Realizado por bolivar.murillo
##### Reportes y ecriptacion
 
> Fecha:  2018-10-09 con hash 521e4fc Realizado por bolivar.murillo
##### Se corrige conflictos
 
> Fecha:  2018-10-09 con hash 59d3d8b Realizado por Eduardo Garcia
##### Registro Atencion Controller
 
> Fecha:  2018-10-09 con hash 500e2cc Realizado por Eduardo Garcia
##### Nuevas variables y reporte 
> Fecha:  2018-10-09 con hash be6baf3 Realizado por Saulo Ismael Velasco Rivera
##### Merge branch '162-reporteria-rdacaa-atenciones-por-usuario' of https://gitlab.com/MSP_EC/rdacaa into 162-reporteria-rdacaa-atenciones-por-usuario
 
> Fecha:  2018-10-09 con hash 3bf9124 Realizado por Eduardo Garcia
##### Merge branch '162-reporteria-rdacaa-atenciones-por-usuario' of https://gitlab.com/MSP_EC/rdacaa into 162-reporteria-rdacaa-atenciones-por-usuario
 
> Fecha:  2018-10-09 con hash 62ab15a Realizado por Saulo Ismael Velasco Rivera
##### Agregango la anotación @Immutable para las enities de las vistas 
> Fecha:  2018-10-09 con hash 2ad0940 Realizado por Saulo Ismael Velasco Rivera
##### Merge branch '162-reporteria-rdacaa-atenciones-por-usuario' of https://gitlab.com/MSP_EC/rdacaa into 162-reporteria-rdacaa-atenciones-por-usuario
 
> Fecha:  2018-10-09 con hash bd5c12d Realizado por Eduardo Garcia
##### Se persiste el valor de la estrategia del paciente
 
> Fecha:  2018-10-09 con hash f3638f4 Realizado por Eduardo Garcia
##### cambios en modulo de seguridades
 
> Fecha:  2018-10-09 con hash ba6c943 Realizado por bolivar.murillo
##### Entities vistas
 
> Fecha:  2018-10-05 con hash d429513 Realizado por Saulo Ismael Velasco Rivera
##### Reportes
 
> Fecha:  2018-10-05 con hash 0d9bc9e Realizado por bolivar.murillo
##### borrando campo cipher 
> Fecha:  2018-10-05 con hash 130b270 Realizado por Saulo Ismael Velasco Rivera
##### Mapeando variables binarias para tabla Usuario 
> Fecha:  2018-10-05 con hash 6fbf544 Realizado por Saulo Ismael Velasco Rivera
##### Cambio vih
 
> Fecha:  2018-10-05 con hash edaff24 Realizado por bolivar.murillo
##### Merge branch '162-reporteria-rdacaa-atenciones-por-usuario' of https://gitlab.com/MSP_EC/rdacaa into 162-reporteria-rdacaa-atenciones-por-usuario
 
> Fecha:  2018-10-05 con hash c2e1e8b Realizado por bolivar.murillo
##### Agregando campos reactivos 
> Fecha:  2018-10-05 con hash 75530d0 Realizado por Saulo Ismael Velasco Rivera
##### Borrando archivos innecesarios 
> Fecha:  2018-10-05 con hash 34bd12c Realizado por Saulo Ismael Velasco Rivera
##### Solucion conflicto de rutas de la bse de datos para la ejecución 
> Fecha:  2018-10-05 con hash 7c3e81a Realizado por Saulo Ismael Velasco Rivera
##### Merge branch '162-reporteria-rdacaa-atenciones-por-usuario' of
https://gitlab.com/MSP_EC/rdacaa into
162-reporteria-rdacaa-atenciones-por-usuario

Conflicts:
	rdacaa-persistencia/src/main/resources/database-config.properties
 
> Fecha:  2018-10-05 con hash 5973b21 Realizado por bolivar.murillo
##### Validacion VIH
 
> Fecha:  2018-10-05 con hash 73b71c2 Realizado por bolivar.murillo
##### Merge branch '162-reporteria-rdacaa-atenciones-por-usuario' of
https://gitlab.com/MSP_EC/rdacaa into
162-reporteria-rdacaa-atenciones-por-usuario

# Conflicts:
#	rdacaa-frontend/src/main/java/ec/gob/msp/rdacaa/amed/controller/DatosAntropometricosController.java
 
> Fecha:  2018-10-05 con hash 068fc6e Realizado por Saulo Ismael Velasco Rivera
##### Implementando interfaz serializable 
> Fecha:  2018-10-05 con hash d499ae3 Realizado por Saulo Ismael Velasco Rivera
##### Se corrige conflictos con subida SONNAR
 
> Fecha:  2018-10-05 con hash df174af Realizado por Eduardo Garcia
##### Se corrigen las vulnerabilidades segun SONARCUBE
 
> Fecha:  2018-10-05 con hash b004a2e Realizado por Eduardo Garcia
##### Mensajes de validacion peso y talla y mapeo vihsida 
> Fecha:  2018-10-05 con hash 736a53d Realizado por Saulo Ismael Velasco Rivera
##### Merge branch '162-reporteria-rdacaa-atenciones-por-usuario' of https://gitlab.com/MSP_EC/rdacaa into 162-reporteria-rdacaa-atenciones-por-usuario
 
> Fecha:  2018-10-05 con hash 4b15db2 Realizado por Eduardo Garcia
##### Corrige alertas de sonnar
 
> Fecha:  2018-10-05 con hash 4308a80 Realizado por Eduardo Garcia
##### Cambios en VIH
 
> Fecha:  2018-10-04 con hash a4aedd1 Realizado por bolivar.murillo
##### Cambio de ruta para leer BDD
 
> Fecha:  2018-10-04 con hash 0b713e4 Realizado por Eduardo Garcia
##### Retornando 'false' si es valor erróneo 
> Fecha:  2018-10-04 con hash a8c7b28 Realizado por Saulo Ismael Velasco Rivera
##### Se corrige funcionalidad de VIOLENCIA cambio de criterio por parte del PO, campo numero formulario no es numerico es varchar
 
> Fecha:  2018-10-04 con hash e67a7fd Realizado por Eduardo Garcia
##### Se carga la funcionalidad del CERTIFICADO para diagnostco Z027
 
> Fecha:  2018-10-03 con hash 1c3f917 Realizado por Eduardo Garcia
##### Mapeo de nuevas tablas 
> Fecha:  2018-10-03 con hash a8f4fd9 Realizado por Saulo Ismael Velasco Rivera
##### Se carga la funcionalidad para GRUPOSVULNERABLES persiste a BDD
 
> Fecha:  2018-10-03 con hash cb95a81 Realizado por Eduardo Garcia
##### Merge branch '162-reporteria-rdacaa-atenciones-por-usuario' of https://gitlab.com/MSP_EC/rdacaa into 162-reporteria-rdacaa-atenciones-por-usuario
 
> Fecha:  2018-10-03 con hash 39dcc19 Realizado por Eduardo Garcia
##### Agregando fecha atencion en Admisión 
> Fecha:  2018-10-03 con hash b2993dc Realizado por Saulo Ismael Velasco Rivera
##### Guardando examen de sifilis 
> Fecha:  2018-10-03 con hash f5e1176 Realizado por Saulo Ismael Velasco Rivera
##### Se resuelve conflictos de COMONNCONTROLPANEL
 
> Fecha:  2018-10-03 con hash 218c194 Realizado por Eduardo Garcia
##### Se sube funcinalidad de grupos VULNERABLES
 
> Fecha:  2018-10-03 con hash 62b4ecf Realizado por Eduardo Garcia
##### Mapeo variables de vulnerabilidad 
> Fecha:  2018-10-02 con hash 1595572 Realizado por Saulo Ismael Velasco Rivera
##### Mejorando diseño de pantalla de reportes 
> Fecha:  2018-10-01 con hash 60b4e3a Realizado por Saulo Ismael Velasco Rivera
##### Guardando ondontologia, cargando filtros de diagnósticos, guardando
referencias e interconsultas. 
> Fecha:  2018-10-01 con hash 14c9618 Realizado por Saulo Ismael Velasco Rivera
##### Merge branch '194-sistema-validaciones-malla-de-validacion-variables-individuales-1-de-3' into 'dev'

Resolve "Sistema validaciones - malla de validación - Variables Individuales  1 de 3"

See merge request MSP_EC/rdacaa!10 
> Fecha:  2018-10-01 con hash 9822bf7 Realizado por Saulo Velasco
##### Se cambio la version del SNAPSHOT para versionar codigo fuente a test Sprint10
 
> Fecha:  2018-10-01 con hash 6eb8108 Realizado por Eduardo Garcia
##### Se corrigne conflictos con ESCRITORIOPRINCIPAL
 
> Fecha:  2018-10-01 con hash 10ea778 Realizado por Eduardo Garcia
##### Se realizan cambios en VIH y se corrigen observaciones SONNAR
 
> Fecha:  2018-10-01 con hash 6f9d83b Realizado por Eduardo Garcia
##### Cambio clave
 
> Fecha:  2018-09-28 con hash c4daaf7 Realizado por bolivar.murillo
##### Reportes
 
> Fecha:  2018-09-28 con hash 0cc961d Realizado por bolivar.murillo
##### Reportes
 
> Fecha:  2018-09-28 con hash 77f0bc2 Realizado por bolivar.murillo
##### Configuracino de BDD para WINDWOS
 
> Fecha:  2018-09-28 con hash 10d2f5f Realizado por Eduardo Garcia
##### Se añade la funcionalidad para eEPIDEMIOLOGIA
 
> Fecha:  2018-09-28 con hash 2bd241c Realizado por Eduardo Garcia
##### Guardar procedimientos 
> Fecha:  2018-09-28 con hash 932aa07 Realizado por Saulo Ismael Velasco Rivera
##### Tabla cie vigilancia 
> Fecha:  2018-09-27 con hash 1ff8c78 Realizado por Saulo Ismael Velasco Rivera
##### Agregando campos orden para la generacion de vistas 
> Fecha:  2018-09-27 con hash 8809791 Realizado por Saulo Ismael Velasco Rivera
##### Merge branch '194-sistema-validaciones-malla-de-validacion-variables-individuales-1-de-3' of https://gitlab.com/MSP_EC/rdacaa into 194-sistema-validaciones-malla-de-validacion-variables-individuales-1-de-3
 
> Fecha:  2018-09-26 con hash dd5cea2 Realizado por Eduardo Garcia
##### .
 
> Fecha:  2018-09-26 con hash cb3a14a Realizado por Eduardo Garcia
##### .
 
> Fecha:  2018-09-26 con hash da7fb30 Realizado por Eduardo Garcia
##### Generación automática de Fecha de creación de atención 
> Fecha:  2018-09-26 con hash e512120 Realizado por Saulo Ismael Velasco Rivera
##### Cambio pantalla procedimientos
 
> Fecha:  2018-09-26 con hash c63282d Realizado por bolivar.murillo
##### Cambiando texto N/A a No aplica
 
> Fecha:  2018-09-26 con hash 52ec7ec Realizado por Saulo Ismael Velasco Rivera
##### Se sube las nuevas validaciones para VIOLENCIA pero se comenta el guardado a la BDD porque da un error y hay presentacion
 
> Fecha:  2018-09-26 con hash 84534ee Realizado por Eduardo Garcia
##### Merge branch '194-sistema-validaciones-malla-de-validacion-variables-individuales-1-de-3' of https://gitlab.com/MSP_EC/rdacaa into 194-sistema-validaciones-malla-de-validacion-variables-individuales-1-de-3
 
> Fecha:  2018-09-25 con hash 7294c90 Realizado por Eduardo Garcia
##### Cambios revision dos
 
> Fecha:  2018-09-25 con hash 6a47693 Realizado por bolivar.murillo
##### Merge branch '194-sistema-validaciones-malla-de-validacion-variables-individuales-1-de-3' of https://gitlab.com/MSP_EC/rdacaa into 194-sistema-validaciones-malla-de-validacion-variables-individuales-1-de-3
 
> Fecha:  2018-09-25 con hash 034ab26 Realizado por Eduardo Garcia
##### Consulta personalizada ciexespecialidad 
> Fecha:  2018-09-25 con hash e645622 Realizado por Saulo Ismael Velasco Rivera
##### Se corrigen los conflictos
 
> Fecha:  2018-09-25 con hash f6c313e Realizado por Eduardo Garcia
##### Se añade las validaciones para informacion de direccion de paciente
 
> Fecha:  2018-09-25 con hash 90f106f Realizado por Eduardo Garcia
##### Reseteando colores de las categorías de los puntajes z 
> Fecha:  2018-09-25 con hash f10fce0 Realizado por Saulo Ismael Velasco Rivera
##### Cambios revision dos
 
> Fecha:  2018-09-25 con hash 97ce589 Realizado por bolivar.murillo
##### Cambios revision dos
 
> Fecha:  2018-09-25 con hash 0a38bfe Realizado por bolivar.murillo
##### corigiendo métodos para guardar valores antropometricos 
> Fecha:  2018-09-24 con hash 08e6bdd Realizado por Saulo Ismael Velasco Rivera
##### reglas nacionalidad
 
> Fecha:  2018-09-24 con hash 5fa0b97 Realizado por bolivar.murillo
##### Listas ordenadas
 
> Fecha:  2018-09-24 con hash 82d55c7 Realizado por bolivar.murillo
##### Quitando restriccion not null 
> Fecha:  2018-09-24 con hash 2504226 Realizado por Saulo Ismael Velasco Rivera
##### Quitando restriccion not nul para campo condicion de diagnostico 
> Fecha:  2018-09-24 con hash 80da09f Realizado por Saulo Ismael Velasco Rivera
##### cambio en archivo de pruebas
 
> Fecha:  2018-09-24 con hash 27a85db Realizado por bolivar.murillo
##### Se corrige peueba TipoSeguroRuleGroupExecutorTest
 
> Fecha:  2018-09-24 con hash d63f6e9 Realizado por Eduardo Garcia
##### Agregando nuevo campo posicion talla y guardando el valor de 0 "De pie"
1 "Acostado" 
> Fecha:  2018-09-24 con hash 05bad2a Realizado por Saulo Ismael Velasco Rivera
##### Se añade la validacion de lista de Grupos prioritarios no vacia
 
> Fecha:  2018-09-21 con hash 2c0f166 Realizado por Eduardo Garcia
##### Guardar variables antropometricas 
> Fecha:  2018-09-21 con hash d8bee83 Realizado por Saulo Ismael Velasco Rivera
##### Merge branch '194-sistema-validaciones-malla-de-validacion-variables-individuales-1-de-3' of https://gitlab.com/MSP_EC/rdacaa into 194-sistema-validaciones-malla-de-validacion-variables-individuales-1-de-3
 
> Fecha:  2018-09-21 con hash 114121e Realizado por Saulo Ismael Velasco Rivera
##### Manejando excepcion adicional 
> Fecha:  2018-09-21 con hash 8309ecc Realizado por Saulo Ismael Velasco Rivera
##### Merge branch '194-sistema-validaciones-malla-de-validacion-variables-individuales-1-de-3' of https://gitlab.com/MSP_EC/rdacaa into 194-sistema-validaciones-malla-de-validacion-variables-individuales-1-de-3
 
> Fecha:  2018-09-21 con hash 53e9987 Realizado por bolivar.murillo
##### cambio cie
 
> Fecha:  2018-09-21 con hash 7c35b06 Realizado por bolivar.murillo
##### Se corrige algunas observaciones echas por hilda en procedimientos y cie
 
> Fecha:  2018-09-21 con hash d1ad97f Realizado por Eduardo Garcia
##### Validaciones para mayor de 5 años 
> Fecha:  2018-09-21 con hash a42caaf Realizado por Saulo Ismael Velasco Rivera
##### Agregando referencia a catalogo 
> Fecha:  2018-09-21 con hash 852dd4b Realizado por Saulo Ismael Velasco Rivera
##### Merge branch '194-sistema-validaciones-malla-de-validacion-variables-individuales-1-de-3' of https://gitlab.com/MSP_EC/rdacaa into 194-sistema-validaciones-malla-de-validacion-variables-individuales-1-de-3
 
> Fecha:  2018-09-21 con hash e59dd07 Realizado por Eduardo Garcia
##### Solucionando mensajes de alertas IMC 
> Fecha:  2018-09-21 con hash 074cc61 Realizado por Saulo Ismael Velasco Rivera
##### Merge branch '194-sistema-validaciones-malla-de-validacion-variables-individuales-1-de-3' of https://gitlab.com/MSP_EC/rdacaa into 194-sistema-validaciones-malla-de-validacion-variables-individuales-1-de-3
 
> Fecha:  2018-09-21 con hash da15c01 Realizado por Eduardo Garcia
##### Merge branch '194-sistema-validaciones-malla-de-validacion-variables-individuales-1-de-3' of https://gitlab.com/MSP_EC/rdacaa into 194-sistema-validaciones-malla-de-validacion-variables-individuales-1-de-3
 
> Fecha:  2018-09-20 con hash aea99ed Realizado por Eduardo Garcia
##### Se añade la funcionalidad para guardar a la BDD datos de violencia
 
> Fecha:  2018-09-20 con hash 2ce01f7 Realizado por Eduardo Garcia
##### Cambio pantalla cie
 
> Fecha:  2018-09-20 con hash b6603f7 Realizado por bolivar.murillo
##### Controlando excepciones de datos antropometricos 
> Fecha:  2018-09-20 con hash c5d67b7 Realizado por Saulo Ismael Velasco Rivera
##### Agregando manejo de exepciones 
> Fecha:  2018-09-20 con hash 10bd85e Realizado por Saulo Ismael Velasco Rivera
##### Arreglando prueba unitaria 
> Fecha:  2018-09-20 con hash e67ac1d Realizado por Saulo Ismael Velasco Rivera
##### Merge branch
'194-sistema-validaciones-malla-de-validacion-variables-individuales-1-de-3'
of https://gitlab.com/MSP_EC/rdacaa into
194-sistema-validaciones-malla-de-validacion-variables-individuales-1-de-3

# Conflicts:
#	rdacaa-persistencia/src/main/java/ec/gob/msp/rdacaa/business/service/ConstantesDetalleCatalogo.java
 
> Fecha:  2018-09-20 con hash cb09f51 Realizado por Saulo Ismael Velasco Rivera
##### Agregar variables antrpometricas a variable de sesion 
> Fecha:  2018-09-20 con hash 90a0aa2 Realizado por Saulo Ismael Velasco Rivera
##### Se corrige interfaz de PROCEDIMIENTOS
 
> Fecha:  2018-09-20 con hash ee3be38 Realizado por Eduardo Garcia
##### Se corrige conflictos con DETALLECATALOGOSRVICE
 
> Fecha:  2018-09-20 con hash ef9aed7 Realizado por Eduardo Garcia
##### Se añade funcionalidad a la pantalla de VIOLENCIA
 
> Fecha:  2018-09-20 con hash ab632bb Realizado por Eduardo Garcia
##### Cambio panatalla de admision
 
> Fecha:  2018-09-20 con hash 83f23e4 Realizado por bolivar.murillo
##### Cambio datos atropométricos 
> Fecha:  2018-09-19 con hash 1d43583 Realizado por Saulo Ismael Velasco Rivera
##### Merge branch '194-sistema-validaciones-malla-de-validacion-variables-individuales-1-de-3' of https://gitlab.com/MSP_EC/rdacaa into 194-sistema-validaciones-malla-de-validacion-variables-individuales-1-de-3
 
> Fecha:  2018-09-19 con hash c0e18b3 Realizado por Saulo Ismael Velasco Rivera
##### Se corrige la funcionalidad de la pantalla de PRESCIPCION
 
> Fecha:  2018-09-19 con hash ae6f4c8 Realizado por Eduardo Garcia
##### cambios admision
 
> Fecha:  2018-09-19 con hash 96ec859 Realizado por bolivar.murillo
##### Merge branch '194-sistema-validaciones-malla-de-validacion-variables-individuales-1-de-3' of https://gitlab.com/MSP_EC/rdacaa into 194-sistema-validaciones-malla-de-validacion-variables-individuales-1-de-3
 
> Fecha:  2018-09-19 con hash 32773ed Realizado por Saulo Ismael Velasco Rivera
##### Mostrando los valores calculados para porcentaje z y sus categorías 
> Fecha:  2018-09-19 con hash 8ca2d70 Realizado por Saulo Ismael Velasco Rivera
##### Resolviendo los complictos
 
> Fecha:  2018-09-19 con hash db81ff4 Realizado por Eduardo Garcia
##### Se añaden las validaciones para TIPOSEGURO
 
> Fecha:  2018-09-19 con hash 9ea234a Realizado por Eduardo Garcia
##### runner tags:
 
> Fecha:  2018-09-19 con hash 2342347 Realizado por bolivar.murillo
##### runner tags
 
> Fecha:  2018-09-19 con hash e3756b4 Realizado por bolivar.murillo
##### runner tag
 
> Fecha:  2018-09-19 con hash 62a3ff6 Realizado por bolivar.murillo
##### Merge branch '194-sistema-validaciones-malla-de-validacion-variables-individuales-1-de-3' of https://gitlab.com/MSP_EC/rdacaa into 194-sistema-validaciones-malla-de-validacion-variables-individuales-1-de-3
 
> Fecha:  2018-09-19 con hash 5710e92 Realizado por Saulo Ismael Velasco Rivera
##### Agregando variables de puntaje z 
> Fecha:  2018-09-19 con hash f01cfa7 Realizado por Saulo Ismael Velasco Rivera
##### Merge branch '194-sistema-validaciones-malla-de-validacion-variables-individuales-1-de-3' of https://gitlab.com/MSP_EC/rdacaa into 194-sistema-validaciones-malla-de-validacion-variables-individuales-1-de-3
 
> Fecha:  2018-09-17 con hash eb903c5 Realizado por Eduardo Garcia
##### Merge branch '194-sistema-validaciones-malla-de-validacion-variables-individuales-1-de-3' of https://gitlab.com/MSP_EC/rdacaa into 194-sistema-validaciones-malla-de-validacion-variables-individuales-1-de-3
 
> Fecha:  2018-09-17 con hash d16a925 Realizado por bolivar.murillo
##### Se aumenta las reglas de sexo
 
> Fecha:  2018-09-17 con hash 3d912e9 Realizado por bolivar.murillo
##### Se añade la funcinalidad de la pantalla para violencia
 
> Fecha:  2018-09-17 con hash 21e0be4 Realizado por Eduardo Garcia
##### Categorias score z 
> Fecha:  2018-09-14 con hash 3385c01 Realizado por Saulo Ismael Velasco Rivera
##### Tabla y clases para acceso a datos de variables de violencia 
> Fecha:  2018-09-14 con hash 1ddca6d Realizado por Saulo Ismael Velasco Rivera
##### Merge branch '194-sistema-validaciones-malla-de-validacion-variables-individuales-1-de-3' of https://gitlab.com/MSP_EC/rdacaa into 194-sistema-validaciones-malla-de-validacion-variables-individuales-1-de-3
 
> Fecha:  2018-09-14 con hash d01f855 Realizado por Saulo Ismael Velasco Rivera
##### Repositorio y servicios de datos tabla lugar atencion validacion 
> Fecha:  2018-09-14 con hash 88a80b6 Realizado por Saulo Ismael Velasco Rivera
##### reglas fecha nacimiento
 
> Fecha:  2018-09-14 con hash d8d5a73 Realizado por bolivar.murillo
##### Merge branch '194-sistema-validaciones-malla-de-validacion-variables-individuales-1-de-3' of https://gitlab.com/MSP_EC/rdacaa into 194-sistema-validaciones-malla-de-validacion-variables-individuales-1-de-3
 
> Fecha:  2018-09-14 con hash bd737cc Realizado por bolivar.murillo
##### reglas fecha nacimiento
 
> Fecha:  2018-09-14 con hash 0a9826d Realizado por bolivar.murillo
##### Nuevas tablas y campos 
> Fecha:  2018-09-14 con hash cdc18b3 Realizado por Saulo Ismael Velasco Rivera
##### Se corrig econflictos con la clase ValidationResultCatalogConstants
 
> Fecha:  2018-09-14 con hash 8ca3d33 Realizado por Eduardo Garcia
##### Se crea las reglas para campo NUMEROARCHIVO
 
> Fecha:  2018-09-14 con hash ad7ce7f Realizado por Eduardo Garcia
##### Dialecto de base de datos personalizado y calculos y pruebas de score Z 
> Fecha:  2018-09-13 con hash 549baa7 Realizado por Saulo Ismael Velasco Rivera
##### Cambio en ruler Noidentificado
 
> Fecha:  2018-09-13 con hash bb0d1d4 Realizado por bolivar.murillo
##### Cambio en ruler
 
> Fecha:  2018-09-13 con hash 7982f9c Realizado por bolivar.murillo
##### Cambio controlador Admision
 
> Fecha:  2018-09-13 con hash 4b20b37 Realizado por bolivar.murillo
##### Merge branch
'194-sistema-validaciones-malla-de-validacion-variables-individuales-1-de-3'
of https://gitlab.com/MSP_EC/rdacaa into
194-sistema-validaciones-malla-de-validacion-variables-individuales-1-de-3

# Conflicts:
#	rdacaa-persistencia/src/main/java/ec/gob/msp/rdacaa/business/service/ConstantesDetalleCatalogo.java
 
> Fecha:  2018-09-12 con hash 27cd5f6 Realizado por Saulo Ismael Velasco Rivera
##### Cálculo score z 
> Fecha:  2018-09-12 con hash a7c1974 Realizado por Saulo Ismael Velasco Rivera
##### Validaciones Identificacion
 
> Fecha:  2018-09-12 con hash a181a01 Realizado por bolivar.murillo
##### Percentiles y calculos, reglas 
> Fecha:  2018-09-11 con hash 1a117d2 Realizado por Saulo Ismael Velasco Rivera
##### Validacion tipo identificacion
 
> Fecha:  2018-09-11 con hash 1829c1e Realizado por bolivar.murillo
##### Merge branch '144-sistema-validaciones-prestadores-externos-malla-de-validacion' into 'dev'

cambio runner

See merge request MSP_EC/rdacaa!8 
> Fecha:  2018-09-10 con hash 13bd2fe Realizado por Saulo Velasco
##### Cambio runner pruebas
 
> Fecha:  2018-09-10 con hash 5c6afb9 Realizado por bolivar.murillo
##### cambio runner
 
> Fecha:  2018-09-10 con hash 248e481 Realizado por bolivar.murillo
##### Merge branch '144-sistema-validaciones-prestadores-externos-malla-de-validacion' into 'dev'

Resolve "Sistema validaciones prestadores externos - malla de validación -estructura en base de datos y programacion"

See merge request MSP_EC/rdacaa!7 
> Fecha:  2018-09-07 con hash a468815 Realizado por Saulo Velasco
##### Subiendo de version y servicios para alerta de cálculo de Zs 
> Fecha:  2018-09-07 con hash 805043f Realizado por Saulo Ismael Velasco Rivera
##### Merge branch '144-sistema-validaciones-prestadores-externos-malla-de-validacion' of https://gitlab.com/MSP_EC/rdacaa into 144-sistema-validaciones-prestadores-externos-malla-de-validacion
 
> Fecha:  2018-09-06 con hash 5c4655e Realizado por Saulo Ismael Velasco Rivera
##### Motor de reglas y scroll automático 
> Fecha:  2018-09-06 con hash 8e5a7b5 Realizado por Saulo Ismael Velasco Rivera
##### Interconsulta y Referencia añadidos a la variable de sesión
 
> Fecha:  2018-09-06 con hash 432ca64 Realizado por jazael.faubla
##### Se modifica la pantalla para INTERCONSULTA y REFERENCIA
 
> Fecha:  2018-09-06 con hash 057c352 Realizado por Eduardo Garcia
##### Mejoras reglas de validacion 
> Fecha:  2018-09-05 con hash d2d1eb3 Realizado por Saulo Ismael Velasco Rivera
##### Agregando al wizard de paneles dinamicos con perfiles y espcecialidades las formas de violencia y grupos vulnerables
 
> Fecha:  2018-09-05 con hash 39cd89e Realizado por jazael.faubla
##### Cambio pantalla de acceso
 
> Fecha:  2018-09-05 con hash f1b9e42 Realizado por bolivar.murillo
##### Agregando pantalla de grupos vulnerables y violencia
 
> Fecha:  2018-09-05 con hash 4608959 Realizado por jazael.faubla
##### Referencia guardada en variable de sesion
 
> Fecha:  2018-09-05 con hash d12c163 Realizado por jazael.faubla
##### Merge branch '144-sistema-validaciones-prestadores-externos-malla-de-validacion' of https://gitlab.com/MSP_EC/rdacaa into 144-sistema-validaciones-prestadores-externos-malla-de-validacion
 
> Fecha:  2018-09-05 con hash 6f9a320 Realizado por jazael.faubla
##### Referencia Controller
 
> Fecha:  2018-09-05 con hash ae396d1 Realizado por jazael.faubla
##### Variables del RDACAA y mejora de reglas
 
> Fecha:  2018-09-04 con hash ab166a3 Realizado por Saulo Ismael Velasco Rivera
##### Guardando datos de Odontologia en la variable de sesión
 
> Fecha:  2018-09-04 con hash 20069c9 Realizado por jazael.faubla
##### Cambio grupos prioritarios
 
> Fecha:  2018-09-04 con hash e12a714 Realizado por bolivar.murillo
##### Odontologia a la variable de sesion
 
> Fecha:  2018-09-04 con hash ed4fe96 Realizado por jazael.faubla
##### Cambio pruebas vacunas
 
> Fecha:  2018-09-04 con hash 46bc483 Realizado por bolivar.murillo
##### Merge branch '144-sistema-validaciones-prestadores-externos-malla-de-validacion' of https://gitlab.com/MSP_EC/rdacaa into 144-sistema-validaciones-prestadores-externos-malla-de-validacion
 
> Fecha:  2018-09-03 con hash 4174e34 Realizado por Saulo Ismael Velasco Rivera
##### Malla de validacion 
> Fecha:  2018-09-03 con hash 07dbbf3 Realizado por Saulo Ismael Velasco Rivera
##### Merge branch '144-sistema-validaciones-prestadores-externos-malla-de-validacion' of https://gitlab.com/MSP_EC/rdacaa into 144-sistema-validaciones-prestadores-externos-malla-de-validacion
 
> Fecha:  2018-09-03 con hash 8902190 Realizado por Eduardo Garcia
##### Se cambia los id de CATALOGOS y DETALLECATALOGOS
 
> Fecha:  2018-09-03 con hash b6ea455 Realizado por Eduardo Garcia
##### Vacunas 95% seguir validando
 
> Fecha:  2018-09-03 con hash 3072506 Realizado por jazael.faubla
##### Merge branch '144-sistema-validaciones-prestadores-externos-malla-de-validacion' of https://gitlab.com/MSP_EC/rdacaa into 144-sistema-validaciones-prestadores-externos-malla-de-validacion
 
> Fecha:  2018-09-03 con hash c465045 Realizado por Eduardo Garcia
##### cambio en referencias
 
> Fecha:  2018-09-03 con hash 6d6fb05 Realizado por bolivar.murillo
##### Merge branch '144-sistema-validaciones-prestadores-externos-malla-de-validacion' of https://gitlab.com/MSP_EC/rdacaa into 144-sistema-validaciones-prestadores-externos-malla-de-validacion
 
> Fecha:  2018-09-03 con hash 5e4a76e Realizado por Eduardo Garcia
##### Merge branch '144-sistema-validaciones-prestadores-externos-malla-de-validacion' of https://gitlab.com/MSP_EC/rdacaa into 144-sistema-validaciones-prestadores-externos-malla-de-validacion
 
> Fecha:  2018-09-03 con hash 67589c7 Realizado por Saulo Ismael Velasco Rivera
##### Merge branch '144-sistema-validaciones-prestadores-externos-malla-de-validacion' of https://gitlab.com/MSP_EC/rdacaa into 144-sistema-validaciones-prestadores-externos-malla-de-validacion
 
> Fecha:  2018-09-03 con hash 7ab4514 Realizado por bolivar.murillo
##### Continuando con el proceso del core de validacion 
> Fecha:  2018-08-31 con hash cb0c6ad Realizado por Saulo Ismael Velasco Rivera
##### Vacunas y Dosis completadas en un 95%
 
> Fecha:  2018-08-31 con hash 5024af6 Realizado por jazael.faubla
##### Merge branch '144-sistema-validaciones-prestadores-externos-malla-de-validacion' of https://gitlab.com/MSP_EC/rdacaa into 144-sistema-validaciones-prestadores-externos-malla-de-validacion
 
> Fecha:  2018-08-31 con hash 1f191f4 Realizado por Eduardo Garcia
##### Vacunas y Dosis completadas en un 90%
 
> Fecha:  2018-08-31 con hash f214011 Realizado por jazael.faubla
##### Merge branch '144-sistema-validaciones-prestadores-externos-malla-de-validacion' of https://gitlab.com/MSP_EC/rdacaa into 144-sistema-validaciones-prestadores-externos-malla-de-validacion
 
> Fecha:  2018-08-31 con hash 91fc89e Realizado por bolivar.murillo
##### Vacunas cargadas
 
> Fecha:  2018-08-31 con hash 458388a Realizado por jazael.faubla
##### Merge branch '144-sistema-validaciones-prestadores-externos-malla-de-validacion' of https://gitlab.com/MSP_EC/rdacaa into 144-sistema-validaciones-prestadores-externos-malla-de-validacion
 
> Fecha:  2018-08-31 con hash abc5971 Realizado por bolivar.murillo
##### Mapeo esquema de vacunación 
> Fecha:  2018-08-31 con hash 1a41a2d Realizado por Saulo Ismael Velasco Rivera
##### Merge branch '144-sistema-validaciones-prestadores-externos-malla-de-validacion' of https://gitlab.com/MSP_EC/rdacaa into 144-sistema-validaciones-prestadores-externos-malla-de-validacion
 
> Fecha:  2018-08-30 con hash 250f559 Realizado por Saulo Ismael Velasco Rivera
##### Reglas y mapeo de variables
 
> Fecha:  2018-08-30 con hash 082cf75 Realizado por Saulo Ismael Velasco Rivera
##### Cambios procedimientos
 
> Fecha:  2018-08-30 con hash 43ccf8f Realizado por bolivar.murillo
##### Catalogo errores 
> Fecha:  2018-08-29 con hash d0e118d Realizado por Saulo Ismael Velasco Rivera
##### runnerTres
 
> Fecha:  2018-08-29 con hash 74cde60 Realizado por bolivar.murillo
##### runnerDos
 
> Fecha:  2018-08-29 con hash 1a34c96 Realizado por bolivar.murillo
##### runner
 
> Fecha:  2018-08-29 con hash d2f182b Realizado por bolivar.murillo
##### Solución recomendaciones de sonarqube 
> Fecha:  2018-08-29 con hash 9c91e9e Realizado por Saulo Ismael Velasco Rivera
##### Merge branch '144-sistema-validaciones-prestadores-externos-malla-de-validacion' of https://gitlab.com/MSP_EC/rdacaa into 144-sistema-validaciones-prestadores-externos-malla-de-validacion
 
> Fecha:  2018-08-29 con hash 1b84de7 Realizado por Eduardo Garcia
##### Cambios por fecha de atencion
 
> Fecha:  2018-08-28 con hash ac02d15 Realizado por bolivar.murillo
##### Eliminado cabecera de clases 
> Fecha:  2018-08-28 con hash 1863c41 Realizado por Saulo Ismael Velasco Rivera
##### Solución sonarqube 
> Fecha:  2018-08-28 con hash 773c0ce Realizado por Saulo Ismael Velasco Rivera
##### Merge branch '144-sistema-validaciones-prestadores-externos-malla-de-validacion' of https://gitlab.com/MSP_EC/rdacaa into 144-sistema-validaciones-prestadores-externos-malla-de-validacion
 
> Fecha:  2018-08-28 con hash 3c3fbd3 Realizado por Eduardo Garcia
##### Corrigiendo bugs en los controladores
 
> Fecha:  2018-08-28 con hash 40ddd1d Realizado por jazael.faubla
##### Mapeando tablas para el tarifario
 
> Fecha:  2018-08-28 con hash 1e25fc7 Realizado por Saulo Ismael Velasco Rivera
##### Quitando prueba de core de validacion
 
> Fecha:  2018-08-28 con hash 83b277d Realizado por Saulo Ismael Velasco Rivera
##### Add GP
 
> Fecha:  2018-08-28 con hash 939babf Realizado por Saulo Ismael Velasco Rivera
##### Solución bugs sonarqube
 
> Fecha:  2018-08-28 con hash a648e11 Realizado por jazael.faubla
##### Cambios por sonarqtres
 
> Fecha:  2018-08-28 con hash 404cf70 Realizado por bolivar.murillo
##### Merge branch '144-sistema-validaciones-prestadores-externos-malla-de-validacion' of https://gitlab.com/MSP_EC/rdacaa into 144-sistema-validaciones-prestadores-externos-malla-de-validacion
 
> Fecha:  2018-08-28 con hash d4221de Realizado por Eduardo Garcia
##### Se corrige conflicto de pull
 
> Fecha:  2018-08-28 con hash 7e1dc4b Realizado por Eduardo Garcia
##### Cambios de public a private en las formas examenes, procedimientos, referencia y prescripcion
 
> Fecha:  2018-08-28 con hash a84bdc3 Realizado por jazael.faubla
##### Cambios por sonarqdos
 
> Fecha:  2018-08-28 con hash c8d7283 Realizado por bolivar.murillo
##### Cambios por sonarq
 
> Fecha:  2018-08-28 con hash 4913c8b Realizado por bolivar.murillo
##### Agregando getters en las formas 
> Fecha:  2018-08-28 con hash 840486e Realizado por Saulo Ismael Velasco Rivera
##### Agregando getters y setters en la pantalla Admisión 
> Fecha:  2018-08-28 con hash 18a0bdc Realizado por Saulo Ismael Velasco Rivera
##### Nuevas tablas para el core de validación 
> Fecha:  2018-08-27 con hash 3056074 Realizado por Saulo Ismael Velasco Rivera
##### Merge branch '144-sistema-validaciones-prestadores-externos-malla-de-validacion' of https://gitlab.com/MSP_EC/rdacaa into 144-sistema-validaciones-prestadores-externos-malla-de-validacion
 
> Fecha:  2018-08-27 con hash 46870eb Realizado por jazael.faubla
##### Perfiles por especialidad terminado
 
> Fecha:  2018-08-27 con hash 07fec1f Realizado por jazael.faubla
##### cambiosxsonaq
 
> Fecha:  2018-08-27 con hash 359a467 Realizado por bolivar.murillo
##### Merge branch '144-sistema-validaciones-prestadores-externos-malla-de-validacion' of https://gitlab.com/MSP_EC/rdacaa into 144-sistema-validaciones-prestadores-externos-malla-de-validacion
 
> Fecha:  2018-08-27 con hash dc11a87 Realizado por Eduardo Garcia
##### Actualizando ambiente local y solucionando conflictos
 
> Fecha:  2018-08-27 con hash 9170069 Realizado por Eduardo Garcia
##### Soluciones para SonarQube 
> Fecha:  2018-08-27 con hash e22d90e Realizado por Saulo Ismael Velasco Rivera
##### Corrigiendo cambios en el codigo local para commit
 
> Fecha:  2018-08-27 con hash 310aa27 Realizado por Eduardo Garcia
##### Avance del core de validación 
> Fecha:  2018-08-24 con hash e013d4c Realizado por Saulo Ismael Velasco Rivera
##### Merge branch '144-sistema-validaciones-prestadores-externos-malla-de-validacion' of https://gitlab.com/MSP_EC/rdacaa into 144-sistema-validaciones-prestadores-externos-malla-de-validacion
 
> Fecha:  2018-08-24 con hash c6921d0 Realizado por jazael.faubla
##### Carga de paneles dinamicos + accion finalizar de manera dinamica
 
> Fecha:  2018-08-24 con hash ab64372 Realizado por jazael.faubla
##### Merge branch '144-sistema-validaciones-prestadores-externos-malla-de-validacion' of https://gitlab.com/MSP_EC/rdacaa into 144-sistema-validaciones-prestadores-externos-malla-de-validacion
 
> Fecha:  2018-08-24 con hash 06b1128 Realizado por Saulo Ismael Velasco Rivera
##### Clases básicas para la malla de validación
 
> Fecha:  2018-08-24 con hash 0789028 Realizado por Saulo Ismael Velasco Rivera
##### Botones por especialidades
 
> Fecha:  2018-08-24 con hash 8e67a17 Realizado por jazael.faubla
##### Corecciones SonarQube
 
> Fecha:  2018-08-24 con hash 9f336d7 Realizado por Saulo Ismael Velasco Rivera
##### Entidades para la malla de validación
 
> Fecha:  2018-08-23 con hash 67c5403 Realizado por Saulo Ismael Velasco Rivera
##### Paneles por especialidades
 
> Fecha:  2018-08-23 con hash becafd6 Realizado por jazael.faubla
##### Validando la pantalla de VIH SIDA con las observaciones presentadas en el ISSUE
 
> Fecha:  2018-08-22 con hash 516f679 Realizado por jazael.faubla
##### XMerge branch '144-sistema-validaciones-prestadores-externos-malla-de-validacion' of https://gitlab.com/MSP_EC/rdacaa into 144-sistema-validaciones-prestadores-externos-malla-de-validacion
 
> Fecha:  2018-08-22 con hash 49dc91c Realizado por Saulo Ismael Velasco Rivera
##### Agregando valores máximos para peso y talla
 
> Fecha:  2018-08-22 con hash a72e2ac Realizado por Saulo Ismael Velasco Rivera
##### Limpiando paneles
 
> Fecha:  2018-08-21 con hash 8b24c88 Realizado por jazael.faubla
##### Agregando tabla de pantallas para asignar permisos por especialidad
 
> Fecha:  2018-08-21 con hash 1480710 Realizado por Saulo Ismael Velasco Rivera
##### Agregando busqueda obviando caracteres ( y )
 
> Fecha:  2018-08-21 con hash 4832771 Realizado por Saulo Ismael Velasco Rivera
##### Selección de combo con Enter
 
> Fecha:  2018-08-21 con hash 357485c Realizado por Saulo Ismael Velasco Rivera
##### Persistiendo en la BD y limpiando variable de sesión
 
> Fecha:  2018-08-21 con hash 6deb5ec Realizado por jazael.faubla
##### Limpiando campos en formularios rdaca
 
> Fecha:  2018-08-21 con hash b8291a0 Realizado por jazael.faubla
##### Limpieza de catalogos
 
> Fecha:  2018-08-21 con hash 9048e67 Realizado por bolivar.murillo
##### Filtrado texto completo
 
> Fecha:  2018-08-20 con hash cda0577 Realizado por Saulo Ismael Velasco Rivera
##### Limpiando paneles
 
> Fecha:  2018-08-20 con hash 42b7757 Realizado por jazael.faubla
##### Agregando reporsitorio y permiso para la nueva entidad Accesoformaespecialidad
 
> Fecha:  2018-08-20 con hash 1784c1e Realizado por Saulo Ismael Velasco Rivera
##### Limpiando paneles
 
> Fecha:  2018-08-20 con hash 141b330 Realizado por jazael.faubla
##### Creando entity Accesoformaespecialidad y realizando recomendaciones de SonarQube
 
> Fecha:  2018-08-20 con hash eea34f1 Realizado por Saulo Ismael Velasco Rivera
##### Mejorando evento de diálogo de Acerca de...
 
> Fecha:  2018-08-20 con hash 89893e9 Realizado por Saulo Ismael Velasco Rivera
##### Arreglar conflicto
 
> Fecha:  2018-08-20 con hash 34d57d7 Realizado por Saulo Ismael Velasco Rivera
##### Recomendaciones de SonarQube
 
> Fecha:  2018-08-20 con hash 77e8ea7 Realizado por Saulo Ismael Velasco Rivera
##### Merge branch '144-sistema-validaciones-prestadores-externos-malla-de-validacion' of https://gitlab.com/MSP_EC/rdacaa into 144-sistema-validaciones-prestadores-externos-malla-de-validacion
 
> Fecha:  2018-08-20 con hash a9f054b Realizado por jazael.faubla
##### Ingresar de manea automatica cuando exista una especialidad cargada en el perfil del usuario
 
> Fecha:  2018-08-20 con hash 6c5d3f5 Realizado por jazael.faubla
##### Cambio variable de session
 
> Fecha:  2018-08-20 con hash cc4027f Realizado por bolivar.murillo
##### Subiendo version del snapshot a 0.0.2-SNAPSHOT
 
> Fecha:  2018-08-17 con hash 258d4c7 Realizado por Saulo Ismael Velasco Rivera
##### Merge branch '88-cierre-del-mes-atenciones-por-usuario' into 'dev'

Fin Sprint 3

See merge request MSP_EC/rdacaa!5 
> Fecha:  2018-08-17 con hash d93bc1d Realizado por Saulo Velasco
##### Corrigiendo sonarqube
 
> Fecha:  2018-08-17 con hash 53b86f4 Realizado por Saulo Ismael Velasco Rivera
##### Agregar el unicodigo  al combo de la entidad
 
> Fecha:  2018-08-16 con hash 92fa922 Realizado por lenin.vallejos
##### Mensaje de confirmación al guardar la atención médica
 
> Fecha:  2018-08-16 con hash cf9fd48 Realizado por lenin.vallejos
##### Merge branch '88-cierre-del-mes-atenciones-por-usuario' of https://gitlab.com/MSP_EC/rdacaa into 88-cierre-del-mes-atenciones-por-usuario
 
> Fecha:  2018-08-16 con hash 99b13c8 Realizado por bolivar.murillo
##### Cambio grupos prioritarios
 
> Fecha:  2018-08-16 con hash bcb5920 Realizado por bolivar.murillo
##### Mensaje de confirmación al guardar la atención médica
 
> Fecha:  2018-08-16 con hash 16b3fbe Realizado por lenin.vallejos
##### Habilitar y desabilitar componentes de VIH SIDA
 
> Fecha:  2018-08-16 con hash 0813f1a Realizado por lenin.vallejos
##### Merge branch '88-cierre-del-mes-atenciones-por-usuario' of https://gitlab.com/MSP_EC/rdacaa into 88-cierre-del-mes-atenciones-por-usuario
 
> Fecha:  2018-08-16 con hash 9b718d8 Realizado por bolivar.murillo
##### Cambio letras
 
> Fecha:  2018-08-16 con hash 60a39c0 Realizado por bolivar.murillo
##### Validación de caracteres especiales
 
> Fecha:  2018-08-16 con hash 156e2d0 Realizado por lenin.vallejos
##### Merge branch '88-cierre-del-mes-atenciones-por-usuario' of https://gitlab.com/MSP_EC/rdacaa into 88-cierre-del-mes-atenciones-por-usuario
 
> Fecha:  2018-08-16 con hash 45f1c79 Realizado por bolivar.murillo
##### Cambio caracteres especiales
 
> Fecha:  2018-08-16 con hash 355575e Realizado por bolivar.murillo
##### Se arregla conflicto de codigo SEMANASDE GEATACION TOLLTIP
 
> Fecha:  2018-08-16 con hash 4a72ff0 Realizado por Eduardo Garcia
##### Se añade la etiqueta campos obligatorios en las pantallas que aplican
 
> Fecha:  2018-08-16 con hash 9c3a8d9 Realizado por Eduardo Garcia
##### Merge branch '88-cierre-del-mes-atenciones-por-usuario' of https://gitlab.com/MSP_EC/rdacaa into 88-cierre-del-mes-atenciones-por-usuario
 
> Fecha:  2018-08-16 con hash 4f9e44b Realizado por lenin.vallejos
##### Riesgo obstetrico como no obligatorio
 
> Fecha:  2018-08-16 con hash f0f7214 Realizado por lenin.vallejos
##### Merge branch '88-cierre-del-mes-atenciones-por-usuario' of https://gitlab.com/MSP_EC/rdacaa into 88-cierre-del-mes-atenciones-por-usuario
 
> Fecha:  2018-08-16 con hash 66a40ae Realizado por Eduardo Garcia
##### Se añade boton en VIH para limpiar campos
 
> Fecha:  2018-08-16 con hash afe4e43 Realizado por Eduardo Garcia
##### Cierre de paneles funcionando correctamente para pŕofesionales y reporte de cierre de mes
 
> Fecha:  2018-08-16 con hash d863d63 Realizado por lenin.vallejos
##### Mensaje de información para la confirmación del guardado de la atención médica
 
> Fecha:  2018-08-16 con hash 5a516a8 Realizado por lenin.vallejos
##### Control pantalla de VIH campos requeridos y no requeridos
 
> Fecha:  2018-08-16 con hash c9682d7 Realizado por lenin.vallejos
##### Solucionando conflictos
 
> Fecha:  2018-08-16 con hash 8b1c975 Realizado por Saulo Ismael Velasco Rivera
##### validacion de pantallas por edad
 
> Fecha:  2018-08-16 con hash 73e609b Realizado por Saulo Ismael Velasco Rivera
##### Merge branch '88-cierre-del-mes-atenciones-por-usuario' of https://gitlab.com/MSP_EC/rdacaa into 88-cierre-del-mes-atenciones-por-usuario
 
> Fecha:  2018-08-16 con hash 4642254 Realizado por bolivar.murillo
##### Cambio variable de session
 
> Fecha:  2018-08-16 con hash 0865c2d Realizado por bolivar.murillo
##### Merge branch '88-cierre-del-mes-atenciones-por-usuario' of https://gitlab.com/MSP_EC/rdacaa into 88-cierre-del-mes-atenciones-por-usuario
 
> Fecha:  2018-08-16 con hash 8c6a3c7 Realizado por Eduardo Garcia
##### Control de dosis repetidas en la lista de vacunas en atencion medica
 
> Fecha:  2018-08-16 con hash 3fd1067 Realizado por lenin.vallejos
##### Se añade la funcionalidad para ELIMINAR registros en la tabla de procedimientos
 
> Fecha:  2018-08-16 con hash 8d94958 Realizado por Eduardo Garcia
##### Merge branch '88-cierre-del-mes-atenciones-por-usuario' of https://gitlab.com/MSP_EC/rdacaa into 88-cierre-del-mes-atenciones-por-usuario
 
> Fecha:  2018-08-16 con hash f97aef5 Realizado por lenin.vallejos
##### Limpiar campos en los diagnosticos
 
> Fecha:  2018-08-16 con hash 969b61f Realizado por lenin.vallejos
##### Se corrige la distribucion de los botones de la pantalla principal
 
> Fecha:  2018-08-16 con hash ef35a87 Realizado por Eduardo Garcia
##### Merge branch '88-cierre-del-mes-atenciones-por-usuario' of https://gitlab.com/MSP_EC/rdacaa into 88-cierre-del-mes-atenciones-por-usuario
 
> Fecha:  2018-08-16 con hash 4dc86e1 Realizado por Eduardo Garcia
##### Limpiar campos en los diagnosticos
 
> Fecha:  2018-08-16 con hash 89c26b4 Realizado por lenin.vallejos
##### Merge branch '88-cierre-del-mes-atenciones-por-usuario' of https://gitlab.com/MSP_EC/rdacaa into 88-cierre-del-mes-atenciones-por-usuario
 
> Fecha:  2018-08-16 con hash 966c243 Realizado por bolivar.murillo
##### Cambio en pantalla admision
 
> Fecha:  2018-08-16 con hash 72ccffd Realizado por bolivar.murillo
##### Merge branch '88-cierre-del-mes-atenciones-por-usuario' of https://gitlab.com/MSP_EC/rdacaa into 88-cierre-del-mes-atenciones-por-usuario
 
> Fecha:  2018-08-15 con hash b5a94f4 Realizado por Eduardo Garcia
##### Se cambia funcioanlidad de Informacion Riesgo Obstetrico
 
> Fecha:  2018-08-15 con hash a894146 Realizado por Eduardo Garcia
##### Merge branch '88-cierre-del-mes-atenciones-por-usuario' of https://gitlab.com/MSP_EC/rdacaa into 88-cierre-del-mes-atenciones-por-usuario
 
> Fecha:  2018-08-15 con hash c7e998a Realizado por Saulo Ismael Velasco Rivera
##### Borrando entradas
 
> Fecha:  2018-08-15 con hash 92b0826 Realizado por Saulo Ismael Velasco Rivera
##### Merge branch '88-cierre-del-mes-atenciones-por-usuario' of https://gitlab.com/MSP_EC/rdacaa into 88-cierre-del-mes-atenciones-por-usuario
 
> Fecha:  2018-08-15 con hash d057691 Realizado por lenin.vallejos
##### Agregar boton eliminar en la tabla de diagnosticos
 
> Fecha:  2018-08-15 con hash 598bf89 Realizado por lenin.vallejos
##### Merge branch '88-cierre-del-mes-atenciones-por-usuario' of https://gitlab.com/MSP_EC/rdacaa into 88-cierre-del-mes-atenciones-por-usuario
 
> Fecha:  2018-08-15 con hash dace823 Realizado por bolivar.murillo
##### Limpiar campos admision
 
> Fecha:  2018-08-15 con hash 391054f Realizado por bolivar.murillo
##### Agregando mensajes de validación detallados para peso y talla, para edad años, meses días
 
> Fecha:  2018-08-15 con hash 04193fa Realizado por Saulo Ismael Velasco Rivera
##### No permitir avanzar para valores erroneos de imc y valor HB riesgo
 
> Fecha:  2018-08-15 con hash 5a06927 Realizado por Saulo Ismael Velasco Rivera
##### Cambio selecionar todo
 
> Fecha:  2018-08-15 con hash 39de771 Realizado por bolivar.murillo
##### Se añade el filtro V01-Y68 en catalogo CIE
 
> Fecha:  2018-08-15 con hash 1228e2b Realizado por Eduardo Garcia
##### Habilitar y desabilitar botones dentro del flujo de atencion RDACA
 
> Fecha:  2018-08-15 con hash f4896c5 Realizado por lenin.vallejos
##### Semanas de gestación agregada a la variable de sesion y agregada al header informativo
 
> Fecha:  2018-08-15 con hash 9a2e323 Realizado por lenin.vallejos
##### Merge branch '88-cierre-del-mes-atenciones-por-usuario' of https://gitlab.com/MSP_EC/rdacaa into 88-cierre-del-mes-atenciones-por-usuario
 
> Fecha:  2018-08-14 con hash e0b13cb Realizado por bolivar.murillo
##### Cambios Seleccione all
 
> Fecha:  2018-08-14 con hash ed33446 Realizado por bolivar.murillo
##### Resolución del orden del Wizard Button
 
> Fecha:  2018-08-14 con hash 5f1b204 Realizado por lenin.vallejos
##### Cambios Seleccione
 
> Fecha:  2018-08-14 con hash 7dc00db Realizado por bolivar.murillo
##### Correccion en la pantalla de admision
 
> Fecha:  2018-08-14 con hash f075fc1 Realizado por Eduardo Garcia
##### Resolución del orden del Wizard Button
 
> Fecha:  2018-08-14 con hash e178254 Realizado por lenin.vallejos
##### Merge branch '88-cierre-del-mes-atenciones-por-usuario' of https://gitlab.com/MSP_EC/rdacaa into 88-cierre-del-mes-atenciones-por-usuario
 
> Fecha:  2018-08-14 con hash 1be5c46 Realizado por bolivar.murillo
##### Cambios Admision
 
> Fecha:  2018-08-14 con hash 29c88ce Realizado por bolivar.murillo
##### Se modifica la pantalla de admision
 
> Fecha:  2018-08-14 con hash 4dfdc30 Realizado por Eduardo Garcia
##### Persistiendo datos al finalizar la atención
 
> Fecha:  2018-08-14 con hash add8d94 Realizado por lenin.vallejos
##### Se modifica la pnatalla INFO OBSTETRICA
 
> Fecha:  2018-08-14 con hash 1d1ded7 Realizado por Eduardo Garcia
##### Se modifica la pantalla de admision para incluir ARCHIVO
 
> Fecha:  2018-08-14 con hash b551a7d Realizado por Eduardo Garcia
##### Persistiendo a la base de datos
 
> Fecha:  2018-08-14 con hash 873fdcb Realizado por lenin.vallejos
##### Cambio Admision
 
> Fecha:  2018-08-14 con hash be85558 Realizado por bolivar.murillo
##### Pantalla de prescripcion grupo de botones con validacion y guardando en la variable de sesión
 
> Fecha:  2018-08-14 con hash 1ae6213 Realizado por lenin.vallejos
##### Pantalla de prescripcion grupo de botones con validacion y guardando en la variable de sesión
 
> Fecha:  2018-08-13 con hash 14c31dd Realizado por lenin.vallejos
##### Se añade los ToolTipText de las frames
 
> Fecha:  2018-08-09 con hash 9840e28 Realizado por Eduardo Garcia
##### Pantalla de prescripcion grupo de botones con validacion y guardando en la variable de sesión
 
> Fecha:  2018-08-09 con hash 14638b1 Realizado por lenin.vallejos
##### Se da la funcinoaldiad a la pantalla de Acerca De
 
> Fecha:  2018-08-09 con hash 1afc1b3 Realizado por Eduardo Garcia
##### Merge branch '88-cierre-del-mes-atenciones-por-usuario' of https://gitlab.com/MSP_EC/rdacaa into 88-cierre-del-mes-atenciones-por-usuario
 
> Fecha:  2018-08-09 con hash 480e7bc Realizado por Eduardo Garcia
##### Se añade la funcionalidad para la seccion ACERCADE
 
> Fecha:  2018-08-09 con hash b27e8ee Realizado por Eduardo Garcia
##### Merge branch '88-cierre-del-mes-atenciones-por-usuario' of https://gitlab.com/MSP_EC/rdacaa into 88-cierre-del-mes-atenciones-por-usuario
 
> Fecha:  2018-08-09 con hash ce92058 Realizado por lenin.vallejos
##### Pantalla de prescripcion grupo de botones
 
> Fecha:  2018-08-09 con hash 0b313f1 Realizado por lenin.vallejos
##### Agregando prueba unitaria de IMC
 
> Fecha:  2018-08-09 con hash 6af7582 Realizado por Saulo Ismael Velasco Rivera
##### Pantalla de prescripcion grupo de botones
 
> Fecha:  2018-08-09 con hash e5e36aa Realizado por lenin.vallejos
##### Merge branch '88-cierre-del-mes-atenciones-por-usuario' of https://gitlab.com/MSP_EC/rdacaa into 88-cierre-del-mes-atenciones-por-usuario
 
> Fecha:  2018-08-09 con hash 9107460 Realizado por Eduardo Garcia
##### Se modifica el tipo de letra en los componentes de los frames
 
> Fecha:  2018-08-09 con hash 9ba1e14 Realizado por Eduardo Garcia
##### Organizando las dependencias del proyecto
 
> Fecha:  2018-08-08 con hash cef3c41 Realizado por Saulo Ismael Velasco Rivera
##### Información obstetrica cambios terminado
 
> Fecha:  2018-08-08 con hash 5bb7c54 Realizado por lenin.vallejos
##### Se modifica la pantalla de SIVAN, aplica ciriterios de validacion
 
> Fecha:  2018-08-08 con hash 4d7ca34 Realizado por Eduardo Garcia
##### Merge branch '88-cierre-del-mes-atenciones-por-usuario' of https://gitlab.com/MSP_EC/rdacaa into 88-cierre-del-mes-atenciones-por-usuario
 
> Fecha:  2018-08-08 con hash 99d1126 Realizado por lenin.vallejos
##### Información obstetrica
 
> Fecha:  2018-08-08 con hash 1b78464 Realizado por lenin.vallejos
##### XMerge branch '88-cierre-del-mes-atenciones-por-usuario' of https://gitlab.com/MSP_EC/rdacaa into 88-cierre-del-mes-atenciones-por-usuario
 
> Fecha:  2018-08-08 con hash c238ff4 Realizado por Saulo Ismael Velasco Rivera
##### Refactorizando para evitar referencias circulares
 
> Fecha:  2018-08-08 con hash 8c53e6c Realizado por Saulo Ismael Velasco Rivera
##### Se modifica pantalla SIVAN
 
> Fecha:  2018-08-08 con hash 7f5d083 Realizado por Eduardo Garcia
##### Merge branch '88-cierre-del-mes-atenciones-por-usuario' of https://gitlab.com/MSP_EC/rdacaa into 88-cierre-del-mes-atenciones-por-usuario
 
> Fecha:  2018-08-08 con hash 30e0f01 Realizado por Eduardo Garcia
##### Validación de información obstetrica
 
> Fecha:  2018-08-08 con hash 4b16605 Realizado por lenin.vallejos
##### Se crea validacion para Odontologia
 
> Fecha:  2018-08-07 con hash 3b4790e Realizado por Eduardo Garcia
##### Se modifica la pantalla de ACCESO se incluye nuevos iconos
 
> Fecha:  2018-08-07 con hash f4e5553 Realizado por Eduardo Garcia
##### Merge branch '88-cierre-del-mes-atenciones-por-usuario' of https://gitlab.com/MSP_EC/rdacaa into 88-cierre-del-mes-atenciones-por-usuario
 
> Fecha:  2018-08-06 con hash 957cd18 Realizado por Eduardo Garcia
##### Validaciones diagnosticos
 
> Fecha:  2018-08-06 con hash af1a2fe Realizado por jazael.faubla
##### Merge branch '88-cierre-del-mes-atenciones-por-usuario' of https://gitlab.com/MSP_EC/rdacaa into 88-cierre-del-mes-atenciones-por-usuario
 
> Fecha:  2018-08-03 con hash 72cea36 Realizado por Eduardo Garcia
##### Se añade la funcinoalidad de la pantalla de procedimeintos
 
> Fecha:  2018-08-03 con hash 52eb55c Realizado por Eduardo Garcia
##### Merge branch '88-cierre-del-mes-atenciones-por-usuario' of https://gitlab.com/MSP_EC/rdacaa into 88-cierre-del-mes-atenciones-por-usuario
 
> Fecha:  2018-08-03 con hash 90d01e2 Realizado por lenin.vallejos
##### Semanas de gestación
 
> Fecha:  2018-08-03 con hash ef0fb76 Realizado por lenin.vallejos
##### Quitando campo innecesario
 
> Fecha:  2018-08-03 con hash f3a8065 Realizado por Saulo Ismael Velasco Rivera
##### Merge branch '88-cierre-del-mes-atenciones-por-usuario' of https://gitlab.com/MSP_EC/rdacaa into 88-cierre-del-mes-atenciones-por-usuario
 
> Fecha:  2018-08-03 con hash a7e9208 Realizado por lenin.vallejos
##### Se corrige el catalog de riesgo obnstetrico y se aumenta semaforizacion
 
> Fecha:  2018-08-03 con hash 1bf755e Realizado por Eduardo Garcia
##### Validaciones de la pantalla VIH
 
> Fecha:  2018-08-03 con hash 5807ae1 Realizado por lenin.vallejos
##### Merge branch '88-cierre-del-mes-atenciones-por-usuario' of https://gitlab.com/MSP_EC/rdacaa into 88-cierre-del-mes-atenciones-por-usuario
 
> Fecha:  2018-08-03 con hash efd9674 Realizado por bolivar.murillo
##### Cambio de archivo final
 
> Fecha:  2018-08-03 con hash 7a7fbda Realizado por bolivar.murillo
##### Se carga logo horizontal en la pantalla de login
 
> Fecha:  2018-08-03 con hash edb05f3 Realizado por Eduardo Garcia
##### Merge branch '88-cierre-del-mes-atenciones-por-usuario' of https://gitlab.com/MSP_EC/rdacaa into 88-cierre-del-mes-atenciones-por-usuario
 
> Fecha:  2018-08-03 con hash 0dc5fa7 Realizado por Eduardo Garcia
##### Se implementa la funcionalidad de la pantalla de referencia
 
> Fecha:  2018-08-03 con hash 07de944 Realizado por Eduardo Garcia
##### Cambio de archivo
 
> Fecha:  2018-08-03 con hash 4233e24 Realizado por bolivar.murillo
##### Merge branch '88-cierre-del-mes-atenciones-por-usuario' of https://gitlab.com/MSP_EC/rdacaa into 88-cierre-del-mes-atenciones-por-usuario
 
> Fecha:  2018-08-03 con hash f098bd8 Realizado por Eduardo Garcia
##### Validaciones adicionales VIH
 
> Fecha:  2018-08-03 con hash f9aaec7 Realizado por lenin.vallejos
##### Merge branch '88-cierre-del-mes-atenciones-por-usuario' of https://gitlab.com/MSP_EC/rdacaa into 88-cierre-del-mes-atenciones-por-usuario
 
> Fecha:  2018-08-02 con hash 57729f3 Realizado por Eduardo Garcia
##### Se alñade funcinalidad a la tabla de referencia
 
> Fecha:  2018-08-02 con hash ce7a552 Realizado por Eduardo Garcia
##### Cambio archivo
 
> Fecha:  2018-08-02 con hash a89bf59 Realizado por bolivar.murillo
##### Agregando mensajes de recomendacion
 
> Fecha:  2018-08-02 con hash 34e6df4 Realizado por Saulo Ismael Velasco Rivera
##### Se solventa el error de conflicot COMMOCONTROLLPANEL
 
> Fecha:  2018-08-02 con hash 6dff2ec Realizado por Eduardo Garcia
##### Se añade Icono MSP Aprobado
 
> Fecha:  2018-08-02 con hash 79d8c67 Realizado por Eduardo Garcia
##### Añadir en la cabecera del label de información de la atención si es INTRAMURAL o EXTRAMURAL
 
> Fecha:  2018-08-02 con hash 415a8ae Realizado por lenin.vallejos
##### Nuevos campos y mapeo atencion prescripcion
 
> Fecha:  2018-08-02 con hash 0530610 Realizado por Saulo Ismael Velasco Rivera
##### Merge branch '88-cierre-del-mes-atenciones-por-usuario' of https://gitlab.com/MSP_EC/rdacaa into 88-cierre-del-mes-atenciones-por-usuario
 
> Fecha:  2018-08-02 con hash b5f079a Realizado por Saulo Ismael Velasco Rivera
##### Optimizacion lazy load
 
> Fecha:  2018-08-02 con hash e00b339 Realizado por Saulo Ismael Velasco Rivera
##### Ordenes medicas
 
> Fecha:  2018-08-01 con hash 9ccd16e Realizado por lenin.vallejos
##### Se añaden funcionalidades al formulñario de odontologia
 
> Fecha:  2018-08-01 con hash d087011 Realizado por Eduardo Garcia
##### Merge branch '88-cierre-del-mes-atenciones-por-usuario' of https://gitlab.com/MSP_EC/rdacaa into 88-cierre-del-mes-atenciones-por-usuario
 
> Fecha:  2018-08-01 con hash 6ac7cb2 Realizado por Eduardo Garcia
##### Ordenes medicas
 
> Fecha:  2018-08-01 con hash cf9d709 Realizado por lenin.vallejos
##### Ordenes medicas
 
> Fecha:  2018-08-01 con hash 58beae0 Realizado por lenin.vallejos
##### Se carga el catalogo para Subsistema R/C de lña pantalla de referencia
 
> Fecha:  2018-08-01 con hash ca4237d Realizado por Eduardo Garcia
##### Servicio de persistencia de atención total
 
> Fecha:  2018-08-01 con hash d59b2d7 Realizado por Saulo Ismael Velasco Rivera
##### Se carga Catalogo ORDENMEDICA
 
> Fecha:  2018-08-01 con hash 65e517f Realizado por Eduardo Garcia
##### Vacunacin terminada
 
> Fecha:  2018-08-01 con hash 9e1d17a Realizado por lenin.vallejos
##### Merge branch '88-cierre-del-mes-atenciones-por-usuario' of https://gitlab.com/MSP_EC/rdacaa into 88-cierre-del-mes-atenciones-por-usuario
 
> Fecha:  2018-07-31 con hash 1d62aa8 Realizado por Saulo Ismael Velasco Rivera
##### Exportando archivo y guardando la atención total
 
> Fecha:  2018-07-31 con hash b158dda Realizado por Saulo Ismael Velasco Rivera
##### Merge branch '88-cierre-del-mes-atenciones-por-usuario' of https://gitlab.com/MSP_EC/rdacaa into 88-cierre-del-mes-atenciones-por-usuario
 
> Fecha:  2018-07-31 con hash f618e61 Realizado por Eduardo Garcia
##### Se modifica la pantalla de procedimientos
 
> Fecha:  2018-07-31 con hash 78ff3c7 Realizado por Eduardo Garcia
##### Merge branch '88-cierre-del-mes-atenciones-por-usuario' of https://gitlab.com/MSP_EC/rdacaa into 88-cierre-del-mes-atenciones-por-usuario
 
> Fecha:  2018-07-31 con hash 1317ca4 Realizado por Saulo Ismael Velasco Rivera
##### Merge branch '88-cierre-del-mes-atenciones-por-usuario' of https://gitlab.com/MSP_EC/rdacaa into 88-cierre-del-mes-atenciones-por-usuario
 
> Fecha:  2018-07-31 con hash 2525001 Realizado por Saulo Ismael Velasco Rivera
##### Sivan terminado
 
> Fecha:  2018-07-31 con hash c7c9d92 Realizado por lenin.vallejos
##### Sivan terminado
 
> Fecha:  2018-07-31 con hash d3ad925 Realizado por lenin.vallejos
##### Merge branch '88-cierre-del-mes-atenciones-por-usuario' of https://gitlab.com/MSP_EC/rdacaa into 88-cierre-del-mes-atenciones-por-usuario
 
> Fecha:  2018-07-31 con hash f115df1 Realizado por Eduardo Garcia
##### Sivan terminado
 
> Fecha:  2018-07-31 con hash 93acb51 Realizado por lenin.vallejos
##### Se modifica el front end de algunos apratados
 
> Fecha:  2018-07-31 con hash 4e951bf Realizado por Eduardo Garcia
##### XMerge branch '88-cierre-del-mes-atenciones-por-usuario' of https://gitlab.com/MSP_EC/rdacaa into 88-cierre-del-mes-atenciones-por-usuario
 
> Fecha:  2018-07-31 con hash 6fb3de3 Realizado por Saulo Ismael Velasco Rivera
##### Utilitario para exportación
 
> Fecha:  2018-07-31 con hash b9fa396 Realizado por Saulo Ismael Velasco Rivera
##### Lista de paneles dinamicos wizardPage
 
> Fecha:  2018-07-31 con hash 60ee87b Realizado por lenin.vallejos
##### Lista de codigos CIE terminada
 
> Fecha:  2018-07-31 con hash 284042e Realizado por lenin.vallejos
##### Se añaden Wizard Pantallas Faltantes
 
> Fecha:  2018-07-31 con hash b902ec6 Realizado por Eduardo Garcia
##### Credenciales
 
> Fecha:  2018-07-31 con hash 89c1b66 Realizado por bolivar.murillo
##### Merge branch '88-cierre-del-mes-atenciones-por-usuario' of https://gitlab.com/MSP_EC/rdacaa into 88-cierre-del-mes-atenciones-por-usuario
 
> Fecha:  2018-07-30 con hash d457023 Realizado por Eduardo Garcia
##### Se añade Odontologia y Referecnaia al WIZARD
 
> Fecha:  2018-07-30 con hash fe9a2af Realizado por Eduardo Garcia
##### Merge branch '88-cierre-del-mes-atenciones-por-usuario' of https://gitlab.com/MSP_EC/rdacaa into 88-cierre-del-mes-atenciones-por-usuario
 
> Fecha:  2018-07-30 con hash 054ce1f Realizado por bolivar.murillo
##### Credenciales
 
> Fecha:  2018-07-30 con hash ee9fed4 Realizado por bolivar.murillo
##### Diagnosticos
 
> Fecha:  2018-07-30 con hash d452228 Realizado por lenin.vallejos
##### Encriptar
 
> Fecha:  2018-07-30 con hash a166d67 Realizado por bolivar.murillo
##### Subiendo modelo de tabla cie
 
> Fecha:  2018-07-30 con hash 0256b92 Realizado por Saulo Ismael Velasco Rivera
##### Encriptar
 
> Fecha:  2018-07-30 con hash 1fef43c Realizado por bolivar.murillo
##### Diagnoisticos seleccionados y agregar a lista
 
> Fecha:  2018-07-30 con hash 7f6df4a Realizado por lenin.vallejos
##### Encriptar
 
> Fecha:  2018-07-30 con hash 1fdb676 Realizado por bolivar.murillo
##### Merge branch '88-cierre-del-mes-atenciones-por-usuario' of https://gitlab.com/MSP_EC/rdacaa into 88-cierre-del-mes-atenciones-por-usuario
 
> Fecha:  2018-07-30 con hash 9978ecd Realizado por bolivar.murillo
##### Agregando campos adicionales y para cifrado
 
> Fecha:  2018-07-30 con hash 2963ded Realizado por Saulo Ismael Velasco Rivera
##### Se crea la vista de SIVAN
 
> Fecha:  2018-07-27 con hash c58b11f Realizado por Eduardo Garcia
##### Merge branch '88-cierre-del-mes-atenciones-por-usuario' of https://gitlab.com/MSP_EC/rdacaa into 88-cierre-del-mes-atenciones-por-usuario
 
> Fecha:  2018-07-27 con hash f83c70f Realizado por bolivar.murillo
##### Merge branch '88-cierre-del-mes-atenciones-por-usuario' of https://gitlab.com/MSP_EC/rdacaa into 88-cierre-del-mes-atenciones-por-usuario
 
> Fecha:  2018-07-27 con hash 73c64c9 Realizado por Saulo Ismael Velasco Rivera
##### Merge branch '88-cierre-del-mes-atenciones-por-usuario' of https://gitlab.com/MSP_EC/rdacaa into 88-cierre-del-mes-atenciones-por-usuario
 
> Fecha:  2018-07-27 con hash c90ec2e Realizado por bolivar.murillo
##### Export archivo utilitario
 
> Fecha:  2018-07-27 con hash 4648303 Realizado por Saulo Ismael Velasco Rivera
##### Merge branch '3-meta-registro-atencion-medica' into 'dev'

Finalizando Sprint 2 "[Meta] Registro Atención Médica"

See merge request MSP_EC/rdacaa!2 
> Fecha:  2018-07-27 con hash 8a1f1a0 Realizado por Saulo Velasco
##### Arreglando hash y equals
 
> Fecha:  2018-07-26 con hash d7e8e17 Realizado por Saulo Ismael Velasco Rivera
##### Merge branch '3-meta-registro-atencion-medica' of https://gitlab.com/MSP_EC/rdacaa into 3-meta-registro-atencion-medica
 
> Fecha:  2018-07-26 con hash c3fb891 Realizado por bolivar.murillo
##### Solucionando reglas sonarqube
 
> Fecha:  2018-07-26 con hash 5a933ea Realizado por Saulo Ismael Velasco Rivera
##### Merge branch '3-meta-registro-atencion-medica' of https://gitlab.com/MSP_EC/rdacaa into 3-meta-registro-atencion-medica
 
> Fecha:  2018-07-26 con hash baac715 Realizado por bolivar.murillo
##### Ciere de spring
 
> Fecha:  2018-07-26 con hash fd86d7a Realizado por bolivar.murillo
##### Pruebas unitarias de talla
 
> Fecha:  2018-07-26 con hash 458787a Realizado por Saulo Ismael Velasco Rivera
##### Carga dinamica de combos CIE
 
> Fecha:  2018-07-25 con hash 0864780 Realizado por jazael.faubla
##### Variable de inicialización de Escritorio principal
 
> Fecha:  2018-07-25 con hash b62899c Realizado por Saulo Ismael Velasco Rivera
##### Arreglando escritorio principal
 
> Fecha:  2018-07-25 con hash 476ff4a Realizado por Saulo Ismael Velasco Rivera
##### Arreglando conflictos
 
> Fecha:  2018-07-25 con hash 518af73 Realizado por Saulo Ismael Velasco Rivera
##### Validacion de cierre final
 
> Fecha:  2018-07-25 con hash 2399db0 Realizado por bolivar.murillo
##### XMerge branch '3-meta-registro-atencion-medica' of https://gitlab.com/MSP_EC/rdacaa into 3-meta-registro-atencion-medica
 
> Fecha:  2018-07-25 con hash 7b94d85 Realizado por Saulo Ismael Velasco Rivera
##### VerificarDos
 
> Fecha:  2018-07-25 con hash 5e42ed7 Realizado por bolivar.murillo
##### Arreglando dependencias y referencias cruzadas
 
> Fecha:  2018-07-25 con hash 025b7e7 Realizado por Saulo Ismael Velasco Rivera
##### Merge branch '3-meta-registro-atencion-medica' of https://gitlab.com/MSP_EC/rdacaa into 3-meta-registro-atencion-medica
 
> Fecha:  2018-07-25 con hash 4bc4a7a Realizado por bolivar.murillo
##### Verificar
 
> Fecha:  2018-07-25 con hash a7d4b4e Realizado por bolivar.murillo
##### Merge branch '3-meta-registro-atencion-medica' of https://gitlab.com/MSP_EC/rdacaa into 3-meta-registro-atencion-medica
 
> Fecha:  2018-07-25 con hash d050bab Realizado por jazael.faubla
##### Funcion validar cierre validacion Fallida
 
> Fecha:  2018-07-25 con hash 245e9bf Realizado por bolivar.murillo
##### Merge branch '3-meta-registro-atencion-medica' of https://gitlab.com/MSP_EC/rdacaa into 3-meta-registro-atencion-medica
 
> Fecha:  2018-07-25 con hash bc23c8a Realizado por jazael.faubla
##### Diagnosticos CIE
 
> Fecha:  2018-07-25 con hash 40665b3 Realizado por jazael.faubla
##### Funcion validar cierre validacion
 
> Fecha:  2018-07-25 con hash 5f7334a Realizado por bolivar.murillo
##### Unificando wizard de atencion médica en proyecto frontend
 
> Fecha:  2018-07-25 con hash a974f48 Realizado por Saulo Ismael Velasco Rivera
##### Funcion validar cierre
 
> Fecha:  2018-07-25 con hash c4d6472 Realizado por bolivar.murillo
##### Unificando proyecto frontend admisión en atención médica
 
> Fecha:  2018-07-25 con hash 4074cf7 Realizado por Saulo Ismael Velasco Rivera
##### Merge branch '3-meta-registro-atencion-medica' of https://gitlab.com/MSP_EC/rdacaa into 3-meta-registro-atencion-medica
 
> Fecha:  2018-07-25 con hash 99ae207 Realizado por Eduardo Garcia
##### Se modifica la pntalla de AdminUsuario para grabar a BDD
 
> Fecha:  2018-07-25 con hash a26c9d1 Realizado por Eduardo Garcia
##### pruebas Unitarias Admision
 
> Fecha:  2018-07-24 con hash 36fdade Realizado por bolivar.murillo
##### Pruebas unitarias de detalle catálogo
 
> Fecha:  2018-07-24 con hash a91492d Realizado por Saulo Ismael Velasco Rivera
##### Pruebas unitarias para base de datos
 
> Fecha:  2018-07-23 con hash ea8a1e5 Realizado por Saulo Ismael Velasco Rivera
##### Agregando mapeo de cierre de atenciones y pruebas unitarias de servicios
 
> Fecha:  2018-07-23 con hash 225cd61 Realizado por Saulo Ismael Velasco Rivera
##### Cargando la base de datos diferenciando el sistema operativo
 
> Fecha:  2018-07-23 con hash eb1ed52 Realizado por Saulo Ismael Velasco Rivera
##### Cambio sonarq
 
> Fecha:  2018-07-23 con hash 735064d Realizado por bolivar.murillo
##### Afinando mapeo de detalle catálogo
 
> Fecha:  2018-07-23 con hash acbafa9 Realizado por Saulo Ismael Velasco Rivera
##### Funcionalidad pantalla AdminUsuario
 
> Fecha:  2018-07-20 con hash 84d40de Realizado por Eduardo Garcia
##### Merge branch '3-meta-registro-atencion-medica' of https://gitlab.com/MSP_EC/rdacaa into 3-meta-registro-atencion-medica
 
> Fecha:  2018-07-19 con hash c858011 Realizado por jazael.faubla
##### Lista de examenes de VIH
 
> Fecha:  2018-07-19 con hash 2151b08 Realizado por jazael.faubla
##### Mapeo de formacion profesional
 
> Fecha:  2018-07-19 con hash 3323e7b Realizado por Saulo Ismael Velasco Rivera
##### Se carga la pantalla de Administracion de usuario
 
> Fecha:  2018-07-19 con hash 7e87546 Realizado por Eduardo Garcia
##### Merge branch '3-meta-registro-atencion-medica' of https://gitlab.com/MSP_EC/rdacaa into 3-meta-registro-atencion-medica
 
> Fecha:  2018-07-19 con hash 7e8fe1d Realizado por Eduardo Garcia
##### Modificando wizard factory para wizard dinámico
 
> Fecha:  2018-07-19 con hash 0866d82 Realizado por Saulo Ismael Velasco Rivera
##### Merge branch '3-meta-registro-atencion-medica' of https://gitlab.com/MSP_EC/rdacaa into 3-meta-registro-atencion-medica
 
> Fecha:  2018-07-19 con hash aa6e03f Realizado por Eduardo Garcia
##### Se carga la informacion en la pntalla de adminusuario
 
> Fecha:  2018-07-19 con hash 36e4c15 Realizado por Eduardo Garcia
##### Panel de opciones para la carga de especialidades medicas al momento de iniciar sesión, se carga la especialidad seleccionada en la variable de sesion, carga informativa en el header referente al usuario logueado
 
> Fecha:  2018-07-19 con hash e2796c1 Realizado por jazael.faubla
##### Panel de opciones para la carga de especialidades medicas al momento de iniciar sesión, se carga la especialidad seleccionada en la variable de sesion, carga informativa en el header referente al usuario logueado
 
> Fecha:  2018-07-19 con hash 5882a19 Realizado por jazael.faubla
##### Panel de opciones para la carga de especialidades medicas al momento de iniciar sesión, se carga la especialidad seleccionada en la variable de sesion, carga informativa en el header referente al usuario logueado
 
> Fecha:  2018-07-19 con hash 887020a Realizado por jazael.faubla
##### Panel de opciones para la carga de especialidades medicas al momento de iniciar sesión, se carga la especialidad seleccionada en la variable de sesion
 
> Fecha:  2018-07-18 con hash 9072fc7 Realizado por jazael.faubla
##### Merge branch '3-meta-registro-atencion-medica' of https://gitlab.com/MSP_EC/rdacaa into 3-meta-registro-atencion-medica
 
> Fecha:  2018-07-18 con hash c3bba62 Realizado por Saulo Ismael Velasco Rivera
##### arreglando validaciones dinámicas
 
> Fecha:  2018-07-18 con hash f45ee2a Realizado por Saulo Ismael Velasco Rivera
##### Se edita el color del label LBLPACIENTE para Vacunas y Diagnostico
 
> Fecha:  2018-07-18 con hash d594062 Realizado por Eduardo Garcia
##### Merge branch '3-meta-registro-atencion-medica' of https://gitlab.com/MSP_EC/rdacaa into 3-meta-registro-atencion-medica
 
> Fecha:  2018-07-18 con hash 02d32b2 Realizado por bolivar.murillo
##### Controles Admision
 
> Fecha:  2018-07-18 con hash ab748ef Realizado por bolivar.murillo
##### Login terminado, falta cargar especialidades
 
> Fecha:  2018-07-18 con hash 7a15464 Realizado por jazael.faubla
##### XMerge branch '3-meta-registro-atencion-medica' of https://gitlab.com/MSP_EC/rdacaa into 3-meta-registro-atencion-medica
 
> Fecha:  2018-07-18 con hash 5166a7a Realizado por Saulo Ismael Velasco Rivera
##### validando hb y hb corregido
 
> Fecha:  2018-07-18 con hash af57659 Realizado por Saulo Ismael Velasco Rivera
##### Login terminado, falta cargar especialidades
 
> Fecha:  2018-07-18 con hash 3e4d1fb Realizado por jazael.faubla
##### Se corrige JTABLE = NULL
 
> Fecha:  2018-07-18 con hash 3e11912 Realizado por Eduardo Garcia
##### Se cargan los catalogos de Vacunas y Diagnosticos
 
> Fecha:  2018-07-18 con hash eab19d0 Realizado por Eduardo Garcia
##### Merge branch '3-meta-registro-atencion-medica' of https://gitlab.com/MSP_EC/rdacaa into 3-meta-registro-atencion-medica
 
> Fecha:  2018-07-18 con hash 7514722 Realizado por Eduardo Garcia
##### Recuperar persona consulta
 
> Fecha:  2018-07-18 con hash 6ee8f17 Realizado por bolivar.murillo
##### Merge branch '3-meta-registro-atencion-medica' of https://gitlab.com/MSP_EC/rdacaa into 3-meta-registro-atencion-medica
 
> Fecha:  2018-07-18 con hash 31b962e Realizado por Eduardo Garcia
##### Controlador y servicio de inicio de Sesion
 
> Fecha:  2018-07-18 con hash dac555e Realizado por jazael.faubla
##### Merge branch '3-meta-registro-atencion-medica' of https://gitlab.com/MSP_EC/rdacaa into 3-meta-registro-atencion-medica
 
> Fecha:  2018-07-18 con hash eb46d7e Realizado por Eduardo Garcia
##### Carga credenciales n veces
 
> Fecha:  2018-07-18 con hash 8026ea0 Realizado por bolivar.murillo
##### Merge branch '3-meta-registro-atencion-medica' of https://gitlab.com/MSP_EC/rdacaa into 3-meta-registro-atencion-medica
 
> Fecha:  2018-07-17 con hash febf1ec Realizado por Eduardo Garcia
##### Credenciales cargadas total
 
> Fecha:  2018-07-17 con hash c1ab8f1 Realizado por bolivar.murillo
##### Modelo de la lista de grupos prioritarios refrescada, cada ves que cambias el sexo
 
> Fecha:  2018-07-17 con hash bd10267 Realizado por jazael.faubla
##### Merge branch '3-meta-registro-atencion-medica' of https://gitlab.com/MSP_EC/rdacaa into 3-meta-registro-atencion-medica
 
> Fecha:  2018-07-17 con hash 91b9a85 Realizado por Eduardo Garcia
##### Agregando idpras en usuario y persona
 
> Fecha:  2018-07-17 con hash 8c627bf Realizado por Saulo Ismael Velasco Rivera
##### Credenciales cargadas
 
> Fecha:  2018-07-17 con hash f49335e Realizado por bolivar.murillo
##### Cambiando de tipo de dato Long to Integer
 
> Fecha:  2018-07-17 con hash ac6fd03 Realizado por Saulo Ismael Velasco Rivera
##### Carga Catalogos en Diagnostico y Vacunas
 
> Fecha:  2018-07-17 con hash c13f24d Realizado por Eduardo Garcia
##### Añadiendo validacion por edad y grpo prioritario de información obstetrica
 
> Fecha:  2018-07-17 con hash 1111d46 Realizado por jazael.faubla
##### Credenciales
 
> Fecha:  2018-07-17 con hash e07e505 Realizado por bolivar.murillo
##### carga de credenciales
 
> Fecha:  2018-07-17 con hash c89d2ee Realizado por bolivar.murillo
##### Añadiendo grupo prioritario embarazada a la variable de sesión
 
> Fecha:  2018-07-17 con hash af17729 Realizado por jazael.faubla
##### Merge branch '3-meta-registro-atencion-medica' of https://gitlab.com/MSP_EC/rdacaa into 3-meta-registro-atencion-medica
 
> Fecha:  2018-07-17 con hash 91378e8 Realizado por Eduardo Garcia
##### Corrigiendo mapeo de tipo indentificacion representante
 
> Fecha:  2018-07-17 con hash 78e6b12 Realizado por Saulo Ismael Velasco Rivera
##### Información obstetrica
 
> Fecha:  2018-07-17 con hash 744c7e2 Realizado por jazael.faubla
##### Corrigiendo mapeo de tipo indentificacion representante
 
> Fecha:  2018-07-17 con hash c7685eb Realizado por Saulo Ismael Velasco Rivera
##### Merge branch '3-meta-registro-atencion-medica' of https://gitlab.com/MSP_EC/rdacaa into 3-meta-registro-atencion-medica
 
> Fecha:  2018-07-17 con hash 893e3f4 Realizado por Eduardo Garcia
##### Corrigiendo mapeo de lista de especialidades médicas
 
> Fecha:  2018-07-17 con hash db92e38 Realizado por Saulo Ismael Velasco Rivera
##### Se cargo la pantalla de Diagnosticos al wizard
 
> Fecha:  2018-07-17 con hash 279b039 Realizado por Eduardo Garcia
##### Agregando Entidad persona repository y servicio
 
> Fecha:  2018-07-16 con hash 3d89e68 Realizado por Saulo Ismael Velasco Rivera
##### Validacion de Indice de Masa Corporal y agregando entidad de especialidad
 
> Fecha:  2018-07-16 con hash 9906e40 Realizado por Saulo Ismael Velasco Rivera
##### Merge branch '3-meta-registro-atencion-medica' of https://gitlab.com/MSP_EC/rdacaa into 3-meta-registro-atencion-medica
 
> Fecha:  2018-07-16 con hash 0a548c4 Realizado por Eduardo Garcia
##### Se añade el valor de SEXO a la variable de secion para el LABELPACIENTE
 
> Fecha:  2018-07-16 con hash 86a4cf2 Realizado por Eduardo Garcia
##### Validacion de datos obstetricos cuando es mujer e intersexual
 
> Fecha:  2018-07-16 con hash 87341c4 Realizado por jazael.faubla
##### Variable de session dos
 
> Fecha:  2018-07-16 con hash 724a36a Realizado por bolivar.murillo
##### Merge branch '3-meta-registro-atencion-medica' of https://gitlab.com/MSP_EC/rdacaa into 3-meta-registro-atencion-medica
 
> Fecha:  2018-07-16 con hash 954ae9f Realizado por bolivar.murillo
##### Variable de session
 
> Fecha:  2018-07-16 con hash 678d50a Realizado por bolivar.murillo
##### Modelos para dieagnosticos de CIE
 
> Fecha:  2018-07-13 con hash dae9624 Realizado por Eduardo Garcia
##### Carga de archivo
 
> Fecha:  2018-07-13 con hash 00838ec Realizado por bolivar.murillo
##### Merge branch '3-meta-registro-atencion-medica' of https://gitlab.com/MSP_EC/rdacaa into 3-meta-registro-atencion-medica
 
> Fecha:  2018-07-13 con hash 9e5a45d Realizado por bolivar.murillo
##### Carga de modelos para la pantalla e Diagnosticos CIE
 
> Fecha:  2018-07-13 con hash 892c2e8 Realizado por Eduardo Garcia
##### Merge branch '3-meta-registro-atencion-medica' of https://gitlab.com/MSP_EC/rdacaa into 3-meta-registro-atencion-medica
 
> Fecha:  2018-07-13 con hash 11b653a Realizado por bolivar.murillo
##### Controles de VIH
 
> Fecha:  2018-07-13 con hash 34a1bc9 Realizado por jazael.faubla
##### Merge branch '3-meta-registro-atencion-medica' of https://gitlab.com/MSP_EC/rdacaa into 3-meta-registro-atencion-medica
 
> Fecha:  2018-07-13 con hash f3f4a08 Realizado por Saulo Ismael Velasco Rivera
##### Agregando validaciones en la pantalla para PESO y TALLA
 
> Fecha:  2018-07-13 con hash 8005ca2 Realizado por Saulo Ismael Velasco Rivera
##### Merge branch '3-meta-registro-atencion-medica' of https://gitlab.com/MSP_EC/rdacaa into 3-meta-registro-atencion-medica
 
> Fecha:  2018-07-13 con hash f874f9c Realizado por bolivar.murillo
##### Se añade los catalogos de VACUNAS
 
> Fecha:  2018-07-13 con hash 259fe69 Realizado por Eduardo Garcia
##### Merge branch '3-meta-registro-atencion-medica' of https://gitlab.com/MSP_EC/rdacaa into 3-meta-registro-atencion-medica
 
> Fecha:  2018-07-13 con hash 171613d Realizado por bolivar.murillo
##### Validaciones
 
> Fecha:  2018-07-13 con hash 80d2329 Realizado por bolivar.murillo
##### Validador para Datepicker
 
> Fecha:  2018-07-13 con hash 1472e6f Realizado por jazael.faubla
##### Control orientacion sexual
 
> Fecha:  2018-07-12 con hash 4527e62 Realizado por bolivar.murillo
##### Merge branch '3-meta-registro-atencion-medica' of https://gitlab.com/MSP_EC/rdacaa into 3-meta-registro-atencion-medica
 
> Fecha:  2018-07-12 con hash ca6acd6 Realizado por bolivar.murillo
##### Agregando validaciones para medidas antropométricas
 
> Fecha:  2018-07-12 con hash ea8e564 Realizado por Saulo Ismael Velasco Rivera
##### Grupos prioritarios
 
> Fecha:  2018-07-12 con hash fdcd854 Realizado por bolivar.murillo
##### Nabvar funcionando
 
> Fecha:  2018-07-12 con hash e2689f6 Realizado por jazael.faubla
##### Se genera un metodo generico en DETALLECATALOGOSERVICE para filtrar los detalles catalogos que sean padres
 
> Fecha:  2018-07-12 con hash c85c67c Realizado por Eduardo Garcia
##### Se borra VACUNASCONTROLLER CREA CONFLICTOS
 
> Fecha:  2018-07-12 con hash 11a2112 Realizado por Eduardo Garcia
##### VACUNAS CONTROLLER
 
> Fecha:  2018-07-12 con hash 68ef11e Realizado por Eduardo Garcia
##### Merge branch '3-meta-registro-atencion-medica' of https://gitlab.com/MSP_EC/rdacaa into 3-meta-registro-atencion-medica
 
> Fecha:  2018-07-12 con hash dfc67c2 Realizado por Eduardo Garcia
##### Vih Sida agregar objeto a la variable de sesion
 
> Fecha:  2018-07-12 con hash 811568d Realizado por jazael.faubla
##### Merge branch '3-meta-registro-atencion-medica' of https://gitlab.com/MSP_EC/rdacaa into 3-meta-registro-atencion-medica
 
> Fecha:  2018-07-12 con hash b00ac24 Realizado por Eduardo Garcia
##### Resolviendo conflicto con DETALLECATALOGOSERVICE
 
> Fecha:  2018-07-12 con hash fe46266 Realizado por Eduardo Garcia
##### Vih Sida agregar control combo para vias de transmision
 
> Fecha:  2018-07-12 con hash ab6b388 Realizado por jazael.faubla
##### Resolucion de conflicos DETALLECATALOG SERVICES
 
> Fecha:  2018-07-12 con hash 0be2740 Realizado por Eduardo Garcia
##### Se añade la funcionalidad del combo de Formacion Profesional
 
> Fecha:  2018-07-12 con hash b5408de Realizado por Eduardo Garcia
##### Pantalla de VIH
 
> Fecha:  2018-07-11 con hash ae34fd0 Realizado por jazael.faubla
##### Merge branch '3-meta-registro-atencion-medica' of https://gitlab.com/MSP_EC/rdacaa into 3-meta-registro-atencion-medica
 
> Fecha:  2018-07-11 con hash f8e3142 Realizado por jazael.faubla
##### Validacion de grupo de botones
 
> Fecha:  2018-07-11 con hash 44f1c0a Realizado por jazael.faubla
##### Agregando mapeo de tablas de validacion y alertas de signos vitales
 
> Fecha:  2018-07-11 con hash 5b3b773 Realizado por Saulo Ismael Velasco Rivera
##### Validacion de grupo de botones
 
> Fecha:  2018-07-11 con hash 8119e54 Realizado por jazael.faubla
##### Rediseñando el layout de admisión
 
> Fecha:  2018-07-11 con hash 3679a9b Realizado por Saulo Ismael Velasco Rivera
##### Funcion para generar las semanas de gestacion en informacion obstetrica
 
> Fecha:  2018-07-10 con hash d91de44 Realizado por jazael.faubla
##### Variable de session grupos prioritarios
 
> Fecha:  2018-07-10 con hash 330ec95 Realizado por bolivar.murillo
##### Merge branch '3-meta-registro-atencion-medica' of https://gitlab.com/MSP_EC/rdacaa into 3-meta-registro-atencion-medica
 
> Fecha:  2018-07-10 con hash b02afd4 Realizado por bolivar.murillo
##### Panel de VIH agregado al wizardContainer
 
> Fecha:  2018-07-10 con hash b270594 Realizado por jazael.faubla
##### Merge branch '3-meta-registro-atencion-medica' of https://gitlab.com/MSP_EC/rdacaa into 3-meta-registro-atencion-medica
 
> Fecha:  2018-07-10 con hash 69b8b51 Realizado por bolivar.murillo
##### Merge branch '3-meta-registro-atencion-medica' of https://gitlab.com/MSP_EC/rdacaa into 3-meta-registro-atencion-medica
 
> Fecha:  2018-07-10 con hash 067b90c Realizado por bolivar.murillo
##### Creando nuevos proyectos de seguridades y administracion de usuarios
 
> Fecha:  2018-07-10 con hash 981adf1 Realizado por Saulo Ismael Velasco Rivera
##### Borrar
 
> Fecha:  2018-07-10 con hash bfd707d Realizado por bolivar.murillo
##### Merge branch '3-meta-registro-atencion-medica' of https://gitlab.com/MSP_EC/rdacaa into 3-meta-registro-atencion-medica
 
> Fecha:  2018-07-10 con hash e46e54f Realizado por jazael.faubla
##### Conflicto resuelto
 
> Fecha:  2018-07-10 con hash c22e5f4 Realizado por jazael.faubla
##### Creando nuevos proyectos de seguridades y administracion de usuarios
 
> Fecha:  2018-07-10 con hash 7c92076 Realizado por Saulo Ismael Velasco Rivera
##### Poniendo como obligatorios los campos de peso y talla
 
> Fecha:  2018-07-10 con hash 469f669 Realizado por Saulo Ismael Velasco Rivera
##### Agregando mapeo de nuevo campo en Atencionobstetrica
 
> Fecha:  2018-07-10 con hash 1c0dcad Realizado por Saulo Ismael Velasco Rivera
##### Informacion obstetrica
 
> Fecha:  2018-07-10 con hash 7ecd3c1 Realizado por jazael.faubla
##### Segmentacion de pantallas
 
> Fecha:  2018-07-10 con hash d20fc8a Realizado por Eduardo Garcia
##### Se bloquean los componentes de atencion medica para que se activen con el WIZARD
 
> Fecha:  2018-07-09 con hash a340b7f Realizado por Eduardo Garcia
##### Merge branch '3-meta-registro-atencion-medica' of https://gitlab.com/MSP_EC/rdacaa into 3-meta-registro-atencion-medica
 
> Fecha:  2018-07-09 con hash e4e6809 Realizado por Eduardo Garcia
##### Se añaden iconos faltantes se espera la aprobacion de PO
 
> Fecha:  2018-07-09 con hash fff6736 Realizado por Eduardo Garcia
##### arreglo variable session
 
> Fecha:  2018-07-09 con hash 4caaecc Realizado por bolivar.murillo
##### Agregando funcionalidad al controlador de datos antropometricos
 
> Fecha:  2018-07-09 con hash 7c78981 Realizado por Saulo Ismael Velasco Rivera
##### Merge branch '3-meta-registro-atencion-medica' of https://gitlab.com/MSP_EC/rdacaa into 3-meta-registro-atencion-medica
 
> Fecha:  2018-07-09 con hash 000b2ef Realizado por jazael.faubla
##### Validacion grupos prioritarios
 
> Fecha:  2018-07-09 con hash 8151ec7 Realizado por bolivar.murillo
##### Calendario menstruacion
 
> Fecha:  2018-07-09 con hash abfc041 Realizado por jazael.faubla
##### Merge branch '3-meta-registro-atencion-medica' of https://gitlab.com/MSP_EC/rdacaa into 3-meta-registro-atencion-medica
 
> Fecha:  2018-07-09 con hash 8da92d9 Realizado por Eduardo Garcia
##### Se añden todos los iconos del menubar
 
> Fecha:  2018-07-09 con hash 02cc3f9 Realizado por Eduardo Garcia
##### Validacion pantalla datos obstetricos
 
> Fecha:  2018-07-09 con hash c46db79 Realizado por jazael.faubla
##### Validacion pantalla datos obstetricos
 
> Fecha:  2018-07-09 con hash 5fac9ec Realizado por jazael.faubla
##### Merge branch '3-meta-registro-atencion-medica' of https://gitlab.com/MSP_EC/rdacaa into 3-meta-registro-atencion-medica
 
> Fecha:  2018-07-09 con hash 660dccc Realizado por jazael.faubla
##### Validacion pantalla datos obstetricos
 
> Fecha:  2018-07-09 con hash b3075a7 Realizado por jazael.faubla
##### Se adicionan nuevos iconos para los aprtados de atencion medica
 
> Fecha:  2018-07-09 con hash 1aba7eb Realizado por Eduardo Garcia
##### Orientacion Sexual
 
> Fecha:  2018-07-09 con hash a3e336a Realizado por bolivar.murillo
##### Pantalla de información obstetrica
 
> Fecha:  2018-07-09 con hash 3bd07fb Realizado por jazael.faubla
##### Pantalla de información obstetrica
 
> Fecha:  2018-07-09 con hash 66a1c79 Realizado por jazael.faubla
##### Merge branch '3-meta-registro-atencion-medica' of https://gitlab.com/MSP_EC/rdacaa into 3-meta-registro-atencion-medica
 
> Fecha:  2018-07-09 con hash 1887e19 Realizado por Eduardo Garcia
##### Carga iconos de la barra de menu
 
> Fecha:  2018-07-09 con hash 5210caf Realizado por Eduardo Garcia
##### gruposPrioritario
 
> Fecha:  2018-07-09 con hash 548eeda Realizado por bolivar.murillo
##### Control telefonos
 
> Fecha:  2018-07-06 con hash 0fd004b Realizado por bolivar.murillo
##### Cambios logos, interfaz de acceso y panel principal
 
> Fecha:  2018-07-06 con hash cb33f41 Realizado por jazael.faubla
##### Cambios logos, interfaz de acceso y panel principal
 
> Fecha:  2018-07-06 con hash 9c292aa Realizado por jazael.faubla
##### Merge branch '3-meta-registro-atencion-medica' of https://gitlab.com/MSP_EC/rdacaa into 3-meta-registro-atencion-medica
 
> Fecha:  2018-07-06 con hash ab488e1 Realizado por Saulo Ismael Velasco Rivera
##### Diseño de la pantalla de Datos Antropométricos y agregando cabecera común
 
> Fecha:  2018-07-06 con hash aefe417 Realizado por Saulo Ismael Velasco Rivera
##### Logos
 
> Fecha:  2018-07-06 con hash 713594d Realizado por bolivar.murillo
##### Cambiando ids de catálogos de persona y agregando archivo para verificar el resultado de las consultas
 
> Fecha:  2018-07-06 con hash f692c32 Realizado por Saulo Ismael Velasco Rivera
##### Merge branch '3-meta-registro-atencion-medica' of https://gitlab.com/MSP_EC/rdacaa into 3-meta-registro-atencion-medica
 
> Fecha:  2018-07-06 con hash ededb64 Realizado por Saulo Ismael Velasco Rivera
##### Buscando tipos de persona por ID
 
> Fecha:  2018-07-06 con hash 579965e Realizado por Saulo Ismael Velasco Rivera
##### Se corrige conflicto para DETALLECATALOGOSERVICE.JAVA
 
> Fecha:  2018-07-06 con hash f34bcd1 Realizado por Eduardo Garcia
##### Se añade ComboBox para Lugares de Atencion (INTRA EXTRA MURAL)
 
> Fecha:  2018-07-06 con hash b9ecb9d Realizado por Eduardo Garcia
##### Quitando método no existente
 
> Fecha:  2018-07-06 con hash 9a0e734 Realizado por Saulo Ismael Velasco Rivera
##### Agregando anotaciones para autogenerar secuencias de tablas consultar los tipos de persona
 
> Fecha:  2018-07-06 con hash f379848 Realizado por Saulo Ismael Velasco Rivera
##### telefonosrE
 
> Fecha:  2018-07-05 con hash 2716519 Realizado por bolivar.murillo
##### telefonos
 
> Fecha:  2018-07-05 con hash ea7ba12 Realizado por bolivar.murillo
##### TipoTelefono
 
> Fecha:  2018-07-05 con hash 4007bed Realizado por bolivar.murillo
##### Merge branch '3-meta-registro-atencion-medica' of https://gitlab.com/MSP_EC/rdacaa into 3-meta-registro-atencion-medica
 
> Fecha:  2018-07-05 con hash 1cc2550 Realizado por bolivar.murillo
##### Pantallas flujo de atención médica adheridas al wizard
 
> Fecha:  2018-07-05 con hash 90de360 Realizado por Miguel Faubla
##### Merge branch '3-meta-registro-atencion-medica' of https://gitlab.com/MSP_EC/rdacaa into 3-meta-registro-atencion-medica
 
> Fecha:  2018-07-05 con hash d1b26e8 Realizado por bolivar.murillo
##### gp2
 
> Fecha:  2018-07-05 con hash b2fcfa5 Realizado por bolivar.murillo
##### Generando mapeo para campos adicioanles de la tabla persona y VIHSida
 
> Fecha:  2018-07-05 con hash b4f6f36 Realizado por Saulo Ismael Velasco Rivera
##### Generando mapeo para campos adicioanles de la tabla persona y VIHSida
 
> Fecha:  2018-07-05 con hash 1ca3217 Realizado por Saulo Ismael Velasco Rivera
##### Merge branch '3-meta-registro-atencion-medica' of https://gitlab.com/MSP_EC/rdacaa into 3-meta-registro-atencion-medica
 
> Fecha:  2018-07-05 con hash ee17bac Realizado por bolivar.murillo
##### gp
 
> Fecha:  2018-07-05 con hash 10ff9fc Realizado por bolivar.murillo
##### Generar sequencial de 17 digitos para no identificados
 
> Fecha:  2018-07-05 con hash 5748764 Realizado por Miguel Faubla
##### Agregando funcionalidad de autocompletar en los combos solo una vez al inicio para evitar errores de coversión de objeto a String
 
> Fecha:  2018-07-05 con hash 45f3836 Realizado por Saulo Ismael Velasco Rivera
##### Modificando filtro de grupos prioritarios para que usar el id del catalogo para sexo hombre, mujer
 
> Fecha:  2018-07-04 con hash 18cb2ae Realizado por Saulo Ismael Velasco Rivera
##### Modificando filtro de grupos prioritarios para que usar el id del catalogo para sexo hombre, mujer
 
> Fecha:  2018-07-04 con hash bd27d4e Realizado por Saulo Ismael Velasco Rivera
##### Generando y personalizando el ORM, DAO y Servicios de Acceso a Datos
 
> Fecha:  2018-07-04 con hash ea978a0 Realizado por Saulo Ismael Velasco Rivera
##### Merge branch '9-admision-datos-demograficos-del-paciente' into 'dev'

Finalizando Sprint "Admisión - Datos Demográficos Del Paciente"

See merge request MSP_EC/rdacaa!1 
> Fecha:  2018-07-04 con hash c6dca6c Realizado por Saulo Velasco
##### Filtrando nacionalidades étnicas
 
> Fecha:  2018-07-04 con hash 2de77c6 Realizado por Saulo Ismael Velasco Rivera
##### Combos
 
> Fecha:  2018-07-03 con hash 98c93a5 Realizado por bolivar.murillo
##### Seteando los combos sin selección 'null' para obligar al usuario a seleccionar un item
 
> Fecha:  2018-07-03 con hash 6a02e9d Realizado por Saulo Ismael Velasco Rivera
##### Actualizando el label del paciente actual en las pantallas subsecuentes
 
> Fecha:  2018-07-03 con hash eba8f6c Realizado por Saulo Ismael Velasco Rivera
##### Actualizando el label del paciente actual en las pantallas subsecuentes
 
> Fecha:  2018-07-03 con hash 7f51bd9 Realizado por Saulo Ismael Velasco Rivera
##### VariableSession
 
> Fecha:  2018-07-03 con hash 7ea7569 Realizado por bolivar.murillo
##### Agregando función para que cuando se finalice o cancele el registro de los datos, se retorne a la pantalla inicial
 
> Fecha:  2018-07-03 con hash d85e8c8 Realizado por Saulo Ismael Velasco Rivera
##### integracion GP
 
> Fecha:  2018-07-02 con hash dc0b9e6 Realizado por bolivar.murillo
##### Cambiando los diseños de internal frames a paneles
 
> Fecha:  2018-07-02 con hash 6298d32 Realizado por Saulo Ismael Velasco Rivera
##### Integrando el código fuente de wizard, admision y grupos prioritarios
 
> Fecha:  2018-07-02 con hash 3b02dd5 Realizado por Saulo Ismael Velasco Rivera
##### Validaciones completas
 
> Fecha:  2018-07-02 con hash 9023ab9 Realizado por bolivar.murillo
##### Merge branch '9-admision-datos-demograficos-del-paciente' of https://gitlab.com/MSP_EC/rdacaa into 9-admision-datos-demograficos-del-paciente
 
> Fecha:  2018-07-02 con hash 1ea6812 Realizado por Saulo Ismael Velasco Rivera
##### Agregando campos de identificación del representante
 
> Fecha:  2018-07-02 con hash 831b546 Realizado por Saulo Ismael Velasco Rivera
##### Validacion de pantalla admision
 
> Fecha:  2018-06-29 con hash b03099c Realizado por bolivar.murillo
##### Validacion fecha
 
> Fecha:  2018-06-29 con hash ae3f3c1 Realizado por bolivar.murillo
##### Cambios Controles
 
> Fecha:  2018-06-28 con hash fd0af40 Realizado por bolivar.murillo
##### Cambios Saulo
 
> Fecha:  2018-06-28 con hash a0c0419 Realizado por bolivar.murillo
##### Obteniendo grupos prioritarios filtrados por edad y sexo
 
> Fecha:  2018-06-28 con hash 8ac4729 Realizado por Saulo Ismael Velasco Rivera
##### Agregando Validación de Grupos Prioritarios
 
> Fecha:  2018-06-28 con hash 36712a4 Realizado por Saulo Ismael Velasco Rivera
##### Agregando entidades generadas desde otra base de datos para mayor detalle
 
> Fecha:  2018-06-28 con hash 27b8228 Realizado por Saulo Ismael Velasco Rivera
##### Carga variable persona Session
 
> Fecha:  2018-06-28 con hash 7f6e028 Realizado por bolivar.murillo
##### Carga variable persona
 
> Fecha:  2018-06-28 con hash 232e9b8 Realizado por bolivar.murillo
##### validaciones filtros
 
> Fecha:  2018-06-27 con hash a6ec62a Realizado por bolivar.murillo
##### Arreglando dependencia
 
> Fecha:  2018-06-27 con hash 04477c7 Realizado por Saulo Ismael Velasco Rivera
##### validaciones solo numero
 
> Fecha:  2018-06-27 con hash 9716f47 Realizado por bolivar.murillo
##### validaciones primer apellido
 
> Fecha:  2018-06-27 con hash 4baf609 Realizado por bolivar.murillo
##### validaciones primer nombre
 
> Fecha:  2018-06-27 con hash efe4031 Realizado por bolivar.murillo
##### Validaciones Identificacion swingx-utilitario
 
> Fecha:  2018-06-26 con hash 5c52bc1 Realizado por bolivar.murillo
##### Validaciones Identificacion swingx
 
> Fecha:  2018-06-26 con hash c4d5020 Realizado por bolivar.murillo
##### Validaciones Identificacion
 
> Fecha:  2018-06-26 con hash 6d61e82 Realizado por bolivar.murillo
##### Agregando clase inicial de validación de cédula
 
> Fecha:  2018-06-26 con hash 1f3061b Realizado por Saulo Ismael Velasco Rivera
##### Modificando interfaz de validación
 
> Fecha:  2018-06-26 con hash bf5aa27 Realizado por Saulo Ismael Velasco Rivera
##### cambio model
 
> Fecha:  2018-06-25 con hash aa51791 Realizado por bolivar.murillo
##### Carga combos
 
> Fecha:  2018-06-25 con hash 646039e Realizado por bolivar.murillo
##### Añadiendo modulo para validaciones y cargando combos dinámicamente
 
> Fecha:  2018-06-25 con hash 51034c4 Realizado por Saulo Ismael Velasco Rivera
##### Merge branch '9-admision-datos-demograficos-del-paciente' of https://gitlab.com/MSP_EC/rdacaa into 9-admision-datos-demograficos-del-paciente
 
> Fecha:  2018-06-25 con hash 781b8c6 Realizado por Saulo Ismael Velasco Rivera
##### Añadiendo nuevo módulo de validación
 
> Fecha:  2018-06-25 con hash 5fe6adf Realizado por Saulo Ismael Velasco Rivera
##### Merge branch '9-admision-datos-demograficos-del-paciente' of https://gitlab.com/MSP_EC/rdacaa into 9-admision-datos-demograficos-del-paciente
 
> Fecha:  2018-06-22 con hash a1b2aff Realizado por bolivar.murillo
##### Catalogos Admision
 
> Fecha:  2018-06-22 con hash be051ea Realizado por bolivar.murillo
##### Agregando métodos a los servicios de datos de Zona, Distrito, Circuito, AtencionGrupoPrioritario
 
> Fecha:  2018-06-22 con hash 6ee6e77 Realizado por Saulo Ismael Velasco Rivera
##### Ordenando los catálogos ASCENDENTEMENTE por descripción
 
> Fecha:  2018-06-22 con hash bd75b46 Realizado por Saulo Ismael Velasco Rivera
##### Agregando métodos para la consulta a los catálogos
 
> Fecha:  2018-06-22 con hash 192b8f1 Realizado por Saulo Ismael Velasco Rivera
##### Agregando mapeo para nuevas tablas de la base de RDACAA
 
> Fecha:  2018-06-21 con hash c62d060 Realizado por Saulo Ismael Velasco Rivera
##### Proyecto base de RDACAA, pueden existir cambios en la estructura posteriormente
 
> Fecha:  2018-06-21 con hash 6568a6a Realizado por Saulo Ismael Velasco Rivera
##### Update README.md 
> Fecha:  2018-06-13 con hash 87abed2 Realizado por Jonathan Finlay
##### Add README.md 
> Fecha:  2018-06-13 con hash 0955f37 Realizado por Jonathan Finlay
##### Add LICENSE 
> Fecha:  2018-06-13 con hash 4a32dea Realizado por Jonathan Finlay
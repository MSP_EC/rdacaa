package ec.gob.msp.rdacaa.business.validation.rule.datosantropometricos.test;

import java.util.Optional;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import com.deliveredtechnologies.rulebook.Fact;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaRawRow;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaRawRowResult;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.malla.RowItem;
import ec.gob.msp.rdacaa.business.validation.rule.atencion.datosantropometricos.categoriapesoparatalla.CategoriaPesoParaTallaRuleGroupExecutor;
import ec.gob.msp.rdacaa.business.validation.rule.atencion.datosantropometricos.categoriatallaparaedad.CategoriaTallaParaEdadRuleGroupExecutor;
import ec.gob.msp.rdacaa.business.validation.rule.atencion.datosantropometricos.peso.PesoRuleGroupExecutor;
import ec.gob.msp.rdacaa.business.validation.rule.atencion.datosantropometricos.puntajezpesoparatalla.PuntajeZPesoParaTallaRuleGroupExecutor;
import ec.gob.msp.rdacaa.business.validation.rule.atencion.datosantropometricos.puntajeztallaparaedad.PuntajeZTallaParaEdadRuleGroupExecutor;
import ec.gob.msp.rdacaa.business.validation.rule.atencion.datosantropometricos.talla.TallaRuleGroupExecutor;
import ec.gob.msp.rdacaa.business.validation.rule.atencion.datosantropometricos.tallacorregida.TallaCorregidaRuleGroupExecutor;
import ec.gob.msp.rdacaa.business.validation.rule.atencion.datosantropometricos.tallatipotoma.TallaTipoTomaRuleGroupExecutor;
import ec.gob.msp.rdacaa.test.ValidationTestContext;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = ValidationTestContext.class)
public class TallaCorregidaCalculateForAgeRuleTest {

	private static final String DE_PIE = "0";
	private static final String ACOSTADO = "1";

	@Autowired
	private TallaRuleGroupExecutor tallaRuleGroupExecutor;

	@Autowired
	private TallaCorregidaRuleGroupExecutor tallaCorregidaRuleGroupExecutor;

	@Autowired
	private TallaTipoTomaRuleGroupExecutor tallaTipoTomaRuleGroupExecutor;
	
	@Autowired
	private PuntajeZTallaParaEdadRuleGroupExecutor puntajeZTallaParaEdadRuleGroupExecutor;
	
	@Autowired
	private CategoriaTallaParaEdadRuleGroupExecutor categoriaTallaParaEdadRuleGroupExecutor;
	
	@Autowired
	private PesoRuleGroupExecutor pesoRuleGroupExecutor;
	
	@Autowired
	private PuntajeZPesoParaTallaRuleGroupExecutor puntajeZPesoParaTallaRuleGroupExecutor;
	
	@Autowired
	private CategoriaPesoParaTallaRuleGroupExecutor categoriaPesoParaTallaRuleGroupExecutor;

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Test
	public void whenFechasPesoTallaCorregidaHasErrors_thenError() {
		RdacaaRawRowResult resultTotal = new RdacaaRawRowResult();
		RdacaaRawRow input = new RdacaaRawRow();
		// ERROR TALLA
		RowItem talla = new RowItem();
		talla.setItemValue("asasa");
		talla.setExcludedFromValidation(false);
		talla.setMandatory(false);
		input.addField(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_TALLA, talla);

		Optional<RdacaaRawRowResult> sa2 = tallaRuleGroupExecutor.execute(input);

		resultTotal.putAll(sa2.get());

		Fact resultTotalFact2 = new Fact("rdacaa_row_result", resultTotal);

		// Tipo Toma Talla
		RowItem tipoTomaTalla = new RowItem();
		tipoTomaTalla.setItemValue(ACOSTADO);
		tipoTomaTalla.setExcludedFromValidation(false);
		tipoTomaTalla.setMandatory(false);
		input.addField(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_CODIGO_TIPO_TOMA_MEDIDA_TALLA, tipoTomaTalla);

		Optional<RdacaaRawRowResult> sa3 = tallaTipoTomaRuleGroupExecutor.execute(input, resultTotalFact2);

		resultTotal.putAll(sa3.get());

		Fact resultTotalFact3 = new Fact("rdacaa_row_result", resultTotal);

		RowItem tallaCorregida = new RowItem();
		tallaCorregida.setItemValue("999990000066666");
		tallaCorregida.setExcludedFromValidation(false);
		tallaCorregida.setMandatory(false);
		input.addField(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_TALLA_CORREGIDA, tallaCorregida);

		Optional<RdacaaRawRowResult> sa4 = tallaCorregidaRuleGroupExecutor.execute(input, resultTotalFact3);

		sa4.ifPresent(r -> {
			System.out.println(r.toString());
		});
		
		resultTotal.putAll(sa4.get());

		Fact resultTotalFact4 = new Fact("rdacaa_row_result", resultTotal);
		
		//PZ Talla para edad
		RowItem pzTallaEdad = new RowItem();
		pzTallaEdad.setItemValue("999990000066666");
		pzTallaEdad.setExcludedFromValidation(false);
		pzTallaEdad.setMandatory(false);
		input.addField(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_PUNTAJE_Z_TALLA_PARA_EDAD, pzTallaEdad);

		Optional<RdacaaRawRowResult> sa5 = puntajeZTallaParaEdadRuleGroupExecutor.execute(input, resultTotalFact4);

		sa5.ifPresent(r -> {
			System.out.println(r.toString());
		});
		
		resultTotal.putAll(sa5.get());

		Fact resultTotalFact5 = new Fact("rdacaa_row_result", resultTotal);
		
		//Cat PZ Talla para edad
		RowItem catPzTallaEdad = new RowItem();
		catPzTallaEdad.setItemValue("999990000066666");
		catPzTallaEdad.setExcludedFromValidation(false);
		catPzTallaEdad.setMandatory(false);
		input.addField(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_CATEGORIA_TALLA_PARA_EDAD, catPzTallaEdad);

		Optional<RdacaaRawRowResult> sa6 = categoriaTallaParaEdadRuleGroupExecutor.execute(input, resultTotalFact5);

		sa6.ifPresent(r -> {
			System.out.println(r.toString());
		});
		
		resultTotal.putAll(sa6.get());

		Fact resultTotalFact6 = new Fact("rdacaa_row_result", resultTotal);
		
		// PESO
		RowItem peso = new RowItem();
		peso.setItemValue("60");
		peso.setExcludedFromValidation(false);
		peso.setMandatory(false);
		input.addField(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_PESO, peso);

		Optional<RdacaaRawRowResult> sa7 = pesoRuleGroupExecutor.execute(input, resultTotalFact6);
		sa7.ifPresent(r -> {
			System.out.println(r.toString());
		});
		
		resultTotal.putAll(sa7.get());

		Fact resultTotalFact7 = new Fact("rdacaa_row_result", resultTotal);
		
		// PZ peso para talla
		
		RowItem pzPesoTalla = new RowItem();
		pzPesoTalla.setItemValue("999990000066666");
		pzPesoTalla.setExcludedFromValidation(false);
		pzPesoTalla.setMandatory(false);
		input.addField(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_PUNTAJE_Z_PESO_PARA_TALLA, pzPesoTalla);

		Optional<RdacaaRawRowResult> sa8 = puntajeZPesoParaTallaRuleGroupExecutor.execute(input, resultTotalFact7);
		sa8.ifPresent(r -> {
			System.out.println(r.toString());
		});
		
		resultTotal.putAll(sa8.get());

		Fact resultTotalFact8 = new Fact("rdacaa_row_result", resultTotal);
	
		// Cat PZ peso para talla
		RowItem catPzPesoTalla = new RowItem();
		catPzPesoTalla.setItemValue("999990000066666");
		catPzPesoTalla.setExcludedFromValidation(false);
		catPzPesoTalla.setMandatory(false);
		input.addField(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_CATEGORIA_PESO_PARA_TALLA, catPzPesoTalla);

		Optional<RdacaaRawRowResult> sa9 = categoriaPesoParaTallaRuleGroupExecutor.execute(input, resultTotalFact8);
		sa9.ifPresent(r -> {
			System.out.println(r.toString());
		});

		Assert.assertTrue(
				"Se espera CODIGO_ERROR_DAT_ANT_TALLA_CORREGIDA_FECHAS_FORMATO_INCORRECTO , "
						+ "CODIGO_ERROR_DAT_ANT_TALLA_CORREGIDA_TIPO_TOMA_TIENE_ERRORES, "
						+ "CODIGO_ERROR_DAT_ANT_TALLA_CORREGIDA_TIPO_TOMA_TIENE_ERRORES",
				sa4.get().get(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_TALLA_CORREGIDA).hasValidationResultByCode(
						ValidationResultCatalogConstants.CODIGO_ERROR_DAT_ANT_TALLA_CORREGIDA_FECHAS_FORMATO_INCORRECTO)
						.isPresent()
						&& sa4.get().get(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_TALLA_CORREGIDA)
								.hasValidationResultByCode(
										ValidationResultCatalogConstants.CODIGO_ERROR_DAT_ANT_TALLA_CORREGIDA_TIPO_TOMA_TIENE_ERRORES)
								.isPresent()
						&& sa4.get().get(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_TALLA_CORREGIDA)
								.hasValidationResultByCode(
										ValidationResultCatalogConstants.CODIGO_ERROR_DAT_ANT_TALLA_CORREGIDA_TIPO_TOMA_TIENE_ERRORES)
								.isPresent());
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Test
	public void whenEdadMayorIgual050100_thenNoError() {
		RdacaaRawRowResult resultTotal = new RdacaaRawRowResult();
		RdacaaRawRow input = new RdacaaRawRow();
		// PESO
		RowItem sexo = new RowItem();
		sexo.setItemValue("17");// mujer
		sexo.setExcludedFromValidation(false);
		input.addField(RdacaaVariableKeyCatalog.PERSONA_CODIGO_SEXO, sexo);
		RowItem grupoPrioritarioEmbarazada = new RowItem();
		grupoPrioritarioEmbarazada.setItemValue("634");
		grupoPrioritarioEmbarazada.setExcludedFromValidation(false);
		input.addField(RdacaaVariableKeyCatalog.GRUPO_PRIORITARIO_1, grupoPrioritarioEmbarazada);
		RowItem fechaNac = new RowItem();
		fechaNac.setItemValue("2000-05-06");
		fechaNac.setExcludedFromValidation(false);
		input.addField(RdacaaVariableKeyCatalog.PERSONA_FECHA_NACIMIENTO, fechaNac);
		RowItem fechaAten = new RowItem();
		fechaAten.setItemValue("2018-11-23");
		fechaAten.setExcludedFromValidation(false);
		input.addField(RdacaaVariableKeyCatalog.ATENCION_FECHA, fechaAten);

		// TALLA
		RowItem talla = new RowItem();
		talla.setItemValue("150");
		talla.setExcludedFromValidation(false);
		talla.setMandatory(false);
		input.addField(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_TALLA, talla);

		Optional<RdacaaRawRowResult> sa2 = tallaRuleGroupExecutor.execute(input);

		sa2.ifPresent(r -> {
			System.out.println(r.toString());
		});

		resultTotal.putAll(sa2.get());

		Fact resultTotalFact2 = new Fact("rdacaa_row_result", resultTotal);

		// Tipo Toma Talla
		RowItem tipoTomaTalla = new RowItem();
		tipoTomaTalla.setItemValue(DE_PIE);
		tipoTomaTalla.setExcludedFromValidation(false);
		tipoTomaTalla.setMandatory(false);
		input.addField(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_CODIGO_TIPO_TOMA_MEDIDA_TALLA, tipoTomaTalla);

		Optional<RdacaaRawRowResult> sa3 = tallaTipoTomaRuleGroupExecutor.execute(input, resultTotalFact2);

		sa3.ifPresent(r -> {
			System.out.println(r.toString());
		});

		resultTotal.putAll(sa3.get());

		Fact resultTotalFact3 = new Fact("rdacaa_row_result", resultTotal);

		RowItem tallaCorregida = new RowItem();
		tallaCorregida.setItemValue("999990000066666");
		tallaCorregida.setExcludedFromValidation(false);
		tallaCorregida.setMandatory(false);
		input.addField(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_TALLA_CORREGIDA, tallaCorregida);

		Optional<RdacaaRawRowResult> sa4 = tallaCorregidaRuleGroupExecutor.execute(input, resultTotalFact3);

		sa4.ifPresent(r -> {
			System.out.println(r.toString());
		});
		
		resultTotal.putAll(sa4.get());

		Fact resultTotalFact4 = new Fact("rdacaa_row_result", resultTotal);
		
		//PZ Talla para edad
		RowItem pzTallaEdad = new RowItem();
		pzTallaEdad.setItemValue("999990000066666");
		pzTallaEdad.setExcludedFromValidation(false);
		pzTallaEdad.setMandatory(false);
		input.addField(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_PUNTAJE_Z_TALLA_PARA_EDAD, pzTallaEdad);

		Optional<RdacaaRawRowResult> sa5 = puntajeZTallaParaEdadRuleGroupExecutor.execute(input, resultTotalFact4);

		sa5.ifPresent(r -> {
			System.out.println(r.toString());
		});
		
		resultTotal.putAll(sa5.get());

		Fact resultTotalFact5 = new Fact("rdacaa_row_result", resultTotal);
		
		//Cat PZ Talla para edad
		RowItem catPzTallaEdad = new RowItem();
		catPzTallaEdad.setItemValue("999990000066666");
		catPzTallaEdad.setExcludedFromValidation(false);
		catPzTallaEdad.setMandatory(false);
		input.addField(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_CATEGORIA_TALLA_PARA_EDAD, catPzTallaEdad);

		Optional<RdacaaRawRowResult> sa6 = categoriaTallaParaEdadRuleGroupExecutor.execute(input, resultTotalFact5);

		sa6.ifPresent(r -> {
			System.out.println(r.toString());
		});
		
		resultTotal.putAll(sa6.get());

		Fact resultTotalFact6 = new Fact("rdacaa_row_result", resultTotal);
		
		// PESO
		RowItem peso = new RowItem();
		peso.setItemValue("60");
		peso.setExcludedFromValidation(false);
		peso.setMandatory(false);
		input.addField(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_PESO, peso);

		Optional<RdacaaRawRowResult> sa7 = pesoRuleGroupExecutor.execute(input, resultTotalFact6);
		sa7.ifPresent(r -> {
			System.out.println(r.toString());
		});
		
		resultTotal.putAll(sa7.get());

		Fact resultTotalFact7 = new Fact("rdacaa_row_result", resultTotal);
		
		// PZ peso para talla
		
		RowItem pzPesoTalla = new RowItem();
		pzPesoTalla.setItemValue("999990000066666");
		pzPesoTalla.setExcludedFromValidation(false);
		pzPesoTalla.setMandatory(false);
		input.addField(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_PUNTAJE_Z_PESO_PARA_TALLA, pzPesoTalla);

		Optional<RdacaaRawRowResult> sa8 = puntajeZPesoParaTallaRuleGroupExecutor.execute(input, resultTotalFact7);
		sa8.ifPresent(r -> {
			System.out.println(r.toString());
		});
		
		resultTotal.putAll(sa8.get());

		Fact resultTotalFact8 = new Fact("rdacaa_row_result", resultTotal);
	
		// Cat PZ peso para talla
		RowItem catPzPesoTalla = new RowItem();
		catPzPesoTalla.setItemValue("999990000066666");
		catPzPesoTalla.setExcludedFromValidation(false);
		catPzPesoTalla.setMandatory(false);
		input.addField(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_CATEGORIA_PESO_PARA_TALLA, catPzPesoTalla);

		Optional<RdacaaRawRowResult> sa9 = categoriaPesoParaTallaRuleGroupExecutor.execute(input, resultTotalFact8);
		sa9.ifPresent(r -> {
			System.out.println(r.toString());
		});

		Assert.assertTrue("No se esperan errores",
				!sa4.get().get(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_TALLA_CORREGIDA).hasErrors());

	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Test
	public void whenEdadMayorIgual20001YMenor50100_DePie_thenNoError() {
		RdacaaRawRowResult resultTotal = new RdacaaRawRowResult();
		RdacaaRawRow input = new RdacaaRawRow();
		// PESO
		RowItem sexo = new RowItem();
		sexo.setItemValue("17");// mujer
		sexo.setExcludedFromValidation(false);
		input.addField(RdacaaVariableKeyCatalog.PERSONA_CODIGO_SEXO, sexo);
		RowItem grupoPrioritarioEmbarazada = new RowItem();
		grupoPrioritarioEmbarazada.setItemValue("634");
		grupoPrioritarioEmbarazada.setExcludedFromValidation(false);
		input.addField(RdacaaVariableKeyCatalog.GRUPO_PRIORITARIO_1, grupoPrioritarioEmbarazada);
		RowItem fechaNac = new RowItem();
		fechaNac.setItemValue("2014-11-01");
		fechaNac.setExcludedFromValidation(false);
		input.addField(RdacaaVariableKeyCatalog.PERSONA_FECHA_NACIMIENTO, fechaNac);
		RowItem fechaAten = new RowItem();
		fechaAten.setItemValue("2018-11-01");
		fechaAten.setExcludedFromValidation(false);
		input.addField(RdacaaVariableKeyCatalog.ATENCION_FECHA, fechaAten);

		// TALLA
		RowItem talla = new RowItem();
		talla.setItemValue("100");
		talla.setExcludedFromValidation(false);
		talla.setMandatory(false);
		input.addField(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_TALLA, talla);

		Optional<RdacaaRawRowResult> sa2 = tallaRuleGroupExecutor.execute(input);

		sa2.ifPresent(r -> {
			System.out.println(r.toString());
		});

		resultTotal.putAll(sa2.get());

		Fact resultTotalFact2 = new Fact("rdacaa_row_result", resultTotal);

		// Tipo Toma Talla
		RowItem tipoTomaTalla = new RowItem();
		tipoTomaTalla.setItemValue(DE_PIE);
		tipoTomaTalla.setExcludedFromValidation(false);
		tipoTomaTalla.setMandatory(false);
		input.addField(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_CODIGO_TIPO_TOMA_MEDIDA_TALLA, tipoTomaTalla);

		Optional<RdacaaRawRowResult> sa3 = tallaTipoTomaRuleGroupExecutor.execute(input, resultTotalFact2);

		sa3.ifPresent(r -> {
			System.out.println(r.toString());
		});

		resultTotal.putAll(sa3.get());

		Fact resultTotalFact3 = new Fact("rdacaa_row_result", resultTotal);

		RowItem tallaCorregida = new RowItem();
		tallaCorregida.setItemValue("999990000066666");
		tallaCorregida.setExcludedFromValidation(false);
		tallaCorregida.setMandatory(false);
		input.addField(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_TALLA_CORREGIDA, tallaCorregida);

		Optional<RdacaaRawRowResult> sa4 = tallaCorregidaRuleGroupExecutor.execute(input, resultTotalFact3);

		sa4.ifPresent(r -> {
			System.out.println(r.toString());
		});
		
		resultTotal.putAll(sa4.get());
		
		Fact resultTotalFact4 = new Fact("rdacaa_row_result", resultTotal);
		
		//PZ Talla para edad
		RowItem pzTallaEdad = new RowItem();
		pzTallaEdad.setItemValue("999990000066666");
		pzTallaEdad.setExcludedFromValidation(false);
		pzTallaEdad.setMandatory(false);
		input.addField(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_PUNTAJE_Z_TALLA_PARA_EDAD, pzTallaEdad);

		Optional<RdacaaRawRowResult> sa5 = puntajeZTallaParaEdadRuleGroupExecutor.execute(input, resultTotalFact4);

		sa5.ifPresent(r -> {
			System.out.println(r.toString());
		});
		
		resultTotal.putAll(sa5.get());

		Fact resultTotalFact5 = new Fact("rdacaa_row_result", resultTotal);
		
		//Cat PZ Talla para edad
		RowItem catPzTallaEdad = new RowItem();
		catPzTallaEdad.setItemValue("999990000066666");
		catPzTallaEdad.setExcludedFromValidation(false);
		catPzTallaEdad.setMandatory(false);
		input.addField(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_CATEGORIA_TALLA_PARA_EDAD, catPzTallaEdad);

		Optional<RdacaaRawRowResult> sa6 = categoriaTallaParaEdadRuleGroupExecutor.execute(input, resultTotalFact5);

		sa6.ifPresent(r -> {
			System.out.println(r.toString());
		});
		
		resultTotal.putAll(sa6.get());

		Fact resultTotalFact6 = new Fact("rdacaa_row_result", resultTotal);
		
		// PESO
		RowItem peso = new RowItem();
		peso.setItemValue("60");
		peso.setExcludedFromValidation(false);
		peso.setMandatory(false);
		input.addField(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_PESO, peso);

		Optional<RdacaaRawRowResult> sa7 = pesoRuleGroupExecutor.execute(input, resultTotalFact6);
		sa7.ifPresent(r -> {
			System.out.println(r.toString());
		});
		
		resultTotal.putAll(sa7.get());

		Fact resultTotalFact7 = new Fact("rdacaa_row_result", resultTotal);
		
		// PZ peso para talla
		
		RowItem pzPesoTalla = new RowItem();
		pzPesoTalla.setItemValue("999990000066666");
		pzPesoTalla.setExcludedFromValidation(false);
		pzPesoTalla.setMandatory(false);
		input.addField(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_PUNTAJE_Z_PESO_PARA_TALLA, pzPesoTalla);

		Optional<RdacaaRawRowResult> sa8 = puntajeZPesoParaTallaRuleGroupExecutor.execute(input, resultTotalFact7);
		sa8.ifPresent(r -> {
			System.out.println(r.toString());
		});
		
		resultTotal.putAll(sa8.get());

		Fact resultTotalFact8 = new Fact("rdacaa_row_result", resultTotal);
	
		// Cat PZ peso para talla
		RowItem catPzPesoTalla = new RowItem();
		catPzPesoTalla.setItemValue("999990000066666");
		catPzPesoTalla.setExcludedFromValidation(false);
		catPzPesoTalla.setMandatory(false);
		input.addField(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_CATEGORIA_PESO_PARA_TALLA, catPzPesoTalla);

		Optional<RdacaaRawRowResult> sa9 = categoriaPesoParaTallaRuleGroupExecutor.execute(input, resultTotalFact8);
		sa9.ifPresent(r -> {
			System.out.println(r.toString());
		});

		Assert.assertTrue("No se esperan errores",
				!sa4.get().get(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_TALLA_CORREGIDA).hasErrors() && sa4.get()
						.get(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_TALLA_CORREGIDA).getValue().equals("100.0"));

	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Test
	public void whenEdadMayorIgual20001YMenor050100Acostado_thenNoError() {
		RdacaaRawRowResult resultTotal = new RdacaaRawRowResult();
		RdacaaRawRow input = new RdacaaRawRow();
		// PESO
		RowItem sexo = new RowItem();
		sexo.setItemValue("17");// mujer
		sexo.setExcludedFromValidation(false);
		input.addField(RdacaaVariableKeyCatalog.PERSONA_CODIGO_SEXO, sexo);
		RowItem grupoPrioritarioEmbarazada = new RowItem();
		grupoPrioritarioEmbarazada.setItemValue("634");
		grupoPrioritarioEmbarazada.setExcludedFromValidation(false);
		input.addField(RdacaaVariableKeyCatalog.GRUPO_PRIORITARIO_1, grupoPrioritarioEmbarazada);
		RowItem fechaNac = new RowItem();
		fechaNac.setItemValue("2014-11-01");
		fechaNac.setExcludedFromValidation(false);
		input.addField(RdacaaVariableKeyCatalog.PERSONA_FECHA_NACIMIENTO, fechaNac);
		RowItem fechaAten = new RowItem();
		fechaAten.setItemValue("2018-11-01");
		fechaAten.setExcludedFromValidation(false);
		input.addField(RdacaaVariableKeyCatalog.ATENCION_FECHA, fechaAten);

		// TALLA
		RowItem talla = new RowItem();
		talla.setItemValue("100");
		talla.setExcludedFromValidation(false);
		talla.setMandatory(false);
		input.addField(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_TALLA, talla);

		Optional<RdacaaRawRowResult> sa2 = tallaRuleGroupExecutor.execute(input);

		sa2.ifPresent(r -> {
			System.out.println(r.toString());
		});

		resultTotal.putAll(sa2.get());

		Fact resultTotalFact2 = new Fact("rdacaa_row_result", resultTotal);

		// Tipo Toma Talla
		RowItem tipoTomaTalla = new RowItem();
		tipoTomaTalla.setItemValue(ACOSTADO);
		tipoTomaTalla.setExcludedFromValidation(false);
		tipoTomaTalla.setMandatory(false);
		input.addField(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_CODIGO_TIPO_TOMA_MEDIDA_TALLA, tipoTomaTalla);

		Optional<RdacaaRawRowResult> sa3 = tallaTipoTomaRuleGroupExecutor.execute(input, resultTotalFact2);

		sa3.ifPresent(r -> {
			System.out.println(r.toString());
		});

		resultTotal.putAll(sa3.get());

		Fact resultTotalFact3 = new Fact("rdacaa_row_result", resultTotal);

		RowItem tallaCorregida = new RowItem();
		tallaCorregida.setItemValue("999990000066666");
		tallaCorregida.setExcludedFromValidation(false);
		tallaCorregida.setMandatory(false);
		input.addField(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_TALLA_CORREGIDA, tallaCorregida);

		Optional<RdacaaRawRowResult> sa4 = tallaCorregidaRuleGroupExecutor.execute(input, resultTotalFact3);

		sa4.ifPresent(r -> {
			System.out.println(r.toString());
		});
		
		resultTotal.putAll(sa4.get());
		
		Fact resultTotalFact4 = new Fact("rdacaa_row_result", resultTotal);
		
		//PZ Talla para edad
		RowItem pzTallaEdad = new RowItem();
		pzTallaEdad.setItemValue("999990000066666");
		pzTallaEdad.setExcludedFromValidation(false);
		pzTallaEdad.setMandatory(false);
		input.addField(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_PUNTAJE_Z_TALLA_PARA_EDAD, pzTallaEdad);

		Optional<RdacaaRawRowResult> sa5 = puntajeZTallaParaEdadRuleGroupExecutor.execute(input, resultTotalFact4);

		sa5.ifPresent(r -> {
			System.out.println(r.toString());
		});
		
		resultTotal.putAll(sa5.get());

		Fact resultTotalFact5 = new Fact("rdacaa_row_result", resultTotal);
		
		//Cat PZ Talla para edad
		RowItem catPzTallaEdad = new RowItem();
		catPzTallaEdad.setItemValue("999990000066666");
		catPzTallaEdad.setExcludedFromValidation(false);
		catPzTallaEdad.setMandatory(false);
		input.addField(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_CATEGORIA_TALLA_PARA_EDAD, catPzTallaEdad);

		Optional<RdacaaRawRowResult> sa6 = categoriaTallaParaEdadRuleGroupExecutor.execute(input, resultTotalFact5);

		sa6.ifPresent(r -> {
			System.out.println(r.toString());
		});
		
		resultTotal.putAll(sa6.get());

		Fact resultTotalFact6 = new Fact("rdacaa_row_result", resultTotal);
		
		// PESO
		RowItem peso = new RowItem();
		peso.setItemValue("60");
		peso.setExcludedFromValidation(false);
		peso.setMandatory(false);
		input.addField(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_PESO, peso);

		Optional<RdacaaRawRowResult> sa7 = pesoRuleGroupExecutor.execute(input, resultTotalFact6);
		sa7.ifPresent(r -> {
			System.out.println(r.toString());
		});
		
		resultTotal.putAll(sa7.get());

		Fact resultTotalFact7 = new Fact("rdacaa_row_result", resultTotal);
		
		// PZ peso para talla
		
		RowItem pzPesoTalla = new RowItem();
		pzPesoTalla.setItemValue("999990000066666");
		pzPesoTalla.setExcludedFromValidation(false);
		pzPesoTalla.setMandatory(false);
		input.addField(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_PUNTAJE_Z_PESO_PARA_TALLA, pzPesoTalla);

		Optional<RdacaaRawRowResult> sa8 = puntajeZPesoParaTallaRuleGroupExecutor.execute(input, resultTotalFact7);
		sa8.ifPresent(r -> {
			System.out.println(r.toString());
		});
		
		resultTotal.putAll(sa8.get());

		Fact resultTotalFact8 = new Fact("rdacaa_row_result", resultTotal);
	
		// Cat PZ peso para talla
		RowItem catPzPesoTalla = new RowItem();
		catPzPesoTalla.setItemValue("999990000066666");
		catPzPesoTalla.setExcludedFromValidation(false);
		catPzPesoTalla.setMandatory(false);
		input.addField(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_CATEGORIA_PESO_PARA_TALLA, catPzPesoTalla);

		Optional<RdacaaRawRowResult> sa9 = categoriaPesoParaTallaRuleGroupExecutor.execute(input, resultTotalFact8);
		sa9.ifPresent(r -> {
			System.out.println(r.toString());
		});

		Assert.assertTrue("No se esperan errores",
				!sa4.get().get(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_TALLA_CORREGIDA).hasErrors() && sa4.get()
						.get(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_TALLA_CORREGIDA).getValue().equals("99.3"));

	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Test
	public void whenEdadMayorIgual010601YMenor020001DePie_thenNoError() {
		RdacaaRawRowResult resultTotal = new RdacaaRawRowResult();
		RdacaaRawRow input = new RdacaaRawRow();
		// PESO
		RowItem sexo = new RowItem();
		sexo.setItemValue("17");// mujer
		sexo.setExcludedFromValidation(false);
		input.addField(RdacaaVariableKeyCatalog.PERSONA_CODIGO_SEXO, sexo);
		RowItem grupoPrioritarioEmbarazada = new RowItem();
		grupoPrioritarioEmbarazada.setItemValue("634");
		grupoPrioritarioEmbarazada.setExcludedFromValidation(false);
		input.addField(RdacaaVariableKeyCatalog.GRUPO_PRIORITARIO_1, grupoPrioritarioEmbarazada);
		RowItem fechaNac = new RowItem();
		fechaNac.setItemValue("2017-04-01");
		fechaNac.setExcludedFromValidation(false);
		input.addField(RdacaaVariableKeyCatalog.PERSONA_FECHA_NACIMIENTO, fechaNac);
		RowItem fechaAten = new RowItem();
		fechaAten.setItemValue("2018-11-01");
		fechaAten.setExcludedFromValidation(false);
		input.addField(RdacaaVariableKeyCatalog.ATENCION_FECHA, fechaAten);

		// TALLA
		RowItem talla = new RowItem();
		talla.setItemValue("80");
		talla.setExcludedFromValidation(false);
		talla.setMandatory(false);
		input.addField(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_TALLA, talla);

		Optional<RdacaaRawRowResult> sa2 = tallaRuleGroupExecutor.execute(input);

		sa2.ifPresent(r -> {
			System.out.println(r.toString());
		});

		resultTotal.putAll(sa2.get());

		Fact resultTotalFact2 = new Fact("rdacaa_row_result", resultTotal);

		// Tipo Toma Talla
		RowItem tipoTomaTalla = new RowItem();
		tipoTomaTalla.setItemValue(DE_PIE);
		tipoTomaTalla.setExcludedFromValidation(false);
		tipoTomaTalla.setMandatory(false);
		input.addField(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_CODIGO_TIPO_TOMA_MEDIDA_TALLA, tipoTomaTalla);

		Optional<RdacaaRawRowResult> sa3 = tallaTipoTomaRuleGroupExecutor.execute(input, resultTotalFact2);

		sa3.ifPresent(r -> {
			System.out.println(r.toString());
		});

		resultTotal.putAll(sa3.get());

		Fact resultTotalFact3 = new Fact("rdacaa_row_result", resultTotal);

		RowItem tallaCorregida = new RowItem();
		tallaCorregida.setItemValue("999990000066666");
		tallaCorregida.setExcludedFromValidation(false);
		tallaCorregida.setMandatory(false);
		input.addField(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_TALLA_CORREGIDA, tallaCorregida);

		Optional<RdacaaRawRowResult> sa4 = tallaCorregidaRuleGroupExecutor.execute(input, resultTotalFact3);

		sa4.ifPresent(r -> {
			System.out.println(r.toString());
		});

		resultTotal.putAll(sa4.get());
		
		Fact resultTotalFact4 = new Fact("rdacaa_row_result", resultTotal);
		
		//PZ Talla para edad
		RowItem pzTallaEdad = new RowItem();
		pzTallaEdad.setItemValue("999990000066666");
		pzTallaEdad.setExcludedFromValidation(false);
		pzTallaEdad.setMandatory(false);
		input.addField(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_PUNTAJE_Z_TALLA_PARA_EDAD, pzTallaEdad);

		Optional<RdacaaRawRowResult> sa5 = puntajeZTallaParaEdadRuleGroupExecutor.execute(input, resultTotalFact4);

		sa5.ifPresent(r -> {
			System.out.println(r.toString());
		});
		
		resultTotal.putAll(sa5.get());

		Fact resultTotalFact5 = new Fact("rdacaa_row_result", resultTotal);
		
		//Cat PZ Talla para edad
		RowItem catPzTallaEdad = new RowItem();
		catPzTallaEdad.setItemValue("999990000066666");
		catPzTallaEdad.setExcludedFromValidation(false);
		catPzTallaEdad.setMandatory(false);
		input.addField(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_CATEGORIA_TALLA_PARA_EDAD, catPzTallaEdad);

		Optional<RdacaaRawRowResult> sa6 = categoriaTallaParaEdadRuleGroupExecutor.execute(input, resultTotalFact5);

		sa6.ifPresent(r -> {
			System.out.println(r.toString());
		});
		
		resultTotal.putAll(sa6.get());

		Fact resultTotalFact6 = new Fact("rdacaa_row_result", resultTotal);
		
		// PESO
		RowItem peso = new RowItem();
		peso.setItemValue("60");
		peso.setExcludedFromValidation(false);
		peso.setMandatory(false);
		input.addField(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_PESO, peso);

		Optional<RdacaaRawRowResult> sa7 = pesoRuleGroupExecutor.execute(input, resultTotalFact6);
		sa7.ifPresent(r -> {
			System.out.println(r.toString());
		});
		
		resultTotal.putAll(sa7.get());

		Fact resultTotalFact7 = new Fact("rdacaa_row_result", resultTotal);
		
		// PZ peso para talla
		
		RowItem pzPesoTalla = new RowItem();
		pzPesoTalla.setItemValue("999990000066666");
		pzPesoTalla.setExcludedFromValidation(false);
		pzPesoTalla.setMandatory(false);
		input.addField(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_PUNTAJE_Z_PESO_PARA_TALLA, pzPesoTalla);

		Optional<RdacaaRawRowResult> sa8 = puntajeZPesoParaTallaRuleGroupExecutor.execute(input, resultTotalFact7);
		sa8.ifPresent(r -> {
			System.out.println(r.toString());
		});
		
		resultTotal.putAll(sa8.get());

		Fact resultTotalFact8 = new Fact("rdacaa_row_result", resultTotal);
	
		// Cat PZ peso para talla
		RowItem catPzPesoTalla = new RowItem();
		catPzPesoTalla.setItemValue("999990000066666");
		catPzPesoTalla.setExcludedFromValidation(false);
		catPzPesoTalla.setMandatory(false);
		input.addField(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_CATEGORIA_PESO_PARA_TALLA, catPzPesoTalla);

		Optional<RdacaaRawRowResult> sa9 = categoriaPesoParaTallaRuleGroupExecutor.execute(input, resultTotalFact8);
		sa9.ifPresent(r -> {
			System.out.println(r.toString());
		});
		
		Assert.assertTrue("No se esperan errores",
				!sa4.get().get(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_TALLA_CORREGIDA).hasErrors() && sa4.get()
						.get(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_TALLA_CORREGIDA).getValue().equals("80.7"));

	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Test
	public void whenEdadMayorIgual010601YMenor020001Acostado_thenNoError() {
		RdacaaRawRowResult resultTotal = new RdacaaRawRowResult();
		RdacaaRawRow input = new RdacaaRawRow();
		// PESO
		RowItem sexo = new RowItem();
		sexo.setItemValue("17");// mujer
		sexo.setExcludedFromValidation(false);
		input.addField(RdacaaVariableKeyCatalog.PERSONA_CODIGO_SEXO, sexo);
		RowItem grupoPrioritarioEmbarazada = new RowItem();
		grupoPrioritarioEmbarazada.setItemValue("634");
		grupoPrioritarioEmbarazada.setExcludedFromValidation(false);
		input.addField(RdacaaVariableKeyCatalog.GRUPO_PRIORITARIO_1, grupoPrioritarioEmbarazada);
		RowItem fechaNac = new RowItem();
		fechaNac.setItemValue("2017-04-01");
		fechaNac.setExcludedFromValidation(false);
		input.addField(RdacaaVariableKeyCatalog.PERSONA_FECHA_NACIMIENTO, fechaNac);
		RowItem fechaAten = new RowItem();
		fechaAten.setItemValue("2018-11-01");
		fechaAten.setExcludedFromValidation(false);
		input.addField(RdacaaVariableKeyCatalog.ATENCION_FECHA, fechaAten);

		// TALLA
		RowItem talla = new RowItem();
		talla.setItemValue("80");
		talla.setExcludedFromValidation(false);
		talla.setMandatory(false);
		input.addField(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_TALLA, talla);

		Optional<RdacaaRawRowResult> sa2 = tallaRuleGroupExecutor.execute(input);

		sa2.ifPresent(r -> {
			System.out.println(r.toString());
		});

		resultTotal.putAll(sa2.get());

		Fact resultTotalFact2 = new Fact("rdacaa_row_result", resultTotal);

		// Tipo Toma Talla
		RowItem tipoTomaTalla = new RowItem();
		tipoTomaTalla.setItemValue(ACOSTADO);
		tipoTomaTalla.setExcludedFromValidation(false);
		tipoTomaTalla.setMandatory(false);
		input.addField(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_CODIGO_TIPO_TOMA_MEDIDA_TALLA, tipoTomaTalla);

		Optional<RdacaaRawRowResult> sa3 = tallaTipoTomaRuleGroupExecutor.execute(input, resultTotalFact2);

		sa3.ifPresent(r -> {
			System.out.println(r.toString());
		});

		resultTotal.putAll(sa3.get());

		Fact resultTotalFact3 = new Fact("rdacaa_row_result", resultTotal);

		RowItem tallaCorregida = new RowItem();
		tallaCorregida.setItemValue("999990000066666");
		tallaCorregida.setExcludedFromValidation(false);
		tallaCorregida.setMandatory(false);
		input.addField(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_TALLA_CORREGIDA, tallaCorregida);

		Optional<RdacaaRawRowResult> sa4 = tallaCorregidaRuleGroupExecutor.execute(input, resultTotalFact3);

		sa4.ifPresent(r -> {
			System.out.println(r.toString());
		});

		resultTotal.putAll(sa4.get());
		
		Fact resultTotalFact4 = new Fact("rdacaa_row_result", resultTotal);
		
		//PZ Talla para edad
		RowItem pzTallaEdad = new RowItem();
		pzTallaEdad.setItemValue("999990000066666");
		pzTallaEdad.setExcludedFromValidation(false);
		pzTallaEdad.setMandatory(false);
		input.addField(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_PUNTAJE_Z_TALLA_PARA_EDAD, pzTallaEdad);

		Optional<RdacaaRawRowResult> sa5 = puntajeZTallaParaEdadRuleGroupExecutor.execute(input, resultTotalFact4);

		sa5.ifPresent(r -> {
			System.out.println(r.toString());
		});
		
		resultTotal.putAll(sa5.get());

		Fact resultTotalFact5 = new Fact("rdacaa_row_result", resultTotal);
		
		//Cat PZ Talla para edad
		RowItem catPzTallaEdad = new RowItem();
		catPzTallaEdad.setItemValue("999990000066666");
		catPzTallaEdad.setExcludedFromValidation(false);
		catPzTallaEdad.setMandatory(false);
		input.addField(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_CATEGORIA_TALLA_PARA_EDAD, catPzTallaEdad);

		Optional<RdacaaRawRowResult> sa6 = categoriaTallaParaEdadRuleGroupExecutor.execute(input, resultTotalFact5);

		sa6.ifPresent(r -> {
			System.out.println(r.toString());
		});
		
		resultTotal.putAll(sa6.get());

		Fact resultTotalFact6 = new Fact("rdacaa_row_result", resultTotal);
		
		// PESO
		RowItem peso = new RowItem();
		peso.setItemValue("60");
		peso.setExcludedFromValidation(false);
		peso.setMandatory(false);
		input.addField(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_PESO, peso);

		Optional<RdacaaRawRowResult> sa7 = pesoRuleGroupExecutor.execute(input, resultTotalFact6);
		sa7.ifPresent(r -> {
			System.out.println(r.toString());
		});
		
		resultTotal.putAll(sa7.get());

		Fact resultTotalFact7 = new Fact("rdacaa_row_result", resultTotal);
		
		// PZ peso para talla
		
		RowItem pzPesoTalla = new RowItem();
		pzPesoTalla.setItemValue("999990000066666");
		pzPesoTalla.setExcludedFromValidation(false);
		pzPesoTalla.setMandatory(false);
		input.addField(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_PUNTAJE_Z_PESO_PARA_TALLA, pzPesoTalla);

		Optional<RdacaaRawRowResult> sa8 = puntajeZPesoParaTallaRuleGroupExecutor.execute(input, resultTotalFact7);
		sa8.ifPresent(r -> {
			System.out.println(r.toString());
		});
		
		resultTotal.putAll(sa8.get());

		Fact resultTotalFact8 = new Fact("rdacaa_row_result", resultTotal);
	
		// Cat PZ peso para talla
		RowItem catPzPesoTalla = new RowItem();
		catPzPesoTalla.setItemValue("999990000066666");
		catPzPesoTalla.setExcludedFromValidation(false);
		catPzPesoTalla.setMandatory(false);
		input.addField(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_CATEGORIA_PESO_PARA_TALLA, catPzPesoTalla);

		Optional<RdacaaRawRowResult> sa9 = categoriaPesoParaTallaRuleGroupExecutor.execute(input, resultTotalFact8);
		sa9.ifPresent(r -> {
			System.out.println(r.toString());
		});
		
		Assert.assertTrue("No se esperan errores",
				!sa4.get().get(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_TALLA_CORREGIDA).hasErrors() && sa4.get()
						.get(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_TALLA_CORREGIDA).getValue().equals("80.0"));

	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Test
	public void whenEdadMenor010601DePie_thenError() {
		RdacaaRawRowResult resultTotal = new RdacaaRawRowResult();
		RdacaaRawRow input = new RdacaaRawRow();
		// PESO
		RowItem sexo = new RowItem();
		sexo.setItemValue("17");// mujer
		sexo.setExcludedFromValidation(false);
		input.addField(RdacaaVariableKeyCatalog.PERSONA_CODIGO_SEXO, sexo);
		RowItem grupoPrioritarioEmbarazada = new RowItem();
		grupoPrioritarioEmbarazada.setItemValue("634");
		grupoPrioritarioEmbarazada.setExcludedFromValidation(false);
		input.addField(RdacaaVariableKeyCatalog.GRUPO_PRIORITARIO_1, grupoPrioritarioEmbarazada);
		RowItem fechaNac = new RowItem();
		fechaNac.setItemValue("2017-07-01");
		fechaNac.setExcludedFromValidation(false);
		input.addField(RdacaaVariableKeyCatalog.PERSONA_FECHA_NACIMIENTO, fechaNac);
		RowItem fechaAten = new RowItem();
		fechaAten.setItemValue("2018-11-01");
		fechaAten.setExcludedFromValidation(false);
		input.addField(RdacaaVariableKeyCatalog.ATENCION_FECHA, fechaAten);

		// TALLA
		RowItem talla = new RowItem();
		talla.setItemValue("80");
		talla.setExcludedFromValidation(false);
		talla.setMandatory(false);
		input.addField(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_TALLA, talla);

		Optional<RdacaaRawRowResult> sa2 = tallaRuleGroupExecutor.execute(input);

		sa2.ifPresent(r -> {
			System.out.println(r.toString());
		});

		resultTotal.putAll(sa2.get());

		Fact resultTotalFact2 = new Fact("rdacaa_row_result", resultTotal);

		// Tipo Toma Talla
		RowItem tipoTomaTalla = new RowItem();
		tipoTomaTalla.setItemValue(DE_PIE);
		tipoTomaTalla.setExcludedFromValidation(false);
		tipoTomaTalla.setMandatory(false);
		input.addField(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_CODIGO_TIPO_TOMA_MEDIDA_TALLA, tipoTomaTalla);

		Optional<RdacaaRawRowResult> sa3 = tallaTipoTomaRuleGroupExecutor.execute(input, resultTotalFact2);

		sa3.ifPresent(r -> {
			System.out.println(r.toString());
		});

		resultTotal.putAll(sa3.get());

		Fact resultTotalFact3 = new Fact("rdacaa_row_result", resultTotal);

		RowItem tallaCorregida = new RowItem();
		tallaCorregida.setItemValue("999990000066666");
		tallaCorregida.setExcludedFromValidation(false);
		tallaCorregida.setMandatory(false);
		input.addField(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_TALLA_CORREGIDA, tallaCorregida);

		Optional<RdacaaRawRowResult> sa4 = tallaCorregidaRuleGroupExecutor.execute(input, resultTotalFact3);

		sa4.ifPresent(r -> {
			System.out.println(r.toString());
		});
		
		resultTotal.putAll(sa4.get());
		
		Fact resultTotalFact4 = new Fact("rdacaa_row_result", resultTotal);
		
		//PZ Talla para edad
		RowItem pzTallaEdad = new RowItem();
		pzTallaEdad.setItemValue("999990000066666");
		pzTallaEdad.setExcludedFromValidation(false);
		pzTallaEdad.setMandatory(false);
		input.addField(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_PUNTAJE_Z_TALLA_PARA_EDAD, pzTallaEdad);

		Optional<RdacaaRawRowResult> sa5 = puntajeZTallaParaEdadRuleGroupExecutor.execute(input, resultTotalFact4);

		sa5.ifPresent(r -> {
			System.out.println(r.toString());
		});
		
		resultTotal.putAll(sa5.get());

		Fact resultTotalFact5 = new Fact("rdacaa_row_result", resultTotal);
		
		//Cat PZ Talla para edad
		RowItem catPzTallaEdad = new RowItem();
		catPzTallaEdad.setItemValue("999990000066666");
		catPzTallaEdad.setExcludedFromValidation(false);
		catPzTallaEdad.setMandatory(false);
		input.addField(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_CATEGORIA_TALLA_PARA_EDAD, catPzTallaEdad);

		Optional<RdacaaRawRowResult> sa6 = categoriaTallaParaEdadRuleGroupExecutor.execute(input, resultTotalFact5);

		sa6.ifPresent(r -> {
			System.out.println(r.toString());
		});
		
		resultTotal.putAll(sa6.get());

		Fact resultTotalFact6 = new Fact("rdacaa_row_result", resultTotal);
		
		// PESO
		RowItem peso = new RowItem();
		peso.setItemValue("60");
		peso.setExcludedFromValidation(false);
		peso.setMandatory(false);
		input.addField(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_PESO, peso);

		Optional<RdacaaRawRowResult> sa7 = pesoRuleGroupExecutor.execute(input, resultTotalFact6);
		sa7.ifPresent(r -> {
			System.out.println(r.toString());
		});
		
		resultTotal.putAll(sa7.get());

		Fact resultTotalFact7 = new Fact("rdacaa_row_result", resultTotal);
		
		// PZ peso para talla
		
		RowItem pzPesoTalla = new RowItem();
		pzPesoTalla.setItemValue("999990000066666");
		pzPesoTalla.setExcludedFromValidation(false);
		pzPesoTalla.setMandatory(false);
		input.addField(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_PUNTAJE_Z_PESO_PARA_TALLA, pzPesoTalla);

		Optional<RdacaaRawRowResult> sa8 = puntajeZPesoParaTallaRuleGroupExecutor.execute(input, resultTotalFact7);
		sa8.ifPresent(r -> {
			System.out.println(r.toString());
		});
		
		resultTotal.putAll(sa8.get());

		Fact resultTotalFact8 = new Fact("rdacaa_row_result", resultTotal);
	
		// Cat PZ peso para talla
		RowItem catPzPesoTalla = new RowItem();
		catPzPesoTalla.setItemValue("999990000066666");
		catPzPesoTalla.setExcludedFromValidation(false);
		catPzPesoTalla.setMandatory(false);
		input.addField(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_CATEGORIA_PESO_PARA_TALLA, catPzPesoTalla);

		Optional<RdacaaRawRowResult> sa9 = categoriaPesoParaTallaRuleGroupExecutor.execute(input, resultTotalFact8);
		sa9.ifPresent(r -> {
			System.out.println(r.toString());
		});

		Assert.assertTrue("Se espera error CODIGO_ERROR_DAT_ANT_TALLA_CORREGIDA_TIPO_TOMA_TIENE_ERRORES",
				sa4.get().get(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_TALLA_CORREGIDA).hasValidationResultByCode(
						ValidationResultCatalogConstants.CODIGO_ERROR_DAT_ANT_TALLA_CORREGIDA_TIPO_TOMA_TIENE_ERRORES).isPresent());

	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Test
	public void whenEdadMenor010601Acostado_thenNoError() {
		RdacaaRawRowResult resultTotal = new RdacaaRawRowResult();
		RdacaaRawRow input = new RdacaaRawRow();
		// PESO
		RowItem sexo = new RowItem();
		sexo.setItemValue("17");// mujer
		sexo.setExcludedFromValidation(false);
		input.addField(RdacaaVariableKeyCatalog.PERSONA_CODIGO_SEXO, sexo);
		RowItem grupoPrioritarioEmbarazada = new RowItem();
		grupoPrioritarioEmbarazada.setItemValue("634");
		grupoPrioritarioEmbarazada.setExcludedFromValidation(false);
		input.addField(RdacaaVariableKeyCatalog.GRUPO_PRIORITARIO_1, grupoPrioritarioEmbarazada);
		RowItem fechaNac = new RowItem();
		fechaNac.setItemValue("2017-07-01");
		fechaNac.setExcludedFromValidation(false);
		input.addField(RdacaaVariableKeyCatalog.PERSONA_FECHA_NACIMIENTO, fechaNac);
		RowItem fechaAten = new RowItem();
		fechaAten.setItemValue("2018-11-01");
		fechaAten.setExcludedFromValidation(false);
		input.addField(RdacaaVariableKeyCatalog.ATENCION_FECHA, fechaAten);

		// TALLA
		RowItem talla = new RowItem();
		talla.setItemValue("80");
		talla.setExcludedFromValidation(false);
		talla.setMandatory(false);
		input.addField(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_TALLA, talla);

		Optional<RdacaaRawRowResult> sa2 = tallaRuleGroupExecutor.execute(input);

		sa2.ifPresent(r -> {
			System.out.println(r.toString());
		});

		resultTotal.putAll(sa2.get());

		Fact resultTotalFact2 = new Fact("rdacaa_row_result", resultTotal);

		// Tipo Toma Talla
		RowItem tipoTomaTalla = new RowItem();
		tipoTomaTalla.setItemValue(ACOSTADO);
		tipoTomaTalla.setExcludedFromValidation(false);
		tipoTomaTalla.setMandatory(false);
		input.addField(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_CODIGO_TIPO_TOMA_MEDIDA_TALLA, tipoTomaTalla);

		Optional<RdacaaRawRowResult> sa3 = tallaTipoTomaRuleGroupExecutor.execute(input, resultTotalFact2);

		sa3.ifPresent(r -> {
			System.out.println(r.toString());
		});

		resultTotal.putAll(sa3.get());

		Fact resultTotalFact3 = new Fact("rdacaa_row_result", resultTotal);

		RowItem tallaCorregida = new RowItem();
		tallaCorregida.setItemValue("999990000066666");
		tallaCorregida.setExcludedFromValidation(false);
		tallaCorregida.setMandatory(false);
		input.addField(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_TALLA_CORREGIDA, tallaCorregida);

		Optional<RdacaaRawRowResult> sa4 = tallaCorregidaRuleGroupExecutor.execute(input, resultTotalFact3);

		sa4.ifPresent(r -> {
			System.out.println(r.toString());
		});
		
		resultTotal.putAll(sa4.get());
		
		Fact resultTotalFact4 = new Fact("rdacaa_row_result", resultTotal);
		
		//PZ Talla para edad
		RowItem pzTallaEdad = new RowItem();
		pzTallaEdad.setItemValue("999990000066666");
		pzTallaEdad.setExcludedFromValidation(false);
		pzTallaEdad.setMandatory(false);
		input.addField(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_PUNTAJE_Z_TALLA_PARA_EDAD, pzTallaEdad);

		Optional<RdacaaRawRowResult> sa5 = puntajeZTallaParaEdadRuleGroupExecutor.execute(input, resultTotalFact4);

		sa5.ifPresent(r -> {
			System.out.println(r.toString());
		});
		
		resultTotal.putAll(sa5.get());

		Fact resultTotalFact5 = new Fact("rdacaa_row_result", resultTotal);
		
		//Cat PZ Talla para edad
		RowItem catPzTallaEdad = new RowItem();
		catPzTallaEdad.setItemValue("999990000066666");
		catPzTallaEdad.setExcludedFromValidation(false);
		catPzTallaEdad.setMandatory(false);
		input.addField(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_CATEGORIA_TALLA_PARA_EDAD, catPzTallaEdad);

		Optional<RdacaaRawRowResult> sa6 = categoriaTallaParaEdadRuleGroupExecutor.execute(input, resultTotalFact5);

		sa6.ifPresent(r -> {
			System.out.println(r.toString());
		});
		
		resultTotal.putAll(sa6.get());

		Fact resultTotalFact6 = new Fact("rdacaa_row_result", resultTotal);
		
		// PESO
		RowItem peso = new RowItem();
		peso.setItemValue("60");
		peso.setExcludedFromValidation(false);
		peso.setMandatory(false);
		input.addField(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_PESO, peso);

		Optional<RdacaaRawRowResult> sa7 = pesoRuleGroupExecutor.execute(input, resultTotalFact6);
		sa7.ifPresent(r -> {
			System.out.println(r.toString());
		});
		
		resultTotal.putAll(sa7.get());

		Fact resultTotalFact7 = new Fact("rdacaa_row_result", resultTotal);
		
		// PZ peso para talla
		
		RowItem pzPesoTalla = new RowItem();
		pzPesoTalla.setItemValue("999990000066666");
		pzPesoTalla.setExcludedFromValidation(false);
		pzPesoTalla.setMandatory(false);
		input.addField(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_PUNTAJE_Z_PESO_PARA_TALLA, pzPesoTalla);

		Optional<RdacaaRawRowResult> sa8 = puntajeZPesoParaTallaRuleGroupExecutor.execute(input, resultTotalFact7);
		sa8.ifPresent(r -> {
			System.out.println(r.toString());
		});
		
		resultTotal.putAll(sa8.get());

		Fact resultTotalFact8 = new Fact("rdacaa_row_result", resultTotal);
	
		// Cat PZ peso para talla
		RowItem catPzPesoTalla = new RowItem();
		catPzPesoTalla.setItemValue("999990000066666");
		catPzPesoTalla.setExcludedFromValidation(false);
		catPzPesoTalla.setMandatory(false);
		input.addField(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_CATEGORIA_PESO_PARA_TALLA, catPzPesoTalla);

		Optional<RdacaaRawRowResult> sa9 = categoriaPesoParaTallaRuleGroupExecutor.execute(input, resultTotalFact8);
		sa9.ifPresent(r -> {
			System.out.println(r.toString());
		});

		Assert.assertTrue("Se espera error CODIGO_ERROR_DAT_ANT_TALLA_CORREGIDA_TIPO_TOMA_TIENE_ERRORES",
				!sa4.get().get(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_TALLA_CORREGIDA).hasErrors() && sa4.get()
				.get(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_TALLA_CORREGIDA).getValue().equals("80.0"));

	}

}

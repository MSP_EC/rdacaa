/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.gob.msp.rdacaa.business.validation.rule.test;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaRawRow;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaRawRowResult;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.malla.RowItem;
import ec.gob.msp.rdacaa.business.validation.rule.persona.tiposeguro.TipoSeguroRuleGroupExecutor;
import ec.gob.msp.rdacaa.test.ValidationTestContext;
import java.util.Optional;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

/**
 *
 * @author eduardo
 */

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = ValidationTestContext.class)
public class TipoSeguroRuleGroupExecutorTest {
    
    @Autowired
    private TipoSeguroRuleGroupExecutor tipoSeguroRuleGroupExecutor;
    
    @Test
    public void whenTipoSeguroIsExcludedAndValorIsNull(){
    	RdacaaRawRow input = new RdacaaRawRow();
    	RowItem ritem = new RowItem();
    	ritem.setItemValue("999990000066666");
    	ritem.setExcludedFromValidation(true);
    	input.addField(RdacaaVariableKeyCatalog.PERSONA_CODIGO_SEGURO, ritem);
        Optional<RdacaaRawRowResult> sa = tipoSeguroRuleGroupExecutor.execute(input);
        sa.ifPresent(r -> {
            System.out.println(r.toString());
        });
        Assert.assertTrue("TipoSeguro debe ser nulo",
                sa.get().get(RdacaaVariableKeyCatalog.PERSONA_CODIGO_SEGURO).getValidationResultList().isEmpty());
    }

    
    @Test
    public void whenTipoSeguroIsNotExcludedAndValorIsNotNull(){
    	RdacaaRawRow input = new RdacaaRawRow();
    	RowItem ritem = new RowItem();
    	ritem.setItemValue("81");
    	ritem.setExcludedFromValidation(false);
    	input.addField(RdacaaVariableKeyCatalog.PERSONA_CODIGO_SEGURO, ritem);
        Optional<RdacaaRawRowResult> sa = tipoSeguroRuleGroupExecutor.execute(input);
        sa.ifPresent(r -> {
            System.out.println(r.toString());
        });
        Assert.assertTrue("El Tipo de Identificacion debe ser nulo",
                sa.get().get(RdacaaVariableKeyCatalog.PERSONA_CODIGO_SEGURO).getValidationResultList().isEmpty());
    }

    
    @Test
    public void whenTipoSeguroIsNull_thenError() {
        RdacaaRawRow input = new RdacaaRawRow();
        RowItem ritem = new RowItem();
        ritem.setItemValue(null);
        ritem.setExcludedFromValidation(false);
        input.addField(RdacaaVariableKeyCatalog.PERSONA_CODIGO_SEGURO, ritem);
        Optional<RdacaaRawRowResult> sa = tipoSeguroRuleGroupExecutor.execute(input);
        sa.ifPresent(r -> {
            System.out.println(r.toString());
        });
        Assert.assertTrue("El Tipo de Seguro es obligatorio",
                sa.get().get(RdacaaVariableKeyCatalog.PERSONA_CODIGO_SEGURO)
                        .hasValidationResultByCode(ValidationResultCatalogConstants.CODIGO_ERROR_TIPO_SEGURO_NULL)
                        .isPresent());
    }
    
    @Test
    public void whenTipoSeguroIsNotNumeric_thenError() {
        RdacaaRawRow input = new RdacaaRawRow();
        RowItem ritem = new RowItem();
        ritem.setItemValue("sdf");
        ritem.setExcludedFromValidation(false);
        input.addField(RdacaaVariableKeyCatalog.PERSONA_CODIGO_SEGURO, ritem);
        Optional<RdacaaRawRowResult> sa = tipoSeguroRuleGroupExecutor.execute(input);
        sa.ifPresent(r -> {
            System.out.println(r.toString());
        });
        Assert.assertTrue("Se espera un error para caracteres no numericos ",
                sa.get().get(RdacaaVariableKeyCatalog.PERSONA_CODIGO_SEGURO)
                        .hasValidationResultByCode(ValidationResultCatalogConstants.CODIGO_ERROR_TIPO_SEGURO_NUMERICO)
                        .isPresent());
    }
    
    @Test
    public void whenTipoSeguroIsNotCorrect_thenError() {
        RdacaaRawRow input = new RdacaaRawRow();
        RowItem ritem = new RowItem();
        ritem.setItemValue("1000");
        ritem.setExcludedFromValidation(false);
        input.addField(RdacaaVariableKeyCatalog.PERSONA_CODIGO_SEGURO, ritem);
        Optional<RdacaaRawRowResult> sa = tipoSeguroRuleGroupExecutor.execute(input);
        sa.ifPresent(r -> {
            System.out.println(r.toString());
        });
        Assert.assertTrue("Se espera un error para codigo incorrecto ",
                sa.get().get(RdacaaVariableKeyCatalog.PERSONA_CODIGO_SEGURO)
                        .hasValidationResultByCode(ValidationResultCatalogConstants.CODIGO_ERROR_TIPO_SEGURO_INCORRECTO)
                        .isPresent());
    }
    
    @Test
    public void whenTipoSeguroIsCorrect_thenOk() {
        RdacaaRawRow input = new RdacaaRawRow();
        RowItem ritem = new RowItem();
        ritem.setItemValue("81");
        ritem.setExcludedFromValidation(false);
        input.addField(RdacaaVariableKeyCatalog.PERSONA_CODIGO_SEGURO, ritem);
        Optional<RdacaaRawRowResult> sa = tipoSeguroRuleGroupExecutor.execute(input);
        sa.ifPresent(r -> {
            System.out.println(r.toString());
        });
        Assert.assertTrue("No se esperan errores para Tipo de Seguro correcto",
                sa.get().get(RdacaaVariableKeyCatalog.PERSONA_CODIGO_SEGURO).getValidationResultList().isEmpty());
    }
}

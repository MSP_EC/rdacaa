/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.gob.msp.rdacaa.business.validation.rule.test;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaRawRow;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaRawRowResult;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.malla.RowItem;
import ec.gob.msp.rdacaa.business.validation.rule.persona.tipobono.TipoBonoRuleGroupExecutor;
import ec.gob.msp.rdacaa.test.ValidationTestContext;
import java.util.Optional;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

/**
 *
 * @author eduardo
 */
@RunWith(SpringRunner.class)
@ContextConfiguration(classes = ValidationTestContext.class)
public class TipoBonoRuleGroupExecutorTest {
    
    @Autowired
    private TipoBonoRuleGroupExecutor tipoBonoRuleGroupExecutor;
    
    @Test
    public void whenTipoBonoIsExcludedAndValorIsNull(){
    	RdacaaRawRow input = new RdacaaRawRow();
    	RowItem ritem = new RowItem();
    	ritem.setItemValue("999990000066666");
    	ritem.setExcludedFromValidation(true);
    	input.addField(RdacaaVariableKeyCatalog.PERSONA_CODIGO_TIPO_BONO, ritem);
        Optional<RdacaaRawRowResult> sa = tipoBonoRuleGroupExecutor.execute(input);
        sa.ifPresent(r -> {
            System.out.println(r.toString());
        });
        Assert.assertTrue("TipoBono debe ser nulo",
                sa.get().get(RdacaaVariableKeyCatalog.PERSONA_CODIGO_TIPO_BONO).getValidationResultList().isEmpty());
    }
    
    @Test
    public void whenTipoBonoIsExcludedAndValorIsNotNull(){
    	RdacaaRawRow input = new RdacaaRawRow();
    	RowItem ritem = new RowItem();
    	ritem.setItemValue("7");
    	ritem.setExcludedFromValidation(true);
    	input.addField(RdacaaVariableKeyCatalog.PERSONA_CODIGO_TIPO_BONO, ritem);
        Optional<RdacaaRawRowResult> sa = tipoBonoRuleGroupExecutor.execute(input);
        sa.ifPresent(r -> {
            System.out.println(r.toString());
        });
        Assert.assertTrue("TipoBono debe ser nulo",
                sa.get().get(RdacaaVariableKeyCatalog.PERSONA_CODIGO_TIPO_BONO)
                        .hasValidationResultByCode(ValidationResultCatalogConstants.CODIGO_ERROR_TIPO_BONO_NO_DEFINIDO)
                        .isPresent());
    }
    
    @Test
    public void whenTipoBonoIsNotExcludedAndValorIsNotNull(){
    	RdacaaRawRow input = new RdacaaRawRow();
    	RowItem ritem = new RowItem();
    	ritem.setItemValue("85");
    	ritem.setExcludedFromValidation(false);
    	input.addField(RdacaaVariableKeyCatalog.PERSONA_CODIGO_TIPO_BONO, ritem);
        Optional<RdacaaRawRowResult> sa = tipoBonoRuleGroupExecutor.execute(input);
        sa.ifPresent(r -> {
            System.out.println(r.toString());
        });
        Assert.assertTrue("TipoBono debe ser nulo",
                sa.get().get(RdacaaVariableKeyCatalog.PERSONA_CODIGO_TIPO_BONO).getValidationResultList().isEmpty());
    }
    
    @Test
    public void whenTipoBonoIsNull_thenError() {
        RdacaaRawRow input = new RdacaaRawRow();
        RowItem ritem = new RowItem();
        ritem.setItemValue(null);
        ritem.setExcludedFromValidation(false);
        input.addField(RdacaaVariableKeyCatalog.PERSONA_CODIGO_TIPO_BONO, ritem);
        Optional<RdacaaRawRowResult> sa = tipoBonoRuleGroupExecutor.execute(input);
        sa.ifPresent(r -> {
            System.out.println(r.toString());
        });
        Assert.assertTrue("El Tipo de Bono es obligatorio",
                sa.get().get(RdacaaVariableKeyCatalog.PERSONA_CODIGO_TIPO_BONO)
                        .hasValidationResultByCode(ValidationResultCatalogConstants.CODIGO_ERROR_TIPO_BONO_NULL)
                        .isPresent());
    }
    
    @Test
    public void whenTipoBonoIsNotNumeric_thenError() {
        RdacaaRawRow input = new RdacaaRawRow();
        RowItem ritem = new RowItem();
        ritem.setItemValue("sdf");
        ritem.setExcludedFromValidation(false);
        input.addField(RdacaaVariableKeyCatalog.PERSONA_CODIGO_TIPO_BONO, ritem);
        Optional<RdacaaRawRowResult> sa = tipoBonoRuleGroupExecutor.execute(input);
        sa.ifPresent(r -> {
            System.out.println(r.toString());
        });
        Assert.assertTrue("Se espera un error para caracteres no numericos ",
                sa.get().get(RdacaaVariableKeyCatalog.PERSONA_CODIGO_TIPO_BONO)
                        .hasValidationResultByCode(ValidationResultCatalogConstants.CODIGO_ERROR_TIPO_BONO_NUMERICO)
                        .isPresent());
    }
    
    @Test
    public void whenTipoBonoIsNotCorrect_thenError() {
        RdacaaRawRow input = new RdacaaRawRow();
        RowItem ritem = new RowItem();
        ritem.setItemValue("100");
        ritem.setExcludedFromValidation(false);
        input.addField(RdacaaVariableKeyCatalog.PERSONA_CODIGO_TIPO_BONO, ritem);
        Optional<RdacaaRawRowResult> sa = tipoBonoRuleGroupExecutor.execute(input);
        sa.ifPresent(r -> {
            System.out.println(r.toString());
        });
        Assert.assertTrue("Se espera un error para codigo incorrecto ",
                sa.get().get(RdacaaVariableKeyCatalog.PERSONA_CODIGO_TIPO_BONO)
                        .hasValidationResultByCode(ValidationResultCatalogConstants.CODIGO_ERROR_TIPO_BONO_INCORRECTO)
                        .isPresent());
    }
    
    @Test
    public void whenTipoBonoIsCorrect_thenOk() {
        RdacaaRawRow input = new RdacaaRawRow();
        RowItem ritem = new RowItem();
        ritem.setItemValue("85");
        ritem.setExcludedFromValidation(false);
        input.addField(RdacaaVariableKeyCatalog.PERSONA_CODIGO_TIPO_BONO, ritem);
        Optional<RdacaaRawRowResult> sa = tipoBonoRuleGroupExecutor.execute(input);
        sa.ifPresent(r -> {
            System.out.println(r.toString());
        });
        Assert.assertTrue("No se esperan errores para Tipo de Bono correcto",
                sa.get().get(RdacaaVariableKeyCatalog.PERSONA_CODIGO_TIPO_BONO).getValidationResultList().isEmpty());
    }
}

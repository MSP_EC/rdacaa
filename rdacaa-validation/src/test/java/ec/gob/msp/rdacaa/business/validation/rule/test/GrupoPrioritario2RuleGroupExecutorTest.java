package ec.gob.msp.rdacaa.business.validation.rule.test;

import java.util.Optional;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import ec.gob.msp.rdacaa.business.service.ConstantesDetalleCatalogo;
import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaRawRow;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaRawRowResult;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.malla.RowItem;
import ec.gob.msp.rdacaa.business.validation.rule.atencion.grupoprioritario.gpr_prioritario2.GrupoPrioritario2RuleGroupExecutor;
import ec.gob.msp.rdacaa.test.ValidationTestContext;

/**
 *
 * @author miguel.faubla
 */
@RunWith(SpringRunner.class)
@ContextConfiguration(classes = ValidationTestContext.class)
public class GrupoPrioritario2RuleGroupExecutorTest {

	@Autowired
	private GrupoPrioritario2RuleGroupExecutor grupoPrioritario2RuleGroupExecutor;

	@Test
	public void whenGrupoPrioritario2IsExcludedAndValorIsNull() {
		RdacaaRawRow input = new RdacaaRawRow();
		RowItem grupoprioritarioId = new RowItem();
		grupoprioritarioId.setItemValue(ConstantesDetalleCatalogo.NOT_DEFINED_VALUE);
		grupoprioritarioId.setExcludedFromValidation(true);
		input.addField(RdacaaVariableKeyCatalog.GRUPO_PRIORITARIO_2, grupoprioritarioId);
		Optional<RdacaaRawRowResult> sa = grupoPrioritario2RuleGroupExecutor.execute(input);
		sa.ifPresent(r -> {
			System.out.println(r.toString());
		});
		Assert.assertTrue("El Grupo Prioritario 2 debe ser nulo",
				sa.get().get(RdacaaVariableKeyCatalog.GRUPO_PRIORITARIO_2).getValidationResultList().isEmpty());
	}

	@Test
	public void whenGrupoPrioritario2IsExcludedAndValorIsNotNull() {
		RdacaaRawRow input = new RdacaaRawRow();
		RowItem grupoprioritarioId = new RowItem();
		grupoprioritarioId.setItemValue("643"); // Trabajador/A Sexual
		grupoprioritarioId.setExcludedFromValidation(true);
		input.addField(RdacaaVariableKeyCatalog.GRUPO_PRIORITARIO_2, grupoprioritarioId);
		Optional<RdacaaRawRowResult> sa = grupoPrioritario2RuleGroupExecutor.execute(input);
		sa.ifPresent(r -> {
			System.out.println(r.toString());
		});
		Assert.assertTrue("El Grupo Prioritario 2 no debe ser nulo",
				sa.get().get(RdacaaVariableKeyCatalog.GRUPO_PRIORITARIO_2)
						.hasValidationResultByCode(
								ValidationResultCatalogConstants.CODIGO_ERROR_GRUPO_PRIORITARIO_2_NO_DEFINIDA)
						.isPresent());
	}

	@Test
	public void whenGrupoPrioritario2IsNotExcludedAndValorIsNotNull() {
		RdacaaRawRow input = new RdacaaRawRow();
		RowItem itemSexo = new RowItem();
		itemSexo.setItemValue("16"); // Hombre
		itemSexo.setExcludedFromValidation(false);
		RowItem itemFechaNac = new RowItem();
		itemFechaNac.setItemValue("2016-09-01");
		itemFechaNac.setExcludedFromValidation(false);
		RowItem itemFechaAtenc = new RowItem();
		itemFechaAtenc.setItemValue("2018-09-14");
		itemFechaAtenc.setExcludedFromValidation(false);
		RowItem grupoprioritarioId = new RowItem();
		grupoprioritarioId.setItemValue("639"); // Privadas De La Libertad
		grupoprioritarioId.setExcludedFromValidation(false);

		input.addField(RdacaaVariableKeyCatalog.PERSONA_CODIGO_SEXO, itemSexo);
		input.addField(RdacaaVariableKeyCatalog.PERSONA_FECHA_NACIMIENTO, itemFechaNac);
		input.addField(RdacaaVariableKeyCatalog.ATENCION_FECHA, itemFechaAtenc);
		input.addField(RdacaaVariableKeyCatalog.GRUPO_PRIORITARIO_2, grupoprioritarioId);
		Optional<RdacaaRawRowResult> sa = grupoPrioritario2RuleGroupExecutor.execute(input);
		sa.ifPresent(r -> {
			System.out.println(r.toString());
		});
		Assert.assertTrue("El Grupo Prioritario 2 no debe ser nulo",
				sa.get().get(RdacaaVariableKeyCatalog.GRUPO_PRIORITARIO_2).getValidationResultList().isEmpty());
	}

	@Test
	public void whenGrupoPrioritario2IsNull_thenError() {
		RdacaaRawRow input = new RdacaaRawRow();
		RowItem grupoprioritarioId = new RowItem();
		grupoprioritarioId.setItemValue(null);
		grupoprioritarioId.setExcludedFromValidation(false);
		input.addField(RdacaaVariableKeyCatalog.GRUPO_PRIORITARIO_2, grupoprioritarioId);
		Optional<RdacaaRawRowResult> sa = grupoPrioritario2RuleGroupExecutor.execute(input);
		sa.ifPresent(r -> {
			System.out.println(r.toString());
		});
		Assert.assertTrue("El campo es obligatorio", sa.get().get(RdacaaVariableKeyCatalog.GRUPO_PRIORITARIO_2)
				.hasValidationResultByCode(ValidationResultCatalogConstants.CODIGO_ERROR_GRUPO_PRIORITARIO_2_NULO)
				.isPresent());
	}

	@Test
	public void whenGrupoPrioritario2IsNotNumeric_thenError() {
		RdacaaRawRow input = new RdacaaRawRow();
		RowItem grupoprioritarioId = new RowItem();
		grupoprioritarioId.setItemValue("GRUPO PRIORITARIO 2");
		grupoprioritarioId.setExcludedFromValidation(false);
		input.addField(RdacaaVariableKeyCatalog.GRUPO_PRIORITARIO_2, grupoprioritarioId);
		Optional<RdacaaRawRowResult> sa = grupoPrioritario2RuleGroupExecutor.execute(input);
		sa.ifPresent(r -> {
			System.out.println(r.toString());
		});
		Assert.assertTrue("Se espera un error para caracteres no numéricos ",
				sa.get().get(RdacaaVariableKeyCatalog.GRUPO_PRIORITARIO_2)
						.hasValidationResultByCode(
								ValidationResultCatalogConstants.CODIGO_ERROR_GRUPO_PRIORITARIO_2_NONUMERICO)
						.isPresent());
	}

	@Test
	public void whenGrupoPrioritario2IsIncorrecto_thenError() {
		RdacaaRawRow input = new RdacaaRawRow();
		RowItem itemSexo = new RowItem();
		itemSexo.setItemValue("114");
		itemSexo.setExcludedFromValidation(false);
		RowItem itemGrupoPrio = new RowItem();
		itemGrupoPrio.setItemValue("145");
		itemGrupoPrio.setExcludedFromValidation(false);
		RowItem itemFechaNac = new RowItem();
		itemFechaNac.setItemValue("2017-09-01");
		itemFechaNac.setExcludedFromValidation(false);
		RowItem itemFechaAtenc = new RowItem();
		itemFechaAtenc.setItemValue("2018-09-14");
		itemFechaAtenc.setExcludedFromValidation(false);
		input.addField(RdacaaVariableKeyCatalog.PERSONA_CODIGO_SEXO, itemSexo);
		input.addField(RdacaaVariableKeyCatalog.PERSONA_FECHA_NACIMIENTO, itemFechaNac);
		input.addField(RdacaaVariableKeyCatalog.ATENCION_FECHA, itemFechaAtenc);
		input.addField(RdacaaVariableKeyCatalog.GRUPO_PRIORITARIO_2, itemGrupoPrio);
		Optional<RdacaaRawRowResult> sa = grupoPrioritario2RuleGroupExecutor.execute(input);
		sa.ifPresent(r -> {
			System.out.println(r.toString());
		});
		Assert.assertTrue("El campo es incorrecto",
				sa.get().get(RdacaaVariableKeyCatalog.GRUPO_PRIORITARIO_2)
						.hasValidationResultByCode(
								ValidationResultCatalogConstants.CODIGO_ERROR_GRUPO_PRIORITARIO_2_INCORRECTO)
						.isPresent());
	}

	@Test
	public void whenGrupoPrioritario2IsCorrect_thenOk() {
		RdacaaRawRow input = new RdacaaRawRow();
		RowItem itemSexo = new RowItem();
		itemSexo.setItemValue("17"); // Mujer
		itemSexo.setExcludedFromValidation(false);
		RowItem grupoprioritarioId = new RowItem();
		grupoprioritarioId.setItemValue("634"); // Embarazadas
		grupoprioritarioId.setExcludedFromValidation(false);
		RowItem itemFechaNac = new RowItem();
		itemFechaNac.setItemValue("2004-09-01");
		itemFechaNac.setExcludedFromValidation(false);
		RowItem itemFechaAtenc = new RowItem();
		itemFechaAtenc.setItemValue("2018-09-14");
		itemFechaAtenc.setExcludedFromValidation(false);

		input.addField(RdacaaVariableKeyCatalog.PERSONA_CODIGO_SEXO, itemSexo);
		input.addField(RdacaaVariableKeyCatalog.PERSONA_FECHA_NACIMIENTO, itemFechaNac);
		input.addField(RdacaaVariableKeyCatalog.ATENCION_FECHA, itemFechaAtenc);
		input.addField(RdacaaVariableKeyCatalog.GRUPO_PRIORITARIO_2, grupoprioritarioId);
		Optional<RdacaaRawRowResult> sa = grupoPrioritario2RuleGroupExecutor.execute(input);
		sa.ifPresent(r -> {
			System.out.println(r.toString());
		});
		Assert.assertTrue("No se esperan errores para Grupo Prioritario 2 correcto",
				sa.get().get(RdacaaVariableKeyCatalog.GRUPO_PRIORITARIO_2).getValidationResultList().isEmpty());
	}

	@Test
	public void whenGrupoPrioritario2IsMandatory() {
		RdacaaRawRow input = new RdacaaRawRow();
		RowItem grupoprioritarioId = new RowItem();
		grupoprioritarioId.setItemValue(ConstantesDetalleCatalogo.NOT_DEFINED_VALUE);
		grupoprioritarioId.setExcludedFromValidation(false);
		grupoprioritarioId.setMandatory(true);
		input.addField(RdacaaVariableKeyCatalog.GRUPO_PRIORITARIO_2, grupoprioritarioId);
		Optional<RdacaaRawRowResult> sa = grupoPrioritario2RuleGroupExecutor.execute(input);
		sa.ifPresent(r -> {
			System.out.println(r.toString());
		});
		Assert.assertTrue("El Grupo Prioritario 2 es mandatorio",
				sa.get().get(RdacaaVariableKeyCatalog.GRUPO_PRIORITARIO_2)
						.hasValidationResultByCode(
								ValidationResultCatalogConstants.CODIGO_ERROR_GRUPO_PRIORITARIO_2_ES_MANDATORIA)
						.isPresent());
	}

	@Test
	public void whenGrupoPrioritario2IsMandatoryAndCorrectValue() {
		RdacaaRawRow input = new RdacaaRawRow();
		RowItem itemSexo = new RowItem();
		itemSexo.setItemValue("17"); // MUjer
		itemSexo.setExcludedFromValidation(false);
		RowItem itemFechaNac = new RowItem();
		itemFechaNac.setItemValue("2002-09-01");
		itemFechaNac.setExcludedFromValidation(false);
		RowItem itemFechaAtenc = new RowItem();
		itemFechaAtenc.setItemValue("2018-09-14");
		itemFechaAtenc.setExcludedFromValidation(false);
		RowItem grupoprioritarioId = new RowItem();
		grupoprioritarioId.setItemValue("634"); // Embarazadas
		grupoprioritarioId.setExcludedFromValidation(false);
		grupoprioritarioId.setMandatory(true);
		
		input.addField(RdacaaVariableKeyCatalog.PERSONA_CODIGO_SEXO, itemSexo);
		input.addField(RdacaaVariableKeyCatalog.PERSONA_FECHA_NACIMIENTO, itemFechaNac);
		input.addField(RdacaaVariableKeyCatalog.ATENCION_FECHA, itemFechaAtenc);
		input.addField(RdacaaVariableKeyCatalog.GRUPO_PRIORITARIO_2, grupoprioritarioId);
		Optional<RdacaaRawRowResult> sa = grupoPrioritario2RuleGroupExecutor.execute(input);
		sa.ifPresent(r -> {
			System.out.println(r.toString());
		});
		Assert.assertTrue("El Grupo Prioritario 2 es mandatorio",
				sa.get().get(RdacaaVariableKeyCatalog.GRUPO_PRIORITARIO_2).getValidationResultList().isEmpty());
	}

}

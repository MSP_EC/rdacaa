package ec.gob.msp.rdacaa.business.validation.rule.datosantropometricos.test;

import java.util.Optional;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaRawRow;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaRawRowResult;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.malla.RowItem;
import ec.gob.msp.rdacaa.business.validation.rule.atencion.datosantropometricos.valorhb.ValorHBRuleGroupExecutor;
import ec.gob.msp.rdacaa.test.ValidationTestContext;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = ValidationTestContext.class)
public class ValorHBValidateForAgeRuleTest {

	@Autowired
	private ValorHBRuleGroupExecutor valorHBRuleGroupExecutor;

	@Test
	public void whenEdadMenor000600NotDefined_thenNoError() {
		RdacaaRawRow input = new RdacaaRawRow();
		RowItem fechaNac = new RowItem();
		fechaNac.setItemValue("2018-06-01");
		fechaNac.setExcludedFromValidation(false);
		input.addField(RdacaaVariableKeyCatalog.PERSONA_FECHA_NACIMIENTO, fechaNac);
		RowItem fechaAten = new RowItem();
		fechaAten.setItemValue("2018-11-01");
		fechaAten.setExcludedFromValidation(false);
		input.addField(RdacaaVariableKeyCatalog.ATENCION_FECHA, fechaAten);
		RowItem valorHB = new RowItem();
		valorHB.setItemValue("999990000066666");
		valorHB.setExcludedFromValidation(false);
		valorHB.setMandatory(false);
		input.addField(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_VALOR_HB, valorHB);

		Optional<RdacaaRawRowResult> sa = valorHBRuleGroupExecutor.execute(input);
		sa.ifPresent(r -> {
			System.out.println(r.toString());
		});
		Assert.assertTrue("El valor de prueba es correcto no debe existir errores",
				!sa.get().get(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_VALOR_HB).hasErrors());
	}

	@Test
	public void whenEdadMayor000600NotDefined_thenNoError() {
		RdacaaRawRow input = new RdacaaRawRow();
		RowItem fechaNac = new RowItem();
		fechaNac.setItemValue("2018-03-01");
		fechaNac.setExcludedFromValidation(false);
		input.addField(RdacaaVariableKeyCatalog.PERSONA_FECHA_NACIMIENTO, fechaNac);
		RowItem fechaAten = new RowItem();
		fechaAten.setItemValue("2018-11-01");
		fechaAten.setExcludedFromValidation(false);
		input.addField(RdacaaVariableKeyCatalog.ATENCION_FECHA, fechaAten);
		RowItem valorHB = new RowItem();
		valorHB.setItemValue("999990000066666");
		valorHB.setExcludedFromValidation(false);
		valorHB.setMandatory(false);
		input.addField(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_VALOR_HB, valorHB);

		Optional<RdacaaRawRowResult> sa = valorHBRuleGroupExecutor.execute(input);
		sa.ifPresent(r -> {
			System.out.println(r.toString());
		});
		Assert.assertTrue("El valor de prueba es correcto no debe existir errores",
				!sa.get().get(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_VALOR_HB).hasErrors());
	}

	@Test
	public void whenEdadMayor000600ValorOK_thenNoError() {
		RdacaaRawRow input = new RdacaaRawRow();
		RowItem fechaNac = new RowItem();
		fechaNac.setItemValue("2018-03-01");
		fechaNac.setExcludedFromValidation(false);
		input.addField(RdacaaVariableKeyCatalog.PERSONA_FECHA_NACIMIENTO, fechaNac);
		RowItem fechaAten = new RowItem();
		fechaAten.setItemValue("2018-11-01");
		fechaAten.setExcludedFromValidation(false);
		input.addField(RdacaaVariableKeyCatalog.ATENCION_FECHA, fechaAten);
		RowItem valorHB = new RowItem();
		valorHB.setItemValue("20");
		valorHB.setExcludedFromValidation(false);
		valorHB.setMandatory(false);
		input.addField(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_VALOR_HB, valorHB);

		Optional<RdacaaRawRowResult> sa = valorHBRuleGroupExecutor.execute(input);
		sa.ifPresent(r -> {
			System.out.println(r.toString());
		});
		Assert.assertTrue("El valor de prueba es correcto no debe existir errores",
				!sa.get().get(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_VALOR_HB).hasErrors());
	}

	@Test
	public void whenEdadMayor000600FechasInvalidas_thenError() {
		RdacaaRawRow input = new RdacaaRawRow();
		RowItem fechaNac = new RowItem();
		fechaNac.setItemValue("XXXX-03-01");
		fechaNac.setExcludedFromValidation(false);
		input.addField(RdacaaVariableKeyCatalog.PERSONA_FECHA_NACIMIENTO, fechaNac);
		RowItem fechaAten = new RowItem();
		fechaAten.setItemValue("2018-11-XX");
		fechaAten.setExcludedFromValidation(false);
		input.addField(RdacaaVariableKeyCatalog.ATENCION_FECHA, fechaAten);
		RowItem valorHB = new RowItem();
		valorHB.setItemValue("20");
		valorHB.setExcludedFromValidation(false);
		valorHB.setMandatory(false);
		input.addField(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_VALOR_HB, valorHB);

		Optional<RdacaaRawRowResult> sa = valorHBRuleGroupExecutor.execute(input);
		sa.ifPresent(r -> {
			System.out.println(r.toString());
		});
		Assert.assertTrue("Se espera error CODIGO_ERROR_DAT_ANT_VALOR_HB_FECHAS_FORMATO_INCORRECTO",
				sa.get().get(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_VALOR_HB).hasValidationResultByCode(
						ValidationResultCatalogConstants.CODIGO_ERROR_DAT_ANT_VALOR_HB_FECHAS_FORMATO_INCORRECTO)
						.isPresent());
	}
	
	@Test
	public void whenEdadMenor000600Not_Undefined_thenError() {
		RdacaaRawRow input = new RdacaaRawRow();
		RowItem fechaNac = new RowItem();
		fechaNac.setItemValue("2018-06-01");
		fechaNac.setExcludedFromValidation(false);
		input.addField(RdacaaVariableKeyCatalog.PERSONA_FECHA_NACIMIENTO, fechaNac);
		RowItem fechaAten = new RowItem();
		fechaAten.setItemValue("2018-11-11");
		fechaAten.setExcludedFromValidation(false);
		input.addField(RdacaaVariableKeyCatalog.ATENCION_FECHA, fechaAten);
		RowItem valorHB = new RowItem();
		valorHB.setItemValue("20");
		valorHB.setExcludedFromValidation(false);
		valorHB.setMandatory(false);
		input.addField(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_VALOR_HB, valorHB);

		Optional<RdacaaRawRowResult> sa = valorHBRuleGroupExecutor.execute(input);
		sa.ifPresent(r -> {
			System.out.println(r.toString());
		});
		Assert.assertTrue("Se espera error CODIGO_ERROR_DAT_ANT_VALOR_HB_NO_ES_MANDATORIA_MENOR_000600",
				sa.get().get(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_VALOR_HB).hasValidationResultByCode(
						ValidationResultCatalogConstants.CODIGO_ERROR_DAT_ANT_VALOR_HB_NO_ES_MANDATORIA_MENOR_000600)
						.isPresent());
	}
	
	@Test
	public void whenEdadMayor000600NoNumerico_thenError() {
		RdacaaRawRow input = new RdacaaRawRow();
		RowItem fechaNac = new RowItem();
		fechaNac.setItemValue("2018-03-01");
		fechaNac.setExcludedFromValidation(false);
		input.addField(RdacaaVariableKeyCatalog.PERSONA_FECHA_NACIMIENTO, fechaNac);
		RowItem fechaAten = new RowItem();
		fechaAten.setItemValue("2018-11-11");
		fechaAten.setExcludedFromValidation(false);
		input.addField(RdacaaVariableKeyCatalog.ATENCION_FECHA, fechaAten);
		RowItem valorHB = new RowItem();
		valorHB.setItemValue("as");
		valorHB.setExcludedFromValidation(false);
		valorHB.setMandatory(false);
		input.addField(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_VALOR_HB, valorHB);

		Optional<RdacaaRawRowResult> sa = valorHBRuleGroupExecutor.execute(input);
		sa.ifPresent(r -> {
			System.out.println(r.toString());
		});
		Assert.assertTrue("Se espera error CODIGO_ERROR_DAT_ANT_VALOR_HB_NO_ES_NUMERO_VALIDO",
				sa.get().get(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_VALOR_HB).hasValidationResultByCode(
						ValidationResultCatalogConstants.CODIGO_ERROR_DAT_ANT_VALOR_HB_NO_ES_NUMERO_VALIDO)
						.isPresent());
	}

	@Test
	public void whenEdadMayor000600FueraDeRango_thenError() {
		RdacaaRawRow input = new RdacaaRawRow();
		RowItem fechaNac = new RowItem();
		fechaNac.setItemValue("2018-03-01");
		fechaNac.setExcludedFromValidation(false);
		input.addField(RdacaaVariableKeyCatalog.PERSONA_FECHA_NACIMIENTO, fechaNac);
		RowItem fechaAten = new RowItem();
		fechaAten.setItemValue("2018-11-11");
		fechaAten.setExcludedFromValidation(false);
		input.addField(RdacaaVariableKeyCatalog.ATENCION_FECHA, fechaAten);
		RowItem valorHB = new RowItem();
		valorHB.setItemValue("35");
		valorHB.setExcludedFromValidation(false);
		valorHB.setMandatory(false);
		input.addField(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_VALOR_HB, valorHB);

		Optional<RdacaaRawRowResult> sa = valorHBRuleGroupExecutor.execute(input);
		sa.ifPresent(r -> {
			System.out.println(r.toString());
		});
		Assert.assertTrue("Se espera error CODIGO_ERROR_DAT_ANT_VALOR_HB_FUERA_RANGO",
				sa.get().get(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_VALOR_HB).hasValidationResultByCode(
						ValidationResultCatalogConstants.CODIGO_ERROR_DAT_ANT_VALOR_HB_FUERA_RANGO)
						.isPresent());
	}
}

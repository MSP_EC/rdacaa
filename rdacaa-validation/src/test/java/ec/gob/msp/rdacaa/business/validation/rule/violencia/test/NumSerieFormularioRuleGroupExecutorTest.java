/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.gob.msp.rdacaa.business.validation.rule.violencia.test;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaRawRow;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaRawRowResult;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.malla.RowItem;
import ec.gob.msp.rdacaa.business.validation.rule.atencion.violencia.numformulario.NumFormularioRuleGroupExecutor;
import ec.gob.msp.rdacaa.test.ValidationTestContext;
import java.util.Optional;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

/**
 *
 * @author eduardo
 */
@RunWith(SpringRunner.class)
@ContextConfiguration(classes = ValidationTestContext.class)
public class NumSerieFormularioRuleGroupExecutorTest {
    
    @Autowired
    private NumFormularioRuleGroupExecutor numFormularioRuleGroupExecutor;
    
    @Test
    public void whenNumSerieFormularioIsNull_thenError() {
        RdacaaRawRow input = new RdacaaRawRow();
        RowItem ritem = new RowItem();
        ritem.setItemValue(null);
        ritem.setExcludedFromValidation(false);
        input.addField(RdacaaVariableKeyCatalog.VIOLENCIA_NUMERO_SERIE, ritem);
        Optional<RdacaaRawRowResult> sa = numFormularioRuleGroupExecutor.execute(input);
        sa.ifPresent(r -> {
            System.out.println(r.toString());
        });
        Assert.assertTrue("El numero de serie de formulario es obligatorio",
                sa.get().get(RdacaaVariableKeyCatalog.VIOLENCIA_NUMERO_SERIE)
                        .hasValidationResultByCode(ValidationResultCatalogConstants.CODIGO_ERROR_VIOLENCIA_NUMFORMULARIO_NULO)
                        .isPresent());
    }
    
    @Test
    public void whenNumSerieFormularioIsNotNumeric_thenError() {
        RdacaaRawRow input = new RdacaaRawRow();
        RowItem ritem = new RowItem();
        ritem.setItemValue("sdf");
        ritem.setExcludedFromValidation(false);
        input.addField(RdacaaVariableKeyCatalog.VIOLENCIA_NUMERO_SERIE, ritem);
        Optional<RdacaaRawRowResult> sa = numFormularioRuleGroupExecutor.execute(input);
        sa.ifPresent(r -> {
            System.out.println(r.toString());
        });
        Assert.assertTrue("Se espera un error para caracteres no numericos",
                sa.get().get(RdacaaVariableKeyCatalog.VIOLENCIA_NUMERO_SERIE)
                        .hasValidationResultByCode(ValidationResultCatalogConstants.CODIGO_ERROR_VIOLENCIA_NUMFORMULARIO_NONUMERICO)
                        .isPresent());
    }
    
    @Test
    public void whenNumSerieFormularioIsNotCorrect_thenError() {
        RdacaaRawRow input = new RdacaaRawRow();
        RowItem ritem = new RowItem();
        ritem.setItemValue("1000000");
        ritem.setExcludedFromValidation(false);
        input.addField(RdacaaVariableKeyCatalog.VIOLENCIA_NUMERO_SERIE, ritem);
        Optional<RdacaaRawRowResult> sa = numFormularioRuleGroupExecutor.execute(input);
        sa.ifPresent(r -> {
            System.out.println(r.toString());
        });
        Assert.assertTrue("Se espera un error para Numero de Serie Formulario incorrecto",
                sa.get().get(RdacaaVariableKeyCatalog.VIOLENCIA_NUMERO_SERIE)
                        .hasValidationResultByCode(ValidationResultCatalogConstants.CODIGO_ERROR_VIOLENCIA_NUMFORMULARIO_LONGITUD_INCORRECTA)
                        .isPresent());
    }
    
    @Test
    public void whenNumSerieFormularioIsCorrect_thenOk() {
        RdacaaRawRow input = new RdacaaRawRow();
        RowItem ritem = new RowItem();
        ritem.setItemValue("7");
        ritem.setExcludedFromValidation(false);
        input.addField(RdacaaVariableKeyCatalog.VIOLENCIA_NUMERO_SERIE, ritem);
        Optional<RdacaaRawRowResult> sa = numFormularioRuleGroupExecutor.execute(input);
        sa.ifPresent(r -> {
            System.out.println(r.toString());
        });
        Assert.assertTrue("No se esperan errores para Numero de Serfie Formulario correcto",
                sa.get().get(RdacaaVariableKeyCatalog.VIOLENCIA_NUMERO_SERIE).getValidationResultList().isEmpty());
    }
}

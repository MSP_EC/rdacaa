package ec.gob.msp.rdacaa.business.validation.rule.test;

import java.util.Optional;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaRawRow;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaRawRowResult;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.malla.RowItem;
import ec.gob.msp.rdacaa.business.validation.rule.persona.fechanacimiento.FechaNacimientoRuleGroupExecutor;
import ec.gob.msp.rdacaa.test.ValidationTestContext;

/**
*
* @author dmurillo
*/
@RunWith(SpringRunner.class)
@ContextConfiguration(classes = ValidationTestContext.class)
public class FechaNacimientoRuleGroupExecutorTest {
	
	@Autowired
	private FechaNacimientoRuleGroupExecutor fechaNacimientoRuleGroupExecutor;
	
	@Test
    public void whenFechaNacimientoIsExcludedAndValorIsNull(){
    	RdacaaRawRow input = new RdacaaRawRow();
    	RowItem ritem = new RowItem();
    	ritem.setItemValue("999990000066666");
    	ritem.setExcludedFromValidation(true);
    	input.addField(RdacaaVariableKeyCatalog.PERSONA_FECHA_NACIMIENTO, ritem);
        Optional<RdacaaRawRowResult> sa = fechaNacimientoRuleGroupExecutor.execute(input);
        sa.ifPresent(r -> {
            System.out.println(r.toString());
        });
        Assert.assertTrue("FechaNacimiento debe ser nulo",
                sa.get().get(RdacaaVariableKeyCatalog.PERSONA_FECHA_NACIMIENTO).getValidationResultList().isEmpty());
    }
    
    @Test
    public void whenFechaNacimientoIsExcludedAndValorIsNotNull(){
    	RdacaaRawRow input = new RdacaaRawRow();
    	RowItem ritem = new RowItem();
    	ritem.setItemValue("2018-09-01");
    	ritem.setExcludedFromValidation(true);
    	input.addField(RdacaaVariableKeyCatalog.PERSONA_FECHA_NACIMIENTO, ritem);
        Optional<RdacaaRawRowResult> sa = fechaNacimientoRuleGroupExecutor.execute(input);
        sa.ifPresent(r -> {
            System.out.println(r.toString());
        });
        Assert.assertTrue("FechaNacimiento debe ser nulo",
                sa.get().get(RdacaaVariableKeyCatalog.PERSONA_FECHA_NACIMIENTO)
                        .hasValidationResultByCode(ValidationResultCatalogConstants.CODIGO_ERROR_FECHANACIMIENTO_NO_DEFINIDA)
                        .isPresent());
    }
    
    @Test
    public void whenFechaNacimientoIsNotExcludedAndValorIsNotNull(){
    	RdacaaRawRow input = new RdacaaRawRow();
    	RowItem ritem = new RowItem();
    	ritem.setItemValue("2018-09-01");
    	ritem.setExcludedFromValidation(false);
    	input.addField(RdacaaVariableKeyCatalog.PERSONA_FECHA_NACIMIENTO, ritem);
        Optional<RdacaaRawRowResult> sa = fechaNacimientoRuleGroupExecutor.execute(input);
        sa.ifPresent(r -> {
            System.out.println(r.toString());
        });
        Assert.assertTrue("FechaNacimiento debe ser nulo",
                sa.get().get(RdacaaVariableKeyCatalog.PERSONA_FECHA_NACIMIENTO).getValidationResultList().isEmpty());
    }

	
	@Test
    public void whenFechanacimientoIsNull_thenError() {
        RdacaaRawRow input = new RdacaaRawRow();
        RowItem ritem = new RowItem();
        ritem.setItemValue(null);
        ritem.setExcludedFromValidation(false);
        input.addField(RdacaaVariableKeyCatalog.PERSONA_FECHA_NACIMIENTO, ritem);
        Optional<RdacaaRawRowResult> sa = fechaNacimientoRuleGroupExecutor.execute(input);
        sa.ifPresent(r -> {
            System.out.println(r.toString());
        });
        Assert.assertTrue("El fecha de nacimiento es obligatorio",
                sa.get().get(RdacaaVariableKeyCatalog.PERSONA_FECHA_NACIMIENTO)
                        .hasValidationResultByCode(ValidationResultCatalogConstants.CODIGO_ERROR_FECHANACIMIENTO_NULO)
                        .isPresent());
    }
	
	@Test
    public void whenFechanacimientoNotInRange_thenError() {
        RdacaaRawRow input = new RdacaaRawRow();
        RowItem ritem = new RowItem();
        ritem.setItemValue("2018-09-15");
        ritem.setExcludedFromValidation(false);
        RowItem ritemFA = new RowItem();
        ritemFA.setItemValue("2018-09-14");
        ritemFA.setExcludedFromValidation(false);
        input.addField(RdacaaVariableKeyCatalog.PERSONA_FECHA_NACIMIENTO, ritem);
        input.addField(RdacaaVariableKeyCatalog.ATENCION_FECHA, ritemFA);
        Optional<RdacaaRawRowResult> sa = fechaNacimientoRuleGroupExecutor.execute(input);
        sa.ifPresent(r -> {
            System.out.println(r.toString());
        });
        Assert.assertTrue("El fecha de nacimiento esta fuera del rango permitido",
                sa.get().get(RdacaaVariableKeyCatalog.PERSONA_FECHA_NACIMIENTO)
                        .hasValidationResultByCode(ValidationResultCatalogConstants.CODIGO_ERROR_FECHANACIMIENTO_FUERA_DE_RANGO)
                        .isPresent());
    }
	
	@Test
    public void whenFechanacimientoIsCorrect_thenOk() {
		
        RdacaaRawRow input = new RdacaaRawRow();
        RowItem ritem = new RowItem();
        ritem.setItemValue("2018-09-01");
        ritem.setExcludedFromValidation(false);
        RowItem ritemFA = new RowItem();
        ritemFA.setItemValue("2018-09-14");
        ritemFA.setExcludedFromValidation(false);
        input.addField(RdacaaVariableKeyCatalog.PERSONA_FECHA_NACIMIENTO, ritem);
        input.addField(RdacaaVariableKeyCatalog.ATENCION_FECHA, ritemFA);
        Optional<RdacaaRawRowResult> sa = fechaNacimientoRuleGroupExecutor.execute(input);
        sa.ifPresent(r -> {
            System.out.println(r.toString());
        });
        Assert.assertTrue("El fecha de nacimiento esta dentro del rango permitido",
                sa.get().get(RdacaaVariableKeyCatalog.PERSONA_FECHA_NACIMIENTO).getValidationResultList().isEmpty());
    }
}

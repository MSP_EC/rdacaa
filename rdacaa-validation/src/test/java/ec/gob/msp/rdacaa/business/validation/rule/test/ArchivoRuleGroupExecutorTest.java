/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.gob.msp.rdacaa.business.validation.rule.test;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaRawRow;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaRawRowResult;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.malla.RowItem;
import ec.gob.msp.rdacaa.business.validation.rule.persona.archivo.ArchivoRuleGroupExecutor;
import ec.gob.msp.rdacaa.test.ValidationTestContext;
import java.util.Optional;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

/**
 *
 * @author eduardo
 */
@RunWith(SpringRunner.class)
@ContextConfiguration(classes = ValidationTestContext.class)
public class ArchivoRuleGroupExecutorTest {
    @Autowired
    private ArchivoRuleGroupExecutor archivoRuleGroupExecutor;
    
    @Test
    public void whenArchivoIsNull_thenError() {
        RdacaaRawRow input = new RdacaaRawRow();
        RowItem ritem = new RowItem();
        ritem.setItemValue(null);
        ritem.setExcludedFromValidation(false);
        input.addField(RdacaaVariableKeyCatalog.PERSONA_NUMERO_ARCHIVO, ritem);
        Optional<RdacaaRawRowResult> sa = archivoRuleGroupExecutor.execute(input);
        sa.ifPresent(r -> {
            System.out.println(r.toString());
        });
        Assert.assertTrue("El numero de archivo es obligatorio",
                sa.get().get(RdacaaVariableKeyCatalog.PERSONA_NUMERO_ARCHIVO)
                        .hasValidationResultByCode(ValidationResultCatalogConstants.CODIGO_ERROR_ARCHIVO_NULO)
                        .isPresent());
    }
    
    @Test
    public void whenArchivoIsNotNumeric_thenError() {
        RdacaaRawRow input = new RdacaaRawRow();
        RowItem ritem = new RowItem();
        ritem.setItemValue("2h4");
        ritem.setExcludedFromValidation(false);
        input.addField(RdacaaVariableKeyCatalog.PERSONA_NUMERO_ARCHIVO, ritem);
        Optional<RdacaaRawRowResult> sa = archivoRuleGroupExecutor.execute(input);
        sa.ifPresent(r -> {
            System.out.println(r.toString());
        });
        Assert.assertTrue("El numero de archivo no es numerico",
                sa.get().get(RdacaaVariableKeyCatalog.PERSONA_NUMERO_ARCHIVO)
                        .hasValidationResultByCode(ValidationResultCatalogConstants.CODIGO_ERROR_ARCHIVO_NUMERICO)
                        .isPresent());
    }
    
    @Test
    public void whenArchivoLongitudIncorrect_thenError() {
		RdacaaRawRow input = new RdacaaRawRow();
		RowItem ritem = new RowItem();
		ritem.setItemValue("11122");
		ritem.setExcludedFromValidation(false);
        input.addField(RdacaaVariableKeyCatalog.PERSONA_NUMERO_ARCHIVO, ritem);
        Optional<RdacaaRawRowResult> sa = archivoRuleGroupExecutor.execute(input);
        sa.ifPresent(r -> {
            System.out.println(r.toString());
        });
        Assert.assertTrue("Se espera un error longitud incorrecta",
                sa.get().get(RdacaaVariableKeyCatalog.PERSONA_NUMERO_ARCHIVO)
                        .hasValidationResultByCode(ValidationResultCatalogConstants.CODIGO_ERROR_ARCHIVO_LONGITUD_INCORRECTA)
                        .isPresent());
	}
    
    @Test
    public void whenArchivoLongitudIsCorrect_thenOk() {
		RdacaaRawRow input = new RdacaaRawRow();
		RowItem ritem = new RowItem();
		ritem.setItemValue("11122639");
		ritem.setExcludedFromValidation(false);
        input.addField(RdacaaVariableKeyCatalog.PERSONA_NUMERO_ARCHIVO, ritem);
        Optional<RdacaaRawRowResult> sa = archivoRuleGroupExecutor.execute(input);
        sa.ifPresent(r -> {
            System.out.println(r.toString());
        });
        Assert.assertTrue("No se esperan errores",
                sa.get().get(RdacaaVariableKeyCatalog.PERSONA_NUMERO_ARCHIVO).getValidationResultList().isEmpty());
	}
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.gob.msp.rdacaa.business.validation.rule.test;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaRawRow;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaRawRowResult;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.malla.RowItem;
import ec.gob.msp.rdacaa.business.validation.rule.persona.tipoidentificacionrep.TipoIdentificacionRepRuleGroupExecutor;
import ec.gob.msp.rdacaa.test.ValidationTestContext;
import java.util.Optional;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

/**
 *
 * @author eduardo
 */
@RunWith(SpringRunner.class)
@ContextConfiguration(classes = ValidationTestContext.class)
public class TipoIdentificacionRepRuleGroupExecutorTest {
    @Autowired
    private TipoIdentificacionRepRuleGroupExecutor tipoIdentificacionRepRuleGroupExecutor;
    
    @Test
    public void whenTipoIdentificacionIsNull_thenError() {
        RdacaaRawRow input = new RdacaaRawRow();
        RowItem ritem = new RowItem();
        ritem.setItemValue(null);
        ritem.setExcludedFromValidation(false);
        input.addField(RdacaaVariableKeyCatalog.PERSONA_CODIGO_TIPO_IDENTIFICACION_REPRESENTANTE, ritem);
        Optional<RdacaaRawRowResult> sa = tipoIdentificacionRepRuleGroupExecutor.execute(input);
        sa.ifPresent(r -> {
            System.out.println(r.toString());
        });
        Assert.assertTrue("El Tipo de Identificacion es obligatorio",
                sa.get().get(RdacaaVariableKeyCatalog.PERSONA_CODIGO_TIPO_IDENTIFICACION_REPRESENTANTE)
                        .hasValidationResultByCode(ValidationResultCatalogConstants.CODIGO_ERROR_TIPO_IDENTIFICACION_REP_NULO)
                        .isPresent());
    }

    @Test
    public void whenTipoIdentificacionIsNotNumeric_thenError() {
        RdacaaRawRow input = new RdacaaRawRow();
        RowItem ritem = new RowItem();
        ritem.setItemValue("REDACAA");
        ritem.setExcludedFromValidation(false);
        input.addField(RdacaaVariableKeyCatalog.PERSONA_CODIGO_TIPO_IDENTIFICACION_REPRESENTANTE, ritem);
        Optional<RdacaaRawRowResult> sa = tipoIdentificacionRepRuleGroupExecutor.execute(input);
        sa.ifPresent(r -> {
            System.out.println(r.toString());
        });
        Assert.assertTrue("Se espera un error para caracteres no numericos ",
                sa.get().get(RdacaaVariableKeyCatalog.PERSONA_CODIGO_TIPO_IDENTIFICACION_REPRESENTANTE)
                        .hasValidationResultByCode(ValidationResultCatalogConstants.CODIGO_ERROR_TIPO_IDENTIFICACION_REP_NUMERICA)
                        .isPresent());
    }
    
    @Test
    public void whenTipoIdentificacionIsNotCorrect_thenError() {
        RdacaaRawRow input = new RdacaaRawRow();
        RowItem ritem = new RowItem();
        ritem.setItemValue("100");
        ritem.setExcludedFromValidation(false);
        input.addField(RdacaaVariableKeyCatalog.PERSONA_CODIGO_TIPO_IDENTIFICACION_REPRESENTANTE, ritem);
        Optional<RdacaaRawRowResult> sa = tipoIdentificacionRepRuleGroupExecutor.execute(input);
        sa.ifPresent(r -> {
            System.out.println(r.toString());
        });
        Assert.assertTrue("Se espera un error para codigo incorrecto ",
                sa.get().get(RdacaaVariableKeyCatalog.PERSONA_CODIGO_TIPO_IDENTIFICACION_REPRESENTANTE)
                        .hasValidationResultByCode(ValidationResultCatalogConstants.CODIGO_ERROR_TIPO_IDENTIFICACION_REP_INCORRECTO)
                        .isPresent());
    }
    
    @Test
    public void whenTipoIdentificacionIsCorrect_thenOk() {
        RdacaaRawRow input = new RdacaaRawRow();
        RowItem ritem = new RowItem();
        ritem.setItemValue("7");
        ritem.setExcludedFromValidation(false);
        input.addField(RdacaaVariableKeyCatalog.PERSONA_CODIGO_TIPO_IDENTIFICACION_REPRESENTANTE, ritem);
        Optional<RdacaaRawRowResult> sa = tipoIdentificacionRepRuleGroupExecutor.execute(input);
        sa.ifPresent(r -> {
            System.out.println(r.toString());
        });
        Assert.assertTrue("No se esperan errores para Tipo de Identificacion correcto",
                sa.get().get(RdacaaVariableKeyCatalog.PERSONA_CODIGO_TIPO_IDENTIFICACION_REPRESENTANTE).getValidationResultList().isEmpty());
    }
}

package ec.gob.msp.rdacaa.business.validation.rule.test;

import java.util.Optional;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import ec.gob.msp.rdacaa.business.service.ConstantesDetalleCatalogo;
import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaRawRow;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaRawRowResult;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.malla.RowItem;
import ec.gob.msp.rdacaa.business.validation.rule.atencion.cie10.numcertificadomedico.NumeroCertificadoMedicoRuleGroupExecutor;
import ec.gob.msp.rdacaa.test.ValidationTestContext;

/**
 *
 * @author miguel.faubla
 */
@RunWith(SpringRunner.class)
@ContextConfiguration(classes = ValidationTestContext.class)
public class NumCertificadoMedicoExecutorTest {

	@Autowired
	private NumeroCertificadoMedicoRuleGroupExecutor numeroCertificadoMedicoRuleGroupExecutor;

	@Test
	public void whenNumeroCertificadoMedicoIsExcludedAndValorIsNull() {
		RdacaaRawRow input = new RdacaaRawRow();
		RowItem numerocertificadomedico = new RowItem();
		numerocertificadomedico.setItemValue(ConstantesDetalleCatalogo.NOT_DEFINED_VALUE);
		numerocertificadomedico.setExcludedFromValidation(true);
		input.addField(RdacaaVariableKeyCatalog.DIAGNOSTICO_NUMERO_CERTIFICADO_MEDICO_UNICO, numerocertificadomedico);
		Optional<RdacaaRawRowResult> sa = numeroCertificadoMedicoRuleGroupExecutor.execute(input);
		sa.ifPresent(r -> {
			System.out.println(r.toString());
		});
		Assert.assertTrue("El numero de certificado medico debe ser nulo",
				sa.get().get(RdacaaVariableKeyCatalog.DIAGNOSTICO_NUMERO_CERTIFICADO_MEDICO_UNICO)
						.getValidationResultList().isEmpty());
	}

	@Test
	public void whenNumeroCertificadoMedicoIsExcludedAndValorIsNotNull() {
		RdacaaRawRow input = new RdacaaRawRow();
		RowItem numerocertificadomedico = new RowItem();
		numerocertificadomedico.setItemValue("24578200");
		numerocertificadomedico.setExcludedFromValidation(true);

		input.addField(RdacaaVariableKeyCatalog.DIAGNOSTICO_NUMERO_CERTIFICADO_MEDICO_UNICO, numerocertificadomedico);
		Optional<RdacaaRawRowResult> sa = numeroCertificadoMedicoRuleGroupExecutor.execute(input);
		sa.ifPresent(r -> {
			System.out.println(r.toString());
		});
		Assert.assertTrue("El numero de certificado medico debe ser nulo",
				sa.get().get(RdacaaVariableKeyCatalog.DIAGNOSTICO_NUMERO_CERTIFICADO_MEDICO_UNICO)
						.hasValidationResultByCode(
								ValidationResultCatalogConstants.CODIGO_ERROR_NUMERO_CERTIFICADO_MEDICO_NO_DEFINIDA)
						.isPresent());
	}

	@Test
	public void whenNumeroCertificadoMedicoIsNotExcludedAndValorIsNotNull() {
		RdacaaRawRow input = new RdacaaRawRow();
		RowItem codigocie10_1 = new RowItem();
		codigocie10_1.setItemValue("Z027"); // Certificado medico unico
		codigocie10_1.setExcludedFromValidation(false);
		RowItem codigocie10_2 = new RowItem();
		codigocie10_2.setItemValue("A009"); // Cólera, no especificado
		codigocie10_2.setExcludedFromValidation(false);
		RowItem codigocie10_3 = new RowItem();
		codigocie10_3.setItemValue("A011"); // Fiebre paratifoidea A
		codigocie10_3.setExcludedFromValidation(false);
		RowItem codigocie10_4 = new RowItem();
		codigocie10_4.setItemValue("B189"); // Hepatitis viral crónica, sin otra especificación
		codigocie10_4.setExcludedFromValidation(false);
		RowItem codigocie10_5 = new RowItem();
		codigocie10_5.setItemValue("B23"); // Enfermedad por virus de la inmunodeficiencia humana [VIH], resultante en
											// otras afecciones
		codigocie10_5.setExcludedFromValidation(false);
		RowItem numerocertificadomedico = new RowItem();
		numerocertificadomedico.setItemValue("24578200");
		numerocertificadomedico.setExcludedFromValidation(false);

		input.addField(RdacaaVariableKeyCatalog.DIAGNOSTICO_CODIGO_CIE_1, codigocie10_1);
		input.addField(RdacaaVariableKeyCatalog.DIAGNOSTICO_CODIGO_CIE_2, codigocie10_2);
		input.addField(RdacaaVariableKeyCatalog.DIAGNOSTICO_CODIGO_CIE_3, codigocie10_3);
		input.addField(RdacaaVariableKeyCatalog.DIAGNOSTICO_CODIGO_CIE_4, codigocie10_4);
		input.addField(RdacaaVariableKeyCatalog.DIAGNOSTICO_CODIGO_CIE_5, codigocie10_5);
		input.addField(RdacaaVariableKeyCatalog.DIAGNOSTICO_NUMERO_CERTIFICADO_MEDICO_UNICO, numerocertificadomedico);

		Optional<RdacaaRawRowResult> sa = numeroCertificadoMedicoRuleGroupExecutor.execute(input);
		sa.ifPresent(r -> {
			System.out.println(r.toString());
		});
		Assert.assertTrue("El numero de certificado medico no debe ser nulo",
				sa.get().get(RdacaaVariableKeyCatalog.DIAGNOSTICO_NUMERO_CERTIFICADO_MEDICO_UNICO)
						.getValidationResultList().isEmpty());
	}

	@Test
	public void whenNumeroCertificadoMedicoIsNull_thenError() {
		RdacaaRawRow input = new RdacaaRawRow();
		RowItem numerocertificadomedico = new RowItem();
		numerocertificadomedico.setItemValue(null);
		numerocertificadomedico.setExcludedFromValidation(false);

		input.addField(RdacaaVariableKeyCatalog.DIAGNOSTICO_NUMERO_CERTIFICADO_MEDICO_UNICO, numerocertificadomedico);
		Optional<RdacaaRawRowResult> sa = numeroCertificadoMedicoRuleGroupExecutor.execute(input);
		sa.ifPresent(r -> {
			System.out.println(r.toString());
		});
		Assert.assertTrue("El campo es obligatorio",
				sa.get().get(RdacaaVariableKeyCatalog.DIAGNOSTICO_NUMERO_CERTIFICADO_MEDICO_UNICO)
						.hasValidationResultByCode(
								ValidationResultCatalogConstants.CODIGO_ERROR_NUMERO_CERTIFICADO_MEDICO_NULO)
						.isPresent());
	}

	@Test
	public void whenNumeroCertificadoMedicoIsNotNumeric_thenError() {
		RdacaaRawRow input = new RdacaaRawRow();
		RowItem numerocertificadomedico = new RowItem();
		numerocertificadomedico.setItemValue("CERTICADOZ027");
		numerocertificadomedico.setExcludedFromValidation(false);
		input.addField(RdacaaVariableKeyCatalog.DIAGNOSTICO_NUMERO_CERTIFICADO_MEDICO_UNICO, numerocertificadomedico);
		Optional<RdacaaRawRowResult> sa = numeroCertificadoMedicoRuleGroupExecutor.execute(input);
		sa.ifPresent(r -> {
			System.out.println(r.toString());
		});
		Assert.assertTrue("Se espera un error para caracteres no numéricos ",
				sa.get().get(RdacaaVariableKeyCatalog.DIAGNOSTICO_NUMERO_CERTIFICADO_MEDICO_UNICO)
						.hasValidationResultByCode(
								ValidationResultCatalogConstants.CODIGO_ERROR_NUMERO_CERTIFICADO_MEDICO_NONUMERICO)
						.isPresent());
	}

	@Test
	public void whenNumeroCertificadoMedicoLongitudIncorrect_thenError() {
		RdacaaRawRow input = new RdacaaRawRow();
		RowItem numerocertificadomedico = new RowItem();
		numerocertificadomedico.setItemValue("555555555555");
		numerocertificadomedico.setExcludedFromValidation(false);

		input.addField(RdacaaVariableKeyCatalog.DIAGNOSTICO_NUMERO_CERTIFICADO_MEDICO_UNICO, numerocertificadomedico);
		Optional<RdacaaRawRowResult> sa = numeroCertificadoMedicoRuleGroupExecutor.execute(input);
		sa.ifPresent(r -> {
			System.out.println(r.toString());
		});
		Assert.assertTrue("Se espera un error longitud incorrecta", sa.get()
				.get(RdacaaVariableKeyCatalog.DIAGNOSTICO_NUMERO_CERTIFICADO_MEDICO_UNICO)
				.hasValidationResultByCode(
						ValidationResultCatalogConstants.CODIGO_ERROR_NUMERO_CERTIFICADO_MEDICO_LONGITUD_INCORRECTA)
				.isPresent());
	}

	@Test
	public void whenNumeroCertificadoMedicoIncorrect_thenError() {
		RdacaaRawRow input = new RdacaaRawRow();
		RowItem codigocie10_1 = new RowItem();
		codigocie10_1.setItemValue("B230"); // Síndrome de infección aguda debida a VIH
		codigocie10_1.setExcludedFromValidation(false);
		RowItem codigocie10_2 = new RowItem();
		codigocie10_2.setItemValue("A009"); // Cólera, no especificado
		codigocie10_2.setExcludedFromValidation(false);
		RowItem codigocie10_3 = new RowItem();
		codigocie10_3.setItemValue("A011"); // Fiebre paratifoidea A
		codigocie10_3.setExcludedFromValidation(false);
		RowItem codigocie10_4 = new RowItem();
		codigocie10_4.setItemValue("B189"); // Hepatitis viral crónica, sin otra especificación
		codigocie10_4.setExcludedFromValidation(false);
		RowItem codigocie10_5 = new RowItem();
		codigocie10_5.setItemValue("B23"); // Enfermedad por virus de la inmunodeficiencia humana [VIH], resultante en
											// otras afecciones
		codigocie10_5.setExcludedFromValidation(false);
		RowItem numerocertificadomedico = new RowItem();
		numerocertificadomedico.setItemValue("24578200");
		numerocertificadomedico.setExcludedFromValidation(false);

		input.addField(RdacaaVariableKeyCatalog.DIAGNOSTICO_CODIGO_CIE_1, codigocie10_1);
		input.addField(RdacaaVariableKeyCatalog.DIAGNOSTICO_CODIGO_CIE_2, codigocie10_2);
		input.addField(RdacaaVariableKeyCatalog.DIAGNOSTICO_CODIGO_CIE_3, codigocie10_3);
		input.addField(RdacaaVariableKeyCatalog.DIAGNOSTICO_CODIGO_CIE_4, codigocie10_4);
		input.addField(RdacaaVariableKeyCatalog.DIAGNOSTICO_CODIGO_CIE_5, codigocie10_5);
		input.addField(RdacaaVariableKeyCatalog.DIAGNOSTICO_NUMERO_CERTIFICADO_MEDICO_UNICO, numerocertificadomedico);
		Optional<RdacaaRawRowResult> sa = numeroCertificadoMedicoRuleGroupExecutor.execute(input);
		sa.ifPresent(r -> {
			System.out.println(r.toString());
		});
		Assert.assertTrue("Se espera un error certificado medico incorrecto",
				sa.get().get(RdacaaVariableKeyCatalog.DIAGNOSTICO_NUMERO_CERTIFICADO_MEDICO_UNICO)
						.hasValidationResultByCode(
								ValidationResultCatalogConstants.CODIGO_ERROR_NUMERO_CERTIFICADO_MEDICO_INCORRECTO)
						.isPresent());
	}

	@Test
	public void whenNumeroCertificadoMedicoIsCorrect_thenOk() {
		RdacaaRawRow input = new RdacaaRawRow();
		RowItem codigocie10_1 = new RowItem();
		codigocie10_1.setItemValue("Z027"); // Certificado medico unico
		codigocie10_1.setExcludedFromValidation(false);
		RowItem codigocie10_2 = new RowItem();
		codigocie10_2.setItemValue("A009"); // Cólera, no especificado
		codigocie10_2.setExcludedFromValidation(false);
		RowItem codigocie10_3 = new RowItem();
		codigocie10_3.setItemValue("A011"); // Fiebre paratifoidea A
		codigocie10_3.setExcludedFromValidation(false);
		RowItem codigocie10_4 = new RowItem();
		codigocie10_4.setItemValue("B189"); // Hepatitis viral crónica, sin otra especificación
		codigocie10_4.setExcludedFromValidation(false);
		RowItem codigocie10_5 = new RowItem();
		codigocie10_5.setItemValue("B23"); // Enfermedad por virus de la inmunodeficiencia humana [VIH], resultante en
											// otras afecciones
		codigocie10_5.setExcludedFromValidation(false);
		RowItem numerocertificadomedico = new RowItem();
		numerocertificadomedico.setItemValue("24578200");
		numerocertificadomedico.setExcludedFromValidation(false);

		input.addField(RdacaaVariableKeyCatalog.DIAGNOSTICO_CODIGO_CIE_1, codigocie10_1);
		input.addField(RdacaaVariableKeyCatalog.DIAGNOSTICO_CODIGO_CIE_2, codigocie10_2);
		input.addField(RdacaaVariableKeyCatalog.DIAGNOSTICO_CODIGO_CIE_3, codigocie10_3);
		input.addField(RdacaaVariableKeyCatalog.DIAGNOSTICO_CODIGO_CIE_4, codigocie10_4);
		input.addField(RdacaaVariableKeyCatalog.DIAGNOSTICO_CODIGO_CIE_5, codigocie10_5);
		input.addField(RdacaaVariableKeyCatalog.DIAGNOSTICO_NUMERO_CERTIFICADO_MEDICO_UNICO, numerocertificadomedico);

		Optional<RdacaaRawRowResult> sa = numeroCertificadoMedicoRuleGroupExecutor.execute(input);
		sa.ifPresent(r -> {
			System.out.println(r.toString());
		});
		Assert.assertTrue("No se esperan errores para certificado medico correcto",
				sa.get().get(RdacaaVariableKeyCatalog.DIAGNOSTICO_NUMERO_CERTIFICADO_MEDICO_UNICO)
						.getValidationResultList().isEmpty());
	}

	@Test
	public void whenNumeroCertificadoMedicoIsMandatory() {
		RdacaaRawRow input = new RdacaaRawRow();
		RowItem numerocertificadomedico = new RowItem();
		numerocertificadomedico.setItemValue(ConstantesDetalleCatalogo.NOT_DEFINED_VALUE);
		numerocertificadomedico.setExcludedFromValidation(false);
		numerocertificadomedico.setMandatory(true);

		input.addField(RdacaaVariableKeyCatalog.DIAGNOSTICO_NUMERO_CERTIFICADO_MEDICO_UNICO, numerocertificadomedico);
		Optional<RdacaaRawRowResult> sa = numeroCertificadoMedicoRuleGroupExecutor.execute(input);
		sa.ifPresent(r -> {
			System.out.println(r.toString());
		});
		Assert.assertTrue("El campo de certificado medico es mandatorio",
				sa.get().get(RdacaaVariableKeyCatalog.DIAGNOSTICO_NUMERO_CERTIFICADO_MEDICO_UNICO)
						.hasValidationResultByCode(
								ValidationResultCatalogConstants.CODIGO_ERROR_NUMERO_CERTIFICADO_MEDICO_ES_MANDATORIA)
						.isPresent());
	}

	@Test
	public void whenNumeroCertificadoMedicoIsMandatoryAndCorrectValue() {
		RdacaaRawRow input = new RdacaaRawRow();
		RowItem codigocie10_1 = new RowItem();
		codigocie10_1.setItemValue("Z027"); // Certificado medico unico
		codigocie10_1.setExcludedFromValidation(false);
		RowItem codigocie10_2 = new RowItem();
		codigocie10_2.setItemValue("A009"); // Cólera, no especificado
		codigocie10_2.setExcludedFromValidation(false);
		RowItem codigocie10_3 = new RowItem();
		codigocie10_3.setItemValue("A011"); // Fiebre paratifoidea A
		codigocie10_3.setExcludedFromValidation(false);
		RowItem codigocie10_4 = new RowItem();
		codigocie10_4.setItemValue("B189"); // Hepatitis viral crónica, sin otra especificación
		codigocie10_4.setExcludedFromValidation(false);
		RowItem codigocie10_5 = new RowItem();
		codigocie10_5.setItemValue("B23"); // Enfermedad por virus de la inmunodeficiencia humana [VIH], resultante en
											// otras afecciones
		codigocie10_5.setExcludedFromValidation(false);
		RowItem numerocertificadomedico = new RowItem();
		numerocertificadomedico.setItemValue("11231232");
		numerocertificadomedico.setMandatory(true);

		input.addField(RdacaaVariableKeyCatalog.DIAGNOSTICO_CODIGO_CIE_1, codigocie10_1);
		input.addField(RdacaaVariableKeyCatalog.DIAGNOSTICO_CODIGO_CIE_2, codigocie10_2);
		input.addField(RdacaaVariableKeyCatalog.DIAGNOSTICO_CODIGO_CIE_3, codigocie10_3);
		input.addField(RdacaaVariableKeyCatalog.DIAGNOSTICO_CODIGO_CIE_4, codigocie10_4);
		input.addField(RdacaaVariableKeyCatalog.DIAGNOSTICO_CODIGO_CIE_5, codigocie10_5);
		input.addField(RdacaaVariableKeyCatalog.DIAGNOSTICO_NUMERO_CERTIFICADO_MEDICO_UNICO, numerocertificadomedico);

		Optional<RdacaaRawRowResult> sa = numeroCertificadoMedicoRuleGroupExecutor.execute(input);
		sa.ifPresent(r -> {
			System.out.println(r.toString());
		});
		Assert.assertTrue("El certificado medico es mandatorio",
				sa.get().get(RdacaaVariableKeyCatalog.DIAGNOSTICO_NUMERO_CERTIFICADO_MEDICO_UNICO)
						.getValidationResultList().isEmpty());
	}

}

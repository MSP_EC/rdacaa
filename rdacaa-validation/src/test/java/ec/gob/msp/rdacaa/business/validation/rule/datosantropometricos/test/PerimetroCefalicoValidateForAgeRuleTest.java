package ec.gob.msp.rdacaa.business.validation.rule.datosantropometricos.test;

import java.util.Optional;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import com.deliveredtechnologies.rulebook.Fact;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaRawRow;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaRawRowResult;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.malla.RowItem;
import ec.gob.msp.rdacaa.business.validation.rule.atencion.datosantropometricos.categoriaperimetrocefalicoparaedad.CategoriaPerimetroCefalicoParaEdadRuleGroupExecutor;
import ec.gob.msp.rdacaa.business.validation.rule.atencion.datosantropometricos.perimetrocefalico.PerimetroCefalicoRuleGroupExecutor;
import ec.gob.msp.rdacaa.business.validation.rule.atencion.datosantropometricos.puntajezperimetrocefalicoparaedad.PuntajeZPerimetroCefalicoParaEdadRuleGroupExecutor;
import ec.gob.msp.rdacaa.test.ValidationTestContext;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = ValidationTestContext.class)
public class PerimetroCefalicoValidateForAgeRuleTest {

	@Autowired
	private PerimetroCefalicoRuleGroupExecutor perimetroCefalicoRuleGroupExecutor;
	
	@Autowired
	private PuntajeZPerimetroCefalicoParaEdadRuleGroupExecutor puntajeZPerimetroCefalicoParaEdadRuleGroupExecutor;
	
	@Autowired
	private CategoriaPerimetroCefalicoParaEdadRuleGroupExecutor categoriaPerimetroCefalicoParaEdadRuleGroupExecutor;

	@Test
	public void whenEdadMayorIgual50000NotDefined_thenNoError() {
		RdacaaRawRowResult resultTotal = new RdacaaRawRowResult();
		
		RdacaaRawRow input = new RdacaaRawRow();
		RowItem fechaNac = new RowItem();
		fechaNac.setItemValue("2013-11-01");
		fechaNac.setExcludedFromValidation(false);
		input.addField(RdacaaVariableKeyCatalog.PERSONA_FECHA_NACIMIENTO, fechaNac);
		RowItem fechaAten = new RowItem();
		fechaAten.setItemValue("2018-12-01");
		fechaAten.setExcludedFromValidation(false);
		input.addField(RdacaaVariableKeyCatalog.ATENCION_FECHA, fechaAten);
		RowItem perimetroCefalico = new RowItem();
		perimetroCefalico.setItemValue("999990000066666");
		perimetroCefalico.setExcludedFromValidation(false);
		perimetroCefalico.setMandatory(false);
		input.addField(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_PERIMETRO_CEFALICO, perimetroCefalico);

		Optional<RdacaaRawRowResult> sa1 = perimetroCefalicoRuleGroupExecutor.execute(input);
		sa1.ifPresent(r -> {
			System.out.println(r.toString());
		});
		
		resultTotal.putAll(sa1.get());

		Fact resultTotalFact1 = new Fact("rdacaa_row_result", resultTotal);
		
		// PZ PC para edad
		RowItem sexo = new RowItem();
		sexo.setItemValue("16");// hombre
		sexo.setExcludedFromValidation(false);
		input.addField(RdacaaVariableKeyCatalog.PERSONA_CODIGO_SEXO, sexo);
		
		RowItem pzPerimetroCefalico = new RowItem();
		pzPerimetroCefalico.setItemValue("999990000066666");
		pzPerimetroCefalico.setExcludedFromValidation(false);
		pzPerimetroCefalico.setMandatory(false);
		input.addField(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_PUNTAJE_Z_PERIMETRO_CEFALICO_PARA_EDAD, pzPerimetroCefalico);
		
		Optional<RdacaaRawRowResult> sa2 = puntajeZPerimetroCefalicoParaEdadRuleGroupExecutor.execute(input, resultTotalFact1);

		sa2.ifPresent(r -> {
			System.out.println(r.toString());
		});
		
		resultTotal.putAll(sa2.get());

		Fact resultTotalFact2 = new Fact("rdacaa_row_result", resultTotal);
		
		// Categoria PZ PC para edad
		
		RowItem catPzPerimetroCefalico = new RowItem();
		catPzPerimetroCefalico.setItemValue("999990000066666");
		catPzPerimetroCefalico.setExcludedFromValidation(false);
		catPzPerimetroCefalico.setMandatory(false);
		input.addField(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_CATEGORIA_PERIMETRO_CEFALICO_PARA_EDAD, catPzPerimetroCefalico);
		
		Optional<RdacaaRawRowResult> sa3 = categoriaPerimetroCefalicoParaEdadRuleGroupExecutor.execute(input, resultTotalFact2);

		sa3.ifPresent(r -> {
			System.out.println(r.toString());
		});
		
		Assert.assertTrue("El valor de prueba es correcto no debe existir errores",
				!sa1.get().get(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_PERIMETRO_CEFALICO).hasErrors());
	}

	@Test
	public void whenEdadMenor50000OK_thenNoError() {
		RdacaaRawRowResult resultTotal = new RdacaaRawRowResult();
		
		RdacaaRawRow input = new RdacaaRawRow();
		RowItem fechaNac = new RowItem();
		fechaNac.setItemValue("2018-11-01");
		fechaNac.setExcludedFromValidation(false);
		input.addField(RdacaaVariableKeyCatalog.PERSONA_FECHA_NACIMIENTO, fechaNac);
		RowItem fechaAten = new RowItem();
		fechaAten.setItemValue("2018-12-01");
		fechaAten.setExcludedFromValidation(false);
		input.addField(RdacaaVariableKeyCatalog.ATENCION_FECHA, fechaAten);
		RowItem perimetroCefalico = new RowItem();
		perimetroCefalico.setItemValue("30");
		perimetroCefalico.setExcludedFromValidation(false);
		perimetroCefalico.setMandatory(false);
		input.addField(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_PERIMETRO_CEFALICO, perimetroCefalico);

		Optional<RdacaaRawRowResult> sa1 = perimetroCefalicoRuleGroupExecutor.execute(input);
		sa1.ifPresent(r -> {
			System.out.println(r.toString());
		});
		
		resultTotal.putAll(sa1.get());

		Fact resultTotalFact1 = new Fact("rdacaa_row_result", resultTotal);
		
		// PZ PC para edad
		RowItem sexo = new RowItem();
		sexo.setItemValue("16");// hombre
		sexo.setExcludedFromValidation(false);
		input.addField(RdacaaVariableKeyCatalog.PERSONA_CODIGO_SEXO, sexo);
		
		RowItem pzPerimetroCefalico = new RowItem();
		pzPerimetroCefalico.setItemValue("999990000066666");
		pzPerimetroCefalico.setExcludedFromValidation(false);
		pzPerimetroCefalico.setMandatory(false);
		input.addField(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_PUNTAJE_Z_PERIMETRO_CEFALICO_PARA_EDAD, pzPerimetroCefalico);
		
		Optional<RdacaaRawRowResult> sa2 = puntajeZPerimetroCefalicoParaEdadRuleGroupExecutor.execute(input, resultTotalFact1);

		sa2.ifPresent(r -> {
			System.out.println(r.toString());
		});
		
		resultTotal.putAll(sa2.get());

		Fact resultTotalFact2 = new Fact("rdacaa_row_result", resultTotal);
		
		// Categoria PZ PC para edad
		
		RowItem catPzPerimetroCefalico = new RowItem();
		catPzPerimetroCefalico.setItemValue("999990000066666");
		catPzPerimetroCefalico.setExcludedFromValidation(false);
		catPzPerimetroCefalico.setMandatory(false);
		input.addField(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_CATEGORIA_PERIMETRO_CEFALICO_PARA_EDAD, catPzPerimetroCefalico);
		
		Optional<RdacaaRawRowResult> sa3 = categoriaPerimetroCefalicoParaEdadRuleGroupExecutor.execute(input, resultTotalFact2);

		sa3.ifPresent(r -> {
			System.out.println(r.toString());
		});
		
		Assert.assertTrue("El valor de prueba es correcto no debe existir errores",
				!sa1.get().get(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_PERIMETRO_CEFALICO).hasErrors());
	}

	@Test
	public void whenEdadMayorIgual50000FEchasInvalidas_thenError() {
		RdacaaRawRowResult resultTotal = new RdacaaRawRowResult();
		
		RdacaaRawRow input = new RdacaaRawRow();
		RowItem fechaNac = new RowItem();
		fechaNac.setItemValue("XXXX-11-01");
		fechaNac.setExcludedFromValidation(false);
		input.addField(RdacaaVariableKeyCatalog.PERSONA_FECHA_NACIMIENTO, fechaNac);
		RowItem fechaAten = new RowItem();
		fechaAten.setItemValue("2018-12-XX");
		fechaAten.setExcludedFromValidation(false);
		input.addField(RdacaaVariableKeyCatalog.ATENCION_FECHA, fechaAten);
		RowItem perimetroCefalico = new RowItem();
		perimetroCefalico.setItemValue("30");
		perimetroCefalico.setExcludedFromValidation(false);
		perimetroCefalico.setMandatory(false);
		input.addField(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_PERIMETRO_CEFALICO, perimetroCefalico);

		Optional<RdacaaRawRowResult> sa1 = perimetroCefalicoRuleGroupExecutor.execute(input);
		sa1.ifPresent(r -> {
			System.out.println(r.toString());
		});
		
		resultTotal.putAll(sa1.get());

		Fact resultTotalFact1 = new Fact("rdacaa_row_result", resultTotal);
		
		// PZ PC para edad
		RowItem sexo = new RowItem();
		sexo.setItemValue("16");// hombre
		sexo.setExcludedFromValidation(false);
		input.addField(RdacaaVariableKeyCatalog.PERSONA_CODIGO_SEXO, sexo);
		
		RowItem pzPerimetroCefalico = new RowItem();
		pzPerimetroCefalico.setItemValue("999990000066666");
		pzPerimetroCefalico.setExcludedFromValidation(false);
		pzPerimetroCefalico.setMandatory(false);
		input.addField(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_PUNTAJE_Z_PERIMETRO_CEFALICO_PARA_EDAD, pzPerimetroCefalico);
		
		Optional<RdacaaRawRowResult> sa2 = puntajeZPerimetroCefalicoParaEdadRuleGroupExecutor.execute(input, resultTotalFact1);

		sa2.ifPresent(r -> {
			System.out.println(r.toString());
		});
		
		resultTotal.putAll(sa2.get());

		Fact resultTotalFact2 = new Fact("rdacaa_row_result", resultTotal);
		
		// Categoria PZ PC para edad
		
		RowItem catPzPerimetroCefalico = new RowItem();
		catPzPerimetroCefalico.setItemValue("999990000066666");
		catPzPerimetroCefalico.setExcludedFromValidation(false);
		catPzPerimetroCefalico.setMandatory(false);
		input.addField(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_CATEGORIA_PERIMETRO_CEFALICO_PARA_EDAD, catPzPerimetroCefalico);
		
		Optional<RdacaaRawRowResult> sa3 = categoriaPerimetroCefalicoParaEdadRuleGroupExecutor.execute(input, resultTotalFact2);

		sa3.ifPresent(r -> {
			System.out.println(r.toString());
		});
		
		Assert.assertTrue("El valor de prueba es correcto no debe existir errores",
				sa1.get().get(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_PERIMETRO_CEFALICO).hasValidationResultByCode(
						ValidationResultCatalogConstants.CODIGO_ERROR_DAT_ANT_PER_CEFALICO_FECHAS_FORMATO_INCORRECTO)
						.isPresent());
	}

	@Test
	public void whenEdadMenor50000NoDefinido_thenError() {
		RdacaaRawRowResult resultTotal = new RdacaaRawRowResult();
		
		RdacaaRawRow input = new RdacaaRawRow();
		RowItem fechaNac = new RowItem();
		fechaNac.setItemValue("2018-11-01");
		fechaNac.setExcludedFromValidation(false);
		input.addField(RdacaaVariableKeyCatalog.PERSONA_FECHA_NACIMIENTO, fechaNac);
		RowItem fechaAten = new RowItem();
		fechaAten.setItemValue("2018-12-01");
		fechaAten.setExcludedFromValidation(false);
		input.addField(RdacaaVariableKeyCatalog.ATENCION_FECHA, fechaAten);
		RowItem perimetroCefalico = new RowItem();
		perimetroCefalico.setItemValue("999990000066666");
		perimetroCefalico.setExcludedFromValidation(false);
		perimetroCefalico.setMandatory(false);
		input.addField(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_PERIMETRO_CEFALICO, perimetroCefalico);

		Optional<RdacaaRawRowResult> sa1 = perimetroCefalicoRuleGroupExecutor.execute(input);
		sa1.ifPresent(r -> {
			System.out.println(r.toString());
		});
		
		resultTotal.putAll(sa1.get());

		Fact resultTotalFact1 = new Fact("rdacaa_row_result", resultTotal);
		
		// PZ PC para edad
		RowItem sexo = new RowItem();
		sexo.setItemValue("16");// hombre
		sexo.setExcludedFromValidation(false);
		input.addField(RdacaaVariableKeyCatalog.PERSONA_CODIGO_SEXO, sexo);
		
		RowItem pzPerimetroCefalico = new RowItem();
		pzPerimetroCefalico.setItemValue("999990000066666");
		pzPerimetroCefalico.setExcludedFromValidation(false);
		pzPerimetroCefalico.setMandatory(false);
		input.addField(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_PUNTAJE_Z_PERIMETRO_CEFALICO_PARA_EDAD, pzPerimetroCefalico);
		
		Optional<RdacaaRawRowResult> sa2 = puntajeZPerimetroCefalicoParaEdadRuleGroupExecutor.execute(input, resultTotalFact1);

		sa2.ifPresent(r -> {
			System.out.println(r.toString());
		});
		
		resultTotal.putAll(sa2.get());

		Fact resultTotalFact2 = new Fact("rdacaa_row_result", resultTotal);
		
		// Categoria PZ PC para edad
		
		RowItem catPzPerimetroCefalico = new RowItem();
		catPzPerimetroCefalico.setItemValue("999990000066666");
		catPzPerimetroCefalico.setExcludedFromValidation(false);
		catPzPerimetroCefalico.setMandatory(false);
		input.addField(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_CATEGORIA_PERIMETRO_CEFALICO_PARA_EDAD, catPzPerimetroCefalico);
		
		Optional<RdacaaRawRowResult> sa3 = categoriaPerimetroCefalicoParaEdadRuleGroupExecutor.execute(input, resultTotalFact2);

		sa3.ifPresent(r -> {
			System.out.println(r.toString());
		});
		
		Assert.assertTrue("Debe tener error CODIGO_ERROR_DAT_ANT_PER_CEFALICO_ES_MANDATORIA_MENOR_050100_ANIOS",
				sa1.get().get(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_PERIMETRO_CEFALICO).hasValidationResultByCode(
						ValidationResultCatalogConstants.CODIGO_ERROR_DAT_ANT_PER_CEFALICO_ES_MANDATORIA_MENOR_050100_ANIOS)
						.isPresent());
	}

	@Test
	public void whenEdadMayor50000ValorDebeSerNoDefinido_thenError() {
		RdacaaRawRowResult resultTotal = new RdacaaRawRowResult();
		
		RdacaaRawRow input = new RdacaaRawRow();
		RowItem fechaNac = new RowItem();
		fechaNac.setItemValue("2012-11-01");
		fechaNac.setExcludedFromValidation(false);
		input.addField(RdacaaVariableKeyCatalog.PERSONA_FECHA_NACIMIENTO, fechaNac);
		RowItem fechaAten = new RowItem();
		fechaAten.setItemValue("2018-12-01");
		fechaAten.setExcludedFromValidation(false);
		input.addField(RdacaaVariableKeyCatalog.ATENCION_FECHA, fechaAten);
		RowItem perimetroCefalico = new RowItem();
		perimetroCefalico.setItemValue("30");
		perimetroCefalico.setExcludedFromValidation(false);
		perimetroCefalico.setMandatory(false);
		input.addField(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_PERIMETRO_CEFALICO, perimetroCefalico);

		Optional<RdacaaRawRowResult> sa1 = perimetroCefalicoRuleGroupExecutor.execute(input);
		sa1.ifPresent(r -> {
			System.out.println(r.toString());
		});
		
		resultTotal.putAll(sa1.get());

		Fact resultTotalFact1 = new Fact("rdacaa_row_result", resultTotal);
		
		// PZ PC para edad
		RowItem sexo = new RowItem();
		sexo.setItemValue("16");// hombre
		sexo.setExcludedFromValidation(false);
		input.addField(RdacaaVariableKeyCatalog.PERSONA_CODIGO_SEXO, sexo);
		
		RowItem pzPerimetroCefalico = new RowItem();
		pzPerimetroCefalico.setItemValue("999990000066666");
		pzPerimetroCefalico.setExcludedFromValidation(false);
		pzPerimetroCefalico.setMandatory(false);
		input.addField(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_PUNTAJE_Z_PERIMETRO_CEFALICO_PARA_EDAD, pzPerimetroCefalico);
		
		Optional<RdacaaRawRowResult> sa2 = puntajeZPerimetroCefalicoParaEdadRuleGroupExecutor.execute(input, resultTotalFact1);

		sa2.ifPresent(r -> {
			System.out.println(r.toString());
		});
		
		resultTotal.putAll(sa2.get());

		Fact resultTotalFact2 = new Fact("rdacaa_row_result", resultTotal);
		
		// Categoria PZ PC para edad
		
		RowItem catPzPerimetroCefalico = new RowItem();
		catPzPerimetroCefalico.setItemValue("999990000066666");
		catPzPerimetroCefalico.setExcludedFromValidation(false);
		catPzPerimetroCefalico.setMandatory(false);
		input.addField(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_CATEGORIA_PERIMETRO_CEFALICO_PARA_EDAD, catPzPerimetroCefalico);
		
		Optional<RdacaaRawRowResult> sa3 = categoriaPerimetroCefalicoParaEdadRuleGroupExecutor.execute(input, resultTotalFact2);

		sa3.ifPresent(r -> {
			System.out.println(r.toString());
		});
		
		Assert.assertTrue("Debe tener error CODIGO_ERROR_DAT_ANT_PER_CEFALICO_ES_NO_DEFINIDO_MAYOR_050100_ANIOS",
				sa1.get().get(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_PERIMETRO_CEFALICO).hasValidationResultByCode(
						ValidationResultCatalogConstants.CODIGO_ERROR_DAT_ANT_PER_CEFALICO_ES_NO_DEFINIDO_MAYOR_050100_ANIOS)
						.isPresent());
	}

	@Test
	public void whenEdadMayorIgual50000ValorNoNumerico_thenError() {
		RdacaaRawRowResult resultTotal = new RdacaaRawRowResult();
		
		RdacaaRawRow input = new RdacaaRawRow();
		RowItem fechaNac = new RowItem();
		fechaNac.setItemValue("2018-11-01");
		fechaNac.setExcludedFromValidation(false);
		input.addField(RdacaaVariableKeyCatalog.PERSONA_FECHA_NACIMIENTO, fechaNac);
		RowItem fechaAten = new RowItem();
		fechaAten.setItemValue("2018-12-01");
		fechaAten.setExcludedFromValidation(false);
		input.addField(RdacaaVariableKeyCatalog.ATENCION_FECHA, fechaAten);
		RowItem perimetroCefalico = new RowItem();
		perimetroCefalico.setItemValue("as");
		perimetroCefalico.setExcludedFromValidation(false);
		perimetroCefalico.setMandatory(false);
		input.addField(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_PERIMETRO_CEFALICO, perimetroCefalico);

		Optional<RdacaaRawRowResult> sa1 = perimetroCefalicoRuleGroupExecutor.execute(input);
		sa1.ifPresent(r -> {
			System.out.println(r.toString());
		});
		
		resultTotal.putAll(sa1.get());

		Fact resultTotalFact1 = new Fact("rdacaa_row_result", resultTotal);
		
		// PZ PC para edad
		RowItem sexo = new RowItem();
		sexo.setItemValue("16");// hombre
		sexo.setExcludedFromValidation(false);
		input.addField(RdacaaVariableKeyCatalog.PERSONA_CODIGO_SEXO, sexo);
		
		RowItem pzPerimetroCefalico = new RowItem();
		pzPerimetroCefalico.setItemValue("999990000066666");
		pzPerimetroCefalico.setExcludedFromValidation(false);
		pzPerimetroCefalico.setMandatory(false);
		input.addField(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_PUNTAJE_Z_PERIMETRO_CEFALICO_PARA_EDAD, pzPerimetroCefalico);
		
		Optional<RdacaaRawRowResult> sa2 = puntajeZPerimetroCefalicoParaEdadRuleGroupExecutor.execute(input, resultTotalFact1);

		sa2.ifPresent(r -> {
			System.out.println(r.toString());
		});
		
		resultTotal.putAll(sa2.get());

		Fact resultTotalFact2 = new Fact("rdacaa_row_result", resultTotal);
		
		// Categoria PZ PC para edad
		
		RowItem catPzPerimetroCefalico = new RowItem();
		catPzPerimetroCefalico.setItemValue("999990000066666");
		catPzPerimetroCefalico.setExcludedFromValidation(false);
		catPzPerimetroCefalico.setMandatory(false);
		input.addField(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_CATEGORIA_PERIMETRO_CEFALICO_PARA_EDAD, catPzPerimetroCefalico);
		
		Optional<RdacaaRawRowResult> sa3 = categoriaPerimetroCefalicoParaEdadRuleGroupExecutor.execute(input, resultTotalFact2);

		sa3.ifPresent(r -> {
			System.out.println(r.toString());
		});
		
		Assert.assertTrue("Debe tener error CODIGO_ERROR_DAT_ANT_PER_CEFALICO_NO_ES_NUMERO_VALIDO",
				sa1.get().get(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_PERIMETRO_CEFALICO)
						.hasValidationResultByCode(
								ValidationResultCatalogConstants.CODIGO_ERROR_DAT_ANT_PER_CEFALICO_NO_ES_NUMERO_VALIDO)
						.isPresent());
	}
	
	@Test
	public void whenEdadMayorIgual50000ValorFueraRango_thenError() {
		RdacaaRawRowResult resultTotal = new RdacaaRawRowResult();
		
		RdacaaRawRow input = new RdacaaRawRow();
		RowItem fechaNac = new RowItem();
		fechaNac.setItemValue("2018-11-01");
		fechaNac.setExcludedFromValidation(false);
		input.addField(RdacaaVariableKeyCatalog.PERSONA_FECHA_NACIMIENTO, fechaNac);
		RowItem fechaAten = new RowItem();
		fechaAten.setItemValue("2018-12-01");
		fechaAten.setExcludedFromValidation(false);
		input.addField(RdacaaVariableKeyCatalog.ATENCION_FECHA, fechaAten);
		RowItem perimetroCefalico = new RowItem();
		perimetroCefalico.setItemValue("60");
		perimetroCefalico.setExcludedFromValidation(false);
		perimetroCefalico.setMandatory(false);
		input.addField(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_PERIMETRO_CEFALICO, perimetroCefalico);

		Optional<RdacaaRawRowResult> sa1 = perimetroCefalicoRuleGroupExecutor.execute(input);
		sa1.ifPresent(r -> {
			System.out.println(r.toString());
		});
		
		resultTotal.putAll(sa1.get());

		Fact resultTotalFact1 = new Fact("rdacaa_row_result", resultTotal);
		
		// PZ PC para edad
		RowItem sexo = new RowItem();
		sexo.setItemValue("16");// hombre
		sexo.setExcludedFromValidation(false);
		input.addField(RdacaaVariableKeyCatalog.PERSONA_CODIGO_SEXO, sexo);
		
		RowItem pzPerimetroCefalico = new RowItem();
		pzPerimetroCefalico.setItemValue("999990000066666");
		pzPerimetroCefalico.setExcludedFromValidation(false);
		pzPerimetroCefalico.setMandatory(false);
		input.addField(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_PUNTAJE_Z_PERIMETRO_CEFALICO_PARA_EDAD, pzPerimetroCefalico);
		
		Optional<RdacaaRawRowResult> sa2 = puntajeZPerimetroCefalicoParaEdadRuleGroupExecutor.execute(input, resultTotalFact1);

		sa2.ifPresent(r -> {
			System.out.println(r.toString());
		});
		
		resultTotal.putAll(sa2.get());

		Fact resultTotalFact2 = new Fact("rdacaa_row_result", resultTotal);
		
		// Categoria PZ PC para edad
		
		RowItem catPzPerimetroCefalico = new RowItem();
		catPzPerimetroCefalico.setItemValue("999990000066666");
		catPzPerimetroCefalico.setExcludedFromValidation(false);
		catPzPerimetroCefalico.setMandatory(false);
		input.addField(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_CATEGORIA_PERIMETRO_CEFALICO_PARA_EDAD, catPzPerimetroCefalico);
		
		Optional<RdacaaRawRowResult> sa3 = categoriaPerimetroCefalicoParaEdadRuleGroupExecutor.execute(input, resultTotalFact2);

		sa3.ifPresent(r -> {
			System.out.println(r.toString());
		});
		Assert.assertTrue("Debe tener error CODIGO_ERROR_DAT_ANT_PER_CEFALICO_FUERA_RANGO",
				sa1.get().get(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_PERIMETRO_CEFALICO)
						.hasValidationResultByCode(
								ValidationResultCatalogConstants.CODIGO_ERROR_DAT_ANT_PER_CEFALICO_FUERA_RANGO)
						.isPresent());
	}
}

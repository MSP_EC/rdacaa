package ec.gob.msp.rdacaa.business.validation.rule.test;

import java.util.Optional;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import ec.gob.msp.rdacaa.business.service.ConstantesDetalleCatalogo;
import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaRawRow;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaRawRowResult;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.malla.RowItem;
import ec.gob.msp.rdacaa.business.validation.rule.atencion.grupoprioritario.gpr_prioritario4.GrupoPrioritario4RuleGroupExecutor;
import ec.gob.msp.rdacaa.test.ValidationTestContext;

/**
 *
 * @author miguel.faubla
 */
@RunWith(SpringRunner.class)
@ContextConfiguration(classes = ValidationTestContext.class)
public class GrupoPrioritario4RuleGroupExecutorTest {

	@Autowired
	private GrupoPrioritario4RuleGroupExecutor grupoPrioritario4RuleGroupExecutor;

	@Test
	public void whenGrupoPrioritario4IsExcludedAndValorIsNull() {
		RdacaaRawRow input = new RdacaaRawRow();
		RowItem grupoprioritarioId = new RowItem();
		grupoprioritarioId.setItemValue(ConstantesDetalleCatalogo.NOT_DEFINED_VALUE);
		grupoprioritarioId.setExcludedFromValidation(true);
		input.addField(RdacaaVariableKeyCatalog.GRUPO_PRIORITARIO_4, grupoprioritarioId);
		Optional<RdacaaRawRowResult> sa = grupoPrioritario4RuleGroupExecutor.execute(input);
		sa.ifPresent(r -> {
			System.out.println(r.toString());
		});
		Assert.assertTrue("El Grupo Prioritario 4 debe ser nulo",
				sa.get().get(RdacaaVariableKeyCatalog.GRUPO_PRIORITARIO_4).getValidationResultList().isEmpty());
	}

	@Test
	public void whenGrupoPrioritario4IsExcludedAndValorIsNotNull() {
		RdacaaRawRow input = new RdacaaRawRow();
		RowItem grupoprioritarioId = new RowItem();
		grupoprioritarioId.setItemValue("643"); // Trabajador/A Sexual
		grupoprioritarioId.setExcludedFromValidation(true);
		input.addField(RdacaaVariableKeyCatalog.GRUPO_PRIORITARIO_4, grupoprioritarioId);
		Optional<RdacaaRawRowResult> sa = grupoPrioritario4RuleGroupExecutor.execute(input);
		sa.ifPresent(r -> {
			System.out.println(r.toString());
		});
		Assert.assertTrue("El Grupo Prioritario 4 no debe ser nulo",
				sa.get().get(RdacaaVariableKeyCatalog.GRUPO_PRIORITARIO_4)
						.hasValidationResultByCode(
								ValidationResultCatalogConstants.CODIGO_ERROR_GRUPO_PRIORITARIO_4_NO_DEFINIDA)
						.isPresent());
	}

	@Test
	public void whenGrupoPrioritario4IsNotExcludedAndValorIsNotNull() {
		RdacaaRawRow input = new RdacaaRawRow();
		RowItem itemSexo = new RowItem();
		itemSexo.setItemValue("16"); // Hombre
		itemSexo.setExcludedFromValidation(false);
		RowItem itemFechaNac = new RowItem();
		itemFechaNac.setItemValue("2016-09-01");
		itemFechaNac.setExcludedFromValidation(false);
		RowItem itemFechaAtenc = new RowItem();
		itemFechaAtenc.setItemValue("2018-09-14");
		itemFechaAtenc.setExcludedFromValidation(false);
		RowItem grupoprioritarioId = new RowItem();
		grupoprioritarioId.setItemValue("638"); // Maltrato Infantil
		grupoprioritarioId.setExcludedFromValidation(false);
		
		input.addField(RdacaaVariableKeyCatalog.PERSONA_CODIGO_SEXO, itemSexo);
		input.addField(RdacaaVariableKeyCatalog.PERSONA_FECHA_NACIMIENTO, itemFechaNac);
		input.addField(RdacaaVariableKeyCatalog.ATENCION_FECHA, itemFechaAtenc);
		input.addField(RdacaaVariableKeyCatalog.GRUPO_PRIORITARIO_4, grupoprioritarioId);
		Optional<RdacaaRawRowResult> sa = grupoPrioritario4RuleGroupExecutor.execute(input);
		sa.ifPresent(r -> {
			System.out.println(r.toString());
		});
		Assert.assertTrue("El Grupo Prioritario 4 no debe ser nulo",
				sa.get().get(RdacaaVariableKeyCatalog.GRUPO_PRIORITARIO_4).getValidationResultList().isEmpty());
	}

	@Test
	public void whenGrupoPrioritario4IsNull_thenError() {
		RdacaaRawRow input = new RdacaaRawRow();
		RowItem grupoprioritarioId = new RowItem();
		grupoprioritarioId.setItemValue(null);
		grupoprioritarioId.setExcludedFromValidation(false);
		input.addField(RdacaaVariableKeyCatalog.GRUPO_PRIORITARIO_4, grupoprioritarioId);
		Optional<RdacaaRawRowResult> sa = grupoPrioritario4RuleGroupExecutor.execute(input);
		sa.ifPresent(r -> {
			System.out.println(r.toString());
		});
		Assert.assertTrue("El campo es obligatorio", sa.get().get(RdacaaVariableKeyCatalog.GRUPO_PRIORITARIO_4)
				.hasValidationResultByCode(ValidationResultCatalogConstants.CODIGO_ERROR_GRUPO_PRIORITARIO_4_NULO)
				.isPresent());
	}

	@Test
	public void whenGrupoPrioritario4IsNotNumeric_thenError() {
		RdacaaRawRow input = new RdacaaRawRow();
		RowItem grupoprioritarioId = new RowItem();
		grupoprioritarioId.setItemValue("GRUPO PRIORITARIO 4");
		grupoprioritarioId.setExcludedFromValidation(false);
		input.addField(RdacaaVariableKeyCatalog.GRUPO_PRIORITARIO_4, grupoprioritarioId);
		Optional<RdacaaRawRowResult> sa = grupoPrioritario4RuleGroupExecutor.execute(input);
		sa.ifPresent(r -> {
			System.out.println(r.toString());
		});
		Assert.assertTrue("Se espera un error para caracteres no numéricos ",
				sa.get().get(RdacaaVariableKeyCatalog.GRUPO_PRIORITARIO_4)
						.hasValidationResultByCode(
								ValidationResultCatalogConstants.CODIGO_ERROR_GRUPO_PRIORITARIO_4_NONUMERICO)
						.isPresent());
	}

	@Test
	public void whenGrupoPrioritario4IsIncorrecto_thenError() {
		RdacaaRawRow input = new RdacaaRawRow();
		RowItem itemSexo = new RowItem();
		itemSexo.setItemValue("114");
		itemSexo.setExcludedFromValidation(false);
		RowItem itemGrupoPrio = new RowItem();
		itemGrupoPrio.setItemValue("621");
		itemGrupoPrio.setExcludedFromValidation(false);
		RowItem itemFechaNac = new RowItem();
		itemFechaNac.setItemValue("2015-09-01");
		itemFechaNac.setExcludedFromValidation(false);
		RowItem itemFechaAtenc = new RowItem();
		itemFechaAtenc.setItemValue("2018-09-14");
		itemFechaAtenc.setExcludedFromValidation(false);
		input.addField(RdacaaVariableKeyCatalog.PERSONA_CODIGO_SEXO, itemSexo);
		input.addField(RdacaaVariableKeyCatalog.PERSONA_FECHA_NACIMIENTO, itemFechaNac);
		input.addField(RdacaaVariableKeyCatalog.ATENCION_FECHA, itemFechaAtenc);
		input.addField(RdacaaVariableKeyCatalog.GRUPO_PRIORITARIO_4, itemGrupoPrio);
		Optional<RdacaaRawRowResult> sa = grupoPrioritario4RuleGroupExecutor.execute(input);
		sa.ifPresent(r -> {
			System.out.println(r.toString());
		});
		Assert.assertTrue("El campo es incorrecto",
				sa.get().get(RdacaaVariableKeyCatalog.GRUPO_PRIORITARIO_4)
						.hasValidationResultByCode(
								ValidationResultCatalogConstants.CODIGO_ERROR_GRUPO_PRIORITARIO_4_INCORRECTO)
						.isPresent());
	}

	@Test
	public void whenGrupoPrioritario4IsCorrect_thenOk() {
		RdacaaRawRow input = new RdacaaRawRow();
		RowItem itemSexo = new RowItem();
		itemSexo.setItemValue("17"); // Mujer
		itemSexo.setExcludedFromValidation(false);
		RowItem grupoprioritarioId = new RowItem();
		grupoprioritarioId.setItemValue("634"); // Embarazadas
		grupoprioritarioId.setExcludedFromValidation(false);
		RowItem itemFechaNac = new RowItem();
		itemFechaNac.setItemValue("2004-09-01");
		itemFechaNac.setExcludedFromValidation(false);
		RowItem itemFechaAtenc = new RowItem();
		itemFechaAtenc.setItemValue("2018-09-14");
		itemFechaAtenc.setExcludedFromValidation(false);
		
		input.addField(RdacaaVariableKeyCatalog.PERSONA_CODIGO_SEXO, itemSexo);
		input.addField(RdacaaVariableKeyCatalog.PERSONA_FECHA_NACIMIENTO, itemFechaNac);
		input.addField(RdacaaVariableKeyCatalog.ATENCION_FECHA, itemFechaAtenc);
		input.addField(RdacaaVariableKeyCatalog.GRUPO_PRIORITARIO_4, grupoprioritarioId);
		Optional<RdacaaRawRowResult> sa = grupoPrioritario4RuleGroupExecutor.execute(input);
		sa.ifPresent(r -> {
			System.out.println(r.toString());
		});
		Assert.assertTrue("No se esperan errores para Grupo Prioritario 4 correcto",
				sa.get().get(RdacaaVariableKeyCatalog.GRUPO_PRIORITARIO_4).getValidationResultList().isEmpty());
	}

	@Test
	public void whenGrupoPrioritario4IsMandatory() {
		RdacaaRawRow input = new RdacaaRawRow();
		RowItem grupoprioritarioId = new RowItem();
		grupoprioritarioId.setItemValue(ConstantesDetalleCatalogo.NOT_DEFINED_VALUE);
		grupoprioritarioId.setExcludedFromValidation(false);
		grupoprioritarioId.setMandatory(true);
		input.addField(RdacaaVariableKeyCatalog.GRUPO_PRIORITARIO_4, grupoprioritarioId);
		Optional<RdacaaRawRowResult> sa = grupoPrioritario4RuleGroupExecutor.execute(input);
		sa.ifPresent(r -> {
			System.out.println(r.toString());
		});
		Assert.assertTrue("El Grupo Prioritario 4 es mandatorio",
				sa.get().get(RdacaaVariableKeyCatalog.GRUPO_PRIORITARIO_4)
						.hasValidationResultByCode(
								ValidationResultCatalogConstants.CODIGO_ERROR_GRUPO_PRIORITARIO_4_ES_MANDATORIA)
						.isPresent());
	}

	@Test
	public void whenGrupoPrioritario4IsMadatoryAndCorrectValue() {
		RdacaaRawRow input = new RdacaaRawRow();
		RowItem itemSexo = new RowItem();
		itemSexo.setItemValue("17"); // MUjer
		itemSexo.setExcludedFromValidation(false);
		RowItem itemFechaNac = new RowItem();
		itemFechaNac.setItemValue("2002-09-01");
		itemFechaNac.setExcludedFromValidation(false);
		RowItem itemFechaAtenc = new RowItem();
		itemFechaAtenc.setItemValue("2018-09-14");
		itemFechaAtenc.setExcludedFromValidation(false);
		RowItem grupoprioritarioId = new RowItem();
		grupoprioritarioId.setItemValue("634"); // Embarazadas
		grupoprioritarioId.setExcludedFromValidation(false);
		grupoprioritarioId.setMandatory(true);
		
		input.addField(RdacaaVariableKeyCatalog.PERSONA_CODIGO_SEXO, itemSexo);
		input.addField(RdacaaVariableKeyCatalog.PERSONA_FECHA_NACIMIENTO, itemFechaNac);
		input.addField(RdacaaVariableKeyCatalog.ATENCION_FECHA, itemFechaAtenc);
		input.addField(RdacaaVariableKeyCatalog.GRUPO_PRIORITARIO_4, grupoprioritarioId);
		Optional<RdacaaRawRowResult> sa = grupoPrioritario4RuleGroupExecutor.execute(input);
		sa.ifPresent(r -> {
			System.out.println(r.toString());
		});
		Assert.assertTrue("El Grupo Prioritario 4 es mandatorio",
				sa.get().get(RdacaaVariableKeyCatalog.GRUPO_PRIORITARIO_4).getValidationResultList().isEmpty());
	}

}

package ec.gob.msp.rdacaa.business.validation.rule.procedimiento.test;

import java.util.Optional;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import ec.gob.msp.rdacaa.business.service.ConstantesDetalleCatalogo;
import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaRawRow;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaRawRowResult;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.malla.RowItem;
import ec.gob.msp.rdacaa.business.validation.rule.atencion.procedimientos.prodto2.Procedimiento2RuleGroupExecutor;
import ec.gob.msp.rdacaa.test.ValidationTestContext;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = ValidationTestContext.class)
public class Procedimiento2RuleGroupExecutorTest {

	@Autowired
	private Procedimiento2RuleGroupExecutor procedimiento2RuleGroupExecutor;
	
	@Test
    public void whenCodigoProcedimientos2IsNull_thenError() {
        RdacaaRawRow input = new RdacaaRawRow();
        RowItem ritem = new RowItem();
        ritem.setItemValue(null);
        ritem.setExcludedFromValidation(false);
        input.addField(RdacaaVariableKeyCatalog.PROCEDIMIENTOS_CODIGO_2, ritem);
        Optional<RdacaaRawRowResult> sa = procedimiento2RuleGroupExecutor.execute(input);
        sa.ifPresent(r -> {
            System.out.println(r.toString());
        });
        Assert.assertTrue("Codigo para procedimiento 2 es obligatorio",
                sa.get().get(RdacaaVariableKeyCatalog.PROCEDIMIENTOS_CODIGO_2)
                        .hasValidationResultByCode(ValidationResultCatalogConstants.CODIGO_ERROR_PROCEDIMIENTO_2_NULO)
                        .isPresent());
    }
	
	@Test
	public void whenCodigoProcedimiento2IsExcludedAndValorIsNull() {
		RdacaaRawRow input = new RdacaaRawRow();
		RowItem ritem = new RowItem();
		ritem.setItemValue(ConstantesDetalleCatalogo.NOT_DEFINED_VALUE);
		ritem.setExcludedFromValidation(true);
		input.addField(RdacaaVariableKeyCatalog.PROCEDIMIENTOS_CODIGO_2, ritem);
		Optional<RdacaaRawRowResult> sa = procedimiento2RuleGroupExecutor.execute(input);
		sa.ifPresent(r -> {
			System.out.println(r.toString());
		});
		Assert.assertTrue("El codigo de procedimiento2 debe ser nulo", sa.get()
				.get(RdacaaVariableKeyCatalog.PROCEDIMIENTOS_CODIGO_2).getValidationResultList().isEmpty());
	}

	@Test
	public void whenCodProcedimiento2IsExcludedAndValorIsNotNull() {
		RdacaaRawRow input = new RdacaaRawRow();
		RowItem ritem = new RowItem();
		ritem.setItemValue("80"); // HTLV L-LL LGG
		ritem.setExcludedFromValidation(true);
		input.addField(RdacaaVariableKeyCatalog.PROCEDIMIENTOS_CODIGO_2, ritem);
		Optional<RdacaaRawRowResult> sa = procedimiento2RuleGroupExecutor.execute(input);
		sa.ifPresent(r -> {
			System.out.println(r.toString());
		});
		Assert.assertTrue("El codigo de procedimiento2 no debe ser nulo",
				sa.get().get(RdacaaVariableKeyCatalog.PROCEDIMIENTOS_CODIGO_2)
						.hasValidationResultByCode(
								ValidationResultCatalogConstants.CODIGO_ERROR_PROCEDIMIENTO_2_NO_DEFINIDO)
						.isPresent());
	}
	
	@Test
	public void whenCodigiProcedimiento2IsNotExcludedAndValorIsNotNull() {
		RdacaaRawRow input = new RdacaaRawRow();
		RowItem ritem = new RowItem();
		ritem.setItemValue("270292"); // Definitivo Inicial Confirmado por Laboratorio
		ritem.setExcludedFromValidation(false);

		input.addField(RdacaaVariableKeyCatalog.PROCEDIMIENTOS_CODIGO_2, ritem);
		Optional<RdacaaRawRowResult> sa = procedimiento2RuleGroupExecutor.execute(input);
		sa.ifPresent(r -> {
			System.out.println(r.toString());
		});
		Assert.assertTrue("El codigo de procedimiento2 no debe ser nulo", sa.get()
				.get(RdacaaVariableKeyCatalog.PROCEDIMIENTOS_CODIGO_2).getValidationResultList().isEmpty());
	}
	
	@Test
	public void whenCodigoProcedimiento2IsNotNumeric_thenError() {
		RdacaaRawRow input = new RdacaaRawRow();
		RowItem ritem = new RowItem();
		ritem.setItemValue("asdfasdf");
		ritem.setExcludedFromValidation(false);
		input.addField(RdacaaVariableKeyCatalog.PROCEDIMIENTOS_CODIGO_2, ritem);
		Optional<RdacaaRawRowResult> sa = procedimiento2RuleGroupExecutor.execute(input);
		sa.ifPresent(r -> {
			System.out.println(r.toString());
		});
		Assert.assertTrue("Se espera un error para caracteres no numéricos ",
				sa.get().get(RdacaaVariableKeyCatalog.PROCEDIMIENTOS_CODIGO_2)
						.hasValidationResultByCode(
								ValidationResultCatalogConstants.CODIGO_ERROR_PROCEDIMIENTO_2_NO_NUMERICO)
						.isPresent());
	}

	@Test
	public void whenCodigoProcedimiento2IsIncorrecto_thenError() {
		RdacaaRawRow input = new RdacaaRawRow();
		RowItem ritem = new RowItem();
		ritem.setItemValue("1325854"); // Valor no aplicable
		ritem.setExcludedFromValidation(false);
		input.addField(RdacaaVariableKeyCatalog.PROCEDIMIENTOS_CODIGO_2, ritem);
		Optional<RdacaaRawRowResult> sa = procedimiento2RuleGroupExecutor.execute(input);
		sa.ifPresent(r -> {
			System.out.println(r.toString());
		});
		Assert.assertTrue("El campo es incorrecto",
				sa.get().get(RdacaaVariableKeyCatalog.PROCEDIMIENTOS_CODIGO_2)
						.hasValidationResultByCode(
								ValidationResultCatalogConstants.CODIGO_ERROR_PROCEDIMIENTO_2_INCORRECTO)
						.isPresent());
	}
	
	
	@Test
	public void whenCondicionProcedimiento2IsCorrect_thenOk() {
		RdacaaRawRow input = new RdacaaRawRow();
		RowItem ritem = new RowItem();
		ritem.setItemValue("270292"); //PIROGLOBULINAS
		ritem.setExcludedFromValidation(false);

		input.addField(RdacaaVariableKeyCatalog.PROCEDIMIENTOS_CODIGO_2, ritem);
		Optional<RdacaaRawRowResult> sa = procedimiento2RuleGroupExecutor.execute(input);
		sa.ifPresent(r -> {
			System.out.println(r.toString());
		});
		Assert.assertTrue("No se esperan errores para codigo procedimiento2 en pantalla de PROCEDIMIENTOS correcto",
				sa.get().get(RdacaaVariableKeyCatalog.PROCEDIMIENTOS_CODIGO_2).getValidationResultList()
						.isEmpty());
	}
	
	
	@Test
	public void whenCodigoProcedimiento2IsMandatory() {
		RdacaaRawRow input = new RdacaaRawRow();
		RowItem ritem = new RowItem();
		ritem.setItemValue(ConstantesDetalleCatalogo.NOT_DEFINED_VALUE);
		ritem.setExcludedFromValidation(false);
		ritem.setMandatory(true);
		input.addField(RdacaaVariableKeyCatalog.PROCEDIMIENTOS_CODIGO_2, ritem);
		Optional<RdacaaRawRowResult> sa = procedimiento2RuleGroupExecutor.execute(input);
		sa.ifPresent(r -> {
			System.out.println(r.toString());
		});
		Assert.assertTrue("La codigo procedimiento 2 en pantalla de PROCEDIMIENTO es mandatorio",
				sa.get().get(RdacaaVariableKeyCatalog.PROCEDIMIENTOS_CODIGO_2)
						.hasValidationResultByCode(
								ValidationResultCatalogConstants.CODIGO_ERROR_PROCEDIMIENTO_2_ES_MANDATORIO)
						.isPresent());
	}
	
	@Test
	public void whenCodigoProcedimiento21IsMandatoryAndCorrectValue() {
		RdacaaRawRow input = new RdacaaRawRow();
		RowItem ritem = new RowItem();
		ritem.setItemValue("270292"); //CD-11
		ritem.setExcludedFromValidation(false);
		ritem.setMandatory(true);

		input.addField(RdacaaVariableKeyCatalog.PROCEDIMIENTOS_CODIGO_2, ritem);
		Optional<RdacaaRawRowResult> sa = procedimiento2RuleGroupExecutor.execute(input);
		sa.ifPresent(r -> {
			System.out.println(r.toString());
		});
		Assert.assertTrue("El codigo de procedimiento 2 en pantalla de PROCEDIMIENTOS es mandatorio", sa.get()
				.get(RdacaaVariableKeyCatalog.PROCEDIMIENTOS_CODIGO_2).getValidationResultList().isEmpty());
	}
}

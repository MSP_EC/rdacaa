package ec.gob.msp.rdacaa.business.validation.rule.test;

import java.util.Optional;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaRawRow;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaRawRowResult;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.malla.RowItem;
import ec.gob.msp.rdacaa.business.validation.rule.persona.noidentificado.NoIdentificadoRuleGroupExecutor;
import ec.gob.msp.rdacaa.test.ValidationTestContext;

/**
*
* @author dmurillo
*/
@RunWith(SpringRunner.class)
@ContextConfiguration(classes = ValidationTestContext.class)
public class NoIdentificadoRuleGroupExecutorTest {
	
	@Autowired
	private NoIdentificadoRuleGroupExecutor noIdentificadoRuleGroupExecutor;
	
	@Test
    public void whenNoIdentificadoLongitudIncorrect_thenError() {
		RdacaaRawRow input = new RdacaaRawRow();
		RowItem ritemTI = new RowItem();
        ritemTI.setItemValue("5");
        ritemTI.setExcludedFromValidation(false);
		RowItem ritem = new RowItem();
		ritem.setItemValue("1234567890123456799");
		ritem.setExcludedFromValidation(false);
		input.addField(RdacaaVariableKeyCatalog.PERSONA_CODIGO_TIPO_IDENTIFICACION, ritemTI);
        input.addField(RdacaaVariableKeyCatalog.PERSONA_IDENTIFICACION, ritem);
        Optional<RdacaaRawRowResult> sa = noIdentificadoRuleGroupExecutor.execute(input);
        sa.ifPresent(r -> {
            System.out.println(r.toString());
        });
        Assert.assertTrue("Se espera un error longitud incorrecta",
                sa.get().get(RdacaaVariableKeyCatalog.PERSONA_IDENTIFICACION)
                        .hasValidationResultByCode(ValidationResultCatalogConstants.CODIGO_ERROR_IDENTIFICACION_CEDULA_LONGITUD_INCORRECTA)
                        .isPresent());
	}
	
	@Test
    public void whenNoIdentificadoIsIncorrect_thenError() {
		RdacaaRawRow input = new RdacaaRawRow();
		RowItem ritemTI = new RowItem();
        ritemTI.setItemValue("5");
        ritemTI.setExcludedFromValidation(false);
		RowItem ritem = new RowItem();
		ritem.setItemValue("NONAPA17201801010");
		ritem.setExcludedFromValidation(false);
		RowItem ritemPN = new RowItem();
		ritemPN.setItemValue("NOMBREUNO");
		ritemPN.setExcludedFromValidation(false);
		RowItem ritemSN = new RowItem();
		ritemSN.setItemValue("NOMBREDOS");
		ritemSN.setExcludedFromValidation(false);
		RowItem ritemPA = new RowItem();
		ritemPA.setItemValue("APELLIDOUNO");
		ritemPA.setExcludedFromValidation(false);
		RowItem ritemSA = new RowItem();
		ritemSA.setItemValue("APELLIDODOS");
		ritemSA.setExcludedFromValidation(false);
		RowItem ritemFC = new RowItem();
		ritemFC.setItemValue("2018-01-01");
		ritemFC.setExcludedFromValidation(false);
		RowItem ritemCN = new RowItem();
		ritemCN.setItemValue("1");
		ritemCN.setExcludedFromValidation(false);
		RowItem ritemCP = new RowItem();
		ritemCP.setItemValue("1011");
		ritemCP.setExcludedFromValidation(false);
		
		input.addField(RdacaaVariableKeyCatalog.PERSONA_CODIGO_TIPO_IDENTIFICACION, ritemTI);
        input.addField(RdacaaVariableKeyCatalog.PERSONA_IDENTIFICACION, ritem);
        input.addField(RdacaaVariableKeyCatalog.PERSONA_PRIMER_NOMBRE, ritemPN);
        input.addField(RdacaaVariableKeyCatalog.PERSONA_SEGUNDO_NOMBRE, ritemSN);
        input.addField(RdacaaVariableKeyCatalog.PERSONA_PRIMER_APELLIDO, ritemPA);
        input.addField(RdacaaVariableKeyCatalog.PERSONA_SEGUNDO_APELLIDO, ritemSA);
        input.addField(RdacaaVariableKeyCatalog.PERSONA_FECHA_NACIMIENTO, ritemFC);
        input.addField(RdacaaVariableKeyCatalog.PERSONA_CODIGO_NACIONALIDAD, ritemCN);
        input.addField(RdacaaVariableKeyCatalog.PERSONA_CODIGO_PARROQUIA, ritemCP);
        
        
        Optional<RdacaaRawRowResult> sa = noIdentificadoRuleGroupExecutor.execute(input);
        sa.ifPresent(r -> {
            System.out.println(r.toString());
        });
        Assert.assertTrue("Se espera un error para la identificacion",
                sa.get().get(RdacaaVariableKeyCatalog.PERSONA_IDENTIFICACION)
                        .hasValidationResultByCode(ValidationResultCatalogConstants.CODIGO_ERROR_IDENTIFICACION_INCORRECTO)
                        .isPresent());
	}
	
	@Test
    public void whenNoIdentificadoIsCorrect_thenOK() {
		RdacaaRawRow input = new RdacaaRawRow();
		RowItem ritemTI = new RowItem();
        ritemTI.setItemValue("5");
        ritemTI.setExcludedFromValidation(false);
		RowItem ritem = new RowItem();
		ritem.setItemValue("NONAPA17201801011");
		ritem.setExcludedFromValidation(false);
		RowItem ritemPN = new RowItem();
		ritemPN.setItemValue("NOMBREUNO");
		ritemPN.setExcludedFromValidation(false);
		RowItem ritemSN = new RowItem();
		ritemSN.setItemValue("NOMBREDOS");
		ritemSN.setExcludedFromValidation(false);
		RowItem ritemPA = new RowItem();
		ritemPA.setItemValue("APELLIDOUNO");
		ritemPA.setExcludedFromValidation(false);
		RowItem ritemSA = new RowItem();
		ritemSA.setItemValue("APELLIDODOS");
		ritemSA.setExcludedFromValidation(false);
		RowItem ritemFC = new RowItem();
		ritemFC.setItemValue("2018-01-01");
		ritemFC.setExcludedFromValidation(false);
		RowItem ritemCN = new RowItem();
		ritemCN.setItemValue("1");
		ritemCN.setExcludedFromValidation(false);
		RowItem ritemCP = new RowItem();
		ritemCP.setItemValue("1011");
		ritemCP.setExcludedFromValidation(false);
		
		input.addField(RdacaaVariableKeyCatalog.PERSONA_CODIGO_TIPO_IDENTIFICACION, ritemTI);
        input.addField(RdacaaVariableKeyCatalog.PERSONA_IDENTIFICACION, ritem);
        input.addField(RdacaaVariableKeyCatalog.PERSONA_PRIMER_NOMBRE, ritemPN);
        input.addField(RdacaaVariableKeyCatalog.PERSONA_SEGUNDO_NOMBRE, ritemSN);
        input.addField(RdacaaVariableKeyCatalog.PERSONA_PRIMER_APELLIDO, ritemPA);
        input.addField(RdacaaVariableKeyCatalog.PERSONA_SEGUNDO_APELLIDO, ritemSA);
        input.addField(RdacaaVariableKeyCatalog.PERSONA_FECHA_NACIMIENTO, ritemFC);
        input.addField(RdacaaVariableKeyCatalog.PERSONA_CODIGO_NACIONALIDAD, ritemCN);
        input.addField(RdacaaVariableKeyCatalog.PERSONA_CODIGO_PARROQUIA, ritemCP);
        
        
        Optional<RdacaaRawRowResult> sa = noIdentificadoRuleGroupExecutor.execute(input);
        sa.ifPresent(r -> {
            System.out.println(r.toString());
        });
        Assert.assertTrue("No se espera un error para la identificacion",
                sa.get().get(RdacaaVariableKeyCatalog.PERSONA_IDENTIFICACION).getValidationResultList().isEmpty());
	}
}

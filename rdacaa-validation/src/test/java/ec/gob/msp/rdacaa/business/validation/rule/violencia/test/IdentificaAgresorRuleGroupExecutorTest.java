package ec.gob.msp.rdacaa.business.validation.rule.violencia.test;

import java.util.Optional;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaRawRow;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaRawRowResult;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.malla.RowItem;
import ec.gob.msp.rdacaa.business.validation.rule.atencion.violencia.identificaagresor.IdentificaAgresorRuleGroupExecutor;
import ec.gob.msp.rdacaa.test.ValidationTestContext;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = ValidationTestContext.class)
public class IdentificaAgresorRuleGroupExecutorTest {
	@Autowired
    private IdentificaAgresorRuleGroupExecutor identificaAgresorRuleGroupExecutor;

	@Test
    public void whenIdentificaAgresorIsExcludedAndValorIsNull(){
    	RdacaaRawRow input = new RdacaaRawRow();
    	RowItem ritem = new RowItem();
    	ritem.setItemValue("999990000066666");
    	ritem.setExcludedFromValidation(true);
    	input.addField(RdacaaVariableKeyCatalog.VIOLENCIA_IDENTIFICA_AGRESOR, ritem);
        Optional<RdacaaRawRowResult> sa = identificaAgresorRuleGroupExecutor.execute(input);
        sa.ifPresent(r -> {
            System.out.println(r.toString());
        });
        Assert.assertTrue("Identifica Agresor debe ser nulo",
                sa.get().get(RdacaaVariableKeyCatalog.VIOLENCIA_IDENTIFICA_AGRESOR).getValidationResultList().isEmpty());
    }
	
	@Test
    public void whenIdentificaAgresorIsExcludedAndValorIsNotNull(){
    	RdacaaRawRow input = new RdacaaRawRow();
    	RowItem ritem = new RowItem();
    	ritem.setItemValue("2625");
    	ritem.setExcludedFromValidation(true);
    	input.addField(RdacaaVariableKeyCatalog.VIOLENCIA_IDENTIFICA_AGRESOR, ritem);
        Optional<RdacaaRawRowResult> sa = identificaAgresorRuleGroupExecutor.execute(input);
        sa.ifPresent(r -> {
            System.out.println(r.toString());
        });
        Assert.assertTrue("Identifica Agresor debe ser nulo",
                sa.get().get(RdacaaVariableKeyCatalog.VIOLENCIA_IDENTIFICA_AGRESOR)
                        .hasValidationResultByCode(ValidationResultCatalogConstants.CODIGO_ERROR_VIOLENCIA_IDENTIFICA_AGRESOR_NO_DEFINIDA)
                        .isPresent());
    }
	
    @Test
    public void whenIdentificaAgresorIsNotExcludedAndValorIsNotNull(){
    	RdacaaRawRow input = new RdacaaRawRow();
    	RowItem ritem = new RowItem();
    	ritem.setItemValue("2625");
    	ritem.setExcludedFromValidation(false);
    	input.addField(RdacaaVariableKeyCatalog.VIOLENCIA_IDENTIFICA_AGRESOR, ritem);
        Optional<RdacaaRawRowResult> sa = identificaAgresorRuleGroupExecutor.execute(input);
        sa.ifPresent(r -> {
            System.out.println(r.toString());
        });
        Assert.assertTrue("Identifica Agresor debe ser nulo",
                sa.get().get(RdacaaVariableKeyCatalog.VIOLENCIA_IDENTIFICA_AGRESOR).getValidationResultList().isEmpty());
    }
    
    @Test
    public void whenIdentificaAgresorIsNull_thenError() {
        RdacaaRawRow input = new RdacaaRawRow();
        RowItem ritem = new RowItem();
        ritem.setItemValue(null);
        ritem.setExcludedFromValidation(false);
        input.addField(RdacaaVariableKeyCatalog.VIOLENCIA_IDENTIFICA_AGRESOR, ritem);
        Optional<RdacaaRawRowResult> sa = identificaAgresorRuleGroupExecutor.execute(input);
        sa.ifPresent(r -> {
            System.out.println(r.toString());
        });
        Assert.assertTrue("El valor para identificacion del agresor es obligatorio",
                sa.get().get(RdacaaVariableKeyCatalog.VIOLENCIA_IDENTIFICA_AGRESOR)
                        .hasValidationResultByCode(ValidationResultCatalogConstants.CODIGO_ERROR_VIOLENCIA_IDENTIFICA_AGRESOR_NULO)
                        .isPresent());
    }
    
    @Test
    public void whenIdentificaAgresorIsNotNumeric_thenError() {
        RdacaaRawRow input = new RdacaaRawRow();
        RowItem ritem = new RowItem();
        ritem.setItemValue("sdf");
        ritem.setExcludedFromValidation(false);
        input.addField(RdacaaVariableKeyCatalog.VIOLENCIA_IDENTIFICA_AGRESOR, ritem);
        Optional<RdacaaRawRowResult> sa = identificaAgresorRuleGroupExecutor.execute(input);
        sa.ifPresent(r -> {
            System.out.println(r.toString());
        });
        Assert.assertTrue("Se espera un error para caracteres no numericos ",
                sa.get().get(RdacaaVariableKeyCatalog.VIOLENCIA_IDENTIFICA_AGRESOR)
                        .hasValidationResultByCode(ValidationResultCatalogConstants.CODIGO_ERROR_VIOLENCIA_IDENTIFICA_AGRESOR_NUMERICO)
                        .isPresent());
    }
    
    @Test
    public void whenIdentificaAgresorIsNotCorrect_thenError() {
        RdacaaRawRow input = new RdacaaRawRow();
        RowItem ritem = new RowItem();
        ritem.setItemValue("100");
        ritem.setExcludedFromValidation(false);
        input.addField(RdacaaVariableKeyCatalog.VIOLENCIA_IDENTIFICA_AGRESOR, ritem);
        Optional<RdacaaRawRowResult> sa = identificaAgresorRuleGroupExecutor.execute(input);
        sa.ifPresent(r -> {
            System.out.println(r.toString());
        });
        Assert.assertTrue("Se espera un error para código de identifica agresor incorrecto ",
                sa.get().get(RdacaaVariableKeyCatalog.VIOLENCIA_IDENTIFICA_AGRESOR)
                        .hasValidationResultByCode(ValidationResultCatalogConstants.CODIGO_ERROR_VIOLENCIA_IDENTIFICA_AGRESOR_INCORRECTO)
                        .isPresent());
    }
    
    @Test
    public void whenIdentificaAgresorIsCorrect_thenOk() {
        RdacaaRawRow input = new RdacaaRawRow();
        RowItem ritem = new RowItem();
        ritem.setItemValue("2625");
        ritem.setExcludedFromValidation(false);
        input.addField(RdacaaVariableKeyCatalog.VIOLENCIA_IDENTIFICA_AGRESOR, ritem);
        Optional<RdacaaRawRowResult> sa = identificaAgresorRuleGroupExecutor.execute(input);
        sa.ifPresent(r -> {
            System.out.println(r.toString());
        });
        Assert.assertTrue("No se esperan errores para Identifica Agresor correcto",
                sa.get().get(RdacaaVariableKeyCatalog.VIOLENCIA_IDENTIFICA_AGRESOR).getValidationResultList().isEmpty());
    }
}

package ec.gob.msp.rdacaa.business.validation.rule.test;

import java.util.Optional;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaRawRow;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaRawRowResult;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.malla.RowItem;
import ec.gob.msp.rdacaa.business.validation.rule.persona.identidadgenero.IdentidadGeneroRuleGroupExecutor;
import ec.gob.msp.rdacaa.test.ValidationTestContext;

/**
*
* @author dmurillo
*/
@RunWith(SpringRunner.class)
@ContextConfiguration(classes = ValidationTestContext.class)
public class IdentidadGeneroRuleGroupExecutorTest {

	@Autowired
	private IdentidadGeneroRuleGroupExecutor identidadGeneroRuleGroupExecutor;
	
	@Test
    public void whenIdentidadGeneroIsNull_thenError() {
        RdacaaRawRow input = new RdacaaRawRow();
        RowItem ritem = new RowItem();
        ritem.setItemValue(null);
        ritem.setExcludedFromValidation(false);
        RowItem ritemFN = new RowItem();
        ritemFN.setItemValue("2008-09-01");
        ritemFN.setExcludedFromValidation(false);
        RowItem ritemFA = new RowItem();
        ritemFA.setItemValue("2018-09-14");
        ritemFA.setExcludedFromValidation(false);
        input.addField(RdacaaVariableKeyCatalog.PERSONA_CODIGO_IDENTIFICACION_GENERO, ritem);
        input.addField(RdacaaVariableKeyCatalog.PERSONA_FECHA_NACIMIENTO, ritemFN);
        input.addField(RdacaaVariableKeyCatalog.ATENCION_FECHA, ritemFA);
        Optional<RdacaaRawRowResult> sa = identidadGeneroRuleGroupExecutor.execute(input);
        sa.ifPresent(r -> {
            System.out.println(r.toString());
        });
        Assert.assertTrue("El campo es obligatorio",
                sa.get().get(RdacaaVariableKeyCatalog.PERSONA_CODIGO_IDENTIFICACION_GENERO)
                        .hasValidationResultByCode(ValidationResultCatalogConstants.CODIGO_ERROR_IDENTIFICACIONGENERO_NULO)
                        .isPresent());
    }
	
	@Test
    public void whenIdentidadGeneroIsNotNumeric_thenError() {
        RdacaaRawRow input = new RdacaaRawRow();
        RowItem ritem = new RowItem();
        ritem.setItemValue("RDACAA");
        ritem.setExcludedFromValidation(false);
        input.addField(RdacaaVariableKeyCatalog.PERSONA_CODIGO_IDENTIFICACION_GENERO, ritem);
        Optional<RdacaaRawRowResult> sa = identidadGeneroRuleGroupExecutor.execute(input);
        sa.ifPresent(r -> {
            System.out.println(r.toString());
        });
        Assert.assertTrue("Se espera un error para caracteres no numericos ",
                sa.get().get(RdacaaVariableKeyCatalog.PERSONA_CODIGO_IDENTIFICACION_GENERO)
                        .hasValidationResultByCode(ValidationResultCatalogConstants.CODIGO_ERROR_IDENTIFICACIONGENERO_NONUMERICA)
                        .isPresent());
    }
	
	@Test
    public void whenIdentidadGeneroIsIncorrecto_thenError() {
        RdacaaRawRow input = new RdacaaRawRow();
        RowItem ritem = new RowItem();
        ritem.setItemValue("37");
        ritem.setExcludedFromValidation(false);
        input.addField(RdacaaVariableKeyCatalog.PERSONA_CODIGO_IDENTIFICACION_GENERO, ritem);
        Optional<RdacaaRawRowResult> sa = identidadGeneroRuleGroupExecutor.execute(input);
        sa.ifPresent(r -> {
            System.out.println(r.toString());
        });
        Assert.assertTrue("El campo es incorrecto",
                sa.get().get(RdacaaVariableKeyCatalog.PERSONA_CODIGO_IDENTIFICACION_GENERO)
                        .hasValidationResultByCode(ValidationResultCatalogConstants.CODIGO_ERROR_IDENTIFICACIONGENERO_INCORRECTO)
                        .isPresent());
    }
	
	@Test
    public void whenIdentidadGeneroIsCorrect_thenOk() {
        RdacaaRawRow input = new RdacaaRawRow();
        RowItem ritem = new RowItem();
        ritem.setItemValue("677");
        ritem.setExcludedFromValidation(false);
        input.addField(RdacaaVariableKeyCatalog.PERSONA_CODIGO_IDENTIFICACION_GENERO, ritem);
        Optional<RdacaaRawRowResult> sa = identidadGeneroRuleGroupExecutor.execute(input);
        sa.ifPresent(r -> {
            System.out.println(r.toString());
        });
        Assert.assertTrue("No se espera error en este test",
                sa.get().get(RdacaaVariableKeyCatalog.PERSONA_CODIGO_IDENTIFICACION_GENERO).getValidationResultList().isEmpty());
    }
}

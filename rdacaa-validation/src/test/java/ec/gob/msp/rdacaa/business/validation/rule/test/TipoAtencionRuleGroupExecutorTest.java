package ec.gob.msp.rdacaa.business.validation.rule.test;

import java.util.Optional;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import ec.gob.msp.rdacaa.business.service.ConstantesDetalleCatalogo;
import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaRawRow;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaRawRowResult;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.malla.RowItem;
import ec.gob.msp.rdacaa.business.validation.rule.atencion.grupoprioritario.tipoatencion.TipoAtencionRuleGroupExecutor;
import ec.gob.msp.rdacaa.test.ValidationTestContext;

/**
 *
 * @author miguel.faubla
 */
@RunWith(SpringRunner.class)
@ContextConfiguration(classes = ValidationTestContext.class)
public class TipoAtencionRuleGroupExecutorTest {

	@Autowired
	private TipoAtencionRuleGroupExecutor tipoAtencionRuleGroupExecutor;

	@Test
	public void whenTipoAtencionIsExcludedAndValorIsNull() {
		RdacaaRawRow input = new RdacaaRawRow();
		RowItem tipoatencionId = new RowItem();
		tipoatencionId.setItemValue(ConstantesDetalleCatalogo.NOT_DEFINED_VALUE);
		tipoatencionId.setExcludedFromValidation(true);
		input.addField(RdacaaVariableKeyCatalog.ESTABLECIMIENTO_TIPO_ATENCION, tipoatencionId);
		Optional<RdacaaRawRowResult> sa = tipoAtencionRuleGroupExecutor.execute(input);
		sa.ifPresent(r -> {
			System.out.println(r.toString());
		});
		Assert.assertTrue("El Tipo de Atención debe ser nulo", sa.get()
				.get(RdacaaVariableKeyCatalog.ESTABLECIMIENTO_TIPO_ATENCION).getValidationResultList().isEmpty());
	}

	@Test
	public void whenTipoAtencionIsExcludedAndValorIsNotNull() {
		RdacaaRawRow input = new RdacaaRawRow();
		RowItem tipoatencionId = new RowItem();
		tipoatencionId.setItemValue("2589"); // Intramural
		tipoatencionId.setExcludedFromValidation(true);
		input.addField(RdacaaVariableKeyCatalog.ESTABLECIMIENTO_TIPO_ATENCION, tipoatencionId);
		Optional<RdacaaRawRowResult> sa = tipoAtencionRuleGroupExecutor.execute(input);
		sa.ifPresent(r -> {
			System.out.println(r.toString());
		});
		Assert.assertTrue("El Tipo de Atención no debe ser nulo",
				sa.get().get(RdacaaVariableKeyCatalog.ESTABLECIMIENTO_TIPO_ATENCION)
						.hasValidationResultByCode(
								ValidationResultCatalogConstants.CODIGO_ERROR_TIPO_ATENCION_ESTABLECIMIENTO_NO_DEFINIDA)
						.isPresent());
	}

	@Test
	public void whenTipoAtencionIsNotExcludedAndValorIsNotNull() {
		RdacaaRawRow input = new RdacaaRawRow();
		RowItem tipoatencionId = new RowItem();
		tipoatencionId.setItemValue("2590"); // Extramural
		tipoatencionId.setExcludedFromValidation(false);
		input.addField(RdacaaVariableKeyCatalog.ESTABLECIMIENTO_TIPO_ATENCION, tipoatencionId);
		Optional<RdacaaRawRowResult> sa = tipoAtencionRuleGroupExecutor.execute(input);
		sa.ifPresent(r -> {
			System.out.println(r.toString());
		});
		Assert.assertTrue("El Tipo de Atención no debe ser nulo", sa.get()
				.get(RdacaaVariableKeyCatalog.ESTABLECIMIENTO_TIPO_ATENCION).getValidationResultList().isEmpty());
	}

	@Test
	public void whenTipoAtencionIsNull_thenError() {
		RdacaaRawRow input = new RdacaaRawRow();
		RowItem tipoatencionId = new RowItem();
		tipoatencionId.setItemValue(null);
		tipoatencionId.setExcludedFromValidation(false);
		input.addField(RdacaaVariableKeyCatalog.ESTABLECIMIENTO_TIPO_ATENCION, tipoatencionId);
		Optional<RdacaaRawRowResult> sa = tipoAtencionRuleGroupExecutor.execute(input);
		sa.ifPresent(r -> {
			System.out.println(r.toString());
		});
		Assert.assertTrue("El campo es obligatorio",
				sa.get().get(RdacaaVariableKeyCatalog.ESTABLECIMIENTO_TIPO_ATENCION)
						.hasValidationResultByCode(
								ValidationResultCatalogConstants.CODIGO_ERROR_TIPO_ATENCION_ESTABLECIMIENTO_NULO)
						.isPresent());
	}

	@Test
	public void whenTipoAtencionIsNotNumeric_thenError() {
		RdacaaRawRow input = new RdacaaRawRow();
		RowItem tipoatencionId = new RowItem();
		tipoatencionId.setItemValue("ATENCION");
		tipoatencionId.setExcludedFromValidation(false);
		input.addField(RdacaaVariableKeyCatalog.ESTABLECIMIENTO_TIPO_ATENCION, tipoatencionId);
		Optional<RdacaaRawRowResult> sa = tipoAtencionRuleGroupExecutor.execute(input);
		sa.ifPresent(r -> {
			System.out.println(r.toString());
		});
		Assert.assertTrue("Se espera un error para caracteres no numéricos ",
				sa.get().get(RdacaaVariableKeyCatalog.ESTABLECIMIENTO_TIPO_ATENCION)
						.hasValidationResultByCode(
								ValidationResultCatalogConstants.CODIGO_ERROR_TIPO_ATENCION_ESTABLECIMIENTO_NONUMERICO)
						.isPresent());
	}

	@Test
	public void whenTipoAtencionIsIncorrecto_thenError() {
		RdacaaRawRow input = new RdacaaRawRow();
		RowItem tipoatencionId = new RowItem();
		tipoatencionId.setItemValue("71483");
		tipoatencionId.setExcludedFromValidation(false);
		input.addField(RdacaaVariableKeyCatalog.ESTABLECIMIENTO_TIPO_ATENCION, tipoatencionId);
		Optional<RdacaaRawRowResult> sa = tipoAtencionRuleGroupExecutor.execute(input);
		sa.ifPresent(r -> {
			System.out.println(r.toString());
		});
		Assert.assertTrue("El campo es incorrecto",
				sa.get().get(RdacaaVariableKeyCatalog.ESTABLECIMIENTO_TIPO_ATENCION)
						.hasValidationResultByCode(
								ValidationResultCatalogConstants.CODIGO_ERROR_TIPO_ATENCION_ESTABLECIMIENTO_INCORRECTO)
						.isPresent());
	}

	@Test
	public void whenTipoAtencionIsCorrect_thenOk() {
		RdacaaRawRow input = new RdacaaRawRow();
		RowItem tipoatencionId = new RowItem();
		tipoatencionId.setItemValue("2589"); // Intramural
		tipoatencionId.setExcludedFromValidation(false);
		input.addField(RdacaaVariableKeyCatalog.ESTABLECIMIENTO_TIPO_ATENCION, tipoatencionId);
		Optional<RdacaaRawRowResult> sa = tipoAtencionRuleGroupExecutor.execute(input);
		sa.ifPresent(r -> {
			System.out.println(r.toString());
		});
		Assert.assertTrue("No se esperan errores para Tipo de Atención correcto", sa.get()
				.get(RdacaaVariableKeyCatalog.ESTABLECIMIENTO_TIPO_ATENCION).getValidationResultList().isEmpty());
	}

	@Test
	public void whenTipoAtencionIsMandatory() {
		RdacaaRawRow input = new RdacaaRawRow();
		RowItem tipoatencionId = new RowItem();
		tipoatencionId.setItemValue(ConstantesDetalleCatalogo.NOT_DEFINED_VALUE);
		tipoatencionId.setExcludedFromValidation(false);
		tipoatencionId.setMandatory(true);
		input.addField(RdacaaVariableKeyCatalog.ESTABLECIMIENTO_TIPO_ATENCION, tipoatencionId);
		Optional<RdacaaRawRowResult> sa = tipoAtencionRuleGroupExecutor.execute(input);
		sa.ifPresent(r -> {
			System.out.println(r.toString());
		});
		Assert.assertTrue("El Tipo de Atención es mandatorio",
				sa.get().get(RdacaaVariableKeyCatalog.ESTABLECIMIENTO_TIPO_ATENCION).hasValidationResultByCode(
						ValidationResultCatalogConstants.CODIGO_ERROR_TIPO_ATENCION_ESTABLECIMIENTO_ES_MANDATORIA)
						.isPresent());
	}

	@Test
	public void whenTipoAtencionIsMadatoryAndCorrectValue() {
		RdacaaRawRow input = new RdacaaRawRow();
		RowItem tipoatencionId = new RowItem();
		tipoatencionId.setItemValue("2589"); // Intramural
		tipoatencionId.setExcludedFromValidation(false);
		tipoatencionId.setMandatory(true);
		input.addField(RdacaaVariableKeyCatalog.ESTABLECIMIENTO_TIPO_ATENCION, tipoatencionId);
		Optional<RdacaaRawRowResult> sa = tipoAtencionRuleGroupExecutor.execute(input);
		sa.ifPresent(r -> {
			System.out.println(r.toString());
		});
		Assert.assertTrue("El Tipo de Atención es mandatorio", sa.get()
				.get(RdacaaVariableKeyCatalog.ESTABLECIMIENTO_TIPO_ATENCION).getValidationResultList().isEmpty());
	}

}

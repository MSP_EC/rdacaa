/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.gob.msp.rdacaa.business.validation.rule.test;

import java.util.Optional;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import ec.gob.msp.rdacaa.business.service.ConstantesDetalleCatalogo;
import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaRawRow;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaRawRowResult;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.malla.RowItem;
import ec.gob.msp.rdacaa.business.validation.rule.atencion.cie10.tipodiagnostico4.TipoDiagnostico4RuleGroupExecutor;
import ec.gob.msp.rdacaa.test.ValidationTestContext;

/**
 *
 * @author miguel.faubla
 */
@RunWith(SpringRunner.class)
@ContextConfiguration(classes = ValidationTestContext.class)
public class TipoDiagnostico4RuleGroupExecutorTest {
	@Autowired
	private TipoDiagnostico4RuleGroupExecutor tipoDiagnostico4RuleGroupExecutor;

	@Test
	public void whenTipoDiagnostico4IsExcludedAndValorIsNull() {
		RdacaaRawRow input = new RdacaaRawRow();
		RowItem tipodiagnosticoId = new RowItem();
		tipodiagnosticoId.setItemValue(ConstantesDetalleCatalogo.NOT_DEFINED_VALUE);
		tipodiagnosticoId.setExcludedFromValidation(true);
		input.addField(RdacaaVariableKeyCatalog.DIAGNOSTICO_CODIGO_TIPO_DIAG_4, tipodiagnosticoId);
		Optional<RdacaaRawRowResult> sa = tipoDiagnostico4RuleGroupExecutor.execute(input);
		sa.ifPresent(r -> {
			System.out.println(r.toString());
		});
		Assert.assertTrue("El Tipo de Diagnostico 4 debe ser nulo", sa.get()
				.get(RdacaaVariableKeyCatalog.DIAGNOSTICO_CODIGO_TIPO_DIAG_4).getValidationResultList().isEmpty());
	}

	@Test
	public void whenTipoDiagnostico4IsExcludedAndValorIsNotNull() {
		RdacaaRawRow input = new RdacaaRawRow();
		RowItem tipodiagnosticoId = new RowItem();
		tipodiagnosticoId.setItemValue("228"); // Morbilidad
		tipodiagnosticoId.setExcludedFromValidation(true);
		input.addField(RdacaaVariableKeyCatalog.DIAGNOSTICO_CODIGO_TIPO_DIAG_4, tipodiagnosticoId);
		Optional<RdacaaRawRowResult> sa = tipoDiagnostico4RuleGroupExecutor.execute(input);
		sa.ifPresent(r -> {
			System.out.println(r.toString());
		});
		Assert.assertTrue("El Codigo Tipo de Diagnostico 4 no debe ser nulo",
				sa.get().get(RdacaaVariableKeyCatalog.DIAGNOSTICO_CODIGO_TIPO_DIAG_4)
						.hasValidationResultByCode(
								ValidationResultCatalogConstants.CODIGO_ERROR_TIPO_DIAGNOSTICO_4_NO_DEFINIDA)
						.isPresent());
	}

	@Test
	public void whenTipoDiagnostico4IsNotExcludedAndValorIsNotNull() {
		RdacaaRawRow input = new RdacaaRawRow();
		RowItem codigoCie = new RowItem();
		codigoCie.setItemValue("Z024");
		codigoCie.setExcludedFromValidation(false);
		RowItem tipodiagnosticoId = new RowItem();
		tipodiagnosticoId.setItemValue("227"); // Prevencion
		tipodiagnosticoId.setExcludedFromValidation(false);
		input.addField(RdacaaVariableKeyCatalog.DIAGNOSTICO_CODIGO_TIPO_DIAG_4, tipodiagnosticoId);
		input.addField(RdacaaVariableKeyCatalog.DIAGNOSTICO_CODIGO_CIE_4, codigoCie);
		Optional<RdacaaRawRowResult> sa = tipoDiagnostico4RuleGroupExecutor.execute(input);
		sa.ifPresent(r -> {
			System.out.println(r.toString());
		});
		Assert.assertTrue("El Tipo de Diagnostico 4 no debe ser nulo", sa.get()
				.get(RdacaaVariableKeyCatalog.DIAGNOSTICO_CODIGO_TIPO_DIAG_4).getValidationResultList().isEmpty());
	}

	@Test
	public void whenTipoDiagnostico4IsNull_thenError() {
		RdacaaRawRow input = new RdacaaRawRow();
		RowItem tipodiagnosticoId = new RowItem();
		tipodiagnosticoId.setItemValue(null);
		tipodiagnosticoId.setExcludedFromValidation(false);
		input.addField(RdacaaVariableKeyCatalog.DIAGNOSTICO_CODIGO_TIPO_DIAG_4, tipodiagnosticoId);
		Optional<RdacaaRawRowResult> sa = tipoDiagnostico4RuleGroupExecutor.execute(input);
		sa.ifPresent(r -> {
			System.out.println(r.toString());
		});
		Assert.assertTrue("El campo es obligatorio", sa.get()
				.get(RdacaaVariableKeyCatalog.DIAGNOSTICO_CODIGO_TIPO_DIAG_4)
				.hasValidationResultByCode(ValidationResultCatalogConstants.CODIGO_ERROR_TIPO_DIAGNOSTICO_4_NULO)
				.isPresent());
	}

	@Test
	public void whenTipoDiagnostico4IsNotNumeric_thenError() {
		RdacaaRawRow input = new RdacaaRawRow();
		RowItem tipodiagnosticoId = new RowItem();
		tipodiagnosticoId.setItemValue("PREVENCION");
		tipodiagnosticoId.setExcludedFromValidation(false);
		input.addField(RdacaaVariableKeyCatalog.DIAGNOSTICO_CODIGO_TIPO_DIAG_4, tipodiagnosticoId);
		Optional<RdacaaRawRowResult> sa = tipoDiagnostico4RuleGroupExecutor.execute(input);
		sa.ifPresent(r -> {
			System.out.println(r.toString());
		});
		Assert.assertTrue("Se espera un error para caracteres no numéricos ",
				sa.get().get(RdacaaVariableKeyCatalog.DIAGNOSTICO_CODIGO_TIPO_DIAG_4)
						.hasValidationResultByCode(
								ValidationResultCatalogConstants.CODIGO_ERROR_TIPO_DIAGNOSTICO_4_NONUMERICO)
						.isPresent());
	}

	@Test
	public void whenTipoDiagnostico4IsIncorrecto_thenError() {
		RdacaaRawRow input = new RdacaaRawRow();
		RowItem tipodiagnosticoId = new RowItem();
		tipodiagnosticoId.setItemValue("228");
		tipodiagnosticoId.setExcludedFromValidation(false);

		RowItem codigoCie = new RowItem();
		codigoCie.setItemValue("Z024");
		codigoCie.setExcludedFromValidation(false);

		input.addField(RdacaaVariableKeyCatalog.DIAGNOSTICO_CODIGO_TIPO_DIAG_4, tipodiagnosticoId);
		input.addField(RdacaaVariableKeyCatalog.DIAGNOSTICO_CODIGO_CIE_4, codigoCie);
		Optional<RdacaaRawRowResult> sa = tipoDiagnostico4RuleGroupExecutor.execute(input);
		sa.ifPresent(r -> {
			System.out.println(r.toString());
		});
		Assert.assertTrue("El campo es incorrecto",
				sa.get().get(RdacaaVariableKeyCatalog.DIAGNOSTICO_CODIGO_TIPO_DIAG_4)
						.hasValidationResultByCode(
								ValidationResultCatalogConstants.CODIGO_ERROR_TIPO_DIAGNOSTICO_4_INCORRECTO)
						.isPresent());
	}

	@Test
	public void whenTipoDiagnostico4IsCorrect_thenOk() {
		RdacaaRawRow input = new RdacaaRawRow();
		RowItem tipodiagnosticoId = new RowItem();
		tipodiagnosticoId.setItemValue("228"); // Morbilidad
		tipodiagnosticoId.setExcludedFromValidation(false);
		RowItem codigoCie = new RowItem();
		codigoCie.setItemValue("A073"); // Isosporiasis
		codigoCie.setExcludedFromValidation(false);
		input.addField(RdacaaVariableKeyCatalog.DIAGNOSTICO_CODIGO_TIPO_DIAG_4, tipodiagnosticoId);
		input.addField(RdacaaVariableKeyCatalog.DIAGNOSTICO_CODIGO_CIE_4, codigoCie);
		Optional<RdacaaRawRowResult> sa = tipoDiagnostico4RuleGroupExecutor.execute(input);
		sa.ifPresent(r -> {
			System.out.println(r.toString());
		});
		Assert.assertTrue("No se esperan errores para Codigo de Tipo de Diagnostico 4 correcto", sa.get()
				.get(RdacaaVariableKeyCatalog.DIAGNOSTICO_CODIGO_TIPO_DIAG_4).getValidationResultList().isEmpty());
	}

	@Test
	public void whenTipoDiagnostico4IsMandatory() {
		RdacaaRawRow input = new RdacaaRawRow();
		RowItem tipodiagnosticoId = new RowItem();
		tipodiagnosticoId.setItemValue(ConstantesDetalleCatalogo.NOT_DEFINED_VALUE);
		tipodiagnosticoId.setExcludedFromValidation(false);
		tipodiagnosticoId.setMandatory(true);
		input.addField(RdacaaVariableKeyCatalog.DIAGNOSTICO_CODIGO_TIPO_DIAG_4, tipodiagnosticoId);
		Optional<RdacaaRawRowResult> sa = tipoDiagnostico4RuleGroupExecutor.execute(input);
		sa.ifPresent(r -> {
			System.out.println(r.toString());
		});
		Assert.assertTrue("El Codigo de Tipo de Diagnostico 4 es mandatorio",
				sa.get().get(RdacaaVariableKeyCatalog.DIAGNOSTICO_CODIGO_TIPO_DIAG_4)
						.hasValidationResultByCode(
								ValidationResultCatalogConstants.CODIGO_ERROR_TIPO_DIAGNOSTICO_4_ES_MANDATORIA)
						.isPresent());
	}

	@Test
	public void whenTipoDiagnostico4IsMandatoryAndCorrectValue() {
		RdacaaRawRow input = new RdacaaRawRow();
		RowItem codigoCie = new RowItem();
		codigoCie.setItemValue("Z935"); // Cistostomía
		codigoCie.setExcludedFromValidation(false);
		RowItem tipodiagnosticoId = new RowItem();
		tipodiagnosticoId.setItemValue("227"); // Prevencion
		tipodiagnosticoId.setExcludedFromValidation(false);
		tipodiagnosticoId.setMandatory(true);
		input.addField(RdacaaVariableKeyCatalog.DIAGNOSTICO_CODIGO_TIPO_DIAG_4, tipodiagnosticoId);
		input.addField(RdacaaVariableKeyCatalog.DIAGNOSTICO_CODIGO_CIE_4, codigoCie);
		Optional<RdacaaRawRowResult> sa = tipoDiagnostico4RuleGroupExecutor.execute(input);
		sa.ifPresent(r -> {
			System.out.println(r.toString());
		});
		Assert.assertTrue("El Codigo de Tipo de Diagnostico 4 es mandatorio", sa.get()
				.get(RdacaaVariableKeyCatalog.DIAGNOSTICO_CODIGO_TIPO_DIAG_4).getValidationResultList().isEmpty());
	}

}

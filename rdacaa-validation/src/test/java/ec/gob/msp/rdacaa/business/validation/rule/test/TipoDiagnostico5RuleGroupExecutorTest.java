/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.gob.msp.rdacaa.business.validation.rule.test;

import java.util.Optional;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import ec.gob.msp.rdacaa.business.service.ConstantesDetalleCatalogo;
import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaRawRow;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaRawRowResult;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.malla.RowItem;
import ec.gob.msp.rdacaa.business.validation.rule.atencion.cie10.tipodiagnostico5.TipoDiagnostico5RuleGroupExecutor;
import ec.gob.msp.rdacaa.test.ValidationTestContext;

/**
 *
 * @author miguel.faubla
 */
@RunWith(SpringRunner.class)
@ContextConfiguration(classes = ValidationTestContext.class)
public class TipoDiagnostico5RuleGroupExecutorTest {
	@Autowired
	private TipoDiagnostico5RuleGroupExecutor tipoDiagnostico5RuleGroupExecutor;

	@Test
	public void whenTipoDiagnostico5IsExcludedAndValorIsNull() {
		RdacaaRawRow input = new RdacaaRawRow();
		RowItem tipodiagnosticoId = new RowItem();
		tipodiagnosticoId.setItemValue(ConstantesDetalleCatalogo.NOT_DEFINED_VALUE);
		tipodiagnosticoId.setExcludedFromValidation(true);
		input.addField(RdacaaVariableKeyCatalog.DIAGNOSTICO_CODIGO_TIPO_DIAG_5, tipodiagnosticoId);
		Optional<RdacaaRawRowResult> sa = tipoDiagnostico5RuleGroupExecutor.execute(input);
		sa.ifPresent(r -> {
			System.out.println(r.toString());
		});
		Assert.assertTrue("El Tipo de Diagnostico 5 debe ser nulo", sa.get()
				.get(RdacaaVariableKeyCatalog.DIAGNOSTICO_CODIGO_TIPO_DIAG_5).getValidationResultList().isEmpty());
	}

	@Test
	public void whenTipoDiagnostico5IsExcludedAndValorIsNotNull() {
		RdacaaRawRow input = new RdacaaRawRow();
		RowItem tipodiagnosticoId = new RowItem();
		tipodiagnosticoId.setItemValue("228"); // Morbilidad
		tipodiagnosticoId.setExcludedFromValidation(true);
		input.addField(RdacaaVariableKeyCatalog.DIAGNOSTICO_CODIGO_TIPO_DIAG_5, tipodiagnosticoId);
		Optional<RdacaaRawRowResult> sa = tipoDiagnostico5RuleGroupExecutor.execute(input);
		sa.ifPresent(r -> {
			System.out.println(r.toString());
		});
		Assert.assertTrue("El Codigo Tipo de Diagnostico 5 no debe ser nulo",
				sa.get().get(RdacaaVariableKeyCatalog.DIAGNOSTICO_CODIGO_TIPO_DIAG_5)
						.hasValidationResultByCode(
								ValidationResultCatalogConstants.CODIGO_ERROR_TIPO_DIAGNOSTICO_5_NO_DEFINIDA)
						.isPresent());
	}

	@Test
	public void whenTipoDiagnostico5IsNotExcludedAndValorIsNotNull() {
		RdacaaRawRow input = new RdacaaRawRow();
		RowItem codigoCie = new RowItem();
		codigoCie.setItemValue("Z024");
		codigoCie.setExcludedFromValidation(false);
		RowItem tipodiagnosticoId = new RowItem();
		tipodiagnosticoId.setItemValue("227"); // Prevencion
		tipodiagnosticoId.setExcludedFromValidation(false);
		input.addField(RdacaaVariableKeyCatalog.DIAGNOSTICO_CODIGO_TIPO_DIAG_5, tipodiagnosticoId);
		input.addField(RdacaaVariableKeyCatalog.DIAGNOSTICO_CODIGO_CIE_5, codigoCie);
		Optional<RdacaaRawRowResult> sa = tipoDiagnostico5RuleGroupExecutor.execute(input);
		sa.ifPresent(r -> {
			System.out.println(r.toString());
		});
		Assert.assertTrue("El Tipo de Diagnostico 5 no debe ser nulo", sa.get()
				.get(RdacaaVariableKeyCatalog.DIAGNOSTICO_CODIGO_TIPO_DIAG_5).getValidationResultList().isEmpty());
	}

	@Test
	public void whenTipoDiagnostico5IsNull_thenError() {
		RdacaaRawRow input = new RdacaaRawRow();
		RowItem tipodiagnosticoId = new RowItem();
		tipodiagnosticoId.setItemValue(null);
		tipodiagnosticoId.setExcludedFromValidation(false);
		input.addField(RdacaaVariableKeyCatalog.DIAGNOSTICO_CODIGO_TIPO_DIAG_5, tipodiagnosticoId);
		Optional<RdacaaRawRowResult> sa = tipoDiagnostico5RuleGroupExecutor.execute(input);
		sa.ifPresent(r -> {
			System.out.println(r.toString());
		});
		Assert.assertTrue("El campo es obligatorio", sa.get()
				.get(RdacaaVariableKeyCatalog.DIAGNOSTICO_CODIGO_TIPO_DIAG_5)
				.hasValidationResultByCode(ValidationResultCatalogConstants.CODIGO_ERROR_TIPO_DIAGNOSTICO_5_NULO)
				.isPresent());
	}

	@Test
	public void whenTipoDiagnostico5IsNotNumeric_thenError() {
		RdacaaRawRow input = new RdacaaRawRow();
		RowItem tipodiagnosticoId = new RowItem();
		tipodiagnosticoId.setItemValue("PREVENCION");
		tipodiagnosticoId.setExcludedFromValidation(false);
		input.addField(RdacaaVariableKeyCatalog.DIAGNOSTICO_CODIGO_TIPO_DIAG_5, tipodiagnosticoId);
		Optional<RdacaaRawRowResult> sa = tipoDiagnostico5RuleGroupExecutor.execute(input);
		sa.ifPresent(r -> {
			System.out.println(r.toString());
		});
		Assert.assertTrue("Se espera un error para caracteres no numéricos ",
				sa.get().get(RdacaaVariableKeyCatalog.DIAGNOSTICO_CODIGO_TIPO_DIAG_5)
						.hasValidationResultByCode(
								ValidationResultCatalogConstants.CODIGO_ERROR_TIPO_DIAGNOSTICO_5_NONUMERICO)
						.isPresent());
	}

	@Test
	public void whenTipoDiagnostico5IsIncorrecto_thenError() {
		RdacaaRawRow input = new RdacaaRawRow();
		RowItem tipodiagnosticoId = new RowItem();
		tipodiagnosticoId.setItemValue("228");
		tipodiagnosticoId.setExcludedFromValidation(false);

		RowItem codigoCie = new RowItem();
		codigoCie.setItemValue("Z024");
		codigoCie.setExcludedFromValidation(false);

		input.addField(RdacaaVariableKeyCatalog.DIAGNOSTICO_CODIGO_TIPO_DIAG_5, tipodiagnosticoId);
		input.addField(RdacaaVariableKeyCatalog.DIAGNOSTICO_CODIGO_CIE_5, codigoCie);
		Optional<RdacaaRawRowResult> sa = tipoDiagnostico5RuleGroupExecutor.execute(input);
		sa.ifPresent(r -> {
			System.out.println(r.toString());
		});
		Assert.assertTrue("El campo es incorrecto",
				sa.get().get(RdacaaVariableKeyCatalog.DIAGNOSTICO_CODIGO_TIPO_DIAG_5)
						.hasValidationResultByCode(
								ValidationResultCatalogConstants.CODIGO_ERROR_TIPO_DIAGNOSTICO_5_INCORRECTO)
						.isPresent());
	}

	@Test
	public void whenTipoDiagnostico5IsCorrect_thenOk() {
		RdacaaRawRow input = new RdacaaRawRow();
		RowItem tipodiagnosticoId = new RowItem();
		tipodiagnosticoId.setItemValue("228"); // Morbilidad
		tipodiagnosticoId.setExcludedFromValidation(false);
		RowItem codigoCie = new RowItem();
		codigoCie.setItemValue("A073"); // Isosporiasis
		codigoCie.setExcludedFromValidation(false);
		input.addField(RdacaaVariableKeyCatalog.DIAGNOSTICO_CODIGO_TIPO_DIAG_5, tipodiagnosticoId);
		input.addField(RdacaaVariableKeyCatalog.DIAGNOSTICO_CODIGO_CIE_5, codigoCie);
		Optional<RdacaaRawRowResult> sa = tipoDiagnostico5RuleGroupExecutor.execute(input);
		sa.ifPresent(r -> {
			System.out.println(r.toString());
		});
		Assert.assertTrue("No se esperan errores para Codigo de Tipo de Diagnostico 5 correcto", sa.get()
				.get(RdacaaVariableKeyCatalog.DIAGNOSTICO_CODIGO_TIPO_DIAG_5).getValidationResultList().isEmpty());
	}

	@Test
	public void whenTipoDiagnostico5IsMandatory() {
		RdacaaRawRow input = new RdacaaRawRow();
		RowItem tipodiagnosticoId = new RowItem();
		tipodiagnosticoId.setItemValue(ConstantesDetalleCatalogo.NOT_DEFINED_VALUE);
		tipodiagnosticoId.setExcludedFromValidation(false);
		tipodiagnosticoId.setMandatory(true);
		input.addField(RdacaaVariableKeyCatalog.DIAGNOSTICO_CODIGO_TIPO_DIAG_5, tipodiagnosticoId);
		Optional<RdacaaRawRowResult> sa = tipoDiagnostico5RuleGroupExecutor.execute(input);
		sa.ifPresent(r -> {
			System.out.println(r.toString());
		});
		Assert.assertTrue("El Codigo de Tipo de Diagnostico 5 es mandatorio",
				sa.get().get(RdacaaVariableKeyCatalog.DIAGNOSTICO_CODIGO_TIPO_DIAG_5)
						.hasValidationResultByCode(
								ValidationResultCatalogConstants.CODIGO_ERROR_TIPO_DIAGNOSTICO_5_ES_MANDATORIA)
						.isPresent());
	}

	@Test
	public void whenTipoDiagnostico5IsMandatoryAndCorrectValue() {
		RdacaaRawRow input = new RdacaaRawRow();
		RowItem codigoCie = new RowItem();
		codigoCie.setItemValue("Z935"); // Cistostomía
		codigoCie.setExcludedFromValidation(false);
		RowItem tipodiagnosticoId = new RowItem();
		tipodiagnosticoId.setItemValue("227"); // Prevencion
		tipodiagnosticoId.setExcludedFromValidation(false);
		tipodiagnosticoId.setMandatory(true);
		input.addField(RdacaaVariableKeyCatalog.DIAGNOSTICO_CODIGO_TIPO_DIAG_5, tipodiagnosticoId);
		input.addField(RdacaaVariableKeyCatalog.DIAGNOSTICO_CODIGO_CIE_5, codigoCie);
		Optional<RdacaaRawRowResult> sa = tipoDiagnostico5RuleGroupExecutor.execute(input);
		sa.ifPresent(r -> {
			System.out.println(r.toString());
		});
		Assert.assertTrue("El Codigo de Tipo de Diagnostico 5 es mandatorio", sa.get()
				.get(RdacaaVariableKeyCatalog.DIAGNOSTICO_CODIGO_TIPO_DIAG_5).getValidationResultList().isEmpty());
	}

}

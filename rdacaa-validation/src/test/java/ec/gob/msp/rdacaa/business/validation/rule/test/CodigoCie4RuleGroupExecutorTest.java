package ec.gob.msp.rdacaa.business.validation.rule.test;

import java.util.Optional;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import ec.gob.msp.rdacaa.business.service.ConstantesDetalleCatalogo;
import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaRawRow;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaRawRowResult;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.malla.RowItem;
import ec.gob.msp.rdacaa.business.validation.rule.atencion.cie10.codigocie4.CodigoCie4RuleGroupExecutor;
import ec.gob.msp.rdacaa.test.ValidationTestContext;

/**
 *
 * @author miguel.faubla
 */
@RunWith(SpringRunner.class)
@ContextConfiguration(classes = ValidationTestContext.class)
public class CodigoCie4RuleGroupExecutorTest {

	@Autowired
	private CodigoCie4RuleGroupExecutor codigoCie4RuleGroupExecutor;

	@Test
	public void whenCodigoCie4IsExcludedAndValorIsNull() {
		RdacaaRawRow input = new RdacaaRawRow();
		RowItem codigocie10Id = new RowItem();
		codigocie10Id.setItemValue(ConstantesDetalleCatalogo.NOT_DEFINED_VALUE);
		codigocie10Id.setExcludedFromValidation(true);
		input.addField(RdacaaVariableKeyCatalog.DIAGNOSTICO_CODIGO_CIE_4, codigocie10Id);
		Optional<RdacaaRawRowResult> sa = codigoCie4RuleGroupExecutor.execute(input);
		sa.ifPresent(r -> {
			System.out.println(r.toString());
		});
		Assert.assertTrue("El Codigo Cie10_4 debe ser nulo",
				sa.get().get(RdacaaVariableKeyCatalog.DIAGNOSTICO_CODIGO_CIE_4).getValidationResultList().isEmpty());
	}

	@Test
	public void whenCodigoCie4IsExcludedAndValorIsNotNull() {
		RdacaaRawRow input = new RdacaaRawRow();
		RowItem codigocie10Id = new RowItem();
		codigocie10Id.setItemValue("C510"); // Tumor maligno del labio mayor
		codigocie10Id.setExcludedFromValidation(true);
		input.addField(RdacaaVariableKeyCatalog.DIAGNOSTICO_CODIGO_CIE_4, codigocie10Id);
		Optional<RdacaaRawRowResult> sa = codigoCie4RuleGroupExecutor.execute(input);
		sa.ifPresent(r -> {
			System.out.println(r.toString());
		});
		Assert.assertTrue("El Codigo Cie10_4 no debe ser nulo",
				sa.get().get(RdacaaVariableKeyCatalog.DIAGNOSTICO_CODIGO_CIE_4)
						.hasValidationResultByCode(
								ValidationResultCatalogConstants.CODIGO_ERROR_COD_DIAGNOSTICO_CIE10_4_NO_DEFINIDA)
						.isPresent());
	}

	@Test
	public void whenCodigoCie4IsNotExcludedAndValorIsNotNull() {
		RdacaaRawRow input = new RdacaaRawRow();
		RowItem itemSexo = new RowItem();
		itemSexo.setItemValue("16"); // Hombre
		itemSexo.setExcludedFromValidation(false);
		RowItem itemFechaNac = new RowItem();
		itemFechaNac.setItemValue("1989-07-03");
		itemFechaNac.setExcludedFromValidation(false);
		RowItem itemFechaAtenc = new RowItem();
		itemFechaAtenc.setItemValue("2018-10-12");
		itemFechaAtenc.setExcludedFromValidation(false);
		RowItem itemEspecialidad = new RowItem();
		itemEspecialidad.setItemValue("1519"); // Odontologia Rural
		itemEspecialidad.setExcludedFromValidation(false);
		RowItem codigocie10Id = new RowItem();
		codigocie10Id.setItemValue("B370"); // Estomatitis candidiásica
		codigocie10Id.setExcludedFromValidation(false);

		input.addField(RdacaaVariableKeyCatalog.PERSONA_CODIGO_SEXO, itemSexo);
		input.addField(RdacaaVariableKeyCatalog.PERSONA_FECHA_NACIMIENTO, itemFechaNac);
		input.addField(RdacaaVariableKeyCatalog.ATENCION_FECHA, itemFechaAtenc);
		input.addField(RdacaaVariableKeyCatalog.ATENCION_CODIGO_ESPECIALIDAD, itemEspecialidad);
		input.addField(RdacaaVariableKeyCatalog.DIAGNOSTICO_CODIGO_CIE_4, codigocie10Id);
		Optional<RdacaaRawRowResult> sa = codigoCie4RuleGroupExecutor.execute(input);
		sa.ifPresent(r -> {
			System.out.println(r.toString());
		});
		Assert.assertTrue("El Codigo Cie10_4 no debe ser nulo",
				sa.get().get(RdacaaVariableKeyCatalog.DIAGNOSTICO_CODIGO_CIE_4).getValidationResultList().isEmpty());
	}

	@Test
	public void whenCodigoCie4IsNull_thenError() {
		RdacaaRawRow input = new RdacaaRawRow();
		RowItem codigocie10Id = new RowItem();
		codigocie10Id.setItemValue(null);
		codigocie10Id.setExcludedFromValidation(false);
		input.addField(RdacaaVariableKeyCatalog.DIAGNOSTICO_CODIGO_CIE_4, codigocie10Id);
		Optional<RdacaaRawRowResult> sa = codigoCie4RuleGroupExecutor.execute(input);
		sa.ifPresent(r -> {
			System.out.println(r.toString());
		});
		Assert.assertTrue("El campo es obligatorio",
				sa.get().get(RdacaaVariableKeyCatalog.DIAGNOSTICO_CODIGO_CIE_4)
						.hasValidationResultByCode(
								ValidationResultCatalogConstants.CODIGO_ERROR_COD_DIAGNOSTICO_CIE10_4_NULO)
						.isPresent());
	}

	@Test
	public void whenCodigoCie4IsNotAlphanumeric_thenError() {
		RdacaaRawRow input = new RdacaaRawRow();
		RowItem codigocie10Id = new RowItem();
		codigocie10Id.setItemValue("14578-ABQ");
		codigocie10Id.setExcludedFromValidation(false);
		input.addField(RdacaaVariableKeyCatalog.DIAGNOSTICO_CODIGO_CIE_4, codigocie10Id);
		Optional<RdacaaRawRowResult> sa = codigoCie4RuleGroupExecutor.execute(input);
		sa.ifPresent(r -> {
			System.out.println(r.toString());
		});
		Assert.assertTrue("Se espera un error para caracteres no alfanuméricos ",
				sa.get().get(RdacaaVariableKeyCatalog.DIAGNOSTICO_CODIGO_CIE_4)
						.hasValidationResultByCode(
								ValidationResultCatalogConstants.CODIGO_ERROR_COD_DIAGNOSTICO_CIE10_4_NOALFANUMERICO)
						.isPresent());
	}

	@Test
	public void whenCodigoCie4LongitudIncorrect_thenError() {
		RdacaaRawRow input = new RdacaaRawRow();
		RowItem itemCodDiagnostico = new RowItem();
		itemCodDiagnostico.setItemValue("5551889");
		itemCodDiagnostico.setExcludedFromValidation(false);
		input.addField(RdacaaVariableKeyCatalog.DIAGNOSTICO_CODIGO_CIE_4, itemCodDiagnostico);
		Optional<RdacaaRawRowResult> sa = codigoCie4RuleGroupExecutor.execute(input);
		sa.ifPresent(r -> {
			System.out.println(r.toString());
		});
		Assert.assertTrue("Se espera un error longitud incorrecta",
				sa.get().get(RdacaaVariableKeyCatalog.DIAGNOSTICO_CODIGO_CIE_4).hasValidationResultByCode(
						ValidationResultCatalogConstants.CODIGO_ERROR_COD_DIAGNOSTICO_CIE10_4_LONGITUD_INCORRECTA)
						.isPresent());
	}

	@Test
	public void whenCodigoCie4IsIncorrecto_thenError() {
		RdacaaRawRow input = new RdacaaRawRow();
		RowItem itemSexo = new RowItem();
		itemSexo.setItemValue("17"); // Mujer
		itemSexo.setExcludedFromValidation(false);
		RowItem itemFechaNac = new RowItem();
		itemFechaNac.setItemValue("1989-07-03");
		itemFechaNac.setExcludedFromValidation(false);
		RowItem itemFechaAtenc = new RowItem();
		itemFechaAtenc.setItemValue("2018-10-12");
		itemFechaAtenc.setExcludedFromValidation(false);
		RowItem itemEspecialidad = new RowItem();
		itemEspecialidad.setItemValue("1516"); // Medicina General / No aplica
		itemEspecialidad.setExcludedFromValidation(false);
		RowItem codigocie10Id = new RowItem();
		codigocie10Id.setItemValue("BB370"); // Estomatitis candidiásica
		codigocie10Id.setExcludedFromValidation(false);

		input.addField(RdacaaVariableKeyCatalog.PERSONA_CODIGO_SEXO, itemSexo);
		input.addField(RdacaaVariableKeyCatalog.PERSONA_FECHA_NACIMIENTO, itemFechaNac);
		input.addField(RdacaaVariableKeyCatalog.ATENCION_FECHA, itemFechaAtenc);
		input.addField(RdacaaVariableKeyCatalog.ATENCION_CODIGO_ESPECIALIDAD, itemEspecialidad);
		input.addField(RdacaaVariableKeyCatalog.DIAGNOSTICO_CODIGO_CIE_4, codigocie10Id);
		Optional<RdacaaRawRowResult> sa = codigoCie4RuleGroupExecutor.execute(input);
		sa.ifPresent(r -> {
			System.out.println(r.toString());
		});
		Assert.assertTrue("El campo es incorrecto",
				sa.get().get(RdacaaVariableKeyCatalog.DIAGNOSTICO_CODIGO_CIE_4)
						.hasValidationResultByCode(
								ValidationResultCatalogConstants.CODIGO_ERROR_COD_DIAGNOSTICO_CIE10_4_INCORRECTO)
						.isPresent());
	}

	@Test
	public void whenCodigoCie4IsCorrect_thenOk() {
		RdacaaRawRow input = new RdacaaRawRow();
		RowItem itemSexo = new RowItem();
		itemSexo.setItemValue("16"); // Hombre
		itemSexo.setExcludedFromValidation(false);
		RowItem itemFechaNac = new RowItem();
		itemFechaNac.setItemValue("1989-07-03");
		itemFechaNac.setExcludedFromValidation(false);
		RowItem itemFechaAtenc = new RowItem();
		itemFechaAtenc.setItemValue("2018-10-12");
		itemFechaAtenc.setExcludedFromValidation(false);
		RowItem itemEspecialidad = new RowItem();
		itemEspecialidad.setItemValue("655"); // Psicología Clínica
		itemEspecialidad.setExcludedFromValidation(false);
		RowItem codigocie10Id = new RowItem();
		codigocie10Id.setItemValue("F001"); // Demencia en la enfermedad de Alzheimer, de comienzo tardío
		codigocie10Id.setExcludedFromValidation(false);

		input.addField(RdacaaVariableKeyCatalog.PERSONA_CODIGO_SEXO, itemSexo);
		input.addField(RdacaaVariableKeyCatalog.PERSONA_FECHA_NACIMIENTO, itemFechaNac);
		input.addField(RdacaaVariableKeyCatalog.ATENCION_FECHA, itemFechaAtenc);
		input.addField(RdacaaVariableKeyCatalog.ATENCION_CODIGO_ESPECIALIDAD, itemEspecialidad);
		input.addField(RdacaaVariableKeyCatalog.DIAGNOSTICO_CODIGO_CIE_4, codigocie10Id);
		Optional<RdacaaRawRowResult> sa = codigoCie4RuleGroupExecutor.execute(input);
		sa.ifPresent(r -> {
			System.out.println(r.toString());
		});
		Assert.assertTrue("No se esperan errores para Codigo de Diagnostico 4(CIE10) correcto",
				sa.get().get(RdacaaVariableKeyCatalog.DIAGNOSTICO_CODIGO_CIE_4).getValidationResultList().isEmpty());
	}

	@Test
	public void whenCodigoCie4IsMandatory() {
		RdacaaRawRow input = new RdacaaRawRow();
		RowItem codigocie10Id = new RowItem();
		codigocie10Id.setItemValue(ConstantesDetalleCatalogo.NOT_DEFINED_VALUE);
		codigocie10Id.setExcludedFromValidation(false);
		codigocie10Id.setMandatory(true);
		input.addField(RdacaaVariableKeyCatalog.DIAGNOSTICO_CODIGO_CIE_4, codigocie10Id);
		Optional<RdacaaRawRowResult> sa = codigoCie4RuleGroupExecutor.execute(input);
		sa.ifPresent(r -> {
			System.out.println(r.toString());
		});
		Assert.assertTrue("El Codigo de Diagnostico 4 es mandatorio",
				sa.get().get(RdacaaVariableKeyCatalog.DIAGNOSTICO_CODIGO_CIE_4)
						.hasValidationResultByCode(
								ValidationResultCatalogConstants.CODIGO_ERROR_COD_DIAGNOSTICO_CIE10_4_ES_MANDATORIA)
						.isPresent());
	}

	@Test
	public void whenCodigoCie4IsMandatoryAndCorrectValue() {
		RdacaaRawRow input = new RdacaaRawRow();
		RowItem itemSexo = new RowItem();
		itemSexo.setItemValue("16"); // Hombre
		itemSexo.setExcludedFromValidation(false);
		RowItem itemFechaNac = new RowItem();
		itemFechaNac.setItemValue("1989-07-03");
		itemFechaNac.setExcludedFromValidation(false);
		RowItem itemFechaAtenc = new RowItem();
		itemFechaAtenc.setItemValue("2018-10-11");
		itemFechaAtenc.setExcludedFromValidation(false);
		RowItem itemEspecialidad = new RowItem();
		itemEspecialidad.setItemValue("667"); // Enfermería Rural
		itemEspecialidad.setExcludedFromValidation(false);
		RowItem codigocie10Id = new RowItem();
		codigocie10Id.setItemValue("Z3081"); // Otras atenciones especificadas para la anticoncepción, condón masculino
		codigocie10Id.setExcludedFromValidation(false);
		codigocie10Id.setMandatory(true);

		input.addField(RdacaaVariableKeyCatalog.PERSONA_CODIGO_SEXO, itemSexo);
		input.addField(RdacaaVariableKeyCatalog.PERSONA_FECHA_NACIMIENTO, itemFechaNac);
		input.addField(RdacaaVariableKeyCatalog.ATENCION_FECHA, itemFechaAtenc);
		input.addField(RdacaaVariableKeyCatalog.ATENCION_CODIGO_ESPECIALIDAD, itemEspecialidad);
		input.addField(RdacaaVariableKeyCatalog.DIAGNOSTICO_CODIGO_CIE_4, codigocie10Id);
		Optional<RdacaaRawRowResult> sa = codigoCie4RuleGroupExecutor.execute(input);
		sa.ifPresent(r -> {
			System.out.println(r.toString());
		});
		Assert.assertTrue("El Codigo de Diagnostico_4 es mandatorio",
				sa.get().get(RdacaaVariableKeyCatalog.DIAGNOSTICO_CODIGO_CIE_4).getValidationResultList().isEmpty());
	}

}

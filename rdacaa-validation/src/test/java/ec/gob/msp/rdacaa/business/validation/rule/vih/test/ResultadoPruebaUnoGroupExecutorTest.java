package ec.gob.msp.rdacaa.business.validation.rule.vih.test;

import java.util.Optional;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import com.deliveredtechnologies.rulebook.Fact;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaRawRow;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaRawRowResult;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.malla.RowItem;
import ec.gob.msp.rdacaa.business.validation.rule.vih.pruebauno.PruebaUnoRuleGroupExecutor;
import ec.gob.msp.rdacaa.business.validation.rule.vih.resultadopruebauno.ResultadoPruebaUnoRuleGroupExecutor;
import ec.gob.msp.rdacaa.test.ValidationTestContext;

/**
*
* @author dmurillo
*/

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = ValidationTestContext.class)
public class ResultadoPruebaUnoGroupExecutorTest {

	@Autowired
    private ResultadoPruebaUnoRuleGroupExecutor resultadoPruebaUnoRuleGroupExecutor;
	@Autowired
	private PruebaUnoRuleGroupExecutor pruebaUnoRuleGroupExecutor;
	private RdacaaRawRowResult resultTotal = new RdacaaRawRowResult();

	private Optional<RdacaaRawRowResult> evaluatePruebaUno(){
		RdacaaRawRow input = new RdacaaRawRow();
		RowItem ritem = new RowItem();
		ritem.setItemValue("2593");
    	ritem.setExcludedFromValidation(false);
    	input.addField(RdacaaVariableKeyCatalog.VIH_CODIGO_PRIMERA_PRUEBA, ritem);
    	return pruebaUnoRuleGroupExecutor.execute(input);
	}
	
	private Fact evaluateAll(){
		Optional<RdacaaRawRowResult>  resultForEach = evaluatePruebaUno();
		RdacaaRawRowResult currentResult = resultForEach.isPresent() ? resultForEach.get() : new RdacaaRawRowResult();
		resultTotal.putAll(currentResult);
		@SuppressWarnings("unchecked")
		Fact resultTotalFact = new Fact("rdacaa_row_result", resultTotal);
		return resultTotalFact;
	}
	
	@Test
	public void whenResultadoPruebaUnoIsNullthenError() {
		
		RdacaaRawRow input = new RdacaaRawRow();
		RowItem ritem = new RowItem();
		ritem.setItemValue(null);
    	ritem.setExcludedFromValidation(true);
    	input.addField(RdacaaVariableKeyCatalog.VIH_VALOR_RESULTADO_PRIMERA_PRUEBA, ritem);
    	
        Optional<RdacaaRawRowResult> sa = resultadoPruebaUnoRuleGroupExecutor.execute(input,evaluateAll());
        sa.ifPresent(r -> {
            System.out.println(r.toString());
        });
        Assert.assertTrue("Resultado prueba Uno nulo",
                sa.get().get(RdacaaVariableKeyCatalog.VIH_VALOR_RESULTADO_PRIMERA_PRUEBA)
                        .hasValidationResultByCode(ValidationResultCatalogConstants.CODIGO_ERROR_VIH_RESULTADO_PRUEBA_UNO_NULO)
                        .isPresent());
	}
	
	@Test
	public void whenResultadoPruebaUnoIsExcludedAndValorIsNull() {
		RdacaaRawRow input = new RdacaaRawRow();
    	RowItem ritem = new RowItem();
    	ritem.setItemValue("999990000066666");
    	ritem.setExcludedFromValidation(true);
    	input.addField(RdacaaVariableKeyCatalog.VIH_VALOR_RESULTADO_PRIMERA_PRUEBA, ritem);
        Optional<RdacaaRawRowResult> sa = resultadoPruebaUnoRuleGroupExecutor.execute(input,evaluateAll());
        sa.ifPresent(r -> {
            System.out.println(r.toString());
        });
        Assert.assertTrue("ResultadoPruebaUno",
                sa.get().get(RdacaaVariableKeyCatalog.VIH_VALOR_RESULTADO_PRIMERA_PRUEBA).getValidationResultList().isEmpty());
	}
	
}

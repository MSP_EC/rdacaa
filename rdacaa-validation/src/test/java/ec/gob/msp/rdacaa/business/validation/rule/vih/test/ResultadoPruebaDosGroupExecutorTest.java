package ec.gob.msp.rdacaa.business.validation.rule.vih.test;

import java.util.Optional;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import com.deliveredtechnologies.rulebook.Fact;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaRawRow;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaRawRowResult;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.malla.RowItem;
import ec.gob.msp.rdacaa.business.validation.rule.vih.pruebados.PruebaDosRuleGroupExecutor;
import ec.gob.msp.rdacaa.business.validation.rule.vih.resultadopruebados.ResultadoPruebaDosRuleGroupExecutor;
import ec.gob.msp.rdacaa.test.ValidationTestContext;

/**
*
* @author dmurillo
*/

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = ValidationTestContext.class)
public class ResultadoPruebaDosGroupExecutorTest {

	@Autowired
    private ResultadoPruebaDosRuleGroupExecutor resultadoPruebaDosRuleGroupExecutor;
	@Autowired
	private PruebaDosRuleGroupExecutor pruebaDosRuleGroupExecutor;
	private RdacaaRawRowResult resultTotal = new RdacaaRawRowResult();

	private Optional<RdacaaRawRowResult> evaluatePruebaUno(){
		RdacaaRawRow input = new RdacaaRawRow();
		RowItem ritem = new RowItem();
		ritem.setItemValue("2597");
    	ritem.setExcludedFromValidation(false);
    	input.addField(RdacaaVariableKeyCatalog.VIH_CODIGO_SEGUNDA_PRUEBA, ritem);
    	return pruebaDosRuleGroupExecutor.execute(input);
	}
	
	private Fact evaluateAll(){
		Optional<RdacaaRawRowResult>  resultForEach = evaluatePruebaUno();
		RdacaaRawRowResult currentResult = resultForEach.isPresent() ? resultForEach.get() : new RdacaaRawRowResult();
		resultTotal.putAll(currentResult);
		@SuppressWarnings("unchecked")
		Fact resultTotalFact = new Fact("rdacaa_row_result", resultTotal);
		return resultTotalFact;
	}
	
	/*@Test
	public void whenResultadoPruebaDosIsNullthenError() {
		
		RdacaaRawRow input = new RdacaaRawRow();
		RowItem ritem = new RowItem();
		ritem.setItemValue(null);
    	ritem.setExcludedFromValidation(true);
    	input.addField(RdacaaVariableKeyCatalog.VIH_VALOR_RESULTADO_SEGUNDA_PRUEBA, ritem);
    	
        Optional<RdacaaRawRowResult> sa = resultadoPruebaDosRuleGroupExecutor.execute(input,evaluateAll());
        sa.ifPresent(r -> {
            System.out.println(r.toString());
        });
        Assert.assertTrue("Resultado prueba Uno nulo",
                sa.get().get(RdacaaVariableKeyCatalog.VIH_VALOR_RESULTADO_SEGUNDA_PRUEBA)
                        .hasValidationResultByCode(ValidationResultCatalogConstants.CODIGO_ERROR_VIH_RESULTADO_PRUEBA_DOS_NULO)
                        .isPresent());
	}
	
	@Test
	public void whenResultadoPruebaDosIsExcludedAndValorIsNull() {
		RdacaaRawRow input = new RdacaaRawRow();
    	RowItem ritem = new RowItem();
    	ritem.setItemValue("999990000066666");
    	ritem.setExcludedFromValidation(true);
    	input.addField(RdacaaVariableKeyCatalog.VIH_VALOR_RESULTADO_SEGUNDA_PRUEBA, ritem);
        Optional<RdacaaRawRowResult> sa = resultadoPruebaDosRuleGroupExecutor.execute(input,evaluateAll());
        sa.ifPresent(r -> {
            System.out.println(r.toString());
        });
        Assert.assertTrue("ResultadoPruebaUno",
                sa.get().get(RdacaaVariableKeyCatalog.VIH_VALOR_RESULTADO_SEGUNDA_PRUEBA).getValidationResultList().isEmpty());
	}*/
	
	@Test
	public void whenResultadoPruebaDosIsIncorrecta() {
		RdacaaRawRow input = new RdacaaRawRow();
    	RowItem ritem = new RowItem();
    	ritem.setItemValue("2597");
    	ritem.setExcludedFromValidation(false);
    	input.addField(RdacaaVariableKeyCatalog.VIH_VALOR_RESULTADO_SEGUNDA_PRUEBA, ritem);
        Optional<RdacaaRawRowResult> sa = resultadoPruebaDosRuleGroupExecutor.execute(input,evaluateAll());
        sa.ifPresent(r -> {
            System.out.println(r.toString());
        });
        Assert.assertTrue("ResultadoPruebaDos",
                !sa.get().get(RdacaaVariableKeyCatalog.VIH_VALOR_RESULTADO_SEGUNDA_PRUEBA).getValidationResultList().isEmpty());
	}
}

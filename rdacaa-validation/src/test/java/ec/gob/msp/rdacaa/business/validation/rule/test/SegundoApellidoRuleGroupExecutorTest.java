package ec.gob.msp.rdacaa.business.validation.rule.test;

import java.util.Optional;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaRawRow;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaRawRowResult;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.malla.RowItem;
import ec.gob.msp.rdacaa.business.validation.rule.persona.segundoapellido.SegundoApellidoRuleGroupExecutor;
import ec.gob.msp.rdacaa.test.ValidationTestContext;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = ValidationTestContext.class)
public class SegundoApellidoRuleGroupExecutorTest {

	@Autowired
	private SegundoApellidoRuleGroupExecutor segundoApellidoRuleGroupExecutor;
	
	@Test
    public void whenSegundoApellidoIsExcludedAndValorIsNull(){
    	RdacaaRawRow input = new RdacaaRawRow();
    	RowItem ritem = new RowItem();
    	ritem.setItemValue("999990000066666");
    	ritem.setExcludedFromValidation(true);
    	input.addField(RdacaaVariableKeyCatalog.PERSONA_SEGUNDO_APELLIDO, ritem);
        Optional<RdacaaRawRowResult> sa = segundoApellidoRuleGroupExecutor.execute(input);
        sa.ifPresent(r -> {
            System.out.println(r.toString());
        });
        Assert.assertTrue("SegundoApellido debe ser nulo",
                sa.get().get(RdacaaVariableKeyCatalog.PERSONA_SEGUNDO_APELLIDO).getValidationResultList().isEmpty());
    }
    
    
    @Test
    public void whenSegundoApellidoIsNotExcludedAndValorIsNotNull(){
    	RdacaaRawRow input = new RdacaaRawRow();
    	RowItem ritem = new RowItem();
    	ritem.setItemValue("REDACAA");
    	ritem.setExcludedFromValidation(false);
    	input.addField(RdacaaVariableKeyCatalog.PERSONA_SEGUNDO_APELLIDO, ritem);
        Optional<RdacaaRawRowResult> sa = segundoApellidoRuleGroupExecutor.execute(input);
        sa.ifPresent(r -> {
            System.out.println(r.toString());
        });
        Assert.assertTrue("SegundoApellido debe ser nulo",
                sa.get().get(RdacaaVariableKeyCatalog.PERSONA_SEGUNDO_APELLIDO).getValidationResultList().isEmpty());
    }
	
	
	@Test
	public void whenSegundoApellidoMenorTresCaracteres_thenError() {
		RdacaaRawRow input = new RdacaaRawRow();
		RowItem ritem = new RowItem();
		ritem.setItemValue("AC");
		ritem.setExcludedFromValidation(false);
		input.addField(RdacaaVariableKeyCatalog.PERSONA_SEGUNDO_APELLIDO, ritem);
		Optional<RdacaaRawRowResult> sa = segundoApellidoRuleGroupExecutor.execute(input);
		sa.ifPresent(r -> {
			System.out.println(r.toString());
		});
		Assert.assertTrue("Se espera un error para Segundo Nombre menor a 3 caracteres",
				sa.get().get(RdacaaVariableKeyCatalog.PERSONA_SEGUNDO_APELLIDO)
						.hasValidationResultByCode(ValidationResultCatalogConstants.CODIGO_ERROR_SEGUNDO_APELLIDO_LONG)
						.isPresent());
	}

	@Test
	public void whenSegundoApellidoIsMayorTresCaracteres_thenNoError() {
		RdacaaRawRow input = new RdacaaRawRow();
		RowItem ritem = new RowItem();
		ritem.setItemValue("REDACAA");
		ritem.setExcludedFromValidation(false);
		input.addField(RdacaaVariableKeyCatalog.PERSONA_SEGUNDO_APELLIDO, ritem);
		Optional<RdacaaRawRowResult> sa = segundoApellidoRuleGroupExecutor.execute(input);
		sa.ifPresent(r -> {
			System.out.println(r.toString());
		});
		Assert.assertTrue("No se espera notificaciones",
				sa.get().get(RdacaaVariableKeyCatalog.PERSONA_SEGUNDO_APELLIDO).getValidationResultList().isEmpty());
	}
	
}

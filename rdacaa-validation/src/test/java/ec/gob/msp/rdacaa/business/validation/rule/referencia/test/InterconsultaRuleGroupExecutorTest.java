package ec.gob.msp.rdacaa.business.validation.rule.referencia.test;

import java.util.Optional;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import ec.gob.msp.rdacaa.business.service.ConstantesDetalleCatalogo;
import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaRawRow;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaRawRowResult;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.malla.RowItem;
import ec.gob.msp.rdacaa.business.validation.rule.persona.interconsulta.InterconsultaRuleGroupExecutor;
import ec.gob.msp.rdacaa.test.ValidationTestContext;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = ValidationTestContext.class)
public class InterconsultaRuleGroupExecutorTest {

	@Autowired
    private InterconsultaRuleGroupExecutor interconsultaRuleGroupExecutor;
	
	@Test
    public void whenCodigoInterconsultaIsNull_thenError() {
        RdacaaRawRow input = new RdacaaRawRow();
        RowItem ritem = new RowItem();
        ritem.setItemValue(null);
        ritem.setExcludedFromValidation(false);
        input.addField(RdacaaVariableKeyCatalog.REFERENCIA_CONTRAREFERENCIA_CODIGO_INTERCONSULTA_TIPO, ritem);
        Optional<RdacaaRawRowResult> sa = interconsultaRuleGroupExecutor.execute(input);
        sa.ifPresent(r -> {
            System.out.println(r.toString());
        });
        Assert.assertTrue("Codigo para tipo Interconsulta es obligatorio",
                sa.get().get(RdacaaVariableKeyCatalog.REFERENCIA_CONTRAREFERENCIA_CODIGO_INTERCONSULTA_TIPO)
                        .hasValidationResultByCode(ValidationResultCatalogConstants.CODIGO_ERROR_CODIGO_INTERCONSULTA_NULO)
                        .isPresent());
    }
	
	
	
	@Test
    public void whenInterconsultaIsExcludedAndValorIsNull(){
    	RdacaaRawRow input = new RdacaaRawRow();
    	RowItem ritem = new RowItem();
    	ritem.setItemValue(ConstantesDetalleCatalogo.NOT_DEFINED_VALUE);
    	ritem.setExcludedFromValidation(true);
    	input.addField(RdacaaVariableKeyCatalog.REFERENCIA_CONTRAREFERENCIA_CODIGO_INTERCONSULTA_TIPO, ritem);
        Optional<RdacaaRawRowResult> sa = interconsultaRuleGroupExecutor.execute(input);
        sa.ifPresent(r -> {
            System.out.println(r.toString());
        });
        Assert.assertTrue("Codigo para tipo Interconsulta debe ser nulo",
                sa.get().get(RdacaaVariableKeyCatalog.REFERENCIA_CONTRAREFERENCIA_CODIGO_INTERCONSULTA_TIPO).getValidationResultList().isEmpty());
    }
	
	@Test
	public void whenInterconsultaIsMandatory() {
		RdacaaRawRow input = new RdacaaRawRow();
		RowItem ritem = new RowItem();
		ritem.setItemValue(ConstantesDetalleCatalogo.NOT_DEFINED_VALUE);
		ritem.setExcludedFromValidation(false);
		ritem.setMandatory(true);
		input.addField(RdacaaVariableKeyCatalog.REFERENCIA_CONTRAREFERENCIA_CODIGO_INTERCONSULTA_TIPO, ritem);
		Optional <RdacaaRawRowResult> sa = interconsultaRuleGroupExecutor.execute(input);
		sa.ifPresent(r -> {System.out.println(r.toString());});
		Assert.assertTrue("El codigo para interconsulta es mandatorio",
				sa.get().get(RdacaaVariableKeyCatalog.REFERENCIA_CONTRAREFERENCIA_CODIGO_INTERCONSULTA_TIPO)
				.hasValidationResultByCode(
						ValidationResultCatalogConstants.CODIGO_ERROR_CODIGO_INTERCONSULTA_ES_MANDATORIO)
				.isPresent()
				);
	}
	
	@Test
    public void whenInterconsultaIsNotNumeric_thenError() {
        RdacaaRawRow input = new RdacaaRawRow();
        RowItem ritem = new RowItem();
        ritem.setItemValue("dfasdf");
        ritem.setExcludedFromValidation(false);
        input.addField(RdacaaVariableKeyCatalog.REFERENCIA_CONTRAREFERENCIA_CODIGO_INTERCONSULTA_TIPO, ritem);
        Optional<RdacaaRawRowResult> sa = interconsultaRuleGroupExecutor.execute(input);
        sa.ifPresent(r -> {
            System.out.println(r.toString());
        });
        Assert.assertTrue("Se espera un error para caracteres no numericos ",
                sa.get().get(RdacaaVariableKeyCatalog.REFERENCIA_CONTRAREFERENCIA_CODIGO_INTERCONSULTA_TIPO)
                        .hasValidationResultByCode(ValidationResultCatalogConstants.CODIGO_ERROR_CODIGO_INTERCONSULTA_NO_NUMERICO)
                        .isPresent());
    }
	
	 @Test
	    public void whenInterconsultaIsNotCorrect_thenError() {
	        RdacaaRawRow input = new RdacaaRawRow();
	        RowItem ritem = new RowItem();
	        ritem.setItemValue("666");
	        ritem.setExcludedFromValidation(false);
	        input.addField(RdacaaVariableKeyCatalog.REFERENCIA_CONTRAREFERENCIA_CODIGO_INTERCONSULTA_TIPO, ritem);
	        Optional<RdacaaRawRowResult> sa = interconsultaRuleGroupExecutor.execute(input);
	        sa.ifPresent(r -> {
	            System.out.println(r.toString());
	        });
	        Assert.assertTrue("Se espera un error para codigo incorrecto ",
	                sa.get().get(RdacaaVariableKeyCatalog.REFERENCIA_CONTRAREFERENCIA_CODIGO_INTERCONSULTA_TIPO)
	                        .hasValidationResultByCode(ValidationResultCatalogConstants.CODIGO_ERROR_CODIGO_INTERCONSULTA_INCORRECTO)
	                        .isPresent());
	    }
	 
	 
	    @Test
	    public void whenInterconsultaIsCorrect_thenOk() {
			RdacaaRawRow input = new RdacaaRawRow();
			RowItem ritem = new RowItem();
			ritem.setItemValue("2587");
			ritem.setExcludedFromValidation(false);
	        input.addField(RdacaaVariableKeyCatalog.REFERENCIA_CONTRAREFERENCIA_CODIGO_INTERCONSULTA_TIPO, ritem);
	        Optional<RdacaaRawRowResult> sa = interconsultaRuleGroupExecutor.execute(input);
	        sa.ifPresent(r -> {
	            System.out.println(r.toString());
	        });
	        Assert.assertTrue("No se esperan errores",
	                sa.get().get(RdacaaVariableKeyCatalog.REFERENCIA_CONTRAREFERENCIA_CODIGO_INTERCONSULTA_TIPO).getValidationResultList().isEmpty());
		}
}

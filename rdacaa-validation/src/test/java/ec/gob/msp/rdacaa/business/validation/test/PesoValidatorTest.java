/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.gob.msp.rdacaa.business.validation.test;

import static ec.gob.msp.rdacaa.business.validation.PesoValidator.PESO_MAX_FUERA_RANGO;
import static ec.gob.msp.rdacaa.business.validation.PesoValidator.PESO_MIN_FUERA_RANGO;

import java.util.List;
import java.util.Optional;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import ec.gob.msp.rdacaa.business.validation.PesoValidator;
import ec.gob.msp.rdacaa.business.validation.common.ValidationResult;
import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalog;
import ec.gob.msp.rdacaa.test.ValidationTestContext;

/**
 *
 * @author Saulo Velasco
 */
@RunWith(SpringRunner.class)
@ContextConfiguration(classes = ValidationTestContext.class)
public class PesoValidatorTest {

    @Autowired
    private PesoValidator pesoValidator;

    @Test
    public void whenPesoNullOrEmpty_thenPesoVacioError() {
        String[] pesoEdadSexo = {"", "050600", "F"};
        List<ValidationResult> listaErrores = pesoValidator.validate(pesoEdadSexo);
        Optional<ValidationResult> error = listaErrores.parallelStream().filter(e -> {
            return e.getCode().equals(ValidationResultCatalog.PESO_VACIO.getValidationError().getCode());

        }).findAny();
        Assert.assertTrue("Se espera un error para peso nulo", error.isPresent());
    }

    /**
     * Min 14.36 Max 38.11 Edad Min 06-10-00 Edad Max 06-10-30 F
     */
    @Test
    public void whenMujerPesoFueraRangoEdad_thenError() {
        String[] pesoEdadSexo = {"13.00", "061000", "F"};
        List<ValidationResult> listaErrores = pesoValidator.validate(pesoEdadSexo);
        Optional<ValidationResult> error = listaErrores.parallelStream().filter(e -> {
            return e.getCode().equals(ValidationResultCatalog.PESO_FUERA_RANGO_PARA_EDAD.getValidationError().getCode());

        }).findAny();
        Assert.assertTrue("Se espera un error PESO_FUERA_RANGO_PARA_EDAD para peso valor fuera de rango", error.isPresent());
    }

    /**
     * Min 14.36 Max 38.11 Edad Min 06-10-00 Edad Max 06-10-30 F
     */
    @Test
    public void whenMujerPesoDentroRangoEdad_thenNoError() {
        String[] pesoEdadSexo = {"15.00", "061000", "F"};
        List<ValidationResult> listaErrores = pesoValidator.validate(pesoEdadSexo);
        Assert.assertTrue("No se esperan errores para valor dentro de rango", listaErrores.isEmpty());
    }

    /**
     * Cuando edad mayor a 09-11-30 entonces error para peso >=
     * PESO_MIN_FUERA_RANGO 0.5 y peso <= PESO_MAX_FUERA_RANGO 500 
     * fuera de rango
     */
    @Test
    public void whenAllSexoMayorEdadFueraRangoPeso_thenError() {
        String[] pesoEdadSexo = {"550.00", "100100", "F"};
        List<ValidationResult> listaErrores = pesoValidator.validate(pesoEdadSexo);
        Optional<ValidationResult> error = listaErrores.parallelStream().filter(e -> {
            return e.getCode().equals(ValidationResultCatalog.PESO_FUERA_RANGO_PARA_EDAD.getValidationError().getCode());

        }).findAny();
        Assert.assertTrue("Se espera un error PESO_FUERA_RANGO_PARA_EDAD para peso valor fuera de rango >= "
                + PESO_MIN_FUERA_RANGO + "y <= " + PESO_MAX_FUERA_RANGO, error.isPresent());
    }
    
     /**
     * Cuando edad mayor a 09-11-30 entonces no error para peso >=
     * PESO_MIN_FUERA_RANGO 0.5 y peso <= PESO_MAX_FUERA_RANGO 500
     * dentro de rango
     */
    @Test
    public void whenAllSexoMayorEdadDentroRangoPeso_thenNoError() {
        String[] pesoEdadSexo = {"400.00", "100100", "F"};
        List<ValidationResult> listaErrores = pesoValidator.validate(pesoEdadSexo);
        Assert.assertTrue("No se espera error para rango >= "
                + PESO_MIN_FUERA_RANGO + "y <= " + PESO_MAX_FUERA_RANGO, listaErrores.isEmpty());
    }
}

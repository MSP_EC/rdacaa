/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.gob.msp.rdacaa.business.validation.rule.referencia.test;

import ec.gob.msp.rdacaa.business.service.ConstantesDetalleCatalogo;
import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaRawRow;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaRawRowResult;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.malla.RowItem;
import ec.gob.msp.rdacaa.business.validation.rule.atencion.referencia.lugar.LugarReferenciaRuleGroupExecutor;
import ec.gob.msp.rdacaa.test.ValidationTestContext;
import java.util.Optional;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

/**
 *
 * @author eduardo
 */
@RunWith(SpringRunner.class)
@ContextConfiguration(classes = ValidationTestContext.class)
public class ReferenciaLugarRuleGroupExecutorTest {
    
    @Autowired
    private LugarReferenciaRuleGroupExecutor LugarReferenciaRuleGroupExecutor;
    
    @Test
    public void whenReferenciaLugarIsNull_thenError() {
        RdacaaRawRow input = new RdacaaRawRow();
        RowItem ritem = new RowItem();
        ritem.setItemValue(null);
        ritem.setExcludedFromValidation(false);
        input.addField(RdacaaVariableKeyCatalog.REFERENCIA_CONTRAREFERENCIA_UNICODIGO_LUGAR_REFERIDO, ritem);
        Optional<RdacaaRawRowResult> sa = LugarReferenciaRuleGroupExecutor.execute(input);
        sa.ifPresent(r -> {
            System.out.println(r.toString());
        });
        Assert.assertTrue("Lugar de Referencia es obligatorio",
                sa.get().get(RdacaaVariableKeyCatalog.REFERENCIA_CONTRAREFERENCIA_UNICODIGO_LUGAR_REFERIDO)
                        .hasValidationResultByCode(ValidationResultCatalogConstants.CODIGO_ERROR_REFERENCIA_LUGAR_NULO)
                        .isPresent());
    }
    
	@Test
    public void whenLugarReferenciaIsExcludedAndValorIsNull(){
    	RdacaaRawRow input = new RdacaaRawRow();
    	RowItem ritem = new RowItem();
    	ritem.setItemValue(ConstantesDetalleCatalogo.NOT_DEFINED_VALUE);
    	ritem.setExcludedFromValidation(true);
    	input.addField(RdacaaVariableKeyCatalog.REFERENCIA_CONTRAREFERENCIA_UNICODIGO_LUGAR_REFERIDO, ritem);
        Optional<RdacaaRawRowResult> sa = LugarReferenciaRuleGroupExecutor.execute(input);
        sa.ifPresent(r -> {
            System.out.println(r.toString());
        });
        Assert.assertTrue("Lugar de referencia debe ser nulo",
                sa.get().get(RdacaaVariableKeyCatalog.REFERENCIA_CONTRAREFERENCIA_UNICODIGO_LUGAR_REFERIDO).getValidationResultList().isEmpty());
    }
    
	
	@Test
	public void whenLugarReferenciaIsMandatory() {
		RdacaaRawRow input = new RdacaaRawRow();
		RowItem ritem = new RowItem();
		ritem.setItemValue(ConstantesDetalleCatalogo.NOT_DEFINED_VALUE);
		ritem.setExcludedFromValidation(false);
		ritem.setMandatory(true);
		input.addField(RdacaaVariableKeyCatalog.REFERENCIA_CONTRAREFERENCIA_UNICODIGO_LUGAR_REFERIDO, ritem);
		Optional <RdacaaRawRowResult> sa = LugarReferenciaRuleGroupExecutor.execute(input);
		sa.ifPresent(r -> {System.out.println(r.toString());});
		Assert.assertTrue("El codigo de lugar de referencia es mandatorio",
				sa.get().get(RdacaaVariableKeyCatalog.REFERENCIA_CONTRAREFERENCIA_UNICODIGO_LUGAR_REFERIDO)
				.hasValidationResultByCode(
						ValidationResultCatalogConstants.CODIGO_ERROR_REFERENCIA_LUGAR_ES_MANDATORIO)
				.isPresent()
				);
	}
	
	@Test
    public void whenLugarReferenciaIsNotNumeric_thenError() {
        RdacaaRawRow input = new RdacaaRawRow();
        RowItem ritem = new RowItem();
        ritem.setItemValue("dfasdf");
        ritem.setExcludedFromValidation(false);
        input.addField(RdacaaVariableKeyCatalog.REFERENCIA_CONTRAREFERENCIA_UNICODIGO_LUGAR_REFERIDO, ritem);
        Optional<RdacaaRawRowResult> sa = LugarReferenciaRuleGroupExecutor.execute(input);
        sa.ifPresent(r -> {
            System.out.println(r.toString());
        });
        Assert.assertTrue("Se espera un error para caracteres no numericos ",
                sa.get().get(RdacaaVariableKeyCatalog.REFERENCIA_CONTRAREFERENCIA_UNICODIGO_LUGAR_REFERIDO)
                        .hasValidationResultByCode(ValidationResultCatalogConstants.CODIGO_ERROR_REFERENCIA_LUGAR_NO_NUMERICO)
                        .isPresent());
    }
	
	 @Test
	    public void whenLugarReferenciaIsNotCorrect_thenError() {
	        RdacaaRawRow input = new RdacaaRawRow();
	        RowItem ritem = new RowItem();
	        ritem.setItemValue("6664664");
	        ritem.setExcludedFromValidation(false);
	        input.addField(RdacaaVariableKeyCatalog.REFERENCIA_CONTRAREFERENCIA_UNICODIGO_LUGAR_REFERIDO, ritem);
	        Optional<RdacaaRawRowResult> sa = LugarReferenciaRuleGroupExecutor.execute(input);
	        sa.ifPresent(r -> {
	            System.out.println(r.toString());
	        });
	        Assert.assertTrue("Se espera un error para codigo incorrecto ",
	                sa.get().get(RdacaaVariableKeyCatalog.REFERENCIA_CONTRAREFERENCIA_UNICODIGO_LUGAR_REFERIDO)
	                        .hasValidationResultByCode(ValidationResultCatalogConstants.CODIGO_ERROR_REFERENCIA_LUGAR_INCORRECTO)
	                        .isPresent());
	    }
//	    @Test
//	    public void whenLugarReferenciaIsCorrect_thenOk() {
//			RdacaaRawRow input = new RdacaaRawRow();
//			RowItem ritem = new RowItem();
//			ritem.setItemValue("2583");
//			ritem.setExcludedFromValidation(false);
//	        input.addField(RdacaaVariableKeyCatalog.REFERENCIA_CONTRAREFERENCIA_UNICODIGO_LUGAR_REFERIDO, ritem);
//	        Optional<RdacaaRawRowResult> sa = LugarReferenciaRuleGroupExecutor.execute(input);
//	        sa.ifPresent(r -> {
//	            System.out.println(r.toString());
//	        });
//	        Assert.assertTrue("No se esperan errores",
//	                sa.get().get(RdacaaVariableKeyCatalog.REFERENCIA_CONTRAREFERENCIA_UNICODIGO_LUGAR_REFERIDO).getValidationResultList().isEmpty());
//		}
}

package ec.gob.msp.rdacaa.business.validation.rule.test;

import java.util.Optional;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaRawRow;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaRawRowResult;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.malla.RowItem;
import ec.gob.msp.rdacaa.business.validation.rule.persona.identificacionetnica.IdentificacionEtnicaRuleGroupExecutor;
import ec.gob.msp.rdacaa.test.ValidationTestContext;

/**
*
* @author dmurillo
*/
@RunWith(SpringRunner.class)
@ContextConfiguration(classes = ValidationTestContext.class)
public class IdentificacionEtnicaRuleGroupExecutorTest {

	@Autowired
    private IdentificacionEtnicaRuleGroupExecutor identificacionEtnicaRuleGroupExecutor;
	
	@Test
    public void whenIdentificacionEtnicaIsExcludedAndValorIsNull(){
    	RdacaaRawRow input = new RdacaaRawRow();
    	RowItem ritem = new RowItem();
    	ritem.setItemValue("999990000066666");
    	ritem.setExcludedFromValidation(true);
    	input.addField(RdacaaVariableKeyCatalog.PERSONA_CODIGO_AUTOIDENTIFICACION_ETNICA, ritem);
        Optional<RdacaaRawRowResult> sa = identificacionEtnicaRuleGroupExecutor.execute(input);
        sa.ifPresent(r -> {
            System.out.println(r.toString());
        });
        Assert.assertTrue("IdentificacionEtnica debe ser nulo",
                sa.get().get(RdacaaVariableKeyCatalog.PERSONA_CODIGO_AUTOIDENTIFICACION_ETNICA).getValidationResultList().isEmpty());
    }
    
    @Test
    public void whenIdentificacionEtnicaIsExcludedAndValorIsNotNull(){
    	RdacaaRawRow input = new RdacaaRawRow();
    	RowItem ritem = new RowItem();
    	ritem.setItemValue("23");
    	ritem.setExcludedFromValidation(true);
    	input.addField(RdacaaVariableKeyCatalog.PERSONA_CODIGO_AUTOIDENTIFICACION_ETNICA, ritem);
        Optional<RdacaaRawRowResult> sa = identificacionEtnicaRuleGroupExecutor.execute(input);
        sa.ifPresent(r -> {
            System.out.println(r.toString());
        });
        Assert.assertTrue("IdentificacionEtnica debe ser nulo",
                sa.get().get(RdacaaVariableKeyCatalog.PERSONA_CODIGO_AUTOIDENTIFICACION_ETNICA)
                        .hasValidationResultByCode(ValidationResultCatalogConstants.CODIGO_ERROR_IDENTIFICACION_ETNICA_NO_DEFINIDA)
                        .isPresent());
    }

    @Test
    public void whenIdentificacionEtnicaIsNull_thenError() {
        RdacaaRawRow input = new RdacaaRawRow();
        RowItem ritem = new RowItem();
        ritem.setItemValue(null);
        ritem.setExcludedFromValidation(false);
        input.addField(RdacaaVariableKeyCatalog.PERSONA_CODIGO_AUTOIDENTIFICACION_ETNICA, ritem);
        Optional<RdacaaRawRowResult> sa = identificacionEtnicaRuleGroupExecutor.execute(input);
        sa.ifPresent(r -> {
            System.out.println(r.toString());
        });
        Assert.assertTrue("IdentificacionEtnica es obligatorio",
                sa.get().get(RdacaaVariableKeyCatalog.PERSONA_CODIGO_AUTOIDENTIFICACION_ETNICA)
                        .hasValidationResultByCode(ValidationResultCatalogConstants.CODIGO_ERROR_IDENTIFICACION_ETNICA_NULO)
                        .isPresent());
    }
    
    @Test
    public void whenIdentificacionEtnicaIncorrect() {
    	RdacaaRawRow input = new RdacaaRawRow();
    	RowItem ritemNac = new RowItem();
    	ritemNac.setItemValue("1");
    	ritemNac.setExcludedFromValidation(false);
        RowItem ritem = new RowItem();
        ritem.setItemValue("486");
        ritem.setExcludedFromValidation(false);
        input.addField(RdacaaVariableKeyCatalog.PERSONA_CODIGO_NACIONALIDAD, ritemNac);
        input.addField(RdacaaVariableKeyCatalog.PERSONA_CODIGO_AUTOIDENTIFICACION_ETNICA, ritem);
        Optional<RdacaaRawRowResult> sa = identificacionEtnicaRuleGroupExecutor.execute(input);
        sa.ifPresent(r -> {
            System.out.println(r.toString());
        });
        Assert.assertTrue("IdentificacionEtnica Incorrecto",
                sa.get().get(RdacaaVariableKeyCatalog.PERSONA_CODIGO_AUTOIDENTIFICACION_ETNICA)
                        .hasValidationResultByCode(ValidationResultCatalogConstants.CODIGO_ERROR_IDENTIFICACION_ETNICA_INCORRECTO)
                        .isPresent());
    }
}

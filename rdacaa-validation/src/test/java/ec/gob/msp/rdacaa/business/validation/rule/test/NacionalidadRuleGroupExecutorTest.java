package ec.gob.msp.rdacaa.business.validation.rule.test;

import java.util.Optional;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaRawRow;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaRawRowResult;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.malla.RowItem;
import ec.gob.msp.rdacaa.business.validation.rule.persona.nacionalidad.NacionalidadRuleGroupExecutor;
import ec.gob.msp.rdacaa.test.ValidationTestContext;

/**
*
* @author dmurillo
*/
@RunWith(SpringRunner.class)
@ContextConfiguration(classes = ValidationTestContext.class)
public class NacionalidadRuleGroupExecutorTest {

	@Autowired
	private NacionalidadRuleGroupExecutor nacionalidadRuleGroupExecutor;
	
	@Test
    public void whenNacionalidadIsExcludedAndValorIsNull(){
    	RdacaaRawRow input = new RdacaaRawRow();
    	RowItem ritem = new RowItem();
    	ritem.setItemValue("999990000066666");
    	ritem.setExcludedFromValidation(true);
    	input.addField(RdacaaVariableKeyCatalog.PERSONA_CODIGO_NACIONALIDAD, ritem);
        Optional<RdacaaRawRowResult> sa = nacionalidadRuleGroupExecutor.execute(input);
        sa.ifPresent(r -> {
            System.out.println(r.toString());
        });
        Assert.assertTrue("Nacionalidad debe ser nulo",
                sa.get().get(RdacaaVariableKeyCatalog.PERSONA_CODIGO_NACIONALIDAD).getValidationResultList().isEmpty());
    }
    
    @Test
    public void whenNacionalidadIsExcludedAndValorIsNotNull(){
    	RdacaaRawRow input = new RdacaaRawRow();
    	RowItem ritem = new RowItem();
    	ritem.setItemValue("1");
    	ritem.setExcludedFromValidation(true);
    	input.addField(RdacaaVariableKeyCatalog.PERSONA_CODIGO_NACIONALIDAD, ritem);
        Optional<RdacaaRawRowResult> sa = nacionalidadRuleGroupExecutor.execute(input);
        sa.ifPresent(r -> {
            System.out.println(r.toString());
        });
        Assert.assertTrue("Nacionalidad debe ser nulo",
                sa.get().get(RdacaaVariableKeyCatalog.PERSONA_CODIGO_NACIONALIDAD)
                        .hasValidationResultByCode(ValidationResultCatalogConstants.CODIGO_ERROR_NACIONALIDAD_NO_DEFINIDO)
                        .isPresent());
    }
    
    @Test
    public void whenNacionalidadIsNotExcludedAndValorIsNotNull(){
    	RdacaaRawRow input = new RdacaaRawRow();
    	RowItem ritem = new RowItem();
    	ritem.setItemValue("1");
    	ritem.setExcludedFromValidation(false);
    	input.addField(RdacaaVariableKeyCatalog.PERSONA_CODIGO_NACIONALIDAD, ritem);
        Optional<RdacaaRawRowResult> sa = nacionalidadRuleGroupExecutor.execute(input);
        sa.ifPresent(r -> {
            System.out.println(r.toString());
        });
        Assert.assertTrue("Nacionalidad debe ser nulo",
                sa.get().get(RdacaaVariableKeyCatalog.PERSONA_CODIGO_NACIONALIDAD).getValidationResultList().isEmpty());
    }

	
	@Test
    public void whenNacionalidadIsNull_thenError() {
        RdacaaRawRow input = new RdacaaRawRow();
        RowItem ritem = new RowItem();
        ritem.setItemValue(null);
        ritem.setExcludedFromValidation(false);
        input.addField(RdacaaVariableKeyCatalog.PERSONA_CODIGO_NACIONALIDAD, ritem);
        Optional<RdacaaRawRowResult> sa = nacionalidadRuleGroupExecutor.execute(input);
        sa.ifPresent(r -> {
            System.out.println(r.toString());
        });
        Assert.assertTrue("El campo es obligatorio",
                sa.get().get(RdacaaVariableKeyCatalog.PERSONA_CODIGO_NACIONALIDAD)
                        .hasValidationResultByCode(ValidationResultCatalogConstants.CODIGO_ERROR_NACIONALIDAD_NULO)
                        .isPresent());
    }
	
	@Test
    public void whenNacionalidadIsNotNumeric_thenError() {
        RdacaaRawRow input = new RdacaaRawRow();
        RowItem ritem = new RowItem();
        ritem.setItemValue("REDACAA");
        ritem.setExcludedFromValidation(false);
        input.addField(RdacaaVariableKeyCatalog.PERSONA_CODIGO_NACIONALIDAD, ritem);
        Optional<RdacaaRawRowResult> sa = nacionalidadRuleGroupExecutor.execute(input);
        sa.ifPresent(r -> {
            System.out.println(r.toString());
        });
        Assert.assertTrue("Se espera un error para caracteres no numericos ",
                sa.get().get(RdacaaVariableKeyCatalog.PERSONA_CODIGO_NACIONALIDAD)
                        .hasValidationResultByCode(ValidationResultCatalogConstants.CODIGO_ERROR_NACIONALIDAD_NONUMERICA)
                        .isPresent());
    }
	
	@Test
    public void whenNacionalidadIsIncorrecto_thenError() {
        RdacaaRawRow input = new RdacaaRawRow();
        RowItem ritem = new RowItem();
        ritem.setItemValue("300");
        ritem.setExcludedFromValidation(false);
        input.addField(RdacaaVariableKeyCatalog.PERSONA_CODIGO_NACIONALIDAD, ritem);
        Optional<RdacaaRawRowResult> sa = nacionalidadRuleGroupExecutor.execute(input);
        sa.ifPresent(r -> {
            System.out.println(r.toString());
        });
        Assert.assertTrue("El campo es incorrecto",
                sa.get().get(RdacaaVariableKeyCatalog.PERSONA_CODIGO_NACIONALIDAD)
                        .hasValidationResultByCode(ValidationResultCatalogConstants.CODIGO_ERROR_NACIONALIDAD_INCORRECTO)
                        .isPresent());
    }
	
	@Test
    public void whenSexoIsCorrecto_thenOk() {
        RdacaaRawRow input = new RdacaaRawRow();
        RowItem ritem = new RowItem();
        ritem.setItemValue("1");
        ritem.setExcludedFromValidation(false);
        input.addField(RdacaaVariableKeyCatalog.PERSONA_CODIGO_NACIONALIDAD, ritem);
        Optional<RdacaaRawRowResult> sa = nacionalidadRuleGroupExecutor.execute(input);
        sa.ifPresent(r -> {
            System.out.println(r.toString());
        });
        Assert.assertTrue("No se espera error para este test",
                sa.get().get(RdacaaVariableKeyCatalog.PERSONA_CODIGO_NACIONALIDAD).getValidationResultList().isEmpty());
    }
}

package ec.gob.msp.rdacaa.business.validation.rule.test;

import java.util.Optional;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaRawRow;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaRawRowResult;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.malla.RowItem;
import ec.gob.msp.rdacaa.business.validation.rule.persona.pueblos.PueblosRuleGroupExecutor;
import ec.gob.msp.rdacaa.test.ValidationTestContext;

/**
*
* @author dmurillo
*/
@RunWith(SpringRunner.class)
@ContextConfiguration(classes = ValidationTestContext.class)
public class PueblosRuleGroupExecutorTest {

	@Autowired
	private PueblosRuleGroupExecutor pueblosRuleGroupExecutor;
	
	@Test
    public void whenPueblosIsExcludedAndValorIsNull(){
    	RdacaaRawRow input = new RdacaaRawRow();
    	RowItem ritem = new RowItem();
    	ritem.setItemValue("999990000066666");
    	ritem.setExcludedFromValidation(true);
    	input.addField(RdacaaVariableKeyCatalog.PERSONA_CODIGO_PUEBLO, ritem);
        Optional<RdacaaRawRowResult> sa = pueblosRuleGroupExecutor.execute(input);
        sa.ifPresent(r -> {
            System.out.println(r.toString());
        });
        Assert.assertTrue("Pueblos debe ser nulo",
                sa.get().get(RdacaaVariableKeyCatalog.PERSONA_CODIGO_PUEBLO).getValidationResultList().isEmpty());
    }
    
    @Test
    public void whenPueblosIsNotExcludedAndValorIsCorrect(){
    	RdacaaRawRow input = new RdacaaRawRow();
    	RowItem ritem = new RowItem();
    	ritem.setItemValue("44");
    	ritem.setExcludedFromValidation(false);
    	RowItem ritemNac = new RowItem();
    	ritemNac.setItemValue("32");
    	ritemNac.setExcludedFromValidation(false);
    	input.addField(RdacaaVariableKeyCatalog.PERSONA_CODIGO_NACIONALIDAD_ETNICA, ritemNac);
    	input.addField(RdacaaVariableKeyCatalog.PERSONA_CODIGO_PUEBLO, ritem);
        Optional<RdacaaRawRowResult> sa = pueblosRuleGroupExecutor.execute(input);
        sa.ifPresent(r -> {
            System.out.println(r.toString());
        });
        Assert.assertTrue("Pueblos debe ser nulo",
                sa.get().get(RdacaaVariableKeyCatalog.PERSONA_CODIGO_PUEBLO).getValidationResultList().isEmpty());
    }


    @Test
    public void whenPueblosIsNull_thenError() {
        RdacaaRawRow input = new RdacaaRawRow();
        RowItem ritem = new RowItem();
        ritem.setItemValue(null);
        ritem.setExcludedFromValidation(false);
        input.addField(RdacaaVariableKeyCatalog.PERSONA_CODIGO_PUEBLO, ritem);
        Optional<RdacaaRawRowResult> sa = pueblosRuleGroupExecutor.execute(input);
        sa.ifPresent(r -> {
            System.out.println(r.toString());
        });
        Assert.assertTrue("Pueblos es obligatorio",
                sa.get().get(RdacaaVariableKeyCatalog.PERSONA_CODIGO_PUEBLO)
                        .hasValidationResultByCode(ValidationResultCatalogConstants.CODIGO_ERROR_PUEBLOS_NULO)
                        .isPresent());
    }
}

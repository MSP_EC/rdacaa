package ec.gob.msp.rdacaa.business.validation.rule.atencion.sivan.leche24horas;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.UtilsRule;

/**
 *
 * @author miguel.faubla
 */
@Rule(order = 6)
public class Leche24HorasIsIncorrect extends UtilsRule {

	@When
	public boolean when() {
		return true;
	}

	@Then
	public RuleState then() {
		String leche24Horas = getVariableFromMap(RdacaaVariableKeyCatalog.SIVAN_RECIBIO_LECHE_ULTIMAS_24H);
		
		Integer edadAniosMesesDias = getEdadAnioMesDias();

		boolean isValidValue = "0".equals(leche24Horas) || "1".equals(leche24Horas);
		
		if (!isValidValue) {
			addValidationResult(RdacaaVariableKeyCatalog.SIVAN_RECIBIO_LECHE_ULTIMAS_24H,
					ValidationResultCatalogConstants.CODIGO_ERROR_SIVAN_RECIBIO_LECHE_ULTIMAS_24H_ES_INCORRECTO);
		}

		if (edadAniosMesesDias == null) {
			addValidationResult(RdacaaVariableKeyCatalog.SIVAN_RECIBIO_LECHE_ULTIMAS_24H,
					ValidationResultCatalogConstants.CODIGO_ERROR_SIVAN_RECIBIO_LECHE_ULTIMAS_24H_FECHAS_INVALIDAS);
			return RuleState.BREAK;
		}

		boolean requiereEdad0mesesA5meses = !(edadAniosMesesDias > 0 && edadAniosMesesDias < 600)
				&& !ValidationSupport.isNotDefined(leche24Horas);
		if (requiereEdad0mesesA5meses) {
			addValidationResult(RdacaaVariableKeyCatalog.SIVAN_RECIBIO_LECHE_ULTIMAS_24H,
					ValidationResultCatalogConstants.CODIGO_ERROR_SIVAN_RECIBIO_LECHE_ULTIMAS_24H_DEBE_TENER_DEFAULT_VALUE);
		}
		
		if(!isValidValue || requiereEdad0mesesA5meses) {
			return RuleState.BREAK;
		}

		return RuleState.NEXT;
	}
}

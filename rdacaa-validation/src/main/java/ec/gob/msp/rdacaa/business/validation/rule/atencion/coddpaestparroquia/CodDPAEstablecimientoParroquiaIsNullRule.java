package ec.gob.msp.rdacaa.business.validation.rule.atencion.coddpaestparroquia;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;

/**
*
* @author dmurillo
*/
@Rule(order = 2)
public class CodDPAEstablecimientoParroquiaIsNullRule extends BaseRule<String> {

	@When
	public boolean when() {
		String parroquia = getVariableFromMap(RdacaaVariableKeyCatalog.ATENCION_UBICACION_PARROQUIA);
		return ValidationSupport.isNullOrEmptyString(parroquia);
	}

	@Then
	public RuleState then() {
		addValidationResult(RdacaaVariableKeyCatalog.ATENCION_UBICACION_PARROQUIA,
				ValidationResultCatalogConstants.CODIGO_ERROR_ATENCION_UBICACION_PARROQUIA_NULO);
		return RuleState.BREAK;
	}
}

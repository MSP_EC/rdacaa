package ec.gob.msp.rdacaa.business.validation.rule.atencion.datosantropometricos.peso;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Given;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.service.SignosVitalesReglasValidacion;
import ec.gob.msp.rdacaa.business.validation.common.ValidationResult;
import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.UtilsRule;

@Rule(order = 6)
public class PesoValidateForAgeRule extends UtilsRule {

	private static final int EDAD_CINCO_ANIO_UN_MES_CERO_DIA = 50100;
	private static final double PESO_MAXIMO = 500;
	private static final double PESO_MINIMO = 0.5d;
	

	@Given("reglasPeso")
	private List<SignosVitalesReglasValidacion> reglasPeso;

	@When
	public boolean when() {
		return true;
	}

	@Then
	public RuleState then() {

		String peso = getVariableFromMap(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_PESO);
		String sexo = getSexoString();
		Integer edadAnioMesDias = getEdadAnioMesDias();

		if (edadAnioMesDias == null) {
			addValidationResult(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_PESO,
					ValidationResultCatalogConstants.CODIGO_ERROR_DAT_ANT_PESO_FECHAS_FORMATO_INCORRECTO);
			// error
			return RuleState.BREAK;
		}
		
		if(sexo == null) {
			addValidationResult(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_PESO,
					ValidationResultCatalogConstants.CODIGO_ERROR_DAT_ANT_PESO_FECHAS_SEXO_INCORRECTO);
			// error
			return RuleState.BREAK;
		}

		if (isEmbarazada() || edadAnioMesDias < EDAD_CINCO_ANIO_UN_MES_CERO_DIA) {
			if (ValidationSupport.isNotDefined(peso)) {
				addValidationResult(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_PESO,
						ValidationResultCatalogConstants.CODIGO_ERROR_DAT_ANT_PESO_ES_MANDATORIA_MENOR_050100_ANIOS_O_EMBARAZADA);
				// error
				return RuleState.BREAK;
			} else {
				// no error
				// validar peso
				return validarPeso(peso, edadAnioMesDias, sexo);
			}
		} else {
			if (ValidationSupport.isNotDefined(peso)) {
				// no error
				return RuleState.BREAK;
			} else {
				// no error
				// validar peso
				return validarPeso(peso, edadAnioMesDias, sexo);
			}
		}

	}

	private RuleState validarPeso(String pesoString, Integer edadAnioMesDias, String sexo) {
		Double peso;
		try {
			peso = Double.valueOf(pesoString);
		} catch (Exception e) {
			addValidationResult(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_PESO,
					ValidationResultCatalogConstants.CODIGO_ERROR_DAT_ANT_PESO_NO_ES_NUMERO_VALIDO);
			return RuleState.BREAK;
		}
		
		BigDecimal pesoBD = BigDecimal.valueOf(peso);

		if (peso >= PESO_MINIMO && peso <= PESO_MAXIMO) {
			
			Optional<ValidationResult> procesarPesoParaEdad = procesarPesoParaEdad(pesoBD, edadAnioMesDias, sexo) ;
			if(procesarPesoParaEdad.isPresent()) {
				addValidationResult(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_PESO, procesarPesoParaEdad.get());
			}
			
			return RuleState.NEXT;
		} else {
			ValidationResult error = getValidationResultFromMap(
					ValidationResultCatalogConstants.CODIGO_ERROR_DAT_ANT_PESO_FUERA_RANGO);
			error.setMessage(error.getMessage() + " DEBE ESTAR ENTRE LOS VALORES DE " + PESO_MINIMO + " A "
					+ PESO_MAXIMO + " kg");
			addValidationResult(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_PESO, error);
			return RuleState.BREAK;
		}

	}

	private Optional<ValidationResult> procesarPesoParaEdad(BigDecimal peso, Integer edad, String sexo) {
		for (SignosVitalesReglasValidacion regla : reglasPeso) {
			if (isEdadPacienteDentroRango(edad, regla.getEdadminima(), regla.getEdadmaxima())
					&& isSexoCorrespondiente(sexo, regla.getSexo())
					&& isValorFueraDeRango(peso, regla.getValorminimo(), regla.getValormaximo())) {
				ValidationResult warning = getValidationResultFromMap(
						ValidationResultCatalogConstants.CODIGO_ADV_DAT_ANT_PESO_FUERA_RANGO_PARA_EDAD);
				String message = warning.getMessage();
				message = message.replace("act", peso.toString());
				message = message.replace("pesomin", regla.getValorminimo().toString());
				message = message.replace("pesomax", regla.getValormaximo().toString());
				message = message.replace("edadmin", regla.getEdadminimaAnios() + " años " + regla.getEdadminimaMeses()
						+ " meses " + regla.getEdadminimaDias() + " días");
				message = message.replace("edadmax", regla.getEdadmaximaAnios() + " años " + regla.getEdadmaximaMeses()
						+ " meses " + regla.getEdadmaximaDias() + " días");
				warning.setMessage(message);
				return Optional.of(warning);
			}
		}
		return Optional.empty();
	}


}

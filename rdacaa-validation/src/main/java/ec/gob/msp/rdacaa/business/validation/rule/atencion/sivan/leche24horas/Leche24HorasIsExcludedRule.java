package ec.gob.msp.rdacaa.business.validation.rule.atencion.sivan.leche24horas;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;

/**
 *
 * @author miguel.faubla
 */

@Rule(order = 3)
public class Leche24HorasIsExcludedRule extends BaseRule<String> {

	@When
	public boolean when() {
		return isExcludedFromValidation(RdacaaVariableKeyCatalog.SIVAN_RECIBIO_LECHE_ULTIMAS_24H);
	}

	@Then
	public RuleState then() {

		String leche24Horas = getVariableFromMap(RdacaaVariableKeyCatalog.SIVAN_RECIBIO_LECHE_ULTIMAS_24H);
		if (!ValidationSupport.isNotDefined(leche24Horas)) {
			addValidationResult(RdacaaVariableKeyCatalog.SIVAN_RECIBIO_LECHE_ULTIMAS_24H,
					ValidationResultCatalogConstants.CODIGO_ERROR_SIVAN_RECIBIO_LECHE_ULTIMAS_24H_NO_DEFINIDA);
		}

		return RuleState.BREAK;

	}
}

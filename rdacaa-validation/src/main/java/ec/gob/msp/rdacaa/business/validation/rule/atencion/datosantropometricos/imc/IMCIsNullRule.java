package ec.gob.msp.rdacaa.business.validation.rule.atencion.datosantropometricos.imc;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;

@Rule(order = 2)
public class IMCIsNullRule extends BaseRule<String> {

	@When
	public boolean when() {
		String imc = getVariableFromMap(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_INDICE_MASA_CORPORAL);
		return ValidationSupport.isNullOrEmptyString(imc);
	}

	@Then
	public RuleState then() {
		addValidationResult(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_INDICE_MASA_CORPORAL,
				ValidationResultCatalogConstants.CODIGO_ERROR_DAT_ANT_IMC_NULO);
		return RuleState.BREAK;
	}
}

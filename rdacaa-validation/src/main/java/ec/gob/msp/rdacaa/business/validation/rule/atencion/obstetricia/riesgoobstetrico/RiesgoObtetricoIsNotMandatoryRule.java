package ec.gob.msp.rdacaa.business.validation.rule.atencion.obstetricia.riesgoobstetrico;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.UtilsRule;

/**
 *
 * @author miguel.faubla
 */
@Rule(order = 6)
public class RiesgoObtetricoIsNotMandatoryRule extends UtilsRule {

	@When
	public boolean when() {
		return true;
	}

	@Then
	public RuleState then() {
		String riesgoObtetrico = getVariableFromMap(RdacaaVariableKeyCatalog.OBSTETRICIA_CODIGO_RIESGO_OBSTETRICO);
		
		Integer edadAniosMesesDias = getEdadAnioMesDias();
		boolean isEmbarazada = isEmbarazada();
		boolean isMujer = "F".equals(getSexoString());
		
		Integer riesgoObtetriconNumber = null;
		
		try {
			riesgoObtetriconNumber = Integer.parseInt(riesgoObtetrico);
		} catch (Exception e) {
			addValidationResult(RdacaaVariableKeyCatalog.OBSTETRICIA_CODIGO_RIESGO_OBSTETRICO,
					ValidationResultCatalogConstants.CODIGO_ERROR_OBSTETRICIA_CODIGO_RIESGO_OBSTETRICO_NO_NUMERICO);
			return RuleState.BREAK;
		}

		boolean isValidValue = validationQueryService.isIdCategorizacionRiesgoObstetricoValido(riesgoObtetriconNumber);
		if (!isValidValue) {
			addValidationResult(RdacaaVariableKeyCatalog.OBSTETRICIA_CODIGO_RIESGO_OBSTETRICO,
					ValidationResultCatalogConstants.CODIGO_ERROR_OBSTETRICIA_CODIGO_RIESGO_OBSTETRICO_ES_INCORRECTO);
		}
		
		boolean requiereMujerEmbarazadaParaValorValido = !(isMujer && isEmbarazada) && isValidValue;
		if (requiereMujerEmbarazadaParaValorValido) {
			addValidationResult(RdacaaVariableKeyCatalog.OBSTETRICIA_CODIGO_RIESGO_OBSTETRICO,
					ValidationResultCatalogConstants.CODIGO_ERROR_OBSTETRICIA_CODIGO_RIESGO_OBSTETRICO_REQUIERE_MUJER_EMBARAZADA);
		}

		if (edadAniosMesesDias == null) {
			addValidationResult(RdacaaVariableKeyCatalog.OBSTETRICIA_CODIGO_RIESGO_OBSTETRICO,
					ValidationResultCatalogConstants.CODIGO_ERROR_OBSTETRICIA_CODIGO_RIESGO_OBSTETRICO_FECHAS_INVALIDAS);
			return RuleState.BREAK;
		}

		boolean requiereMayor8AniosMenor59Anios = !(edadAniosMesesDias >= 80000 && edadAniosMesesDias <= 590000)
				&& !ValidationSupport.isNotDefined(riesgoObtetrico);
		if (requiereMayor8AniosMenor59Anios) {
			addValidationResult(RdacaaVariableKeyCatalog.OBSTETRICIA_CODIGO_RIESGO_OBSTETRICO,
					ValidationResultCatalogConstants.CODIGO_ERROR_OBSTETRICIA_CODIGO_RIESGO_OBSTETRICO_DEBE_TENER_DEFAULT_VALUE);
		}
		
		if(!isValidValue || requiereMujerEmbarazadaParaValorValido || requiereMayor8AniosMenor59Anios) {
			return RuleState.BREAK;
		}

		return RuleState.NEXT;
	}
}

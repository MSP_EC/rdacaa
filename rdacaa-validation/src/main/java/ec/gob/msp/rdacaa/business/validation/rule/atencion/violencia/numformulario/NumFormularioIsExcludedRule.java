/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.gob.msp.rdacaa.business.validation.rule.atencion.violencia.numformulario;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;
import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;

/**
 *
 * @author eduardo
 */
@Rule(order = 3)
public class NumFormularioIsExcludedRule extends BaseRule<String> {
    
        @When
	public boolean when() {
		return isExcludedFromValidation(RdacaaVariableKeyCatalog.VIOLENCIA_NUMERO_SERIE);
	}

	@Then
	public RuleState then() {
		
		String numFormulario = getVariableFromMap(RdacaaVariableKeyCatalog.VIOLENCIA_NUMERO_SERIE);
		if(!ValidationSupport.isNotDefined(numFormulario)){
			addValidationResult(RdacaaVariableKeyCatalog.VIOLENCIA_NUMERO_SERIE,
					ValidationResultCatalogConstants.CODIGO_ERROR_VIOLENCIA_NUMFORMULARIO_NO_DEFINIDA);
		}
		
		return RuleState.BREAK;

	}
}

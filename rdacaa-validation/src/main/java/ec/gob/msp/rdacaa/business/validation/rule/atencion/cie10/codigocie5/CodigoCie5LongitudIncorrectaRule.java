package ec.gob.msp.rdacaa.business.validation.rule.atencion.cie10.codigocie5;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;

@Rule(order = 7)
public class CodigoCie5LongitudIncorrectaRule extends BaseRule<String> {

	@When
	public boolean when() {
		String codigocie5 = getVariableFromMap(RdacaaVariableKeyCatalog.DIAGNOSTICO_CODIGO_CIE_5);
		Boolean valor = false;
		valor = ValidationSupport.isStringMoreThanXCharacters(codigocie5, 5);
		if (!valor) {
			valor = ValidationSupport.isStringLessThanXCharacters(codigocie5, 3);
		}

		return valor;
	}

	@Then
	public RuleState then() {
		addValidationResult(RdacaaVariableKeyCatalog.DIAGNOSTICO_CODIGO_CIE_5,
				ValidationResultCatalogConstants.CODIGO_ERROR_COD_DIAGNOSTICO_CIE10_5_LONGITUD_INCORRECTA);
		return RuleState.BREAK;
	}
}

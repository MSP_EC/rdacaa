package ec.gob.msp.rdacaa.business.validation.rule.persona.interconsulta;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;
import lombok.EqualsAndHashCode.Exclude;

@Rule(order = 3)
public class InterconsultaIsExcludedRule extends BaseRule<String>{
	
	@When 
	public boolean when() {
		return isExcludedFromValidation(RdacaaVariableKeyCatalog.REFERENCIA_CONTRAREFERENCIA_CODIGO_INTERCONSULTA_TIPO);
	}
	
	@Then
	public RuleState then() {
		
		String interconsulta = getVariableFromMap(RdacaaVariableKeyCatalog.REFERENCIA_CONTRAREFERENCIA_CODIGO_INTERCONSULTA_TIPO);
		if(!ValidationSupport.isNotDefined(interconsulta)){
			addValidationResult(RdacaaVariableKeyCatalog.REFERENCIA_CONTRAREFERENCIA_CODIGO_INTERCONSULTA_TIPO,
					ValidationResultCatalogConstants.CODIGO_ERROR_CODIGO_INTERCONSULTA_NO_DEFINIDO);
		}
		
		return RuleState.BREAK;

	}
	

}

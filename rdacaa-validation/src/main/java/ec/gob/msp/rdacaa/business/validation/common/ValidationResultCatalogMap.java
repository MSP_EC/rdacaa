package ec.gob.msp.rdacaa.business.validation.common;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import ec.gob.msp.rdacaa.business.entity.Notificacion;
import ec.gob.msp.rdacaa.business.service.NotificacionService;

@Component
public class ValidationResultCatalogMap {

	@Autowired
	private NotificacionService notificacionService;

	private Map<String, Notificacion> notificacionesMap;

	@PostConstruct
	private void init() {
		notificacionesMap = new ConcurrentHashMap<>();
		List<Notificacion> notificacionesDB = notificacionService.findAll();
		notificacionesDB.forEach(notificacion -> notificacionesMap.put(notificacion.getCodigo(), notificacion));
	}

	public Notificacion getNotificacion(String key) {
		return notificacionesMap.get(key);
	}

	public ValidationResult getValidationResult(String key) {
		Notificacion notificacion = notificacionesMap.get(key);
		return new ValidationResult(notificacion.getCodigo(), notificacion.getDescripcion(), null,
				ResultLevel.getLevel(notificacion.getNivel()));
	}

}

package ec.gob.msp.rdacaa.business.validation.malla;

import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;

public class RdacaaRawRowResult extends ConcurrentHashMap<String, RdacaaVariableValueResult> {

	private static final long serialVersionUID = 850294688109635089L;

	public static RdacaaRawRowResult getInstance() {
		return new RdacaaRawRowResult();
	}

	public RdacaaRawRowResult addResultToField(String key, RdacaaVariableValueResult value) {
		super.put(key, value);
		return this;
	}

	public boolean hasErrors() {
		Optional<Entry<String, RdacaaVariableValueResult>> hasError = this.entrySet().parallelStream()
				.filter((Entry<String, RdacaaVariableValueResult> e) -> e.getValue().hasErrors()).findAny();
		return hasError.isPresent();
	}

	@Override
	public String toString() {
		StringBuilder resultToString = new StringBuilder();
		this.forEach((k, v) -> resultToString.append("Item : " + k + " Count : " + v).append(System.lineSeparator()));
		return resultToString.toString();
	}
}

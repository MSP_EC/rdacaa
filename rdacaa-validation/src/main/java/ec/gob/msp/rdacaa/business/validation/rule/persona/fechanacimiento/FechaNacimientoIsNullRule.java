package ec.gob.msp.rdacaa.business.validation.rule.persona.fechanacimiento;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;

@Rule(order = 2)
public class FechaNacimientoIsNullRule  extends BaseRule<String> {

	@When
	public boolean when() {
		String fechaNacimiento = getVariableFromMap(RdacaaVariableKeyCatalog.PERSONA_FECHA_NACIMIENTO);
		return ValidationSupport.isNullOrEmptyString(fechaNacimiento);
	}

	@Then
	public RuleState then() {
		addValidationResult(RdacaaVariableKeyCatalog.PERSONA_FECHA_NACIMIENTO,
				ValidationResultCatalogConstants.CODIGO_ERROR_FECHANACIMIENTO_NULO);
		return RuleState.BREAK;
	}
}

package ec.gob.msp.rdacaa.business.validation.rule.atencion.cie10.numcertificadomedico;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;

/**
 *
 * @author miguel.faubla
 */

@Rule(order = 3)
public class NumeroCertificadoMedicoIsExcludedRule extends BaseRule<String> {

	@When
	public boolean when() {
		return isExcludedFromValidation(RdacaaVariableKeyCatalog.DIAGNOSTICO_NUMERO_CERTIFICADO_MEDICO_UNICO);
	}

	@Then
	public RuleState then() {

		String numerocertificadomedico = getVariableFromMap(
				RdacaaVariableKeyCatalog.DIAGNOSTICO_NUMERO_CERTIFICADO_MEDICO_UNICO);
		if (!ValidationSupport.isNotDefined(numerocertificadomedico)) {
			addValidationResult(RdacaaVariableKeyCatalog.DIAGNOSTICO_NUMERO_CERTIFICADO_MEDICO_UNICO,
					ValidationResultCatalogConstants.CODIGO_ERROR_NUMERO_CERTIFICADO_MEDICO_NO_DEFINIDA);
		}

		return RuleState.BREAK;

	}
}

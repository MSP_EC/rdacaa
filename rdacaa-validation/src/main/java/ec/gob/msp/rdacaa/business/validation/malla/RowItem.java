package ec.gob.msp.rdacaa.business.validation.malla;

import lombok.Getter;
import lombok.Setter;

public class RowItem {

	@Getter
	@Setter
	private Object itemValue;
	
	@Getter
	@Setter
	private boolean excludedFromValidation;
	
	@Getter
	@Setter
	private boolean isMandatory;
	
}

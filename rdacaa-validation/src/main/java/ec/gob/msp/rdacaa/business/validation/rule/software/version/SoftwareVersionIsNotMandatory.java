package ec.gob.msp.rdacaa.business.validation.rule.software.version;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;

/**
 *
 * @author miguel.faubla
 */
@Rule(order = 5)
public class SoftwareVersionIsNotMandatory extends BaseRule<String> {

	@When
	public boolean when() {
		String softwareVersion = getVariableFromMap(RdacaaVariableKeyCatalog.SOFTWARE_VERSION);
		return (!isMandatory(RdacaaVariableKeyCatalog.SOFTWARE_VERSION)
				&& ValidationSupport.isNotDefined(softwareVersion));
	}

	@Then
	public RuleState then() {
		return RuleState.BREAK;
	}
}

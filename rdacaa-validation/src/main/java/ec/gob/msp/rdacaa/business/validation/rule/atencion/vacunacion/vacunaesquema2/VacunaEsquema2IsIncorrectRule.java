package ec.gob.msp.rdacaa.business.validation.rule.atencion.vacunacion.vacunaesquema2;

import org.apache.commons.lang3.StringUtils;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.UtilsRule;

/**
 *
 * @author miguel.faubla
 */
@Rule(order = 6)
public class VacunaEsquema2IsIncorrectRule extends UtilsRule {

	@When
	public boolean when() {
		return true;
	}

	@Then
	public RuleState then() {

		String vacunaEsquema2 = getVariableFromMap(RdacaaVariableKeyCatalog.VACUNAS_CODIGO_ESQUEMA_2);
		String vacuna2Id = getVariableFromMap(RdacaaVariableKeyCatalog.VACUNAS_CODIGO_APLICACION_2);
		String vacunaNroDosis2 = getVariableFromMap(RdacaaVariableKeyCatalog.VACUNAS_CODIGO_DOSIS_2);

		if (!(StringUtils.isNumeric(vacunaEsquema2) && StringUtils.isNumeric(vacuna2Id)
				&& StringUtils.isNumeric(vacunaNroDosis2))) {
			addValidationResult(RdacaaVariableKeyCatalog.VACUNAS_CODIGO_ESQUEMA_2,
					ValidationResultCatalogConstants.CODIGO_ERROR_VACUNAS_CODIGO_ESQUEMA_2_COD_VACUNA_DOSIS_NUMERICOS);
			return RuleState.BREAK;
		}

		Integer edadAnioParcial = getEdadAnioParcial();

		Integer edadMesParcial = getEdadMesParcial();

		Integer edadDiasParcial = getEdadDiasParcial();

		if (!validationQueryService.isEsquemaVacunacionValido(vacunaEsquema2, vacuna2Id, vacunaNroDosis2)) {
			addValidationResult(RdacaaVariableKeyCatalog.VACUNAS_CODIGO_ESQUEMA_2,
					ValidationResultCatalogConstants.CODIGO_ERROR_VACUNAS_CODIGO_ESQUEMA_2_ES_INCORRECTO);
			return RuleState.BREAK;
		} else if (edadAnioParcial != null && edadMesParcial != null && edadDiasParcial != null
				&& validationQueryService.verifyApplicationVacunaByEdadAndEsquemaId(edadDiasParcial, edadMesParcial,
				edadAnioParcial, Integer.parseInt(vacunaEsquema2)) == null) {
			addValidationResult(RdacaaVariableKeyCatalog.VACUNAS_CODIGO_ESQUEMA_2,
					ValidationResultCatalogConstants.CODIGO_ERROR_VACUNAS_CODIGO_ESQUEMA_2_EDAD_INCORRECTA);
			return RuleState.BREAK;
		} else {
			return RuleState.NEXT;
		}
		
	}
}

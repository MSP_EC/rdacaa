package ec.gob.msp.rdacaa.business.validation.rule;

import java.util.Optional;

import com.deliveredtechnologies.rulebook.Fact;

import ec.gob.msp.rdacaa.business.validation.malla.RdacaaRawRow;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaRawRowResult;

public interface RuleExecutor {

	Optional<RdacaaRawRowResult> execute(RdacaaRawRow input, Fact... extraFacts);

}
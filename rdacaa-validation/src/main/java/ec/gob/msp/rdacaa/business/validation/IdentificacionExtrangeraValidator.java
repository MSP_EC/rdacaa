package ec.gob.msp.rdacaa.business.validation;

import java.util.ArrayList;
import java.util.List;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResult;
import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalog;
import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.common.Validator;

public class IdentificacionExtrangeraValidator extends ValidationSupport implements Validator<String> {
    
    private static final int VEINTE_CARACTERES = 20;
    private static final int TRES_CARACTERES = 3;

    @Override
    public List<ValidationResult> validate(String identificacion) {

        List<ValidationResult> listaErrores = new ArrayList<>();
        if (isNullOrEmptyString(identificacion)) {
            listaErrores.add(ValidationResultCatalog.IDENTIFICACION_VACIA.getValidationError());
        }else if(isStringMoreThanXCharacters(identificacion, VEINTE_CARACTERES)){
            listaErrores.add(ValidationResultCatalog.IDENTIFICACION_SUPERA20CARACTERES.getValidationError());
        }else if(isStringLessThanXCharacters(identificacion, TRES_CARACTERES)){
            listaErrores.add(ValidationResultCatalog.NOMBRE_MENOR_TRES_CARACTERES.getValidationError());
        }

        return listaErrores;
    }
    
}

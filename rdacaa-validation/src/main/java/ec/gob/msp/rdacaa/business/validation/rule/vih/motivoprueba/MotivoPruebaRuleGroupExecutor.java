package ec.gob.msp.rdacaa.business.validation.rule.vih.motivoprueba;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.deliveredtechnologies.rulebook.model.RuleBook;

import ec.gob.msp.rdacaa.business.validation.malla.RdacaaRawRowResult;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.RdacaaRuleExecutor;

/**
*
* @author dmurillo
*/
@Component
public class MotivoPruebaRuleGroupExecutor extends RdacaaRuleExecutor {

	@Autowired
	public MotivoPruebaRuleGroupExecutor(
			@Qualifier("ruleBookReglasVihMotivoPrueba") RuleBook<RdacaaRawRowResult> ruleBook) {
		super();
		this.ruleBook = ruleBook;
		this.variableKey = RdacaaVariableKeyCatalog.VIH_CODIGO_MOTIVO_PRUEBA;
	}
}

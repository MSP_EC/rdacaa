package ec.gob.msp.rdacaa.business.validation.rule.persona.identificacion;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;

@Rule(order = 2)
public class IdentificacionIsNullRule  extends BaseRule<String>  {
	
	@When
	public boolean when() {
		String identificacion = getVariableFromMap(RdacaaVariableKeyCatalog.PERSONA_IDENTIFICACION);
		return ValidationSupport.isNullOrEmptyString(identificacion);
	}

	@Then
	public RuleState then() {
		addValidationResult(RdacaaVariableKeyCatalog.PERSONA_IDENTIFICACION,
				ValidationResultCatalogConstants.CODIGO_ERROR_IDENTIFICACION_NULO);
		return RuleState.BREAK;
	}
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.gob.msp.rdacaa.business.validation.rule.atencion.violencia.lesionesocurridas;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;
import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;

/**
 *
 * @author eduardo
 */
@Rule(order = 5)
public class LesionesOcurridasIsNotMandatory extends BaseRule<String> {
    
    @When
	public boolean when() {
		
		String codigoLesion = getVariableFromMap(RdacaaVariableKeyCatalog.VIOLENCIA_CODIGO_LESION);
		return (!isMandatory(RdacaaVariableKeyCatalog.VIOLENCIA_CODIGO_LESION) &&
				ValidationSupport.isNotDefined(codigoLesion));
		
	}

	@Then
	public RuleState then() {
		return RuleState.BREAK;
	}
}

package ec.gob.msp.rdacaa.business.validation.rule.atencion.odontologia.obturado;

import org.apache.commons.lang3.StringUtils;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;

@Rule(order = 6)
public class ObturadosIsNumericaRule extends BaseRule<String>{
	@When
	public boolean when() {
		String dientesObturados = getVariableFromMap(RdacaaVariableKeyCatalog.ODONTOLOGIA_CANTIDAD_PIEZAS_OBTURADAS);
		return !StringUtils.isNumeric(dientesObturados);
	}

	@Then
	public RuleState then() {
		addValidationResult(RdacaaVariableKeyCatalog.ODONTOLOGIA_CANTIDAD_PIEZAS_OBTURADAS,
				ValidationResultCatalogConstants.CODIGO_ERROR_DIENTES_OBTURADOS_NO_NUMERICO);
		return RuleState.BREAK;
	}
}

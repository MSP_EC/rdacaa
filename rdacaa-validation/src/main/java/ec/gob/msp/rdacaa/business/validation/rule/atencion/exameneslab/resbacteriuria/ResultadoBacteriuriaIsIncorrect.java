package ec.gob.msp.rdacaa.business.validation.rule.atencion.exameneslab.resbacteriuria;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.UtilsRule;

/**
 *
 * @author miguel.faubla
 */
@Rule(order = 6)
public class ResultadoBacteriuriaIsIncorrect extends UtilsRule {

	@When
	public boolean when() {
		return true;
	}

	@Then
	public RuleState then() {
		String resultadoBacteriuria = getVariableFromMap(RdacaaVariableKeyCatalog.EXAMENES_LABORATORIO_RESULTADO_BACTERIURIA);
		
		boolean isEmbarazada = isEmbarazada();
		boolean isMujer = "F".equals(getSexoString());
		
		Integer resultadoBacteriuriaInteger = null;
		
		try {
			resultadoBacteriuriaInteger = Integer.parseInt(resultadoBacteriuria);
		} catch (Exception e) {
			addValidationResult(RdacaaVariableKeyCatalog.EXAMENES_LABORATORIO_RESULTADO_BACTERIURIA,
					ValidationResultCatalogConstants.CODIGO_ERROR_EXAMENES_LABORATORIO_RESULTADO_BACTERIURIA_NO_NUMERICO);
			return RuleState.BREAK;
		}
		
		boolean isValidValue = validationQueryService.isResultadoBacteriuriaValido(resultadoBacteriuriaInteger);
		if (!isValidValue) {
			addValidationResult(RdacaaVariableKeyCatalog.EXAMENES_LABORATORIO_RESULTADO_BACTERIURIA,
					ValidationResultCatalogConstants.CODIGO_ERROR_EXAMENES_LABORATORIO_RESULTADO_BACTERIURIA_ES_INCORRECTO);
		}
		
		boolean requiereMujerEmbarazadaParaValorValido = !(isMujer && isEmbarazada) && isValidValue;
		if (requiereMujerEmbarazadaParaValorValido) {
			addValidationResult(RdacaaVariableKeyCatalog.EXAMENES_LABORATORIO_RESULTADO_BACTERIURIA,
					ValidationResultCatalogConstants.CODIGO_ERROR_EXAMENES_LABORATORIO_RESULTADO_BACTERIURIA_REQUIERE_MUJER_EMBARAZADA);
		}
		
		if (!isValidValue || requiereMujerEmbarazadaParaValorValido ) {
			return RuleState.BREAK;
		}
		
		return RuleState.NEXT;
	}
}

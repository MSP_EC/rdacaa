package ec.gob.msp.rdacaa.business.validation.rule.atencion.datosantropometricos.categoriapesoparatalla;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Given;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.service.ScoreZAndCategoriaService;
import ec.gob.msp.rdacaa.business.service.SignosVitalesReglasValidacion;
import ec.gob.msp.rdacaa.business.service.SignosVitalesReglasValidacionNotFoundException;
import ec.gob.msp.rdacaa.business.service.ScoreZAndCategoriaService.PercentilDetalleNotFoundException;
import ec.gob.msp.rdacaa.business.service.ScoreZAndCategoriaService.PercentilNotFoundException;
import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.UtilsRule;

@Rule(order = 6)
public class CategoriaPesoParaTallaCalculateRule extends UtilsRule {

	@Given("scoreZAndCategoriaService")
	private ScoreZAndCategoriaService scoreZAndCategoriaService;

	@When
	public boolean when() {
		return true;
	}

	@Then
	public RuleState then() {

		String categoriaPesoParaTalla = "999990000066666";
		String puntajeZPesoParaTalla = getRdacaaRawRowResultValue(
				RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_PUNTAJE_Z_PESO_PARA_TALLA);
		boolean puntajeZPesoParaTallaHasErrors = variableHasErrors(
				RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_PUNTAJE_Z_PESO_PARA_TALLA);
		Integer edadAnioMesDias = getEdadAnioMesDias();
		String sexo = getSexoString();

		boolean valuesNotDefined = ValidationSupport.isNotDefined(puntajeZPesoParaTalla);

		setRdacaaRawRowResultValue(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_CATEGORIA_PESO_PARA_TALLA,
				categoriaPesoParaTalla);

		if (edadAnioMesDias == null) {
			addValidationResult(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_CATEGORIA_PESO_PARA_TALLA,
					ValidationResultCatalogConstants.CODIGO_ERROR_DAT_ANT_CATEGORIA_PESO_TALLA_FECHAS_FORMATO_INCORRECTO);
		}

		if (sexo == null) {
			addValidationResult(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_CATEGORIA_PESO_PARA_TALLA,
					ValidationResultCatalogConstants.CODIGO_ERROR_DAT_ANT_CATEGORIA_PESO_TALLA_SEXO_INCORRECTO);
		}

		if (puntajeZPesoParaTallaHasErrors) {
			addValidationResult(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_CATEGORIA_PESO_PARA_TALLA,
					ValidationResultCatalogConstants.CODIGO_ERROR_DAT_ANT_CATEGORIA_PESO_TALLA_PZ_PESO_PARA_TALLA_ERROR);
		}

		if (valuesNotDefined) {
			addValidationResult(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_CATEGORIA_PESO_PARA_TALLA,
					ValidationResultCatalogConstants.CODIGO_INFO_DAT_ANT_CATEGORIA_PESO_TALLA_PZ_PESO_PARA_TALLA_NO_DEFINIDO);
		}

		if (edadAnioMesDias == null || sexo == null || puntajeZPesoParaTallaHasErrors || valuesNotDefined) {
			return RuleState.BREAK;
		}

		try {
			SignosVitalesReglasValidacion categoriaPesoParaLongitudTalla = scoreZAndCategoriaService
					.calcularCategoriaPesoParaLongitudTalla(Double.parseDouble(puntajeZPesoParaTalla), edadAnioMesDias,
							sexo);
			categoriaPesoParaTalla = categoriaPesoParaLongitudTalla.getIdSignoVitalAlerta().toString();
		} catch (PercentilNotFoundException | PercentilDetalleNotFoundException
				| SignosVitalesReglasValidacionNotFoundException e) {
			addValidationResult(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_CATEGORIA_PESO_PARA_TALLA,
					ValidationResultCatalogConstants.CODIGO_INFO_DAT_ANT_CATEGORIA_PESO_TALLA_NO_APLICA);

			return RuleState.BREAK;
		}

		setRdacaaRawRowResultValue(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_CATEGORIA_PESO_PARA_TALLA,
				categoriaPesoParaTalla);

		addValidationResult(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_CATEGORIA_PESO_PARA_TALLA,
				ValidationResultCatalogConstants.CODIGO_INFO_DAT_ANT_CATEGORIA_PESO_TALLA_VALOR);

		return RuleState.NEXT;

	}
}

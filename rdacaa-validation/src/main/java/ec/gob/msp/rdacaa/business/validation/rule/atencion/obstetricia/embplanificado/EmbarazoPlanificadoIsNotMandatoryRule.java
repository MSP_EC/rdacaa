package ec.gob.msp.rdacaa.business.validation.rule.atencion.obstetricia.embplanificado;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;

/**
 *
 * @author miguel.faubla
 */
@Rule(order = 5)
public class EmbarazoPlanificadoIsNotMandatoryRule extends BaseRule<String> {

	@When
	public boolean when() {
		String embarazoPlanificado = getVariableFromMap(RdacaaVariableKeyCatalog.OBSTETRICIA_ES_EMBARAZO_PLANIFICADO);
		return (!isMandatory(RdacaaVariableKeyCatalog.OBSTETRICIA_ES_EMBARAZO_PLANIFICADO)
				&& ValidationSupport.isNotDefined(embarazoPlanificado));
	}

	@Then
	public RuleState then() {
		return RuleState.BREAK;
	}
}

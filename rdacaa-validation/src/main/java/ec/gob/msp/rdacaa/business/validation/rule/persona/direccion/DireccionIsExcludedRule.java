package ec.gob.msp.rdacaa.business.validation.rule.persona.direccion;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;

/**
*
* @author dmurillo
*/

@Rule(order = 3)
public class DireccionIsExcludedRule extends BaseRule<String> {
	
	@When
	public boolean when() {
		return isExcludedFromValidation(RdacaaVariableKeyCatalog.PERSONA_DIRECCION_DOMICILIO);
	}

	@Then
	public RuleState then() {
		
		String tipoIdentificacion = getVariableFromMap(RdacaaVariableKeyCatalog.PERSONA_DIRECCION_DOMICILIO);
		if(!ValidationSupport.isNotDefined(tipoIdentificacion)){
			addValidationResult(RdacaaVariableKeyCatalog.PERSONA_DIRECCION_DOMICILIO,
					ValidationResultCatalogConstants.CODIGO_ERROR_DIRECCION_NO_DEFINIDO);
		}
		
		return RuleState.BREAK;

	}
}

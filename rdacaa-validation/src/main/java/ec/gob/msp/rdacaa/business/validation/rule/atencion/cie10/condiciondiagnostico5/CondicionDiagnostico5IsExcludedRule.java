package ec.gob.msp.rdacaa.business.validation.rule.atencion.cie10.condiciondiagnostico5;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;

/**
 *
 * @author miguel.faubla
 */

@Rule(order = 3)
public class CondicionDiagnostico5IsExcludedRule extends BaseRule<String> {

	@When
	public boolean when() {
		return isExcludedFromValidation(RdacaaVariableKeyCatalog.DIAGNOSTICO_CODIGO_CONDICION_DIAG_5);
	}

	@Then
	public RuleState then() {

		String condiciondiagnostico5 = getVariableFromMap(RdacaaVariableKeyCatalog.DIAGNOSTICO_CODIGO_CONDICION_DIAG_5);
		if (!ValidationSupport.isNotDefined(condiciondiagnostico5)) {
			addValidationResult(RdacaaVariableKeyCatalog.DIAGNOSTICO_CODIGO_CONDICION_DIAG_5,
					ValidationResultCatalogConstants.CODIGO_ERROR_CONDICION_DIAGNOSTICO_5_NO_DEFINIDA);
		}

		return RuleState.BREAK;

	}
}

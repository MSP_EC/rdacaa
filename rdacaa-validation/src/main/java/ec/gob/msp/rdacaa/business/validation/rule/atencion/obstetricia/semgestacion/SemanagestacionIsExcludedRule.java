package ec.gob.msp.rdacaa.business.validation.rule.atencion.obstetricia.semgestacion;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;

/**
 *
 * @author miguel.faubla
 */

@Rule(order = 3)
public class SemanagestacionIsExcludedRule extends BaseRule<String> {

	@When
	public boolean when() {
		return isExcludedFromValidation(RdacaaVariableKeyCatalog.OBSTETRICIA_VALOR_SEMANA_GESTACION);
	}

	@Then
	public RuleState then() {

		String semGestacion = getVariableFromMap(RdacaaVariableKeyCatalog.OBSTETRICIA_VALOR_SEMANA_GESTACION);
		if (!ValidationSupport.isNotDefined(semGestacion)) {
			addValidationResult(RdacaaVariableKeyCatalog.OBSTETRICIA_VALOR_SEMANA_GESTACION,
					ValidationResultCatalogConstants.CODIGO_ERROR_OBSTETRICIA_VALOR_SEMANA_GESTACION_NO_DEFINIDA);
		}

		return RuleState.BREAK;

	}
}

package ec.gob.msp.rdacaa.business.validation.rule.atencion.datosantropometricos.talla;

import java.util.List;
import java.util.Optional;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.deliveredtechnologies.rulebook.Fact;
import com.deliveredtechnologies.rulebook.model.RuleBook;
import com.deliveredtechnologies.rulebook.util.ArrayUtils;

import ec.gob.msp.rdacaa.business.service.SignosVitalesReglasValidacion;
import ec.gob.msp.rdacaa.business.service.SignosvitalesreglasService;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaRawRow;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaRawRowResult;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.RdacaaRuleExecutor;

@Component
public class TallaRuleGroupExecutor extends RdacaaRuleExecutor {
	
	@Autowired
	private SignosvitalesreglasService signosvitalesreglasService;
	private List<SignosVitalesReglasValidacion> reglasTalla;

	@Autowired
	public TallaRuleGroupExecutor(
			@Qualifier("ReglasDatoAntropometricoTalla") RuleBook<RdacaaRawRowResult> ruleBook) {
		super();
		this.ruleBook = ruleBook;
		this.variableKey = RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_TALLA;
	}
	
	@PostConstruct
	private void init() {
		reglasTalla = signosvitalesreglasService.findAllReglasTalla();
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public Optional<RdacaaRawRowResult> execute(RdacaaRawRow input, Fact... extraFacts) {
    	Fact reglasTallaFact = new Fact("reglasTalla", reglasTalla);
    	Fact[] extra = ArrayUtils.combine(extraFacts, new Fact[] {reglasTallaFact});
		return super.execute(input, extra);
    }
}

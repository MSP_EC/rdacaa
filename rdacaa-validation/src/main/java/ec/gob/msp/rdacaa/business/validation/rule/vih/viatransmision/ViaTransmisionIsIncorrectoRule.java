package ec.gob.msp.rdacaa.business.validation.rule.vih.viatransmision;

import com.deliveredtechnologies.rulebook.RuleState;
import org.apache.commons.lang3.StringUtils;

import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;

/**
*
* @author dmurillo
*/
@Rule(order = 6)
public class ViaTransmisionIsIncorrectoRule extends BaseRule<String> {

	@When
	public boolean when() {
            return true;
//		String prueba = getVariableFromMap(RdacaaVariableKeyCatalog.VIH_CODIGO_VIA_TRANSMISION);
//		if(StringUtils.isNumeric(prueba)){
//			return !this.validationQueryService.isCodigoVihViaTransmisionValido(Integer.valueOf(prueba));
//		}
//		return true;
	}

	@Then
	public RuleState then() {
            
            boolean resultadoPruebaUnoHasErrors = variableHasErrors(RdacaaVariableKeyCatalog.VIH_VALOR_RESULTADO_PRIMERA_PRUEBA);
            boolean resultadoPruebaDosHasErrors = variableHasErrors(RdacaaVariableKeyCatalog.VIH_VALOR_RESULTADO_SEGUNDA_PRUEBA);
            String resultadoPruebaUnoValue = getRdacaaRawRowResultValue(RdacaaVariableKeyCatalog.VIH_VALOR_RESULTADO_PRIMERA_PRUEBA);
            String resultadoPruebaDosValue = getRdacaaRawRowResultValue(RdacaaVariableKeyCatalog.VIH_VALOR_RESULTADO_SEGUNDA_PRUEBA);
            
            if(resultadoPruebaUnoHasErrors && resultadoPruebaDosHasErrors){
                addValidationResult(RdacaaVariableKeyCatalog.VIH_CODIGO_VIA_TRANSMISION, 
                        ValidationResultCatalogConstants.CODIGO_ERROR_VIH_CODIGO_VIASTRANSMISION_SIN_ERROR);
                return RuleState.BREAK;
            }
            
            if (ValidationSupport.isNotDefined(resultadoPruebaUnoValue) && ValidationSupport.isNotDefined(resultadoPruebaDosValue)){
                addValidationResult(RdacaaVariableKeyCatalog.VIH_CODIGO_VIA_TRANSMISION, 
                        ValidationResultCatalogConstants.CODIGO_ERROR_VIH_CODIGO_VIASTRANSMISION_PRUEBA);
                return RuleState.BREAK;
            }
            
            String vias = getVariableFromMap(RdacaaVariableKeyCatalog.VIH_CODIGO_VIA_TRANSMISION);
            boolean viasValida = false;
            if (StringUtils.isNumeric(vias)){
                viasValida = this.validationQueryService.isCodigoVihViaTransmisionValido(Integer.valueOf(vias));
            }
            
            if (!viasValida){
            
                addValidationResult(RdacaaVariableKeyCatalog.VIH_CODIGO_VIA_TRANSMISION,
                        ValidationResultCatalogConstants.CODIGO_ERROR_VIH_CODIGO_VIASTRANSMISION_INCORRECTO);
            }
//		addValidationResult(RdacaaVariableKeyCatalog.VIH_CODIGO_VIA_TRANSMISION,
//				ValidationResultCatalogConstants.CODIGO_ERROR_VIH_CODIGO_VIASTRANSMISION_INCORRECTO);
                return RuleState.NEXT; 
	}
}

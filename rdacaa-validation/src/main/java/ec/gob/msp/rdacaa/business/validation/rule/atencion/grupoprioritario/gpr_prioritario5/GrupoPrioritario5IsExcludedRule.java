package ec.gob.msp.rdacaa.business.validation.rule.atencion.grupoprioritario.gpr_prioritario5;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;

/**
 *
 * @author miguel.faubla
 */

@Rule(order = 3)
public class GrupoPrioritario5IsExcludedRule extends BaseRule<String> {

	@When
	public boolean when() {
		return isExcludedFromValidation(RdacaaVariableKeyCatalog.GRUPO_PRIORITARIO_5);
	}

	@Then
	public RuleState then() {

		String grupoprioritario5 = getVariableFromMap(RdacaaVariableKeyCatalog.GRUPO_PRIORITARIO_5);
		if (!ValidationSupport.isNotDefined(grupoprioritario5)) {
			addValidationResult(RdacaaVariableKeyCatalog.GRUPO_PRIORITARIO_5,
					ValidationResultCatalogConstants.CODIGO_ERROR_GRUPO_PRIORITARIO_5_NO_DEFINIDA);
		}

		return RuleState.BREAK;

	}
}

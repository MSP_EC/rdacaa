package ec.gob.msp.rdacaa.business.validation.rule.atencion.obstetricia.plantransporte;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.UtilsRule;

/**
 *
 * @author miguel.faubla
 */
@Rule(order = 6)
public class PlanTransporteIsIncorrect extends UtilsRule {

	@When
	public boolean when() {
		return true;
	}

	@Then
	public RuleState then() {
		String planTransporte = getVariableFromMap(RdacaaVariableKeyCatalog.OBSTETRICIA_TIENE_PLAN_TRANSPORTE);
		Integer edadAniosMesesDias = getEdadAnioMesDias();

		boolean isValidValue = "0".equals(planTransporte) || "1".equals(planTransporte);
		
		boolean isEmbarazada = isEmbarazada();
		boolean isMujer = "F".equals(getSexoString());

		if (!isValidValue) {
			addValidationResult(RdacaaVariableKeyCatalog.OBSTETRICIA_TIENE_PLAN_TRANSPORTE,
					ValidationResultCatalogConstants.CODIGO_ERROR_OBSTETRICIA_TIENE_PLAN_TRANSPORTE_ES_INCORRECTO);
		}
		
		boolean requiereMujerEmbarazadaParaValorValido = !(isMujer && isEmbarazada) && isValidValue;
		if (requiereMujerEmbarazadaParaValorValido) {
			addValidationResult(RdacaaVariableKeyCatalog.OBSTETRICIA_TIENE_PLAN_TRANSPORTE,
					ValidationResultCatalogConstants.CODIGO_ERROR_OBSTETRICIA_TIENE_PLAN_TRANSPORTE_REQUIERE_MUJER_EMBARAZADA);
		}

		if (edadAniosMesesDias == null) {
			addValidationResult(RdacaaVariableKeyCatalog.OBSTETRICIA_TIENE_PLAN_TRANSPORTE,
					ValidationResultCatalogConstants.CODIGO_ERROR_OBSTETRICIA_TIENE_PLAN_TRANSPORTE_FECHAS_INVALIDAS);
			return RuleState.BREAK;
		}

		boolean requiereMayor8AniosMenor59Anios = !(edadAniosMesesDias >= 80000 && edadAniosMesesDias <= 590000)
				&& !ValidationSupport.isNotDefined(planTransporte);
		if (requiereMayor8AniosMenor59Anios) {
			addValidationResult(RdacaaVariableKeyCatalog.OBSTETRICIA_TIENE_PLAN_TRANSPORTE,
					ValidationResultCatalogConstants.CODIGO_ERROR_OBSTETRICIA_TIENE_PLAN_TRANSPORTE_DEBE_TENER_DEFAULT_VALUE);
		}
		
		if(!isValidValue || requiereMujerEmbarazadaParaValorValido || requiereMayor8AniosMenor59Anios) {
			return RuleState.BREAK;
		}

		return RuleState.NEXT;
	}
}

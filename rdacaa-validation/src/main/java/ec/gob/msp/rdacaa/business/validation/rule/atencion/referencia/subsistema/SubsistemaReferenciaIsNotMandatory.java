package ec.gob.msp.rdacaa.business.validation.rule.atencion.referencia.subsistema;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;

@Rule(order = 5)
public class SubsistemaReferenciaIsNotMandatory extends BaseRule<String> {

	@When
	public boolean when() {
		
		String subsistemaReferencia = getVariableFromMap(RdacaaVariableKeyCatalog.REFERENCIA_CONTRAREFERENCIA_CODIGO_SUBSISTEMA);
		return (!isMandatory(RdacaaVariableKeyCatalog.REFERENCIA_CONTRAREFERENCIA_CODIGO_SUBSISTEMA) &&
				ValidationSupport.isNotDefined(subsistemaReferencia));
		
	}
	
	@Then
	public RuleState then() {
		return RuleState.BREAK;
	}	
}

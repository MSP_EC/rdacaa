package ec.gob.msp.rdacaa.business.validation.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.deliveredtechnologies.rulebook.model.RuleBook;
import com.deliveredtechnologies.rulebook.model.runner.RuleBookRunner;

import ec.gob.msp.rdacaa.business.validation.malla.RdacaaRawRowResult;

@Configuration
public class RuleBookConfig {

	@SuppressWarnings("unchecked")
	@Bean("ReglasAtencionUUID")
	public RuleBook<RdacaaRawRowResult> ruleBookReglasAtencionUUID() {
		return new RuleBookRunner("ec.gob.msp.rdacaa.business.validation.rule.atencion.uuid");
	}

	@SuppressWarnings("unchecked")
	@Bean("ReglasPersonaUUID")
	public RuleBook<RdacaaRawRowResult> ruleBookReglasPersonaUUID() {
		return new RuleBookRunner("ec.gob.msp.rdacaa.business.validation.rule.persona.uuid");
	}

	@SuppressWarnings("unchecked")
	@Bean("ReglasTipoIdentificacion")
	public RuleBook<RdacaaRawRowResult> ruleBookReglasTipoIdentificacion() {
		return new RuleBookRunner("ec.gob.msp.rdacaa.business.validation.rule.persona.tipoidentificacion");
	}

	@SuppressWarnings("unchecked")
	@Bean("ReglasIdentificacion")
	public RuleBook<RdacaaRawRowResult> ruleBookReglasIdentificacion() {
		return new RuleBookRunner("ec.gob.msp.rdacaa.business.validation.rule.persona.identificacion");
	}

	@SuppressWarnings("unchecked")
	@Bean("ReglasFechaNacimiento")
	public RuleBook<RdacaaRawRowResult> ruleBookReglasFechaNacimiento() {
		return new RuleBookRunner("ec.gob.msp.rdacaa.business.validation.rule.persona.fechanacimiento");
	}

	@SuppressWarnings("unchecked")
	@Bean("ReglasPrimerNombre")
	public RuleBook<RdacaaRawRowResult> ruleBookReglasPrimerNombre() {
		return new RuleBookRunner("ec.gob.msp.rdacaa.business.validation.rule.persona.primernombre");
	}

	@SuppressWarnings("unchecked")
	@Bean("ReglasSegundoNombre")
	public RuleBook<RdacaaRawRowResult> ruleBookReglasSegundoNombre() {
		return new RuleBookRunner("ec.gob.msp.rdacaa.business.validation.rule.persona.segundonombre");
	}

	@SuppressWarnings("unchecked")
	@Bean("ReglasPrimerApellido")
	public RuleBook<RdacaaRawRowResult> ruleBookReglasPrimerApellido() {
		return new RuleBookRunner("ec.gob.msp.rdacaa.business.validation.rule.persona.primerapellido");
	}

	@SuppressWarnings("unchecked")
	@Bean("ReglasSegundoApellido")
	public RuleBook<RdacaaRawRowResult> ruleBookReglasSegundoApellido() {
		return new RuleBookRunner("ec.gob.msp.rdacaa.business.validation.rule.persona.segundoapellido");
	}

	@SuppressWarnings("unchecked")
	@Bean("ReglasNoIdentificado")
	public RuleBook<RdacaaRawRowResult> ruleBookReglasNoIdentificado() {
		return new RuleBookRunner("ec.gob.msp.rdacaa.business.validation.rule.persona.noidentificado");
	}

	@SuppressWarnings("unchecked")
	@Bean("ReglasArchivo")
	public RuleBook<RdacaaRawRowResult> ruleBookReglasArchivo() {
		return new RuleBookRunner("ec.gob.msp.rdacaa.business.validation.rule.persona.archivo");
	}

	@SuppressWarnings("unchecked")
	@Bean("ReglasSexo")
	public RuleBook<RdacaaRawRowResult> ruleBookReglasSexo() {
		return new RuleBookRunner("ec.gob.msp.rdacaa.business.validation.rule.persona.sexo");
	}

	@SuppressWarnings("unchecked")
	@Bean("ReglasTipoBono")
	public RuleBook<RdacaaRawRowResult> ruleBookReglasTipoBono() {
		return new RuleBookRunner("ec.gob.msp.rdacaa.business.validation.rule.persona.tipobono");
	}

	@SuppressWarnings("unchecked")
	@Bean("ReglasTipoSeguro")
	public RuleBook<RdacaaRawRowResult> ruleBookReglasTipoSeguro() {
		return new RuleBookRunner("ec.gob.msp.rdacaa.business.validation.rule.persona.tiposeguro");

	}

	@SuppressWarnings("unchecked")
	@Bean("ReglasOrientacionSexual")
	public RuleBook<RdacaaRawRowResult> ruleBookReglasOrientacionSexual() {
		return new RuleBookRunner("ec.gob.msp.rdacaa.business.validation.rule.persona.orientacionsexual");
	}

	@SuppressWarnings("unchecked")
	@Bean("ReglasIdentidadGenero")
	public RuleBook<RdacaaRawRowResult> ruleBookReglasIdentidadGenero() {
		return new RuleBookRunner("ec.gob.msp.rdacaa.business.validation.rule.persona.identidadgenero");
	}

	@SuppressWarnings("unchecked")
	@Bean("ReglasTipoIdentificacionRep")
	public RuleBook<RdacaaRawRowResult> ruleBookReglasIdentidadRep() {
		return new RuleBookRunner("ec.gob.msp.rdacaa.business.validation.rule.persona.tipoidentificacionrep");
	}

	@SuppressWarnings("unchecked")
	@Bean("ReglasDireccionDomicilio")
	public RuleBook<RdacaaRawRowResult> ruleBookReglasDireccionDomicilio() {
		return new RuleBookRunner("ec.gob.msp.rdacaa.business.validation.rule.persona.direccion");
	}

	@SuppressWarnings("unchecked")
	@Bean("ReglasIdentificacionRep")
	public RuleBook<RdacaaRawRowResult> ruleBookReglasIdentificacionRepresentante() {
		return new RuleBookRunner("ec.gob.msp.rdacaa.business.validation.rule.persona.identificacionrep");
	}

	@SuppressWarnings("unchecked")
	@Bean("ReglasTelefonoPaciente")
	public RuleBook<RdacaaRawRowResult> ruleBookReglasTelefonoPaciente() {
		return new RuleBookRunner("ec.gob.msp.rdacaa.business.validation.rule.persona.telefonopaciente");
	}

	@SuppressWarnings("unchecked")
	@Bean("ReglasTelefonoFamiliar")
	public RuleBook<RdacaaRawRowResult> ruleBookReglasTelefonoFamiliar() {
		return new RuleBookRunner("ec.gob.msp.rdacaa.business.validation.rule.persona.telefonofamiliar");
	}

	@SuppressWarnings("unchecked")
	@Bean("ReglasIdentificacionEtnica")
	public RuleBook<RdacaaRawRowResult> ruleBookReglasReglasIdentificacionEtnica() {
		return new RuleBookRunner("ec.gob.msp.rdacaa.business.validation.rule.persona.identificacionetnica");
	}

	@SuppressWarnings("unchecked")
	@Bean("ReglasNacionalidad")
	public RuleBook<RdacaaRawRowResult> ruleBookReglasNacionalidad() {
		return new RuleBookRunner("ec.gob.msp.rdacaa.business.validation.rule.persona.nacionalidad");
	}

	@SuppressWarnings("unchecked")
	@Bean("ReglasNacionalidadEtnica")
	public RuleBook<RdacaaRawRowResult> ruleBookReglasNacionalidadEtnica() {
		return new RuleBookRunner("ec.gob.msp.rdacaa.business.validation.rule.persona.nacionalidadetnica");
	}

	@SuppressWarnings("unchecked")
	@Bean("ReglasPueblos")
	public RuleBook<RdacaaRawRowResult> ruleBookReglasPueblos() {
		return new RuleBookRunner("ec.gob.msp.rdacaa.business.validation.rule.persona.pueblos");
	}

	@SuppressWarnings("unchecked")
	@Bean("ReglasParroquia")
	public RuleBook<RdacaaRawRowResult> ruleBookReglasParroquia() {
		return new RuleBookRunner("ec.gob.msp.rdacaa.business.validation.rule.persona.parroquia");
	}

	@SuppressWarnings("unchecked")
	@Bean("ReglasEstrategiaMsp")
	public RuleBook<RdacaaRawRowResult> ruleBookReglasEstrategiaMsp() {
		return new RuleBookRunner("ec.gob.msp.rdacaa.business.validation.rule.atencion.grupoprioritario.estrategiamsp");
	}

	@SuppressWarnings("unchecked")
	@Bean("ReglasLugarAtencion")
	public RuleBook<RdacaaRawRowResult> ruleBookReglasLugarAtencion() {
		return new RuleBookRunner("ec.gob.msp.rdacaa.business.validation.rule.atencion.grupoprioritario.lugaratencion");
	}

	@SuppressWarnings("unchecked")
	@Bean("ReglasTipoAtencion")
	public RuleBook<RdacaaRawRowResult> ruleBookReglasTipoAtencion() {
		return new RuleBookRunner("ec.gob.msp.rdacaa.business.validation.rule.atencion.grupoprioritario.tipoatencion");
	}

	@SuppressWarnings("unchecked")
	@Bean("ReglasGrupoPrioritario1")
	public RuleBook<RdacaaRawRowResult> ruleBookReglasGrupoPrioritario1() {
		return new RuleBookRunner(
				"ec.gob.msp.rdacaa.business.validation.rule.atencion.grupoprioritario.gpr_prioritario1");
	}

	@SuppressWarnings("unchecked")
	@Bean("ReglasGrupoPrioritario2")
	public RuleBook<RdacaaRawRowResult> ruleBookReglasGrupoPrioritario2() {
		return new RuleBookRunner(
				"ec.gob.msp.rdacaa.business.validation.rule.atencion.grupoprioritario.gpr_prioritario2");
	}

	@SuppressWarnings("unchecked")
	@Bean("ReglasGrupoPrioritario3")
	public RuleBook<RdacaaRawRowResult> ruleBookReglasGrupoPrioritario3() {
		return new RuleBookRunner(
				"ec.gob.msp.rdacaa.business.validation.rule.atencion.grupoprioritario.gpr_prioritario3");
	}

	@SuppressWarnings("unchecked")
	@Bean("ReglasGrupoPrioritario4")
	public RuleBook<RdacaaRawRowResult> ruleBookReglasGrupoPrioritario4() {
		return new RuleBookRunner(
				"ec.gob.msp.rdacaa.business.validation.rule.atencion.grupoprioritario.gpr_prioritario4");
	}

	@SuppressWarnings("unchecked")
	@Bean("ReglasGrupoPrioritario5")
	public RuleBook<RdacaaRawRowResult> ruleBookReglasGrupoPrioritario5() {
		return new RuleBookRunner(
				"ec.gob.msp.rdacaa.business.validation.rule.atencion.grupoprioritario.gpr_prioritario5");
	}

	@SuppressWarnings("unchecked")
	@Bean("ReglasCodigoCie1")
	public RuleBook<RdacaaRawRowResult> ruleBookReglasCodigoCie1() {
		return new RuleBookRunner("ec.gob.msp.rdacaa.business.validation.rule.atencion.cie10.codigocie1");
	}

	@SuppressWarnings("unchecked")
	@Bean("ReglasTipoDiagnostico1")
	public RuleBook<RdacaaRawRowResult> ruleBookReglasTipoDiagnostico1() {
		return new RuleBookRunner("ec.gob.msp.rdacaa.business.validation.rule.atencion.cie10.tipodiagnostico1");
	}

	@SuppressWarnings("unchecked")
	@Bean("ReglasTipoAtencionDiag1")
	public RuleBook<RdacaaRawRowResult> ruleBookReglasTipoAtencionDiag1() {
		return new RuleBookRunner("ec.gob.msp.rdacaa.business.validation.rule.atencion.cie10.tipoatencion1");
	}

	@SuppressWarnings("unchecked")
	@Bean("ReglasCondicionDiagnostico1")
	public RuleBook<RdacaaRawRowResult> ruleBookReglasCondicionDiagnostico1() {
		return new RuleBookRunner("ec.gob.msp.rdacaa.business.validation.rule.atencion.cie10.condiciondiagnostico1");
	}

	@SuppressWarnings("unchecked")
	@Bean("ReglasNumeroCertificadoMedico")
	public RuleBook<RdacaaRawRowResult> ruleBookReglasNumeroCertificadoMedico() {
		return new RuleBookRunner("ec.gob.msp.rdacaa.business.validation.rule.atencion.cie10.numcertificadomedico");
	}

	@SuppressWarnings("unchecked")
	@Bean("ReglasNumeroSerieFormulario")
	public RuleBook<RdacaaRawRowResult> ruleBookReglasNumSerieFormulario() {
		return new RuleBookRunner("ec.gob.msp.rdacaa.business.validation.rule.atencion.violencia.numformulario");
	}

	@SuppressWarnings("unchecked")
	@Bean("ReglasLesionesOcurridas")
	public RuleBook<RdacaaRawRowResult> ruleBookReglasLesionesOcurridas() {
		return new RuleBookRunner("ec.gob.msp.rdacaa.business.validation.rule.atencion.violencia.lesionesocurridas");
	}

	@SuppressWarnings("unchecked")
	@Bean("ReglasCodigoCie2")
	public RuleBook<RdacaaRawRowResult> ruleBookReglasCodigoCie2() {
		return new RuleBookRunner("ec.gob.msp.rdacaa.business.validation.rule.atencion.cie10.codigocie2");
	}

	@SuppressWarnings("unchecked")
	@Bean("ReglasCodigoCie3")
	public RuleBook<RdacaaRawRowResult> ruleBookReglasCodigoCie3() {
		return new RuleBookRunner("ec.gob.msp.rdacaa.business.validation.rule.atencion.cie10.codigocie3");
	}

	@SuppressWarnings("unchecked")
	@Bean("ReglasCodigoCie4")
	public RuleBook<RdacaaRawRowResult> ruleBookReglasCodigoCie4() {
		return new RuleBookRunner("ec.gob.msp.rdacaa.business.validation.rule.atencion.cie10.codigocie4");
	}

	@SuppressWarnings("unchecked")
	@Bean("ReglasCodigoCie5")
	public RuleBook<RdacaaRawRowResult> ruleBookReglasCodigoCie5() {
		return new RuleBookRunner("ec.gob.msp.rdacaa.business.validation.rule.atencion.cie10.codigocie5");
	}

	@SuppressWarnings("unchecked")
	@Bean("ReglasTipoDiagnostico2")
	public RuleBook<RdacaaRawRowResult> ruleBookReglasTipoDiagnostico2() {
		return new RuleBookRunner("ec.gob.msp.rdacaa.business.validation.rule.atencion.cie10.tipodiagnostico2");
	}

	@SuppressWarnings("unchecked")
	@Bean("ReglasTipoDiagnostico3")
	public RuleBook<RdacaaRawRowResult> ruleBookReglasTipoDiagnostico3() {
		return new RuleBookRunner("ec.gob.msp.rdacaa.business.validation.rule.atencion.cie10.tipodiagnostico3");
	}

	@SuppressWarnings("unchecked")
	@Bean("ReglasTipoDiagnostico4")
	public RuleBook<RdacaaRawRowResult> ruleBookReglasTipoDiagnostico4() {
		return new RuleBookRunner("ec.gob.msp.rdacaa.business.validation.rule.atencion.cie10.tipodiagnostico4");
	}

	@SuppressWarnings("unchecked")
	@Bean("ReglasTipoDiagnostico5")
	public RuleBook<RdacaaRawRowResult> ruleBookReglasTipoDiagnostico5() {
		return new RuleBookRunner("ec.gob.msp.rdacaa.business.validation.rule.atencion.cie10.tipodiagnostico5");
	}
	
	@SuppressWarnings("unchecked")
	@Bean("ruleBookReglasVihMotivoPrueba")
	public RuleBook<RdacaaRawRowResult> ruleBookReglasVihMotivoPrueba() {
		return new RuleBookRunner("ec.gob.msp.rdacaa.business.validation.rule.vih.motivoprueba");
	}
	
	@SuppressWarnings("unchecked")
	@Bean("ruleBookReglasVihPruebaUno")
	public RuleBook<RdacaaRawRowResult> ruleBookReglasVihPruebaUno() {
		return new RuleBookRunner("ec.gob.msp.rdacaa.business.validation.rule.vih.pruebauno");
	}
	
	@SuppressWarnings("unchecked")
	@Bean("ruleBookReglasVihPruebaDos")
	public RuleBook<RdacaaRawRowResult> ruleBookReglasVihPruebaDos() {
		return new RuleBookRunner("ec.gob.msp.rdacaa.business.validation.rule.vih.pruebados");
	}
	
	@SuppressWarnings("unchecked")
	@Bean("ruleBookReglasVihResultadoPruebaUno")
	public RuleBook<RdacaaRawRowResult> ruleBookReglasVihResultadoPruebaUno() {
		return new RuleBookRunner("ec.gob.msp.rdacaa.business.validation.rule.vih.resultadopruebauno");
	}
	
	@SuppressWarnings("unchecked")
	@Bean("ruleBookReglasVihResultadoPruebaDos")
	public RuleBook<RdacaaRawRowResult> ruleBookReglasVihResultadoPruebaDos() {
		return new RuleBookRunner("ec.gob.msp.rdacaa.business.validation.rule.vih.resultadopruebados");
	}
	
	@SuppressWarnings("unchecked")
	@Bean("ruleBookReglasVihViatransmision")
	public RuleBook<RdacaaRawRowResult> ruleBookReglasVihViatransmision() {
		return new RuleBookRunner("ec.gob.msp.rdacaa.business.validation.rule.vih.viatransmision");
	}
	
	@SuppressWarnings("unchecked")
	@Bean("ruleBookReglasVihCargaViral")
	public RuleBook<RdacaaRawRowResult> ruleBookReglasVihCargaViral() {
		return new RuleBookRunner("ec.gob.msp.rdacaa.business.validation.rule.vih.cargaviral");
	}
	
	@SuppressWarnings("unchecked")
	@Bean("ruleBookReglasVihCD4")
	public RuleBook<RdacaaRawRowResult> ruleBookReglasVihCD4() {
		return new RuleBookRunner("ec.gob.msp.rdacaa.business.validation.rule.vih.cd4");
	}

        
        @SuppressWarnings("unchecked")
	@Bean("ReglasIdentificaAgresor")
	public RuleBook<RdacaaRawRowResult> ruleBookReglasIdentificaAgresor() {
		return new RuleBookRunner("ec.gob.msp.rdacaa.business.validation.rule.atencion.violencia.identificaagresor");
	}


	@SuppressWarnings("unchecked")
	@Bean("ReglasTipoAtencionDiag2")
	public RuleBook<RdacaaRawRowResult> ruleBookReglasTipoAtencionDiag2() {
		return new RuleBookRunner("ec.gob.msp.rdacaa.business.validation.rule.atencion.cie10.tipoatencion2");
	}

	@SuppressWarnings("unchecked")
	@Bean("ReglasTipoAtencionDiag3")
	public RuleBook<RdacaaRawRowResult> ruleBookReglasTipoAtencionDiag3() {
		return new RuleBookRunner("ec.gob.msp.rdacaa.business.validation.rule.atencion.cie10.tipoatencion3");
	}

	@SuppressWarnings("unchecked")
	@Bean("ReglasTipoAtencionDiag4")
	public RuleBook<RdacaaRawRowResult> ruleBookReglasTipoAtencionDiag4() {
		return new RuleBookRunner("ec.gob.msp.rdacaa.business.validation.rule.atencion.cie10.tipoatencion4");
	}

	@SuppressWarnings("unchecked")
	@Bean("ReglasTipoAtencionDiag5")
	public RuleBook<RdacaaRawRowResult> ruleBookReglasTipoAtencionDiag5() {
		return new RuleBookRunner("ec.gob.msp.rdacaa.business.validation.rule.atencion.cie10.tipoatencion5");
	}

	@SuppressWarnings("unchecked")
	@Bean("ReglasCondicionDiagnostico2")
	public RuleBook<RdacaaRawRowResult> ruleBookReglasCondicionDiagnostico2() {
		return new RuleBookRunner("ec.gob.msp.rdacaa.business.validation.rule.atencion.cie10.condiciondiagnostico2");
	}

	@SuppressWarnings("unchecked")
	@Bean("ReglasCondicionDiagnostico3")
	public RuleBook<RdacaaRawRowResult> ruleBookReglasCondicionDiagnostico3() {
		return new RuleBookRunner("ec.gob.msp.rdacaa.business.validation.rule.atencion.cie10.condiciondiagnostico3");
	}

	@SuppressWarnings("unchecked")
	@Bean("ReglasCondicionDiagnostico4")
	public RuleBook<RdacaaRawRowResult> ruleBookReglasCondicionDiagnostico4() {
		return new RuleBookRunner("ec.gob.msp.rdacaa.business.validation.rule.atencion.cie10.condiciondiagnostico4");
	}

	@SuppressWarnings("unchecked")
	@Bean("ReglasCondicionDiagnostico5")
	public RuleBook<RdacaaRawRowResult> ruleBookReglasCondicionDiagnostico5() {
		return new RuleBookRunner("ec.gob.msp.rdacaa.business.validation.rule.atencion.cie10.condiciondiagnostico5");
	}
	
	@SuppressWarnings("unchecked")
	@Bean("ReglasParentescoAgresor")
	public RuleBook<RdacaaRawRowResult> ruleBookReglasParentescoAgresor() {
		return new RuleBookRunner("ec.gob.msp.rdacaa.business.validation.rule.atencion.violencia.parentescoagresor");
	}
	
	@SuppressWarnings("unchecked")
	@Bean("ReglasDatoAntropometricoTalla")
	public RuleBook<RdacaaRawRowResult> ruleBookReglasReglasDatoAntropometricoTalla() {
		return new RuleBookRunner("ec.gob.msp.rdacaa.business.validation.rule.atencion.datosantropometricos.talla");
	}
	
	@SuppressWarnings("unchecked")
	@Bean("ReglasDatoAntropometricoTallaCorregida")
	public RuleBook<RdacaaRawRowResult> ruleBookReglasReglasDatoAntropometricoTallaCorregida() {
		return new RuleBookRunner("ec.gob.msp.rdacaa.business.validation.rule.atencion.datosantropometricos.tallacorregida");
	}
	
	@SuppressWarnings("unchecked")
	@Bean("ReglasDatoAntropometricoPeso")
	public RuleBook<RdacaaRawRowResult> ruleBookReglasReglasDatoAntropometricoPeso() {
		return new RuleBookRunner("ec.gob.msp.rdacaa.business.validation.rule.atencion.datosantropometricos.peso");
	}
	
	@SuppressWarnings("unchecked")
	@Bean("ReglasDatoAntropometricoPerimetroCefalico")
	public RuleBook<RdacaaRawRowResult> ruleBookReglasReglasDatoAntropometricoPerimetroCefalico() {
		return new RuleBookRunner("ec.gob.msp.rdacaa.business.validation.rule.atencion.datosantropometricos.perimetrocefalico");
	}
	
	@SuppressWarnings("unchecked")
	@Bean("ReglasDatoAntropometricoIMC")
	public RuleBook<RdacaaRawRowResult> ruleBookReglasReglasDatoAntropometricoIMC() {
		return new RuleBookRunner("ec.gob.msp.rdacaa.business.validation.rule.atencion.datosantropometricos.imc");
	}
	
	@SuppressWarnings("unchecked")
	@Bean("ReglasDatoAntropometricoIMCClasificacion")
	public RuleBook<RdacaaRawRowResult> ruleBookReglasReglasDatoAntropometricoIMCClasificacion() {
		return new RuleBookRunner("ec.gob.msp.rdacaa.business.validation.rule.atencion.datosantropometricos.imcclasificacion");
	}
	
	@SuppressWarnings("unchecked")
	@Bean("ReglasDatoAntropometricoTipoTomaTalla")
	public RuleBook<RdacaaRawRowResult> ruleBookReglasReglasDatoAntropometricoTipoTomaTalla() {
		return new RuleBookRunner("ec.gob.msp.rdacaa.business.validation.rule.atencion.datosantropometricos.tallatipotoma");
	}
	
	@SuppressWarnings("unchecked")
	@Bean("ReglasDatoAntropometricoPuntajeZTallaParaEdad")
	public RuleBook<RdacaaRawRowResult> ruleBookReglasReglasDatoAntropometricoPuntajeZTallaParaEdad() {
		return new RuleBookRunner("ec.gob.msp.rdacaa.business.validation.rule.atencion.datosantropometricos.puntajeztallaparaedad");
	}
	
	@SuppressWarnings("unchecked")
	@Bean("ReglasDatoAntropometricoPuntajeZPesoParaEdad")
	public RuleBook<RdacaaRawRowResult> ruleBookReglasReglasDatoAntropometricoPuntajeZPesoParaEdad() {
		return new RuleBookRunner("ec.gob.msp.rdacaa.business.validation.rule.atencion.datosantropometricos.puntajezpesoparaedad");
	}
	
	@SuppressWarnings("unchecked")
	@Bean("ReglasDatoAntropometricoPuntajeZPesoParaTalla")
	public RuleBook<RdacaaRawRowResult> ruleBookReglasReglasDatoAntropometricoPuntajeZPesoParaTalla() {
		return new RuleBookRunner("ec.gob.msp.rdacaa.business.validation.rule.atencion.datosantropometricos.puntajezpesoparatalla");
	}
	
	@SuppressWarnings("unchecked")
	@Bean("ReglasDatoAntropometricoPuntajeZIMCParaEdad")
	public RuleBook<RdacaaRawRowResult> ruleBookReglasReglasDatoAntropometricoPuntajeZIMCParaEdad() {
		return new RuleBookRunner("ec.gob.msp.rdacaa.business.validation.rule.atencion.datosantropometricos.puntajezimcparaedad");
	}
	
	@SuppressWarnings("unchecked")
	@Bean("ReglasDatoAntropometricoPuntajeZPerimetroCefalicoParaEdad")
	public RuleBook<RdacaaRawRowResult> ruleBookReglasReglasDatoAntropometricoPerimetroCefalicoParaEdad() {
		return new RuleBookRunner("ec.gob.msp.rdacaa.business.validation.rule.atencion.datosantropometricos.puntajezperimetrocefalicoparaedad");
	}
	
	@SuppressWarnings("unchecked")
	@Bean("ReglasDatoAntropometricoCategoriaTallaParaEdad")
	public RuleBook<RdacaaRawRowResult> ruleBookReglasReglasDatoAntropometricoCategoriaTallaParaEdad() {
		return new RuleBookRunner("ec.gob.msp.rdacaa.business.validation.rule.atencion.datosantropometricos.categoriatallaparaedad");
	}
	
	@SuppressWarnings("unchecked")
	@Bean("ReglasDatoAntropometricoCategoriaPesoParaEdad")
	public RuleBook<RdacaaRawRowResult> ruleBookReglasReglasDatoAntropometricoCategoriaPesoParaEdad() {
		return new RuleBookRunner("ec.gob.msp.rdacaa.business.validation.rule.atencion.datosantropometricos.categoriapesoparaedad");
	}
	
	@SuppressWarnings("unchecked")
	@Bean("ReglasDatoAntropometricoCategoriaPesoParaTalla")
	public RuleBook<RdacaaRawRowResult> ruleBookReglasReglasDatoAntropometricoCategoriaPesoParaTalla() {
		return new RuleBookRunner("ec.gob.msp.rdacaa.business.validation.rule.atencion.datosantropometricos.categoriapesoparatalla");
	}
	
	@SuppressWarnings("unchecked")
	@Bean("ReglasDatoAntropometricoCategoriaIMCParaEdad")
	public RuleBook<RdacaaRawRowResult> ruleBookReglasReglasDatoAntropometricoCategoriaIMCParaEdad() {
		return new RuleBookRunner("ec.gob.msp.rdacaa.business.validation.rule.atencion.datosantropometricos.categoriaimcparaedad");
	}
	
	@SuppressWarnings("unchecked")
	@Bean("ReglasDatoAntropometricoCategoriaPerimetroCefalicoParaEdad")
	public RuleBook<RdacaaRawRowResult> ruleBookReglasReglasDatoAntropometricoCategoriaPerimetroCefalicoParaEdad() {
		return new RuleBookRunner("ec.gob.msp.rdacaa.business.validation.rule.atencion.datosantropometricos.categoriaperimetrocefalicoparaedad");
	}
	
	@SuppressWarnings("unchecked")
	@Bean("ReglasDatoAntropometricoValorHB")
	public RuleBook<RdacaaRawRowResult> ruleBookReglasReglasDatoAntropometricoValorHB() {
		return new RuleBookRunner("ec.gob.msp.rdacaa.business.validation.rule.atencion.datosantropometricos.valorhb");
	}
	
	@SuppressWarnings("unchecked")
	@Bean("ReglasDatoAntropometricoValorHBCorregido")
	public RuleBook<RdacaaRawRowResult> ruleBookReglasReglasDatoAntropometricoValorHBCorregido() {
		return new RuleBookRunner("ec.gob.msp.rdacaa.business.validation.rule.atencion.datosantropometricos.valorhbcorregido");
	}
	
	@SuppressWarnings("unchecked")
	@Bean("ReglasDatoAntropometricoValorHBIndicadorAnemia")
	public RuleBook<RdacaaRawRowResult> ruleBookReglasReglasDatoAntropometricoValorHBIndicadorAnemia() {
		return new RuleBookRunner("ec.gob.msp.rdacaa.business.validation.rule.atencion.datosantropometricos.valorhbindicadoranemia");
	}
        
        @SuppressWarnings("unchecked")
        @Bean("ReglasSubsistemaReferencia")
        public RuleBook<RdacaaRawRowResult> ruleBookReglasSubsistemaReferencia() {
            return new RuleBookRunner("ec.gob.msp.rdacaa.business.validation.rule.atencion.referencia.subsistema");
        }
        
        @SuppressWarnings("unchecked")
        @Bean("ReglasLugarReferencia")
        public RuleBook<RdacaaRawRowResult> ruleBookReglasLugarReferencia() {
            return new RuleBookRunner("ec.gob.msp.rdacaa.business.validation.rule.atencion.referencia.lugar");
        }
        
        @SuppressWarnings("unchecked")
        @Bean("ReglasInterconsulta")
        public RuleBook<RdacaaRawRowResult> ruleBookReglasInterconsulta() {
            return new RuleBookRunner("ec.gob.msp.rdacaa.business.validation.rule.persona.interconsulta");
        }
        
        @SuppressWarnings("unchecked")
        @Bean("ReglasProcedimiento1")
        public RuleBook<RdacaaRawRowResult> ruleBookReglasProcedimiento1() {
            return new RuleBookRunner("ec.gob.msp.rdacaa.business.validation.rule.atencion.procedimientos.prodto1");
        }
        
        @SuppressWarnings("unchecked")
        @Bean("ReglasProcedimiento2")
        public RuleBook<RdacaaRawRowResult> ruleBookReglasProcedimiento2() {
            return new RuleBookRunner("ec.gob.msp.rdacaa.business.validation.rule.atencion.procedimientos.prodto2");
        }
        
        @SuppressWarnings("unchecked")
        @Bean("ReglasProcedimiento3")
        public RuleBook<RdacaaRawRowResult> ruleBookReglasProcedimiento3() {
            return new RuleBookRunner("ec.gob.msp.rdacaa.business.validation.rule.atencion.procedimientos.prodto3");
        }
        
        @SuppressWarnings("unchecked")
        @Bean("ReglasProcedimiento4")
        public RuleBook<RdacaaRawRowResult> ruleBookReglasProcedimiento4() {
            return new RuleBookRunner("ec.gob.msp.rdacaa.business.validation.rule.atencion.procedimientos.prodto4");
        }
        
        @SuppressWarnings("unchecked")
        @Bean("ReglasProcedimiento5")
        public RuleBook<RdacaaRawRowResult> ruleBookReglasProcedimiento5() {
            return new RuleBookRunner("ec.gob.msp.rdacaa.business.validation.rule.atencion.procedimientos.prodto5");
        }
        @SuppressWarnings("unchecked")
        @Bean("ReglasActividadesProcedimientos1")
        public RuleBook<RdacaaRawRowResult> ruleBookReglasActividadesProcedimiento1() {
            return new RuleBookRunner("ec.gob.msp.rdacaa.business.validation.rule.atencion.procedimientos.actividad1");
        }
        
        @SuppressWarnings("unchecked")
        @Bean("ReglasActividadesProcedimientos2")
        public RuleBook<RdacaaRawRowResult> ruleBookReglasActividadesProcedimiento2() {
            return new RuleBookRunner("ec.gob.msp.rdacaa.business.validation.rule.atencion.procedimientos.actividad2");
        }
        
        @SuppressWarnings("unchecked")
        @Bean("ReglasActividadesProcedimientos3")
        public RuleBook<RdacaaRawRowResult> ruleBookReglasActividadesProcedimiento3() {
            return new RuleBookRunner("ec.gob.msp.rdacaa.business.validation.rule.atencion.procedimientos.actividad3");
        }
        
        @SuppressWarnings("unchecked")
        @Bean("ReglasActividadesProcedimientos4")
        public RuleBook<RdacaaRawRowResult> ruleBookReglasActividadesProcedimiento4() {
            return new RuleBookRunner("ec.gob.msp.rdacaa.business.validation.rule.atencion.procedimientos.actividad4");
        }
        
        @SuppressWarnings("unchecked")
        @Bean("ReglasActividadesProcedimientos5")
        public RuleBook<RdacaaRawRowResult> ruleBookReglasActividadesProcedimiento5() {
            return new RuleBookRunner("ec.gob.msp.rdacaa.business.validation.rule.atencion.procedimientos.actividad5");
        }
        @SuppressWarnings("unchecked")
        @Bean("ReglasDientesCariados")
        public RuleBook<RdacaaRawRowResult> ruleBookReglasDientesCariados() {
            return new RuleBookRunner("ec.gob.msp.rdacaa.business.validation.rule.atencion.odontologia.cariados");
        }
        
        @SuppressWarnings("unchecked")
        @Bean("ReglasDientesExtraidos")
        public RuleBook<RdacaaRawRowResult> ruleBookReglasDientesExtraidos() {
            return new RuleBookRunner("ec.gob.msp.rdacaa.business.validation.rule.atencion.odontologia.extraido");
        }
        
        @SuppressWarnings("unchecked")
        @Bean("ReglasDientesObturados")
        public RuleBook<RdacaaRawRowResult> ruleBookReglasDientesObturados() {
            return new RuleBookRunner("ec.gob.msp.rdacaa.business.validation.rule.atencion.odontologia.obturado");
        }
        
        @SuppressWarnings("unchecked")
        @Bean("ReglasDientesPerdidos")
        public RuleBook<RdacaaRawRowResult> ruleBookReglasDientesPerdidos() {
            return new RuleBookRunner("ec.gob.msp.rdacaa.business.validation.rule.atencion.odontologia.perdido");
        }
        
        @SuppressWarnings("unchecked")
        @Bean("ReglasSifilisNoTreponemica")
        public RuleBook<RdacaaRawRowResult> ruleBookReglasSifilisNoTreponemica() {
            return new RuleBookRunner("ec.gob.msp.rdacaa.business.validation.rule.atencion.exameneslab.sifilisnotreponemica");
        }
        
        @SuppressWarnings("unchecked")
        @Bean("ReglasSifilisTreponemica")
        public RuleBook<RdacaaRawRowResult> ruleBookReglasSifilisTreponemica() {
            return new RuleBookRunner("ec.gob.msp.rdacaa.business.validation.rule.atencion.exameneslab.sifilistreponemica");
        }
        
        @SuppressWarnings("unchecked")
        @Bean("ReglasSifilisTratamiento")
        public RuleBook<RdacaaRawRowResult> ruleBookReglasSifilisTratamiento() {
            return new RuleBookRunner("ec.gob.msp.rdacaa.business.validation.rule.atencion.exameneslab.sifilistratamiento");
        }
        
        @SuppressWarnings("unchecked")
        @Bean("ReglasSifilisTratamientoPareja")
        public RuleBook<RdacaaRawRowResult> ruleBookReglasSifilisTratamientoPareja() {
            return new RuleBookRunner("ec.gob.msp.rdacaa.business.validation.rule.atencion.exameneslab.sifilistratamientopareja");
        }
        
        @SuppressWarnings("unchecked")
        @Bean("ReglasResultadoBacteriuria")
        public RuleBook<RdacaaRawRowResult> ruleBookReglasResultadoBacteriuria() {
            return new RuleBookRunner("ec.gob.msp.rdacaa.business.validation.rule.atencion.exameneslab.resbacteriuria");
        }
        
        @SuppressWarnings("unchecked")
        @Bean("ReglasRiesgoObtetrico")
        public RuleBook<RdacaaRawRowResult> ruleBookReglasRiesgoObtetrico() {
            return new RuleBookRunner("ec.gob.msp.rdacaa.business.validation.rule.atencion.obstetricia.riesgoobstetrico");
        }
        
        @SuppressWarnings("unchecked")
        @Bean("ReglasPlanParto")
        public RuleBook<RdacaaRawRowResult> ruleBookReglasPlanParto() {
            return new RuleBookRunner("ec.gob.msp.rdacaa.business.validation.rule.atencion.obstetricia.planparto");
        }
        
        @SuppressWarnings("unchecked")
        @Bean("ReglasPlanTransporte")
        public RuleBook<RdacaaRawRowResult> ruleBookReglasPlanTransporte() {
            return new RuleBookRunner("ec.gob.msp.rdacaa.business.validation.rule.atencion.obstetricia.plantransporte");
        }
        
        @SuppressWarnings("unchecked")
        @Bean("ReglasEmbarazoPlanificado")
        public RuleBook<RdacaaRawRowResult> ruleBookReglasEmbarazoPlanificado() {
            return new RuleBookRunner("ec.gob.msp.rdacaa.business.validation.rule.atencion.obstetricia.embplanificado");
        }
        
        @SuppressWarnings("unchecked")
        @Bean("ReglasMetodoSemanagestacion")
        public RuleBook<RdacaaRawRowResult> ruleBookMetodoSemanagestacion() {
            return new RuleBookRunner("ec.gob.msp.rdacaa.business.validation.rule.atencion.obstetricia.metodosemgestacion");
        }
        
        @SuppressWarnings("unchecked")
        @Bean("ReglasSemanagestacion")
        public RuleBook<RdacaaRawRowResult> ruleBookReglasSemanagestacion() {
            return new RuleBookRunner("ec.gob.msp.rdacaa.business.validation.rule.atencion.obstetricia.semgestacion");
        }
        
        @SuppressWarnings("unchecked")
        @Bean("ReglasFUM")
        public RuleBook<RdacaaRawRowResult> ruleBookReglasFUM() {
            return new RuleBookRunner("ec.gob.msp.rdacaa.business.validation.rule.atencion.obstetricia.fum");
        }      
        
        @SuppressWarnings("unchecked")
        @Bean("ReglasSuplementoHierroMultivitaminas")
        public RuleBook<RdacaaRawRowResult> ruleBookReglasSuplementoHierroMultivitaminas() {
            return new RuleBookRunner("ec.gob.msp.rdacaa.business.validation.rule.atencion.prescripcion.suphierromultivita");
        }
        
        @SuppressWarnings("unchecked")
        @Bean("ReglasSuplementoVitaminaA")
        public RuleBook<RdacaaRawRowResult> ruleBookReglasSuplementoVitaminaA() {
            return new RuleBookRunner("ec.gob.msp.rdacaa.business.validation.rule.atencion.prescripcion.supvitaminaa");
        } 
        
        @SuppressWarnings("unchecked")
        @Bean("ReglasSuplementoHierroJarabe")
        public RuleBook<RdacaaRawRowResult> ruleBookReglasSuplementoHierroJarabe() {
            return new RuleBookRunner("ec.gob.msp.rdacaa.business.validation.rule.atencion.prescripcion.suphierrojarabe");
        }
        
        @SuppressWarnings("unchecked")
        @Bean("ReglasSuplementoHierroAcidoFolico")
        public RuleBook<RdacaaRawRowResult> ruleBookReglasSuplementoHierroAcidoFolico() {
            return new RuleBookRunner("ec.gob.msp.rdacaa.business.validation.rule.atencion.prescripcion.suphierroacidofolico");
        }
        
        @SuppressWarnings("unchecked")
        @Bean("ReglasMadrePeriodoLactancia")
        public RuleBook<RdacaaRawRowResult> ruleBookReglasMadrePeriodoLactancia() {
            return new RuleBookRunner("ec.gob.msp.rdacaa.business.validation.rule.atencion.sivan.madreperlactancia");
        }
        
        @SuppressWarnings("unchecked")
        @Bean("Reglas24HorasLeche")
        public RuleBook<RdacaaRawRowResult> ruleBookReglas24HorasLeche() {
            return new RuleBookRunner("ec.gob.msp.rdacaa.business.validation.rule.atencion.sivan.leche24horas");
        }
        
        @SuppressWarnings("unchecked")
        @Bean("Reglas24HorasAlimentos")
        public RuleBook<RdacaaRawRowResult> ruleBookReglas24HorasAlimentos() {
            return new RuleBookRunner("ec.gob.msp.rdacaa.business.validation.rule.atencion.sivan.alimentos24horas");
        }
        
        @SuppressWarnings("unchecked")
        @Bean("ReglasVacuna1")
        public RuleBook<RdacaaRawRowResult> ruleBookReglasVacuna1() {
            return new RuleBookRunner("ec.gob.msp.rdacaa.business.validation.rule.atencion.vacunacion.vacuna1");
        } 
        
        @SuppressWarnings("unchecked")
        @Bean("ReglasVacuna2")
        public RuleBook<RdacaaRawRowResult> ruleBookReglasVacuna2() {
            return new RuleBookRunner("ec.gob.msp.rdacaa.business.validation.rule.atencion.vacunacion.vacuna2");
        }
        
        @SuppressWarnings("unchecked")
        @Bean("ReglasVacuna3")
        public RuleBook<RdacaaRawRowResult> ruleBookReglasVacuna3() {
            return new RuleBookRunner("ec.gob.msp.rdacaa.business.validation.rule.atencion.vacunacion.vacuna3");
        }
        
        @SuppressWarnings("unchecked")
        @Bean("ReglasVacuna4")
        public RuleBook<RdacaaRawRowResult> ruleBookReglasVacuna4() {
            return new RuleBookRunner("ec.gob.msp.rdacaa.business.validation.rule.atencion.vacunacion.vacuna4");
        }
        
        @SuppressWarnings("unchecked")
        @Bean("ReglasVacuna5")
        public RuleBook<RdacaaRawRowResult> ruleBookReglasVacuna5() {
            return new RuleBookRunner("ec.gob.msp.rdacaa.business.validation.rule.atencion.vacunacion.vacuna5");
        }
        
        @SuppressWarnings("unchecked")
        @Bean("ReglasVacunaNroDosis1")
        public RuleBook<RdacaaRawRowResult> ruleBookReglasVacunaNroDosis1() {
            return new RuleBookRunner("ec.gob.msp.rdacaa.business.validation.rule.atencion.vacunacion.vacunanrodosis1");
        }
        
        @SuppressWarnings("unchecked")
        @Bean("ReglasVacunaNroDosis2")
        public RuleBook<RdacaaRawRowResult> ruleBookReglasVacunaNroDosis2() {
            return new RuleBookRunner("ec.gob.msp.rdacaa.business.validation.rule.atencion.vacunacion.vacunanrodosis2");
        }
        
        @SuppressWarnings("unchecked")
        @Bean("ReglasVacunaNroDosis3")
        public RuleBook<RdacaaRawRowResult> ruleBookReglasVacunaNroDosis3() {
            return new RuleBookRunner("ec.gob.msp.rdacaa.business.validation.rule.atencion.vacunacion.vacunanrodosis3");
        }
        
        @SuppressWarnings("unchecked")
        @Bean("ReglasVacunaNroDosis4")
        public RuleBook<RdacaaRawRowResult> ruleBookReglasVacunaNroDosis4() {
            return new RuleBookRunner("ec.gob.msp.rdacaa.business.validation.rule.atencion.vacunacion.vacunanrodosis4");
        }
        
        @SuppressWarnings("unchecked")
        @Bean("ReglasVacunaNroDosis5")
        public RuleBook<RdacaaRawRowResult> ruleBookReglasVacunaNroDosis5() {
            return new RuleBookRunner("ec.gob.msp.rdacaa.business.validation.rule.atencion.vacunacion.vacunanrodosis5");
        }
        
        @SuppressWarnings("unchecked")
        @Bean("ReglasVacunaEsquema1")
        public RuleBook<RdacaaRawRowResult> ruleBookReglasVacunaEsquema1() {
            return new RuleBookRunner("ec.gob.msp.rdacaa.business.validation.rule.atencion.vacunacion.vacunaesquema1");
        }
        
        @SuppressWarnings("unchecked")
        @Bean("ReglasVacunaEsquema2")
        public RuleBook<RdacaaRawRowResult> ruleBookReglasVacunaEsquema2() {
            return new RuleBookRunner("ec.gob.msp.rdacaa.business.validation.rule.atencion.vacunacion.vacunaesquema2");
        }
        
        @SuppressWarnings("unchecked")
        @Bean("ReglasVacunaEsquema3")
        public RuleBook<RdacaaRawRowResult> ruleBookReglasVacunaEsquema3() {
            return new RuleBookRunner("ec.gob.msp.rdacaa.business.validation.rule.atencion.vacunacion.vacunaesquema3");
        }
        
        @SuppressWarnings("unchecked")
        @Bean("ReglasVacunaEsquema4")
        public RuleBook<RdacaaRawRowResult> ruleBookReglasVacunaEsquema4() {
            return new RuleBookRunner("ec.gob.msp.rdacaa.business.validation.rule.atencion.vacunacion.vacunaesquema4");
        }
        
        @SuppressWarnings("unchecked")
        @Bean("ReglasVacunaEsquema5")
        public RuleBook<RdacaaRawRowResult> ruleBookReglasVacunaEsquema5() {
            return new RuleBookRunner("ec.gob.msp.rdacaa.business.validation.rule.atencion.vacunacion.vacunaesquema5");
        }
        
        @SuppressWarnings("unchecked")
        @Bean("ReglasVacunaFecha1")
        public RuleBook<RdacaaRawRowResult> ruleBookReglasVacunaFecha1() {
            return new RuleBookRunner("ec.gob.msp.rdacaa.business.validation.rule.atencion.vacunacion.vacunafecha1");
        }
        
        @SuppressWarnings("unchecked")
        @Bean("ReglasVacunaFecha2")
        public RuleBook<RdacaaRawRowResult> ruleBookReglasVacunaFecha2() {
            return new RuleBookRunner("ec.gob.msp.rdacaa.business.validation.rule.atencion.vacunacion.vacunafecha2");
        }
        
        @SuppressWarnings("unchecked")
        @Bean("ReglasVacunaFecha3")
        public RuleBook<RdacaaRawRowResult> ruleBookReglasVacunaFecha3() {
            return new RuleBookRunner("ec.gob.msp.rdacaa.business.validation.rule.atencion.vacunacion.vacunafecha3");
        }
        
        @SuppressWarnings("unchecked")
        @Bean("ReglasVacunaFecha4")
        public RuleBook<RdacaaRawRowResult> ruleBookReglasVacunaFecha4() {
            return new RuleBookRunner("ec.gob.msp.rdacaa.business.validation.rule.atencion.vacunacion.vacunafecha4");
        }
        
        @SuppressWarnings("unchecked")
        @Bean("ReglasVacunaFecha5")
        public RuleBook<RdacaaRawRowResult> ruleBookReglasVacunaFecha5() {
            return new RuleBookRunner("ec.gob.msp.rdacaa.business.validation.rule.atencion.vacunacion.vacunafecha5");
        }
        
        @SuppressWarnings("unchecked")
        @Bean("ReglasVacunaLote1")
        public RuleBook<RdacaaRawRowResult> ruleBookReglasVacunaLote1() {
            return new RuleBookRunner("ec.gob.msp.rdacaa.business.validation.rule.atencion.vacunacion.vacunalote1");
        }

        @SuppressWarnings("unchecked")
        @Bean("ReglasVacunaLote2")
        public RuleBook<RdacaaRawRowResult> ruleBookReglasVacunaLote2() {
            return new RuleBookRunner("ec.gob.msp.rdacaa.business.validation.rule.atencion.vacunacion.vacunalote2");
        }
        
        @SuppressWarnings("unchecked")
        @Bean("ReglasVacunaLote3")
        public RuleBook<RdacaaRawRowResult> ruleBookReglasVacunaLote3() {
            return new RuleBookRunner("ec.gob.msp.rdacaa.business.validation.rule.atencion.vacunacion.vacunalote3");
        }
        
        @SuppressWarnings("unchecked")
        @Bean("ReglasVacunaLote4")
        public RuleBook<RdacaaRawRowResult> ruleBookReglasVacunaLote4() {
            return new RuleBookRunner("ec.gob.msp.rdacaa.business.validation.rule.atencion.vacunacion.vacunalote4");
        }
        
        @SuppressWarnings("unchecked")
        @Bean("ReglasVacunaLote5")
        public RuleBook<RdacaaRawRowResult> ruleBookReglasVacunaLote5() {
            return new RuleBookRunner("ec.gob.msp.rdacaa.business.validation.rule.atencion.vacunacion.vacunalote5");
        }

        @SuppressWarnings("unchecked")
        @Bean("ReglasSoftwareVersion")
        public RuleBook<RdacaaRawRowResult> ruleBookReglasSoftwareVersion() {
            return new RuleBookRunner("ec.gob.msp.rdacaa.business.validation.rule.software.version");
        }
        
        @SuppressWarnings("unchecked")
        @Bean("ReglasFechaAtencion")
        public RuleBook<RdacaaRawRowResult> ruleBookReglasFechaAtencion() {
            return new RuleBookRunner("ec.gob.msp.rdacaa.business.validation.rule.atencion.fechaatencion");
        }
        
        @SuppressWarnings("unchecked")
        @Bean("ReglasProfesional")
        public RuleBook<RdacaaRawRowResult> ruleBookReglasProfesional() {
            return new RuleBookRunner("ec.gob.msp.rdacaa.business.validation.rule.atencion.profesional");
        }
        
        @SuppressWarnings("unchecked")
        @Bean("ReglasEspecialidadProfesional")
        public RuleBook<RdacaaRawRowResult> ruleBookReglasEspecialidadProfesional() {
            return new RuleBookRunner("ec.gob.msp.rdacaa.business.validation.rule.atencion.profespecialidad");
        }
        
        @SuppressWarnings("unchecked")
        @Bean("ReglasGrupoVulnerable1")
        public RuleBook<RdacaaRawRowResult> ruleBookReglasGrupoVulnerable1() {
            return new RuleBookRunner("ec.gob.msp.rdacaa.business.validation.rule.atencion.grupovulnerable.gpr_vulnerable1");
        }
        
        @SuppressWarnings("unchecked")
        @Bean("ReglasGrupoVulnerable2")
        public RuleBook<RdacaaRawRowResult> ruleBookReglasGrupoVulnerable2() {
            return new RuleBookRunner("ec.gob.msp.rdacaa.business.validation.rule.atencion.grupovulnerable.gpr_vulnerable2");
        }
        
        @SuppressWarnings("unchecked")
        @Bean("ReglasGrupoVulnerable3")
        public RuleBook<RdacaaRawRowResult> ruleBookReglasGrupoVulnerable3() {
            return new RuleBookRunner("ec.gob.msp.rdacaa.business.validation.rule.atencion.grupovulnerable.gpr_vulnerable3");
        }
        
        @SuppressWarnings("unchecked")
        @Bean("ReglasGrupoVulnerable4")
        public RuleBook<RdacaaRawRowResult> ruleBookReglasGrupoVulnerable4() {
            return new RuleBookRunner("ec.gob.msp.rdacaa.business.validation.rule.atencion.grupovulnerable.gpr_vulnerable4");
        }
        
        @SuppressWarnings("unchecked")
        @Bean("ReglasGrupoVulnerable5")
        public RuleBook<RdacaaRawRowResult> ruleBookReglasGrupoVulnerable5() {
            return new RuleBookRunner("ec.gob.msp.rdacaa.business.validation.rule.atencion.grupovulnerable.gpr_vulnerable5");
        }
        
        @SuppressWarnings("unchecked")
        @Bean("ReglasUbicacionParroquiaAtencion")
        public RuleBook<RdacaaRawRowResult> ruleBookReglasUbicacionParroquiaAtencion() {
            return new RuleBookRunner("ec.gob.msp.rdacaa.business.validation.rule.atencion.coddpaestparroquia");
        }
        
        @SuppressWarnings("unchecked")
        @Bean("ReglasVacunaGrupoRiesgo1")
        public RuleBook<RdacaaRawRowResult> ruleBookReglasVacunaGrupoRiesgo1() {
            return new RuleBookRunner("ec.gob.msp.rdacaa.business.validation.rule.atencion.vacunacion.vacunagrpriesgo1");
        }
        
        @SuppressWarnings("unchecked")
        @Bean("ReglasVacunaGrupoRiesgo2")
        public RuleBook<RdacaaRawRowResult> ruleBookReglasVacunaGrupoRiesgo2() {
            return new RuleBookRunner("ec.gob.msp.rdacaa.business.validation.rule.atencion.vacunacion.vacunagrpriesgo2");
        }
        
        @SuppressWarnings("unchecked")
        @Bean("ReglasVacunaGrupoRiesgo3")
        public RuleBook<RdacaaRawRowResult> ruleBookReglasVacunaGrupoRiesgo3() {
            return new RuleBookRunner("ec.gob.msp.rdacaa.business.validation.rule.atencion.vacunacion.vacunagrpriesgo3");
        }
        
        @SuppressWarnings("unchecked")
        @Bean("ReglasVacunaGrupoRiesgo4")
        public RuleBook<RdacaaRawRowResult> ruleBookReglasVacunaGrupoRiesgo4() {
            return new RuleBookRunner("ec.gob.msp.rdacaa.business.validation.rule.atencion.vacunacion.vacunagrpriesgo4");
        }
        
        @SuppressWarnings("unchecked")
        @Bean("ReglasVacunaGrupoRiesgo5")
        public RuleBook<RdacaaRawRowResult> ruleBookReglasVacunaGrupoRiesgo5() {
            return new RuleBookRunner("ec.gob.msp.rdacaa.business.validation.rule.atencion.vacunacion.vacunagrpriesgo5");
        }
}

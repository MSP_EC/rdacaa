package ec.gob.msp.rdacaa.business.validation.persist;

import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import ec.gob.msp.rdacaa.business.entity.Notificacion;
import ec.gob.msp.rdacaa.business.entity.Resultadovalidacion;
import ec.gob.msp.rdacaa.business.entity.Variable;
import ec.gob.msp.rdacaa.business.service.ResultadovalidacionService;
import ec.gob.msp.rdacaa.business.validation.common.ValidationResult;
import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogMap;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaRawRowResult;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableEntityCatalog;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableValueResult;

@Component
public class ValidationResultPersist {

	@Autowired
	private ResultadovalidacionService resultadovalidacionService;

	@Autowired
	private RdacaaVariableEntityCatalog rdacaaVariableEntityCatalog;

	@Autowired
	private ValidationResultCatalogMap validationResultCatalogMap;

	@Transactional(propagation = Propagation.REQUIRED)
	public void saveAllNotificationByAtencion(List<Optional<RdacaaRawRowResult>> listaOutputs) {

		resultadovalidacionService.truncateResultadovalidacionTable();
		for (Optional<RdacaaRawRowResult> resultOptional : listaOutputs) {

			if (resultOptional.isPresent()) {

				RdacaaRawRowResult result = resultOptional.get();

				String uuid = (String) result.get(RdacaaVariableKeyCatalog.PERSONA_IDENTIFICACION).getValue();

				List<Resultadovalidacion> resultToPersist = new ArrayList<>();

				result.entrySet().parallelStream().forEach((Entry<String, RdacaaVariableValueResult> e) -> {

					String key = e.getKey();
					Variable variable = rdacaaVariableEntityCatalog.get(key);

					RdacaaVariableValueResult rvr = e.getValue();

					Object value = rvr.getValue();

					List<ValidationResult> validationResultList = rvr.getValidationResultList();

					validationResultList.forEach(vr -> {

						Notificacion notificacion = validationResultCatalogMap.getNotificacion(vr.getCode());
						Resultadovalidacion rv = new Resultadovalidacion();
						rv.setNotificacionId(notificacion);
						rv.setNotificaciondescripcion(vr.getMessage());
						rv.setValor(value.toString());
						rv.setVariableId(variable);
						rv.setUuidatencion(uuid);

						resultToPersist.add(rv);
					});

					resultadovalidacionService.saveAll(resultToPersist);

				});
			}
		}
	}
}

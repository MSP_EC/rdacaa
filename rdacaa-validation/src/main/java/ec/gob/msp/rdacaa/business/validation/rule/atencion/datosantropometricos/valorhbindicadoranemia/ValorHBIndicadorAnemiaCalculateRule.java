package ec.gob.msp.rdacaa.business.validation.rule.atencion.datosantropometricos.valorhbindicadoranemia;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Given;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.service.MedicionPacienteAlertasService;
import ec.gob.msp.rdacaa.business.service.SignosVitalesReglasValidacion;
import ec.gob.msp.rdacaa.business.validation.common.ValidationResult;
import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.UtilsRule;

@Rule(order = 6)
public class ValorHBIndicadorAnemiaCalculateRule extends UtilsRule {
	
	private static final Logger logger = LoggerFactory.getLogger(ValorHBIndicadorAnemiaCalculateRule.class);

	private static final Integer IS_EMBARAZADA_GRUPO_PRIORITARIO_ID = 634;

	@Given("medicionPacienteAlertasService")
	private MedicionPacienteAlertasService medicionPacienteAlertasService;

	@When
	public boolean when() {
		return true;
	}

	@Then
	public RuleState then() {
		String valorHBIndicadorAnemia = "999990000066666";
		String valorHBCorregido = getRdacaaRawRowResultValue(
				RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_VALOR_HB_CORREGIDO);
		boolean valorHBCorregidoHasErrors = variableHasErrors(
				RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_VALOR_HB_CORREGIDO);
		Integer edadAnioMesDias = getEdadAnioMesDias();
		String sexo = getSexoString();
		boolean isEmbarazada = isEmbarazada();
		
		boolean valuesNotDefined = ValidationSupport.isNotDefined(valorHBCorregido);

		setRdacaaRawRowResultValue(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_CODIGO_INDICADOR_ANEMIA,
				valorHBIndicadorAnemia);

		if (edadAnioMesDias == null) {
			addValidationResult(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_CODIGO_INDICADOR_ANEMIA,
					ValidationResultCatalogConstants.CODIGO_ERROR_DAT_ANT_CODIGO_INDICADOR_ANEMIA_FECHAS_FORMATO_INCORRECTO);
		}

		if (sexo == null) {
			addValidationResult(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_CODIGO_INDICADOR_ANEMIA,
					ValidationResultCatalogConstants.CODIGO_ERROR_DAT_ANT_CODIGO_INDICADOR_ANEMIA_SEXO_INCORRECTO);
		}

		if (valorHBCorregidoHasErrors) {
			addValidationResult(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_CODIGO_INDICADOR_ANEMIA,
					ValidationResultCatalogConstants.CODIGO_ERROR_DAT_ANT_CODIGO_INDICADOR_ANEMIA_VALORHB_CORREGIDO_ERROR);
		}
		
		if(valuesNotDefined) {
			addValidationResult(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_CODIGO_INDICADOR_ANEMIA,
					ValidationResultCatalogConstants.CODIGO_INFO_DAT_ANT_CODIGO_INDICADOR_ANEMIA_VALORHB_CORREGIDO_NO_DEFINIDO);
		}

		if (sexo == null || edadAnioMesDias == null || valorHBCorregidoHasErrors || valuesNotDefined) {
			return RuleState.BREAK;
		}

		procesarHBRiesgoCorregidoParaEdad(valorHBCorregido, edadAnioMesDias, sexo,
				isEmbarazada ? IS_EMBARAZADA_GRUPO_PRIORITARIO_ID : null);

		return RuleState.NEXT;

	}

	private void procesarHBRiesgoCorregidoParaEdad(String valor, Integer edad, String sexo,
			Integer grupoPrioritarioEmbarazada) {
		try {
			SignosVitalesReglasValidacion alertaFound = medicionPacienteAlertasService
					.calculateIndicadorAnemiaHBRiesgoCorregido(valor, edad, sexo, grupoPrioritarioEmbarazada);
			if (alertaFound != null) {
				ValidationResult info = getValidationResultFromMap(
						ValidationResultCatalogConstants.CODIGO_INFO_DAT_ANT_CODIGO_INDICADOR_ANEMIA_VALOR);
				String message = info.getMessage();
				message = message + " \"" + alertaFound.getAlerta() + "\"";
				info.setMessage(message);

				setRdacaaRawRowResultValue(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_CODIGO_INDICADOR_ANEMIA,
						alertaFound.getIdSignoVitalAlerta().toString());
				addValidationResult(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_CODIGO_INDICADOR_ANEMIA, info);
			}
		} catch (Exception e) {
			logger.error("Error en procesarHBRiesgoCorregidoParaEdad",e);
			// Se controla mas adelante
		}
	}
}

package ec.gob.msp.rdacaa.business.validation.rule.atencion.procedimientos.prodto1;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;

@Rule(order = 5)
public class Procedimiento1IsNotMandatory extends BaseRule<String>  {

	@When
	public boolean when() {
		String procedimiento1 = getVariableFromMap(RdacaaVariableKeyCatalog.PROCEDIMIENTOS_CODIGO_1);
		return (!isMandatory(RdacaaVariableKeyCatalog.PROCEDIMIENTOS_CODIGO_1)
				&& ValidationSupport.isNotDefined(procedimiento1));
	}

	@Then
	public RuleState then() {
		return RuleState.BREAK;
	}
}

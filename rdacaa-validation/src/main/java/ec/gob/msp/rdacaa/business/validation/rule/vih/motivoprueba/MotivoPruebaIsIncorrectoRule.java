package ec.gob.msp.rdacaa.business.validation.rule.vih.motivoprueba;

import org.apache.commons.lang3.StringUtils;

import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;

/**
*
* @author dmurillo
*/
@Rule(order = 6)
public class MotivoPruebaIsIncorrectoRule extends BaseRule<String> {

	@When
	public boolean when() {
		String motivo = getVariableFromMap(RdacaaVariableKeyCatalog.VIH_CODIGO_MOTIVO_PRUEBA);
		if(StringUtils.isNumeric(motivo)){
			return !this.validationQueryService.isCodigoVihMotivoPruebaValido(Integer.valueOf(motivo));
		}
		return true;
	}

	@Then
	public void then() {
		addValidationResult(RdacaaVariableKeyCatalog.VIH_CODIGO_MOTIVO_PRUEBA,
				ValidationResultCatalogConstants.CODIGO_ERROR_VIH_CODIGO_MOTIVO_PRUEBA_INCORRECTO);
	}
}

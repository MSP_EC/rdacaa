package ec.gob.msp.rdacaa.business.validation.rule.vih.motivoprueba;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;

/**
*
* @author dmurillo
*/
@Rule(order = 4)
public class MotivoPruebaIsMandatoryRule extends BaseRule<String> {

	@When
	public boolean when() {
		return true;
	}

	@Then
	public RuleState then() {

		String motivo = getVariableFromMap(RdacaaVariableKeyCatalog.VIH_CODIGO_MOTIVO_PRUEBA);
		if (isMandatory(RdacaaVariableKeyCatalog.VIH_CODIGO_MOTIVO_PRUEBA)
				&& ValidationSupport.isNotDefined(motivo)) {
			addValidationResult(RdacaaVariableKeyCatalog.VIH_CODIGO_MOTIVO_PRUEBA,
					ValidationResultCatalogConstants.CODIGO_ERROR_VIH_CODIGO_MOTIVO_PRUEBA_ES_MANDATORIA);
			return RuleState.BREAK;
		} else if (!isMandatory(RdacaaVariableKeyCatalog.VIH_CODIGO_MOTIVO_PRUEBA)
				&& ValidationSupport.isNotDefined(motivo)) {
			return RuleState.BREAK;
		} else {
			return RuleState.NEXT;
		}

	}
}

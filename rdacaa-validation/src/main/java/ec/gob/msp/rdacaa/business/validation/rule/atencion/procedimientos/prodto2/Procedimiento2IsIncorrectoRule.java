package ec.gob.msp.rdacaa.business.validation.rule.atencion.procedimientos.prodto2;

import org.apache.commons.lang3.StringUtils;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;

@Rule(order = 7)
public class Procedimiento2IsIncorrectoRule extends BaseRule<String>{

	@When
	public boolean when() {	
		String procedimiento2 = getVariableFromMap(RdacaaVariableKeyCatalog.PROCEDIMIENTOS_CODIGO_2);
		if (StringUtils.isNumeric(procedimiento2)) {
			return !this.validationQueryService.isProcedimientoValido(procedimiento2);
		}
		return true;
	}

	@Then
	public RuleState then() {
		addValidationResult(RdacaaVariableKeyCatalog.PROCEDIMIENTOS_CODIGO_2,
				ValidationResultCatalogConstants.CODIGO_ERROR_PROCEDIMIENTO_2_INCORRECTO);
		return RuleState.BREAK;
	}
}

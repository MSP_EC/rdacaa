/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.gob.msp.rdacaa.business.validation.rule.atencion.referencia.lugar;

import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;
import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;

/**
 *
 * @author eduardo
 */
@Rule(order = 7)
public class LugarReferenciaIsIncorrectoRule extends BaseRule<String> {

	private static final int SUBSISTEMA_NO_APLICA = 2583;

	@When
	public boolean when() {
		String subsistemaReferencia = getVariableFromMap(
				RdacaaVariableKeyCatalog.REFERENCIA_CONTRAREFERENCIA_UNICODIGO_LUGAR_REFERIDO);
		String lugarReferencia = getVariableFromMap(
				RdacaaVariableKeyCatalog.REFERENCIA_CONTRAREFERENCIA_UNICODIGO_LUGAR_REFERIDO);

		boolean valor = true;
		if (subsistemaReferencia != null && Integer.valueOf(subsistemaReferencia) != SUBSISTEMA_NO_APLICA) {

			valor = !this.validationQueryService.isLugarReferenciaValida(Integer.valueOf(lugarReferencia));
		}
		return valor;
	}

	@Then
	public void then() {
		addValidationResult(RdacaaVariableKeyCatalog.REFERENCIA_CONTRAREFERENCIA_UNICODIGO_LUGAR_REFERIDO,
				ValidationResultCatalogConstants.CODIGO_ERROR_REFERENCIA_LUGAR_INCORRECTO);
	}
}

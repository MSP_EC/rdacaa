package ec.gob.msp.rdacaa.business.validation.rule.atencion.vacunacion.vacunagrpriesgo5;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.deliveredtechnologies.rulebook.model.RuleBook;

import ec.gob.msp.rdacaa.business.validation.malla.RdacaaRawRowResult;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.RdacaaRuleExecutor;

@Component
public class VacunaGrupoRiesgo5RuleGroupExecutor extends RdacaaRuleExecutor {

	@Autowired
	public VacunaGrupoRiesgo5RuleGroupExecutor(
			@Qualifier("ReglasVacunaGrupoRiesgo5") RuleBook<RdacaaRawRowResult> ruleBook) {
		super();
		this.ruleBook = ruleBook;
		this.variableKey = RdacaaVariableKeyCatalog.VACUNAS_CODIGO_GRUPO_RIESGO_5;
	}
}

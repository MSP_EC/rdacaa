/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.gob.msp.rdacaa.business.validation.rule.atencion.grupoprioritario.gpr_prioritario1;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;
import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;
import org.apache.commons.lang3.StringUtils;

/**
 *
 * @author miguel.faubla
 */
@Rule(order = 6)
public class GrupoPrioritario1IsNumericaRule extends BaseRule<String>  {
    @When
    public boolean when() {
        String grupoprioritario1 = getVariableFromMap(RdacaaVariableKeyCatalog.GRUPO_PRIORITARIO_1);
        return !StringUtils.isNumeric(grupoprioritario1);
    }

    @Then
    public RuleState then() {
        addValidationResult(RdacaaVariableKeyCatalog.GRUPO_PRIORITARIO_1, ValidationResultCatalogConstants.CODIGO_ERROR_GRUPO_PRIORITARIO_1_NONUMERICO);
        return RuleState.BREAK;
    }
}

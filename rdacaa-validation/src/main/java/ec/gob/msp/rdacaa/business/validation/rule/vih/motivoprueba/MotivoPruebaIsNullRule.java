package ec.gob.msp.rdacaa.business.validation.rule.vih.motivoprueba;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;

/**
*
* @author dmurillo
*/
@Rule(order = 2)
public class MotivoPruebaIsNullRule extends BaseRule<String> {

	@When
	public boolean when() {
		String motivo = getVariableFromMap(RdacaaVariableKeyCatalog.VIH_CODIGO_MOTIVO_PRUEBA);
		return ValidationSupport.isNullOrEmptyString(motivo);
	}

	@Then
	public RuleState then() {
		addValidationResult(RdacaaVariableKeyCatalog.VIH_CODIGO_MOTIVO_PRUEBA,
				ValidationResultCatalogConstants.CODIGO_ERROR_VIH_CODIGO_MOTIVO_PRUEBA_NULO);
		return RuleState.BREAK;
	}
}

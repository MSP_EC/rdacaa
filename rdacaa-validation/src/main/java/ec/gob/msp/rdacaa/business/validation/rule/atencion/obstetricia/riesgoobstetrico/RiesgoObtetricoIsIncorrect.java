package ec.gob.msp.rdacaa.business.validation.rule.atencion.obstetricia.riesgoobstetrico;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;

/**
 *
 * @author miguel.faubla
 */
@Rule(order = 5)
public class RiesgoObtetricoIsIncorrect extends BaseRule<String> {

	@When
	public boolean when() {
		String riesgoObtetrico = getVariableFromMap(RdacaaVariableKeyCatalog.OBSTETRICIA_CODIGO_RIESGO_OBSTETRICO);
		return (!isMandatory(RdacaaVariableKeyCatalog.OBSTETRICIA_CODIGO_RIESGO_OBSTETRICO)
				&& ValidationSupport.isNotDefined(riesgoObtetrico));
	}

	@Then
	public RuleState then() {
		return RuleState.BREAK;
	}
}

package ec.gob.msp.rdacaa.business.validation.rule.atencion.vacunacion.vacunaesquema4;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.deliveredtechnologies.rulebook.model.RuleBook;

import ec.gob.msp.rdacaa.business.validation.malla.RdacaaRawRowResult;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.RdacaaRuleExecutor;

@Component
public class VacunaEsquema4RuleGroupExecutor extends RdacaaRuleExecutor {

	@Autowired
	public VacunaEsquema4RuleGroupExecutor(
			@Qualifier("ReglasVacunaEsquema4") RuleBook<RdacaaRawRowResult> ruleBook) {
		super();
		this.ruleBook = ruleBook;
		this.variableKey = RdacaaVariableKeyCatalog.VACUNAS_CODIGO_ESQUEMA_4;
	}
}

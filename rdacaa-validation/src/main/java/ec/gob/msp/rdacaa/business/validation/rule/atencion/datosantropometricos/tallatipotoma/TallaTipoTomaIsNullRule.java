package ec.gob.msp.rdacaa.business.validation.rule.atencion.datosantropometricos.tallatipotoma;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;

@Rule(order = 2)
public class TallaTipoTomaIsNullRule extends BaseRule<String> {

	@When
	public boolean when() {
		String tallaTipoToma = getVariableFromMap(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_CODIGO_TIPO_TOMA_MEDIDA_TALLA);
		return ValidationSupport.isNullOrEmptyString(tallaTipoToma);
	}

	@Then
	public RuleState then() {
		addValidationResult(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_CODIGO_TIPO_TOMA_MEDIDA_TALLA,
				ValidationResultCatalogConstants.CODIGO_ERROR_COD_DAT_ANT_TIP_TOMA_TALLA_NULO);
		return RuleState.BREAK;
	}
}

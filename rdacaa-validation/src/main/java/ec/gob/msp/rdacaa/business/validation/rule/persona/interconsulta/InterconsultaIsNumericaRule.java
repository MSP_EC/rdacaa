package ec.gob.msp.rdacaa.business.validation.rule.persona.interconsulta;

import org.apache.commons.lang3.StringUtils;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;

@Rule(order = 6)
public class InterconsultaIsNumericaRule extends BaseRule<String> {

	@When
	public boolean when() {
		String interconsulta = getVariableFromMap(RdacaaVariableKeyCatalog.REFERENCIA_CONTRAREFERENCIA_CODIGO_INTERCONSULTA_TIPO);
		return !StringUtils.isNumeric(interconsulta);
	}
	
	@Then
	public RuleState then() {
		addValidationResult(RdacaaVariableKeyCatalog.REFERENCIA_CONTRAREFERENCIA_CODIGO_INTERCONSULTA_TIPO,
				ValidationResultCatalogConstants.CODIGO_ERROR_CODIGO_INTERCONSULTA_NO_NUMERICO);
		return RuleState.BREAK;
	}
}

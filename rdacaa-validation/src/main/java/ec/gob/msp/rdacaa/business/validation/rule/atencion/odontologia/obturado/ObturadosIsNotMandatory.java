package ec.gob.msp.rdacaa.business.validation.rule.atencion.odontologia.obturado;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;

@Rule(order = 5)
public class ObturadosIsNotMandatory extends BaseRule<String>{

	@When
	public boolean when() {
		
		String dientesObturados = getVariableFromMap(RdacaaVariableKeyCatalog.ODONTOLOGIA_CANTIDAD_PIEZAS_OBTURADAS);
		return (!isMandatory(RdacaaVariableKeyCatalog.ODONTOLOGIA_CANTIDAD_PIEZAS_OBTURADAS) &&
				ValidationSupport.isNotDefined(dientesObturados));
		
	}

	@Then
	public RuleState then() {
		return RuleState.BREAK;
	}
}

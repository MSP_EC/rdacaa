package ec.gob.msp.rdacaa.business.validation.rule.atencion.sivan.alimentos24horas;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.UtilsRule;

/**
 *
 * @author miguel.faubla
 */
@Rule(order = 6)
public class Alimentos24HorasIsIncorrect extends UtilsRule {

	@When
	public boolean when() {
		return true;
	}

	@Then
	public RuleState then() {
		String alimentos24Horas = getVariableFromMap(
				RdacaaVariableKeyCatalog.SIVAN_RECIBIO_ALIMENTOS_SOLIDOS_ULTIMAS_24H);

		Integer edadAniosMesesDias = getEdadAnioMesDias();

		boolean isValidValue = "0".equals(alimentos24Horas) || "1".equals(alimentos24Horas);

		if (!isValidValue) {
			addValidationResult(RdacaaVariableKeyCatalog.SIVAN_RECIBIO_ALIMENTOS_SOLIDOS_ULTIMAS_24H,
					ValidationResultCatalogConstants.CODIGO_ERROR_SIVAN_RECIBIO_ALIMENTOS_SOLIDOS_ULTIMAS_24H_ES_INCORRECTO);
		}

		if (edadAniosMesesDias == null) {
			addValidationResult(RdacaaVariableKeyCatalog.SIVAN_RECIBIO_ALIMENTOS_SOLIDOS_ULTIMAS_24H,
					ValidationResultCatalogConstants.CODIGO_ERROR_SIVAN_RECIBIO_ALIMENTOS_SOLIDOS_ULTIMAS_24H_FECHAS_INVALIDAS);
			return RuleState.BREAK;
		}

		boolean requiereEdad6mesesA8meses = !(edadAniosMesesDias >= 600 && edadAniosMesesDias < 900)
				&& !ValidationSupport.isNotDefined(alimentos24Horas);
		if (requiereEdad6mesesA8meses) {
			addValidationResult(RdacaaVariableKeyCatalog.SIVAN_RECIBIO_ALIMENTOS_SOLIDOS_ULTIMAS_24H,
					ValidationResultCatalogConstants.CODIGO_ERROR_SIVAN_RECIBIO_ALIMENTOS_SOLIDOS_ULTIMAS_24H_DEBE_TENER_DEFAULT_VALUE);
		}

		if (!isValidValue || requiereEdad6mesesA8meses) {
			return RuleState.BREAK;
		}

		return RuleState.NEXT;
	}
}

package ec.gob.msp.rdacaa.business.validation.rule.atencion.grupovulnerable.gpr_vulnerable2;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;

@Rule(order = 5)
public class GrupoVulnerable2IsNotMandatory extends BaseRule<String>{

	@When
	public boolean when() {
		String GrupoVulnerable2 = getVariableFromMap(RdacaaVariableKeyCatalog.GRUPO_VULNERABLE_2);
		return (!isMandatory(RdacaaVariableKeyCatalog.GRUPO_VULNERABLE_2) &&
				ValidationSupport.isNotDefined(GrupoVulnerable2));
		
	}

	@Then
	public RuleState then() {
		return RuleState.BREAK;
	}
}

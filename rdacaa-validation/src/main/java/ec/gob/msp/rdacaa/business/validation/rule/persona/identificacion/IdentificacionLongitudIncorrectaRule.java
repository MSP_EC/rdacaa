package ec.gob.msp.rdacaa.business.validation.rule.persona.identificacion;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.service.ConstantesDetalleCatalogo;
import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;

@Rule(order = 6)
public class IdentificacionLongitudIncorrectaRule extends BaseRule<String> {
	
	@When
	public boolean when() {
		String tipoIdentificacion = getVariableFromMap(RdacaaVariableKeyCatalog.PERSONA_CODIGO_TIPO_IDENTIFICACION);
		String identificacion = getVariableFromMap(RdacaaVariableKeyCatalog.PERSONA_IDENTIFICACION);
		Boolean valor = false;
		if(Integer.valueOf(tipoIdentificacion) == ConstantesDetalleCatalogo.DETALLECATALOGOCEDULA){
			valor = ValidationSupport.isStringMoreThanXCharacters(identificacion, 10);
			if(!valor){
				valor = ValidationSupport.isStringLessThanXCharacters(identificacion, 10);
			}
		}
		return valor;
	}

	@Then
	public RuleState then() {
		addValidationResult(RdacaaVariableKeyCatalog.PERSONA_IDENTIFICACION,
				ValidationResultCatalogConstants.CODIGO_ERROR_IDENTIFICACION_CEDULA_LONGITUD_INCORRECTA);
		return RuleState.BREAK;
	}
}

package ec.gob.msp.rdacaa.business.validation.rule.atencion.datosantropometricos.peso;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;

/**
 *
 * @author miguel.faubla
 */

@Rule(order = 3)
public class PesoIsExcludedRule extends BaseRule<String> {

	@When
	public boolean when() {
		return isExcludedFromValidation(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_PESO);
	}

	@Then
	public RuleState then() {

		String codigocie1 = getVariableFromMap(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_PESO);
		if (!ValidationSupport.isNotDefined(codigocie1)) {
			addValidationResult(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_PESO,
					ValidationResultCatalogConstants.CODIGO_ERROR_DAT_ANT_PESO_NO_DEFINIDA);
		}

		return RuleState.BREAK;

	}
}

package ec.gob.msp.rdacaa.business.validation.rule.atencion.datosantropometricos.categoriatallaparaedad;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.deliveredtechnologies.rulebook.Fact;
import com.deliveredtechnologies.rulebook.model.RuleBook;
import com.deliveredtechnologies.rulebook.util.ArrayUtils;

import ec.gob.msp.rdacaa.business.service.ScoreZAndCategoriaService;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaRawRow;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaRawRowResult;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.RdacaaRuleExecutor;

@Component
public class CategoriaTallaParaEdadRuleGroupExecutor extends RdacaaRuleExecutor {
	
	@Autowired
	private ScoreZAndCategoriaService scoreZAndCategoriaService;

	@Autowired
	public CategoriaTallaParaEdadRuleGroupExecutor(
			@Qualifier("ReglasDatoAntropometricoCategoriaTallaParaEdad") RuleBook<RdacaaRawRowResult> ruleBook) {
		super();
		this.ruleBook = ruleBook;
		this.variableKey = RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_CATEGORIA_TALLA_PARA_EDAD;
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public Optional<RdacaaRawRowResult> execute(RdacaaRawRow input, Fact... extraFacts) {
		Fact service = new Fact("scoreZAndCategoriaService", scoreZAndCategoriaService);
		Fact[] extra = ArrayUtils.combine(extraFacts, new Fact[] {service});
		return super.execute(input, extra);
	}
}

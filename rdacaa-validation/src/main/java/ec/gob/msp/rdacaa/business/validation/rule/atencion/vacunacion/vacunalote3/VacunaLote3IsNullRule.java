package ec.gob.msp.rdacaa.business.validation.rule.atencion.vacunacion.vacunalote3;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;

/**
 *
 * @author miguel.faubla
 */
@Rule(order = 2)
public class VacunaLote3IsNullRule extends BaseRule<String> {

	@When
	public boolean when() {
		String vacunaLote3 = getVariableFromMap(RdacaaVariableKeyCatalog.VACUNAS_CODIGO_LOTE_3);
		return ValidationSupport.isNullOrEmptyString(vacunaLote3);
	}

	@Then
	public RuleState then() {
		addValidationResult(RdacaaVariableKeyCatalog.VACUNAS_CODIGO_LOTE_3,
				ValidationResultCatalogConstants.CODIGO_ERROR_VACUNAS_CODIGO_LOTE_3_NULO);
		return RuleState.BREAK;
	}

}

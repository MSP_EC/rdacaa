package ec.gob.msp.rdacaa.business.validation.rule.persona.orientacionsexual;

import org.apache.commons.lang3.StringUtils;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;

/**
*
* @author dmurillo
*/
@Rule(order = 6)
public class OrientacionSexualIsNumericaRule extends BaseRule<String>  {
	
	@When
	public boolean when() {
		String orientacionSexual = getVariableFromMap(RdacaaVariableKeyCatalog.PERSONA_CODIGO_ORIENTACION_SEXUAL);
		return !StringUtils.isNumeric(orientacionSexual);
	}

	@Then
	public RuleState then() {
		addValidationResult(RdacaaVariableKeyCatalog.PERSONA_CODIGO_ORIENTACION_SEXUAL,
				ValidationResultCatalogConstants.CODIGO_ERROR_ORIENTACIONSEXUAL_NONUMERICA);
		return RuleState.BREAK;
	}
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.gob.msp.rdacaa.business.validation.rule.persona.identificacionrep;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;
import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;

/**
 *
 * @author eduardo
 */
@Rule(order = 2)
public class IdentificacionRepIsNullRule extends BaseRule<String> {
    @When
	public boolean when() {
		String identificacionRep = getVariableFromMap(RdacaaVariableKeyCatalog.PERSONA_IDENTIFICACION_REPRESENTANTE);
		return ValidationSupport.isNullOrEmptyString(identificacionRep);
	}

	@Then
	public RuleState then() {
		addValidationResult(RdacaaVariableKeyCatalog.PERSONA_IDENTIFICACION_REPRESENTANTE,
				ValidationResultCatalogConstants.CODIGO_ERROR_IDENTIFICACION_REP_NULO);
		return RuleState.BREAK;
	}
}

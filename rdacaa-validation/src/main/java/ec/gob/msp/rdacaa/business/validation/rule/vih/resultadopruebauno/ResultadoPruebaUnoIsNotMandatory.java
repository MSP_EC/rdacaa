package ec.gob.msp.rdacaa.business.validation.rule.vih.resultadopruebauno;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;

/**
*
* @author dmurillo
*/
@Rule(order = 5)
public class ResultadoPruebaUnoIsNotMandatory extends BaseRule<String> {

	@When
	public boolean when() {
		Boolean valor = false;
		if(!variableHasErrors(RdacaaVariableKeyCatalog.VIH_CODIGO_PRIMERA_PRUEBA)){
			String prueba = getVariableFromMap(RdacaaVariableKeyCatalog.VIH_VALOR_RESULTADO_PRIMERA_PRUEBA);
			valor = (!isMandatory(RdacaaVariableKeyCatalog.VIH_VALOR_RESULTADO_PRIMERA_PRUEBA) &&
					ValidationSupport.isNotDefined(prueba));
		}
		return valor;
	}

	@Then
	public RuleState then() {
		return RuleState.BREAK;
	}
}

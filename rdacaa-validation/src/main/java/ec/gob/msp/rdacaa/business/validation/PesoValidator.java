/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.gob.msp.rdacaa.business.validation;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import ec.gob.msp.rdacaa.business.service.SignosVitalesReglasValidacion;
import ec.gob.msp.rdacaa.business.service.SignosvitalesreglasService;
import ec.gob.msp.rdacaa.business.validation.common.ValidationResult;
import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalog;
import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.common.Validator;

/**
 *
 * @author Saulo Velasco
 */
@Component
public class PesoValidator extends ValidationSupport implements Validator<String[]> {

	private static final long serialVersionUID = 6205389239796060124L;
	@Autowired
    private SignosvitalesreglasService signosvitalesreglasService;
    private List<SignosVitalesReglasValidacion> reglasPeso;
    public static final BigDecimal PESO_MIN_FUERA_RANGO = new BigDecimal("0.5");
    public static final BigDecimal PESO_MAX_FUERA_RANGO = new BigDecimal("500.0");
    private static final String AMARILLO = "amarillo";
	private static final String AMBOS_SEXOS = "A";

    @PostConstruct
    private void init() {
        reglasPeso = signosvitalesreglasService.findAllReglasPeso();
    }

    @Override
    public List<ValidationResult> validate(String[] pesoEdadSexo) {

        List<ValidationResult> listaErrores = new ArrayList<>();

        if (isNullOrEmptyString(pesoEdadSexo[0])) {
            listaErrores.add(ValidationResultCatalog.PESO_VACIO.getValidationError());
        } else {
            BigDecimal peso = new BigDecimal(pesoEdadSexo[0]);
            Integer edad = Integer.parseInt(pesoEdadSexo[1]);
            String sexo = pesoEdadSexo[2];
            Optional<ValidationResult> warning = procesarPesoParaEdad(peso, edad, sexo);
            if (warning.isPresent()) {
                listaErrores.add(warning.get());
            }
        }

        return listaErrores;
    }

    private Optional<ValidationResult> procesarPesoParaEdad(BigDecimal peso, Integer edad, String sexo) {
        for (SignosVitalesReglasValidacion regla : reglasPeso) {
            if (isEdadPacienteDentroRango(edad, regla.getEdadminima(), regla.getEdadmaxima()) 
            		&& isSexoCorrespondiente(sexo, regla.getSexo())
            		&& isPesoFueraDeRango(peso, regla.getValorminimo(), regla.getValormaximo() )) {
                    ValidationResult warning = ValidationResultCatalog.PESO_FUERA_RANGO_PARA_EDAD.getValidationError();
                    String message = warning.getMessage();
                    message = message.replace("act", peso.toString());
                    message = message.replace("pesomin", regla.getValorminimo().toString());
                    message = message.replace("pesomax", regla.getValormaximo().toString());
                    message = message.replace("edadmin", regla.getEdadminimaAnios() + " años " + regla.getEdadminimaMeses() + " meses " + regla.getEdadminimaDias() + " días" );
                    message = message.replace("edadmax", regla.getEdadmaximaAnios() + " años " + regla.getEdadmaximaMeses() + " meses " + regla.getEdadmaximaDias() + " días" );
                    warning = new ValidationResult(warning.getCode(), message);
                    warning.setColor(AMARILLO);
                    return Optional.of(warning);
            }
        }
        if (isPesoFueraDeRango(peso, PESO_MIN_FUERA_RANGO, PESO_MAX_FUERA_RANGO)) {
            ValidationResult warning = ValidationResultCatalog.PESO_FUERA_RANGO_PARA_EDAD.getValidationError();
            String message = warning.getMessage();
            message = message.replace("act", peso.toString());
            message = message.replace("pesomin", PESO_MIN_FUERA_RANGO.toString());
            message = message.replace("pesomax", PESO_MAX_FUERA_RANGO.toString());
            message = message.replace(" PARA LA EDAD DE edadmin / edadmax", "");
            warning = new ValidationResult(warning.getCode(), message);
            warning.setColor(AMARILLO);
            return Optional.of(warning);
        }
        return Optional.empty();
    }
    
	private boolean isEdadPacienteDentroRango(Integer edad, Integer edadMin, Integer edadMax) {
		return edad >= edadMin && edad <= edadMax;
	}
	
	private boolean isSexoCorrespondiente(String sexoIn, String sexoRegla) {
		return sexoRegla.equals(sexoIn) || sexoRegla.equals(AMBOS_SEXOS);
	}
	
	private boolean isPesoFueraDeRango(BigDecimal peso, BigDecimal pesoMin, BigDecimal pesoMax) {
		return !(peso.compareTo(pesoMin) >= 0 && peso.compareTo(pesoMax) <= 0);
	}
	
}

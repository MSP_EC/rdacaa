package ec.gob.msp.rdacaa.business.validation.rule.atencion.vacunacion.vacunafecha3;

import java.util.Date;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.UtilsRule;

/**
 *
 * @author miguel.faubla
 */
@Rule(order = 6)
public class VacunaFecha3IsIncorrectRule extends UtilsRule {

	@When
	public boolean when() {
		return true;
	}

	@Then
	public RuleState then() {

		String vacunaFechaString3 = getVariableFromMap(RdacaaVariableKeyCatalog.VACUNAS_CODIGO_FECHA_3);
		String fechaAtencionString = getVariableFromMap(RdacaaVariableKeyCatalog.ATENCION_FECHA);
		boolean isFechaVacunaValida = isFechaValida(vacunaFechaString3) && 
				getFechaFromString(vacunaFechaString3).after(getFechaFromString("2018-12-31"));
		if (!isFechaVacunaValida) {
			addValidationResult(RdacaaVariableKeyCatalog.VACUNAS_CODIGO_FECHA_3,
					ValidationResultCatalogConstants.CODIGO_ERROR_VACUNAS_CODIGO_FECHA_3_ES_INCORRECTA);
			return RuleState.BREAK;
		}

		boolean isFechaAtencionValida =isFechaValida(fechaAtencionString) && 
				getFechaFromString(fechaAtencionString).after(getFechaFromString("2018-12-31"));
		if (!isFechaAtencionValida) {
			addValidationResult(RdacaaVariableKeyCatalog.VACUNAS_CODIGO_FECHA_3,
					ValidationResultCatalogConstants.CODIGO_ERROR_VACUNAS_CODIGO_FECHA_3_FECHA_ATENCION_ES_INCORRECTA);
			return RuleState.BREAK;
		}

		Date vacunaFecha3 = getFechaFromString(vacunaFechaString3);
		Date fechaAtencion = getFechaFromString(fechaAtencionString);
		
		if(vacunaFecha3.after(fechaAtencion)) {
			addValidationResult(RdacaaVariableKeyCatalog.VACUNAS_CODIGO_FECHA_3,
					ValidationResultCatalogConstants.CODIGO_ERROR_VACUNAS_CODIGO_FECHA_3_POSTERIOR_FECHA_ATENCION);
			return RuleState.BREAK;
		}
		
		return RuleState.NEXT;
	}
}

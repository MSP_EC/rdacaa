package ec.gob.msp.rdacaa.business.validation.rule.persona.sexo;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;

/**
*
* @author dmurillo
*/
@Rule(order = 5)
public class SexoIsNotMandatory extends BaseRule<String> {

	@When
	public boolean when() {
		
		String tipoIdentificacion = getVariableFromMap(RdacaaVariableKeyCatalog.PERSONA_CODIGO_SEXO);
		return (!isMandatory(RdacaaVariableKeyCatalog.PERSONA_CODIGO_SEXO) &&
				ValidationSupport.isNotDefined(tipoIdentificacion));
		
	}

	@Then
	public RuleState then() {
		return RuleState.BREAK;
	}
}

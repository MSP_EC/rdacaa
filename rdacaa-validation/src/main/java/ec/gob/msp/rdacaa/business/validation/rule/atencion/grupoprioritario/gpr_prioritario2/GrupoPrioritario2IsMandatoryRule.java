package ec.gob.msp.rdacaa.business.validation.rule.atencion.grupoprioritario.gpr_prioritario2;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;

/**
 *
 * @author miguel.faubla
 */
@Rule(order = 4)
public class GrupoPrioritario2IsMandatoryRule extends BaseRule<String> {

	@When
	public boolean when() {
		return true;

	}

	@Then
	public RuleState then() {

		String grupoprioritario2 = getVariableFromMap(RdacaaVariableKeyCatalog.GRUPO_PRIORITARIO_2);
		if (isMandatory(RdacaaVariableKeyCatalog.GRUPO_PRIORITARIO_2)
				&& ValidationSupport.isNotDefined(grupoprioritario2)) {
			addValidationResult(RdacaaVariableKeyCatalog.GRUPO_PRIORITARIO_2,
					ValidationResultCatalogConstants.CODIGO_ERROR_GRUPO_PRIORITARIO_2_ES_MANDATORIA);
			return RuleState.BREAK;
		} else if (!isMandatory(RdacaaVariableKeyCatalog.GRUPO_PRIORITARIO_2)
				&& ValidationSupport.isNotDefined(grupoprioritario2)) {
			return RuleState.BREAK;
		} else {
			return RuleState.NEXT;
		}

	}

}

package ec.gob.msp.rdacaa.business.validation.rule.atencion.exameneslab.sifilistreponemica;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.deliveredtechnologies.rulebook.model.RuleBook;

import ec.gob.msp.rdacaa.business.validation.malla.RdacaaRawRowResult;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.RdacaaRuleExecutor;

@Component
public class SifilisTreponemicaRuleGroupExecutor extends RdacaaRuleExecutor {

	@Autowired
	public SifilisTreponemicaRuleGroupExecutor(
			@Qualifier("ReglasSifilisTreponemica") RuleBook<RdacaaRawRowResult> ruleBook) {
		super();
		this.ruleBook = ruleBook;
		this.variableKey = RdacaaVariableKeyCatalog.EXAMENES_LABORATORIO_SIFILIS_TREPONEMICA_RESULTADO;
	}
}

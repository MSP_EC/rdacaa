/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.gob.msp.rdacaa.business.validation.rule.atencion.cie10.condiciondiagnostico2;

import org.apache.commons.lang3.StringUtils;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;

/**
 *
 * @author miguel.faubla
 */
@Rule(order = 7)
public class CondicionDiagnostico2IsIncorrectoRule extends BaseRule<String> {
	@When
	public boolean when() {
		String condiciondiagnostico2 = getVariableFromMap(RdacaaVariableKeyCatalog.DIAGNOSTICO_CODIGO_CONDICION_DIAG_2);
		if (StringUtils.isNumeric(condiciondiagnostico2)) {
			return !this.validationQueryService.isCodCondicionDiagnosticoValido(Integer.valueOf(condiciondiagnostico2));
		}
		return true;
	}

	@Then
	public RuleState then() {
		addValidationResult(RdacaaVariableKeyCatalog.DIAGNOSTICO_CODIGO_CONDICION_DIAG_2,
				ValidationResultCatalogConstants.CODIGO_ERROR_CONDICION_DIAGNOSTICO_2_INCORRECTO);
		return RuleState.BREAK;
	}
}

package ec.gob.msp.rdacaa.business.validation.rule.atencion.vacunacion.vacunaesquema4;

import org.apache.commons.lang3.StringUtils;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.UtilsRule;

/**
 *
 * @author miguel.faubla
 */
@Rule(order = 6)
public class VacunaEsquema4IsIncorrectRule extends UtilsRule {

	@When
	public boolean when() {
		return true;
	}

	@Then
	public RuleState then() {

		String vacunaEsquema4 = getVariableFromMap(RdacaaVariableKeyCatalog.VACUNAS_CODIGO_ESQUEMA_4);
		String vacuna4Id = getVariableFromMap(RdacaaVariableKeyCatalog.VACUNAS_CODIGO_APLICACION_4);
		String vacunaNroDosis4 = getVariableFromMap(RdacaaVariableKeyCatalog.VACUNAS_CODIGO_DOSIS_4);

		if (!(StringUtils.isNumeric(vacunaEsquema4) && StringUtils.isNumeric(vacuna4Id)
				&& StringUtils.isNumeric(vacunaNroDosis4))) {
			addValidationResult(RdacaaVariableKeyCatalog.VACUNAS_CODIGO_ESQUEMA_4,
					ValidationResultCatalogConstants.CODIGO_ERROR_VACUNAS_CODIGO_ESQUEMA_4_COD_VACUNA_DOSIS_NUMERICOS);
			return RuleState.BREAK;
		}

		Integer edadAnioParcial = getEdadAnioParcial();

		Integer edadMesParcial = getEdadMesParcial();

		Integer edadDiasParcial = getEdadDiasParcial();

		if (!validationQueryService.isEsquemaVacunacionValido(vacunaEsquema4, vacuna4Id, vacunaNroDosis4)) {
			addValidationResult(RdacaaVariableKeyCatalog.VACUNAS_CODIGO_ESQUEMA_4,
					ValidationResultCatalogConstants.CODIGO_ERROR_VACUNAS_CODIGO_ESQUEMA_4_ES_INCORRECTO);
			return RuleState.BREAK;
		} else if (edadAnioParcial != null && edadMesParcial != null && edadDiasParcial != null
				&& validationQueryService.verifyApplicationVacunaByEdadAndEsquemaId(edadDiasParcial, edadMesParcial,
				edadAnioParcial, Integer.parseInt(vacunaEsquema4)) == null) {
			addValidationResult(RdacaaVariableKeyCatalog.VACUNAS_CODIGO_ESQUEMA_4,
					ValidationResultCatalogConstants.CODIGO_ERROR_VACUNAS_CODIGO_ESQUEMA_4_EDAD_INCORRECTA);
			return RuleState.BREAK;
		} else {
			return RuleState.NEXT;
		}
		
	}
}

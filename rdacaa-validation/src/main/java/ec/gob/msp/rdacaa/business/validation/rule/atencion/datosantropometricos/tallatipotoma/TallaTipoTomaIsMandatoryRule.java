package ec.gob.msp.rdacaa.business.validation.rule.atencion.datosantropometricos.tallatipotoma;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;

/**
*
* @author dmurillo
*/
@Rule(order = 4)
public class TallaTipoTomaIsMandatoryRule extends BaseRule<String> {
	@When
	public boolean when() {
		return true;

	}

	@Then
	public RuleState then() {

		String tallaTipoToma = getVariableFromMap(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_CODIGO_TIPO_TOMA_MEDIDA_TALLA);
		if (isMandatory(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_CODIGO_TIPO_TOMA_MEDIDA_TALLA)
				&& ValidationSupport.isNotDefined(tallaTipoToma)) {
			addValidationResult(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_CODIGO_TIPO_TOMA_MEDIDA_TALLA,
					ValidationResultCatalogConstants.CODIGO_ERROR_COD_DAT_ANT_TIP_TOMA_TALLA_ES_MANDATORIA);
			return RuleState.BREAK;
		} else {
			return RuleState.NEXT;
		}
	}
}

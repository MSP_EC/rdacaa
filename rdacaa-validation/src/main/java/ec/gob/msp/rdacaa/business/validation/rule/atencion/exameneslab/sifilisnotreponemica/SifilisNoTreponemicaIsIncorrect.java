package ec.gob.msp.rdacaa.business.validation.rule.atencion.exameneslab.sifilisnotreponemica;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.UtilsRule;

/**
 *
 * @author miguel.faubla
 */
@Rule(order = 6)
public class SifilisNoTreponemicaIsIncorrect extends UtilsRule {

	@When
	public boolean when() {
		return true;
	}

	@Then
	public RuleState then() {

		String sifilisNoTreponemica = getVariableFromMap(
				RdacaaVariableKeyCatalog.EXAMENES_LABORATORIO_SIFILIS_NO_TREPONEMICA_RESULTADO);
		String semGestacion = getVariableFromMap(RdacaaVariableKeyCatalog.OBSTETRICIA_VALOR_SEMANA_GESTACION);
		boolean semGestacionHasErrors = variableHasErrors(RdacaaVariableKeyCatalog.OBSTETRICIA_VALOR_SEMANA_GESTACION);

		boolean isEmbarazada = isEmbarazada();
		boolean isMujer = "F".equals(getSexoString());

		boolean isValidValue = "1".equals(sifilisNoTreponemica.trim()) || "2".equals(sifilisNoTreponemica.trim())
				|| "3".equals(sifilisNoTreponemica.trim());

		if (!isValidValue) {
			addValidationResult(RdacaaVariableKeyCatalog.EXAMENES_LABORATORIO_SIFILIS_NO_TREPONEMICA_RESULTADO,
					ValidationResultCatalogConstants.CODIGO_ERROR_EXAMENES_LABORATORIO_SIFILIS_NO_TREPONEMICA_RESULTADO_ES_INCORRECTO);
		}

		boolean requiereMujerEmbarazadaParaValorValido = !(isMujer && isEmbarazada) && isValidValue;
		if (requiereMujerEmbarazadaParaValorValido) {
			addValidationResult(RdacaaVariableKeyCatalog.EXAMENES_LABORATORIO_SIFILIS_NO_TREPONEMICA_RESULTADO,
					ValidationResultCatalogConstants.CODIGO_ERROR_EXAMENES_LABORATORIO_SIFILIS_NO_TREPONEMICA_RESULTADO_REQUIERE_MUJER_EMBARAZADA);
		}

		if (semGestacionHasErrors) {
			addValidationResult(RdacaaVariableKeyCatalog.EXAMENES_LABORATORIO_SIFILIS_NO_TREPONEMICA_RESULTADO,
					ValidationResultCatalogConstants.CODIGO_ERROR_EXAMENES_LABORATORIO_SIFILIS_NO_TREPONEMICA_RESULTADO_SEMANA_GESTACION_TIENE_ERRORES);
		}
		
		boolean esSemana20 = !semGestacionHasErrors && Double.parseDouble(semGestacion) > 20
				&& Double.parseDouble(semGestacion) < 21;
		
		if (esSemana20 && !ValidationSupport.isNotDefined(sifilisNoTreponemica)) {
			addValidationResult(RdacaaVariableKeyCatalog.EXAMENES_LABORATORIO_SIFILIS_NO_TREPONEMICA_RESULTADO,
					ValidationResultCatalogConstants.CODIGO_ERROR_EXAMENES_LABORATORIO_SIFILIS_NO_TREPONEMICA_RESULTADO_SEMANA_20_DEBE_TENER_DEFAULT_VALUE);
		}

		if (!isValidValue || requiereMujerEmbarazadaParaValorValido || semGestacionHasErrors
				|| (esSemana20 && !ValidationSupport.isNotDefined(sifilisNoTreponemica))) {
			return RuleState.BREAK;
		}

		return RuleState.NEXT;
	}
}

package ec.gob.msp.rdacaa.business.validation.rule.persona.pueblos;

import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;

/**
*
* @author dmurillo
*/
@Rule(order = 6)
public class PueblosIsIncorrectoRule extends BaseRule<String> {

	private static final int KICHWA = 32;
	
	@When
	public boolean when() {
		String nacionalidadEtnica = getVariableFromMap(RdacaaVariableKeyCatalog.PERSONA_CODIGO_NACIONALIDAD_ETNICA);
		String pueblo = getVariableFromMap(RdacaaVariableKeyCatalog.PERSONA_CODIGO_PUEBLO);
		boolean valor = true;
		if(nacionalidadEtnica != null && Integer.valueOf(nacionalidadEtnica) == KICHWA && pueblo != null){
			valor = !this.validationQueryService.isPuebloValido(Integer.valueOf(pueblo));
		}
		return valor;
	}

	@Then
	public void then() {
		addValidationResult(RdacaaVariableKeyCatalog.PERSONA_CODIGO_PUEBLO,
				ValidationResultCatalogConstants.CODIGO_ERROR_PUEBLOS_INCORRECTO);
	}
}

package ec.gob.msp.rdacaa.business.validation.rule.atencion.procedimientos.actividad1;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;

@Rule(order = 4)
public class Actividades1IsMandatoryRule extends BaseRule<String>{

	@When
	public boolean when() {
		return true;

	}

	@Then
	public RuleState then() {

		String actividades1 = getVariableFromMap(
				RdacaaVariableKeyCatalog.PROCEDIMIENTOS_CANTIDAD_1);
		if (isMandatory(RdacaaVariableKeyCatalog.PROCEDIMIENTOS_CANTIDAD_1)
				&& ValidationSupport.isNotDefined(actividades1)) {
			addValidationResult(RdacaaVariableKeyCatalog.PROCEDIMIENTOS_CANTIDAD_1,
					ValidationResultCatalogConstants.CODIGO_ERROR_ACTIVIDAD_1_ES_MANDATORIO);
			return RuleState.BREAK;
		} else if (!isMandatory(RdacaaVariableKeyCatalog.PROCEDIMIENTOS_CANTIDAD_1)
				&& ValidationSupport.isNotDefined(actividades1)) {
			return RuleState.BREAK;
		} else {
			return RuleState.NEXT;
		}

	}
}

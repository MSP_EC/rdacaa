package ec.gob.msp.rdacaa.business.validation.database.common;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.crypto.tink.KeysetHandle;

import ec.gob.msp.rdacaa.seguridades.util.SeguridadUtil;
import ec.gob.msp.rdacaa.utilitario.exportdatabase.AtencionCipherDto;

public class DecipheringWithPrivateKeyDataFunction implements Function<Object[], Object> {

	private static final Logger logger = LoggerFactory.getLogger(DecipheringWithPrivateKeyDataFunction.class);

	private KeysetHandle keysetHandle;

	@Override
	public Object apply(Object[] cipherObjects) {

		List<AtencionCipherDto> listaCipherDto = new ArrayList<>();

		if (cipherObjects[0] instanceof ResultSet) {
			ResultSet rs = (ResultSet) cipherObjects[0];
			String privateKeyPath = (String) cipherObjects[1];

			try {
				if (keysetHandle == null) {
					keysetHandle = SeguridadUtil.leerLlavePrivada(privateKeyPath);
				}

				mapToAtencionCipherDto(listaCipherDto, rs);
				decipherVariables(listaCipherDto, keysetHandle);
			} catch (SQLException | GeneralSecurityException | IOException e) {
				throw new CustomSQLException("Error obteniendo datos del ResultSet", e);
			}
		}

		return listaCipherDto;
	}

	private void decipherVariables(List<AtencionCipherDto> listaCipherDto, KeysetHandle keysetHandle) {

		listaCipherDto.parallelStream().forEach(dto -> {
			try {
				String kuCident = SeguridadUtil.descifrarPrivadoString(dto.getKuCident(), keysetHandle);
				dto.setKsCident(kuCident);
				logger.debug("kuCident => {}", kuCident);
			} catch (GeneralSecurityException e) {
				throw new DecipheringErrorException("Error descifrando el archivo", e);
			}
		});
	}

	private void mapToAtencionCipherDto(List<AtencionCipherDto> listaCipherDto, ResultSet rs) throws SQLException {

		while (rs.next()) {
			AtencionCipherDto dto = new AtencionCipherDto();
			dto.setUuid(rs.getString("COD_UUID"));
			dto.setKsCident(rs.getString("KS_CIDENT"));
			dto.setKuCident(rs.getString("KU_CIDENT"));
			dto.setKuKeyid(rs.getString("KU_KEYID"));
			listaCipherDto.add(dto);
		}
	}
}

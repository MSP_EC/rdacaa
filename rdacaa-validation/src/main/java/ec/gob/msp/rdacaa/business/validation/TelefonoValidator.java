package ec.gob.msp.rdacaa.business.validation;

import java.util.ArrayList;
import java.util.List;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResult;
import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalog;
import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.common.Validator;

public class TelefonoValidator extends ValidationSupport implements Validator<String> {

    private static final int TELEFONO_CELULAR = 1;
	private static final int TELEFONO_LOCAL = 0;
	private static final int NUEVE_CARACTERES_LOCAL = 9;
    private static final int DIEZ_CARACTERES_CELULAR = 10;
    
	@Override
	public List<ValidationResult> validate(String telefono) {

		List<ValidationResult> listaErrores = new ArrayList<>();
		if (isNullOrEmptyString(telefono)) {
			listaErrores.add(ValidationResultCatalog.TELEFONO_VACIO_O_NULL.getValidationError());
		}

		return listaErrores;
	}

	public List<ValidationResult> validateTipoContacto(String telefono, int tipoContacto) {

		List<ValidationResult> listaErrores = new ArrayList<>();

		if (isNullOrEmptyString(telefono)) {
			listaErrores.add(ValidationResultCatalog.TELEFONO_VACIO_O_NULL.getValidationError());
		} else if (telefono.length() != NUEVE_CARACTERES_LOCAL && tipoContacto == TELEFONO_LOCAL) {
			listaErrores.add(ValidationResultCatalog.TELEFONO_MENOR_NUEVE_CARACTERES.getValidationError());
		} else if (telefono.length() != DIEZ_CARACTERES_CELULAR && tipoContacto == TELEFONO_CELULAR) {
			listaErrores.add(ValidationResultCatalog.TELEFONO_MENOR_DIEZ_CARACTERES.getValidationError());
		}

		return listaErrores;
	}

}

package ec.gob.msp.rdacaa.business.validation.malla;

import java.util.List;
import java.util.Optional;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResult;
import lombok.Getter;
import lombok.Setter;

public class RdacaaVariableValueResult {

	@Getter
	@Setter
	private Object value;

	@Getter
	@Setter
	private List<ValidationResult> validationResultList;

	public RdacaaVariableValueResult() {

	}

	public RdacaaVariableValueResult(Object value, List<ValidationResult> validationResultList) {
		this.value = value;
		this.validationResultList = validationResultList;
	}

	public static RdacaaVariableValueResult getInstance(Object value, List<ValidationResult> validationResultList) {
		return new RdacaaVariableValueResult(value, validationResultList);
	}

	public boolean hasErrors() {
		return validationResultList.parallelStream().anyMatch(ValidationResult::isError);
	}

	public Optional<ValidationResult> hasValidationResultByCode(String codeValidationResult) {
		return validationResultList.parallelStream().filter(r -> r.getCode().equals(codeValidationResult)).findFirst();
	}

	@Override
	public String toString() {
		StringBuilder resultToString = new StringBuilder();
		resultToString.append("Valor " + value).append(System.lineSeparator());
		validationResultList.forEach(r -> resultToString
				.append(" Codigo : " + r.getCode() + " Descripción : " + r.getMessage())
				.append(System.lineSeparator()));
		return resultToString.toString();
	}
}

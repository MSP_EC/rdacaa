/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.gob.msp.rdacaa.business.validation;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import ec.gob.msp.rdacaa.business.service.MedicionPacienteAlertasService;
import ec.gob.msp.rdacaa.business.service.SignosVitalesReglasValidacion;
import ec.gob.msp.rdacaa.business.validation.common.ValidationResult;
import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalog;
import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.common.Validator;
import ec.gob.msp.rdacaa.utilitario.medico.UtilitarioMedico;

/**
 *
 * @author msp
 */
@Component
public class IndiceMasaCorporalValidator extends ValidationSupport implements Validator<String[]> {

	private final Logger logger = LoggerFactory.getLogger(IndiceMasaCorporalValidator.class);

	@Autowired
	private MedicionPacienteAlertasService medicionPacienteAlertasService;
	private static final BigDecimal VALOR_MAXIMO_IMC = new BigDecimal("200.00");
	private static final BigDecimal VALOR_NORMAL_MINIMO_IMC = new BigDecimal("18.50");
	private static final BigDecimal VALOR_NORMAL_MAXIMO_IMC = new BigDecimal("24.99");
	private static final String ALERTA_ROJA = "rojo";


	@Override
	public List<ValidationResult> validate(String[] imcEdadTalla) {

		List<ValidationResult> listaErrores = new ArrayList<>();

		if (isNullOrEmptyString(imcEdadTalla[0])) {
			listaErrores.add(ValidationResultCatalog.IMC_VACIO.getValidationError());
		} else {
			String imc = imcEdadTalla[0];
			Integer edad = Integer.parseInt(imcEdadTalla[1]);
			BigDecimal peso = new BigDecimal(imcEdadTalla[2]);
			Optional<ValidationResult> warning = procesarIMCParaEdad(imc, edad, peso);
			if (warning.isPresent()) {
				listaErrores.add(warning.get());
			}
		}

		return listaErrores;
	}

	
	private Optional<ValidationResult> procesarIMCParaEdad(String imc, Integer edad, BigDecimal talla) {
		logger.debug("I.M.C {}", imc);
		try {
			SignosVitalesReglasValidacion alertaFound = medicionPacienteAlertasService.calculateClasificacionIMC(imc, edad, null);
			
			if(alertaFound != null) {
				logger.debug("minimo alerta {}", alertaFound.getValorMinimoAlerta());
				logger.debug("maximo alerta {}", alertaFound.getValorMaximoAlerta());
				ValidationResult warning = ValidationResultCatalog.IMC_ALERTA.getValidationError();
				String message = warning.getMessage();
				message = message.replace("alerta", alertaFound.getAlerta());
				Double[] pesoRecomendado = UtilitarioMedico.calcularPesoRecomendadoDadoIMCTalla(VALOR_NORMAL_MINIMO_IMC.doubleValue(),
						VALOR_NORMAL_MAXIMO_IMC.doubleValue(), talla.doubleValue());
				String sugerencia = ". PESO MÍNIMO RECOMENDADO " + pesoRecomendado[0] + " kg" + " Y PESO MÁXIMO RECOMENDADO "
						+ pesoRecomendado[1] + " kg " + "PARA LA TALLA DE " + talla +"cm";
				message = message.concat(sugerencia);
				logger.debug("alerta {}", alertaFound.getAlerta());
				warning = new ValidationResult(warning.getCode(), message);
				warning.setColor(alertaFound.getColor());
				return Optional.of(warning);
			}
		}catch (Exception e) {
			//Se controla mas adelante
		}
		
		BigDecimal imcBD = new BigDecimal(imc);
		if (imcBD.compareTo(VALOR_MAXIMO_IMC) > 0) {
			ValidationResult warning = ValidationResultCatalog.IMC_MAYOR_200_ALERTA.getValidationError();
			warning = new ValidationResult(warning.getCode(), warning.getMessage(), ALERTA_ROJA);
			return Optional.of(warning);
		}
		return Optional.empty();
	}

}

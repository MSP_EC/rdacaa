package ec.gob.msp.rdacaa.business.validation.rule.atencion.obstetricia.metodosemgestacion;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;

/**
 *
 * @author miguel.faubla
 */

@Rule(order = 3)
public class MetodoSemanagestacionIsExcludedRule extends BaseRule<String> {

	@When
	public boolean when() {
		return isExcludedFromValidation(RdacaaVariableKeyCatalog.OBSTETRICIA_CODIGO_METODO_DET_SEMANAS_GESTACION);
	}

	@Then
	public RuleState then() {

		String embarazoPlanificado = getVariableFromMap(RdacaaVariableKeyCatalog.OBSTETRICIA_CODIGO_METODO_DET_SEMANAS_GESTACION);
		if (!ValidationSupport.isNotDefined(embarazoPlanificado)) {
			addValidationResult(RdacaaVariableKeyCatalog.OBSTETRICIA_CODIGO_METODO_DET_SEMANAS_GESTACION,
					ValidationResultCatalogConstants.CODIGO_ERROR_OBSTETRICIA_CODIGO_METODO_DET_SEMANAS_GESTACION_NO_DEFINIDA);
		}

		return RuleState.BREAK;

	}
}

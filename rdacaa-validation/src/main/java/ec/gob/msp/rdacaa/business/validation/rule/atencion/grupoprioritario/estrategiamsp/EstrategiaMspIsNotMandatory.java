package ec.gob.msp.rdacaa.business.validation.rule.atencion.grupoprioritario.estrategiamsp;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;

/**
 *
 * @author miguel.faubla
 */
@Rule(order = 5)
public class EstrategiaMspIsNotMandatory extends BaseRule<String> {

	@When
	public boolean when() {
		String estrategiamsp = getVariableFromMap(RdacaaVariableKeyCatalog.ESTRATEGIA_MSP);
		return (!isMandatory(RdacaaVariableKeyCatalog.ESTRATEGIA_MSP) && ValidationSupport.isNotDefined(estrategiamsp));
	}

	@Then
	public RuleState then() {
		return RuleState.BREAK;
	}
}

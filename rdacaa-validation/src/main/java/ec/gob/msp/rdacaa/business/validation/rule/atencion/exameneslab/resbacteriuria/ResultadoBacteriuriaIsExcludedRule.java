package ec.gob.msp.rdacaa.business.validation.rule.atencion.exameneslab.resbacteriuria;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;

/**
 *
 * @author miguel.faubla
 */

@Rule(order = 3)
public class ResultadoBacteriuriaIsExcludedRule extends BaseRule<String> {

	@When
	public boolean when() {
		return isExcludedFromValidation(RdacaaVariableKeyCatalog.EXAMENES_LABORATORIO_RESULTADO_BACTERIURIA);
	}

	@Then
	public RuleState then() {

		String resultadoBacteriuria = getVariableFromMap(RdacaaVariableKeyCatalog.EXAMENES_LABORATORIO_RESULTADO_BACTERIURIA);
		if (!ValidationSupport.isNotDefined(resultadoBacteriuria)) {
			addValidationResult(RdacaaVariableKeyCatalog.EXAMENES_LABORATORIO_RESULTADO_BACTERIURIA,
					ValidationResultCatalogConstants.CODIGO_ERROR_EXAMENES_LABORATORIO_RESULTADO_BACTERIURIA_NO_DEFINIDA);
		}

		return RuleState.BREAK;

	}
}

package ec.gob.msp.rdacaa.business.validation.rule.atencion.fechaatencion;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.UtilsRule;

@Rule(order = 6)
public class FechaAtencionIsIncorrectRule extends UtilsRule {

	@When
	public boolean when() {
		String fechaAtencion = getVariableFromMap(RdacaaVariableKeyCatalog.ATENCION_FECHA);
		boolean isFechaValida = isFechaValida(fechaAtencion) && 
				getFechaFromString(fechaAtencion).after(getFechaFromString("2018-12-31"));
		return !isFechaValida;
	}

	@Then
	public RuleState then() {
		addValidationResult(RdacaaVariableKeyCatalog.ATENCION_FECHA,
				ValidationResultCatalogConstants.CODIGO_ERROR_ATENCION_FECHA_ES_INCORRECTA);
		return RuleState.BREAK;
	}
	
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.gob.msp.rdacaa.business.validation.rule.persona.telefonofamiliar;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;
import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;

/**
 *
 * @author eduardo
 */
@Rule(order = 7)
public class TelefonoFamiliarLongitudIncorrectaRule extends BaseRule<String> {
	@When
	public boolean when() {
		String telefonoFamiliar = getVariableFromMap(RdacaaVariableKeyCatalog.PERSONA_TELEFONO_FAMILIAR);
		return ValidationSupport.isStringLessThanXCharacters(telefonoFamiliar, 9)
				|| ValidationSupport.isStringMoreThanXCharacters(telefonoFamiliar, 10);
	}

	@Then
	public RuleState then() {
		addValidationResult(RdacaaVariableKeyCatalog.PERSONA_TELEFONO_FAMILIAR,
				ValidationResultCatalogConstants.CODIGO_ERROR_TELEFONO_FAMILIAR_LONGITUD_INCORRECTA);
		return RuleState.BREAK;
	}
}

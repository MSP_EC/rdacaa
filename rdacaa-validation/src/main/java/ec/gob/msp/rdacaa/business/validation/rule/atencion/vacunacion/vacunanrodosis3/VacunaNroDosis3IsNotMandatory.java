package ec.gob.msp.rdacaa.business.validation.rule.atencion.vacunacion.vacunanrodosis3;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;

/**
 *
 * @author miguel.faubla
 */
@Rule(order = 5)
public class VacunaNroDosis3IsNotMandatory extends BaseRule<String> {

	@When
	public boolean when() {
		String vacunaNroDosis3 = getVariableFromMap(RdacaaVariableKeyCatalog.VACUNAS_CODIGO_DOSIS_3);
		return (!isMandatory(RdacaaVariableKeyCatalog.VACUNAS_CODIGO_DOSIS_3)
				&& ValidationSupport.isNotDefined(vacunaNroDosis3));
	}

	@Then
	public RuleState then() {
		return RuleState.BREAK;
	}
}

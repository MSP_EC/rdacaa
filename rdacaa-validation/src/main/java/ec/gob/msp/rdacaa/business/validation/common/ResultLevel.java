package ec.gob.msp.rdacaa.business.validation.common;

public enum ResultLevel {

	INF, ADV, ERR;
	
	public static ResultLevel getLevel(String key) {
    	switch (key) {
		case "INF":
			return ResultLevel.INF;
		case "ADV":
			return ResultLevel.ADV;
		case "ERR":
			return ResultLevel.ERR;
		default:
			return ResultLevel.ERR;
		}
	}
}

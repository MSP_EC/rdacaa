package ec.gob.msp.rdacaa.business.validation.rule.atencion.coddpaestparroquia;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;

/**
*
* @author dmurillo
*/

@Rule(order = 3)
public class CodDPAEstablecimientoParroquiaIsExcludedRule extends BaseRule<String> {

	@When
	public boolean when() {
		return isExcludedFromValidation(RdacaaVariableKeyCatalog.ATENCION_UBICACION_PARROQUIA);
	}

	@Then
	public RuleState then() {
		
		String parroquia = getVariableFromMap(RdacaaVariableKeyCatalog.ATENCION_UBICACION_PARROQUIA);
		if(!ValidationSupport.isNotDefined(parroquia)){
			addValidationResult(RdacaaVariableKeyCatalog.ATENCION_UBICACION_PARROQUIA,
					ValidationResultCatalogConstants.CODIGO_ERROR_ATENCION_UBICACION_PARROQUIA_NO_DEFINIDA);
		}
		
		return RuleState.BREAK;

	}
}

package ec.gob.msp.rdacaa.business.validation.rule.atencion.datosantropometricos.imcclasificacion;

import java.math.BigDecimal;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Given;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.service.MedicionPacienteAlertasService;
import ec.gob.msp.rdacaa.business.service.SignosVitalesReglasValidacion;
import ec.gob.msp.rdacaa.business.validation.common.ValidationResult;
import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.UtilsRule;
import ec.gob.msp.rdacaa.utilitario.medico.UtilitarioMedico;

@Rule(order = 6)
public class IMCClasificacionCalculateRule extends UtilsRule {

	private static final BigDecimal VALOR_NORMAL_MINIMO_IMC = new BigDecimal("18.50");
	private static final BigDecimal VALOR_NORMAL_MAXIMO_IMC = new BigDecimal("24.99");

	@Given("medicionPacienteAlertasService")
	private MedicionPacienteAlertasService medicionPacienteAlertasService;

	@When
	public boolean when() {
		return true;
	}

	@Then
	public RuleState then() {
		String imcClasificacion = "999990000066666";
		String imc = getRdacaaRawRowResultValue(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_INDICE_MASA_CORPORAL);
		String tallaCorregida = getRdacaaRawRowResultValue(
				RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_TALLA_CORREGIDA);
		Integer edadAnioMesDias = getEdadAnioMesDias();
		boolean imcHasErrors = variableHasErrors(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_INDICE_MASA_CORPORAL);
		boolean tallaCorregidaHasErrors = variableHasErrors(
				RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_TALLA_CORREGIDA);
		
		boolean valuesNotDefined = ValidationSupport.isNotDefined(imc);

		setRdacaaRawRowResultValue(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_IMC_CLASIFICACION, imcClasificacion);

		if (edadAnioMesDias == null) {
			addValidationResult(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_IMC_CLASIFICACION,
					ValidationResultCatalogConstants.CODIGO_ERROR_DAT_ANT_IMC_CLASIFICACION_FECHAS_FORMATO_INCORRECTO);
		}

		if (imcHasErrors) {
			addValidationResult(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_IMC_CLASIFICACION,
					ValidationResultCatalogConstants.CODIGO_ERROR_DAT_ANT_IMC_CLASIFICACION_IMC_ERROR);
		}

		if (tallaCorregidaHasErrors) {
			addValidationResult(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_IMC_CLASIFICACION,
					ValidationResultCatalogConstants.CODIGO_ERROR_DAT_ANT_IMC_CLASIFICACION_TALLA_CORREGIDA_ERRORES);
		}
		
		if(valuesNotDefined) {
			addValidationResult(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_IMC_CLASIFICACION,
					ValidationResultCatalogConstants.CODIGO_INFO_DAT_ANT_IMC_CLASIFICACION_IMC_NO_DEFINIDO);
		}

		if (imcHasErrors || tallaCorregidaHasErrors || edadAnioMesDias == null || valuesNotDefined) {
			return RuleState.BREAK;
		}

		procesarIMCParaEdad(imc, edadAnioMesDias, tallaCorregida);

		return RuleState.NEXT;
	}

	private void procesarIMCParaEdad(String imc, Integer edad, String tallaCorregidaString) {
		try {
			BigDecimal tallaCorregida = new BigDecimal(tallaCorregidaString);

			SignosVitalesReglasValidacion alertaFound = medicionPacienteAlertasService.calculateClasificacionIMC(imc,
					edad, null);

			if (alertaFound != null) {

				ValidationResult warning = getValidationResultFromMap(
						ValidationResultCatalogConstants.CODIGO_INFO_DAT_ANT_IMC_CLASIFICACION_VALOR);
				String message = warning.getMessage();
				message = message + "\"" + alertaFound.getAlerta() + "\"";
				Double[] pesoRecomendado = UtilitarioMedico.calcularPesoRecomendadoDadoIMCTalla(
						VALOR_NORMAL_MINIMO_IMC.doubleValue(), VALOR_NORMAL_MAXIMO_IMC.doubleValue(),
						tallaCorregida.doubleValue());
				String sugerencia = ". PESO MÍNIMO RECOMENDADO " + pesoRecomendado[0] + " kg"
						+ " Y PESO MÁXIMO RECOMENDADO " + pesoRecomendado[1] + " kg " + "PARA LA TALLA DE "
						+ tallaCorregida + "cm";
				message = message.concat(sugerencia);
				warning.setMessage(message);
				setRdacaaRawRowResultValue(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_IMC_CLASIFICACION,
						alertaFound.getIdSignoVitalAlerta().toString());
				addValidationResult(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_IMC_CLASIFICACION, warning);
			}
		} catch (Exception e) {
			// Se controla mas adelante
		}
	}
}

package ec.gob.msp.rdacaa.business.validation.rule.atencion.grupovulnerable.gpr_vulnerable4;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;

@Rule(order = 2)
public class GrupoVulnerable4IsNullRule extends BaseRule<String>{

	@When
	public boolean when() {
		String grupoVulnerable4 = getVariableFromMap(RdacaaVariableKeyCatalog.GRUPO_VULNERABLE_4);
		return ValidationSupport.isNullOrEmptyString(grupoVulnerable4);
	}

	@Then
	public RuleState then() {
		addValidationResult(RdacaaVariableKeyCatalog.GRUPO_VULNERABLE_4,
				ValidationResultCatalogConstants.CODIGO_ERROR_GRUPO_VULNERABLE_4_NULO);
		return RuleState.BREAK;
	}
}

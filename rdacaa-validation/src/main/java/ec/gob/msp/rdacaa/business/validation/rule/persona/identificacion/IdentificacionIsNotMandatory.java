package ec.gob.msp.rdacaa.business.validation.rule.persona.identificacion;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;

/**
*
* @author dmurillo
*/
@Rule(order = 5)
public class IdentificacionIsNotMandatory extends BaseRule<String> {

	@When
	public boolean when() {
		
		String identificacion = getVariableFromMap(RdacaaVariableKeyCatalog.PERSONA_IDENTIFICACION);
		return (!isMandatory(RdacaaVariableKeyCatalog.PERSONA_IDENTIFICACION) &&
				ValidationSupport.isNotDefined(identificacion));
		
	}

	@Then
	public RuleState then() {
		return RuleState.BREAK;
	}
}

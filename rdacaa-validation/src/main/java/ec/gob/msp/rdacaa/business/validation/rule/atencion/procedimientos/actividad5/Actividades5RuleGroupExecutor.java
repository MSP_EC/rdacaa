package ec.gob.msp.rdacaa.business.validation.rule.atencion.procedimientos.actividad5;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.deliveredtechnologies.rulebook.model.RuleBook;

import ec.gob.msp.rdacaa.business.validation.malla.RdacaaRawRowResult;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.RdacaaRuleExecutor;

@Component
public class Actividades5RuleGroupExecutor extends RdacaaRuleExecutor {
	@Autowired
	public Actividades5RuleGroupExecutor(
			@Qualifier("ReglasActividadesProcedimientos5") RuleBook<RdacaaRawRowResult> ruleBook) {
		super();
		this.ruleBook = ruleBook;
		this.variableKey = RdacaaVariableKeyCatalog.PROCEDIMIENTOS_CANTIDAD_5;
	}
}

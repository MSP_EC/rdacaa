package ec.gob.msp.rdacaa.business.validation.rule.atencion.vacunacion.vacunalote2;

import org.apache.commons.lang3.StringUtils;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;

/**
 *
 * @author miguel.faubla
 */
@Rule(order = 6)
public class VacunaLote2IsIncorrect extends BaseRule<String> {

	@When
	public boolean when() {
		String vacunaLote2 = getVariableFromMap(RdacaaVariableKeyCatalog.VACUNAS_CODIGO_LOTE_2);
		return !StringUtils.isAlphanumeric(vacunaLote2) || ValidationSupport.isStringMoreThanXCharacters(vacunaLote2, 20) ;
	}

	@Then
	public RuleState then() {
		addValidationResult(RdacaaVariableKeyCatalog.VACUNAS_CODIGO_LOTE_2,
				ValidationResultCatalogConstants.CODIGO_ERROR_VACUNAS_CODIGO_LOTE_2_ES_INCORRECTA);
		return RuleState.BREAK;
	}
}

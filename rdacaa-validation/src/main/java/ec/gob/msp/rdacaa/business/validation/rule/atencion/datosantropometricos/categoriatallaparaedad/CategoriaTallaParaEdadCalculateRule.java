package ec.gob.msp.rdacaa.business.validation.rule.atencion.datosantropometricos.categoriatallaparaedad;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Given;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.service.ScoreZAndCategoriaService;
import ec.gob.msp.rdacaa.business.service.SignosVitalesReglasValidacion;
import ec.gob.msp.rdacaa.business.service.SignosVitalesReglasValidacionNotFoundException;
import ec.gob.msp.rdacaa.business.service.ScoreZAndCategoriaService.PercentilDetalleNotFoundException;
import ec.gob.msp.rdacaa.business.service.ScoreZAndCategoriaService.PercentilNotFoundException;
import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.UtilsRule;

@Rule(order = 6)
public class CategoriaTallaParaEdadCalculateRule extends UtilsRule {

	@Given("scoreZAndCategoriaService")
	private ScoreZAndCategoriaService scoreZAndCategoriaService;

	@When
	public boolean when() {
		return true;
	}

	@Then
	public RuleState then() {
		String categoriaTallaParaEdad = "999990000066666";
		String puntajeZTallaParaEdad = getRdacaaRawRowResultValue(
				RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_PUNTAJE_Z_TALLA_PARA_EDAD);
		boolean puntajeZTallaParaEdadHasErrors = variableHasErrors(
				RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_PUNTAJE_Z_TALLA_PARA_EDAD);
		Integer edadAnioMesDias = getEdadAnioMesDias();
		String sexo = getSexoString();

		boolean valuesNotDefined = ValidationSupport.isNotDefined(puntajeZTallaParaEdad);

		setRdacaaRawRowResultValue(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_CATEGORIA_TALLA_PARA_EDAD,
				categoriaTallaParaEdad);

		if (edadAnioMesDias == null) {
			addValidationResult(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_CATEGORIA_TALLA_PARA_EDAD,
					ValidationResultCatalogConstants.CODIGO_ERROR_DAT_ANT_CATEGORIA_TALLA_EDAD_FECHAS_FORMATO_INCORRECTO);
		}

		if (sexo == null) {
			addValidationResult(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_CATEGORIA_TALLA_PARA_EDAD,
					ValidationResultCatalogConstants.CODIGO_ERROR_DAT_ANT_CATEGORIA_TALLA_EDAD_SEXO_INCORRECTO);
		}

		if (puntajeZTallaParaEdadHasErrors) {
			addValidationResult(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_CATEGORIA_TALLA_PARA_EDAD,
					ValidationResultCatalogConstants.CODIGO_ERROR_DAT_ANT_CATEGORIA_TALLA_EDAD_PZ_TALLA_PARA_EDAD_ERROR);
		}

		if (valuesNotDefined) {
			addValidationResult(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_CATEGORIA_TALLA_PARA_EDAD,
					ValidationResultCatalogConstants.CODIGO_INFO_DAT_ANT_CATEGORIA_TALLA_EDAD_PZ_TALLA_PARA_EDAD_NO_DEFINIDO);
		}

		if (edadAnioMesDias == null || sexo == null || puntajeZTallaParaEdadHasErrors || valuesNotDefined) {
			return RuleState.BREAK;
		}

		try {
			SignosVitalesReglasValidacion categoriaTallaParaEdadAlerta = scoreZAndCategoriaService
					.calcularCategoriaTallaParaEdad(Double.parseDouble(puntajeZTallaParaEdad), edadAnioMesDias, sexo);
			categoriaTallaParaEdad = categoriaTallaParaEdadAlerta.getIdSignoVitalAlerta().toString();
		} catch (PercentilNotFoundException | PercentilDetalleNotFoundException
				| SignosVitalesReglasValidacionNotFoundException e) {
			addValidationResult(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_CATEGORIA_TALLA_PARA_EDAD,
					ValidationResultCatalogConstants.CODIGO_INFO_DAT_ANT_CATEGORIA_TALLA_EDAD_NO_APLICA);

			return RuleState.BREAK;
		}
		
		setRdacaaRawRowResultValue(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_CATEGORIA_TALLA_PARA_EDAD,
				categoriaTallaParaEdad);

		addValidationResult(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_CATEGORIA_TALLA_PARA_EDAD,
				ValidationResultCatalogConstants.CODIGO_INFO_DAT_ANT_CATEGORIA_TALLA_EDAD_VALOR);

		return RuleState.NEXT;

	}
}

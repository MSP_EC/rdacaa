package ec.gob.msp.rdacaa.business.validation.rule.atencion.violencia.parentescoagresor;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;

@Rule(order = 3)
public class ParentescoAgresorIsExcludedRule extends BaseRule<String> {

	@When
	public boolean when() {
		return isExcludedFromValidation(RdacaaVariableKeyCatalog.VIOLENCIA_PARENTESCO_AGRESOR);
	}
	
	@Then
	public RuleState then() {
		
		String parentescoAgresor = getVariableFromMap(RdacaaVariableKeyCatalog.VIOLENCIA_PARENTESCO_AGRESOR);
		if(!ValidationSupport.isNotDefined(parentescoAgresor)){
			addValidationResult(RdacaaVariableKeyCatalog.VIOLENCIA_PARENTESCO_AGRESOR,
					ValidationResultCatalogConstants.CODIGO_ERROR_VIOLENCIA_PARENTESCO_AGRESOR_NO_DEFINIDO);
		}
		
		return RuleState.BREAK;

	}
}

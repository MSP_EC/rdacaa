package ec.gob.msp.rdacaa.business.validation.rule.atencion.procedimientos.actividad1;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;

@Rule(order = 7)
public class Actividades1LongitudIncorrectaRule extends BaseRule<String> {

	@When
	public boolean when() {
		String actividades1 = getVariableFromMap(
				RdacaaVariableKeyCatalog.PROCEDIMIENTOS_CANTIDAD_1);
		return ValidationSupport.isStringMoreThanXCharacters(actividades1, 2);
	}

	@Then
	public RuleState then() {
		addValidationResult(RdacaaVariableKeyCatalog.PROCEDIMIENTOS_CANTIDAD_1,
				ValidationResultCatalogConstants.CODIGO_ERROR_ACTIVIDAD_1_LONGITUD_INCORRECTA);
		return RuleState.BREAK;
	}
}

package ec.gob.msp.rdacaa.business.validation.rule.atencion.datosantropometricos.valorhb;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;

/**
*
* @author dmurillo
*/
@Rule(order = 5)
public class ValorHBIsNotMandatory extends BaseRule<String> {

	@When
	public boolean when() {
		
		String valorHB = getVariableFromMap(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_VALOR_HB);
		return (!isMandatory(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_VALOR_HB) &&
				ValidationSupport.isNotDefined(valorHB));
		
	}

	@Then
	public RuleState then() {
		return RuleState.NEXT;
	}
}

package ec.gob.msp.rdacaa.business.validation.rule.atencion.profesional;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;

/**
 *
 * @author miguel.faubla
 */
@Rule(order = 6)
public class ProfesionalIsIncorrect extends BaseRule<String> {

	@When
	public boolean when() {
		return true;
	}

	@Then
	public RuleState then() {
		String codProfesional = getVariableFromMap(RdacaaVariableKeyCatalog.PROFESIONAL_CODIGO);

		Integer codProfesionalInt = null;

		try {
			codProfesionalInt = Integer.parseInt(codProfesional);
		} catch (Exception e) {
			codProfesionalInt = null;
			addValidationResult(RdacaaVariableKeyCatalog.PROFESIONAL_CODIGO,
					ValidationResultCatalogConstants.CODIGO_ERROR_PROFESIONAL_CODIGO_NO_NUMERICO);
		}

		if (codProfesionalInt != null) {
			return RuleState.NEXT;
		} else {
			return RuleState.BREAK;
		}

	}
}

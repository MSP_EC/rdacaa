package ec.gob.msp.rdacaa.business.validation.rule.atencion.vacunacion.vacunalote2;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;

/**
 *
 * @author miguel.faubla
 */

@Rule(order = 3)
public class VacunaLote2IsExcludedRule extends BaseRule<String> {

	@When
	public boolean when() {
		return isExcludedFromValidation(RdacaaVariableKeyCatalog.VACUNAS_CODIGO_LOTE_2);
	}

	@Then
	public RuleState then() {

		String vacunaLote2 = getVariableFromMap(RdacaaVariableKeyCatalog.VACUNAS_CODIGO_LOTE_2);
		if (!ValidationSupport.isNotDefined(vacunaLote2)) {
			addValidationResult(RdacaaVariableKeyCatalog.VACUNAS_CODIGO_LOTE_2,
					ValidationResultCatalogConstants.CODIGO_ERROR_VACUNAS_CODIGO_LOTE_2_NO_DEFINIDA);
		}

		return RuleState.BREAK;

	}
}

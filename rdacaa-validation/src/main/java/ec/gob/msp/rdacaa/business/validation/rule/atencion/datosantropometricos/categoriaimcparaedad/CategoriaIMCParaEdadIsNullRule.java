package ec.gob.msp.rdacaa.business.validation.rule.atencion.datosantropometricos.categoriaimcparaedad;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;

@Rule(order = 2)
public class CategoriaIMCParaEdadIsNullRule extends BaseRule<String> {

	@When
	public boolean when() {
		String imcParaEdad = getVariableFromMap(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_CATEGORIA_IMC_PARA_EDAD);
		return ValidationSupport.isNullOrEmptyString(imcParaEdad);
	}

	@Then
	public RuleState then() {
		addValidationResult(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_CATEGORIA_IMC_PARA_EDAD,
				ValidationResultCatalogConstants.CODIGO_ERROR_DAT_ANT_CATEGORIA_IMC_EDAD_NULO);
		return RuleState.BREAK;
	}
}

package ec.gob.msp.rdacaa.business.validation.rule.atencion.datosantropometricos.valorhbindicadoranemia;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;

/**
*
* @author dmurillo
*/
@Rule(order = 4)
public class ValorHBIndicadorAnemiaIsMandatoryRule extends BaseRule<String> {
	@When
	public boolean when() {
		return true;

	}

	@Then
	public RuleState then() {

		String indicadorAnemia = getVariableFromMap(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_CODIGO_INDICADOR_ANEMIA);
		if (isMandatory(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_CODIGO_INDICADOR_ANEMIA)
				&& ValidationSupport.isNotDefined(indicadorAnemia)) {
			addValidationResult(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_CODIGO_INDICADOR_ANEMIA,
					ValidationResultCatalogConstants.CODIGO_ERROR_DAT_ANT_CODIGO_INDICADOR_ANEMIA_ES_MANDATORIA);
			return RuleState.BREAK;
		} else {
			return RuleState.NEXT;
		}
	}
}

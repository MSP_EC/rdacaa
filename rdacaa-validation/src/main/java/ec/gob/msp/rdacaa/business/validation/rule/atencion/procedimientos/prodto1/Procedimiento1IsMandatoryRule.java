package ec.gob.msp.rdacaa.business.validation.rule.atencion.procedimientos.prodto1;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;

@Rule(order = 4)
public class Procedimiento1IsMandatoryRule extends BaseRule<String> {

	@When
	public boolean when() {
		return true;

	}
	
	@Then
	public RuleState then() {
		String procedimiento1 = getVariableFromMap(RdacaaVariableKeyCatalog.PROCEDIMIENTOS_CODIGO_1);
		if (isMandatory(RdacaaVariableKeyCatalog.PROCEDIMIENTOS_CODIGO_1)
			&& ValidationSupport.isNotDefined(procedimiento1)) {
			addValidationResult(RdacaaVariableKeyCatalog.PROCEDIMIENTOS_CODIGO_1,
					ValidationResultCatalogConstants.CODIGO_ERROR_PROCEDIMIENTO_1_ES_MANDATORIO);
			return RuleState.BREAK;
		} else if (!isMandatory(RdacaaVariableKeyCatalog.PROCEDIMIENTOS_CODIGO_1)&& 
				ValidationSupport.isNotDefined(procedimiento1)) {
			return RuleState.BREAK;
		} else {
			return RuleState.NEXT;
		}

	}

}

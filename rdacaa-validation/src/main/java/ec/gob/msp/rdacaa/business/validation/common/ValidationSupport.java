package ec.gob.msp.rdacaa.business.validation.common;

import com.google.common.base.Strings;

import ec.gob.msp.rdacaa.business.validation.usuario.UsuarioValidation;

public class ValidationSupport {
	
    public static final String NOT_DEFINED_VALUE = "999990000066666";

    public static boolean isNullOrEmptyString(String value) {
        return Strings.isNullOrEmpty(value);
    }

    public static  boolean isNullValue(Object value) {
        return value == null;
    }

    public static  boolean isValueGreaterThanZero(long value) {
        return value > 0;
    }

    public static  boolean isValueGreaterThanZero(double value) {
        return value > 0;
    }
    
    public static  boolean isStringLessThanXCharacters(String value, int lenght) {
        return Strings.nullToEmpty(value).length()<lenght;
    }

    public static  boolean isStringMoreThanXCharacters(String value, int lenght) {
        return Strings.nullToEmpty(value).length()>lenght;
    }
    
    public static  boolean isCedulaValida(String value) {
        return UsuarioValidation.isIdentificacionValida(value);
    }
    
    public static  boolean isNotDefined(String value){
    	return value.equals(NOT_DEFINED_VALUE); 
    }
}

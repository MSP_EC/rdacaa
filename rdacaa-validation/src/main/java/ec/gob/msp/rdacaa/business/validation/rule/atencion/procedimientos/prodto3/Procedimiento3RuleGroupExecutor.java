package ec.gob.msp.rdacaa.business.validation.rule.atencion.procedimientos.prodto3;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.deliveredtechnologies.rulebook.model.RuleBook;

import ec.gob.msp.rdacaa.business.validation.malla.RdacaaRawRowResult;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.RdacaaRuleExecutor;
@Component
public class Procedimiento3RuleGroupExecutor extends RdacaaRuleExecutor {


	@Autowired
	public Procedimiento3RuleGroupExecutor(@Qualifier("ReglasProcedimiento3") RuleBook<RdacaaRawRowResult> ruleBook) {
		super();
		this.ruleBook = ruleBook;
		this.variableKey = RdacaaVariableKeyCatalog.PROCEDIMIENTOS_CODIGO_1;
	}
}

package ec.gob.msp.rdacaa.business.validation.rule.vih.pruebados;

import com.deliveredtechnologies.rulebook.RuleState;
import org.apache.commons.lang3.StringUtils;

import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;

/**
*
* @author dmurillo
*/
@Rule(order = 6)
public class PruebaDosIsIncorrectoRule extends BaseRule<String> {

	@When
	public boolean when() {
            return true;
//		String prueba = getVariableFromMap(RdacaaVariableKeyCatalog.VIH_CODIGO_SEGUNDA_PRUEBA);
//		if(StringUtils.isNumeric(prueba)){
//			return !this.validationQueryService.isCodigoVihPruebaValido(Integer.valueOf(prueba));
//		}
//		return true;
	}

	@Then
	public RuleState then() {
            
            boolean resultadoPruebaUnoHasErrors = variableHasErrors(RdacaaVariableKeyCatalog.VIH_VALOR_RESULTADO_PRIMERA_PRUEBA);
            String resultadoPruebaUnoValue = getRdacaaRawRowResultValue(RdacaaVariableKeyCatalog.VIH_VALOR_RESULTADO_PRIMERA_PRUEBA);
            
            if(resultadoPruebaUnoHasErrors){
                addValidationResult(RdacaaVariableKeyCatalog.VIH_CODIGO_SEGUNDA_PRUEBA,
                        ValidationResultCatalogConstants.CODIGO_ERROR_VIH_CODIGO_PRUEBA_DOS_REQUIERE_MOTIVO_SIN_ERROR);
                return RuleState.BREAK;
            }
            
            if(ValidationSupport.isNotDefined(resultadoPruebaUnoValue)){
                addValidationResult(RdacaaVariableKeyCatalog.VIH_CODIGO_SEGUNDA_PRUEBA,
                        ValidationResultCatalogConstants.CODIGO_ERROR_VIH_CODIGO_PRUEBA_DOS_REQUIERE_MOTIVO_PRUEBA);
                return RuleState.BREAK;
            }
            
            String prueba = getVariableFromMap(RdacaaVariableKeyCatalog.VIH_CODIGO_SEGUNDA_PRUEBA);
            boolean pruebaValida = false;
            if(StringUtils.isNumeric(prueba)){
                pruebaValida = this.validationQueryService.isCodigoVihPruebaValido(Integer.valueOf(prueba));
            }
            
            if (!pruebaValida){
                addValidationResult(RdacaaVariableKeyCatalog.VIH_CODIGO_SEGUNDA_PRUEBA, 
                        ValidationResultCatalogConstants.CODIGO_ERROR_VIH_CODIGO_PRUEBA_DOS_INCORRECTO);
            }
            
//		addValidationResult(RdacaaVariableKeyCatalog.VIH_CODIGO_SEGUNDA_PRUEBA,
//				ValidationResultCatalogConstants.CODIGO_ERROR_VIH_CODIGO_PRUEBA_DOS_INCORRECTO);

        return RuleState.NEXT;
	}
}

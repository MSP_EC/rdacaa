/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.gob.msp.rdacaa.business.validation.rule.atencion.violencia.identificaagresor;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;
import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;

/**
 *
 * @author eduardo
 */

@Rule(order = 3)
public class IdentificaAgresorIsExcludedRule extends BaseRule<String> {
    
    @When
	public boolean when() {
		return isExcludedFromValidation(RdacaaVariableKeyCatalog.VIOLENCIA_IDENTIFICA_AGRESOR);
	}

	@Then
	public RuleState then() {
		
		String identificaAgresor = getVariableFromMap(RdacaaVariableKeyCatalog.VIOLENCIA_IDENTIFICA_AGRESOR);
		if(!ValidationSupport.isNotDefined(identificaAgresor)){
			addValidationResult(RdacaaVariableKeyCatalog.VIOLENCIA_IDENTIFICA_AGRESOR,
					ValidationResultCatalogConstants.CODIGO_ERROR_VIOLENCIA_IDENTIFICA_AGRESOR_NO_DEFINIDA);
		}
		
		return RuleState.BREAK;

	}
}

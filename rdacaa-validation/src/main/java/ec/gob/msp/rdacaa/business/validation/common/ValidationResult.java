package ec.gob.msp.rdacaa.business.validation.common;

import lombok.Getter;
import lombok.Setter;

public class ValidationResult {

    @Getter
    private String code;
    @Getter
    @Setter
    private String message;
    @Getter
    @Setter
    private String color;
	@Getter
	@Setter
	private ResultLevel nivel;

	public ValidationResult(String code, String message) {
		this(code, message, null);
	}

	public ValidationResult(String code, String message, String color) {
		this(code, message, color, null);
	}

	public ValidationResult(String code, String message, String color, ResultLevel nivel) {
		this.code = code;
		this.message = message;
		this.color = color;
		this.nivel = nivel;
	}
	
	public boolean isError() {
		return nivel.equals(ResultLevel.ERR);
	}
	
	public boolean isWarning() {
		return nivel.equals(ResultLevel.ADV);
	}
	
	public boolean isInfo() {
		return nivel.equals(ResultLevel.INF);
	}

}

package ec.gob.msp.rdacaa.business.validation.rule.persona.segundoapellido;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.deliveredtechnologies.rulebook.model.RuleBook;

import ec.gob.msp.rdacaa.business.validation.malla.RdacaaRawRowResult;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.RdacaaRuleExecutor;

@Component
public class SegundoApellidoRuleGroupExecutor extends RdacaaRuleExecutor {

	@Autowired
	public SegundoApellidoRuleGroupExecutor(@Qualifier("ReglasSegundoApellido") RuleBook<RdacaaRawRowResult> ruleBook) {
		super();
		this.ruleBook = ruleBook;
		this.variableKey = RdacaaVariableKeyCatalog.PERSONA_SEGUNDO_APELLIDO;
	}

}

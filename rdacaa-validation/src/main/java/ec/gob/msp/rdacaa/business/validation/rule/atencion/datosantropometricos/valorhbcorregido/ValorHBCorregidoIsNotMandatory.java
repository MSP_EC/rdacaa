package ec.gob.msp.rdacaa.business.validation.rule.atencion.datosantropometricos.valorhbcorregido;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;

/**
*
* @author dmurillo
*/
@Rule(order = 5)
public class ValorHBCorregidoIsNotMandatory extends BaseRule<String> {

	@When
	public boolean when() {
		
		String valorHBCorregido = getVariableFromMap(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_VALOR_HB_CORREGIDO);
		return (!isMandatory(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_VALOR_HB_CORREGIDO) &&
				ValidationSupport.isNotDefined(valorHBCorregido));
		
	}

	@Then
	public RuleState then() {
		return RuleState.NEXT;
	}
}

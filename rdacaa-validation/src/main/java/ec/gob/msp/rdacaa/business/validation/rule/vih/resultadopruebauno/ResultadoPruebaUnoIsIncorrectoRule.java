package ec.gob.msp.rdacaa.business.validation.rule.vih.resultadopruebauno;

import com.deliveredtechnologies.rulebook.RuleState;
import org.apache.commons.lang3.StringUtils;

import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;

/**
*
* @author dmurillo
*/
@Rule(order = 6)
public class ResultadoPruebaUnoIsIncorrectoRule extends BaseRule<String> {
	@When
	public boolean when() {
		
            return true;
//		Boolean valor = false;
//		if(!variableHasErrors(RdacaaVariableKeyCatalog.VIH_CODIGO_PRIMERA_PRUEBA)){
//			String prueba = getVariableFromMap(RdacaaVariableKeyCatalog.VIH_VALOR_RESULTADO_PRIMERA_PRUEBA);
//			if(StringUtils.isNumeric(prueba)){
//				if(!(Integer.valueOf(prueba) == 0 || Integer.valueOf(prueba) == 1)){
//					valor = true;
//				}
//			}
//		}
//		return valor;
	}

	@Then
	public RuleState then() {
            
            boolean resultadoPruebaUnoErrors = variableHasErrors(RdacaaVariableKeyCatalog.VIH_CODIGO_PRIMERA_PRUEBA);
            String resultadoPruebaValue = getRdacaaRawRowResultValue(RdacaaVariableKeyCatalog.VIH_CODIGO_PRIMERA_PRUEBA);
            
            if (resultadoPruebaUnoErrors){
                addValidationResult(RdacaaVariableKeyCatalog.VIH_VALOR_RESULTADO_PRIMERA_PRUEBA,
                        ValidationResultCatalogConstants.CODIGO_ERROR_VIH_RESULTADO_PRUEBA_REQUIERE_PRUEBA_UNO_SIN_ERROR);
                return RuleState.BREAK;
            }
            
            if (ValidationSupport.isNotDefined(resultadoPruebaValue)){
                addValidationResult(RdacaaVariableKeyCatalog.VIH_VALOR_RESULTADO_PRIMERA_PRUEBA,
                        ValidationResultCatalogConstants.CODIGO_ERROR_VIH_RESULTADO_PRUEBA_REQUIERE_PRUEBA_UNO_PRUEBA);
                return RuleState.BREAK;
            }
            
            String resultadoPrueba = getVariableFromMap(RdacaaVariableKeyCatalog.VIH_VALOR_RESULTADO_PRIMERA_PRUEBA);
            boolean resultadoPruebaValida = false;
            if(StringUtils.isNumeric(resultadoPrueba)){
                if((Integer.valueOf(resultadoPrueba) == 0 || Integer.valueOf(resultadoPrueba) == 1)){
					resultadoPruebaValida = true;
				}
            }
            
            if (!resultadoPruebaValida){
                addValidationResult(RdacaaVariableKeyCatalog.VIH_VALOR_RESULTADO_PRIMERA_PRUEBA,
				ValidationResultCatalogConstants.CODIGO_ERROR_VIH_RESULTADO_PRUEBA_UNO_INCORRECTO);
            }

            
//		addValidationResult(RdacaaVariableKeyCatalog.VIH_VALOR_RESULTADO_PRIMERA_PRUEBA,
//				ValidationResultCatalogConstants.CODIGO_ERROR_VIH_RESULTADO_PRUEBA_UNO_INCORRECTO);

            return RuleState.NEXT; 
	}
}

package ec.gob.msp.rdacaa.business.validation.database.export;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ec.gob.msp.rdacaa.business.validation.database.common.DecipheringLocalCipheredDataFunction;
import ec.gob.msp.rdacaa.business.validation.database.common.MapperFunction;
import ec.gob.msp.rdacaa.business.validation.database.common.PreparingResultsFunction;
import ec.gob.msp.rdacaa.business.validation.database.common.ValidatorFunction;
import ec.gob.msp.rdacaa.utilitario.exportdatabase.CipheredDataParams;
import ec.gob.msp.rdacaa.utilitario.exportdatabase.SQLiteUtil;

@Service
public class ExportValidatedDatabase {

	private DecipheringLocalCipheredDataFunction decipherFunction = new DecipheringLocalCipheredDataFunction();

	@Autowired
	private MapperFunction mapperFunction;

	@Autowired
	private ValidatorFunction validatorFunction;

	@Autowired
	private PreparingResultsFunction preparingResultsFunction;

	public void exportValidatedFile(String pathFileOrigen, String pathDirArchivoAValidar, byte[] cipheredKey,
			String username) throws IOException {
		Object[] credentials = { cipheredKey, username };

		CipheredDataParams cipheredDataParams = new CipheredDataParams(credentials, true, false, null);
		
		SQLiteUtil.exportAfterValidationMeshCipheredFile(pathDirArchivoAValidar, decipherFunction, mapperFunction,
				validatorFunction, preparingResultsFunction, cipheredDataParams);
	}

}

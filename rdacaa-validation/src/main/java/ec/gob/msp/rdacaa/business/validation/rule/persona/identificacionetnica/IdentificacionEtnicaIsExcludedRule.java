package ec.gob.msp.rdacaa.business.validation.rule.persona.identificacionetnica;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;

/**
*
* @author dmurillo
*/

@Rule(order = 3)
public class IdentificacionEtnicaIsExcludedRule extends BaseRule<String> {
	
	@When
	public boolean when() {
		return isExcludedFromValidation(RdacaaVariableKeyCatalog.PERSONA_CODIGO_AUTOIDENTIFICACION_ETNICA);
	}

	@Then
	public RuleState then() {
		
		String identificacionEtnica = getVariableFromMap(RdacaaVariableKeyCatalog.PERSONA_CODIGO_AUTOIDENTIFICACION_ETNICA);
		if(!ValidationSupport.isNotDefined(identificacionEtnica)){
			addValidationResult(RdacaaVariableKeyCatalog.PERSONA_CODIGO_AUTOIDENTIFICACION_ETNICA,
					ValidationResultCatalogConstants.CODIGO_ERROR_IDENTIFICACION_ETNICA_NO_DEFINIDA);
		}
		
		return RuleState.BREAK;

	}
}

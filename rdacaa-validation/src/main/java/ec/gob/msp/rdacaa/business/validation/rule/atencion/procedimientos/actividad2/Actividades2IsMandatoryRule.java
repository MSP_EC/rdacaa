package ec.gob.msp.rdacaa.business.validation.rule.atencion.procedimientos.actividad2;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;

@Rule(order = 4)
public class Actividades2IsMandatoryRule extends BaseRule<String>{

	@When
	public boolean when() {
		return true;

	}

	@Then
	public RuleState then() {

		String actividades2 = getVariableFromMap(
				RdacaaVariableKeyCatalog.PROCEDIMIENTOS_CANTIDAD_2);
		if (isMandatory(RdacaaVariableKeyCatalog.PROCEDIMIENTOS_CANTIDAD_2)
				&& ValidationSupport.isNotDefined(actividades2)) {
			addValidationResult(RdacaaVariableKeyCatalog.PROCEDIMIENTOS_CANTIDAD_2,
					ValidationResultCatalogConstants.CODIGO_ERROR_ACTIVIDAD_2_ES_MANDATORIO);
			return RuleState.BREAK;
		} else if (!isMandatory(RdacaaVariableKeyCatalog.PROCEDIMIENTOS_CANTIDAD_2)
				&& ValidationSupport.isNotDefined(actividades2)) {
			return RuleState.BREAK;
		} else {
			return RuleState.NEXT;
		}

	}
}

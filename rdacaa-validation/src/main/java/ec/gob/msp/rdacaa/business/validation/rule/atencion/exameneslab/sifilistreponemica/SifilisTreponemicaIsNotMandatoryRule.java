package ec.gob.msp.rdacaa.business.validation.rule.atencion.exameneslab.sifilistreponemica;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;

/**
 *
 * @author miguel.faubla
 */
@Rule(order = 5)
public class SifilisTreponemicaIsNotMandatoryRule extends BaseRule<String> {

	@When
	public boolean when() {
		String sifilisTreponemica = getVariableFromMap(RdacaaVariableKeyCatalog.EXAMENES_LABORATORIO_SIFILIS_TREPONEMICA_RESULTADO);
		return (!isMandatory(RdacaaVariableKeyCatalog.EXAMENES_LABORATORIO_SIFILIS_TREPONEMICA_RESULTADO)
				&& ValidationSupport.isNotDefined(sifilisTreponemica));
	}

	@Then
	public RuleState then() {
		return RuleState.BREAK;
	}
}

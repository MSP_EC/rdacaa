package ec.gob.msp.rdacaa.business.validation.rule.atencion.datosantropometricos.imcclasificacion;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;

/**
*
* @author dmurillo
*/
@Rule(order = 5)
public class IMCClasificacionIsNotMandatory extends BaseRule<String> {

	@When
	public boolean when() {
		
		String imcClasificacion = getVariableFromMap(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_IMC_CLASIFICACION);
		return (!isMandatory(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_IMC_CLASIFICACION) &&
				ValidationSupport.isNotDefined(imcClasificacion));
		
	}

	@Then
	public RuleState then() {
		return RuleState.NEXT;
	}
}

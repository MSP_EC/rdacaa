package ec.gob.msp.rdacaa.business.validation.rule.atencion.odontologia.extraido;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;

@Rule(order = 7)
public class ExtraidosLongitudIncorrectaRule extends BaseRule<String>{

	@When
	public boolean when() {
		String dientesExtraidos = getVariableFromMap(
				RdacaaVariableKeyCatalog.ODONTOLOGIA_CANTIDAD_PIEZAS_EXTRAIDAS);
		return ValidationSupport.isStringMoreThanXCharacters(dientesExtraidos, 2);
	}

	@Then
	public RuleState then() {
		addValidationResult(RdacaaVariableKeyCatalog.ODONTOLOGIA_CANTIDAD_PIEZAS_EXTRAIDAS,
				ValidationResultCatalogConstants.CODIGO_ERROR_DIENTES_EXTRAIDOS_LONGITUD_INCORRECTA);
		return RuleState.BREAK;
	}
}

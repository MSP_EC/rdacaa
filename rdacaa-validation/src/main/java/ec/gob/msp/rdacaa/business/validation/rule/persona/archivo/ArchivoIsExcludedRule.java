/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.gob.msp.rdacaa.business.validation.rule.persona.archivo;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;
import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;

/**
 *
 * @author eduardo
 */
@Rule(order = 3)
public class ArchivoIsExcludedRule extends BaseRule<String> {
    @When
	public boolean when() {
		return isExcludedFromValidation(RdacaaVariableKeyCatalog.PERSONA_NUMERO_ARCHIVO);
	}

	@Then
	public RuleState then() {

		String archivo = getVariableFromMap(RdacaaVariableKeyCatalog.PERSONA_NUMERO_ARCHIVO);
		if (!ValidationSupport.isNotDefined(archivo)) {
			addValidationResult(RdacaaVariableKeyCatalog.PERSONA_NUMERO_ARCHIVO,
					ValidationResultCatalogConstants.CODIGO_ERROR_ARCHIVO_NO_DEFINIDA);
		}

		return RuleState.BREAK;

	}
}

package ec.gob.msp.rdacaa.business.validation.rule.atencion.grupoprioritario.tipoatencion;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;

/**
 *
 * @author miguel.faubla
 */
@Rule(order = 4)
public class TipoAtencionIsMandatoryRule extends BaseRule<String> {

	@When
	public boolean when() {
		return true;
	}

	@Then
	public RuleState then() {

		String tipoatencion = getVariableFromMap(RdacaaVariableKeyCatalog.ESTABLECIMIENTO_TIPO_ATENCION);
		if (isMandatory(RdacaaVariableKeyCatalog.ESTABLECIMIENTO_TIPO_ATENCION)
				&& ValidationSupport.isNotDefined(tipoatencion)) {
			addValidationResult(RdacaaVariableKeyCatalog.ESTABLECIMIENTO_TIPO_ATENCION,
					ValidationResultCatalogConstants.CODIGO_ERROR_TIPO_ATENCION_ESTABLECIMIENTO_ES_MANDATORIA);
			return RuleState.BREAK;
		} else if (!isMandatory(RdacaaVariableKeyCatalog.ESTABLECIMIENTO_TIPO_ATENCION)
				&& ValidationSupport.isNotDefined(tipoatencion)) {
			return RuleState.BREAK;
		} else {
			return RuleState.NEXT;
		}

	}

}

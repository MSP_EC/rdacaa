package ec.gob.msp.rdacaa.business.validation.rule.atencion.datosantropometricos.puntajezperimetrocefalicoparaedad;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Given;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.service.ScoreZAndCategoriaService;
import ec.gob.msp.rdacaa.business.service.ScoreZAndCategoriaService.PercentilDetalleNotFoundException;
import ec.gob.msp.rdacaa.business.service.ScoreZAndCategoriaService.PercentilNotFoundException;
import ec.gob.msp.rdacaa.business.service.SignosVitalesReglasValidacionNotFoundException;
import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.UtilsRule;

@Rule(order = 6)
public class PuntajeZPerimetroCefalicoParaEdadCalculateRule extends UtilsRule {

	@Given("scoreZAndCategoriaService")
	private ScoreZAndCategoriaService scoreZAndCategoriaService;

	@When
	public boolean when() {
		return true;
	}

	@Then
	public RuleState then() {

		String puntajeZPerimetroCefalicoParaEdad = "999990000066666";
		String perimetroCefalico = getVariableFromMap(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_PERIMETRO_CEFALICO);
		boolean perimetroCefalicoHasErrors = variableHasErrors(
				RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_PERIMETRO_CEFALICO);
		String sexo = getSexoString();
		Integer edadAnioMesDias = getEdadAnioMesDias();
		Integer edadEnMeses = getEdadMeses();
		Integer edadEnDias = getEdadDias();
		
		boolean valuesNotDefined = ValidationSupport.isNotDefined(perimetroCefalico);

		setRdacaaRawRowResultValue(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_PUNTAJE_Z_PERIMETRO_CEFALICO_PARA_EDAD,
				puntajeZPerimetroCefalicoParaEdad);

		if (edadAnioMesDias == null || edadEnMeses == null || edadEnDias == null) {
			addValidationResult(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_PUNTAJE_Z_PERIMETRO_CEFALICO_PARA_EDAD,
					ValidationResultCatalogConstants.CODIGO_ERROR_DAT_ANT_PUNTAJE_Z_PERIM_CEFALICO_EDAD_FECHAS_FORMATO_INCORRECTO);
		}

		if (sexo == null) {
			addValidationResult(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_PUNTAJE_Z_PERIMETRO_CEFALICO_PARA_EDAD,
					ValidationResultCatalogConstants.CODIGO_ERROR_DAT_ANT_PUNTAJE_Z_PERIM_CEFALICO_EDAD_SEXO_INCORRECTO);
		}

		if (perimetroCefalicoHasErrors) {
			addValidationResult(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_PUNTAJE_Z_PERIMETRO_CEFALICO_PARA_EDAD,
					ValidationResultCatalogConstants.CODIGO_ERROR_DAT_ANT_PUNTAJE_Z_PERIM_CEFALICO_EDAD_PERIM_CEFALICO_ERROR);
		}

		if (valuesNotDefined) {
			addValidationResult(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_PUNTAJE_Z_PERIMETRO_CEFALICO_PARA_EDAD,
					ValidationResultCatalogConstants.CODIGO_INFO_DAT_ANT_PUNTAJE_Z_PERIM_CEFALICO_EDAD_PERIM_CEFALICO_NO_DEFINIDO);
		}
		
		if ((edadAnioMesDias == null || edadEnMeses == null || edadEnDias == null) || sexo == null
				|| perimetroCefalicoHasErrors || valuesNotDefined) {
			return RuleState.BREAK;
		}

		try {
			Double scoreZPerimetroCefalicoParaEdad = scoreZAndCategoriaService.calcularScoreZPerimetroCefalicoParaEdad(
					Double.parseDouble(perimetroCefalico), edadAnioMesDias, edadEnMeses, edadEnDias, sexo);
			puntajeZPerimetroCefalicoParaEdad = scoreZPerimetroCefalicoParaEdad.toString();
		} catch (PercentilNotFoundException | PercentilDetalleNotFoundException
				| SignosVitalesReglasValidacionNotFoundException e) {
			addValidationResult(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_PUNTAJE_Z_PERIMETRO_CEFALICO_PARA_EDAD,
					ValidationResultCatalogConstants.CODIGO_INFO_DAT_ANT_PUNTAJE_Z_PERIM_CEFALICO_EDAD_NO_APLICA);
			
			return RuleState.BREAK;
		}
		
		setRdacaaRawRowResultValue(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_PUNTAJE_Z_PERIMETRO_CEFALICO_PARA_EDAD,
				puntajeZPerimetroCefalicoParaEdad);
		
		addValidationResult(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_PUNTAJE_Z_PERIMETRO_CEFALICO_PARA_EDAD,
				ValidationResultCatalogConstants.CODIGO_INFO_DAT_ANT_PUNTAJE_Z_PERIM_CEFALICO_EDAD_VALOR);
		
		return RuleState.NEXT;
	}
}

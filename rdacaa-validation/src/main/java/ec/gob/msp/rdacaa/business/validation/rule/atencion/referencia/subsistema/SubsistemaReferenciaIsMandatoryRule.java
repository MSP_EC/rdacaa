package ec.gob.msp.rdacaa.business.validation.rule.atencion.referencia.subsistema;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;

@Rule(order = 4)
public class SubsistemaReferenciaIsMandatoryRule extends BaseRule<String> {

	@When
	public boolean when() {
		return true;

	}
	
	@Then
	public RuleState then() {

		String subsistemaReferencia = getVariableFromMap(RdacaaVariableKeyCatalog.REFERENCIA_CONTRAREFERENCIA_CODIGO_SUBSISTEMA);
		if (isMandatory(RdacaaVariableKeyCatalog.REFERENCIA_CONTRAREFERENCIA_CODIGO_SUBSISTEMA)
				&& ValidationSupport.isNotDefined(subsistemaReferencia)) {
			addValidationResult(RdacaaVariableKeyCatalog.REFERENCIA_CONTRAREFERENCIA_CODIGO_SUBSISTEMA,
					ValidationResultCatalogConstants.CODIGO_ERROR_REFERENCIA_SUBSISTEMA_ES_MANDATORIA);
			return RuleState.BREAK;
		} else if (!isMandatory(RdacaaVariableKeyCatalog.REFERENCIA_CONTRAREFERENCIA_CODIGO_SUBSISTEMA)
				&& ValidationSupport.isNotDefined(subsistemaReferencia)) {
			return RuleState.BREAK;
		} else {
			return RuleState.NEXT;
		}

	}
}

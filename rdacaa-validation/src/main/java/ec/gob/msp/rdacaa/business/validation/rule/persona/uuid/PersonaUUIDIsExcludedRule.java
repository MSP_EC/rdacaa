package ec.gob.msp.rdacaa.business.validation.rule.persona.uuid;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;

/**
 *
 * @author miguel.faubla
 */

@Rule(order = 3)
public class PersonaUUIDIsExcludedRule extends BaseRule<String> {

	@When
	public boolean when() {
		return isExcludedFromValidation(RdacaaVariableKeyCatalog.PERSONA_CODIGO_UUID);
	}

	@Then
	public RuleState then() {

		String atencionUUID = getVariableFromMap(RdacaaVariableKeyCatalog.PERSONA_CODIGO_UUID);
		if (!ValidationSupport.isNotDefined(atencionUUID)) {
			addValidationResult(RdacaaVariableKeyCatalog.PERSONA_CODIGO_UUID,
					ValidationResultCatalogConstants.CODIGO_ERROR_PERSONA_CODIGO_UUID_NO_DEFINIDA);
		}

		return RuleState.BREAK;

	}
}

package ec.gob.msp.rdacaa.business.validation.rule.atencion.procedimientos.actividad5;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;

@Rule(order = 3)
public class Actividades5IsExcludedRule extends BaseRule<String>{
	@When
	public boolean when() {
		return isExcludedFromValidation(RdacaaVariableKeyCatalog.PROCEDIMIENTOS_CANTIDAD_5);
	}

	@Then
	public RuleState then() {

		String actividades5 = getVariableFromMap(
				RdacaaVariableKeyCatalog.PROCEDIMIENTOS_CANTIDAD_5);
		if (!ValidationSupport.isNotDefined(actividades5)) {
			addValidationResult(RdacaaVariableKeyCatalog.PROCEDIMIENTOS_CANTIDAD_5,
					ValidationResultCatalogConstants.CODIGO_ERROR_ACTIVIDAD_5_NO_DEFINIDO);
		}

		return RuleState.BREAK;

	}
}

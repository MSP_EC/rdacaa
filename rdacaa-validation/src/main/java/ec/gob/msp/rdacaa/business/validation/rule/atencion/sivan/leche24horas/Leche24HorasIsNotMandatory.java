package ec.gob.msp.rdacaa.business.validation.rule.atencion.sivan.leche24horas;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;

/**
 *
 * @author miguel.faubla
 */
@Rule(order = 5)
public class Leche24HorasIsNotMandatory extends BaseRule<String> {

	@When
	public boolean when() {
		String leche24Horas = getVariableFromMap(RdacaaVariableKeyCatalog.SIVAN_RECIBIO_LECHE_ULTIMAS_24H);
		return (!isMandatory(RdacaaVariableKeyCatalog.SIVAN_RECIBIO_LECHE_ULTIMAS_24H)
				&& ValidationSupport.isNotDefined(leche24Horas));
	}

	@Then
	public RuleState then() {
		return RuleState.BREAK;
	}
}

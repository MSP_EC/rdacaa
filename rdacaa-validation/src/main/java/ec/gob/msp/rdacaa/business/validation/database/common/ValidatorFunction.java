package ec.gob.msp.rdacaa.business.validation.database.common;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.function.Function;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import ec.gob.msp.rdacaa.business.validation.malla.RdacaaRawRow;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaRawRowResult;
import ec.gob.msp.rdacaa.business.validation.rule.RowRuleExecutorBuilder;

@Component
public class ValidatorFunction implements Function<Object, Object> {

	private static final Logger logger = LoggerFactory.getLogger(ValidatorFunction.class);

	@Autowired
	private RowRuleExecutorBuilder rowRuleExecutorBuilder;

	@SuppressWarnings("unchecked")
	@Override
	public Object apply(Object listRows) {

		List<Optional<RdacaaRawRowResult>> listaOutputs = new ArrayList<>();

		try {
			if (listRows instanceof List) {
				List<RdacaaRawRow> listRawRows = (List<RdacaaRawRow>) listRows;
				List<Future<?>> listFuturos = new ArrayList<>();

				ExecutorService executorService = Executors.newFixedThreadPool(6);

				for (RdacaaRawRow rdacaaRawRow : listRawRows) {
					Runnable runnableTask = () -> {
						Optional<RdacaaRawRowResult> output = rowRuleExecutorBuilder
								.executeRuleGroupsByCondiciones(rdacaaRawRow);
						synchronized (listaOutputs) {
							listaOutputs.add(output);
						}
					};
					Future<?> future = executorService.submit(runnableTask);
					listFuturos.add(future);
				}

				while (!isProcessValidationDone(listFuturos)) {
					Thread.sleep(200);
				}

				executorService.shutdown();
			}
		} catch (Exception e) {
			logger.info("Error de validación", e);
			return new ArrayList<Optional<RdacaaRawRowResult>>();
		}

		return listaOutputs;

	}

	private boolean isProcessValidationDone(List<Future<?>> listFuturos) {
		return listFuturos.stream().allMatch(Future::isDone);
	}
}

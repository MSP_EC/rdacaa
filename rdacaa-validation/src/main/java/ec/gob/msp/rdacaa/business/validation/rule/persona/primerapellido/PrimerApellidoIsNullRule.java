package ec.gob.msp.rdacaa.business.validation.rule.persona.primerapellido;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;

@Rule(order = 2)
public class PrimerApellidoIsNullRule extends BaseRule<String> {

	@When
	public boolean when() {
		String primerApellido = getVariableFromMap(RdacaaVariableKeyCatalog.PERSONA_PRIMER_APELLIDO);
		return ValidationSupport.isNullOrEmptyString(primerApellido);
	}

	@Then
	public RuleState then() {
		addValidationResult(RdacaaVariableKeyCatalog.PERSONA_PRIMER_APELLIDO,
				ValidationResultCatalogConstants.CODIGO_ERROR_PRIMER_APELLIDO_NULO);
		return RuleState.BREAK;
	}

}

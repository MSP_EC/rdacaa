package ec.gob.msp.rdacaa.business.validation.rule.atencion.vacunacion.vacunaesquema1;

import org.apache.commons.lang3.StringUtils;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.UtilsRule;

/**
 *
 * @author miguel.faubla
 */
@Rule(order = 6)
public class VacunaEsquema1IsIncorrectRule extends UtilsRule {

	@When
	public boolean when() {
		return true;
	}

	@Then
	public RuleState then() {

		String vacunaEsquema1 = getVariableFromMap(RdacaaVariableKeyCatalog.VACUNAS_CODIGO_ESQUEMA_1);
		String vacuna1Id = getVariableFromMap(RdacaaVariableKeyCatalog.VACUNAS_CODIGO_APLICACION_1);
		String vacunaNroDosis1 = getVariableFromMap(RdacaaVariableKeyCatalog.VACUNAS_CODIGO_DOSIS_1);

		if (!(StringUtils.isNumeric(vacunaEsquema1) && StringUtils.isNumeric(vacuna1Id)
				&& StringUtils.isNumeric(vacunaNroDosis1))) {
			addValidationResult(RdacaaVariableKeyCatalog.VACUNAS_CODIGO_ESQUEMA_1,
					ValidationResultCatalogConstants.CODIGO_ERROR_VACUNAS_CODIGO_ESQUEMA_1_COD_VACUNA_DOSIS_NUMERICOS);
			return RuleState.BREAK;
		}

		Integer edadAnioParcial = getEdadAnioParcial();

		Integer edadMesParcial = getEdadMesParcial();

		Integer edadDiasParcial = getEdadDiasParcial();

		if (!validationQueryService.isEsquemaVacunacionValido(vacunaEsquema1, vacuna1Id, vacunaNroDosis1)) {
			addValidationResult(RdacaaVariableKeyCatalog.VACUNAS_CODIGO_ESQUEMA_1,
					ValidationResultCatalogConstants.CODIGO_ERROR_VACUNAS_CODIGO_ESQUEMA_1_ES_INCORRECTO);
			return RuleState.BREAK;
		} else if (edadAnioParcial != null && edadMesParcial != null && edadDiasParcial != null
				&& validationQueryService.verifyApplicationVacunaByEdadAndEsquemaId(edadDiasParcial, edadMesParcial,
						edadAnioParcial, Integer.parseInt(vacunaEsquema1)) == null) {
			addValidationResult(RdacaaVariableKeyCatalog.VACUNAS_CODIGO_ESQUEMA_1,
					ValidationResultCatalogConstants.CODIGO_ERROR_VACUNAS_CODIGO_ESQUEMA_1_EDAD_INCORRECTA);
			return RuleState.BREAK;
		} else {
			return RuleState.NEXT;
		}

	}
}

package ec.gob.msp.rdacaa.business.validation.rule.atencion.procedimientos.prodto1;

import org.apache.commons.lang3.StringUtils;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;

@Rule(order = 7)
public class Procedimiento1IsIncorrectoRule extends BaseRule<String>{

	@When
	public boolean when() {	
		String procedimiento1 = getVariableFromMap(RdacaaVariableKeyCatalog.PROCEDIMIENTOS_CODIGO_1);
		if (StringUtils.isNumeric(procedimiento1)) {
			return !this.validationQueryService.isProcedimientoValido(procedimiento1);
		}
		return true;
	}

	@Then
	public RuleState then() {
		addValidationResult(RdacaaVariableKeyCatalog.PROCEDIMIENTOS_CODIGO_1,
				ValidationResultCatalogConstants.CODIGO_ERROR_PROCEDIMIENTO_1_INCORRECTO);
		return RuleState.BREAK;
	}
}

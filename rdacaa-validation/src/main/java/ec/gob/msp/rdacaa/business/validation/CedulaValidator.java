package ec.gob.msp.rdacaa.business.validation;

import java.util.ArrayList;
import java.util.List;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResult;
import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalog;
import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.common.Validator;
import ec.gob.msp.rdacaa.business.validation.usuario.UsuarioValidation;

public class CedulaValidator extends ValidationSupport implements Validator<String> {

	private static final long serialVersionUID = 7208481577012645618L;

	@Override
    public List<ValidationResult> validate(String cedula) {

        List<ValidationResult> listaErrores = new ArrayList<>();
        if (!UsuarioValidation.isIdentificacionValida(cedula)) {
            listaErrores.add(ValidationResultCatalog.CEDULA_INVALIDA.getValidationError());
        }

        return listaErrores;
    }

}

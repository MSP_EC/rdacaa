package ec.gob.msp.rdacaa.business.validation.rule.atencion.obstetricia.metodosemgestacion;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;

/**
 *
 * @author miguel.faubla
 */
@Rule(order = 4)
public class MetodoSemanagestacionIsMandatoryRule extends BaseRule<String> {

	@When
	public boolean when() {
		return true;

	}

	@Then
	public RuleState then() {

		String metodoSemanagestacion = getVariableFromMap(RdacaaVariableKeyCatalog.OBSTETRICIA_CODIGO_METODO_DET_SEMANAS_GESTACION);
		if (isMandatory(RdacaaVariableKeyCatalog.OBSTETRICIA_CODIGO_METODO_DET_SEMANAS_GESTACION)
				&& ValidationSupport.isNotDefined(metodoSemanagestacion)) {
			addValidationResult(RdacaaVariableKeyCatalog.OBSTETRICIA_CODIGO_METODO_DET_SEMANAS_GESTACION,
					ValidationResultCatalogConstants.CODIGO_ERROR_OBSTETRICIA_CODIGO_METODO_DET_SEMANAS_GESTACION_ES_MANDATORIA);
			return RuleState.BREAK;
		} else if (!isMandatory(RdacaaVariableKeyCatalog.OBSTETRICIA_CODIGO_METODO_DET_SEMANAS_GESTACION)
				&& ValidationSupport.isNotDefined(metodoSemanagestacion)) {
			return RuleState.BREAK;
		} else {
			return RuleState.NEXT;
		}

	}

}

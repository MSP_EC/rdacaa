package ec.gob.msp.rdacaa.business.validation.rule;

import com.deliveredtechnologies.rulebook.annotation.Given;
import com.deliveredtechnologies.rulebook.annotation.Result;

import ec.gob.msp.rdacaa.business.entity.Variable;
import ec.gob.msp.rdacaa.business.service.ValidationQueryService;
import ec.gob.msp.rdacaa.business.validation.common.ValidationResult;
import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogMap;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaRawRow;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaRawRowResult;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableEntityCatalog;
import ec.gob.msp.rdacaa.business.validation.malla.RowItem;

public abstract class BaseRule<T> {

	@Given("rdacaa_row")
	protected RdacaaRawRow row;

	@Given("validacion_variable_catalog")
	protected RdacaaVariableEntityCatalog rdacaaVariableEntityCatalog;

	@Given("validacion_result_catalog")
	protected ValidationResultCatalogMap validationResultCatalogMap;

	@Given("validacion_query_service")
	protected ValidationQueryService validationQueryService;

	@Given("rdacaa_row_result")
	protected RdacaaRawRowResult resultTotal;

	@Result
	protected RdacaaRawRowResult result;

	@SuppressWarnings("unchecked")
	public T getVariableFromMap(String key) {
		RowItem item = row.get(key);
		return item != null ? (T) item.getItemValue() : null;
	}

	public boolean isExcludedFromValidation(String key) {
		RowItem item = row.get(key);
		return item != null ? item.isExcludedFromValidation() : null;
	}

	public boolean isMandatory(String key) {
		RowItem item = row.get(key);
		return item != null ? item.isMandatory() : null;
	}

	public void setIsMandatory(String key, boolean isMandatory) {
		RowItem item = row.get(key);
		item.setMandatory(isMandatory);
	}

	public boolean variableHasErrors(String keyVariable) {
		return this.resultTotal.get(keyVariable).hasErrors();
	}

	public void addValidationResult(String keyVariable, String keyValidationResult) {
		this.result.get(keyVariable).getValidationResultList()
				.add(validationResultCatalogMap.getValidationResult(keyValidationResult));
	}

	public void addValidationResult(String keyVariable, ValidationResult validationResult) {
		this.result.get(keyVariable).getValidationResultList().add(validationResult);
	}
	
	public void setRdacaaRawRowResultValue(String keyVariable, Object value) {
		this.result.get(keyVariable).setValue(value);
	}
	
	public String getRdacaaRawRowResultValue(String keyVariable) {
		return (String) this.resultTotal.get(keyVariable).getValue();
	}
	
	public Variable getValidationVariableFromMap(String keyVariable) {
		return rdacaaVariableEntityCatalog.get(keyVariable);
	}

	public ValidationResult getValidationResultFromMap(String keyValidationResult) {
		return validationResultCatalogMap.getValidationResult(keyValidationResult);
	}
}

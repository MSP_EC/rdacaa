package ec.gob.msp.rdacaa.business.validation.rule.atencion.prescripcion.suphierromultivita;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.UtilsRule;

/**
 *
 * @author miguel.faubla
 */
@Rule(order = 6)
public class SuplementoHierroMultivitaminasIsIncorrect extends UtilsRule {

	@When
	public boolean when() {
		return true;
	}

	@Then
	public RuleState then() {

		String suplementoHierroMultivitaminas = getVariableFromMap(
				RdacaaVariableKeyCatalog.PRESCRIPCION_SUPLEMENTOS_HIERRO_MULTIVITAMINAS);

		Integer edadAniosMesesDias = getEdadAnioMesDias();

		boolean isValidValue = "0".equals(suplementoHierroMultivitaminas) || "1".equals(suplementoHierroMultivitaminas);

		if (!isValidValue) {
			addValidationResult(RdacaaVariableKeyCatalog.PRESCRIPCION_SUPLEMENTOS_HIERRO_MULTIVITAMINAS,
					ValidationResultCatalogConstants.CODIGO_ERROR_PRESCRIPCION_SUPLEMENTOS_HIERRO_MULTIVITAMINAS_ES_INCORRECTO);
		}

		if (edadAniosMesesDias == null) {
			addValidationResult(RdacaaVariableKeyCatalog.PRESCRIPCION_SUPLEMENTOS_HIERRO_MULTIVITAMINAS,
					ValidationResultCatalogConstants.CODIGO_ERROR_PRESCRIPCION_SUPLEMENTOS_HIERRO_MULTIVITAMINAS_FECHAS_INVALIDAS);
			return RuleState.BREAK;
		}

		boolean requiereEdad6mesesA24meses = !(edadAniosMesesDias >= 600 && edadAniosMesesDias < 20000)
				&& !ValidationSupport.isNotDefined(suplementoHierroMultivitaminas);
		if (requiereEdad6mesesA24meses) {
			addValidationResult(RdacaaVariableKeyCatalog.PRESCRIPCION_SUPLEMENTOS_HIERRO_MULTIVITAMINAS,
					ValidationResultCatalogConstants.CODIGO_ERROR_PRESCRIPCION_SUPLEMENTOS_HIERRO_MULTIVITAMINAS_DEBE_TENER_DEFAULT_VALUE);
		}
		
		if(!isValidValue || requiereEdad6mesesA24meses) {
			return RuleState.BREAK;
		}

		return RuleState.NEXT;
	}
}

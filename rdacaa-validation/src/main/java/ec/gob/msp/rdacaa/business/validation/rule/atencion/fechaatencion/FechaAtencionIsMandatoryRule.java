package ec.gob.msp.rdacaa.business.validation.rule.atencion.fechaatencion;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;

/**
 *
 * @author miguel.faubla
 */
@Rule(order = 4)
public class FechaAtencionIsMandatoryRule extends BaseRule<String> {

	@When
	public boolean when() {
		return true;

	}

	@Then
	public RuleState then() {

		String fechaAtencion = getVariableFromMap(RdacaaVariableKeyCatalog.ATENCION_FECHA);
		if (isMandatory(RdacaaVariableKeyCatalog.ATENCION_FECHA)
				&& ValidationSupport.isNotDefined(fechaAtencion)) {
			addValidationResult(RdacaaVariableKeyCatalog.ATENCION_FECHA,
					ValidationResultCatalogConstants.CODIGO_ERROR_ATENCION_FECHA_ES_MANDATORIA);
			return RuleState.BREAK;
		} else if (!isMandatory(RdacaaVariableKeyCatalog.ATENCION_FECHA)
				&& ValidationSupport.isNotDefined(fechaAtencion)) {
			return RuleState.BREAK;
		} else {
			return RuleState.NEXT;
		}

	}

}

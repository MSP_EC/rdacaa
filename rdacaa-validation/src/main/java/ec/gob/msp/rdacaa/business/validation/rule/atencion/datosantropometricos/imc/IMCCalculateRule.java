package ec.gob.msp.rdacaa.business.validation.rule.atencion.datosantropometricos.imc;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.UtilsRule;
import ec.gob.msp.rdacaa.utilitario.medico.UtilitarioMedico;

@Rule(order = 6)
public class IMCCalculateRule extends UtilsRule {

	private static final Double VALOR_MAXIMO_IMC = 200.00d;

	@When
	public boolean when() {
		return true;
	}

	@Then
	public RuleState then() {

		String imc = "999990000066666";
		String peso = getVariableFromMap(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_PESO);
		String tallaCorregida = getRdacaaRawRowResultValue(
				RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_TALLA_CORREGIDA);
		boolean pesoHasErrors = variableHasErrors(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_PESO);
		boolean tallaCorregidaHasErrors = variableHasErrors(
				RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_TALLA_CORREGIDA);
		
		boolean pesoOTallaNoDefinido = ValidationSupport.isNotDefined(peso)
				|| ValidationSupport.isNotDefined(tallaCorregida);

		setRdacaaRawRowResultValue(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_INDICE_MASA_CORPORAL, imc);

		if (pesoHasErrors) {
			addValidationResult(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_INDICE_MASA_CORPORAL,
					ValidationResultCatalogConstants.CODIGO_ERROR_DAT_ANT_IMC_PESO_TIENE_ERRORES);
		}

		if (tallaCorregidaHasErrors) {
			addValidationResult(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_INDICE_MASA_CORPORAL,
					ValidationResultCatalogConstants.CODIGO_ERROR_DAT_ANT_IMC_TALLA_CORREGIDA_TIENE_ERRORES);
		}

		if (pesoOTallaNoDefinido) {
			addValidationResult(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_INDICE_MASA_CORPORAL,
					ValidationResultCatalogConstants.CODIGO_INFO_DAT_ANT_IMC_PESO_O_TALLA_NO_DEFINIDO);
		}

		if (pesoHasErrors || tallaCorregidaHasErrors || pesoOTallaNoDefinido) {
			return RuleState.BREAK;
		}

		Double imcDouble = UtilitarioMedico.calcularIndiceMasaCorporal(Double.parseDouble(peso),
				Double.parseDouble(tallaCorregida) / 100);
		imc = imcDouble.toString();

		setRdacaaRawRowResultValue(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_INDICE_MASA_CORPORAL, imc);

		if (imcDouble > VALOR_MAXIMO_IMC) {
			addValidationResult(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_INDICE_MASA_CORPORAL,
					ValidationResultCatalogConstants.CODIGO_ERR_DAT_ANT_IMC_MAYOR_200);
			return RuleState.BREAK;
		}

		addValidationResult(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_INDICE_MASA_CORPORAL,
				ValidationResultCatalogConstants.CODIGO_INFO_DAT_ANT_IMC_VALOR);
		return RuleState.NEXT;
	}
}

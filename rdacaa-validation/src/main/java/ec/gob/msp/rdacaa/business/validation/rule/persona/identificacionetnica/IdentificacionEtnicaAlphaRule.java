package ec.gob.msp.rdacaa.business.validation.rule.persona.identificacionetnica;

import java.util.ArrayList;
import java.util.List;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResult;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaRawRowResult;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableValueResult;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;

/**
*
* @author dmurillo
*/

@Rule(order = 1)
public class IdentificacionEtnicaAlphaRule extends BaseRule<String>  {
	
	@When
	public boolean when() {
		this.result = RdacaaRawRowResult.getInstance();
		List<ValidationResult> listaErrores = new ArrayList<>();
		this.result.addResultToField(RdacaaVariableKeyCatalog.PERSONA_CODIGO_AUTOIDENTIFICACION_ETNICA,
				RdacaaVariableValueResult.getInstance(
						getVariableFromMap(RdacaaVariableKeyCatalog.PERSONA_CODIGO_AUTOIDENTIFICACION_ETNICA), listaErrores));
		return true;
	}

	@Then
	public RuleState then() {
		return RuleState.NEXT;
	}
}

package ec.gob.msp.rdacaa.business.validation.rule.atencion.profesional;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;

/**
 *
 * @author miguel.faubla
 */
@Rule(order = 2)
public class ProfesionalIsNullRule extends BaseRule<String> {

	@When
	public boolean when() {
		String codProfesional = getVariableFromMap(RdacaaVariableKeyCatalog.PROFESIONAL_CODIGO);
		return ValidationSupport.isNullOrEmptyString(codProfesional);
	}

	@Then
	public RuleState then() {
		addValidationResult(RdacaaVariableKeyCatalog.PROFESIONAL_CODIGO,
				ValidationResultCatalogConstants.CODIGO_ERROR_PROFESIONAL_CODIGO_NULO);
		return RuleState.BREAK;
	}

}

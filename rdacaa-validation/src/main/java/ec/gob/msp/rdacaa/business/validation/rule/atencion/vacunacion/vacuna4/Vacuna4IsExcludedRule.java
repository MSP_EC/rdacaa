package ec.gob.msp.rdacaa.business.validation.rule.atencion.vacunacion.vacuna4;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;

/**
 *
 * @author miguel.faubla
 */

@Rule(order = 3)
public class Vacuna4IsExcludedRule extends BaseRule<String> {

	@When
	public boolean when() {
		return isExcludedFromValidation(RdacaaVariableKeyCatalog.VACUNAS_CODIGO_APLICACION_4);
	}

	@Then
	public RuleState then() {

		String vacuna4 = getVariableFromMap(RdacaaVariableKeyCatalog.VACUNAS_CODIGO_APLICACION_4);
		if (!ValidationSupport.isNotDefined(vacuna4)) {
			addValidationResult(RdacaaVariableKeyCatalog.VACUNAS_CODIGO_APLICACION_4,
					ValidationResultCatalogConstants.CODIGO_ERROR_VACUNAS_CODIGO_APLICACION_4_NO_DEFINIDA);
		}

		return RuleState.BREAK;

	}
}

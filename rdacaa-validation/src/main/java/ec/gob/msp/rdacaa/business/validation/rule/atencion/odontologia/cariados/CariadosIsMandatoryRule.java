package ec.gob.msp.rdacaa.business.validation.rule.atencion.odontologia.cariados;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;

@Rule(order = 4)
public class CariadosIsMandatoryRule extends BaseRule<String> {

	@When
	public boolean when() {
		return true;
	}
	
	@Then
	public RuleState then() {

		String dientesCariados = getVariableFromMap(RdacaaVariableKeyCatalog.ODONTOLOGIA_CANTIDAD_PIEZAS_CARIADAS);
		if (isMandatory(RdacaaVariableKeyCatalog.ODONTOLOGIA_CANTIDAD_PIEZAS_CARIADAS) 
				&& ValidationSupport.isNotDefined(dientesCariados)) {
			addValidationResult(RdacaaVariableKeyCatalog.ODONTOLOGIA_CANTIDAD_PIEZAS_CARIADAS,
					ValidationResultCatalogConstants.CODIGO_ERROR_DIENTES_CARIADOS_ES_MANDATORIO);
			return RuleState.BREAK;
		} else if (!isMandatory(RdacaaVariableKeyCatalog.ODONTOLOGIA_CANTIDAD_PIEZAS_CARIADAS)
				&& ValidationSupport.isNotDefined(dientesCariados)) {
			return RuleState.BREAK;
		} else {
			return RuleState.NEXT;
		}

	}
}

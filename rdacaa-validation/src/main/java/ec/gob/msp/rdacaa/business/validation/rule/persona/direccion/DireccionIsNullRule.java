/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.gob.msp.rdacaa.business.validation.rule.persona.direccion;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;
import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;

/**
 *
 * @author eduardo
 */
@Rule(order =2)
public class DireccionIsNullRule extends BaseRule<String>{
    @When
	public boolean when() {
		String direccionDomicilio = getVariableFromMap(RdacaaVariableKeyCatalog.PERSONA_DIRECCION_DOMICILIO);
		return ValidationSupport.isNullOrEmptyString(direccionDomicilio);
	}

	@Then
	public RuleState then() {
		addValidationResult(RdacaaVariableKeyCatalog.PERSONA_DIRECCION_DOMICILIO,
				ValidationResultCatalogConstants.CODIGO_ERROR_DIRECCION_NULL);
		return RuleState.BREAK;
	}
}

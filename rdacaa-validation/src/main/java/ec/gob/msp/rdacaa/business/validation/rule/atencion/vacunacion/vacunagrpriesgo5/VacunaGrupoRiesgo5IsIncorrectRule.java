package ec.gob.msp.rdacaa.business.validation.rule.atencion.vacunacion.vacunagrpriesgo5;

import org.apache.commons.lang3.StringUtils;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.UtilsRule;

@Rule(order = 6)
public class VacunaGrupoRiesgo5IsIncorrectRule extends UtilsRule {

	@When
	public boolean when() {
		return true;
	}

	@Then
	public RuleState then() {

		String vacuna5Id = getVariableFromMap(RdacaaVariableKeyCatalog.VACUNAS_CODIGO_APLICACION_5);
		String vacunaGrupoRiesgo5 = getVariableFromMap(RdacaaVariableKeyCatalog.VACUNAS_CODIGO_GRUPO_RIESGO_5);
		
		boolean vacuna5IdHasErrors = variableHasErrors(RdacaaVariableKeyCatalog.VACUNAS_CODIGO_APLICACION_5);
		
		if(vacuna5IdHasErrors || ValidationSupport.isNotDefined(vacunaGrupoRiesgo5)) {
			return RuleState.NEXT;
		}

		if (!(StringUtils.isNumeric(vacuna5Id) && StringUtils.isNumeric(vacunaGrupoRiesgo5))) {
			addValidationResult(RdacaaVariableKeyCatalog.VACUNAS_CODIGO_GRUPO_RIESGO_5,
					ValidationResultCatalogConstants.CODIGO_ERROR_VACUNAS_CODIGO_GRUPO_RIESGO_5_COD_VACUNA_DOSIS_NUMERICOS);
			return RuleState.BREAK;
		}
		
		boolean isHombre = "M".equals(getSexoString());
		boolean isMujer = "F".equals(getSexoString());
		boolean isIntersexual = "I".equals(getSexoString());
		
		if(getSexoString() == null) {
			addValidationResult(RdacaaVariableKeyCatalog.VACUNAS_CODIGO_GRUPO_RIESGO_5,
					ValidationResultCatalogConstants.CODIGO_ERROR_VACUNAS_CODIGO_GRUPO_RIESGO_5_SEXO_INCORRECTO);
			return RuleState.BREAK;
		}
		
		boolean vacunaHasGrupoRiesgo = validationQueryService.vacunaHasGrupoRiesgo(vacuna5Id, isHombre, isMujer, isIntersexual);
		
		if(vacunaHasGrupoRiesgo && !ValidationSupport.isNotDefined(vacunaGrupoRiesgo5)) {
			if(!validationQueryService.isGrupoRiesgoValido(vacuna5Id, vacunaGrupoRiesgo5, isHombre, isMujer, isIntersexual)) {
				addValidationResult(RdacaaVariableKeyCatalog.VACUNAS_CODIGO_GRUPO_RIESGO_5,
						ValidationResultCatalogConstants.CODIGO_ERROR_VACUNAS_CODIGO_GRUPO_RIESGO_5_DEBE_TENER_GRUPO_RIESGO_VALIDO);
				return RuleState.BREAK;
			}else {
				return RuleState.NEXT;
			}
		} else if(!vacunaHasGrupoRiesgo && !ValidationSupport.isNotDefined(vacunaGrupoRiesgo5)) {
			addValidationResult(RdacaaVariableKeyCatalog.VACUNAS_CODIGO_GRUPO_RIESGO_5,
					ValidationResultCatalogConstants.CODIGO_ERROR_VACUNAS_CODIGO_GRUPO_RIESGO_5_DEBE_TENER_GRUPO_RIESGO_NO_DEFINIDO);
			return RuleState.BREAK;
		} else if(!vacunaHasGrupoRiesgo && ValidationSupport.isNotDefined(vacunaGrupoRiesgo5)) {
			return RuleState.NEXT;
		} else if(vacunaHasGrupoRiesgo && ValidationSupport.isNotDefined(vacunaGrupoRiesgo5)) {
			addValidationResult(RdacaaVariableKeyCatalog.VACUNAS_CODIGO_GRUPO_RIESGO_5,
					ValidationResultCatalogConstants.CODIGO_ERROR_VACUNAS_CODIGO_GRUPO_RIESGO_5_DEBE_TENER_GRUPO_RIESGO_VALIDO);
			return RuleState.BREAK;
		}
		
		return RuleState.NEXT;
	}
}

package ec.gob.msp.rdacaa.business.validation.rule.atencion.procedimientos.prodto5;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;

@Rule(order = 5)
public class Procedimiento5IsNotMandatory extends BaseRule<String>{
	@When
	public boolean when() {
		String procedimiento5 = getVariableFromMap(RdacaaVariableKeyCatalog.PROCEDIMIENTOS_CODIGO_5);
		return (!isMandatory(RdacaaVariableKeyCatalog.PROCEDIMIENTOS_CODIGO_5)
				&& ValidationSupport.isNotDefined(procedimiento5));
	}

	@Then
	public RuleState then() {
		return RuleState.BREAK;
	}
}

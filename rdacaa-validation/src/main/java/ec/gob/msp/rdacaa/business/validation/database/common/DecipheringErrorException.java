package ec.gob.msp.rdacaa.business.validation.database.common;

public class DecipheringErrorException extends RuntimeException{

	private static final long serialVersionUID = 4668712342356030278L;

	public DecipheringErrorException() {
		super();
	}

	public DecipheringErrorException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public DecipheringErrorException(String message, Throwable cause) {
		super(message, cause);
	}

	public DecipheringErrorException(String message) {
		super(message);
	}

	public DecipheringErrorException(Throwable cause) {
		super(cause);
	}
	
}

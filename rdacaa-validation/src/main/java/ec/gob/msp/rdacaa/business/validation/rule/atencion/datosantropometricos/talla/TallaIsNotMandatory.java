package ec.gob.msp.rdacaa.business.validation.rule.atencion.datosantropometricos.talla;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;

/**
*
* @author dmurillo
*/
@Rule(order = 5)
public class TallaIsNotMandatory extends BaseRule<String> {

	@When
	public boolean when() {
		
		String talla = getVariableFromMap(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_TALLA);
		return (!isMandatory(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_TALLA) &&
				ValidationSupport.isNotDefined(talla));
		
	}

	@Then
	public RuleState then() {
		return RuleState.NEXT;
	}
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.gob.msp.rdacaa.business.validation.rule.atencion.grupovulnerable.gpr_vulnerable2;

import com.deliveredtechnologies.rulebook.model.RuleBook;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaRawRowResult;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.RdacaaRuleExecutor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

/**
 *
 * @author eduardo
 */
@Component
public class GrupoVulnerable2RuleGroupExecutor extends RdacaaRuleExecutor {
    @Autowired
    public GrupoVulnerable2RuleGroupExecutor(@Qualifier("ReglasGrupoVulnerable2") RuleBook<RdacaaRawRowResult> ruleBook){
        super();
        this.ruleBook = ruleBook;
        this.variableKey = RdacaaVariableKeyCatalog.GRUPO_VULNERABLE_2;
    }
}

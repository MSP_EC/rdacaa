package ec.gob.msp.rdacaa.business.validation.rule.persona.identidadgenero;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;

/**
*
* @author dmurillo
*/

@Rule(order = 3)
public class IdentidadGeneroIsExcludedRule extends BaseRule<String> {
	@When
	public boolean when() {
		return isExcludedFromValidation(RdacaaVariableKeyCatalog.PERSONA_CODIGO_IDENTIFICACION_GENERO);
	}

	@Then
	public RuleState then() {
		
		String identidadGenero = getVariableFromMap(RdacaaVariableKeyCatalog.PERSONA_CODIGO_IDENTIFICACION_GENERO);
		if(!ValidationSupport.isNotDefined(identidadGenero)){
			addValidationResult(RdacaaVariableKeyCatalog.PERSONA_CODIGO_IDENTIFICACION_GENERO,
					ValidationResultCatalogConstants.CODIGO_ERROR_IDENTIFICACIONGENERO_NO_DEFINIDO);
		}
		
		return RuleState.BREAK;

	}
}

package ec.gob.msp.rdacaa.business.validation.rule.atencion.prescripcion.supvitaminaa;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.UtilsRule;

/**
 *
 * @author miguel.faubla
 */
@Rule(order = 6)
public class SuplementoVitaminaAIsIncorrect extends UtilsRule {

	@When
	public boolean when() {
		return true;
	}

	@Then
	public RuleState then() {

		String suplementoVitaminaA = getVariableFromMap(
				RdacaaVariableKeyCatalog.PRESCRIPCION_SUPLEMENTOS_VITAMINA_A);

		Integer edadAniosMesesDias = getEdadAnioMesDias();

		boolean isValidValue = "0".equals(suplementoVitaminaA) || "1".equals(suplementoVitaminaA);

		if (!isValidValue) {
			addValidationResult(RdacaaVariableKeyCatalog.PRESCRIPCION_SUPLEMENTOS_VITAMINA_A,
					ValidationResultCatalogConstants.CODIGO_ERROR_PRESCRIPCION_SUPLEMENTOS_VITAMINA_A_ES_INCORRECTO);
		}

		if (edadAniosMesesDias == null) {
			addValidationResult(RdacaaVariableKeyCatalog.PRESCRIPCION_SUPLEMENTOS_VITAMINA_A,
					ValidationResultCatalogConstants.CODIGO_ERROR_PRESCRIPCION_SUPLEMENTOS_VITAMINA_A_FECHAS_INVALIDAS);
			return RuleState.BREAK;
		}

		boolean requiereEdad6mesesA59meses = !(edadAniosMesesDias >= 600 && edadAniosMesesDias < 50000)
				&& !ValidationSupport.isNotDefined(suplementoVitaminaA);
		if (requiereEdad6mesesA59meses) {
			addValidationResult(RdacaaVariableKeyCatalog.PRESCRIPCION_SUPLEMENTOS_VITAMINA_A,
					ValidationResultCatalogConstants.CODIGO_ERROR_PRESCRIPCION_SUPLEMENTOS_VITAMINA_A_DEBE_TENER_DEFAULT_VALUE);
		}
		
		if(!isValidValue || requiereEdad6mesesA59meses) {
			return RuleState.BREAK;
		}

		return RuleState.NEXT;
	}
}

package ec.gob.msp.rdacaa.business.validation.rule.atencion.datosantropometricos.categoriaperimetrocefalicoparaedad;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;

/**
*
* @author dmurillo
*/
@Rule(order = 5)
public class CategoriaPerimetroCefalicoParaEdadIsNotMandatory extends BaseRule<String> {

	@When
	public boolean when() {
		
		String categoriaPerimetroCefalicoParaEdad = getVariableFromMap(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_CATEGORIA_PERIMETRO_CEFALICO_PARA_EDAD);
		return (!isMandatory(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_CATEGORIA_PERIMETRO_CEFALICO_PARA_EDAD) &&
				ValidationSupport.isNotDefined(categoriaPerimetroCefalicoParaEdad));
		
	}

	@Then
	public RuleState then() {
		return RuleState.NEXT;
	}
}

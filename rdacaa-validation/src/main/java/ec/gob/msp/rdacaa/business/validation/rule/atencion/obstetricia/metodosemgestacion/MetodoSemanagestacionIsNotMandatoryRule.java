package ec.gob.msp.rdacaa.business.validation.rule.atencion.obstetricia.metodosemgestacion;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;

/**
 *
 * @author miguel.faubla
 */
@Rule(order = 5)
public class MetodoSemanagestacionIsNotMandatoryRule extends BaseRule<String> {

	@When
	public boolean when() {
		String metodoSemanagestacion = getVariableFromMap(RdacaaVariableKeyCatalog.OBSTETRICIA_CODIGO_METODO_DET_SEMANAS_GESTACION);
		return (!isMandatory(RdacaaVariableKeyCatalog.OBSTETRICIA_CODIGO_METODO_DET_SEMANAS_GESTACION)
				&& ValidationSupport.isNotDefined(metodoSemanagestacion));
	}

	@Then
	public RuleState then() {
		return RuleState.BREAK;
	}
}

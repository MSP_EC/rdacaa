package ec.gob.msp.rdacaa.business.validation;

import java.util.ArrayList;
import java.util.List;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResult;
import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalog;
import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.common.Validator;

public class ArchivoValidator extends ValidationSupport implements Validator<String> {
	
	private static final long serialVersionUID = -4092093932921414222L;
	private static final int SEIS_CARACTERES = 6;

	public List<ValidationResult> validate(String archivo) {
		
		List<ValidationResult> listaErrores = new ArrayList<>();
		
		if(isNullOrEmptyString(archivo)) {
			listaErrores.add(ValidationResultCatalog.ARCHIVO_VACIO.getValidationError());
		} else if(isStringLessThanXCharacters(archivo,SEIS_CARACTERES)) {
			listaErrores.add(ValidationResultCatalog.ARCHIVO_MENOR_SEIS_CARACTERES.getValidationError());
		}
		
		return listaErrores;
	}

}

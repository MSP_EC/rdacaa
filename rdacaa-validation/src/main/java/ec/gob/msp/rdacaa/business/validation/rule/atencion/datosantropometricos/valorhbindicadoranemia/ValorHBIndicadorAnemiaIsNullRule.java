package ec.gob.msp.rdacaa.business.validation.rule.atencion.datosantropometricos.valorhbindicadoranemia;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;

@Rule(order = 2)
public class ValorHBIndicadorAnemiaIsNullRule extends BaseRule<String> {

	@When
	public boolean when() {
		String indicadorAnemia = getVariableFromMap(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_CODIGO_INDICADOR_ANEMIA);
		return ValidationSupport.isNullOrEmptyString(indicadorAnemia);
	}

	@Then
	public RuleState then() {
		addValidationResult(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_CODIGO_INDICADOR_ANEMIA,
				ValidationResultCatalogConstants.CODIGO_ERROR_DAT_ANT_CODIGO_INDICADOR_ANEMIA_NULO);
		return RuleState.BREAK;
	}
}

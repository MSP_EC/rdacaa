package ec.gob.msp.rdacaa.business.validation.rule.atencion.datosantropometricos.puntajezimcparaedad;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Given;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.service.ScoreZAndCategoriaService;
import ec.gob.msp.rdacaa.business.service.ScoreZAndCategoriaService.PercentilDetalleNotFoundException;
import ec.gob.msp.rdacaa.business.service.ScoreZAndCategoriaService.PercentilNotFoundException;
import ec.gob.msp.rdacaa.business.service.SignosVitalesReglasValidacionNotFoundException;
import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.UtilsRule;

@Rule(order = 6)
public class PuntajeZIMCParaEdadCalculateRule extends UtilsRule {

	@Given("scoreZAndCategoriaService")
	private ScoreZAndCategoriaService scoreZAndCategoriaService;

	@When
	public boolean when() {
		return true;
	}

	@Then
	public RuleState then() {
		String puntajeZIMCParaEdad = "999990000066666";
		String imc = getRdacaaRawRowResultValue(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_INDICE_MASA_CORPORAL);
		boolean imcHasErrors = variableHasErrors(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_INDICE_MASA_CORPORAL);
		String sexo = getSexoString();
		Integer edadAnioMesDias = getEdadAnioMesDias();
		Integer edadEnMeses = getEdadMeses();
		Integer edadEnDias = getEdadDias();

		boolean valuesNotDefined = ValidationSupport.isNotDefined(imc);

		setRdacaaRawRowResultValue(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_PUNTAJE_Z_IMC_PARA_EDAD,
				puntajeZIMCParaEdad);

		if (edadAnioMesDias == null || edadEnMeses == null || edadEnDias == null) {
			addValidationResult(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_PUNTAJE_Z_IMC_PARA_EDAD,
					ValidationResultCatalogConstants.CODIGO_ERROR_DAT_ANT_PUNTAJE_Z_IMC_EDAD_FECHAS_FORMATO_INCORRECTO);
		}

		if (sexo == null) {
			addValidationResult(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_PUNTAJE_Z_IMC_PARA_EDAD,
					ValidationResultCatalogConstants.CODIGO_ERROR_DAT_ANT_PUNTAJE_Z_IMC_EDAD_SEXO_INCORRECTO);
		}

		if (imcHasErrors) {
			addValidationResult(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_PUNTAJE_Z_IMC_PARA_EDAD,
					ValidationResultCatalogConstants.CODIGO_ERROR_DAT_ANT_PUNTAJE_Z_IMC_EDAD_IMC_ERROR);
		}

		if (valuesNotDefined) {
			addValidationResult(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_PUNTAJE_Z_IMC_PARA_EDAD,
					ValidationResultCatalogConstants.CODIGO_INFO_DAT_ANT_PUNTAJE_Z_IMC_EDAD_IMC_NO_DEFINIDO);
		}

		if ((edadAnioMesDias == null || edadEnMeses == null || edadEnDias == null) || sexo == null || imcHasErrors
				|| valuesNotDefined) {
			return RuleState.BREAK;
		}

		try {
			Double scoreZIMCparaEdad = scoreZAndCategoriaService.calcularScoreZIMCparaEdad(Double.parseDouble(imc),
					edadAnioMesDias, edadEnMeses, edadEnDias, sexo);
			puntajeZIMCParaEdad = scoreZIMCparaEdad.toString();
		} catch (PercentilNotFoundException | PercentilDetalleNotFoundException
				| SignosVitalesReglasValidacionNotFoundException e) {
			addValidationResult(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_PUNTAJE_Z_IMC_PARA_EDAD,
					ValidationResultCatalogConstants.CODIGO_INFO_DAT_ANT_PUNTAJE_Z_IMC_EDAD_NO_APLICA);

			return RuleState.BREAK;
		}

		setRdacaaRawRowResultValue(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_PUNTAJE_Z_IMC_PARA_EDAD,
				puntajeZIMCParaEdad);

		addValidationResult(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_PUNTAJE_Z_IMC_PARA_EDAD,
				ValidationResultCatalogConstants.CODIGO_INFO_DAT_ANT_PUNTAJE_Z_IMC_EDAD_VALOR);

		return RuleState.NEXT;
	}
}

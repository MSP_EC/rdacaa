package ec.gob.msp.rdacaa.business.validation.rule.vih.pruebauno;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;

/**
*
* @author dmurillo
*/
@Rule(order = 5)
public class PruebaUnoIsNotMandatory extends BaseRule<String> {

	@When
	public boolean when() {
		String prueba = getVariableFromMap(RdacaaVariableKeyCatalog.VIH_CODIGO_PRIMERA_PRUEBA);
		return (!isMandatory(RdacaaVariableKeyCatalog.VIH_CODIGO_PRIMERA_PRUEBA) &&
				ValidationSupport.isNotDefined(prueba));
		
	}

	@Then
	public RuleState then() {
		return RuleState.BREAK;
	}
}

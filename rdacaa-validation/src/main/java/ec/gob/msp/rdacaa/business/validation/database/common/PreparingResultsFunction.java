package ec.gob.msp.rdacaa.business.validation.database.common;

import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.function.Function;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResult;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaRawRowResult;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableValueResult;

@Component
public class PreparingResultsFunction implements Function<Object, List<String[]>> {
	
	private static final Logger logger = LoggerFactory.getLogger(PreparingResultsFunction.class);

	@SuppressWarnings("unchecked")
	@Override
	public List<String[]> apply(Object exportResults) {

		List<Optional<RdacaaRawRowResult>> listaOutputs = (List<Optional<RdacaaRawRowResult>>) exportResults;

		List<String[]> resultToPersist = new ArrayList<>();

		for (Optional<RdacaaRawRowResult> resultOptional : listaOutputs) {

			if (resultOptional.isPresent()) {

				RdacaaRawRowResult result = resultOptional.get();

				String uuidPersona = result.containsKey(RdacaaVariableKeyCatalog.PERSONA_CODIGO_UUID)
						? (String) result.get(RdacaaVariableKeyCatalog.PERSONA_CODIGO_UUID).getValue()
						: "";
				String uuidAtencion = result.containsKey(RdacaaVariableKeyCatalog.ATENCION_CODIGO_UUID)
						? (String) result.get(RdacaaVariableKeyCatalog.ATENCION_CODIGO_UUID).getValue()
						: "";

				result.entrySet().forEach((Entry<String, RdacaaVariableValueResult> e) -> {

					String key = e.getKey();
					logger.debug("Saving key {}",key);

					RdacaaVariableValueResult rvr = e.getValue();

					Object value = rvr.getValue();
					logger.debug("Saving value {}",value);

					List<ValidationResult> validationResultList = rvr.getValidationResultList();

					validationResultList.forEach(vr -> {

						String[] resultadoValidacion = new String[6];
						if (!isVariablePersona(key)) {
							resultadoValidacion[0] = uuidAtencion;
						} else {
							resultadoValidacion[1] = uuidPersona;
						}

						resultadoValidacion[2] = key;
						resultadoValidacion[3] = value == null ? null : value.toString();
						resultadoValidacion[4] = vr.getCode();
						resultadoValidacion[5] = vr.getMessage();

						resultToPersist.add(resultadoValidacion);
					});

				});
			}
		}

		return resultToPersist;
	}

	private boolean isVariablePersona(String key) {
		return key.equals(RdacaaVariableKeyCatalog.PERSONA_CODIGO_UUID)
				|| key.equals(RdacaaVariableKeyCatalog.PERSONA_NUMERO_ARCHIVO)
				|| key.equals(RdacaaVariableKeyCatalog.PERSONA_CODIGO_TIPO_IDENTIFICACION)
				|| key.equals(RdacaaVariableKeyCatalog.PERSONA_IDENTIFICACION)
				|| key.equals(RdacaaVariableKeyCatalog.PERSONA_FECHA_NACIMIENTO)
				|| key.equals(RdacaaVariableKeyCatalog.PERSONA_PRIMER_NOMBRE)
				|| key.equals(RdacaaVariableKeyCatalog.PERSONA_SEGUNDO_NOMBRE)
				|| key.equals(RdacaaVariableKeyCatalog.PERSONA_PRIMER_APELLIDO)
				|| key.equals(RdacaaVariableKeyCatalog.PERSONA_SEGUNDO_APELLIDO)
				|| key.equals(RdacaaVariableKeyCatalog.PERSONA_CODIGO_SEXO)
				|| key.equals(RdacaaVariableKeyCatalog.PERSONA_CODIGO_ORIENTACION_SEXUAL)
				|| key.equals(RdacaaVariableKeyCatalog.PERSONA_CODIGO_IDENTIFICACION_GENERO)
				|| key.equals(RdacaaVariableKeyCatalog.PERSONA_CODIGO_NACIONALIDAD)
				|| key.equals(RdacaaVariableKeyCatalog.PERSONA_CODIGO_AUTOIDENTIFICACION_ETNICA)
				|| key.equals(RdacaaVariableKeyCatalog.PERSONA_CODIGO_NACIONALIDAD_ETNICA)
				|| key.equals(RdacaaVariableKeyCatalog.PERSONA_CODIGO_PUEBLO)
				|| key.equals(RdacaaVariableKeyCatalog.PERSONA_CODIGO_SEGURO)
				|| key.equals(RdacaaVariableKeyCatalog.PERSONA_CODIGO_TIPO_BONO)
				|| key.equals(RdacaaVariableKeyCatalog.PERSONA_CODIGO_PARROQUIA)
				|| key.equals(RdacaaVariableKeyCatalog.PERSONA_TELEFONO_PACIENTE)
				|| key.equals(RdacaaVariableKeyCatalog.PERSONA_TELEFONO_FAMILIAR)
				|| key.equals(RdacaaVariableKeyCatalog.PERSONA_DIRECCION_DOMICILIO)
				|| key.equals(RdacaaVariableKeyCatalog.PERSONA_CODIGO_TIPO_IDENTIFICACION_REPRESENTANTE)
				|| key.equals(RdacaaVariableKeyCatalog.PERSONA_IDENTIFICACION_REPRESENTANTE);
	}

}

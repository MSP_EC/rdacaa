package ec.gob.msp.rdacaa.business.validation;

import java.util.ArrayList;
import java.util.List;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResult;
import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalog;
import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.common.Validator;

public class PrimerNombreValidator extends ValidationSupport implements Validator<String> {

	private static final int TRES_CARACTERES = 3;

	public List<ValidationResult> validate(String nombre) {
		
		List<ValidationResult> listaErrores = new ArrayList<>();
		
		if(isNullOrEmptyString(nombre)) {
			listaErrores.add(ValidationResultCatalog.NOMBRE_VACIO_O_NULL.getValidationError());
		} else if(isStringLessThanXCharacters(nombre,TRES_CARACTERES)) {
			listaErrores.add(ValidationResultCatalog.NOMBRE_MENOR_TRES_CARACTERES.getValidationError());
		}
                
		
		return listaErrores;
	}
        
}

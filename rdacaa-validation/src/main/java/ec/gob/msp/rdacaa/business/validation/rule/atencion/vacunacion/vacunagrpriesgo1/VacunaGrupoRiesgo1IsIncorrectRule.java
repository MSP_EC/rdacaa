package ec.gob.msp.rdacaa.business.validation.rule.atencion.vacunacion.vacunagrpriesgo1;

import org.apache.commons.lang3.StringUtils;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.UtilsRule;

@Rule(order = 6)
public class VacunaGrupoRiesgo1IsIncorrectRule extends UtilsRule {

	@When
	public boolean when() {
		return true;
	}

	@Then
	public RuleState then() {

		String vacuna1Id = getVariableFromMap(RdacaaVariableKeyCatalog.VACUNAS_CODIGO_APLICACION_1);
		String vacunaGrupoRiesgo1 = getVariableFromMap(RdacaaVariableKeyCatalog.VACUNAS_CODIGO_GRUPO_RIESGO_1);

		boolean vacuna1IdHasErrors = variableHasErrors(RdacaaVariableKeyCatalog.VACUNAS_CODIGO_APLICACION_1);
		
		if(vacuna1IdHasErrors || ValidationSupport.isNotDefined(vacunaGrupoRiesgo1)) {
			return RuleState.NEXT;
		}
		
		if (!(StringUtils.isNumeric(vacuna1Id) && StringUtils.isNumeric(vacunaGrupoRiesgo1))) {
			addValidationResult(RdacaaVariableKeyCatalog.VACUNAS_CODIGO_GRUPO_RIESGO_1,
					ValidationResultCatalogConstants.CODIGO_ERROR_VACUNAS_CODIGO_GRUPO_RIESGO_1_COD_VACUNA_DOSIS_NUMERICOS);
			return RuleState.BREAK;
		}
		
		boolean isHombre = "M".equals(getSexoString());
		boolean isMujer = "F".equals(getSexoString());
		boolean isIntersexual = "I".equals(getSexoString());
		
		if(getSexoString() == null) {
			addValidationResult(RdacaaVariableKeyCatalog.VACUNAS_CODIGO_GRUPO_RIESGO_1,
					ValidationResultCatalogConstants.CODIGO_ERROR_VACUNAS_CODIGO_GRUPO_RIESGO_1_SEXO_INCORRECTO);
			return RuleState.BREAK;
		}
		
		boolean vacunaHasGrupoRiesgo = validationQueryService.vacunaHasGrupoRiesgo(vacuna1Id, isHombre, isMujer, isIntersexual);
		
		if(vacunaHasGrupoRiesgo && !ValidationSupport.isNotDefined(vacunaGrupoRiesgo1)) {
			if(!validationQueryService.isGrupoRiesgoValido(vacuna1Id, vacunaGrupoRiesgo1, isHombre, isMujer, isIntersexual)) {
				addValidationResult(RdacaaVariableKeyCatalog.VACUNAS_CODIGO_GRUPO_RIESGO_1,
						ValidationResultCatalogConstants.CODIGO_ERROR_VACUNAS_CODIGO_GRUPO_RIESGO_1_DEBE_TENER_GRUPO_RIESGO_VALIDO);
				return RuleState.BREAK;
			}else {
				return RuleState.NEXT;
			}
		} else if(!vacunaHasGrupoRiesgo && !ValidationSupport.isNotDefined(vacunaGrupoRiesgo1)) {
			addValidationResult(RdacaaVariableKeyCatalog.VACUNAS_CODIGO_GRUPO_RIESGO_1,
					ValidationResultCatalogConstants.CODIGO_ERROR_VACUNAS_CODIGO_GRUPO_RIESGO_1_DEBE_TENER_GRUPO_RIESGO_NO_DEFINIDO);
			return RuleState.BREAK;
		} else if(!vacunaHasGrupoRiesgo && ValidationSupport.isNotDefined(vacunaGrupoRiesgo1)) {
			return RuleState.NEXT;
		} else if(vacunaHasGrupoRiesgo && ValidationSupport.isNotDefined(vacunaGrupoRiesgo1)) {
			addValidationResult(RdacaaVariableKeyCatalog.VACUNAS_CODIGO_GRUPO_RIESGO_1,
					ValidationResultCatalogConstants.CODIGO_ERROR_VACUNAS_CODIGO_GRUPO_RIESGO_1_DEBE_TENER_GRUPO_RIESGO_VALIDO);
			return RuleState.BREAK;
		}
		
		return RuleState.NEXT;
	}
}

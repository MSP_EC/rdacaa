package ec.gob.msp.rdacaa.business.validation.rule.persona.identidadgenero;

import org.apache.commons.lang3.StringUtils;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;

/**
*
* @author dmurillo
*/
@Rule(order = 6)
public class IdentidadGeneroIsNumericaRule extends BaseRule<String> {
	@When
	public boolean when() {
		String identidadGenero = getVariableFromMap(RdacaaVariableKeyCatalog.PERSONA_CODIGO_IDENTIFICACION_GENERO);
		return !StringUtils.isNumeric(identidadGenero);
	}

	@Then
	public RuleState then() {
		addValidationResult(RdacaaVariableKeyCatalog.PERSONA_CODIGO_IDENTIFICACION_GENERO,
				ValidationResultCatalogConstants.CODIGO_ERROR_IDENTIFICACIONGENERO_NONUMERICA);
		return RuleState.BREAK;
	}
}

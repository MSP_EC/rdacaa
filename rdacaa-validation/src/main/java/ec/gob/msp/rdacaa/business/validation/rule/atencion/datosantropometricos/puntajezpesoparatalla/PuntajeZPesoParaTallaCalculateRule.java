package ec.gob.msp.rdacaa.business.validation.rule.atencion.datosantropometricos.puntajezpesoparatalla;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Given;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.service.ScoreZAndCategoriaService;
import ec.gob.msp.rdacaa.business.service.ScoreZAndCategoriaService.PercentilDetalleNotFoundException;
import ec.gob.msp.rdacaa.business.service.ScoreZAndCategoriaService.PercentilNotFoundException;
import ec.gob.msp.rdacaa.business.service.SignosVitalesReglasValidacionNotFoundException;
import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.UtilsRule;

@Rule(order = 6)
public class PuntajeZPesoParaTallaCalculateRule extends UtilsRule {

	@Given("scoreZAndCategoriaService")
	private ScoreZAndCategoriaService scoreZAndCategoriaService;

	private static final String DE_PIE = "0";

	@When
	public boolean when() {
		return true;
	}

	@Then
	public RuleState then() {
		String puntajeZPesoParaTalla = "999990000066666";
		String peso = getVariableFromMap(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_PESO);
		boolean pesoHasErrors = variableHasErrors(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_PESO);
		String tallaCorregida = getRdacaaRawRowResultValue(
				RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_TALLA_CORREGIDA);
		boolean tallaCorregidaHasErrors = variableHasErrors(
				RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_TALLA_CORREGIDA);
		String tipoTomaTalla = getVariableFromMap(
				RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_CODIGO_TIPO_TOMA_MEDIDA_TALLA);
		boolean tipoTomaTallaHasErrors = variableHasErrors(
				RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_CODIGO_TIPO_TOMA_MEDIDA_TALLA);
		Integer edadAnioMesDias = getEdadAnioMesDias();
		Integer edadEnMeses = getEdadMeses();
		Integer edadEnDias = getEdadDias();
		String sexo = getSexoString();

		boolean valuesNotDefined = ValidationSupport.isNotDefined(peso)
				|| ValidationSupport.isNotDefined(tallaCorregida) || ValidationSupport.isNotDefined(tipoTomaTalla);

		setRdacaaRawRowResultValue(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_PUNTAJE_Z_PESO_PARA_TALLA,
				puntajeZPesoParaTalla);

		if (edadAnioMesDias == null || edadEnMeses == null || edadEnDias == null) {
			addValidationResult(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_PUNTAJE_Z_PESO_PARA_TALLA,
					ValidationResultCatalogConstants.CODIGO_ERROR_DAT_ANT_PUNTAJE_Z_PESO_TALLA_FECHAS_FORMATO_INCORRECTO);
		}

		if (tallaCorregidaHasErrors) {
			addValidationResult(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_PUNTAJE_Z_PESO_PARA_TALLA,
					ValidationResultCatalogConstants.CODIGO_ERROR_DAT_ANT_PUNTAJE_Z_PESO_TALLA_TALLA_CORREGIDA_TIENE_ERRORES);
		}

		if (tipoTomaTallaHasErrors) {
			addValidationResult(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_PUNTAJE_Z_PESO_PARA_TALLA,
					ValidationResultCatalogConstants.CODIGO_ERROR_DAT_ANT_PUNTAJE_Z_PESO_TALLA_TIPO_TOMA_TIENE_ERRORES);
		}

		if (sexo == null) {
			addValidationResult(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_PUNTAJE_Z_PESO_PARA_TALLA,
					ValidationResultCatalogConstants.CODIGO_ERROR_DAT_ANT_PUNTAJE_Z_PESO_TALLA_SEXO_INCORRECTO);
		}

		if (pesoHasErrors) {
			addValidationResult(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_PUNTAJE_Z_PESO_PARA_TALLA,
					ValidationResultCatalogConstants.CODIGO_ERROR_DAT_ANT_PUNTAJE_Z_PESO_TALLA_PESO_ERROR);
		}

		if (valuesNotDefined) {
			addValidationResult(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_PUNTAJE_Z_PESO_PARA_TALLA,
					ValidationResultCatalogConstants.CODIGO_INFO_DAT_ANT_PUNTAJE_Z_PESO_TALLA_PESO_TALLA_CORR_TIPO_TOMA_NO_DEFINIDO);
		}

		if ((edadAnioMesDias == null || edadEnMeses == null || edadEnDias == null) || sexo == null
				|| tallaCorregidaHasErrors || tipoTomaTallaHasErrors || pesoHasErrors || valuesNotDefined) {
			return RuleState.BREAK;
		}

		try {
			Double scoreZPesoParaLongitudTalla = scoreZAndCategoriaService.calcularScoreZPesoParaLongitudTalla(
					Double.parseDouble(peso), Double.parseDouble(tallaCorregida), edadAnioMesDias, edadEnMeses,
					edadEnDias, sexo, DE_PIE.equals(tipoTomaTalla));
			puntajeZPesoParaTalla = scoreZPesoParaLongitudTalla.toString();
		} catch (PercentilNotFoundException | PercentilDetalleNotFoundException
				| SignosVitalesReglasValidacionNotFoundException e) {
			addValidationResult(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_PUNTAJE_Z_PESO_PARA_TALLA,
					ValidationResultCatalogConstants.CODIGO_INFO_DAT_ANT_PUNTAJE_Z_PESO_TALLA_NO_APLICA);

			return RuleState.BREAK;
		}

		setRdacaaRawRowResultValue(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_PUNTAJE_Z_PESO_PARA_TALLA,
				puntajeZPesoParaTalla);

		addValidationResult(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_PUNTAJE_Z_PESO_PARA_TALLA,
				ValidationResultCatalogConstants.CODIGO_INFO_DAT_ANT_PUNTAJE_Z_PESO_TALLA_VALOR);

		return RuleState.NEXT;
	}
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.gob.msp.rdacaa.business.validation.rule.atencion.cie10.tipodiagnostico3;

import org.apache.commons.lang3.StringUtils;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.service.ConstantesDetalleCatalogo;
import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;

/**
 *
 * @author miguel.faubla
 */
@Rule(order = 7)
public class TipoDiagnostico3IsIncorrectoRule extends BaseRule<String> {
	@When
	public boolean when() {
		String codigocie3 = getVariableFromMap(RdacaaVariableKeyCatalog.DIAGNOSTICO_CODIGO_CIE_3);
		String tipodiagnostico3 = getVariableFromMap(RdacaaVariableKeyCatalog.DIAGNOSTICO_CODIGO_TIPO_DIAG_3);
		String caracter = codigocie3.trim().toUpperCase().substring(0, 1);

		if (StringUtils.isNumeric(tipodiagnostico3)) {
			if (caracter.equals("Z")
					&& Integer.valueOf(tipodiagnostico3).equals(ConstantesDetalleCatalogo.TIP_DIAG_MORBILIDAD)) {
				return true;
			} else if (!caracter.equals("Z")
					&& Integer.valueOf(tipodiagnostico3).equals(ConstantesDetalleCatalogo.TIP_DIAG_PREVENCION)) {
				return true;
			}

			return !this.validationQueryService.isCodTipoDiagnosticoValido(Integer.valueOf(tipodiagnostico3));
		}
		return true;
	}

	@Then
	public RuleState then() {
		addValidationResult(RdacaaVariableKeyCatalog.DIAGNOSTICO_CODIGO_TIPO_DIAG_3,
				ValidationResultCatalogConstants.CODIGO_ERROR_TIPO_DIAGNOSTICO_3_INCORRECTO);
		return RuleState.BREAK;
	}
}

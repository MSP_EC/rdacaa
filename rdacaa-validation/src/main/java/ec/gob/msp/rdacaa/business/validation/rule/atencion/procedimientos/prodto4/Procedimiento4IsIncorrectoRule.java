package ec.gob.msp.rdacaa.business.validation.rule.atencion.procedimientos.prodto4;

import org.apache.commons.lang3.StringUtils;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;

@Rule(order = 7)
public class Procedimiento4IsIncorrectoRule extends BaseRule<String> {

	@When
	public boolean when() {
		
		String procedimiento4 = getVariableFromMap(RdacaaVariableKeyCatalog.PROCEDIMIENTOS_CODIGO_4);
		if (StringUtils.isNumeric(procedimiento4)) {
			return !this.validationQueryService.isProcedimientoValido(procedimiento4);
		}
		return true;
	}

	@Then
	public RuleState then() {
		addValidationResult(RdacaaVariableKeyCatalog.PROCEDIMIENTOS_CODIGO_4,
				ValidationResultCatalogConstants.CODIGO_ERROR_PROCEDIMIENTO_4_INCORRECTO);
		return RuleState.BREAK;
	}
}

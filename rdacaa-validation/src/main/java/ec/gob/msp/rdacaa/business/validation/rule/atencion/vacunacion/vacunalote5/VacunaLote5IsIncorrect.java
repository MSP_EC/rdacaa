package ec.gob.msp.rdacaa.business.validation.rule.atencion.vacunacion.vacunalote5;

import org.apache.commons.lang3.StringUtils;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;

/**
 *
 * @author miguel.faubla
 */
@Rule(order = 6)
public class VacunaLote5IsIncorrect extends BaseRule<String> {

	@When
	public boolean when() {
		String vacunaLote5 = getVariableFromMap(RdacaaVariableKeyCatalog.VACUNAS_CODIGO_LOTE_5);
		return !StringUtils.isAlphanumeric(vacunaLote5) || ValidationSupport.isStringMoreThanXCharacters(vacunaLote5, 20) ;
	}

	@Then
	public RuleState then() {
		addValidationResult(RdacaaVariableKeyCatalog.VACUNAS_CODIGO_LOTE_5,
				ValidationResultCatalogConstants.CODIGO_ERROR_VACUNAS_CODIGO_LOTE_5_ES_INCORRECTA);
		return RuleState.BREAK;
	}
}

package ec.gob.msp.rdacaa.business.validation.rule.atencion.violencia.parentescoagresor;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;

@Rule(order = 2)
public class ParentescoAgresorIsNullRule extends BaseRule<String> {
	
	@When
	public boolean when() {
		String parentescoAgresor = getVariableFromMap(RdacaaVariableKeyCatalog.VIOLENCIA_PARENTESCO_AGRESOR);
		return ValidationSupport.isNullOrEmptyString(parentescoAgresor);
	}

	@Then
	public RuleState then() {
		addValidationResult(RdacaaVariableKeyCatalog.VIOLENCIA_PARENTESCO_AGRESOR,
				ValidationResultCatalogConstants.CODIGO_ERROR_VIOLENCIA_PARENTESCO_AGRESOR_NULO);
		return RuleState.BREAK;
	}

}

package ec.gob.msp.rdacaa.business.validation.database.common;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import com.google.crypto.tink.KeysetHandle;

import ec.gob.msp.rdacaa.seguridades.util.SeguridadUtil;
import ec.gob.msp.rdacaa.utilitario.exportdatabase.AtencionCipherDto;

@Component
public class CipheringPublicKeyPlainDataFunction implements Function<Object, Object> {

	private static final Logger logger = LoggerFactory.getLogger(CipheringPublicKeyPlainDataFunction.class);
	
	@Autowired
	private Environment env;
	
	private KeysetHandle keysetHandle;
	
	@PostConstruct
	public void init() throws GeneralSecurityException, IOException {
		keysetHandle = SeguridadUtil.leerLlavePublica(env.getProperty("publickey"));
	}
	
	@Override
	public Object apply(Object cipherObjects) {

		List<AtencionCipherDto> listaCipherDto = new ArrayList<>();

		if (cipherObjects instanceof ResultSet) {
			ResultSet rs = (ResultSet) cipherObjects;
			
			try {
				mapToAtencionCipherDto(listaCipherDto, rs);
				decipherVariables(listaCipherDto);
			} catch (SQLException e) {
				throw new CustomSQLException("Error obteniendo datos del ResultSet", e);
			}
		}

		return listaCipherDto;
	}

	private void decipherVariables(List<AtencionCipherDto> listaCipherDto){
		
		listaCipherDto.parallelStream().forEach(dto -> {
			try {
				String kuCident = SeguridadUtil.cifrarPublicoString(dto.getKsCident(),keysetHandle);
				dto.setKuCident(kuCident);
				dto.setKuKeyid(String.valueOf(keysetHandle.getKeysetInfo().getPrimaryKeyId()));
				logger.debug("kuCident => {}", kuCident);
			} catch (GeneralSecurityException e) {
				throw new DecipheringErrorException("Error cifrando el archivo", e);
			}
		});
	}

	private void mapToAtencionCipherDto(List<AtencionCipherDto> listaCipherDto, ResultSet rs) throws SQLException {

		while (rs.next()) {
			AtencionCipherDto dto = new AtencionCipherDto();
			dto.setUuid(rs.getString("COD_UUID"));
			dto.setKsCident(rs.getString("KS_CIDENT"));
			dto.setKuCident(rs.getString("KU_CIDENT"));
			dto.setKuKeyid(rs.getString("KU_KEYID"));
			listaCipherDto.add(dto);
		}
	}

}

package ec.gob.msp.rdacaa.control.swing.model;

import java.util.List;

public abstract class DefaultListModel<T> extends javax.swing.DefaultListModel<T> {


    
    public void addElements(List<T> elements) {
        elements.forEach(this::addElement);
    }

    public void clear() {
        removeAllElements();
    }
}

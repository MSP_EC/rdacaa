package ec.gob.msp.rdacaa.control.swing.personalizado;

import java.awt.Dimension;
import javax.swing.JTree;
import javax.swing.tree.DefaultTreeCellRenderer;

/**
 *
 * @author christian
 */
public class ArbolIess extends JTree {

    private static final long serialVersionUID = 1190168423837482361L;

    public ArbolIess() {
        setBackground(new java.awt.Color(255, 255, 255));
        setForeground(new java.awt.Color(108, 119, 132));
        setFont(new java.awt.Font("Open Sans Semibold", 0, 13));
        DefaultTreeCellRenderer renderArbol;
        Dimension d = new Dimension(250, 25);
        renderArbol = (DefaultTreeCellRenderer) getCellRenderer();
        renderArbol.setBackgroundNonSelectionColor(new java.awt.Color(255, 255, 255));
        renderArbol.setTextNonSelectionColor(new java.awt.Color(108, 119, 132));
        renderArbol.setBackgroundSelectionColor(new java.awt.Color(255, 255, 255));
        renderArbol.setBorder(null);
        renderArbol.setBorderSelectionColor(new java.awt.Color(255, 255, 255));
        renderArbol.setTextSelectionColor(new java.awt.Color(1, 141, 217));
        renderArbol.setPreferredSize(d);
    }

}

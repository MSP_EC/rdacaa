/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.gob.msp.rdacaa.control.swing.personalizado;

import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Insets;
import java.awt.RenderingHints;
import java.awt.geom.Area;
import java.awt.geom.Rectangle2D;
import java.awt.geom.RoundRectangle2D;
import javax.swing.JButton;
import javax.swing.border.AbstractBorder;

/**
 *
 * @author jtarapuez
 */
public class BotonSubirPDF extends JButton {

    Color bgColor;
    Color ColorLetra;
    private Dimension d = new Dimension(270, 30);

    public BotonSubirPDF() {

        setContentAreaFilled(false);
        setPreferredSize(d);
        setVisible(true);

        setFont(new java.awt.Font("Open Sans", 0, 12));
        setForeground(new java.awt.Color(101, 101, 101));

        setText("");
        setOpaque(true);

        bgColor = new java.awt.Color(247, 247, 247);

        setBorder(new RoundedCornerBorder());

        setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/botones/bot_subir_pdf_off.png"))); // NOI18N
        setText("");
        setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        setIconTextGap(12);
        setMargin(new java.awt.Insets(2, 20, 2, 14));
        setRolloverIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/botones/bot_subir_pdf_on.png")));

    }

    protected void paintComponent(Graphics g) {
        Graphics2D g2d = (Graphics2D) g;
        g2d.setColor(bgColor);
        g2d.fillRect(0, 0, getWidth() - 1, getHeight() - 1);
        // g.setColor(getForeground());

        super.paintComponent(g);
    }

    class RoundedCornerBorder extends AbstractBorder {

        @Override
        public void paintBorder(Component c, Graphics g, int x, int y, int width, int height) {
            Graphics2D g2 = (Graphics2D) g.create();
            g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
            int r = height - 1;

            int w = getWidth();
            int h = getHeight();

            RoundRectangle2D round = new RoundRectangle2D.Float(0, 0, w - 1, h - 1, 10, 10);
            Container parent = c.getParent();
            if (parent != null) {

                g2.setColor(parent.getBackground());
                Area corner = new Area(new Rectangle2D.Float(x, y, width, height));
                corner.subtract(new Area(round));
                g2.fill(corner);
            }
            g2.setColor(Color.LIGHT_GRAY);
            g2.draw(round);
            g2.dispose();
        }

        @Override
        public Insets getBorderInsets(Component c) {
            return new Insets(2, 10, 2, 2);
        }

        @Override
        public Insets getBorderInsets(Component c, Insets insets) {
            insets.left = insets.right = 2;

            insets.top = insets.bottom = 2;
            return insets;
        }
    }

}

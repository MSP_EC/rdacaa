package ec.gob.msp.rdacaa.control.swing.model;

import org.jdesktop.swingx.autocomplete.ObjectToStringConverter;

public abstract class CustomObjectToStringConverter extends ObjectToStringConverter {

	@Override
	public String[] getPossibleStringsForItem(Object item) {
		String preferred = getPreferredStringForItem(item);
		boolean isPreferredNull = preferred == null;
		if(!isPreferredNull) {
			String[] splitArray = preferred.split("( )|(/)|(,)|(;)|(-)|(_)|(\\.)|(\\()|(\\))");
			String[] total = new String[splitArray.length + 1];
			total[0] = preferred;
			for (int i = 0; i < splitArray.length; i++) {
				total[i + 1] = splitArray[i].trim();
			}
			return total;
		}
		
		return new String[0];
	}

	public abstract String getPreferredStringForItem(Object item);

}

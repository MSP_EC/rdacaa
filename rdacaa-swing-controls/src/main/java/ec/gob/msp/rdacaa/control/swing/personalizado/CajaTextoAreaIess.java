/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.gob.msp.rdacaa.control.swing.personalizado;

import java.awt.Color;
import java.awt.Insets;
import javax.swing.BorderFactory;
import javax.swing.JTextArea;

import ec.gob.msp.rdacaa.control.swing.utilitario.color.UtilitarioColor;

/**
 *
 * @author christian
 */
public class CajaTextoAreaIess extends JTextArea {

    private static final long serialVersionUID = -7608363946563492338L;

    public CajaTextoAreaIess() {
        setMargin(new Insets(3, 6, 3, 6));
        setFont(new java.awt.Font("Open Sans", 0, 12));
        setForeground(new java.awt.Color(101, 101, 101));
        setOpaque(true);
        javax.swing.BorderFactory.createEmptyBorder(10, 10, 10, 10);
    }

    public void setearBordeCajaRequerida() {
        setBorder(BorderFactory.createLineBorder(UtilitarioColor.CAMPO_REQUERIDO, 1));
    }

    public void setearBordeCajaNormal() {
        setBorder(BorderFactory.createLineBorder(Color.LIGHT_GRAY, 1));
    }

}

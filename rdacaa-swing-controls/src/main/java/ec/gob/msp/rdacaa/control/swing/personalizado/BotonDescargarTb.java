/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.gob.msp.rdacaa.control.swing.personalizado;

import javax.swing.JButton;

/**
 *
 * @author jtarapuez
 */
public class BotonDescargarTb extends JButton {

    public BotonDescargarTb() {
    
    setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/bot_descargar_off.png"))); // NOI18N
      setBorder(null);
        setBorderPainted(false);
        setContentAreaFilled(false);
       setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
       setPreferredSize(new java.awt.Dimension(148, 43));
        setRolloverIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/bot_descargar_on.png"))); // NOI18N
    
    
    }
    
 


}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.gob.msp.rdacaa.control.swing.personalizado;

import javax.swing.JButton;

/**
 *
 * @author jtarapuez
 */
public class BotonLoginIess extends JButton {

    private static final long serialVersionUID = -5510157648133322385L;

    public BotonLoginIess() {
        setIcon((new javax.swing.ImageIcon(getClass().getResource("/imagenes/bot_ingresar_off.png"))));
        setBorderPainted(false);
        setContentAreaFilled(false);
        setPreferredSize(new java.awt.Dimension(330, 52));
        setRolloverIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/bot_ingresar_on.png")));
    }
}

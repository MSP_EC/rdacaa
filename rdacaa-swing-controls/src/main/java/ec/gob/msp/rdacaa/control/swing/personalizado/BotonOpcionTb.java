/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.gob.msp.rdacaa.control.swing.personalizado;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import javax.swing.JButton;

/**
 *
 * @author christian
 */
public class BotonOpcionTb extends JButton {

    private static final long serialVersionUID = 1179364060793553214L;

    public BotonOpcionTb(String texto) {
        super(texto);
        setContentAreaFilled(false);
        setForeground(new java.awt.Color(65, 65, 65));
        setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        setFont(new java.awt.Font("Open Sans", Font.BOLD, 12));
        setIconTextGap(20);
        setRequestFocusEnabled(false);
        setBorderPainted(false);
    }

    protected void paintComponent(Graphics g) {
        Graphics2D g2d = (Graphics2D) g;
        g2d.setColor(Color.WHITE);
        g2d.fillRect(0, 0, getWidth() - 1, getHeight() - 1);
        g.setColor(getForeground());
        super.paintComponent(g);
    }
}

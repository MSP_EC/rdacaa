/*
 * Copyright 2016 INSTITUTO ECUATORIANO DE SEGURIDAD SOCIAL - ECUADOR
 * Todos los derechos reservados
 */
package ec.gob.msp.rdacaa.control.swing.personalizado;

import javax.swing.JButton;

/**
 * <b>
 * Descripción de clase.
 * </b>
 *
 * @author christian
 * <p>
 * [$Author: christian $, $Date: 2016-12-31
 * </p>
 */
public class BotonFirmaElectronica extends JButton {

    public BotonFirmaElectronica() {
        setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/botones/bot_firmar_off.png"))); // NOI18N
        setBorder(null);
        setBorderPainted(false);
        setContentAreaFilled(false);
        setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        setPreferredSize(new java.awt.Dimension(106, 29));
        setRolloverIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/botones/bot_firmar_on.png"))); // NOI18N    }

    }
}

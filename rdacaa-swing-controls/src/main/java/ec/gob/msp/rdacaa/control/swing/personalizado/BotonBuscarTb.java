package ec.gob.msp.rdacaa.control.swing.personalizado;

import javax.swing.JButton;

/**
 *
 * @author jtarapuez
 */
public class BotonBuscarTb extends JButton {

	private static final long serialVersionUID = 2548519051663737696L;

	public BotonBuscarTb() {

		setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/bot_buscar_off.png"))); // NOI18N
		setBorder(null);
		setBorderPainted(false);
		setContentAreaFilled(false);
		setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
		setPreferredSize(new java.awt.Dimension(148, 43));
		setRolloverIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/bot_buscar_on.png"))); // NOI18N

	}

}

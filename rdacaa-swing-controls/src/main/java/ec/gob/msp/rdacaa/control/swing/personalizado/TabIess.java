/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.gob.msp.rdacaa.control.swing.personalizado;

import java.awt.Dimension;
import javax.swing.JTabbedPane;

import ec.gob.msp.rdacaa.control.swing.utilitario.tab.UtilitarioTab;

/**
 *
 * @author christian
 */
public class TabIess extends JTabbedPane {

    private static final long serialVersionUID = -5084980945618147969L;

    public TabIess() {
        this.setPreferredSize(new Dimension(100, 100));
        this.setUI(new UtilitarioTab());
        this.setVisible(true);
        this.setBorder(null);
        this.setForeground(new java.awt.Color(57, 57, 57));
        this.setFont(new java.awt.Font("Open Sans", 0, 12));
    }

}

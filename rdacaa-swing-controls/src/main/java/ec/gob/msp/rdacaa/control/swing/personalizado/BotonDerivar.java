package ec.gob.msp.rdacaa.control.swing.personalizado;

import javax.swing.JButton;

/**
 *
 * @author jtarapuez
 */
public class BotonDerivar extends JButton {

	private static final long serialVersionUID = -663968421594223488L;

	public BotonDerivar() {
		setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/botones/bot_derivacion_off.png")));
		setBorder(null);
		setBorderPainted(false);
		setContentAreaFilled(false);
		setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
		setPreferredSize(new java.awt.Dimension(94, 29));
		setRolloverIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/botones/bot_derivacion_on.png")));
	}

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.gob.msp.rdacaa.control.swing.utilitario.combo;

import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Rectangle;
import javax.swing.DefaultListCellRenderer;
import javax.swing.ImageIcon;

import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JList;
import javax.swing.ListCellRenderer;
import javax.swing.plaf.ComboBoxUI;
import javax.swing.plaf.basic.BasicComboBoxUI;

public class ComboUtilitario extends BasicComboBoxUI {

    private ImageIcon flecha = new ImageIcon(getClass().getResource("/imagenes/flecha2.png"));
    private ImageIcon espacio = new ImageIcon(getClass().getResource("/imagenes/nulo.png"));
    private Color red = new Color(247, 247, 247);
    private Color red1 = Color.BLACK;

    public static ComboBoxUI createUI(JComponent c) {
        return new ComboUtilitario();
    }

    @Override
    protected JButton createArrowButton() {
        JButton basicArrowButton1 = new JButton(flecha);
        return basicArrowButton1;
    }

    @Override
    public void paintCurrentValueBackground(Graphics g, Rectangle bounds, boolean hasFocus) {
        g.setColor(red);
        g.fillRect(bounds.x, bounds.y, bounds.width, bounds.height);
    }

    @Override
    protected ListCellRenderer createRenderer() {
        return new DefaultListCellRenderer() {
            @Override
            public Component getListCellRendererComponent(JList list, Object value, int index,
                    boolean isSelected, boolean cellHasFocus) {
                super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
                list.setSelectionBackground(red);
                list.setSelectionForeground(new Color(125, 125, 125));
                if (isSelected) {
                    setBackground(new Color(17, 157, 233));
                    setForeground(Color.WHITE);
                }
                if (index != -1) {
                    setIcon(espacio);
                }
                return this;
            }
        };
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.gob.msp.rdacaa.control.swing.personalizado;

import javax.swing.Box;
import javax.swing.JLabel;
import javax.swing.JToolBar;

/**
 *
 * @author jtarapuez
 */
public class ToolBarIess extends JToolBar {

    private static final long serialVersionUID = -8873425138909021296L;

    public ToolBarIess(String etiqueta) {
        setBackground(new java.awt.Color(3, 169, 244));

        setBorder(null);
        setForeground(new java.awt.Color(255, 255, 255));
        setRollover(true);
        setPreferredSize(new java.awt.Dimension(100, 41));
        JLabel label = new JLabel();
        label.setFont(new java.awt.Font("Open Sans Semibold", 1, 16)); // NOI18N
        label.setForeground(new java.awt.Color(255, 255, 255));
        label.setText("<html>&nbsp " + etiqueta + "</html>");
        label.setBackground(new java.awt.Color(1, 141, 217));
        label.setOpaque(false);
        add(label);
        add(Box.createHorizontalGlue());
    }
}

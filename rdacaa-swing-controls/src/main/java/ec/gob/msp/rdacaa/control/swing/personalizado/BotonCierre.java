package ec.gob.msp.rdacaa.control.swing.personalizado;

import javax.swing.JButton;

/**
 *
 * @author dmurillo
 */
public class BotonCierre extends JButton {

	private static final long serialVersionUID = -3452594316912489373L;

	public BotonCierre() {
		setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/botones/bot_cierre.png"))); // NOI18N
		setBorder(null);
		setBorderPainted(false);
		setContentAreaFilled(false);
		setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
		setPreferredSize(new java.awt.Dimension(94, 29));
		setRolloverIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/botones/bot_cierre.png"))); // NOI18N
	}

}

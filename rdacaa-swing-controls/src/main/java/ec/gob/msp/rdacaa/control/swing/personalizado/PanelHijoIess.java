/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.gob.msp.rdacaa.control.swing.personalizado;

import javax.swing.JPanel;

/**
 *
 * @author jtarapuez
 */
public class PanelHijoIess extends JPanel {

    private static final long serialVersionUID = 1526089345024933390L;

    public PanelHijoIess(String titulo) {
        setBackground(new java.awt.Color(255, 255, 255));
        setBorder(javax.swing.BorderFactory.createTitledBorder(null, titulo, javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Open Sans", 1, 15), new java.awt.Color(3,169,244))); // NOI18N
    }

    public PanelHijoIess() {
        setBackground(new java.awt.Color(255, 255, 255));
    }
}

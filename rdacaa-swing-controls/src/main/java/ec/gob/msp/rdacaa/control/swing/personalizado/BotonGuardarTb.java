/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.gob.msp.rdacaa.control.swing.personalizado;

import javax.swing.JButton;

/**
 *
 * @author jtarapuez
 */
public class BotonGuardarTb extends JButton {

    private static final long serialVersionUID = -3953783970962678395L;

    public BotonGuardarTb() {
        setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/bot_guardar_off.png"))); // NOI18N
        setBorder(null);
        setBorderPainted(false);
        setContentAreaFilled(false);
        setToolTipText("Guardar");
        setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        setPreferredSize(new java.awt.Dimension(148, 43));
        setRolloverIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/bot_guardar_on.png"))); // NOI18N
    }

}

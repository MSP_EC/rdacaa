package ec.gob.msp.rdacaa.control.swing.personalizado;

import javax.swing.JButton;

/**
 *
 * @author jtarapuez
 */
public class BotonDescargar extends JButton {

	private static final long serialVersionUID = -3886372846479436272L;

	public BotonDescargar() {

		setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/botones/bot_descargar_off.png"))); // NOI18N
		setBorder(null);
		setBorderPainted(false);
		setContentAreaFilled(false);
		setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
		setPreferredSize(new java.awt.Dimension(94, 29));
		setRolloverIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/botones/bot_descargar_on.png"))); // NOI18N

	}

}

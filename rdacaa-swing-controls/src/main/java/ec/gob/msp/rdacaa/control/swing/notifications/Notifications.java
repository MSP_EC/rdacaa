package ec.gob.msp.rdacaa.control.swing.notifications;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResult;
import ec.gob.msp.rdacaa.control.swing.utilitario.color.UtilitarioColor;
import java.awt.Color;
import java.awt.GridLayout;
import java.util.List;
import javax.swing.*;

public class Notifications {
	
	private Notifications() {}

    public static void showFormValidationAlert(List<ValidationResult> listaErrores, String titulo) {
        JOptionPane.showMessageDialog(null,
                getPanelMensajes(listaErrores),
                titulo,
                JOptionPane.ERROR_MESSAGE);
    }

    private static JPanel getPanelMensajes(List<ValidationResult> listaErrores) {
        JPanel panel = new JPanel(new GridLayout(0, 1, 5, 5));
        listaErrores.stream().map((error) -> getLabel(error.getMessage())).forEachOrdered((messageLabel) -> {
            panel.add(messageLabel);
        });

        return panel;
    }

    public static JPanel getPanelMensajesTextArea(List<ValidationResult> listaErrores) {
        JPanel panel = new JPanel(new GridLayout(0, 1, 5, 5));
        listaErrores.stream().map((error) -> getTextArea(error.getMessage(), error.getColor())).forEachOrdered((messageLabel) -> {
            panel.add(messageLabel);
        });

        return panel;
    }

    private static JLabel getLabel(String title) {
        return new JLabel(title);
    }
    
    private static JTextArea getTextArea(String message, String color){
        JTextArea messageTextArea = new JTextArea(message);
        messageTextArea.setLineWrap(true);
        messageTextArea.setEditable(false);
        messageTextArea.setBackground(mapStringToColor(color));
        return messageTextArea;
    }
    
    public static Color mapStringToColor(String colorString){
        Color color = null;
        switch(colorString==null ? "" : colorString){
            case UtilitarioColor.ROJO_STRING : color = UtilitarioColor.ROJO;
                break;
            case UtilitarioColor.NARANJA_STRING : color = UtilitarioColor.NARANJA;
                break;
            case UtilitarioColor.AMARILLO_STRING : color= UtilitarioColor.AMARILLO;
                break;
            case UtilitarioColor.VERDE_STRING : color = UtilitarioColor.VERDE;
                break;
            case UtilitarioColor.BLANCO_STRING : color = Color.WHITE;
            	break;
            default: 
                color = UtilitarioColor.VERDE;
        }
        return color;
    }
    
}

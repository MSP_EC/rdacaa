package ec.gob.msp.rdacaa.control.swing.personalizado;

import javax.swing.JButton;

/**
 *
 * @author jtarapuez
 */
public class BotonAceptarTb extends JButton {

	private static final long serialVersionUID = -775477976756352459L;

	public BotonAceptarTb() {

		setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/bot_aceptar_off.png"))); // NOI18N
		setBorder(null);
		setBorderPainted(false);
		setContentAreaFilled(false);
		setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
		setPreferredSize(new java.awt.Dimension(148, 43));
		setRolloverIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/bot_aceptar_on.png"))); // NOI18N

	}

}

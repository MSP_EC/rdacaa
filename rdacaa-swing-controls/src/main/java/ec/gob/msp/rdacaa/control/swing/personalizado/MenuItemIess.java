/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.gob.msp.rdacaa.control.swing.personalizado;

import javax.swing.JMenuItem;

/**
 *
 * @author jtarapuez
 */
public class MenuItemIess extends JMenuItem {

    private static final long serialVersionUID = -1995702874988260989L;

    public MenuItemIess(String texto) {
        setBackground(new java.awt.Color(255, 255, 255));
        setFont(new java.awt.Font("Open Sans", 0, 12)); // NOI18N
        setForeground(new java.awt.Color(108, 119, 132));
        setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/hijo.png"))); // NOI18N
        setText(texto);
        setBorder(null);
        //setIconTextGap(40);
        setOpaque(true);
    }

}

/*
 * Copyright 2016 INSTITUTO ECUATORIANO DE SEGURIDAD SOCIAL - ECUADOR
 * Todos los derechos reservados
 */
package ec.gob.msp.rdacaa.utilitario.forma;

import ec.gob.msp.rdacaa.business.entity.Cierreatenciones;
import ec.gob.msp.rdacaa.business.entity.Diagnosticoatencion;
import ec.gob.msp.rdacaa.business.entity.Procedimientoatencion;
import ec.gob.msp.rdacaa.business.entity.Registrovacunacion;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.util.ArrayList;
import java.util.List;
import javax.swing.DefaultCellEditor;
import javax.swing.JComboBox;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.table.AbstractTableModel;
import lombok.*;


/**
 * <b>
 * Incluir aqui la descripcion de la clase.
 * </b>
 *
 * @author christian
 * <p>
 * [$Author: christian $, $Date: 31/05/2016]</p>
 */
public @Data class ModeloTabla<T> extends AbstractTableModel {

    private static final long serialVersionUID = 6140701330257202761L;

    private String[] columnas;

    private List<T> listaDatos;
    private String tabla;

    public ModeloTabla(String[] columnas, String tabla) {
        this.columnas = columnas;
        this.listaDatos = new ArrayList();
        this.tabla = tabla;
    }

    public void addFila(List<T> listaDatos) {
        getListaDatos().addAll(listaDatos);
        fireTableDataChanged();
    }

    @Override
    public int getRowCount() {
        return this.getListaDatos().size();
    }

    @Override
    public int getColumnCount() {
        return getColumnas().length;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        switch (getTabla()) {
            
            case "DiagnosticoAtencion":
                return cargarTablaDiagnosticoAtencion(rowIndex, columnIndex);
            case "Cierreatenciones" :  
                return cargarTablaCierreMensual(rowIndex, columnIndex);
            case "Vacunas" :  
                //return cargarTablaVacunas(rowIndex, columnIndex);
            case "Procedimientos" :  
                return cargarTablaProcedimientos(rowIndex, columnIndex);
            default:
                return "";
        }
    }
    
    private Object cargarTablaDiagnosticoAtencion(int rowIndex, int columnIndex) {

        Diagnosticoatencion obj = (Diagnosticoatencion) getListaDatos().get(rowIndex);
        switch (columnIndex) {
            case 0:
                return obj;
            case 1:
                return obj.getCieId().getCodigo();
            case 2:
                return obj.getCieId().getNombre();
            case 3:
                return obj.getCtmorbilidaddiagnosticoId().getDescripcion();
            case 4:     
                return obj.getCtprevenciondiagnosticoId().getDescripcion();
            case 5:
                return (obj.getCtcondiciondiagnositcoId() != null) ? obj.getCtcondiciondiagnositcoId().getDescripcion() : "";
            default:
                return "";                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    
        }
    }
    
    private Object cargarTablaProcedimientos(int rowIndex, int columnIndex){
    
        Procedimientoatencion obj = (Procedimientoatencion) getListaDatos().get(rowIndex);
        switch (columnIndex){
            case 0:
                return obj;
            case 1:
                return obj.getDetalleserviciotarifarioId().getDescripcion();    
            case 2:
                return obj.getNumactividades();
            case 3:
                return "Eliminar";
            default:
                return "";
        }
    }

    
    private Object cargarTablaCierreMensual(int rowIndex, int columnIndex) {
        Cierreatenciones obj = (Cierreatenciones) getListaDatos().get(rowIndex);
        switch (columnIndex) {
            case 0:
                return obj;
            case 1:
                return obj.getAnio();
            case 2:
                return UtilitarioForma.devolverDescripcionMes(obj.getMes());
            case 3:
                return UtilitarioForma.devolverDescripcionEstado(obj.getEstadocierre());
            case 4:
                return obj.getEstadocierre() == 0 ? "exportar" : "";
            default:
                return ""; 
        }
    }
  
    public void ocultaColumnaCodigo(JTable tblDatos) {
        tblDatos.getColumnModel().getColumn(0).setWidth(0);
        tblDatos.getColumnModel().getColumn(0).setMaxWidth(0);
        tblDatos.getColumnModel().getColumn(0).setMinWidth(0);
        tblDatos.getColumnModel().getColumn(0).setPreferredWidth(0);
        tblDatos.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
    }

    public void ocultaColumnaCodigoCheck(JTable tblDatos) {
        tblDatos.getColumnModel().getColumn(0).setWidth(0);
        tblDatos.getColumnModel().getColumn(0).setMaxWidth(0);
        tblDatos.getColumnModel().getColumn(0).setMinWidth(0);
        tblDatos.getColumnModel().getColumn(0).setPreferredWidth(0);
        tblDatos.getColumnModel().getColumn(2).setCellEditor(new DefaultCellEditor(new JComboBox()));
        tblDatos.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
    }

    @Override
    public String getColumnName(int col) {
        return getColumnas()[col];
    }
    
     private void resizeColumns(JTable tblDatos, int[] percentages) {
        int total = tblDatos.getWidth();
        for (int i = 1; i < tblDatos.getColumnModel().getColumnCount(); i++) {
            if (percentages.length >= i) {
                int size = total * percentages[i - 1] / 100;
                tblDatos.getColumnModel().getColumn(i).setPreferredWidth(size);
            }
        }
    }
    
    public void definirAnchoColumnas(final JTable tblDatos, final int[] percentages) {
        resizeColumns(tblDatos, percentages);
        tblDatos.addComponentListener(new ComponentAdapter() {
            @Override
            public void componentResized(ComponentEvent e) {
                resizeColumns(tblDatos, percentages);
            }
        });
    }

}

/*
* Copyright 2016 INSTITUTO ECUATORIANO DE SEGURIDAD SOCIAL - ECUADOR
* Todos los derechos reservados
*/
package ec.gob.msp.rdacaa.control.swing.utilitario.color;

import java.awt.Color;

/**
 *
 * @author aalvarezvi
 */
public class UtilitarioColor {
    
    public final static Color CAMPO_REQUERIDO = new Color(238, 81, 81);
    
    public final static Color ROJO = new Color(219, 106, 68);

    public final static Color NARANJA = new Color(247, 161, 40);

    public final static Color AMARILLO = new Color(254,224,116);

    public final static Color VERDE =  new Color(39, 247, 119);
    
    public final static String ROJO_STRING = "rojo";

    public final static String NARANJA_STRING = "naranja";

    public final static String AMARILLO_STRING = "amarillo";

    public final static String VERDE_STRING = "verde";
    
    public final static String BLANCO_STRING = "blanco";
    
}

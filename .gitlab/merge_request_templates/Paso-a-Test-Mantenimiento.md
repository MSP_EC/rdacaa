<!---
Título del MR → «TEST-Sistema-Issue XX: Brevísima descripción»
Ejemplo → «TEST-PRA-Issue 999: HC de Cardiología en AMED»

Códigos de sistemas: https://git.msp.gob.ec/ProSalud/smt/blob/master/codigos.md
--->

## Objetivo del cambio

- Describir el objetivo del cambio.

### OTRS:
- Nro Ticket - 📎Adjuntar ticket.pdf

### Historias de Usuario:
- Proyecto#Issue (Ej. PRY.2017.05#998)
- Proyecto#Issue (Ej. PRY.2017.05#999)

## Ejecución

### Instrucciones de Respaldo

- N/A 

### Instrucciones Infraestructura

- Seguir los pasos de la siguiente wiki.

[Bundle New install](https://git.msp.gob.ec/Interoperabilidad/Interoperabilidad-Gestion/wikis/servicemix/bundle-new-install)

Para desplegar el siguiente artefacto en los servidores de Apache Servicemix usar los siguientes datos:

- **Nombre:** Nombre_artefacto
- **Version:** Version_artefacto

### Instrucciones Base de Datos

- N/A 

### Instrucciones Aplicación

- N/A

### Instrucciones de Recuperación/Rollback

- Seguir los pasos de la siguiente wiki.

[Bundle New install](https://git.msp.gob.ec/Interoperabilidad/Interoperabilidad-Gestion/wikis/servicemix/bundle-new-install)

Para desplegar el siguiente artefacto en los servidores de Apache Servicemix usar los siguientes datos:

- **Nombre:** Nombre_artefacto
- **Version:** Version_artefacto

### Instrucciones de Post-implementación

- N/A

## Criterios de Aceptación

### Aprobado GIIS
* Revisión de código estático exitosa ([wiki][])
    * Análisis SAST y DAST.
* Pruebas Unitarias cubren los requerimientos ([PU][])
    * [Link de la clase donde están las PU](https://git.msp.gob.ec/Interoperabilidad/SWUtilities/tree/dev/SWUtilities/src/test/java/ec/gob/msp/sw_utilities)
* Pruebas de Integración de todos los requisitos funcionales probados (positivas y negativas) ([PI][])
    * No aplica debido a que se trata de un mantenimiento.
* Cumple con la documentación técnica ([metodología][])
    * Link Documentación Técnica

*NOTA: En caso de no cumplir con algo, el responsable de la GIIS debe poner la justificación como comentario*

### Aprobado GIPTIC
* Cuenta con los insumos necesarios para continuar con el flujo de paso a test.

### Aprobado GIIRSC
* Cuenta con los insumos requeridos para promoverlo al ambiente de pruebas.
 
*NOTA: Las referencias o evidencias deben ser subidas por quien genera el MR. En caso de no cumplir con algo, el responsable de la GIPTIC debe poner la justificación como comentario*

[wiki]: https://git.msp.gob.ec/ProSalud/smt/wikis/mejores-pr%C3%A1cticas/pruebas
[PU]: https://git.msp.gob.ec/ProSalud/smt/wikis/mejores-pr%C3%A1cticas/pruebas#pruebas-unitarias
[PI]: https://git.msp.gob.ec/ProSalud/smt/wikis/mejores-pr%C3%A1cticas/pruebas#pruebas-de-integraci%C3%B3n
[metodología]: https://git.msp.gob.ec/ProSalud/aot/wikis/Metodolog%C3%ADa-Software

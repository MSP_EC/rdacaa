<!---
Título del MR → «PROD-Sistema-Issue XX: Brevísima descripción»
Ejemplo → «PROD-PRA-Issue 999: HC de Cardiología en AMED»

Códigos de sistemas: https://git.msp.gob.ec/ProSalud/smt/blob/master/codigos.md
--->

## Objetivo del cambio

- Describir el objetivo del cambio.

### Historias de Usuario
- Proyecto#Issue (Ej. PRY.2017.05#998)
- Proyecto#Issue (Ej. PRY.2017.05#999)

### Memorando:
- Nro Memorando - 📎Adjuntar memorando.pdf

### OTRS:
- Nro Ticket - 📎Adjuntar ticket.pdf

## Ejecución

### Ventana de mantenimiento

- Fecha y hora inicio: dd/mm/aaaa hh:mm
- Fecha y hora fin estimado: dd/mm/aaaa hh:mm

📎Adjuntar ventana de mantenimiento.

### Instrucciones de Respaldo

- N/A

### Instrucciones Infraestructura

- Seguir los pasos de la siguiente wiki.

[Bundle New install](https://git.msp.gob.ec/Interoperabilidad/Interoperabilidad-Gestion/wikis/servicemix/bundle-new-install)

Para desplegar el siguiente artefacto en los servidores de Apache Servicemix usar los siguientes datos:

- **Nombre:** Nombre_artefacto
- **Version:** Version_artefacto

### Instrucciones Base de Datos

- N/A

### Instrucciones Aplicación

- N/A 

### Instrucciones de Recuperación/Rollback

- Seguir los pasos de la siguiente wiki.

[Bundle New install](https://git.msp.gob.ec/Interoperabilidad/Interoperabilidad-Gestion/wikis/servicemix/bundle-new-install)

Para desplegar el siguiente artefacto en los servidores de Apache Servicemix usar los siguientes datos:

- **Nombre:** Nombre_artefacto
- **Version:** Version_artefacto

### Instrucciones de Post-implementación

- Crear el siguiente TAG relacionado a este versionamiento:
    * TAG: v1.0.0
    * Describir funcionalidad incorporada en el versionamiento.

## Criterios de Aceptación

* Merge Request - Paso a Test:
    * Link

### Aprobado GIIS
* Pruebas de carga y estrés.
    * No aplica, debido a que se trata de un mantenimiento.
* Memorando de paso a producción
    * 📎Adjuntar memorando

*NOTA: En caso de no cumplir con algo, el responsable de la GIIS debe poner la justificación como comentario*

### Aprobado RESATI (Responsable de Seguridad del Área de Tecnologías de la Información)
* Pruebas de Seguridad
    * Al ser un paso a producción de un web service, no se realiza análisis de código dinámico (DAST) ya que no se cuenta con inferza web, únicamente se realiza el análsis de código estático (SAST).

*NOTA: En caso de no cumplir, el RESATI debe poner la exepción con una justificación o negar el MR según corresponda, como un comentario*

### Aprobado GIPTIC
* Cuenta con los insumos necesarios para continuar con el flujo de paso a producción.

### Aprobado GIIRSC
* Cuenta con los insumos requeridos para promoverlo al ambiente de producción

*NOTA: Las referencias o evidencias deben ser subidas por quien genera el MR. En caso de no cumplir con algo, el responsable de la GIPTIC debe poner la justificación como comentario*

[wiki]: https://git.msp.gob.ec/ProSalud/smt/wikis/mejores-pr%C3%A1cticas/pruebas
[PU]: https://git.msp.gob.ec/ProSalud/smt/wikis/mejores-pr%C3%A1cticas/pruebas#pruebas-unitarias
[PI]: https://git.msp.gob.ec/ProSalud/smt/wikis/mejores-pr%C3%A1cticas/pruebas#pruebas-de-integraci%C3%B3n
[UAT]: https://git.msp.gob.ec/ProSalud/smt/wikis/mejores-pr%C3%A1cticas/pruebas#pruebas-de-sistema-funcionales

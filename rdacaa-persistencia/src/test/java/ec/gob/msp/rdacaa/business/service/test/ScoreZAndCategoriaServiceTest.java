package ec.gob.msp.rdacaa.business.service.test;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import ec.gob.msp.rdacaa.business.service.ScoreZAndCategoriaService;
import ec.gob.msp.rdacaa.business.service.SignosVitalesReglasValidacion;
import ec.gob.msp.rdacaa.test.PersistenciaTestContext;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = PersistenciaTestContext.class)
public class ScoreZAndCategoriaServiceTest {

	private static final Logger logger = LoggerFactory.getLogger(ScoreZAndCategoriaServiceTest.class);

	@Autowired
	private ScoreZAndCategoriaService scoreZAndCategoriaService;

	@Test
	public void whenInputsDados_thenCalcularScoreZTallaParaEdad() {
		Double talla = 80.7d;
		Integer edadAniosMesDias = 10816;
		Integer edadEnMeses = 20;
		Integer edadEnDias = 624;
		String sexo = "M";

		Double z = scoreZAndCategoriaService.calcularScoreZTallaParaEdad(talla, edadAniosMesDias, edadEnMeses, edadEnDias, sexo);

		Assert.assertTrue("Se espera ScoreZTallaParaEdad igual a -1.40 pero fue " + z, z.equals(-1.40d));
	}

	@Test
	public void whenInputsDados_thenCalcularScoreZPesoParaEdad() {
		Double peso = 15.0d;
		Integer edadAniosMesDias = 10816;
		Integer edadEnMeses = 20;
		Integer edadEnDias = 624;
		String sexo = "M";

		Double z = scoreZAndCategoriaService.calcularScoreZPesoParaEdad(peso, edadAniosMesDias, edadEnMeses, edadEnDias, sexo);

		Assert.assertTrue("Se espera ScoreZPesoParaEdad igual a 2.41 pero fue " + z, z.equals(2.41d));
	}

	@Test
	public void whenInputsDados_TallaDePie_thenCalcularScoreZPesoParaLongitudTalla() {
		Double peso = 15.0d;
		Double talla = 80.7d;
		Integer edadAniosMesDias = 10816;
		Integer edadEnMeses = 20;
		Integer edadEnDias = 624;
		String sexo = "M";

		Double z = scoreZAndCategoriaService.calcularScoreZPesoParaLongitudTalla(peso, talla, edadAniosMesDias, edadEnMeses,
				edadEnDias, sexo, true);

		Assert.assertTrue("Se espera ScoreZPesoParaLongitudTalla igual a 3.93 pero fue " + z, z.equals(3.93d));
	}

	@Test
	public void whenInputsDados_TallaAcostado_thenCalcularScoreZPesoParaLongitudTalla() {
		Double peso = 15.0d;
		Double talla = 80.0d;
		Integer edadAniosMesDias = 10816;
		Integer edadEnMeses = 20;
		Integer edadEnDias = 624;
		String sexo = "M";

		Double z = scoreZAndCategoriaService.calcularScoreZPesoParaLongitudTalla(peso, talla, edadAniosMesDias, edadEnMeses,
				edadEnDias, sexo, false);

		Assert.assertTrue("Se espera ScoreZPesoParaLongitudTalla igual a 4.24 pero fue " + z, z.equals(4.24d));
	}

	@Test
	public void whenInputsDados_Perim_Cefalico_thenCalcularScoreZPerimetroCefalicoParaEdad() {
		Double perimetroCefalico = 50.0d;
		Integer edadAniosMesDias = 10816;
		Integer edadEnMeses = 20;
		Integer edadEnDias = 624;
		String sexo = "M";

		Double z = scoreZAndCategoriaService.calcularScoreZPerimetroCefalicoParaEdad(perimetroCefalico, edadAniosMesDias,
				edadEnMeses, edadEnDias, sexo);

		Assert.assertTrue("Se espera ScoreZPesoParaLongitudTalla igual a 1.66 pero fue " + z, z.equals(1.66d));
	}

	@Test
	public void whenInputsDados_IMC_thenCalcularScoreZIMCparaEdad() {
		Double imc = 23.03d;
		Integer edadAniosMesDias = 10815;
		Integer edadEnMeses = 20;
		Integer edadEnDias = 623;
		String sexo = "M";

		Double z = scoreZAndCategoriaService.calcularScoreZIMCparaEdad(imc, edadAniosMesDias, edadEnMeses, edadEnDias, sexo);

		Assert.assertTrue("Se espera ScoreZPesoParaLongitudTalla igual a 4.44 pero fue " + z, z.equals(4.44d));
	}

	@Test
	public void whenScoreZTallaParaEdad_thenCalcularCategoria() {
		Double z = -1.40d;
		Integer edadAniosMesDias = 10816;
		SignosVitalesReglasValidacion alerta = scoreZAndCategoriaService.calcularCategoriaTallaParaEdad(z, edadAniosMesDias, "M");
		logger.info(alerta.toString());
		Assert.assertTrue("Se espera una alerta 'Normal' pero se tiene " + alerta.getAlerta(),
				alerta.getAlerta().equals("Normal"));
	}

	@Test
	public void whenScoreZPesoParaEdad_thenCalcularCategoria() {
		Double z = 2.41d;
		Integer edadAniosMesDias = 10816;
		SignosVitalesReglasValidacion alerta = scoreZAndCategoriaService.calcularCategoriaPesoParaEdad(z, edadAniosMesDias, "M");
		logger.info(alerta.toString());
		Assert.assertTrue("Se espera una alerta 'Peso elevado para la edad' pero se tiene " + alerta.getAlerta(),
				alerta.getAlerta().equals("Peso elevado para la edad"));
	}

	@Test
	public void whenScoreZPesoParaLongitudTalla_thenCalcularCategoria() {
		Double z = 3.93d;
		Integer edadAniosMesDias = 10816;
		SignosVitalesReglasValidacion alerta = scoreZAndCategoriaService.calcularCategoriaPesoParaLongitudTalla(z,
				edadAniosMesDias, "M");
		logger.info(alerta.toString());
		Assert.assertTrue("Se espera una alerta 'Obesidad' pero se tiene " + alerta.getAlerta(),
				alerta.getAlerta().equals("Obesidad"));
	}

	@Test
	public void whenScoreZIMCparaEdad_thenCalcularCategoria() {
		Double z = 4.44d;
		Integer edadAniosMesDias = 10816;
		SignosVitalesReglasValidacion alerta = scoreZAndCategoriaService.calcularCategoriaIMCparaEdad(z, edadAniosMesDias, "M");
		logger.info(alerta.toString());
		Assert.assertTrue("Se espera una alerta 'Obesidad' pero se tiene " + alerta.getAlerta(),
				alerta.getAlerta().equals("Obesidad"));
	}

	@Test
	public void whenScoreZPerimetroCefalicoParaEdad_thenCalcularCategoria() {
		Double z = 1.66d;
		Integer edadAniosMesDias = 10816;
		SignosVitalesReglasValidacion alerta = scoreZAndCategoriaService.calcularCategoriaPerimetroCefalicoParaEdad(z,
				edadAniosMesDias, "M");
		logger.info(alerta.toString());
		Assert.assertTrue("Se espera una alerta 'Normal' pero se tiene " + alerta.getAlerta(),
				alerta.getAlerta().equals("Normal"));
	}
}

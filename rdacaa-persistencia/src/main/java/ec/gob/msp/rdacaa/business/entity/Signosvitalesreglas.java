package ec.gob.msp.rdacaa.business.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author msp
 */
@Entity
@Table(name = "signosvitalesreglas")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Signosvitalesreglas.findAll", query = "SELECT s FROM Signosvitalesreglas s")})
public class Signosvitalesreglas implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "id", nullable = false)
    private Integer id;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "valorminimo", precision = 16, scale = 2)
    private BigDecimal valorminimo;
    @Column(name = "valormaximo", precision = 16, scale = 2)
    private BigDecimal valormaximo;
    @Column(name = "edadminima", length = 10)
    private String edadminima;
    @Column(name = "edadmaxima", length = 10)
    private String edadmaxima;
    @Column(name = "sexo", length = 2)
    private String sexo;
    @Column(name = "estado")
    private Integer estado;
    @JoinColumn(name = "ctgrupoprioritario_id", referencedColumnName = "id")
    @ManyToOne
    private Detallecatalogo ctgrupoprioritarioId;
    @JoinColumn(name = "ctsignovital_id", referencedColumnName = "id", nullable = false)
    @ManyToOne(optional = false)
    private Detallecatalogo ctsignovitalId;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "signovitalreglaId")
    private List<Signosvitalesalertas> signosvitalesalertasList;

    public Signosvitalesreglas() {
    }

    public Signosvitalesreglas(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public BigDecimal getValorminimo() {
        return valorminimo;
    }

    public void setValorminimo(BigDecimal valorminimo) {
        this.valorminimo = valorminimo;
    }

    public BigDecimal getValormaximo() {
        return valormaximo;
    }

    public void setValormaximo(BigDecimal valormaximo) {
        this.valormaximo = valormaximo;
    }

    public String getEdadminima() {
        return edadminima;
    }

    public void setEdadminima(String edadminima) {
        this.edadminima = edadminima;
    }

    public String getEdadmaxima() {
        return edadmaxima;
    }

    public void setEdadmaxima(String edadmaxima) {
        this.edadmaxima = edadmaxima;
    }

    public String getSexo() {
        return sexo;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }

    public Integer getEstado() {
        return estado;
    }

    public void setEstado(Integer estado) {
        this.estado = estado;
    }

    public Detallecatalogo getCtgrupoprioritarioId() {
        return ctgrupoprioritarioId;
    }

    public void setCtgrupoprioritarioId(Detallecatalogo ctgrupoprioritarioId) {
        this.ctgrupoprioritarioId = ctgrupoprioritarioId;
    }

    public Detallecatalogo getCtsignovitalId() {
        return ctsignovitalId;
    }

    public void setCtsignovitalId(Detallecatalogo ctsignovitalId) {
        this.ctsignovitalId = ctsignovitalId;
    }

    @XmlTransient
    public List<Signosvitalesalertas> getSignosvitalesalertasList() {
        return signosvitalesalertasList;
    }

    public void setSignosvitalesalertasList(List<Signosvitalesalertas> signosvitalesalertasList) {
        this.signosvitalesalertasList = signosvitalesalertasList;
    }

    @Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

    @Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Signosvitalesreglas other = (Signosvitalesreglas) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id)) {
			return false;
		}
		return true;
	}

    @Override
    public String toString() {
        return "ec.gob.msp.rdacaa.business.entity.Signosvitalesreglas[ id=" + id + " ]";
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.gob.msp.rdacaa.business.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

import org.hibernate.annotations.Immutable;

/**
 *
 * @author msp
 */
@Entity
@Immutable
@Table(name = "VARIABLES_PRESCRIPCION", catalog = "", schema = "")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "VariablesPrescripcion.findAll", query = "SELECT v FROM VariablesPrescripcion v")})
public class VariablesPrescripcion implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Column(name = "ID_ATENCION")
    private Integer idAtencion;
    @Column(name = "UUID_ATENCION", length = 2000000000)
    private String uuidAtencion;
    @Column(name = "PRE_SUP_HIE_MULTIVITAMINAS")
    private Integer preSupHieMultivitaminas;
    @Column(name = "PRE_SUP_HIE_MULTIVITAMINAS_DESC", length = 2000000000)
    private String preSupHieMultivitaminasDesc;
    @Column(name = "PRE_SUP_VITAMINA_A")
    private Integer preSupVitaminaA;
    @Column(name = "PRE_SUP_VITAMINA_A_DESC", length = 2000000000)
    private String preSupVitaminaADesc;
    @Column(name = "PRE_SUP_HIE_JARABE")
    private Integer preSupHieJarabe;
    @Column(name = "PRE_SUP_HIE_JARABE_DESC", length = 2000000000)
    private String preSupHieJarabeDesc;
    @Column(name = "PRE_SUP_HIE_ACI_FOLICO")
    private Integer preSupHieAciFolico;
    @Column(name = "PRE_SUP_HIE_ACI_FOLICO_DESC", length = 2000000000)
    private String preSupHieAciFolicoDesc;

    public VariablesPrescripcion() {
    }

    public Integer getIdAtencion() {
        return idAtencion;
    }

    public void setIdAtencion(Integer idAtencion) {
        this.idAtencion = idAtencion;
    }

    public String getUuidAtencion() {
        return uuidAtencion;
    }

    public void setUuidAtencion(String uuidAtencion) {
        this.uuidAtencion = uuidAtencion;
    }

    public Integer getPreSupHieMultivitaminas() {
        return preSupHieMultivitaminas;
    }

    public void setPreSupHieMultivitaminas(Integer preSupHieMultivitaminas) {
        this.preSupHieMultivitaminas = preSupHieMultivitaminas;
    }

    public String getPreSupHieMultivitaminasDesc() {
        return preSupHieMultivitaminasDesc;
    }

    public void setPreSupHieMultivitaminasDesc(String preSupHieMultivitaminasDesc) {
        this.preSupHieMultivitaminasDesc = preSupHieMultivitaminasDesc;
    }

    public Integer getPreSupVitaminaA() {
        return preSupVitaminaA;
    }

    public void setPreSupVitaminaA(Integer preSupVitaminaA) {
        this.preSupVitaminaA = preSupVitaminaA;
    }

    public String getPreSupVitaminaADesc() {
        return preSupVitaminaADesc;
    }

    public void setPreSupVitaminaADesc(String preSupVitaminaADesc) {
        this.preSupVitaminaADesc = preSupVitaminaADesc;
    }

    public Integer getPreSupHieJarabe() {
        return preSupHieJarabe;
    }

    public void setPreSupHieJarabe(Integer preSupHieJarabe) {
        this.preSupHieJarabe = preSupHieJarabe;
    }

    public String getPreSupHieJarabeDesc() {
        return preSupHieJarabeDesc;
    }

    public void setPreSupHieJarabeDesc(String preSupHieJarabeDesc) {
        this.preSupHieJarabeDesc = preSupHieJarabeDesc;
    }

    public Integer getPreSupHieAciFolico() {
        return preSupHieAciFolico;
    }

    public void setPreSupHieAciFolico(Integer preSupHieAciFolico) {
        this.preSupHieAciFolico = preSupHieAciFolico;
    }

    public String getPreSupHieAciFolicoDesc() {
        return preSupHieAciFolicoDesc;
    }

    public void setPreSupHieAciFolicoDesc(String preSupHieAciFolicoDesc) {
        this.preSupHieAciFolicoDesc = preSupHieAciFolicoDesc;
    }
    
}

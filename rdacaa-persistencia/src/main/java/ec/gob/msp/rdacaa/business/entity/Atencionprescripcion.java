package ec.gob.msp.rdacaa.business.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author msp
 */
@Entity
@Table(name = "atencionprescripcion")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Atencionprescripcion.findAll", query = "SELECT a FROM Atencionprescripcion a")})
public class Atencionprescripcion implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id", nullable = false)
    private Integer id;
    @Column(name = "hierrominerales")
    private Integer hierrominerales;
    @Column(name = "vitamina")
    private Integer vitamina;
    @Column(name = "hierrojarabe")
    private Integer hierrojarabe;
    @Column(name = "hierroacidofo")
    private Integer hierroacidofo;
    @JoinColumn(name = "atencion_id", referencedColumnName = "id", nullable = false)
    @ManyToOne(optional = false)
    private Atencionmedica atencionId;

    public Atencionprescripcion() {
    }

    public Atencionprescripcion(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getHierrominerales() {
        return hierrominerales;
    }

    public void setHierrominerales(Integer hierrominerales) {
        this.hierrominerales = hierrominerales;
    }

    public Integer getVitamina() {
        return vitamina;
    }

    public void setVitamina(Integer vitamina) {
        this.vitamina = vitamina;
    }

    public Integer getHierrojarabe() {
        return hierrojarabe;
    }

    public void setHierrojarabe(Integer hierrojarabe) {
        this.hierrojarabe = hierrojarabe;
    }

    public Integer getHierroacidofo() {
        return hierroacidofo;
    }

    public void setHierroacidofo(Integer hierroacidofo) {
        this.hierroacidofo = hierroacidofo;
    }

    public Atencionmedica getAtencionId() {
        return atencionId;
    }

    public void setAtencionId(Atencionmedica atencionId) {
        this.atencionId = atencionId;
    }

    @Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

    @Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Atencionprescripcion other = (Atencionprescripcion) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id)) {
			return false;
		}
		return true;
	}

    @Override
    public String toString() {
        return "ec.gob.msp.rdacaa.business.entity.Atencionprescripcion[ id=" + id + " ]";
    }
    
}

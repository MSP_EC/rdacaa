/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.gob.msp.rdacaa.business.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

import org.hibernate.annotations.Immutable;

/**
 *
 * @author msp
 */
@Entity
@Immutable
@Table(name = "VARIABLES_ESTRATEGIA", catalog = "", schema = "")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "VariablesEstrategia.findAll", query = "SELECT v FROM VariablesEstrategia v")})
public class VariablesEstrategia implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Column(name = "ID_ATENCION")
    private Integer idAtencion;
    @Column(name = "UUID_ATENCION", length = 2000000000)
    private String uuidAtencion;
    @Column(name = "COD_ESTRATEGIA")
    private Integer codEstrategia;
    @Column(name = "COD_ESTRATEGIA_DESC", length = 2000000000)
    private String codEstrategiaDesc;

    public VariablesEstrategia() {
    }

    public Integer getIdAtencion() {
        return idAtencion;
    }

    public void setIdAtencion(Integer idAtencion) {
        this.idAtencion = idAtencion;
    }

    public String getUuidAtencion() {
        return uuidAtencion;
    }

    public void setUuidAtencion(String uuidAtencion) {
        this.uuidAtencion = uuidAtencion;
    }

    public Integer getCodEstrategia() {
        return codEstrategia;
    }

    public void setCodEstrategia(Integer codEstrategia) {
        this.codEstrategia = codEstrategia;
    }

    public String getCodEstrategiaDesc() {
        return codEstrategiaDesc;
    }

    public void setCodEstrategiaDesc(String codEstrategiaDesc) {
        this.codEstrategiaDesc = codEstrategiaDesc;
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.gob.msp.rdacaa.business.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ec.gob.msp.rdacaa.business.entity.Cie;
import ec.gob.msp.rdacaa.business.entity.Detallecatalogo;
import ec.gob.msp.rdacaa.business.entity.Detalleserviciotarifario;
import ec.gob.msp.rdacaa.business.entity.Entidad;
import ec.gob.msp.rdacaa.business.entity.Esquemavacunacion;
import ec.gob.msp.rdacaa.business.entity.Parroquia;
import ec.gob.msp.rdacaa.business.entity.Vacuna;

/**
 *
 * @author dmurillo
 */
@Service
public class ValidationQueryService {

	@Autowired
	private DetallecatalogoService detallecatalogoService;

	@Autowired
	private LugaratencionvalidacionService lugaratencionvalidacionService;

	@Autowired
	private GprioritariovalidacionService gprioritariovalidacionService;

	@Autowired
	private CieService cieService;

	@Autowired
	private ParroquiaService parroquiaService;

	@Autowired
	private EntidadService entidadService;

	@Autowired
	private DetalleserviciotarifarioService detalleserviciotarifarioService;

	@Autowired
	private GvulnerablevalidacionService gvulnerablevalidacionService;

	@Autowired
	private VacunaService vacunaService;

	@Autowired
	private EsquemaVacunacionService esquemaVacunacionService;

	@Autowired
	private PersonaService personaService;
	
	@Autowired
	private GriesgoxvacunaService griesgoxvacunaService;

	public Boolean isCodigoTipoIdentificacionValido(Integer id) {
		return detallecatalogoService.isDetalleCatalogoValido(ConstantesDetalleCatalogo.TIPOS_DE_IDENTIFICACION, id);
	}

	public Boolean isCodigoSexoValido(Integer id, Integer anios, Integer meses, Integer dias) {
		List<Detallecatalogo> sexos;
		Boolean valor = false;
		if (anios <= 1) {
			if (anios == 0) {
				sexos = detallecatalogoService.findAllSexos();
			} else if (anios == 1 && meses == 0 && dias == 0) {
				sexos = detallecatalogoService.findAllSexos();
			} else {
				sexos = detallecatalogoService.findAllSexosNoIntersexual();
			}
		} else {
			sexos = detallecatalogoService.findAllSexosNoIntersexual();
		}
		for (Detallecatalogo sexoItem : sexos) {
			if (sexoItem.getId().equals(id)) {
				valor = true;
				break;
			}
		}
		return valor;
	}

	public Boolean isEvaluateOrientacionSexual(Integer anios, Integer meses, Integer dias) {
		Boolean valor = false;
		if (anios >= ConstantesDetalleCatalogo.EDAD_IG && (meses > 0 || dias > 0)) {
			valor = true;
		}
		return valor;
	}

	public Boolean isCodigoOrientacionSexualValido(Integer id, Integer sexoId, Integer edad) {
		List<Detallecatalogo> orientaciones = detallecatalogoService.findByCatalogoIdSexoIdEdad(sexoId, edad);
		Boolean valor = false;
		for (Detallecatalogo orientacionItem : orientaciones) {
			if (orientacionItem.getId().equals(id)) {
				valor = true;
				break;
			}
		}
		return valor;
	}

	public Boolean isCodigoIdentificacionGeneroValido(Integer id) {
		List<Detallecatalogo> identidades = detallecatalogoService
				.findAllByCatalogoId(ConstantesDetalleCatalogo.IDENTIDAD_DE_GENERO);
		Boolean valor = false;
		for (Detallecatalogo identidadItem : identidades) {
			if (identidadItem.getId().equals(id)) {
				valor = true;
				break;
			}
		}
		return valor;
	}

	public Boolean isNacionalidaValida(Integer id) {
		return detallecatalogoService.isPaisValido(id);
	}

	public Boolean isCodigoEstrategiaMspValido(Integer id) {
		List<Detallecatalogo> estrategias = detallecatalogoService
				.findAllByCatalogoId(ConstantesDetalleCatalogo.ESTRATEGIAMSP);
		Boolean valor = false;
		for (Detallecatalogo estrategiaItem : estrategias) {
			if (estrategiaItem.getId().equals(id)) {
				valor = true;
				break;
			}
		}

		return valor;
	}

	public Boolean isCodigoTipoAtencionValido(Integer id) {
		List<Detallecatalogo> tipoatencion = detallecatalogoService
				.findAllByCatalogoId(ConstantesDetalleCatalogo.TIPO_ATENCION);
		Boolean valor = false;
		for (Detallecatalogo tipoatencionItem : tipoatencion) {
			if (tipoatencionItem.getId().equals(id)) {
				valor = true;
				break;
			}
		}

		return valor;
	}

	public Boolean isValidCodigoLugarAtencionByEdadAndSexo(Integer idLugarAtencion, Integer edadAnios,
			Integer edadMeses, Integer edadDias, Integer sexoId) {
		List<Detallecatalogo> lugarAtencion = lugaratencionvalidacionService.findLugarAtencionFiltrados(edadAnios,
				edadMeses, edadDias, sexoId);
		Boolean valor = false;
		for (Detallecatalogo lugarAtencionItem : lugarAtencion) {
			if (lugarAtencionItem.getId().equals(idLugarAtencion)) {
				valor = true;
				break;
			}
		}

		return valor;
	}

	public Boolean isValidCodigoGrupoPrioritarioByEdadAndSexo(Integer idGrupoPrioritario, Integer edadAnios,
			Integer edadMeses, Integer edadDias, Integer sexoId) {
		List<Detallecatalogo> gruposprioritarios = gprioritariovalidacionService
				.findGruposPrioritariosFiltrados(edadAnios, edadMeses, edadDias, sexoId);
		Boolean valor = false;
		for (Detallecatalogo grupoprioritarioItem : gruposprioritarios) {
			if (grupoprioritarioItem.getId().equals(idGrupoPrioritario)) {
				valor = true;
				break;
			}
		}

		return valor;
	}

	public Boolean isValidCodigoCIE10(Integer dia, Integer mes, Integer anio, Integer sexoId, Integer especialidadId,
			String codCIE) {
		Boolean valor = false;
		List<Cie> diagnosticosCie = cieService.findAllCie10ConsolidadorByEdadAndIdSexoAndIdEspecialidad(dia, mes, anio,
				sexoId, especialidadId);
		for (Cie diagnosticoCieItem : diagnosticosCie) {
			if (diagnosticoCieItem.getCodigo().equals(codCIE)) {
				valor = true;
				break;
			}
		}
		List<Cie> diagnosticosCieNoEsp = cieService.findAllCie10ConsolidadorByEdadAndIdSexoAndNoEspecialidad(dia, mes, anio,
				sexoId);
		for (Cie diagnosticoCieItem : diagnosticosCieNoEsp) {
			if (diagnosticoCieItem.getCodigo().equals(codCIE)) {
				valor = true;
				break;
			}
		}

		return valor;
	}

	public Boolean isCodTipoDiagnosticoValido(Integer id) {
		List<Detallecatalogo> tiposdiagnosticos = detallecatalogoService
				.findAllByCatalogoId(ConstantesDetalleCatalogo.TIPO_DIAGNOSTICO);
		Boolean valor = false;
		for (Detallecatalogo tipodiagnosticoItem : tiposdiagnosticos) {
			if (tipodiagnosticoItem.getId().equals(id)) {
				valor = true;
				break;
			}
		}

		return valor;
	}

	public Boolean isCodCondicionDiagnosticoValido(Integer id) {
		List<Detallecatalogo> condiciondiagnostico = detallecatalogoService
				.findAllByCatalogoId(ConstantesDetalleCatalogo.CONDICION_DIAGNOSTICO);
		Boolean valor = false;
		for (Detallecatalogo condiciondiagnosticoItem : condiciondiagnostico) {
			if (condiciondiagnosticoItem.getId().equals(id)) {
				valor = true;
				break;
			}
		}

		return valor;
	}

	public Boolean isCodTipoAtencionDiagnosticoValido(Integer id) {
		List<Detallecatalogo> tipoatenciondiagnostico = detallecatalogoService
				.findAllByCatalogoId(ConstantesDetalleCatalogo.TIPO_ATENCION_DIAGNOSTICO);
		Boolean valor = false;
		for (Detallecatalogo tipoatenciondiagnosticoItem : tipoatenciondiagnostico) {
			if (tipoatenciondiagnosticoItem.getId().equals(id)) {
				valor = true;
				break;
			}
		}

		return valor;
	}

	public Boolean isParroquiaValido(String code) {
		Boolean valor = false;
		Parroquia parroquia = parroquiaService.findOneByParroquiaByCode(code);
		if (parroquia != null) {
			valor = true;
		}
		return valor;
	}

	public Boolean isIdentidadEtnicaValido(Integer id) {
		return detallecatalogoService.isDetalleCatalogoValido(ConstantesDetalleCatalogo.AUTOIDENTIFICACION_ETNICA, id);
	}

	public Boolean isNacionalidadEtnicaValido(Integer id) {
		Boolean valor = false;
		for (Detallecatalogo detallecatalogo : detallecatalogoService.findAllNacionalidades()) {
			if (id.equals(detallecatalogo.getId())) {
				valor = true;
				break;
			}
		}
		return valor;
	}

	public Boolean isPuebloValido(Integer id) {
		Boolean valor = false;
		for (Detallecatalogo detallecatalogo : detallecatalogoService.findAllPueblosKichwa()) {
			if (id.equals(detallecatalogo.getId())) {
				valor = true;
				break;
			}
		}
		return valor;
	}

	public Boolean isCodigoLesionValido(Integer id) {
		return detallecatalogoService.isDetalleCatalogoValido(ConstantesDetalleCatalogo.LESIONES_OCURRIDAS_AGRESOR, id);
	}

	public Boolean isCodigoIdentificaAgresorValido(Integer id) {
		return detallecatalogoService.isDetalleCatalogoValido(ConstantesDetalleCatalogo.IDENTIFICA_AGRESOR, id);
	}

	public Boolean isCodigoVihMotivoPruebaValido(Integer id) {
		return detallecatalogoService.isDetalleCatalogoValido(ConstantesDetalleCatalogo.MOTIVO_PRUEBA_VIH_ID, id);
	}

	public Boolean isCodigoVihPruebaValido(Integer id) {
		return detallecatalogoService.isDetalleCatalogoValido(ConstantesDetalleCatalogo.PRUEBAS_VIH, id);
	}

	public Boolean isCodigoVihViaTransmisionValido(Integer id) {
		return detallecatalogoService.isDetalleCatalogoValido(ConstantesDetalleCatalogo.VIA_TRANSMISION_VIH_ID, id);
	}

	public Boolean validarRango(Integer minimo, Integer maximo, Integer valor) {
		return (valor >= minimo && valor <= maximo) ? true : false;
	}

	public Boolean isCodigoParentescoAgresorValido(Integer id) {
		return detallecatalogoService.isDetalleCatalogoValido(ConstantesDetalleCatalogo.PARENTESCO_AGRESOR, id);
	}

	public Boolean isCodigoSubsistemaReferenciaValido(Integer id) {
		return detallecatalogoService.isDetalleCatalogoValido(ConstantesDetalleCatalogo.SUBSISTEMA_R_C, id);
	}

	public Boolean isLugarReferenciaValida(Integer id) {
		Boolean valor = false;
		Entidad lugarReferencia = entidadService.findOneByEntidadId(id);
		if (lugarReferencia != null) {
			valor = true;
		}
		return valor;
	}

	public Boolean isCodigoTipoInterconsultaValido(Integer id) {
		return detallecatalogoService.isDetalleCatalogoValido(ConstantesDetalleCatalogo.TIPO_INTERCONSULTA, id);
	}

	public Boolean isProcedimientoValido(String codigoProc) {
		Boolean valor = false;
		Detalleserviciotarifario procedimientos = detalleserviciotarifarioService
				.findOneByDetalleServiciotarifarioCodigo(codigoProc);
		if (procedimientos != null) {
			valor = true;
		}
		return valor;
	}

	public Entidad findOneByEntidadId(Integer entidadID) {
		return entidadService.findOneByEntidadId(entidadID);
	}

	public Boolean isGrupoVulnerableValido(Integer id) {
		return detallecatalogoService.isDetalleCatalogoValido(ConstantesDetalleCatalogo.GRUPOS_VULNERABLES, id);
	}

	public Optional<Vacuna> findVacunaById(String vacunaId) {
		return vacunaService.findById(Integer.parseInt(vacunaId));
	}

	public boolean isEsquemaVacunacionValido(String esquemaId, String vacunaId, String dosisValue) {
		return esquemaVacunacionService.findEsquemaVacunacion(esquemaId, vacunaId, dosisValue).isPresent();
	}

	public Esquemavacunacion verifyApplicationVacunaByEdadAndEsquemaId(Integer dia, Integer mes, Integer anio,
			Integer esquemaId) {
		return esquemaVacunacionService.verifyApplicationVacunaByEdadAndEsquemaId(dia, mes, anio, esquemaId);
	}
	
	public boolean vacunaHasGrupoRiesgo(String vacunaId, Boolean isHombre, Boolean isMujer, Boolean isInterSexual) {
		return griesgoxvacunaService.countAllGriesgoxVacuna(Integer.valueOf(vacunaId), isHombre, isMujer, isInterSexual) != 0;
	}
	
	public boolean isGrupoRiesgoValido(String vacunaId, String grpRiesgoId, Boolean isHombre, Boolean isMujer, Boolean isInterSexual) {
		return griesgoxvacunaService.isGriesgoxVacunaValido(Integer.valueOf(vacunaId),Integer.parseInt(grpRiesgoId), isHombre, isMujer, isInterSexual);
	}

	public Boolean isCodigoTipoBonoValido(Integer id) {
		return detallecatalogoService.isDetalleCatalogoValido(ConstantesDetalleCatalogo.TIPO_BONO_ESTADO, id);
	}

	public Boolean isCodigoSeguroValido(Integer id) {
		return detallecatalogoService.isDetalleCatalogoValido(ConstantesDetalleCatalogo.TIPO_SEGURO_SALUD, id);
	}

	public boolean isResultadoBacteriuriaValido(Integer bacteriuriaId) {
		return detallecatalogoService.isDetalleCatalogoValido(ConstantesDetalleCatalogo.RESULTADO_EXAMEN_BACTERIURIA,
				bacteriuriaId);
	}

	public boolean isIdprofesionalValido(Integer idPras) {
		return personaService.findOneByPersonaIdPras(idPras) != null;
	}

	public boolean isIdEspecialidadValido(Integer especialidadId) {
		return detallecatalogoService.isDetalleCatalogoValido(ConstantesDetalleCatalogo.ESPECIALIDADES_MEDICAS,
				especialidadId);
	}

	public boolean isIdCategorizacionRiesgoObstetricoValido(Integer idcatRiesgoObst) {
		return detallecatalogoService.isDetalleCatalogoValido(ConstantesDetalleCatalogo.CATEG_RIESGO_OBSTETRICO_ID,
				idcatRiesgoObst);
	}

	public boolean isIdMetodosSemanasGestacionValido(Integer idMetodosSemanasGestacion) {
		return detallecatalogoService.isDetalleCatalogoValido(ConstantesDetalleCatalogo.METODOS_SEMANAS_GESTACION,
				idMetodosSemanasGestacion);
	}

	public boolean isGrupoVulnerableValidoPorEdadSexoEmbarazada(Integer grupoVulnerableID, Integer edadAnios,
		Integer edadMeses, Integer edadDias, Integer sexoId, Integer embarazaId) {
		List<Detallecatalogo> resultado = gvulnerablevalidacionService
				.findGruposVulnerablesFiltrados(edadAnios, edadMeses, edadDias, sexoId, embarazaId);
		return resultado.stream().anyMatch(gv -> grupoVulnerableID.equals(gv.getId()));
	}
}

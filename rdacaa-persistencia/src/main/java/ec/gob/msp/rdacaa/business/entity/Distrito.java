package ec.gob.msp.rdacaa.business.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author msp
 */
@Entity
@Table(name = "distrito")
@XmlRootElement
@NamedQueries({ @NamedQuery(name = "Distrito.findAll", query = "SELECT d FROM Distrito d") })
public class Distrito implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
	@Basic(optional = false)
	@Column(name = "id", nullable = false)
	private Integer id;
	@Column(name = "codigo", length = 20)
	private String codigo;
	@Column(name = "descripcion", length = 100)
	private String descripcion;
	@Column(name = "distribucion", length = 255)
	private String distribucion;
	@Column(name = "estado")
	private Integer estado;
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "distritoId")
	private List<Circuito> circuitoList;
	@JoinColumn(name = "zona_id", referencedColumnName = "id", nullable = false)
	@ManyToOne(optional = false)
	private Zona zonaId;

	public Distrito() {
	}

	public Distrito(Integer id) {
		this.id = id;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getDistribucion() {
		return distribucion;
	}

	public void setDistribucion(String distribucion) {
		this.distribucion = distribucion;
	}

	public Integer getEstado() {
		return estado;
	}

	public void setEstado(Integer estado) {
		this.estado = estado;
	}

	@XmlTransient
	public List<Circuito> getCircuitoList() {
		return circuitoList;
	}

	public void setCircuitoList(List<Circuito> circuitoList) {
		this.circuitoList = circuitoList;
	}

	public Zona getZonaId() {
		return zonaId;
	}

	public void setZonaId(Zona zonaId) {
		this.zonaId = zonaId;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Distrito other = (Distrito) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "ec.gob.msp.rdacaa.business.entity.Distrito[ id=" + id + " ]";
	}

}

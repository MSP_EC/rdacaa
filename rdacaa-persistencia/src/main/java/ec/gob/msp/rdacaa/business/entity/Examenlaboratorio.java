package ec.gob.msp.rdacaa.business.entity;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author msp
 */
@Entity
@Table(name = "examenlaboratorio", uniqueConstraints = { @UniqueConstraint(columnNames = { "id" }) })
@XmlRootElement
@NamedQueries({ @NamedQuery(name = "Examenlaboratorio.findAll", query = "SELECT e FROM Examenlaboratorio e") })
public class Examenlaboratorio implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	@Basic(optional = false)
	@Column(name = "id", nullable = false)
	private Integer id;
	@Column(name = "notreponemica")
	private Integer notreponemica;
	@Column(name = "treponemica")
	private Integer treponemica;
	@Column(name = "tratamiento")
	private Integer tratamiento;
	@Column(name = "tratamientoprja")
	private Integer tratamientoprja;
	@JoinColumn(name = "ctresultadoexmn_id", referencedColumnName = "id")
	@ManyToOne
	private Detallecatalogo ctresultadoexmnId;
	@JoinColumn(name = "atencionmedica_id", referencedColumnName = "id", nullable = false)
	@ManyToOne(optional = false)
	private Atencionmedica atencionmedicaId;

	public Examenlaboratorio() {
	}

	public Examenlaboratorio(Integer id) {
		this.id = id;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getNotreponemica() {
		return notreponemica;
	}

	public void setNotreponemica(Integer notreponemica) {
		this.notreponemica = notreponemica;
	}

	public Integer getTreponemica() {
		return treponemica;
	}

	public void setTreponemica(Integer treponemica) {
		this.treponemica = treponemica;
	}

	public Integer getTratamiento() {
		return tratamiento;
	}

	public void setTratamiento(Integer tratamiento) {
		this.tratamiento = tratamiento;
	}

	public Integer getTratamientoprja() {
		return tratamientoprja;
	}

	public void setTratamientoprja(Integer tratamientoprja) {
		this.tratamientoprja = tratamientoprja;
	}

	public Detallecatalogo getCtresultadoexmnId() {
		return ctresultadoexmnId;
	}

	public void setCtresultadoexmnId(Detallecatalogo ctresultadoexmnId) {
		this.ctresultadoexmnId = ctresultadoexmnId;
	}

	public Atencionmedica getAtencionmedicaId() {
		return atencionmedicaId;
	}

	public void setAtencionmedicaId(Atencionmedica atencionmedicaId) {
		this.atencionmedicaId = atencionmedicaId;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Examenlaboratorio other = (Examenlaboratorio) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "ec.gob.msp.rdacaa.business.entity.Examenlaboratorio[ id=" + id + " ]";
	}

}

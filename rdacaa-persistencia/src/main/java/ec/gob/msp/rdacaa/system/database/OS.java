package ec.gob.msp.rdacaa.system.database;

/**
 *
 * @author Saulo Velasco
 */
public enum OS {
	WINDOWS, UNIX, POSIX_UNIX, MAC, OTHER;

	private String version;

	public String getVersion() {
		return version;
	}

}

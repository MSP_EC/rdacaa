package ec.gob.msp.rdacaa.business.repository;

import ec.gob.msp.rdacaa.business.entity.Formafrontend;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.stereotype.Repository;

/**
 * Generated by Spring Data Generator on 21/08/2018
 */
@Repository
public interface FormafrontendRepository extends JpaRepository<Formafrontend, Integer>,
		JpaSpecificationExecutor<Formafrontend>, QuerydslPredicateExecutor<Formafrontend> {

}

package ec.gob.msp.rdacaa.business.service;

import ec.gob.msp.rdacaa.business.entity.Detallecatalogo;
import ec.gob.msp.rdacaa.business.entity.Vacuna;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class VacunaXEsquema {

	private Integer idVacuna;

	private String nombreVacunaXNombreEsquema;

	private Detallecatalogo esquema;

	private Vacuna vacuna;

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((esquema == null) ? 0 : esquema.hashCode());
		result = prime * result + ((idVacuna == null) ? 0 : idVacuna.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		VacunaXEsquema other = (VacunaXEsquema) obj;
		if (esquema == null) {
			if (other.esquema != null)
				return false;
		} else if (!esquema.equals(other.esquema))
			return false;
		if (idVacuna == null) {
			if (other.idVacuna != null)
				return false;
		} else if (!idVacuna.equals(other.idVacuna))
			return false;
		return true;
	}

}

package ec.gob.msp.rdacaa.business.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author msp
 */
@Entity
@Table(name = "vihsida", uniqueConstraints = {
    @UniqueConstraint(columnNames = {"id"})})
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Vihsida.findAll", query = "SELECT v FROM Vihsida v")})
public class Vihsida implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)    
    @Basic(optional = false)
    @Column(name = "id", nullable = false)
    private Integer id;
    @Column(name = "reactivaonouno")
    private Integer reactivaonouno;
    @Column(name = "reactivaonodos")
    private Integer reactivaonodos;
    @Column(name = "cargaviral")
    private Integer cargaviral;
    @Column(name = "cd4")
    private Integer cd4;
    @JoinColumn(name = "atencionmedica_id", referencedColumnName = "id", nullable = false)
    @ManyToOne(optional = false)
    private Atencionmedica atencionmedicaId;
    @JoinColumn(name = "ctmotivoprueba_id", referencedColumnName = "id")
    @ManyToOne
    private Detallecatalogo ctmotivopruebaId;
    @JoinColumn(name = "ctpruebados_id", referencedColumnName = "id")
    @ManyToOne
    private Detallecatalogo ctpruebadosId;
    @JoinColumn(name = "ctpruebauno_id", referencedColumnName = "id")
    @ManyToOne
    private Detallecatalogo ctpruebaunoId;
    @JoinColumn(name = "ctviastransmision_id", referencedColumnName = "id")
    @ManyToOne
    private Detallecatalogo ctviastransmisionId;

    public Vihsida() {
    }

    public Vihsida(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getReactivaonouno() {
		return reactivaonouno;
	}

	public void setReactivaonouno(Integer reactivaonouno) {
		this.reactivaonouno = reactivaonouno;
	}

	public Integer getReactivaonodos() {
		return reactivaonodos;
	}

	public void setReactivaonodos(Integer reactivaonodos) {
		this.reactivaonodos = reactivaonodos;
	}

	public Integer getCargaviral() {
        return cargaviral;
    }

    public void setCargaviral(Integer cargaviral) {
        this.cargaviral = cargaviral;
    }

    public Integer getCd4() {
        return cd4;
    }

    public void setCd4(Integer cd4) {
        this.cd4 = cd4;
    }

    public Atencionmedica getAtencionmedicaId() {
        return atencionmedicaId;
    }

    public void setAtencionmedicaId(Atencionmedica atencionmedicaId) {
        this.atencionmedicaId = atencionmedicaId;
    }

    public Detallecatalogo getCtmotivopruebaId() {
        return ctmotivopruebaId;
    }

    public void setCtmotivopruebaId(Detallecatalogo ctmotivopruebaId) {
        this.ctmotivopruebaId = ctmotivopruebaId;
    }

    public Detallecatalogo getCtpruebadosId() {
        return ctpruebadosId;
    }

    public void setCtpruebadosId(Detallecatalogo ctpruebadosId) {
        this.ctpruebadosId = ctpruebadosId;
    }

    public Detallecatalogo getCtpruebaunoId() {
        return ctpruebaunoId;
    }

    public void setCtpruebaunoId(Detallecatalogo ctpruebaunoId) {
        this.ctpruebaunoId = ctpruebaunoId;
    }

    public Detallecatalogo getCtviastransmisionId() {
        return ctviastransmisionId;
    }

    public void setCtviastransmisionId(Detallecatalogo ctviastransmisionId) {
        this.ctviastransmisionId = ctviastransmisionId;
    }

    @Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

    @Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Vihsida other = (Vihsida) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id)) {
			return false;
		}
		return true;
	}

    @Override
    public String toString() {
        return "ec.gob.msp.rdacaa.business.entity.Vihsida[ id=" + id + " ]";
    }
    
}

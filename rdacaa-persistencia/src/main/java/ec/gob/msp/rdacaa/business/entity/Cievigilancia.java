/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.gob.msp.rdacaa.business.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author msp
 */
@Entity
@Table(name = "cievigilancia", uniqueConstraints = {
    @UniqueConstraint(columnNames = {"id"})})
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Cievigilancia.findAll", query = "SELECT c FROM Cievigilancia c")})
public class Cievigilancia implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "id", nullable = false)
    private Integer id;
    @Column(name = "estado")
    private Integer estado;
    @Column(name = "codigocie", length = 6)
    private String codigocie;
    @JoinColumn(name = "cie_id", referencedColumnName = "id")
    @ManyToOne
    private Cie cieId;
    @JoinColumn(name = "ctcondiciondiagnositco_id", referencedColumnName = "id")
    @ManyToOne
    private Detallecatalogo ctcondiciondiagnositcoId;
    @JoinColumn(name = "ctmorbilidaddiagnostico_id", referencedColumnName = "id")
    @ManyToOne
    private Detallecatalogo ctmorbilidaddiagnosticoId;

    public Cievigilancia() {
    }

    public Cievigilancia(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getEstado() {
        return estado;
    }

    public void setEstado(Integer estado) {
        this.estado = estado;
    }

    public String getCodigocie() {
        return codigocie;
    }

    public void setCodigocie(String codigocie) {
        this.codigocie = codigocie;
    }

    public Cie getCieId() {
        return cieId;
    }

    public void setCieId(Cie cieId) {
        this.cieId = cieId;
    }

    public Detallecatalogo getCtcondiciondiagnositcoId() {
        return ctcondiciondiagnositcoId;
    }

    public void setCtcondiciondiagnositcoId(Detallecatalogo ctcondiciondiagnositcoId) {
        this.ctcondiciondiagnositcoId = ctcondiciondiagnositcoId;
    }

    public Detallecatalogo getCtmorbilidaddiagnosticoId() {
        return ctmorbilidaddiagnosticoId;
    }

    public void setCtmorbilidaddiagnosticoId(Detallecatalogo ctmorbilidaddiagnosticoId) {
        this.ctmorbilidaddiagnosticoId = ctmorbilidaddiagnosticoId;
    }

    @Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

    @Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Cievigilancia other = (Cievigilancia) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

    @Override
    public String toString() {
        return "ec.gob.msp.rdacaa.business.entity.Cievigilancia[ id=" + id + " ]";
    }
    
}

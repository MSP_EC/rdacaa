/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.gob.msp.rdacaa.business.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

import org.hibernate.annotations.Immutable;

/**
 *
 * @author msp
 */
@Entity
@Immutable
@Table(name = "VARIABLES_GRUPOS_PRIORITARIOS", catalog = "", schema = "")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "VariablesGruposPrioritarios.findAll", query = "SELECT v FROM VariablesGruposPrioritarios v")})
public class VariablesGruposPrioritarios implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Column(name = "ID_ATENCION")
    private Integer idAtencion;
    @Column(name = "UUID_ATENCION", length = 2000000000)
    private String uuidAtencion;
    @Column(name = "COD_GPR_PRIORITARIO_1", length = 2000000000)
    private String codGprPrioritario1;
    @Column(name = "COD_GPR_PRIORITARIO_DESC_1", length = 2000000000)
    private String codGprPrioritarioDesc1;
    @Column(name = "COD_GPR_PRIORITARIO_2", length = 2000000000)
    private String codGprPrioritario2;
    @Column(name = "COD_GPR_PRIORITARIO_DESC_2", length = 2000000000)
    private String codGprPrioritarioDesc2;
    @Column(name = "COD_GPR_PRIORITARIO_3", length = 2000000000)
    private String codGprPrioritario3;
    @Column(name = "COD_GPR_PRIORITARIO_DESC_3", length = 2000000000)
    private String codGprPrioritarioDesc3;
    @Column(name = "COD_GPR_PRIORITARIO_4", length = 2000000000)
    private String codGprPrioritario4;
    @Column(name = "COD_GPR_PRIORITARIO_DESC_4", length = 2000000000)
    private String codGprPrioritarioDesc4;
    @Column(name = "COD_GPR_PRIORITARIO_5", length = 2000000000)
    private String codGprPrioritario5;
    @Column(name = "COD_GPR_PRIORITARIO_DESC_5", length = 2000000000)
    private String codGprPrioritarioDesc5;

    public VariablesGruposPrioritarios() {
    }

    public Integer getIdAtencion() {
        return idAtencion;
    }

    public void setIdAtencion(Integer idAtencion) {
        this.idAtencion = idAtencion;
    }

    public String getUuidAtencion() {
        return uuidAtencion;
    }

    public void setUuidAtencion(String uuidAtencion) {
        this.uuidAtencion = uuidAtencion;
    }

    public String getCodGprPrioritario1() {
        return codGprPrioritario1;
    }

    public void setCodGprPrioritario1(String codGprPrioritario1) {
        this.codGprPrioritario1 = codGprPrioritario1;
    }

    public String getCodGprPrioritarioDesc1() {
        return codGprPrioritarioDesc1;
    }

    public void setCodGprPrioritarioDesc1(String codGprPrioritarioDesc1) {
        this.codGprPrioritarioDesc1 = codGprPrioritarioDesc1;
    }

    public String getCodGprPrioritario2() {
        return codGprPrioritario2;
    }

    public void setCodGprPrioritario2(String codGprPrioritario2) {
        this.codGprPrioritario2 = codGprPrioritario2;
    }

    public String getCodGprPrioritarioDesc2() {
        return codGprPrioritarioDesc2;
    }

    public void setCodGprPrioritarioDesc2(String codGprPrioritarioDesc2) {
        this.codGprPrioritarioDesc2 = codGprPrioritarioDesc2;
    }

    public String getCodGprPrioritario3() {
        return codGprPrioritario3;
    }

    public void setCodGprPrioritario3(String codGprPrioritario3) {
        this.codGprPrioritario3 = codGprPrioritario3;
    }

    public String getCodGprPrioritarioDesc3() {
        return codGprPrioritarioDesc3;
    }

    public void setCodGprPrioritarioDesc3(String codGprPrioritarioDesc3) {
        this.codGprPrioritarioDesc3 = codGprPrioritarioDesc3;
    }

    public String getCodGprPrioritario4() {
        return codGprPrioritario4;
    }

    public void setCodGprPrioritario4(String codGprPrioritario4) {
        this.codGprPrioritario4 = codGprPrioritario4;
    }

    public String getCodGprPrioritarioDesc4() {
        return codGprPrioritarioDesc4;
    }

    public void setCodGprPrioritarioDesc4(String codGprPrioritarioDesc4) {
        this.codGprPrioritarioDesc4 = codGprPrioritarioDesc4;
    }

    public String getCodGprPrioritario5() {
        return codGprPrioritario5;
    }

    public void setCodGprPrioritario5(String codGprPrioritario5) {
        this.codGprPrioritario5 = codGprPrioritario5;
    }

    public String getCodGprPrioritarioDesc5() {
        return codGprPrioritarioDesc5;
    }

    public void setCodGprPrioritarioDesc5(String codGprPrioritarioDesc5) {
        this.codGprPrioritarioDesc5 = codGprPrioritarioDesc5;
    }
}

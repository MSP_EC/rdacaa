package ec.gob.msp.rdacaa.business.entity;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author msp
 */
@Entity
@Table(name = "accesoformaespecialidad", uniqueConstraints = { @UniqueConstraint(columnNames = { "id" }) })
@XmlRootElement
@NamedQueries({ @NamedQuery(name = "Accesoformaespecialidad.findAll", query = "SELECT a FROM Accesoformaespecialidad a") })
public class Accesoformaespecialidad implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Basic(optional = false)
	@Column(name = "id", nullable = false)
	private Integer id;
	@Column(name = "orden")
	private Integer orden;
	@Column(name = "estado")
	private Integer estado;
	@JoinColumn(name = "ctespecialidad_id", referencedColumnName = "id", nullable = false)
	@ManyToOne(optional = false)
	private Detallecatalogo ctespecialidadId;
	@JoinColumn(name = "codigoformafrontend_id", referencedColumnName = "id", nullable = false)
	@ManyToOne(optional = false)
	private Formafrontend codigoformafrontendId;

	public Accesoformaespecialidad() {
	}

	public Accesoformaespecialidad(Integer id) {
		this.id = id;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
        
	public Integer getOrden() {
		return orden;
	}

	public void setOrden(Integer orden) {
		this.orden = orden;
	}

	public Integer getEstado() {
		return estado;
	}

	public void setEstado(Integer estado) {
		this.estado = estado;
	}

	public Detallecatalogo getCtespecialidadId() {
		return ctespecialidadId;
	}

	public void setCtespecialidadId(Detallecatalogo ctespecialidadId) {
		this.ctespecialidadId = ctespecialidadId;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Accesoformaespecialidad other = (Accesoformaespecialidad) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "ec.gob.msp.rdacaa.business.entity.Accesoformaespecialidad[ id=" + id + " ]";
	}

	public Formafrontend getCodigoformafrontendId() {
		return codigoformafrontendId;
	}

	public void setCodigoformafrontendId(Formafrontend codigoformafrontendId) {
		this.codigoformafrontendId = codigoformafrontendId;
	}

}

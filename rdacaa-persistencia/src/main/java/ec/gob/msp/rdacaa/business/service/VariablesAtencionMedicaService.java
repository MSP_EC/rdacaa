package ec.gob.msp.rdacaa.business.service;

import ec.gob.msp.rdacaa.business.entity.VariablesAtencionMedica;
import ec.gob.msp.rdacaa.business.repository.VariablesAtencionMedicaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
* Generated by Spring Data Generator on 05/10/2018
*/
@Component
public class VariablesAtencionMedicaService {

	private VariablesAtencionMedicaRepository variablesAtencionMedicaRepository;

	@Autowired
	public VariablesAtencionMedicaService(VariablesAtencionMedicaRepository variablesAtencionMedicaRepository) {
		this.variablesAtencionMedicaRepository = variablesAtencionMedicaRepository;
	}

}

package ec.gob.msp.rdacaa.utilitario.exportdatabase;

import java.io.IOException;
import java.io.Serializable;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

@Service
public class ExportSQLiteDatabase implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Autowired
	private Environment env;

	public void processExport(String pathDestino, Object... params) throws IOException {
		SQLiteUtil.sqlBaseOrigenToBaseDestino(env.getProperty("originExportDatabase"), pathDestino,
				env.getProperty("sqlExportPersona"), env.getProperty("sqlExportAtencion"), params);
	}
}

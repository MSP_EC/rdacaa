/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.gob.msp.rdacaa.business.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

import org.hibernate.annotations.Immutable;

/**
 *
 * @author msp
 */
@Entity
@Immutable
@Table(name = "VARIABLES_LUGAR_TIPO", catalog = "", schema = "")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "VariablesLugarTipo.findAll", query = "SELECT v FROM VariablesLugarTipo v")})
public class VariablesLugarTipo implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Column(name = "ID_ATENCION")
    private Integer idAtencion;
    @Column(name = "UUID_ATENCION", length = 2000000000)
    private String uuidAtencion;
    @Column(name = "COD_EST_LUG_ATENCION")
    private Integer codEstLugAtencion;
    @Column(name = "COD_EST_LUG_ATENCION_DESC", length = 2000000000)
    private String codEstLugAtencionDesc;
    @Column(name = "COD_EST_TIP_ATENCION")
    private Integer codEstTipAtencion;
    @Column(name = "COD_EST_TIP_ATENCION_DESC", length = 2000000000)
    private String codEstTipAtencionDesc;

    public VariablesLugarTipo() {
    }

    public Integer getIdAtencion() {
        return idAtencion;
    }

    public void setIdAtencion(Integer idAtencion) {
        this.idAtencion = idAtencion;
    }

    public String getUuidAtencion() {
        return uuidAtencion;
    }

    public void setUuidAtencion(String uuidAtencion) {
        this.uuidAtencion = uuidAtencion;
    }

    public Integer getCodEstLugAtencion() {
        return codEstLugAtencion;
    }

    public void setCodEstLugAtencion(Integer codEstLugAtencion) {
        this.codEstLugAtencion = codEstLugAtencion;
    }

    public String getCodEstLugAtencionDesc() {
        return codEstLugAtencionDesc;
    }

    public void setCodEstLugAtencionDesc(String codEstLugAtencionDesc) {
        this.codEstLugAtencionDesc = codEstLugAtencionDesc;
    }

    public Integer getCodEstTipAtencion() {
        return codEstTipAtencion;
    }

    public void setCodEstTipAtencion(Integer codEstTipAtencion) {
        this.codEstTipAtencion = codEstTipAtencion;
    }

    public String getCodEstTipAtencionDesc() {
        return codEstTipAtencionDesc;
    }

    public void setCodEstTipAtencionDesc(String codEstTipAtencionDesc) {
        this.codEstTipAtencionDesc = codEstTipAtencionDesc;
    }
    
}

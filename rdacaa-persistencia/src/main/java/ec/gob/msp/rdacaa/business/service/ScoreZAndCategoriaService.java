package ec.gob.msp.rdacaa.business.service;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ec.gob.msp.rdacaa.business.entity.Percentil;
import ec.gob.msp.rdacaa.business.entity.Percentildetalle;
import ec.gob.msp.rdacaa.utilitario.medico.UtilitarioMedico;

@Service
public class ScoreZAndCategoriaService {

	private PercentildetalleService percentildetalleService;

	private PercentilService percentilService;

	private SignosvitalesreglasService signosvitalesreglasService;

	private List<SignosVitalesReglasValidacion> reglasPesoParaEdad;

	private List<SignosVitalesReglasValidacion> reglasTallaParaEdad;

	private List<SignosVitalesReglasValidacion> reglasPesoParaLongitudTalla;

	private List<SignosVitalesReglasValidacion> reglasPerimetroCefalicoParaEdad;

	private List<SignosVitalesReglasValidacion> reglasIMCparaEdad;

	private static final String AMBOS_SEXOS = "A";

	@Autowired
	public ScoreZAndCategoriaService(PercentildetalleService percentildetalleService, PercentilService percentilService,
			SignosvitalesreglasService signosvitalesreglasService) {
		this.percentildetalleService = percentildetalleService;
		this.percentilService = percentilService;
		this.signosvitalesreglasService = signosvitalesreglasService;
	}

	@PostConstruct
	private void init() {
		this.reglasPesoParaEdad = this.signosvitalesreglasService.findAllReglasAlertasPesoParaEdad();
		this.reglasTallaParaEdad = this.signosvitalesreglasService.findAllReglasAlertasTallaParaEdad();
		this.reglasPesoParaLongitudTalla = this.signosvitalesreglasService.findAllReglasAlertasPesoParaLongitudTalla();
		this.reglasPerimetroCefalicoParaEdad = this.signosvitalesreglasService
				.findAllReglasAlertasPerimetroCefalicoEdad();
		this.reglasIMCparaEdad = this.signosvitalesreglasService.findAllReglasAlertasIMCParaEdad();
	}

	private Double calculateScoreZ(ValoresAntropometricos valores, Integer signoVitalScoreZ, Integer edadAniosMesDias,
			Integer edadEnMeses, Integer edadEnDias, String sexo, boolean isTallaDePie) {

		Double valorAntropometrico = getValorAntropometricoPorTipoScoreZ(valores, signoVitalScoreZ);

		Percentil percentil = percentilService.findPercentilByCondicion(signoVitalScoreZ, edadAniosMesDias, sexo,
				isTallaDePie);

		Double valorForScoreZ = null;

		if (percentil == null) {
			throw new PercentilNotFoundException("Percentil no encontrado");
		}

		switch (percentil.getMedida()) {
		case "M":
			valorForScoreZ = edadEnMeses.doubleValue();
			break;
		case "D":
			valorForScoreZ = edadEnDias.doubleValue();
			break;
		default:
			valorForScoreZ = valores.getTalla();
			break;
		}

		Percentildetalle percentildetalle = percentildetalleService
				.findPercentilDetalleByPercentilAndValor(percentil.getId(), valorForScoreZ);

		if (percentildetalle != null) {
			return UtilitarioMedico.calcularScoreZGivenValues(valorAntropometrico,
					percentildetalle.getL().doubleValue(), percentildetalle.getM().doubleValue(),
					percentildetalle.getS().doubleValue());
		} else {
			throw new PercentilDetalleNotFoundException("PercentilDetalle no encontrado");
		}
	}

	private Double getValorAntropometricoPorTipoScoreZ(ValoresAntropometricos valores, Integer signoVitalScoreZ) {

		switch (signoVitalScoreZ) {
		case ConstantesDetalleCatalogo.TIPO_MEDICION_TALLA_PARA_EDAD:
			return valores.getTalla();
		case ConstantesDetalleCatalogo.TIPO_MEDICION_PESO_PARA_EDAD:
			return valores.getPeso();
		case ConstantesDetalleCatalogo.TIPO_MEDICION_IMC_PARA_EDAD:
			return valores.getImc();
		case ConstantesDetalleCatalogo.TIPO_MEDICION_PESO_PARA_LONGITUD_TALLA:
			return valores.getPeso();
		case ConstantesDetalleCatalogo.TIPO_MEDICION_PERIMETRO_CEFALICO_PARA_EDAD:
			return valores.getPerimetroCefalico();
		default:
			return null;
		}
	}

	public Double calcularScoreZPesoParaEdad(Double peso, Integer edadAniosMesDias, Integer edadEnMeses,
			Integer edadEnDias, String sexo) {
		return calculateScoreZ(ValoresAntropometricos.getInstance(peso, null, null, null),
				ConstantesDetalleCatalogo.TIPO_MEDICION_PESO_PARA_EDAD, edadAniosMesDias, edadEnMeses, edadEnDias, sexo,
				false);
	}

	public Double calcularScoreZTallaParaEdad(Double talla, Integer edadAniosMesDias, Integer edadEnMeses,
			Integer edadEnDias, String sexo) {
		return calculateScoreZ(ValoresAntropometricos.getInstance(null, talla, null, null),
				ConstantesDetalleCatalogo.TIPO_MEDICION_TALLA_PARA_EDAD, edadAniosMesDias, edadEnMeses, edadEnDias,
				sexo, false);
	}

	public Double calcularScoreZPesoParaLongitudTalla(Double peso, Double talla, Integer edadAniosMesDias,
			Integer edadEnMeses, Integer edadEnDias, String sexo, boolean isTallaDePie) {
		return calculateScoreZ(ValoresAntropometricos.getInstance(peso, talla, null, null),
				ConstantesDetalleCatalogo.TIPO_MEDICION_PESO_PARA_LONGITUD_TALLA, edadAniosMesDias, edadEnMeses,
				edadEnDias, sexo, isTallaDePie);
	}

	public Double calcularScoreZPerimetroCefalicoParaEdad(Double perimetroCefalico, Integer edadAniosMesDias,
			Integer edadEnMeses, Integer edadEnDias, String sexo) {
		return calculateScoreZ(ValoresAntropometricos.getInstance(null, null, null, perimetroCefalico),
				ConstantesDetalleCatalogo.TIPO_MEDICION_PERIMETRO_CEFALICO_PARA_EDAD, edadAniosMesDias, edadEnMeses,
				edadEnDias, sexo, false);
	}

	public Double calcularScoreZIMCparaEdad(Double imc, Integer edadAniosMesDias, Integer edadEnMeses,
			Integer edadEnDias, String sexo) {
		return calculateScoreZ(ValoresAntropometricos.getInstance(null, null, imc, null),
				ConstantesDetalleCatalogo.TIPO_MEDICION_IMC_PARA_EDAD, edadAniosMesDias, edadEnMeses, edadEnDias, sexo,
				false);
	}

	public SignosVitalesReglasValidacion calcularCategoriaPesoParaEdad(Double scorez, Integer edadAniosMesDias,
			String sexo) {

		return findSignosVitalesReglasValidacion(reglasPesoParaEdad, scorez, edadAniosMesDias, sexo);
	}

	public SignosVitalesReglasValidacion calcularCategoriaTallaParaEdad(Double scorez, Integer edadAniosMesDias,
			String sexo) {

		return findSignosVitalesReglasValidacion(reglasTallaParaEdad, scorez, edadAniosMesDias, sexo);
	}

	public SignosVitalesReglasValidacion calcularCategoriaPesoParaLongitudTalla(Double scorez, Integer edadAniosMesDias,
			String sexo) {

		return findSignosVitalesReglasValidacion(reglasPesoParaLongitudTalla, scorez, edadAniosMesDias, sexo);
	}

	public SignosVitalesReglasValidacion calcularCategoriaPerimetroCefalicoParaEdad(Double scorez,
			Integer edadAniosMesDias, String sexo) {

		return findSignosVitalesReglasValidacion(reglasPerimetroCefalicoParaEdad, scorez, edadAniosMesDias, sexo);
	}

	public SignosVitalesReglasValidacion calcularCategoriaIMCparaEdad(Double scorez, Integer edadAniosMesDias,
			String sexo) {

		return findSignosVitalesReglasValidacion(reglasIMCparaEdad, scorez, edadAniosMesDias, sexo);
	}

	private SignosVitalesReglasValidacion findSignosVitalesReglasValidacion(List<SignosVitalesReglasValidacion> reglas,
			Double scorez, Integer edadAniosMesDias, String sexo) {

		Optional<SignosVitalesReglasValidacion> alertaFound = reglas.parallelStream().filter(
				regla -> isEdadPacienteDentroRango(edadAniosMesDias, regla.getEdadminima(), regla.getEdadmaxima())
						&& isValorDentroRango(BigDecimal.valueOf(scorez), regla.getValorMinimoAlerta(),
								regla.getValorMaximoAlerta())
						&& isSexoCorrespondiente(sexo, regla.getSexo()))
				.findFirst();

		SignosVitalesReglasValidacion alerta = alertaFound.isPresent() ? alertaFound.get() : null;

		if (alerta == null) {
			throw new SignosVitalesReglasValidacionNotFoundException("Categoría no encontrada");
		}

		return alerta;
	}

	private boolean isSexoCorrespondiente(String sexoIn, String sexoRegla) {
		return sexoRegla.equals(sexoIn) || sexoRegla.equals(AMBOS_SEXOS);
	}

	private boolean isEdadPacienteDentroRango(Integer edad, Integer edadMin, Integer edadMax) {
		return edad >= edadMin && edad <= edadMax;
	}

	private boolean isValorDentroRango(BigDecimal valor, BigDecimal imcMin, BigDecimal imcMax) {
		return valor.compareTo(imcMin) >= 0 && valor.compareTo(imcMax) <= 0;
	}

	public class PercentilNotFoundException extends RuntimeException {

		private static final long serialVersionUID = 4602337013331425176L;

		public PercentilNotFoundException(String message) {
			super(message);
		}

	}

	public class PercentilDetalleNotFoundException extends RuntimeException {

		private static final long serialVersionUID = 4602337013331425176L;

		public PercentilDetalleNotFoundException(String message) {
			super(message);
		}

	}
}

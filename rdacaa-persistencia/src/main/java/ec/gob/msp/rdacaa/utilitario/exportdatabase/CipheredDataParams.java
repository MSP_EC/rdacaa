package ec.gob.msp.rdacaa.utilitario.exportdatabase;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CipheredDataParams {

	public CipheredDataParams(Object[] credentials, boolean isLocalCipher, boolean isMSPCipher, String privateKeypath) {
		super();
		this.credentials = credentials;
		this.isLocalCipher = isLocalCipher;
		this.isMSPCipher = isMSPCipher;
		this.privateKeypath = privateKeypath;
	}

	private Object[] credentials;

	private boolean isLocalCipher;

	private boolean isMSPCipher;

	private String privateKeypath;
}

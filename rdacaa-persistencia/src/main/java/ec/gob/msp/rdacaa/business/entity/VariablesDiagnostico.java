/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.gob.msp.rdacaa.business.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

import org.hibernate.annotations.Immutable;

/**
 *
 * @author msp
 */
@Entity
@Immutable
@Table(name = "VARIABLES_DIAGNOSTICO", catalog = "", schema = "")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "VariablesDiagnostico.findAll", query = "SELECT v FROM VariablesDiagnostico v")})
public class VariablesDiagnostico implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Column(name = "ID_ATENCION")
    private Integer idAtencion;
    @Column(name = "UUID_ATENCION", length = 2000000000)
    private String uuidAtencion;
    @Column(name = "COD_DIAG_CIE_1", length = 2000000000)
    private String codDiagCie1;
    @Column(name = "COD_DIAG_TIP_DIAGNOSTICO_1", length = 2000000000)
    private String codDiagTipDiagnostico1;
    @Column(name = "COD_DIAG_TIP_DIAGNOSTICO_DESCN_1", length = 2000000000)
    private String codDiagTipDiagnosticoDescn1;
    @Column(name = "COD_DIAG_TIP_ATENCION_1", length = 2000000000)
    private String codDiagTipAtencion1;
    @Column(name = "COD_DIAG_TIP_ATENCION_DESC_1", length = 2000000000)
    private String codDiagTipAtencionDesc1;
    @Column(name = "COD_DIAG_CON_DIAGNOSTICO_1", length = 2000000000)
    private String codDiagConDiagnostico1;
    @Column(name = "COD_DIAG_CON_DIAGNOSTICO_DESC_1", length = 2000000000)
    private String codDiagConDiagnosticoDesc1;
    @Column(name = "COD_DIAG_CIE_2", length = 2000000000)
    private String codDiagCie2;
    @Column(name = "COD_DIAG_TIP_DIAGNOSTICO_2", length = 2000000000)
    private String codDiagTipDiagnostico2;
    @Column(name = "COD_DIAG_TIP_DIAGNOSTICO_DESC_2", length = 2000000000)
    private String codDiagTipDiagnosticoDesc2;
    @Column(name = "COD_DIAG_TIP_ATENCION_2", length = 2000000000)
    private String codDiagTipAtencion2;
    @Column(name = "COD_DIAG_TIP_ATENCION_DESC_2", length = 2000000000)
    private String codDiagTipAtencionDesc2;
    @Column(name = "COD_DIAG_CON_DIAGNOSTICO_2", length = 2000000000)
    private String codDiagConDiagnostico2;
    @Column(name = "COD_DIAG_CON_DIAGNOSTICO_DESC_2", length = 2000000000)
    private String codDiagConDiagnosticoDesc2;
    @Column(name = "COD_DIAG_CIE_3", length = 2000000000)
    private String codDiagCie3;
    @Column(name = "COD_DIAG_TIP_DIAGNOSTICO_3", length = 2000000000)
    private String codDiagTipDiagnostico3;
    @Column(name = "COD_DIAG_TIP_DIAGNOSTICO_DESC_3", length = 2000000000)
    private String codDiagTipDiagnosticoDesc3;
    @Column(name = "COD_DIAG_TIP_ATENCION_3", length = 2000000000)
    private String codDiagTipAtencion3;
    @Column(name = "COD_DIAG_TIP_ATENCION_DESC_3", length = 2000000000)
    private String codDiagTipAtencionDesc3;
    @Column(name = "COD_DIAG_CON_DIAGNOSTICO_3", length = 2000000000)
    private String codDiagConDiagnostico3;
    @Column(name = "COD_DIAG_CON_DIAGNOSTICO_DESC_3", length = 2000000000)
    private String codDiagConDiagnosticoDesc3;
    @Column(name = "COD_DIAG_CIE_4", length = 2000000000)
    private String codDiagCie4;
    @Column(name = "COD_DIAG_TIP_DIAGNOSTICO_4", length = 2000000000)
    private String codDiagTipDiagnostico4;
    @Column(name = "COD_DIAG_TIP_DIAGNOSTICO_DESC_4", length = 2000000000)
    private String codDiagTipDiagnosticoDesc4;
    @Column(name = "COD_DIAG_TIP_ATENCION_4", length = 2000000000)
    private String codDiagTipAtencion4;
    @Column(name = "COD_DIAG_TIP_ATENCION_DESC_4", length = 2000000000)
    private String codDiagTipAtencionDesc4;
    @Column(name = "COD_DIAG_CON_DIAGNOSTICO_4", length = 2000000000)
    private String codDiagConDiagnostico4;
    @Column(name = "COD_DIAG_CON_DIAGNOSTICO_DESC_4", length = 2000000000)
    private String codDiagConDiagnosticoDesc4;
    @Column(name = "COD_DIAG_CIE_5", length = 2000000000)
    private String codDiagCie5;
    @Column(name = "COD_DIAG_TIP_DIAGNOSTICO_5", length = 2000000000)
    private String codDiagTipDiagnostico5;
    @Column(name = "COD_DIAG_TIP_DIAGNOSTICO_DESC_5", length = 2000000000)
    private String codDiagTipDiagnosticoDesc5;
    @Column(name = "COD_DIAG_TIP_ATENCION_5", length = 2000000000)
    private String codDiagTipAtencion5;
    @Column(name = "COD_DIAG_TIP_ATENCION_DESC_5", length = 2000000000)
    private String codDiagTipAtencionDesc5;
    @Column(name = "COD_DIAG_CON_DIAGNOSTICO_5", length = 2000000000)
    private String codDiagConDiagnostico5;
    @Column(name = "COD_DIAG_CON_DIAGNOSTICO_DESC_5", length = 2000000000)
    private String codDiagConDiagnosticoDesc5;
    @Column(name = "DIAG_NUM_CER_MEDICO_UNICO", length = 2000000000)
    private String diagNumCerMedicoUnico;

    public VariablesDiagnostico() {
    }

    public Integer getIdAtencion() {
        return idAtencion;
    }

    public void setIdAtencion(Integer idAtencion) {
        this.idAtencion = idAtencion;
    }

    public String getUuidAtencion() {
        return uuidAtencion;
    }

    public void setUuidAtencion(String uuidAtencion) {
        this.uuidAtencion = uuidAtencion;
    }

    public String getCodDiagCie1() {
        return codDiagCie1;
    }

    public void setCodDiagCie1(String codDiagCie1) {
        this.codDiagCie1 = codDiagCie1;
    }

    public String getCodDiagTipDiagnostico1() {
        return codDiagTipDiagnostico1;
    }

    public void setCodDiagTipDiagnostico1(String codDiagTipDiagnostico1) {
        this.codDiagTipDiagnostico1 = codDiagTipDiagnostico1;
    }

    public String getCodDiagTipDiagnosticoDescn1() {
        return codDiagTipDiagnosticoDescn1;
    }

    public void setCodDiagTipDiagnosticoDescn1(String codDiagTipDiagnosticoDescn1) {
        this.codDiagTipDiagnosticoDescn1 = codDiagTipDiagnosticoDescn1;
    }

    public String getCodDiagTipAtencion1() {
        return codDiagTipAtencion1;
    }

    public void setCodDiagTipAtencion1(String codDiagTipAtencion1) {
        this.codDiagTipAtencion1 = codDiagTipAtencion1;
    }

    public String getCodDiagTipAtencionDesc1() {
        return codDiagTipAtencionDesc1;
    }

    public void setCodDiagTipAtencionDesc1(String codDiagTipAtencionDesc1) {
        this.codDiagTipAtencionDesc1 = codDiagTipAtencionDesc1;
    }

    public String getCodDiagConDiagnostico1() {
        return codDiagConDiagnostico1;
    }

    public void setCodDiagConDiagnostico1(String codDiagConDiagnostico1) {
        this.codDiagConDiagnostico1 = codDiagConDiagnostico1;
    }

    public String getCodDiagConDiagnosticoDesc1() {
        return codDiagConDiagnosticoDesc1;
    }

    public void setCodDiagConDiagnosticoDesc1(String codDiagConDiagnosticoDesc1) {
        this.codDiagConDiagnosticoDesc1 = codDiagConDiagnosticoDesc1;
    }

    public String getCodDiagCie2() {
        return codDiagCie2;
    }

    public void setCodDiagCie2(String codDiagCie2) {
        this.codDiagCie2 = codDiagCie2;
    }

    public String getCodDiagTipDiagnostico2() {
        return codDiagTipDiagnostico2;
    }

    public void setCodDiagTipDiagnostico2(String codDiagTipDiagnostico2) {
        this.codDiagTipDiagnostico2 = codDiagTipDiagnostico2;
    }

    public String getCodDiagTipDiagnosticoDesc2() {
        return codDiagTipDiagnosticoDesc2;
    }

    public void setCodDiagTipDiagnosticoDesc2(String codDiagTipDiagnosticoDesc2) {
        this.codDiagTipDiagnosticoDesc2 = codDiagTipDiagnosticoDesc2;
    }

    public String getCodDiagTipAtencion2() {
        return codDiagTipAtencion2;
    }

    public void setCodDiagTipAtencion2(String codDiagTipAtencion2) {
        this.codDiagTipAtencion2 = codDiagTipAtencion2;
    }

    public String getCodDiagTipAtencionDesc2() {
        return codDiagTipAtencionDesc2;
    }

    public void setCodDiagTipAtencionDesc2(String codDiagTipAtencionDesc2) {
        this.codDiagTipAtencionDesc2 = codDiagTipAtencionDesc2;
    }

    public String getCodDiagConDiagnostico2() {
        return codDiagConDiagnostico2;
    }

    public void setCodDiagConDiagnostico2(String codDiagConDiagnostico2) {
        this.codDiagConDiagnostico2 = codDiagConDiagnostico2;
    }

    public String getCodDiagConDiagnosticoDesc2() {
        return codDiagConDiagnosticoDesc2;
    }

    public void setCodDiagConDiagnosticoDesc2(String codDiagConDiagnosticoDesc2) {
        this.codDiagConDiagnosticoDesc2 = codDiagConDiagnosticoDesc2;
    }

    public String getCodDiagCie3() {
        return codDiagCie3;
    }

    public void setCodDiagCie3(String codDiagCie3) {
        this.codDiagCie3 = codDiagCie3;
    }

    public String getCodDiagTipDiagnostico3() {
        return codDiagTipDiagnostico3;
    }

    public void setCodDiagTipDiagnostico3(String codDiagTipDiagnostico3) {
        this.codDiagTipDiagnostico3 = codDiagTipDiagnostico3;
    }

    public String getCodDiagTipDiagnosticoDesc3() {
        return codDiagTipDiagnosticoDesc3;
    }

    public void setCodDiagTipDiagnosticoDesc3(String codDiagTipDiagnosticoDesc3) {
        this.codDiagTipDiagnosticoDesc3 = codDiagTipDiagnosticoDesc3;
    }

    public String getCodDiagTipAtencion3() {
        return codDiagTipAtencion3;
    }

    public void setCodDiagTipAtencion3(String codDiagTipAtencion3) {
        this.codDiagTipAtencion3 = codDiagTipAtencion3;
    }

    public String getCodDiagTipAtencionDesc3() {
        return codDiagTipAtencionDesc3;
    }

    public void setCodDiagTipAtencionDesc3(String codDiagTipAtencionDesc3) {
        this.codDiagTipAtencionDesc3 = codDiagTipAtencionDesc3;
    }

    public String getCodDiagConDiagnostico3() {
        return codDiagConDiagnostico3;
    }

    public void setCodDiagConDiagnostico3(String codDiagConDiagnostico3) {
        this.codDiagConDiagnostico3 = codDiagConDiagnostico3;
    }

    public String getCodDiagConDiagnosticoDesc3() {
        return codDiagConDiagnosticoDesc3;
    }

    public void setCodDiagConDiagnosticoDesc3(String codDiagConDiagnosticoDesc3) {
        this.codDiagConDiagnosticoDesc3 = codDiagConDiagnosticoDesc3;
    }

    public String getCodDiagCie4() {
        return codDiagCie4;
    }

    public void setCodDiagCie4(String codDiagCie4) {
        this.codDiagCie4 = codDiagCie4;
    }

    public String getCodDiagTipDiagnostico4() {
        return codDiagTipDiagnostico4;
    }

    public void setCodDiagTipDiagnostico4(String codDiagTipDiagnostico4) {
        this.codDiagTipDiagnostico4 = codDiagTipDiagnostico4;
    }

    public String getCodDiagTipDiagnosticoDesc4() {
        return codDiagTipDiagnosticoDesc4;
    }

    public void setCodDiagTipDiagnosticoDesc4(String codDiagTipDiagnosticoDesc4) {
        this.codDiagTipDiagnosticoDesc4 = codDiagTipDiagnosticoDesc4;
    }

    public String getCodDiagTipAtencion4() {
        return codDiagTipAtencion4;
    }

    public void setCodDiagTipAtencion4(String codDiagTipAtencion4) {
        this.codDiagTipAtencion4 = codDiagTipAtencion4;
    }

    public String getCodDiagTipAtencionDesc4() {
        return codDiagTipAtencionDesc4;
    }

    public void setCodDiagTipAtencionDesc4(String codDiagTipAtencionDesc4) {
        this.codDiagTipAtencionDesc4 = codDiagTipAtencionDesc4;
    }

    public String getCodDiagConDiagnostico4() {
        return codDiagConDiagnostico4;
    }

    public void setCodDiagConDiagnostico4(String codDiagConDiagnostico4) {
        this.codDiagConDiagnostico4 = codDiagConDiagnostico4;
    }

    public String getCodDiagConDiagnosticoDesc4() {
        return codDiagConDiagnosticoDesc4;
    }

    public void setCodDiagConDiagnosticoDesc4(String codDiagConDiagnosticoDesc4) {
        this.codDiagConDiagnosticoDesc4 = codDiagConDiagnosticoDesc4;
    }

    public String getCodDiagCie5() {
        return codDiagCie5;
    }

    public void setCodDiagCie5(String codDiagCie5) {
        this.codDiagCie5 = codDiagCie5;
    }

    public String getCodDiagTipDiagnostico5() {
        return codDiagTipDiagnostico5;
    }

    public void setCodDiagTipDiagnostico5(String codDiagTipDiagnostico5) {
        this.codDiagTipDiagnostico5 = codDiagTipDiagnostico5;
    }

    public String getCodDiagTipDiagnosticoDesc5() {
        return codDiagTipDiagnosticoDesc5;
    }

    public void setCodDiagTipDiagnosticoDesc5(String codDiagTipDiagnosticoDesc5) {
        this.codDiagTipDiagnosticoDesc5 = codDiagTipDiagnosticoDesc5;
    }

    public String getCodDiagTipAtencion5() {
        return codDiagTipAtencion5;
    }

    public void setCodDiagTipAtencion5(String codDiagTipAtencion5) {
        this.codDiagTipAtencion5 = codDiagTipAtencion5;
    }

    public String getCodDiagTipAtencionDesc5() {
        return codDiagTipAtencionDesc5;
    }

    public void setCodDiagTipAtencionDesc5(String codDiagTipAtencionDesc5) {
        this.codDiagTipAtencionDesc5 = codDiagTipAtencionDesc5;
    }

    public String getCodDiagConDiagnostico5() {
        return codDiagConDiagnostico5;
    }

    public void setCodDiagConDiagnostico5(String codDiagConDiagnostico5) {
        this.codDiagConDiagnostico5 = codDiagConDiagnostico5;
    }

    public String getCodDiagConDiagnosticoDesc5() {
        return codDiagConDiagnosticoDesc5;
    }

    public void setCodDiagConDiagnosticoDesc5(String codDiagConDiagnosticoDesc5) {
        this.codDiagConDiagnosticoDesc5 = codDiagConDiagnosticoDesc5;
    }

    public String getDiagNumCerMedicoUnico() {
        return diagNumCerMedicoUnico;
    }

    public void setDiagNumCerMedicoUnico(String diagNumCerMedicoUnico) {
        this.diagNumCerMedicoUnico = diagNumCerMedicoUnico;
    }
    
}

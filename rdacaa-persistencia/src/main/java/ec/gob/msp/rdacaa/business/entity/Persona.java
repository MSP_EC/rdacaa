package ec.gob.msp.rdacaa.business.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author msp
 */
@Entity
@Table(name = "persona")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Persona.findAll", query = "SELECT p FROM Persona p")})
public class Persona implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id", nullable = false)
    private Integer id;
    @Column(name = "uuidpersona", length = 250)
    private String uuidpersona;
    @Column(name = "idpras")
    private Integer idpras;
    @Column(name = "numerohistoriaclinica", length = 30)
    private String numerohistoriaclinica;
    @Column(name = "telefonopaciente", length = 15)
    private String telefonopaciente;
    @Column(name = "telefonofamiliar", length = 15)
    private String telefonofamiliar;
    @Column(name = "direcciondomicilio", length = 250)
    private String direcciondomicilio;
    @Column(name = "numeroidentificacion", length = 20)
    private String numeroidentificacion;
    @Column(name = "primernombre", length = 50)
    private String primernombre;
    @Column(name = "segundonombre", length = 50)
    private String segundonombre;
    @Column(name = "primerapellido", length = 50)
    private String primerapellido;
    @Column(name = "segundoapellido", length = 50)
    private String segundoapellido;
    @Column(name = "fechanacimiento")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechanacimiento;
    @Column(name = "numeroarchivo")
    private String numeroarchivo;
    @Column(name = "numidentificacionrepte", length = 20)
    private String numidentificacionrepte;
    @Column(name = "estado")
    private Integer estado;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "personaId")
    private List<Entidadpersona> entidadpersonaList;
    @JoinColumn(name = "ctnacionalidadetnica_id", referencedColumnName = "id")
    @ManyToOne
    private Detallecatalogo ctnacionalidadetnicaId;
    @JoinColumn(name = "ctetnia_id", referencedColumnName = "id")
    @ManyToOne
    private Detallecatalogo ctetniaId;
    @JoinColumn(name = "ctformacionprof_id", referencedColumnName = "id")
    @ManyToOne
    private Detallecatalogo ctformacionprofId;
    @JoinColumn(name = "ctidentidadgenero_id", referencedColumnName = "id")
    @ManyToOne
    private Detallecatalogo ctidentidadgeneroId;
    @JoinColumn(name = "parroquia_id", referencedColumnName = "id")
    @ManyToOne
    private Parroquia parroquiaId;
    @JoinColumn(name = "ctorientacionsexual_id", referencedColumnName = "id")
    @ManyToOne
    private Detallecatalogo ctorientacionsexualId;
    @JoinColumn(name = "ctpueblo_id", referencedColumnName = "id")
    @ManyToOne
    private Detallecatalogo ctpuebloId;
    @JoinColumn(name = "ctsexo_id", referencedColumnName = "id")
    @ManyToOne
    private Detallecatalogo ctsexoId;
    @JoinColumn(name = "cttipobono_id", referencedColumnName = "id")
    @ManyToOne
    private Detallecatalogo cttipobonoId;
    @JoinColumn(name = "cttipoidentificacion_id", referencedColumnName = "id")
    @ManyToOne
    private Detallecatalogo cttipoidentificacionId;
    @JoinColumn(name = "cttipoidentrepte_id", referencedColumnName = "id")
    @ManyToOne
    private Detallecatalogo cttipoidentrepteId;
    @JoinColumn(name = "cttiposegurosalud_id", referencedColumnName = "id")
    @ManyToOne
    private Detallecatalogo cttiposegurosaludId;
    @JoinColumn(name = "pais_id", referencedColumnName = "id")
    @ManyToOne
    private Pais paisId;
    @JoinColumn(name = "cttipopersona_id", referencedColumnName = "id", nullable = false)
    @ManyToOne(optional = false)
    private Detallecatalogo cttipopersonaId;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "personaId")
    private List<Usuario> usuarioList;

    public Persona() {
    }

    public Persona(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
    
    public String getUuidpersona() {
        return uuidpersona;
    }

    public void setUuidpersona(String uuidpersona) {
        this.uuidpersona = uuidpersona;
    }

    public Integer getIdpras() {
        return idpras;
    }

    public void setIdpras(Integer idpras) {
        this.idpras = idpras;
    }

    public String getNumerohistoriaclinica() {
        return numerohistoriaclinica;
    }

    public void setNumerohistoriaclinica(String numerohistoriaclinica) {
        this.numerohistoriaclinica = numerohistoriaclinica;
    }

    public String getTelefonopaciente() {
        return telefonopaciente;
    }

    public void setTelefonopaciente(String telefonopaciente) {
        this.telefonopaciente = telefonopaciente;
    }

    public String getTelefonofamiliar() {
        return telefonofamiliar;
    }

    public void setTelefonofamiliar(String telefonofamiliar) {
        this.telefonofamiliar = telefonofamiliar;
    }

    public String getDirecciondomicilio() {
        return direcciondomicilio;
    }

    public void setDirecciondomicilio(String direcciondomicilio) {
        this.direcciondomicilio = direcciondomicilio;
    }

    public String getNumeroidentificacion() {
        return numeroidentificacion;
    }

    public void setNumeroidentificacion(String numeroidentificacion) {
        this.numeroidentificacion = numeroidentificacion;
    }

    public String getPrimernombre() {
        return primernombre;
    }

    public void setPrimernombre(String primernombre) {
        this.primernombre = primernombre;
    }

    public String getSegundonombre() {
        return segundonombre;
    }

    public void setSegundonombre(String segundonombre) {
        this.segundonombre = segundonombre;
    }

    public String getPrimerapellido() {
        return primerapellido;
    }

    public void setPrimerapellido(String primerapellido) {
        this.primerapellido = primerapellido;
    }

    public String getSegundoapellido() {
        return segundoapellido;
    }

    public void setSegundoapellido(String segundoapellido) {
        this.segundoapellido = segundoapellido;
    }

    public Date getFechanacimiento() {
        return fechanacimiento;
    }

    public void setFechanacimiento(Date fechanacimiento) {
        this.fechanacimiento = fechanacimiento;
    }

    public String getNumeroarchivo() {
        return numeroarchivo;
    }

    public void setNumeroarchivo(String numeroarchivo) {
        this.numeroarchivo = numeroarchivo;
    }

    public String getNumidentificacionrepte() {
        return numidentificacionrepte;
    }

    public void setNumidentificacionrepte(String numidentificacionrepte) {
        this.numidentificacionrepte = numidentificacionrepte;
    }

    public Integer getEstado() {
        return estado;
    }

    public void setEstado(Integer estado) {
        this.estado = estado;
    }

    @XmlTransient
    public List<Entidadpersona> getEntidadpersonaList() {
        return entidadpersonaList;
    }

    public void setEntidadpersonaList(List<Entidadpersona> entidadpersonaList) {
        this.entidadpersonaList = entidadpersonaList;
    }

    public Detallecatalogo getCtnacionalidadetnicaId() {
        return ctnacionalidadetnicaId;
    }

    public void setCtnacionalidadetnicaId(Detallecatalogo ctnacionalidadetnicaId) {
        this.ctnacionalidadetnicaId = ctnacionalidadetnicaId;
    }

    public Detallecatalogo getCtetniaId() {
        return ctetniaId;
    }

    public void setCtetniaId(Detallecatalogo ctetniaId) {
        this.ctetniaId = ctetniaId;
    }

    public Detallecatalogo getCtformacionprofId() {
        return ctformacionprofId;
    }

    public void setCtformacionprofId(Detallecatalogo ctformacionprofId) {
        this.ctformacionprofId = ctformacionprofId;
    }

    public Detallecatalogo getCtidentidadgeneroId() {
        return ctidentidadgeneroId;
    }

    public void setCtidentidadgeneroId(Detallecatalogo ctidentidadgeneroId) {
        this.ctidentidadgeneroId = ctidentidadgeneroId;
    }

    public Parroquia getParroquiaId() {
        return parroquiaId;
    }

    public void setParroquiaId(Parroquia parroquiaId) {
        this.parroquiaId = parroquiaId;
    }

    public Detallecatalogo getCtorientacionsexualId() {
        return ctorientacionsexualId;
    }

    public void setCtorientacionsexualId(Detallecatalogo ctorientacionsexualId) {
        this.ctorientacionsexualId = ctorientacionsexualId;
    }

    public Detallecatalogo getCtpuebloId() {
        return ctpuebloId;
    }

    public void setCtpuebloId(Detallecatalogo ctpuebloId) {
        this.ctpuebloId = ctpuebloId;
    }

    public Detallecatalogo getCtsexoId() {
        return ctsexoId;
    }

    public void setCtsexoId(Detallecatalogo ctsexoId) {
        this.ctsexoId = ctsexoId;
    }

    public Detallecatalogo getCttipobonoId() {
        return cttipobonoId;
    }

    public void setCttipobonoId(Detallecatalogo cttipobonoId) {
        this.cttipobonoId = cttipobonoId;
    }

    public Detallecatalogo getCttipoidentificacionId() {
        return cttipoidentificacionId;
    }

    public void setCttipoidentificacionId(Detallecatalogo cttipoidentificacionId) {
        this.cttipoidentificacionId = cttipoidentificacionId;
    }

    public Detallecatalogo getCttipoidentrepteId() {
        return cttipoidentrepteId;
    }

    public void setCttipoidentrepteId(Detallecatalogo cttipoidentrepteId) {
        this.cttipoidentrepteId = cttipoidentrepteId;
    }

    public Detallecatalogo getCttiposegurosaludId() {
        return cttiposegurosaludId;
    }

    public void setCttiposegurosaludId(Detallecatalogo cttiposegurosaludId) {
        this.cttiposegurosaludId = cttiposegurosaludId;
    }

    public Pais getPaisId() {
        return paisId;
    }

    public void setPaisId(Pais paisId) {
        this.paisId = paisId;
    }

    public Detallecatalogo getCttipopersonaId() {
        return cttipopersonaId;
    }

    public void setCttipopersonaId(Detallecatalogo cttipopersonaId) {
        this.cttipopersonaId = cttipopersonaId;
    }

    @XmlTransient
    public List<Usuario> getUsuarioList() {
        return usuarioList;
    }

    public void setUsuarioList(List<Usuario> usuarioList) {
        this.usuarioList = usuarioList;
    }

    @Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

    @Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Persona other = (Persona) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id)) {
			return false;
		}
		return true;
	}

    @Override
    public String toString() {
        return "ec.gob.msp.rdacaa.business.entity.Persona[ id=" + id + " ]";
    }

}

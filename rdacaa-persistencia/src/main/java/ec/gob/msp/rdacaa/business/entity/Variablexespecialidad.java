/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.gob.msp.rdacaa.business.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author msp
 */
@Entity
@Table(name = "variablexespecialidad", catalog = "rdacaa", schema = "public", uniqueConstraints = {
    @UniqueConstraint(columnNames = {"id"})})
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Variablexespecialidad.findAll", query = "SELECT v FROM Variablexespecialidad v")})
public class Variablexespecialidad implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "id", nullable = false)
    private Integer id;
    @Column(name = "ismandatorio")
    private Integer ismandatorio;
    @Column(name = "estado")
    private Integer estado;
    @JoinColumn(name = "ctespecialidad_id", referencedColumnName = "id")
    @ManyToOne
    private Detallecatalogo ctespecialidadId;
    @JoinColumn(name = "variable_id", referencedColumnName = "id")
    @ManyToOne
    private Variable variableId;

    public Variablexespecialidad() {
    }

    public Variablexespecialidad(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getIsmandatorio() {
        return ismandatorio;
    }

    public void setIsmandatorio(Integer ismandatorio) {
        this.ismandatorio = ismandatorio;
    }

    public Integer getEstado() {
        return estado;
    }

    public void setEstado(Integer estado) {
        this.estado = estado;
    }

    public Detallecatalogo getCtespecialidadId() {
        return ctespecialidadId;
    }

    public void setCtespecialidadId(Detallecatalogo ctespecialidadId) {
        this.ctespecialidadId = ctespecialidadId;
    }

    public Variable getVariableId() {
        return variableId;
    }

    public void setVariableId(Variable variableId) {
        this.variableId = variableId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Variablexespecialidad)) {
            return false;
        }
        Variablexespecialidad other = (Variablexespecialidad) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ec.gob.msp.rdacaa.business.entity.Variablexespecialidad[ id=" + id + " ]";
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.gob.msp.rdacaa.business.service;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ec.gob.msp.rdacaa.business.entity.Cierreatenciones;
import ec.gob.msp.rdacaa.business.entity.Usuario;
import ec.gob.msp.rdacaa.utilitario.fecha.FechasUtil;

/**
 *
 * @author dmurillo
 */
@Service
public class CierreMensualService {
    
    private CierreatencionesService cierreatencionesService;
    
    @Autowired
    public CierreMensualService(CierreatencionesService cierreatencionesService){
        this.cierreatencionesService = cierreatencionesService;
    }
    
    public void cierreMensual(Usuario usuario, String anio, Integer mes){
        
        /*Cierreatenciones actual;
        Cierreatenciones proximo;
        actual = cierreatencionesService.findOneByUsuarioId(usuario.getId());
        if(actual != null){
            int mesFinal = 0;
            int anioFial = Integer.parseInt(actual.getAnio()) ;
            if(actual.getMes() == 12){
                mesFinal = 1;
                anioFial = anioFial + 1;
            }else{
                mesFinal = actual.getMes() + 1;
            }
            proximo = new Cierreatenciones();
            proximo.setAnio(String.valueOf(anioFial));
            proximo.setEstadocierre(1);
            proximo.setMes(mesFinal);
            proximo.setUsuarioId(usuario);
            cierreatencionesService.save(proximo);
            
            actual.setEstadocierre(0);
            actual.setFechacreacion(new Date());
            cierreatencionesService.saveAndFlush(actual);
        }else{
            createProximoCierre(usuario);
        }*/
        
    }
    
    public void createProximoCierre(Usuario usuario) {
    	Cierreatenciones actual = cierreatencionesService.findOneByUsuarioId(usuario.getId());
    	if(actual == null) {
    		Cierreatenciones proximo;
    		proximo = new Cierreatenciones();
    		proximo.setAnio(String.valueOf(FechasUtil.getAnioMes(0)));
    		proximo.setEstadocierre(1);
    		proximo.setMes(FechasUtil.getAnioMes(1));
    		proximo.setUsuarioId(usuario);
    		cierreatencionesService.save(proximo);
    	}

    }
}

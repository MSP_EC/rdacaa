/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.gob.msp.rdacaa.business.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

import org.hibernate.annotations.Immutable;

/**
 *
 * @author msp
 */
@Entity
@Immutable
@Table(name = "VARIABLES_REF_INT", catalog = "", schema = "")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "VariablesRefInt.findAll", query = "SELECT v FROM VariablesRefInt v")})
public class VariablesRefInt implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Column(name = "ID_ATENCION")
    private Integer idAtencion;
    @Column(name = "UUID_ATENCION", length = 2000000000)
    private String uuidAtencion;
    @Column(name = "REF_CON_SUBSISTEMA")
    private Integer refConSubsistema;
    @Column(name = "REF_CON_SUBSISTEMA_DESC", length = 2000000000)
    private String refConSubsistemaDesc;
    @Column(name = "REF_CON_SUBSISTEMA_COD")
    private Integer refConSubsistemaCod;
    @Column(name = "REF_CON_LUG_REFERIDO")
    private Integer refConLugReferido;
    @Column(name = "REF_CON_LUG_REFERIDO_NOMBRE_OFICIAL", length = 2000000000)
    private String refConLugReferidoNombreOficial;
    @Column(name = "REF_CON_INTERCONSULTA")
    private Integer refConInterconsulta;
    @Column(name = "REF_CON_INTERCONSULTA_DESC", length = 2000000000)
    private String refConInterconsultaDesc;
    @Column(name = "REF_CON_INTERCONSULTA_COD")
    private Integer refConInterconsultaCod;

    public VariablesRefInt() {
    }

    public Integer getIdAtencion() {
        return idAtencion;
    }

    public void setIdAtencion(Integer idAtencion) {
        this.idAtencion = idAtencion;
    }

    public String getUuidAtencion() {
        return uuidAtencion;
    }

    public void setUuidAtencion(String uuidAtencion) {
        this.uuidAtencion = uuidAtencion;
    }

    public Integer getRefConSubsistema() {
        return refConSubsistema;
    }

    public void setRefConSubsistema(Integer refConSubsistema) {
        this.refConSubsistema = refConSubsistema;
    }

    public String getRefConSubsistemaDesc() {
        return refConSubsistemaDesc;
    }

    public void setRefConSubsistemaDesc(String refConSubsistemaDesc) {
        this.refConSubsistemaDesc = refConSubsistemaDesc;
    }

    public Integer getRefConLugReferido() {
        return refConLugReferido;
    }

    public void setRefConLugReferido(Integer refConLugReferido) {
        this.refConLugReferido = refConLugReferido;
    }

    public String getRefConLugReferidoNombreOficial() {
        return refConLugReferidoNombreOficial;
    }

    public void setRefConLugReferidoNombreOficial(String refConLugReferidoNombreOficial) {
        this.refConLugReferidoNombreOficial = refConLugReferidoNombreOficial;
    }

    public Integer getRefConInterconsulta() {
        return refConInterconsulta;
    }

    public void setRefConInterconsulta(Integer refConInterconsulta) {
        this.refConInterconsulta = refConInterconsulta;
    }

    public String getRefConInterconsultaDesc() {
        return refConInterconsultaDesc;
    }

    public void setRefConInterconsultaDesc(String refConInterconsultaDesc) {
        this.refConInterconsultaDesc = refConInterconsultaDesc;
    }

	public Integer getRefConSubsistemaCod() {
		return refConSubsistemaCod;
	}

	public void setRefConSubsistemaCod(Integer refConSubsistemaCod) {
		this.refConSubsistemaCod = refConSubsistemaCod;
	}

	public Integer getRefConInterconsultaCod() {
		return refConInterconsultaCod;
	}

	public void setRefConInterconsultaCod(Integer refConInterconsultaCod) {
		this.refConInterconsultaCod = refConInterconsultaCod;
	}
    
    
}

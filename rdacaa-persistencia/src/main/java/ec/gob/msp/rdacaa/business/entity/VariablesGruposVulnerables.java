/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.gob.msp.rdacaa.business.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

import org.hibernate.annotations.Immutable;

/**
 *
 * @author msp
 */
@Entity
@Immutable
@Table(name = "VARIABLES_GRUPOS_VULNERABLES", catalog = "", schema = "")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "VariablesGruposVulnerables.findAll", query = "SELECT v FROM VariablesGruposVulnerables v")})
public class VariablesGruposVulnerables implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Column(name = "ID_ATENCION")
    private Integer idAtencion;
    @Column(name = "UUID_ATENCION", length = 2000000000)
    private String uuidAtencion;
    @Column(name = "COD_GRP_VULNERABLE_1", length = 2000000000)
    private String codGrpVulnerable1;
    @Column(name = "COD_GRP_VULNERABLE_DESC_1", length = 2000000000)
    private String codGrpVulnerableDesc1;
    @Column(name = "COD_GRP_VULNERABLE_2", length = 2000000000)
    private String codGrpVulnerable2;
    @Column(name = "COD_GRP_VULNERABLE_DESC_2", length = 2000000000)
    private String codGrpVulnerableDesc2;
    @Column(name = "COD_GRP_VULNERABLE_3", length = 2000000000)
    private String codGrpVulnerable3;
    @Column(name = "COD_GRP_VULNERABLE_DESC_3", length = 2000000000)
    private String codGrpVulnerableDesc3;
    @Column(name = "COD_GRP_VULNERABLE_4", length = 2000000000)
    private String codGrpVulnerable4;
    @Column(name = "COD_GRP_VULNERABLE_DESC_4", length = 2000000000)
    private String codGrpVulnerableDesc4;
    @Column(name = "COD_GRP_VULNERABLE_5", length = 2000000000)
    private String codGrpVulnerable5;
    @Column(name = "COD_GRP_VULNERABLE_DESC_5", length = 2000000000)
    private String codGrpVulnerableDesc5;

    public VariablesGruposVulnerables() {
    }

    public Integer getIdAtencion() {
        return idAtencion;
    }

    public void setIdAtencion(Integer idAtencion) {
        this.idAtencion = idAtencion;
    }

    public String getUuidAtencion() {
        return uuidAtencion;
    }

    public void setUuidAtencion(String uuidAtencion) {
        this.uuidAtencion = uuidAtencion;
    }

    public String getCodGrpVulnerable1() {
        return codGrpVulnerable1;
    }

    public void setCodGrpVulnerable1(String codGrpVulnerable1) {
        this.codGrpVulnerable1 = codGrpVulnerable1;
    }

    public String getCodGrpVulnerableDesc1() {
        return codGrpVulnerableDesc1;
    }

    public void setCodGrpVulnerableDesc1(String codGrpVulnerableDesc1) {
        this.codGrpVulnerableDesc1 = codGrpVulnerableDesc1;
    }

    public String getCodGrpVulnerable2() {
        return codGrpVulnerable2;
    }

    public void setCodGrpVulnerable2(String codGrpVulnerable2) {
        this.codGrpVulnerable2 = codGrpVulnerable2;
    }

    public String getCodGrpVulnerableDesc2() {
        return codGrpVulnerableDesc2;
    }

    public void setCodGrpVulnerableDesc2(String codGrpVulnerableDesc2) {
        this.codGrpVulnerableDesc2 = codGrpVulnerableDesc2;
    }

    public String getCodGrpVulnerable3() {
        return codGrpVulnerable3;
    }

    public void setCodGrpVulnerable3(String codGrpVulnerable3) {
        this.codGrpVulnerable3 = codGrpVulnerable3;
    }

    public String getCodGrpVulnerableDesc3() {
        return codGrpVulnerableDesc3;
    }

    public void setCodGrpVulnerableDesc3(String codGrpVulnerableDesc3) {
        this.codGrpVulnerableDesc3 = codGrpVulnerableDesc3;
    }

    public String getCodGrpVulnerable4() {
        return codGrpVulnerable4;
    }

    public void setCodGrpVulnerable4(String codGrpVulnerable4) {
        this.codGrpVulnerable4 = codGrpVulnerable4;
    }

    public String getCodGrpVulnerableDesc4() {
        return codGrpVulnerableDesc4;
    }

    public void setCodGrpVulnerableDesc4(String codGrpVulnerableDesc4) {
        this.codGrpVulnerableDesc4 = codGrpVulnerableDesc4;
    }

    public String getCodGrpVulnerable5() {
        return codGrpVulnerable5;
    }

    public void setCodGrpVulnerable5(String codGrpVulnerable5) {
        this.codGrpVulnerable5 = codGrpVulnerable5;
    }

    public String getCodGrpVulnerableDesc5() {
        return codGrpVulnerableDesc5;
    }

    public void setCodGrpVulnerableDesc5(String codGrpVulnerableDesc5) {
        this.codGrpVulnerableDesc5 = codGrpVulnerableDesc5;
    }
    
}

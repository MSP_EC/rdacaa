/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.gob.msp.rdacaa.business.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

import org.hibernate.annotations.Immutable;

/**
 *
 * @author msp
 */
@Entity
@Immutable
@Table(name = "VARIABLES_VIOLENCIA", catalog = "", schema = "")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "VariablesViolencia.findAll", query = "SELECT v FROM VariablesViolencia v")})
public class VariablesViolencia implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Column(name = "ID_ATENCION")
    private Integer idAtencion;
    @Column(name = "UUID_ATENCION", length = 2000000000)
    private String uuidAtencion;
    @Column(name = "VIOL_NUM_SERIE", length = 2000000000)
    private String violNumSerie;
    @Column(name = "COD_VIOL_IDE_AGRESOR")
    private Integer codViolIdeAgresor;
    @Column(name = "COD_VIOL_IDE_AGRESOR_DESC", length = 2000000000)
    private String codViolIdeAgresorDesc;
    @Column(name = "COD_VIOL_LESIONES")
    private Integer codViolLesiones;
    @Column(name = "COD_VIOL_LESIONES_DESC", length = 2000000000)
    private String codViolLesionesDesc;
    @Column(name = "COD_VIOL_PARENTESCO")
    private Integer codViolParentesco;
    @Column(name = "COD_VIOL_PARENTESCO_DESC", length = 2000000000)
    private String codViolParentescoDesc;

    public VariablesViolencia() {
    }

    public Integer getIdAtencion() {
        return idAtencion;
    }

    public void setIdAtencion(Integer idAtencion) {
        this.idAtencion = idAtencion;
    }

    public String getUuidAtencion() {
        return uuidAtencion;
    }

    public void setUuidAtencion(String uuidAtencion) {
        this.uuidAtencion = uuidAtencion;
    }

    public String getViolNumSerie() {
        return violNumSerie;
    }

    public void setViolNumSerie(String violNumSerie) {
        this.violNumSerie = violNumSerie;
    }

    public Integer getCodViolIdeAgresor() {
        return codViolIdeAgresor;
    }

    public void setCodViolIdeAgresor(Integer codViolIdeAgresor) {
        this.codViolIdeAgresor = codViolIdeAgresor;
    }

    public String getCodViolIdeAgresorDesc() {
        return codViolIdeAgresorDesc;
    }

    public void setCodViolIdeAgresorDesc(String codViolIdeAgresorDesc) {
        this.codViolIdeAgresorDesc = codViolIdeAgresorDesc;
    }

    public Integer getCodViolLesiones() {
        return codViolLesiones;
    }

    public void setCodViolLesiones(Integer codViolLesiones) {
        this.codViolLesiones = codViolLesiones;
    }

    public String getCodViolLesionesDesc() {
        return codViolLesionesDesc;
    }

    public void setCodViolLesionesDesc(String codViolLesionesDesc) {
        this.codViolLesionesDesc = codViolLesionesDesc;
    }

    public Integer getCodViolParentesco() {
        return codViolParentesco;
    }

    public void setCodViolParentesco(Integer codViolParentesco) {
        this.codViolParentesco = codViolParentesco;
    }

    public String getCodViolParentescoDesc() {
        return codViolParentescoDesc;
    }

    public void setCodViolParentescoDesc(String codViolParentescoDesc) {
        this.codViolParentescoDesc = codViolParentescoDesc;
    }
    
}

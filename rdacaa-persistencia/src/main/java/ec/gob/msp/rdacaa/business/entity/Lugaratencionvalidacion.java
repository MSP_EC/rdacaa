/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.gob.msp.rdacaa.business.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author msp
 */
@Entity
@Table(name = "lugaratencionvalidacion", catalog = "rdacaa", schema = "public", uniqueConstraints = {
    @UniqueConstraint(columnNames = {"id"})})
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Lugaratencionvalidacion.findAll", query = "SELECT l FROM Lugaratencionvalidacion l")})
public class Lugaratencionvalidacion implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "id", nullable = false)
    private Integer id;
    @Column(name = "edadminanios")
    private Integer edadminanios;
    @Column(name = "edadminmeses")
    private Integer edadminmeses;
    @Column(name = "edadmindias")
    private Integer edadmindias;
    @Column(name = "edadmaxanios")
    private Integer edadmaxanios;
    @Column(name = "edadmaxmeses")
    private Integer edadmaxmeses;
    @Column(name = "edadmaxdias")
    private Integer edadmaxdias;
    @Column(name = "hombre")
    private Integer hombre;
    @Column(name = "mujer")
    private Integer mujer;
    @Column(name = "estado")
    private Integer estado;
    @JoinColumn(name = "ctlugaratencion_id", referencedColumnName = "id")
    @ManyToOne
    private Detallecatalogo ctlugaratencionId;

    public Lugaratencionvalidacion() {
    }

    public Lugaratencionvalidacion(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getEdadminanios() {
        return edadminanios;
    }

    public void setEdadminanios(Integer edadminanios) {
        this.edadminanios = edadminanios;
    }

    public Integer getEdadminmeses() {
        return edadminmeses;
    }

    public void setEdadminmeses(Integer edadminmeses) {
        this.edadminmeses = edadminmeses;
    }

    public Integer getEdadmindias() {
        return edadmindias;
    }

    public void setEdadmindias(Integer edadmindias) {
        this.edadmindias = edadmindias;
    }

    public Integer getEdadmaxanios() {
        return edadmaxanios;
    }

    public void setEdadmaxanios(Integer edadmaxanios) {
        this.edadmaxanios = edadmaxanios;
    }

    public Integer getEdadmaxmeses() {
        return edadmaxmeses;
    }

    public void setEdadmaxmeses(Integer edadmaxmeses) {
        this.edadmaxmeses = edadmaxmeses;
    }

    public Integer getEdadmaxdias() {
        return edadmaxdias;
    }

    public void setEdadmaxdias(Integer edadmaxdias) {
        this.edadmaxdias = edadmaxdias;
    }

    public Integer getHombre() {
        return hombre;
    }

    public void setHombre(Integer hombre) {
        this.hombre = hombre;
    }

    public Integer getMujer() {
        return mujer;
    }

    public void setMujer(Integer mujer) {
        this.mujer = mujer;
    }

    public Integer getEstado() {
        return estado;
    }

    public void setEstado(Integer estado) {
        this.estado = estado;
    }

    public Detallecatalogo getCtlugaratencionId() {
        return ctlugaratencionId;
    }

    public void setCtlugaratencionId(Detallecatalogo ctlugaratencionId) {
        this.ctlugaratencionId = ctlugaratencionId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Lugaratencionvalidacion)) {
            return false;
        }
        Lugaratencionvalidacion other = (Lugaratencionvalidacion) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ec.gob.msp.rdacaa.business.entity.Lugaratencionvalidacion[ id=" + id + " ]";
    }
    
}

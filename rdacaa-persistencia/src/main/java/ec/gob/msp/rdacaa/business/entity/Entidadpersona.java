package ec.gob.msp.rdacaa.business.entity;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author msp
 */
@Entity
@Table(name = "entidadpersona")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Entidadpersona.findAll", query = "SELECT e FROM Entidadpersona e")})
public class Entidadpersona implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id", nullable = false)
    private Integer id;
    @Column(name = "estado")
    private Integer estado;
    @JoinColumn(name = "ctespecialidad_id", referencedColumnName = "id", nullable = false)
    @ManyToOne(optional = false)
    private Detallecatalogo ctespecialidadId;
    @JoinColumn(name = "entidad_id", referencedColumnName = "id", nullable = false)
    @ManyToOne(optional = false)
    private Entidad entidadId;
    @JoinColumn(name = "persona_id", referencedColumnName = "id", nullable = false)
    @ManyToOne(optional = false)
    private Persona personaId;

    public Entidadpersona() {
    }

    public Entidadpersona(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getEstado() {
        return estado;
    }

    public void setEstado(Integer estado) {
        this.estado = estado;
    }

    public Detallecatalogo getCtespecialidadId() {
        return ctespecialidadId;
    }

    public void setCtespecialidadId(Detallecatalogo ctespecialidadId) {
        this.ctespecialidadId = ctespecialidadId;
    }

    public Entidad getEntidadId() {
        return entidadId;
    }

    public void setEntidadId(Entidad entidadId) {
        this.entidadId = entidadId;
    }

    public Persona getPersonaId() {
        return personaId;
    }

    public void setPersonaId(Persona personaId) {
        this.personaId = personaId;
    }

    @Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

    @Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Entidadpersona other = (Entidadpersona) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id)) {
			return false;
		}
		return true;
	}

    @Override
    public String toString() {
        return "ec.gob.msp.rdacaa.business.entity.Entidadpersona[ id=" + id + " ]";
    }
    
}

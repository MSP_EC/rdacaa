package ec.gob.msp.rdacaa.business.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.stereotype.Repository;

import ec.gob.msp.rdacaa.business.entity.Serviciotarifario;

/**
 * Generated by Spring Data Generator on 28/08/2018
 */
@Repository
public interface ServiciotarifarioRepository extends JpaRepository<Serviciotarifario, Integer>,
		JpaSpecificationExecutor<Serviciotarifario>, QuerydslPredicateExecutor<Serviciotarifario> {

}

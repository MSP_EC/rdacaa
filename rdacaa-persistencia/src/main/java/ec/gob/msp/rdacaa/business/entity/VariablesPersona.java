/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.gob.msp.rdacaa.business.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

import org.hibernate.annotations.Immutable;

/**
 *
 * @author msp
 */
@Entity
@Immutable
@Table(name = "VARIABLES_PERSONA", catalog = "", schema = "")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "VariablesPersona.findAll", query = "SELECT v FROM VariablesPersona v")})
public class VariablesPersona implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Column(name = "PER_NUM_ARCHIVO")
    private Integer perNumArchivo;
    @Column(name = "COD_PER_TIP_IDENTIFICACION")
    private Integer codPerTipIdentificacion;
    @Column(name = "COD_PER_TIP_IDENTIFICACION_DESC", length = 2000000000)
    private String codPerTipIdentificacionDesc;
    @Column(name = "COD_PER_IDENTIFICACION", length = 2000000000)
    private String codPerIdentificacion;
    @Column(name = "PER_FEC_NACIMIENTO", length = 2000000000)
    private String perFecNacimiento;
    @Column(name = "PER_PRI_NOMBRE", length = 2000000000)
    private String perPriNombre;
    @Column(name = "PER_SEG_NOMBRE", length = 2000000000)
    private String perSegNombre;
    @Column(name = "PER_PRI_APELLIDO", length = 2000000000)
    private String perPriApellido;
    @Column(name = "PER_SEG_APELLIDO", length = 2000000000)
    private String perSegApellido;
    @Column(name = "COD_PER_SEXO")
    private Integer codPerSexo;
    @Column(name = "COD_PER_SEXO_DESCRIPCION", length = 2000000000)
    private String codPerSexoDescripcion;
    @Column(name = "COD_PER_ORI_SEXUAL")
    private Integer codPerOriSexual;
    @Column(name = "COD_PER_ORI_SEXUAL_DESC", length = 2000000000)
    private String codPerOriSexualDesc;
    @Column(name = "COD_PER_IDE_GENERO")
    private Integer codPerIdeGenero;
    @Column(name = "COD_PER_IDE_GENERO_DESC", length = 2000000000)
    private String codPerIdeGeneroDesc;
    @Column(name = "COD_PER_NACIONALIDAD")
    private Integer codPerNacionalidad;
    @Column(name = "COD_PER_NACIONALIDAD_DESC", length = 2000000000)
    private String codPerNacionalidadDesc;
    @Column(name = "COD_PER_AUT_ETNICA")
    private Integer codPerAutEtnica;
    @Column(name = "COD_PER_AUT_ETNICA_DESC", length = 2000000000)
    private String codPerAutEtnicaDesc;
    @Column(name = "COD_PER_NAC_ETNICA")
    private Integer codPerNacEtnica;
    @Column(name = "COD_PER_NAC_ETNICA_DESC", length = 2000000000)
    private String codPerNacEtnicaDesc;
    @Column(name = "COD_PER_PUEBLOS")
    private Integer codPerPueblos;
    @Column(name = "COD_PER_PUEBLOS_DESC", length = 2000000000)
    private String codPerPueblosDesc;
    @Column(name = "COD_PER_PARROQUIA", length = 2000000000)
    private String codPerParroquia;
    @Column(name = "COD_PER_PARROQUIA_DESC", length = 2000000000)
    private String codPerParroquiaDesc;
    @Column(name = "PER_TEL_PACIENTE", length = 2000000000)
    private String perTelPaciente;
    @Column(name = "PER_TEL_FAMILIAR", length = 2000000000)
    private String perTelFamiliar;
    @Column(name = "PER_DIR_DOMICILIO", length = 2000000000)
    private String perDirDomicilio;
    @Column(name = "COD_PER_TIP_IDE_REPRESENTANTE")
    private Integer codPerTipIdeRepresentante;
    @Column(name = "COD_PER_TIP_IDE_REPRESENTANTE_DESC", length = 2000000000)
    private String codPerTipIdeRepresentanteDesc;
    @Column(name = "PER_IDE_REPRESENTANTE", length = 2000000000)
    private String perIdeRepresentante;

    public VariablesPersona() {
    }

    public Integer getPerNumArchivo() {
        return perNumArchivo;
    }

    public void setPerNumArchivo(Integer perNumArchivo) {
        this.perNumArchivo = perNumArchivo;
    }

    public Integer getCodPerTipIdentificacion() {
        return codPerTipIdentificacion;
    }

    public void setCodPerTipIdentificacion(Integer codPerTipIdentificacion) {
        this.codPerTipIdentificacion = codPerTipIdentificacion;
    }

    public String getCodPerTipIdentificacionDesc() {
        return codPerTipIdentificacionDesc;
    }

    public void setCodPerTipIdentificacionDesc(String codPerTipIdentificacionDesc) {
        this.codPerTipIdentificacionDesc = codPerTipIdentificacionDesc;
    }

    public String getCodPerIdentificacion() {
        return codPerIdentificacion;
    }

    public void setCodPerIdentificacion(String codPerIdentificacion) {
        this.codPerIdentificacion = codPerIdentificacion;
    }

    public String getPerFecNacimiento() {
        return perFecNacimiento;
    }

    public void setPerFecNacimiento(String perFecNacimiento) {
        this.perFecNacimiento = perFecNacimiento;
    }

    public String getPerPriNombre() {
        return perPriNombre;
    }

    public void setPerPriNombre(String perPriNombre) {
        this.perPriNombre = perPriNombre;
    }

    public String getPerSegNombre() {
        return perSegNombre;
    }

    public void setPerSegNombre(String perSegNombre) {
        this.perSegNombre = perSegNombre;
    }

    public String getPerPriApellido() {
        return perPriApellido;
    }

    public void setPerPriApellido(String perPriApellido) {
        this.perPriApellido = perPriApellido;
    }

    public String getPerSegApellido() {
        return perSegApellido;
    }

    public void setPerSegApellido(String perSegApellido) {
        this.perSegApellido = perSegApellido;
    }

    public Integer getCodPerSexo() {
        return codPerSexo;
    }

    public void setCodPerSexo(Integer codPerSexo) {
        this.codPerSexo = codPerSexo;
    }

    public String getCodPerSexoDescripcion() {
        return codPerSexoDescripcion;
    }

    public void setCodPerSexoDescripcion(String codPerSexoDescripcion) {
        this.codPerSexoDescripcion = codPerSexoDescripcion;
    }

    public Integer getCodPerOriSexual() {
        return codPerOriSexual;
    }

    public void setCodPerOriSexual(Integer codPerOriSexual) {
        this.codPerOriSexual = codPerOriSexual;
    }

    public String getCodPerOriSexualDesc() {
        return codPerOriSexualDesc;
    }

    public void setCodPerOriSexualDesc(String codPerOriSexualDesc) {
        this.codPerOriSexualDesc = codPerOriSexualDesc;
    }

    public Integer getCodPerIdeGenero() {
        return codPerIdeGenero;
    }

    public void setCodPerIdeGenero(Integer codPerIdeGenero) {
        this.codPerIdeGenero = codPerIdeGenero;
    }

    public String getCodPerIdeGeneroDesc() {
        return codPerIdeGeneroDesc;
    }

    public void setCodPerIdeGeneroDesc(String codPerIdeGeneroDesc) {
        this.codPerIdeGeneroDesc = codPerIdeGeneroDesc;
    }

    public Integer getCodPerNacionalidad() {
        return codPerNacionalidad;
    }

    public void setCodPerNacionalidad(Integer codPerNacionalidad) {
        this.codPerNacionalidad = codPerNacionalidad;
    }

    public String getCodPerNacionalidadDesc() {
        return codPerNacionalidadDesc;
    }

    public void setCodPerNacionalidadDesc(String codPerNacionalidadDesc) {
        this.codPerNacionalidadDesc = codPerNacionalidadDesc;
    }

    public Integer getCodPerAutEtnica() {
        return codPerAutEtnica;
    }

    public void setCodPerAutEtnica(Integer codPerAutEtnica) {
        this.codPerAutEtnica = codPerAutEtnica;
    }

    public String getCodPerAutEtnicaDesc() {
        return codPerAutEtnicaDesc;
    }

    public void setCodPerAutEtnicaDesc(String codPerAutEtnicaDesc) {
        this.codPerAutEtnicaDesc = codPerAutEtnicaDesc;
    }

    public Integer getCodPerNacEtnica() {
        return codPerNacEtnica;
    }

    public void setCodPerNacEtnica(Integer codPerNacEtnica) {
        this.codPerNacEtnica = codPerNacEtnica;
    }

    public String getCodPerNacEtnicaDesc() {
        return codPerNacEtnicaDesc;
    }

    public void setCodPerNacEtnicaDesc(String codPerNacEtnicaDesc) {
        this.codPerNacEtnicaDesc = codPerNacEtnicaDesc;
    }

    public Integer getCodPerPueblos() {
        return codPerPueblos;
    }

    public void setCodPerPueblos(Integer codPerPueblos) {
        this.codPerPueblos = codPerPueblos;
    }

    public String getCodPerPueblosDesc() {
        return codPerPueblosDesc;
    }

    public void setCodPerPueblosDesc(String codPerPueblosDesc) {
        this.codPerPueblosDesc = codPerPueblosDesc;
    }

    public String getCodPerParroquia() {
        return codPerParroquia;
    }

    public void setCodPerParroquia(String codPerParroquia) {
        this.codPerParroquia = codPerParroquia;
    }

    public String getCodPerParroquiaDesc() {
        return codPerParroquiaDesc;
    }

    public void setCodPerParroquiaDesc(String codPerParroquiaDesc) {
        this.codPerParroquiaDesc = codPerParroquiaDesc;
    }

    public String getPerTelPaciente() {
        return perTelPaciente;
    }

    public void setPerTelPaciente(String perTelPaciente) {
        this.perTelPaciente = perTelPaciente;
    }

    public String getPerTelFamiliar() {
        return perTelFamiliar;
    }

    public void setPerTelFamiliar(String perTelFamiliar) {
        this.perTelFamiliar = perTelFamiliar;
    }

    public String getPerDirDomicilio() {
        return perDirDomicilio;
    }

    public void setPerDirDomicilio(String perDirDomicilio) {
        this.perDirDomicilio = perDirDomicilio;
    }

    public Integer getCodPerTipIdeRepresentante() {
        return codPerTipIdeRepresentante;
    }

    public void setCodPerTipIdeRepresentante(Integer codPerTipIdeRepresentante) {
        this.codPerTipIdeRepresentante = codPerTipIdeRepresentante;
    }

    public String getCodPerTipIdeRepresentanteDesc() {
        return codPerTipIdeRepresentanteDesc;
    }

    public void setCodPerTipIdeRepresentanteDesc(String codPerTipIdeRepresentanteDesc) {
        this.codPerTipIdeRepresentanteDesc = codPerTipIdeRepresentanteDesc;
    }

    public String getPerIdeRepresentante() {
        return perIdeRepresentante;
    }

    public void setPerIdeRepresentante(String perIdeRepresentante) {
        this.perIdeRepresentante = perIdeRepresentante;
    }
    
}

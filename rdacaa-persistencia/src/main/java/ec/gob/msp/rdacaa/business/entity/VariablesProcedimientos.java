/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.gob.msp.rdacaa.business.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

import org.hibernate.annotations.Immutable;

/**
 *
 * @author msp
 */
@Entity
@Immutable
@Table(name = "VARIABLES_PROCEDIMIENTOS", catalog = "", schema = "")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "VariablesProcedimientos.findAll", query = "SELECT v FROM VariablesProcedimientos v")})
public class VariablesProcedimientos implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Column(name = "ID_ATENCION")
    private Integer idAtencion;
    @Column(name = "UUID_ATENCION", length = 2000000000)
    private String uuidAtencion;
    @Column(name = "COD_PROCEDIMIENTO_1", length = 2000000000)
    private String codProcedimiento1;
    @Column(name = "COD_PROCEDIMIENTO_DESC_1", length = 2000000000)
    private String codProcedimientoDesc1;
    @Column(name = "PROC_CANTIDAD_1", length = 2000000000)
    private String procCantidad1;
    @Column(name = "COD_PROCEDIMIENTO_2", length = 2000000000)
    private String codProcedimiento2;
    @Column(name = "COD_PROCEDIMIENTO_DESC_2", length = 2000000000)
    private String codProcedimientoDesc2;
    @Column(name = "PROC_CANTIDAD_2", length = 2000000000)
    private String procCantidad2;
    @Column(name = "COD_PROCEDIMIENTO_3", length = 2000000000)
    private String codProcedimiento3;
    @Column(name = "COD_PROCEDIMIENTO_DESC_3", length = 2000000000)
    private String codProcedimientoDesc3;
    @Column(name = "PROC_CANTIDAD_3", length = 2000000000)
    private String procCantidad3;
    @Column(name = "COD_PROCEDIMIENTO_4", length = 2000000000)
    private String codProcedimiento4;
    @Column(name = "COD_PROCEDIMIENTO_DESC_4", length = 2000000000)
    private String codProcedimientoDesc4;
    @Column(name = "PROC_CANTIDAD_4", length = 2000000000)
    private String procCantidad4;
    @Column(name = "COD_PROCEDIMIENTO_5", length = 2000000000)
    private String codProcedimiento5;
    @Column(name = "COD_PROCEDIMIENTO_DESC_5", length = 2000000000)
    private String codProcedimientoDesc5;
    @Column(name = "PROC_CANTIDAD_5", length = 2000000000)
    private String procCantidad5;

    public VariablesProcedimientos() {
    }

    public Integer getIdAtencion() {
        return idAtencion;
    }

    public void setIdAtencion(Integer idAtencion) {
        this.idAtencion = idAtencion;
    }

    public String getUuidAtencion() {
        return uuidAtencion;
    }

    public void setUuidAtencion(String uuidAtencion) {
        this.uuidAtencion = uuidAtencion;
    }

    public String getCodProcedimiento1() {
        return codProcedimiento1;
    }

    public void setCodProcedimiento1(String codProcedimiento1) {
        this.codProcedimiento1 = codProcedimiento1;
    }

    public String getCodProcedimientoDesc1() {
        return codProcedimientoDesc1;
    }

    public void setCodProcedimientoDesc1(String codProcedimientoDesc1) {
        this.codProcedimientoDesc1 = codProcedimientoDesc1;
    }

    public String getProcCantidad1() {
        return procCantidad1;
    }

    public void setProcCantidad1(String procCantidad1) {
        this.procCantidad1 = procCantidad1;
    }

    public String getCodProcedimiento2() {
        return codProcedimiento2;
    }

    public void setCodProcedimiento2(String codProcedimiento2) {
        this.codProcedimiento2 = codProcedimiento2;
    }

    public String getCodProcedimientoDesc2() {
        return codProcedimientoDesc2;
    }

    public void setCodProcedimientoDesc2(String codProcedimientoDesc2) {
        this.codProcedimientoDesc2 = codProcedimientoDesc2;
    }

    public String getProcCantidad2() {
        return procCantidad2;
    }

    public void setProcCantidad2(String procCantidad2) {
        this.procCantidad2 = procCantidad2;
    }

    public String getCodProcedimiento3() {
        return codProcedimiento3;
    }

    public void setCodProcedimiento3(String codProcedimiento3) {
        this.codProcedimiento3 = codProcedimiento3;
    }

    public String getCodProcedimientoDesc3() {
        return codProcedimientoDesc3;
    }

    public void setCodProcedimientoDesc3(String codProcedimientoDesc3) {
        this.codProcedimientoDesc3 = codProcedimientoDesc3;
    }

    public String getProcCantidad3() {
        return procCantidad3;
    }

    public void setProcCantidad3(String procCantidad3) {
        this.procCantidad3 = procCantidad3;
    }

    public String getCodProcedimiento4() {
        return codProcedimiento4;
    }

    public void setCodProcedimiento4(String codProcedimiento4) {
        this.codProcedimiento4 = codProcedimiento4;
    }

    public String getCodProcedimientoDesc4() {
        return codProcedimientoDesc4;
    }

    public void setCodProcedimientoDesc4(String codProcedimientoDesc4) {
        this.codProcedimientoDesc4 = codProcedimientoDesc4;
    }

    public String getProcCantidad4() {
        return procCantidad4;
    }

    public void setProcCantidad4(String procCantidad4) {
        this.procCantidad4 = procCantidad4;
    }

    public String getCodProcedimiento5() {
        return codProcedimiento5;
    }

    public void setCodProcedimiento5(String codProcedimiento5) {
        this.codProcedimiento5 = codProcedimiento5;
    }

    public String getCodProcedimientoDesc5() {
        return codProcedimientoDesc5;
    }

    public void setCodProcedimientoDesc5(String codProcedimientoDesc5) {
        this.codProcedimientoDesc5 = codProcedimientoDesc5;
    }

    public String getProcCantidad5() {
        return procCantidad5;
    }

    public void setProcCantidad5(String procCantidad5) {
        this.procCantidad5 = procCantidad5;
    }
    
}

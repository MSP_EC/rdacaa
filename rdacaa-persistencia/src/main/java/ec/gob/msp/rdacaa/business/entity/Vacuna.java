package ec.gob.msp.rdacaa.business.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author msp
 */
@Entity
@Table(name = "vacuna", uniqueConstraints = { @UniqueConstraint(columnNames = { "id" }) })
@XmlRootElement
@NamedQueries({ @NamedQuery(name = "Vacuna.findAll", query = "SELECT v FROM Vacuna v") })
public class Vacuna implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
	@Basic(optional = false)
	@Column(name = "id", nullable = false)
	private Integer id;
	@Column(name = "nombrevacuna", length = 200)
	private String nombrevacuna;
	@Column(name = "descripcionvacuna", length = 500)
	private String descripcionvacuna;
	@Column(name = "hombre")
	private Integer hombre;
	@Column(name = "mujer")
	private Integer mujer;
	@Column(name = "intersexual")
	private Integer intersexual;

	public Vacuna() {
	}

	public Vacuna(Integer id) {
		this.id = id;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNombrevacuna() {
		return nombrevacuna;
	}

	public void setNombrevacuna(String nombrevacuna) {
		this.nombrevacuna = nombrevacuna;
	}

	public String getDescripcionvacuna() {
		return descripcionvacuna;
	}

	public void setDescripcionvacuna(String descripcionvacuna) {
		this.descripcionvacuna = descripcionvacuna;
	}

	public Integer getHombre() {
		return hombre;
	}

	public void setHombre(Integer hombre) {
		this.hombre = hombre;
	}

	public Integer getMujer() {
		return mujer;
	}

	public void setMujer(Integer mujer) {
		this.mujer = mujer;
	}

	public Integer getIntersexual() {
		return intersexual;
	}

	public void setIntersexual(Integer intersexual) {
		this.intersexual = intersexual;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Vacuna other = (Vacuna) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "ec.gob.msp.rdacaa.business.entity.Vacuna[ id=" + id + " ]";
	}

}

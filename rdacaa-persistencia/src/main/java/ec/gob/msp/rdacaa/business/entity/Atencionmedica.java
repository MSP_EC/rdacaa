package ec.gob.msp.rdacaa.business.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;
import javax.xml.bind.annotation.XmlRootElement;

import org.hibernate.annotations.CreationTimestamp;

/**
 *
 * @author msp
 */
@Entity
@Table(name = "atencionmedica", uniqueConstraints = { @UniqueConstraint(columnNames = { "id" }),
		@UniqueConstraint(columnNames = { "uuid" }) })
@XmlRootElement
@NamedQueries({ @NamedQuery(name = "Atencionmedica.findAll", query = "SELECT a FROM Atencionmedica a") })
public class Atencionmedica implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Basic(optional = false)
	@Column(name = "id", nullable = false)
	private Integer id;
	@Basic(optional = false)
	@Column(name = "uuid", nullable = false, length = 36)
	private String uuid;
	@Basic(optional = false)
	@Column(name = "entidad_id", nullable = false)
	private int entidadId;
	@Column(name = "fechacreacion")
	@Temporal(TemporalType.TIMESTAMP)
	@CreationTimestamp
	private Date fechacreacion;
	@Column(name = "fechaatencion")
	@Temporal(TemporalType.TIMESTAMP)
	private Date fechaatencion;
	@Column(name = "estado")
	private Integer estado;
	@Column(name = "ks_cident", length = 250)
	private String ksCident;
	@Column(name = "ku_cident", length = 250)
	private String kuCident;
	@Column(name = "ku_keyid", length = 250)
	private String kuKeyid;
	@JoinColumn(name = "ctespecialidadmedica_id", referencedColumnName = "id")
	@ManyToOne
	private Detallecatalogo ctespecialidadmedicaId;
	@JoinColumn(name = "cttipobono_id", referencedColumnName = "id")
	@ManyToOne
	private Detallecatalogo cttipobonoId;
	@JoinColumn(name = "cttiposegurosalud_id", referencedColumnName = "id")
	@ManyToOne
	private Detallecatalogo cttiposegurosaludId;
	@JoinColumn(name = "usuariocreacion_id", referencedColumnName = "id")
	@ManyToOne
	private Persona usuariocreacionId;
	@JoinColumn(name = "ubicacion_parroquia_id", referencedColumnName = "id")
    @ManyToOne
    private Parroquia ubicacionParroquiaId;

	public Atencionmedica() {
	}

	public Atencionmedica(Integer id) {
		this.id = id;
	}

	public Atencionmedica(Integer id, String uuid, int entidadId) {
		this.id = id;
		this.uuid = uuid;
		this.entidadId = entidadId;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public int getEntidadId() {
		return entidadId;
	}

	public void setEntidadId(int entidadId) {
		this.entidadId = entidadId;
	}

	public Date getFechacreacion() {
		return fechacreacion;
	}

	public void setFechacreacion(Date fechacreacion) {
		this.fechacreacion = fechacreacion;
	}

	public Date getFechaatencion() {
		return fechaatencion;
	}

	public void setFechaatencion(Date fechaatencion) {
		this.fechaatencion = fechaatencion;
	}

	public Integer getEstado() {
		return estado;
	}

	public void setEstado(Integer estado) {
		this.estado = estado;
	}
    
	public String getKsCident() {
		return ksCident;
	}

	public void setKsCident(String ksCident) {
		this.ksCident = ksCident;
	}

	public String getKuCident() {
		return kuCident;
	}

	public void setKuCident(String kuCident) {
		this.kuCident = kuCident;
	}

	public String getKuKeyid() {
		return kuKeyid;
	}

	public void setKuKeyid(String kuKeyid) {
		this.kuKeyid = kuKeyid;
	}

	public Detallecatalogo getCtespecialidadmedicaId() {
		return ctespecialidadmedicaId;
	}

	public void setCtespecialidadmedicaId(Detallecatalogo ctespecialidadmedicaId) {
		this.ctespecialidadmedicaId = ctespecialidadmedicaId;
	}

	public Detallecatalogo getCttipobonoId() {
		return cttipobonoId;
	}

	public void setCttipobonoId(Detallecatalogo cttipobonoId) {
		this.cttipobonoId = cttipobonoId;
	}

	public Detallecatalogo getCttiposegurosaludId() {
		return cttiposegurosaludId;
	}

	public void setCttiposegurosaludId(Detallecatalogo cttiposegurosaludId) {
		this.cttiposegurosaludId = cttiposegurosaludId;
	}

	public Persona getUsuariocreacionId() {
		return usuariocreacionId;
	}

	public void setUsuariocreacionId(Persona usuariocreacionId) {
		this.usuariocreacionId = usuariocreacionId;
	}
	
    public Parroquia getUbicacionParroquiaId() {
        return ubicacionParroquiaId;
    }

    public void setUbicacionParroquiaId(Parroquia ubicacionParroquiaId) {
        this.ubicacionParroquiaId = ubicacionParroquiaId;
    }

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((uuid == null) ? 0 : uuid.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Atencionmedica other = (Atencionmedica) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (uuid == null) {
			if (other.uuid != null)
				return false;
		} else if (!uuid.equals(other.uuid))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "ec.gob.msp.rdacaa.business.entity.Atencionmedica[ id=" + id + " ]";
	}

}

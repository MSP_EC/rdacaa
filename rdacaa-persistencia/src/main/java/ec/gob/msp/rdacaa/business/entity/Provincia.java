package ec.gob.msp.rdacaa.business.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author msp
 */
@Entity
@Table(name = "provincia")
@XmlRootElement
@NamedQueries({ @NamedQuery(name = "Provincia.findAll", query = "SELECT p FROM Provincia p") })
public class Provincia implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
	@Basic(optional = false)
	@Column(name = "id", nullable = false)
	private Integer id;
	@Column(name = "codprovincia", length = 150)
	private String codprovincia;
	@Basic(optional = false)
	@Column(name = "descripcion", nullable = false, length = 250)
	private String descripcion;
	@JoinColumn(name = "ctregion_id", referencedColumnName = "id")
	@ManyToOne
	private Detallecatalogo ctregionId;
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "provinciaId")
	private List<Canton> cantonList;

	public Provincia() {
	}

	public Provincia(Integer id) {
		this.id = id;
	}

	public Provincia(Integer id, String descripcion) {
		this.id = id;
		this.descripcion = descripcion;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getCodprovincia() {
		return codprovincia;
	}

	public void setCodprovincia(String codprovincia) {
		this.codprovincia = codprovincia;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Detallecatalogo getCtregionId() {
		return ctregionId;
	}

	public void setCtregionId(Detallecatalogo ctregionId) {
		this.ctregionId = ctregionId;
	}

	@XmlTransient
	public List<Canton> getCantonList() {
		return cantonList;
	}

	public void setCantonList(List<Canton> cantonList) {
		this.cantonList = cantonList;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Provincia other = (Provincia) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "ec.gob.msp.rdacaa.business.entity.Provincia[ id=" + id + " ]";
	}

}

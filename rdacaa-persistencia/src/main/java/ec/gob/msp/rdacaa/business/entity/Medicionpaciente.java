package ec.gob.msp.rdacaa.business.entity;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author msp
 */
@Entity
@Table(name = "medicionpaciente", uniqueConstraints = {
    @UniqueConstraint(columnNames = {"id"})})
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Medicionpaciente.findAll", query = "SELECT m FROM Medicionpaciente m")})
public class Medicionpaciente implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id", nullable = false)
    private Integer id;
    @Column(name = "valor", length = 30)
    private String valor;
    @Column(name = "posiciontalla")
    private Integer posiciontalla;
    @JoinColumn(name = "atencionmedica_id", referencedColumnName = "id", nullable = false)
    @ManyToOne(optional = false)
    private Atencionmedica atencionmedicaId;
    @JoinColumn(name = "ctantropovital_id", referencedColumnName = "id", nullable = false)
    @ManyToOne(optional = false)
    private Detallecatalogo ctantropovitalId;
    @JoinColumn(name = "signosvitalesalertas_id", referencedColumnName = "id")
    @ManyToOne
    private Signosvitalesalertas signosvitalesalertasId;

    public Medicionpaciente() {
    }

    public Medicionpaciente(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    public Integer getPosiciontalla() {
		return posiciontalla;
	}

	public void setPosiciontalla(Integer posiciontalla) {
		this.posiciontalla = posiciontalla;
	}

	public Atencionmedica getAtencionmedicaId() {
        return atencionmedicaId;
    }

    public void setAtencionmedicaId(Atencionmedica atencionmedicaId) {
        this.atencionmedicaId = atencionmedicaId;
    }

    public Detallecatalogo getCtantropovitalId() {
        return ctantropovitalId;
    }

    public void setCtantropovitalId(Detallecatalogo ctantropovitalId) {
        this.ctantropovitalId = ctantropovitalId;
    }

    public Signosvitalesalertas getSignosvitalesalertasId() {
        return signosvitalesalertasId;
    }

    public void setSignosvitalesalertasId(Signosvitalesalertas signosvitalesalertasId) {
        this.signosvitalesalertasId = signosvitalesalertasId;
    }

    @Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Medicionpaciente other = (Medicionpaciente) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "ec.gob.msp.rdacaa.business.entity.Medicionpaciente[ id=" + id + " ]";
	}

}

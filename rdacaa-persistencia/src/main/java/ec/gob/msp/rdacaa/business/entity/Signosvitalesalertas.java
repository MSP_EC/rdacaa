package ec.gob.msp.rdacaa.business.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author msp
 */
@Entity
@Table(name = "signosvitalesalertas")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Signosvitalesalertas.findAll", query = "SELECT s FROM Signosvitalesalertas s")})
public class Signosvitalesalertas implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "id", nullable = false)
    private Integer id;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "valorminimo", precision = 16, scale = 2)
    private BigDecimal valorminimo;
    @Column(name = "valormaximo", precision = 16, scale = 2)
    private BigDecimal valormaximo;
    @Column(name = "alerta", length = 50)
    private String alerta;
    @Column(name = "color", length = 50)
    private String color;
    @Column(name = "estado")
    private Integer estado;
    @JoinColumn(name = "signovitalregla_id", referencedColumnName = "id", nullable = false)
    @ManyToOne(optional = false)
    private Signosvitalesreglas signovitalreglaId;

    public Signosvitalesalertas() {
    }

    public Signosvitalesalertas(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public BigDecimal getValorminimo() {
        return valorminimo;
    }

    public void setValorminimo(BigDecimal valorminimo) {
        this.valorminimo = valorminimo;
    }

    public BigDecimal getValormaximo() {
        return valormaximo;
    }

    public void setValormaximo(BigDecimal valormaximo) {
        this.valormaximo = valormaximo;
    }

    public String getAlerta() {
        return alerta;
    }

    public void setAlerta(String alerta) {
        this.alerta = alerta;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public Integer getEstado() {
        return estado;
    }

    public void setEstado(Integer estado) {
        this.estado = estado;
    }

    public Signosvitalesreglas getSignovitalreglaId() {
        return signovitalreglaId;
    }

    public void setSignovitalreglaId(Signosvitalesreglas signovitalreglaId) {
        this.signovitalreglaId = signovitalreglaId;
    }

    @Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

    @Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Signosvitalesalertas other = (Signosvitalesalertas) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id)) {
			return false;
		}
		return true;
	}

    @Override
    public String toString() {
        return "ec.gob.msp.rdacaa.business.entity.Signosvitalesalertas[ id=" + id + " ]";
    }

}

package ec.gob.msp.rdacaa.business.service;

import ec.gob.msp.rdacaa.business.entity.QVariablesDatosObstetricos;
import ec.gob.msp.rdacaa.business.entity.VariablesDatosObstetricos;
import ec.gob.msp.rdacaa.business.repository.VariablesDatosObstetricosRepository;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.querydsl.core.types.dsl.BooleanExpression;

/**
* Generated by Spring Data Generator on 05/10/2018
*/
@Component
public class VariablesDatosObstetricosService {

	private VariablesDatosObstetricosRepository variablesDatosObstetricosRepository;

	@Autowired
	public VariablesDatosObstetricosService(VariablesDatosObstetricosRepository variablesDatosObstetricosRepository) {
		this.variablesDatosObstetricosRepository = variablesDatosObstetricosRepository;
	}

	public VariablesDatosObstetricos findOneByAtencionId(Integer atencionId) {
        QVariablesDatosObstetricos qVariablesDatosObstetricos = QVariablesDatosObstetricos.variablesDatosObstetricos;
        BooleanExpression isDato = qVariablesDatosObstetricos.idAtencion.eq(atencionId);
        Optional<VariablesDatosObstetricos> variablesDatosObstetricos = variablesDatosObstetricosRepository.findOne(isDato);
        return variablesDatosObstetricos.isPresent() ? variablesDatosObstetricos.get() : null;
	}
}

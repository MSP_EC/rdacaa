package ec.gob.msp.rdacaa.business.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author msp
 */
@Entity
@Table(name = "parroquia")
@XmlRootElement
@NamedQueries({ @NamedQuery(name = "Parroquia.findAll", query = "SELECT p FROM Parroquia p") })
public class Parroquia implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	@Basic(optional = false)
	@Column(name = "id", nullable = false)
	private Integer id;
	@Basic(optional = false)
	@Column(name = "descripcion", nullable = false, length = 100)
	private String descripcion;
	@Column(name = "codparroquia", length = 20)
	private String codparroquia;
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "parroquiaId")
	private List<Persona> personaList;
	@JoinColumn(name = "canton_id", referencedColumnName = "id", nullable = false)
	@ManyToOne(optional = false)
	private Canton cantonId;
	@JoinColumn(name = "cttipo_id", referencedColumnName = "id")
	@ManyToOne
	private Detallecatalogo cttipoId;

	public Parroquia() {
	}

	public Parroquia(Integer id) {
		this.id = id;
	}

	public Parroquia(Integer id, String descripcion) {
		this.id = id;
		this.descripcion = descripcion;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getCodparroquia() {
		return codparroquia;
	}

	public void setCodparroquia(String codparroquia) {
		this.codparroquia = codparroquia;
	}

	@XmlTransient
	public List<Persona> getPersonaList() {
		return personaList;
	}

	public void setPersonaList(List<Persona> personaList) {
		this.personaList = personaList;
	}

	public Canton getCantonId() {
		return cantonId;
	}

	public void setCantonId(Canton cantonId) {
		this.cantonId = cantonId;
	}

	public Detallecatalogo getCttipoId() {
		return cttipoId;
	}

	public void setCttipoId(Detallecatalogo cttipoId) {
		this.cttipoId = cttipoId;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Parroquia other = (Parroquia) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "ec.gob.msp.rdacaa.business.entity.Parroquia[ id=" + id + " ]";
	}

}

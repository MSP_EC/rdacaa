package ec.gob.msp.rdacaa.business.entity;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author msp
 */
@Entity
@Table(name = "procedimientoatencion")
@XmlRootElement
@NamedQueries({ @NamedQuery(name = "Procedimientoatencion.findAll", query = "SELECT p FROM Procedimientoatencion p") })
public class Procedimientoatencion implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
	@Basic(optional = false)
	@Column(name = "id", nullable = false)
	private Integer id;
	@Column(name = "numactividades")
	private Integer numactividades;
	@JoinColumn(name = "atencionmedica_id", referencedColumnName = "id", nullable = false)
	@ManyToOne(optional = false)
	private Atencionmedica atencionmedicaId;
	@JoinColumn(name = "detalleserviciotarifario_id", referencedColumnName = "id", nullable = false)
	@ManyToOne(optional = false)
	private Detalleserviciotarifario detalleserviciotarifarioId;
	@Column(name = "orden", nullable = false)
	private Integer orden;

	public Procedimientoatencion() {
	}

	public Procedimientoatencion(Integer id) {
		this.id = id;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getNumactividades() {
		return numactividades;
	}

	public void setNumactividades(Integer numactividades) {
		this.numactividades = numactividades;
	}

	public Atencionmedica getAtencionmedicaId() {
		return atencionmedicaId;
	}

	public void setAtencionmedicaId(Atencionmedica atencionmedicaId) {
		this.atencionmedicaId = atencionmedicaId;
	}

	public Detalleserviciotarifario getDetalleserviciotarifarioId() {
		return detalleserviciotarifarioId;
	}

	public void setDetalleserviciotarifarioId(Detalleserviciotarifario detalleserviciotarifarioId) {
		this.detalleserviciotarifarioId = detalleserviciotarifarioId;
	}

	public Integer getOrden() {
		return orden;
	}

	public void setOrden(Integer orden) {
		this.orden = orden;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Procedimientoatencion other = (Procedimientoatencion) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "ec.gob.msp.rdacaa.business.entity.Procedimientoatencion[ id=" + id + " ]";
	}

}

package ec.gob.msp.rdacaa.business.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.stereotype.Repository;

import ec.gob.msp.rdacaa.business.entity.Percentil;

/**
 * Generated by Spring Data Generator on 11/09/2018
 */
@Repository
public interface PercentilRepository extends JpaRepository<Percentil, Integer>, JpaSpecificationExecutor<Percentil>,
		QuerydslPredicateExecutor<Percentil> {

}

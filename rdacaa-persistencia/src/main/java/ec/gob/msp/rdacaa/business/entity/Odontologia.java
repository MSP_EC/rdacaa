package ec.gob.msp.rdacaa.business.entity;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author msp
 */
@Entity
@Table(name = "odontologia")
@XmlRootElement
@NamedQueries({ @NamedQuery(name = "Odontologia.findAll", query = "SELECT o FROM Odontologia o") })
public class Odontologia implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
	@Basic(optional = false)
	@Column(name = "id", nullable = false)
	private Integer id;
	@Column(name = "dientecariado")
	private Integer dientecariado;
	@Column(name = "dienteextraido")
	private Integer dienteextraido;
	@Column(name = "dienteobturado")
	private Integer dienteobturado;
	@Column(name = "dienteperdido")
	private Integer dienteperdido;
	@JoinColumn(name = "atencionmedica_id", referencedColumnName = "id", nullable = false)
	@ManyToOne(optional = false)
	private Atencionmedica atencionmedicaId;

	public Odontologia() {
	}

	public Odontologia(Integer id) {
		this.id = id;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getDientecariado() {
		return dientecariado;
	}

	public void setDientecaido(Integer dientecariado) {
		this.dientecariado = dientecariado;
	}

	public Integer getDienteextraido() {
		return dienteextraido;
	}

	public void setDienteextraido(Integer dienteextraido) {
		this.dienteextraido = dienteextraido;
	}

	public Integer getDienteobturado() {
		return dienteobturado;
	}

	public void setDienteobturado(Integer dienteobturado) {
		this.dienteobturado = dienteobturado;
	}

	public Integer getDienteperdido() {
		return dienteperdido;
	}

	public void setDienteperdido(Integer dienteperdido) {
		this.dienteperdido = dienteperdido;
	}

	public Atencionmedica getAtencionmedicaId() {
		return atencionmedicaId;
	}

	public void setAtencionmedicaId(Atencionmedica atencionmedicaId) {
		this.atencionmedicaId = atencionmedicaId;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Odontologia other = (Odontologia) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "ec.gob.msp.rdacaa.business.entity.Odontologia[ id=" + id + " ]";
	}

}

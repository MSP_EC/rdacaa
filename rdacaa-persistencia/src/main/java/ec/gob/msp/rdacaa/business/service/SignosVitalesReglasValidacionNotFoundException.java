package ec.gob.msp.rdacaa.business.service;

public class SignosVitalesReglasValidacionNotFoundException extends RuntimeException {

	private static final long serialVersionUID = -1450041366768391485L;

	public SignosVitalesReglasValidacionNotFoundException(String message) {
		super(message);
	}

}

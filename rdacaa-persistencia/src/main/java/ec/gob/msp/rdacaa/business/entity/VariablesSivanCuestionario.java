/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.gob.msp.rdacaa.business.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

import org.hibernate.annotations.Immutable;

/**
 *
 * @author msp
 */
@Entity
@Immutable
@Table(name = "VARIABLES_SIVAN_CUESTIONARIO", catalog = "", schema = "")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "VariablesSivanCuestionario.findAll", query = "SELECT v FROM VariablesSivanCuestionario v")})
public class VariablesSivanCuestionario implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Column(name = "ID_ATENCION")
    private Integer idAtencion;
    @Column(name = "UUID_ATENCION", length = 2000000000)
    private String uuidAtencion;
    @Column(name = "SIV_MAD_PER_LACTANCIA")
    private Integer sivMadPerLactancia;
    @Column(name = "SIV_MAD_PER_LACTANCIA_DESC", length = 2000000000)
    private String sivMadPerLactanciaDesc;
    @Column(name = "SIV_24H_LECHE")
    private Integer siv24hLeche;
    @Column(name = "SIV_24H_LECHE_DESC", length = 2000000000)
    private String siv24hLecheDesc;
    @Column(name = "SIV_24H_ALIMENTOS")
    private Integer siv24hAlimentos;
    @Column(name = "SIV_24H_ALIMENTOS_DESC", length = 2000000000)
    private String siv24hAlimentosDesc;

    public VariablesSivanCuestionario() {
    }

    public Integer getIdAtencion() {
        return idAtencion;
    }

    public void setIdAtencion(Integer idAtencion) {
        this.idAtencion = idAtencion;
    }

    public String getUuidAtencion() {
        return uuidAtencion;
    }

    public void setUuidAtencion(String uuidAtencion) {
        this.uuidAtencion = uuidAtencion;
    }

    public Integer getSivMadPerLactancia() {
        return sivMadPerLactancia;
    }

    public void setSivMadPerLactancia(Integer sivMadPerLactancia) {
        this.sivMadPerLactancia = sivMadPerLactancia;
    }

    public String getSivMadPerLactanciaDesc() {
        return sivMadPerLactanciaDesc;
    }

    public void setSivMadPerLactanciaDesc(String sivMadPerLactanciaDesc) {
        this.sivMadPerLactanciaDesc = sivMadPerLactanciaDesc;
    }

    public Integer getSiv24hLeche() {
        return siv24hLeche;
    }

    public void setSiv24hLeche(Integer siv24hLeche) {
        this.siv24hLeche = siv24hLeche;
    }

    public String getSiv24hLecheDesc() {
        return siv24hLecheDesc;
    }

    public void setSiv24hLecheDesc(String siv24hLecheDesc) {
        this.siv24hLecheDesc = siv24hLecheDesc;
    }

    public Integer getSiv24hAlimentos() {
        return siv24hAlimentos;
    }

    public void setSiv24hAlimentos(Integer siv24hAlimentos) {
        this.siv24hAlimentos = siv24hAlimentos;
    }

    public String getSiv24hAlimentosDesc() {
        return siv24hAlimentosDesc;
    }

    public void setSiv24hAlimentosDesc(String siv24hAlimentosDesc) {
        this.siv24hAlimentosDesc = siv24hAlimentosDesc;
    }
    
}

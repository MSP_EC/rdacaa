package ec.gob.msp.rdacaa.business.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author msp
 */
@Entity
@Table(name = "detallecatalogo")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Detallecatalogo.findAll", query = "SELECT d FROM Detallecatalogo d")})
public class Detallecatalogo implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "id", nullable = false)
    private Integer id;
    @Column(name = "descripcion", length = 250)
    private String descripcion;
    @Column(name = "valor", length = 250)
    private String valor;
    @Column(name = "detallecatalogo_id")
    private Integer detallecatalogoId;
    @Column(name = "ordenvisualizacion")
    private Integer ordenvisualizacion;
    @JoinColumn(name = "catalogo_id", referencedColumnName = "id", nullable = false)
    @ManyToOne(optional = false)
    private Catalogo catalogoId;

    public Detallecatalogo() {
    }

    public Detallecatalogo(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    public Integer getDetallecatalogoId() {
        return detallecatalogoId;
    }

    public void setDetallecatalogoId(Integer detallecatalogoId) {
        this.detallecatalogoId = detallecatalogoId;
    }

    public Catalogo getCatalogoId() {
        return catalogoId;
    }

    public void setCatalogoId(Catalogo catalogoId) {
        this.catalogoId = catalogoId;
    }

    public Integer getOrdenvisualizacion() {
        return ordenvisualizacion;
    }

    public void setOrdenvisualizacion(Integer ordenvisualizacion) {
        this.ordenvisualizacion = ordenvisualizacion;
    }

    @Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

    @Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Detallecatalogo other = (Detallecatalogo) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id)) {
			return false;
		}
		return true;
	}

    @Override
    public String toString() {
        return "ec.gob.msp.rdacaa.business.entity.Detallecatalogo[ id=" + id + " ]";
    }
    
}
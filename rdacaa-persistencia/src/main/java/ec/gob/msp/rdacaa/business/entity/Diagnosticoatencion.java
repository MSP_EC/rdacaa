package ec.gob.msp.rdacaa.business.entity;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author msp
 */
@Entity
@Table(name = "diagnosticoatencion")
@XmlRootElement
@NamedQueries({ @NamedQuery(name = "Diagnosticoatencion.findAll", query = "SELECT d FROM Diagnosticoatencion d") })
public class Diagnosticoatencion implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	@Basic(optional = false)
	@Column(name = "id", nullable = false)
	private Integer id;
	@Column(name = "estado")
	private Integer estado;
	@Column(name = "observacion")
	private Integer observacion;
	@JoinColumn(name = "atencionmedica_id", referencedColumnName = "id", nullable = false)
	@ManyToOne(optional = false)
	private Atencionmedica atencionmedicaId;
	@JoinColumn(name = "cie_id", referencedColumnName = "id", nullable = false)
	@ManyToOne(optional = false)
	private Cie cieId;
	@JoinColumn(name = "ctcondiciondiagnositco_id", referencedColumnName = "id")
	@ManyToOne
	private Detallecatalogo ctcondiciondiagnositcoId;
	@JoinColumn(name = "ctmorbilidaddiagnostico_id", referencedColumnName = "id", nullable = false)
	@ManyToOne(optional = false)
	private Detallecatalogo ctmorbilidaddiagnosticoId;
	@JoinColumn(name = "ctprevenciondiagnostico_id", referencedColumnName = "id", nullable = false)
	@ManyToOne(optional = false)
	private Detallecatalogo ctprevenciondiagnosticoId;
	@Column(name = "numerocertificado")
	private String numerocertificado;
        @Column(name = "orden", nullable = false)
	private Integer orden;

	public Diagnosticoatencion() {
	}

	public Diagnosticoatencion(Integer id) {
		this.id = id;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getEstado() {
		return estado;
	}

	public void setEstado(Integer estado) {
		this.estado = estado;
	}

	public Integer getObservacion() {
		return observacion;
	}

	public void setObservacion(Integer observacion) {
		this.observacion = observacion;
	}

	public Atencionmedica getAtencionmedicaId() {
		return atencionmedicaId;
	}

	public void setAtencionmedicaId(Atencionmedica atencionmedicaId) {
		this.atencionmedicaId = atencionmedicaId;
	}

	public Cie getCieId() {
		return cieId;
	}

	public void setCieId(Cie cieId) {
		this.cieId = cieId;
	}

	public Detallecatalogo getCtcondiciondiagnositcoId() {
		return ctcondiciondiagnositcoId;
	}

	public void setCtcondiciondiagnositcoId(Detallecatalogo ctcondiciondiagnositcoId) {
		this.ctcondiciondiagnositcoId = ctcondiciondiagnositcoId;
	}

	public Detallecatalogo getCtmorbilidaddiagnosticoId() {
		return ctmorbilidaddiagnosticoId;
	}

	public void setCtmorbilidaddiagnosticoId(Detallecatalogo ctmorbilidaddiagnosticoId) {
		this.ctmorbilidaddiagnosticoId = ctmorbilidaddiagnosticoId;
	}

	public Detallecatalogo getCtprevenciondiagnosticoId() {
		return ctprevenciondiagnosticoId;
	}

	public void setCtprevenciondiagnosticoId(Detallecatalogo ctprevenciondiagnosticoId) {
		this.ctprevenciondiagnosticoId = ctprevenciondiagnosticoId;
	}

        public String getNumerocertificado() {
            return numerocertificado;
        }

        public void setNumerocertificado(String numerocertificado) {
            this.numerocertificado = numerocertificado;
        }


	public Integer getOrden() {
		return orden;
	}

	public void setOrden(Integer orden) {
		this.orden = orden;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Diagnosticoatencion other = (Diagnosticoatencion) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "ec.gob.msp.rdacaa.business.entity.Diagnosticoatencion[ id=" + id + " ]";
	}

}

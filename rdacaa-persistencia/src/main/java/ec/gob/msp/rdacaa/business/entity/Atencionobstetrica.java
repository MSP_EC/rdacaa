package ec.gob.msp.rdacaa.business.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author msp
 */
@Entity
@Table(name = "atencionobstetrica")
@XmlRootElement
@NamedQueries({ @NamedQuery(name = "Atencionobstetrica.findAll", query = "SELECT a FROM Atencionobstetrica a") })
public class Atencionobstetrica implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	@Basic(optional = false)
	@Column(name = "id", nullable = false)
	private Integer id;
	@Column(name = "plandeparto")
	private Integer plandeparto;
	@Column(name = "embarazoplanificado")
	private Integer embarazoplanificado;
	@Column(name = "fum")
	@Temporal(TemporalType.TIMESTAMP)
	private Date fum;
	@Column(name = "semgestacion")
	private Double semgestacion;
	@Column(name = "plantransporte")
	private Integer plantransporte;
	@JoinColumn(name = "atencionmedica_id", referencedColumnName = "id", nullable = false)
	@ManyToOne(optional = false)
	private Atencionmedica atencionmedicaId;
	@JoinColumn(name = "ctmetodosemgestacion_id", referencedColumnName = "id", nullable = false)
	@ManyToOne(optional = false)
	private Detallecatalogo ctmetodosemgestacionId;
	@JoinColumn(name = "ctriesgoobstetrico_id", referencedColumnName = "id", nullable = false)
	@ManyToOne(optional = false)
	private Detallecatalogo ctriesgoobstetricoId;

	public Atencionobstetrica() {
	}

	public Atencionobstetrica(Integer id) {
		this.id = id;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getPlandeparto() {
		return plandeparto;
	}

	public void setPlandeparto(Integer plandeparto) {
		this.plandeparto = plandeparto;
	}

	public Integer getEmbarazoplanificado() {
		return embarazoplanificado;
	}

	public void setEmbarazoplanificado(Integer embarazoplanificado) {
		this.embarazoplanificado = embarazoplanificado;
	}

	public Date getFum() {
		return fum;
	}

	public void setFum(Date fum) {
		this.fum = fum;
	}

	public Double getSemgestacion() {
		return semgestacion;
	}

	public void setSemgestacion(Double semgestacion) {
		this.semgestacion = semgestacion;
	}

	public Integer getPlantransporte() {
		return plantransporte;
	}

	public void setPlantransporte(Integer plantransporte) {
		this.plantransporte = plantransporte;
	}

	public Atencionmedica getAtencionmedicaId() {
		return atencionmedicaId;
	}

	public void setAtencionmedicaId(Atencionmedica atencionmedicaId) {
		this.atencionmedicaId = atencionmedicaId;
	}

	public Detallecatalogo getCtmetodosemgestacionId() {
		return ctmetodosemgestacionId;
	}

	public void setCtmetodosemgestacionId(Detallecatalogo ctmetodosemgestacionId) {
		this.ctmetodosemgestacionId = ctmetodosemgestacionId;
	}

	public Detallecatalogo getCtriesgoobstetricoId() {
		return ctriesgoobstetricoId;
	}

	public void setCtriesgoobstetricoId(Detallecatalogo ctriesgoobstetricoId) {
		this.ctriesgoobstetricoId = ctriesgoobstetricoId;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Atencionobstetrica other = (Atencionobstetrica) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "ec.gob.msp.rdacaa.business.entity.Atencionobstetrica[ id=" + id + " ]";
	}

}
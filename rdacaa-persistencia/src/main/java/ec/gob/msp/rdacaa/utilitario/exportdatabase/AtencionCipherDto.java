package ec.gob.msp.rdacaa.utilitario.exportdatabase;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class AtencionCipherDto {
	
	private String uuid;
	private String ksCident;
	private String kuCident;
	private String kuKeyid;
}

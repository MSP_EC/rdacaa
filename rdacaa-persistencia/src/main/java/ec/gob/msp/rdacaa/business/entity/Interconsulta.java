package ec.gob.msp.rdacaa.business.entity;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author msp
 */
@Entity
@Table(name = "interconsulta")
@XmlRootElement
@NamedQueries({ @NamedQuery(name = "Interconsulta.findAll", query = "SELECT i FROM Interconsulta i") })
public class Interconsulta implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
	@Basic(optional = false)
	@Column(name = "id", nullable = false)
	private Integer id;
	@Column(name = "descripcion", length = 250)
	private String descripcion;
	@JoinColumn(name = "atencionmedica_id", referencedColumnName = "id", nullable = false)
	@ManyToOne(optional = false)
	private Atencionmedica atencionmedicaId;
	@JoinColumn(name = "ctespecialidadconsultada_id", referencedColumnName = "id")
	@ManyToOne
	private Detallecatalogo ctespecialidadconsultadaId;
	@JoinColumn(name = "ctespecialidadsolicitada_id", referencedColumnName = "id")
	@ManyToOne
	private Detallecatalogo ctespecialidadsolicitadaId;
	@JoinColumn(name = "cttipo_id", referencedColumnName = "id")
	@ManyToOne
	private Detallecatalogo cttipoId;

	public Interconsulta() {
	}

	public Interconsulta(Integer id) {
		this.id = id;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Atencionmedica getAtencionmedicaId() {
		return atencionmedicaId;
	}

	public void setAtencionmedicaId(Atencionmedica atencionmedicaId) {
		this.atencionmedicaId = atencionmedicaId;
	}

	public Detallecatalogo getCtespecialidadconsultadaId() {
		return ctespecialidadconsultadaId;
	}

	public void setCtespecialidadconsultadaId(Detallecatalogo ctespecialidadconsultadaId) {
		this.ctespecialidadconsultadaId = ctespecialidadconsultadaId;
	}

	public Detallecatalogo getCtespecialidadsolicitadaId() {
		return ctespecialidadsolicitadaId;
	}

	public void setCtespecialidadsolicitadaId(Detallecatalogo ctespecialidadsolicitadaId) {
		this.ctespecialidadsolicitadaId = ctespecialidadsolicitadaId;
	}

	public Detallecatalogo getCttipoId() {
		return cttipoId;
	}

	public void setCttipoId(Detallecatalogo cttipoId) {
		this.cttipoId = cttipoId;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Interconsulta other = (Interconsulta) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "ec.gob.msp.rdacaa.business.entity.Interconsulta[ id=" + id + " ]";
	}

}

# RDACAA 2.0
SOFTWARE REGISTRO DIARIO AUTOMATIZADO DE CONSULTAS Y ATENCIONES AMBULATORIAS RDACAA VERSIÓN 2.0
### Pre-requisitos 📋
Para le ejecución del sistema se necesitan los siguientes elementos:
*  Java 1.8.0 o superior
* Sqlite 3.0

## Installation 
Descargar instalador desde [PRAS - Plataforma Registros de Atenciones de Salud](https://sgrdacaa-valrdaca.msp.gob.ec/) para instalar RDACAA 2.0

#Instalación en Linux 🔧

```bash
* tar -Jxvf nombre_archivo.tar.xz
* chmod 775 create_lanzador.sh
* chmod 775 execute.sh
* chmod 775 -R jre1.8.0_181/
* sudo ./create_lanzador.sh
* sudo chown -R grupo:usuario /opt/rdacaa/
```
#Instalación en Windows 🔧
```bash
* Descomprimir los archivos en una carpeta en el disco C de preferencia aunque puede funcionar desde cualquier lugar.
* Ubicar el archivo RDACAA_2.0.exe y generar un acceso directo hacia o desde el escritorio.
* Ejecutar la aplicación
```

## Construido con 🛠️
* [Sqlite](https://www.sqlite.org/download.html)
* [Java 8](https://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html)
* [Maven](https://maven.apache.org/download.cgi)
* [Spring Boot](https://mvnrepository.com/artifact/org.springframework.boot/spring-boot/2.0.4.RELEASE)
* [Java Swing](https://docs.oracle.com/javase/7/docs/api/javax/swing/package-summary.html)

## Contribuyendo 🖇️
Por favor leer el documento [CONTRIBUTING.md]

## Versionado 📌
Usamos [GitLab](https://git.msp.gob.ec/) para el versionado. Para todas las versiones disponibles, mira los [tags en este repositorio](https://git.msp.gob.ec/HCE/hcue-rdacaa/tags).

## Ver cambios con git log
git log --pretty=format:"##### %B %n> Fecha:  %ad con hash %h Realizado por %an" --date=short > CHANGELOG-temp.md

## Autores

* **David Murillo** - *Maintainer* - [bolivar.murillo](https://gitlab.com/bolivar.murillo)
* **Eduardo García** - *Developer* - [eduardo.garcia](https://gitlab.com/eduardo.garcia)
* **Maribel Vargas** - *Reporter* - [Maribel.vargas](https://gitlab.com/Maribel.vargas)
* **Brenda Curvelo** - *Reporter* - [Brenda.curvelo](https://gitlab.com/Brenda.Curvelo)
* **Anita Jimenez** - *Reporter* - [Anita.jimenez](https://gitlab.com/Maribel.vargas)
* **Miguel Faubla** - *Developer* - [miguel.faubla](https://gitlab.com/miguel.faubla)
* **Saulo Velasco** - *Developer* - [Saulo Velasco](https://git.msp.gob.ec/saulo.velasco)
* **Hilda Basabanda** - *Reporter* - [hilda.basabanda](https://gitlab.com/hilda.basabanda)
* **Wilson Quito** - *Reporter* - [Wilson.Quito](https://gitlab.com/Wilson.Quito)

## Licencia 📄
Este proyecto está bajo la Licencia (GNU) - mira el archivo [LICENSE.md](https://git.msp.gob.ec/HCE/hcue-rdacaa/blob/master/LICENSE) para detalles


# Listado de las personas que han participado del proyecto

## Autores

* **David Murillo** - *Maintainer* - [bolivar.murillo](https://gitlab.com/bolivar.murillo)
* **David Tabango** - *Maintainer* - [david.tabango](https://gitlab.com/david.tabango)
* **Diego Melo** - *Maintainer* - [diegomeloz1](https://gitlab.com/diegomeloz1)
* **Diego Vega** - *Developer* - [diego.vega](https://gitlab.com/diego.vega)
* **Eduardo García** - *Developer* - [eduardo.garcia](https://gitlab.com/eduardo.garcia)
* **Fausto Suarez** - *Developer* - [naibaff](https://gitlab.com/naibaff)
* **Jonathan Finlay** - *Owner* - [jfinlay](https://gitlab.com/jfinlay)
* **Karina Molina** - *Maintainer* - [karina.molina](https://gitlab.com/karina.molina)
* **Maribel Vargas** - *Reporter* - [Maribel.vargas](https://gitlab.com/Maribel.vargas)
* **Brenda Curvelo** - *Reporter* - [Brenda.curvelo](https://gitlab.com/Brenda.Curvelo)
* **Anita Jimenez** - *Reporter* - [Anita.jimenez](https://gitlab.com/Maribel.vargas)
* **Miguel Faubla** - *Developer* - [miguel.faubla](https://gitlab.com/miguel.faubla)
* **Nathali Armijos** - *Developer* - [natali.armijos](https://gitlab.com/natali.armijos)
* **Saulo Velasco** - *Developer* - [Saulo.Velasco](https://git.msp.gob.ec/saulo.velasco)
* **Paúl Ochoa** - *Developer* - [palichis](https://gitlab.com/palichis)
* **Hilda Basabanda** - *Reporter* - [hilda.basabanda](https://gitlab.com/hilda.basabanda)
* **Wilson Quito** - *Developer* - [Wilson.Quito](https://gitlab.com/Wilson.Quito)

